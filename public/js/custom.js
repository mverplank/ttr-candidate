 /*Custom code of JS*/ 
  
    $(document).on("ajaxStart", function() {
        if($('#media_append_section').length <= 0){
            $('#loadingDiv').show();
        }
    });

    $(document).on("ajaxStop", function() {
        $('#loadingDiv').hide();
    });
    //file size
    function formatFileSize(bytes,decimalPoint){
       if(bytes == 0) return '0 Bytes';
       var k = 1000,
           dm = decimalPoint || 2,
           sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
           i = Math.floor(Math.log(bytes) / Math.log(k));
       return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    function getAllMedia(type,dimension='', page_content='media'){
     
        var date = $('.filterMediaByDate').val();
        var title = $('#valueMediaByTitle').val();
        var url  = "/admin/cms/media/get_more_media";
        
        //var type = $('#media_type').attr('data-type');
        var data =  {'page':1, 'type':type,'dimension':dimension, 'title':title, 'date':date, 'page_content':page_content};
        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            if(page_content == "media"){
                console.log('yo so');
                $("#"+type).find('.jFiler-items').find('ul').html('');
                $("#media_append_section").css({'max-height':'none','overflow':'none','margin-top':'0px'});
                $("#"+type).find("ul.jFiler-items-list.jFiler-items-grid").append(response);
            }else{

                $("#media_append_section").find('.jFiler-items-list.jFiler-items-grid').html(response);
                //$("#media_append_section").css({'max-height':'none','overflow':'none','margin-top':'0px'});
            }            
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });
    }

    var maxLength = 45;  
    // For the audio upload in the show
    function initializeFileUpload(obj){
        
       /* extension = null;
        filesTypeError = " ";
        if($(obj).hasClass('host_audio')){
           extension = ['mp3', 'wav', '.wma', '.ogg'];
           filesTypeError = "Only upload Audio files";
        }
        var file_limit   = $(obj).data('files-max');
        var file_maxsize = $(obj).data('file-size-max');
        var image_for    = $(obj).data('files-for');
        var element_id   = $(obj).attr('id');
        var dir          = $(obj).data('dir');
        var module       = $(obj).data('module');
        var form_type    = $(obj).data('form_type');
        var unique_id    = $(obj).data('unique_id');
        var existing_files = [];
      
        var data = {'module' : module, 'folder' : image_for, 'id' : unique_id, 'form_type': form_type};
        makeAjaxRequest('POST', '/admin/uploads/get_files', data)
        .done(function(response) {
            if(response.status == 'success'){
                existing_files = response.data;
            }
            // Initialize the file upload            
            $('#user_files').filer({
                limit: file_limit,
                maxSize: file_maxsize,
                //extensions: ['jpg', 'jpeg', 'png', 'gif', 'psd', 'mp3', 'wav', 'avi', 'mov', 'mp4', '3gp', 'flv', 'pdf' ],
                extensions: extension,
                changeInput: true,
                showThumbs: true,
                addMore: true,
                uploadFile: {
                    url: APP_URL+"/admin/uploads/upload",
                    data: {'user_files':true, 'limit':file_limit, 'dir':dir, 'file_maxsize':file_maxsize, 'module':module, 'image_for':image_for, 'id':unique_id,'form_type': form_type},
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    beforeSend: function(){
                        
                    },
                    success: function(data, el){
                        //console.log(data);
                        if(data.upload == "error"){
                            swal({
                               title: '', 
                               text: data.message,
                               type: 'warning'
                            });
                        }else{
                            var parent = el.find(".jFiler-jProgressBar").parent();
                                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        }
                       
                    },
                    error: function(el){
                        console.log(el);
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    },
                    statusCode: null,
                    onProgress: null,
                    onComplete: null
                },
                init: function(){
                   // alert();
                },
                files: existing_files,
                onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                    if(file != undefined){
                        var data = [];
                        var data = {'file_path': file.path, 'ajax': true};
                        makeAjaxRequest('POST', '/admin/uploads/delete_files', data)
                    }
                },
                captions: {
                    button: "Choose Files",
                    feedback: "Choose files To Upload",
                    feedback2: "files were chosen",
                    drop: "Drop file here to Upload",
                    removeConfirmation: "Are you sure you want to remove this file?",
                    errors: {
                        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                        filesType: filesTypeError,
                        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                    }
                }
            });
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });*/
    }
    if($('#grid').length > 0){
        new AnimOnScroll(document.getElementById( 'grid' ), {
            minDuration : 0.4,
            maxDuration : 0.7,
            viewportFactor : 0.2
        });
    }
 
    $(document).ready(function () {

        ShowsToChannel();
        $("input[name='data[ContentCategory][is_custom_index_row]']").click(function(){
            var radioValue = $("input[name='data[ContentCategory][is_custom_index_row]']:checked").val();
            if(radioValue == 'image'){
                $('.custom_index_row_params_json').css('display','block');
            }else{
                 $('.custom_index_row_params_json').css('display','none');
            }
        });
        //form validation
        $( "#ContentDataAdminAddForm, #ContentDataAdminEditForm" ).validate({
          rules: {
            'data[ContentData][title]': {
               required: true,
             // url: true
            },
            'data[ContentData][external_url]': {
              // required: true,
              url: true
            }
          },

        });
        //variables used in load_media.js
        nextpage   = 2;
        totalpages = $('#media_type').data('total_media');
        pageArray  = [];

        $(".ItemClass").on("click", function () {
            //console.log($(this).closest("div.row").find(".ItemClass"));
            if ($(this).is(":checked")){
               $(this).closest("div.row").find(".ItemSetOpt").css('display', 'block'); 
            }else{
                 $(this).closest("div.row").find(".ItemSetOpt").css('display', 'none');
            }
            
        });
        if($('.ItemClass').is(":checked")){
            $(this).closest("div.row").find(".ItemSetOpt").css('display', 'block');
        }
        //Show already selected content category acc. to the id selected in url.
        if($('#ContentCategoryParentId').length > 0){
            var content_type_id = $('#content_type_id').data('id');
            $('#ContentCategoryParentId').val(content_type_id);
            $('#content_type_title').text($('#ContentCategoryParentId option:selected').text());
            
        }
        
        var content_category_id = $("#ContentCategoryParentId").val();
        var catediturl =APP_URL+"/admin/cms/content_categories/edit/"+content_category_id;
        var condaturl =APP_URL+"/admin/cms/content_data/add/"+content_category_id;
        if(content_category_id != ''){
            $('#content_cate_btn').attr("href", catediturl);
            $('#content_cate_btn').removeAttr("disabled");

            $('#content_data_url').attr("href", condaturl);
            $('#content_data_url').removeAttr("disabled");

            $('#content_data_url').attr("target", '_blank');
        }
        else{
             $('#content_cate_btn').attr("disabled", true);
             $('#content_data_url').attr("disabled", true);
        }
        // Disabled the options for the resheduled timing in the upcoming episodes
        $('#RescheduleEpisode option').each(function() {
            if ($(this).text().indexOf('Episode scheduled') >= 0){
                $(this).attr('disabled', 'disabled');
            } 
        });

        // For podcast statistics of episodes view pages
        if($('#distributed-series').length > 0){
            $.ChartJs.init([], []);
        }
        /* Start the datepicker code on the dashboard*/
        var start = moment().subtract(29, 'days');
        var end   = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            var start = start.format('YYYY-MM-DD');
            var end   = end.format('YYYY-MM-DD');
            favoritesStatsEpisode(start, end);
            favoritesStatsShow(start, end);
            favoritesStatsHost(start, end);
            favoritesStatsGuest(start, end);
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        if($('#favoritesStatsEpisode').length > 0){
            cb(start, end);
        }
        
        /* End the datpicker at the dashboard*/

        var check_click = 0;
        // Set the interval of the edit episode page
        if($('#episode_edit_page').length > 0){            
            var episode_id = $('#episode_edit_page').data('episode_id');
            function setEpisodeInterval(){
                $.ajax({
                    url : APP_URL+'/admin/show/episodes/set_interval',
                    type: 'post',
                    data: {'id':episode_id},
                    success: function(response){
                        // Perform operation on the return value
                        if(response[0].status == "error"){
                          
                            $('.custom-modal-text').html(response[0].message);
                            if(check_click == 0){                                
                                $('#open_confliction_message').trigger('click');
                            }          
                            check_click = 1;                  
                        }else{
                            check_click = 0;
                            Custombox.close();
                            stopEpisodeInterval();
                        }
                    }
                });
            }
            function stopEpisodeInterval() {
                clearInterval(episodeInterval);
            }
               
            var idleState = false;
            var idleTimer = null;
            $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {                   
                clearTimeout(idleTimer);
                if (idleState == true) { 
                    //$("body").css('background-color','#fff');  
                    // episodeInterval = setInterval(setEpisodeInterval,1000);    
                }
                idleState = false;
                idleTimer = setTimeout(function () {
                    idleState = true; 
                }, 1000);
            });
            $("body").trigger("mousemove");
        }
        $('.selectpicker').selectpicker('refresh');

        // Intialize the file object in media library
       /* if($('#filer_input1').length > 0){
           getAllMedia();
        }*/
      
        //Initialize catgeory tree (create an instance)
        var treeContentCategory = $('.content_category').jstree({
            "core": {
                'check_callback': true,
                "themes":{
                    "icons":false
                },
            },
            "contextmenu": {
                "items": function ($node) {
                    var tree = $(".content_category").jstree(true);
                    return {
                        "Rename": {
                            "label": "Rename",
                            "action" : function (data) {
                                var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                                inst.edit(obj);                                
                            }
                        },
                        "Create": {
                            "label": "Create",
                            "action": function (obj) { 
                                $node = tree.create_node($node);
                                tree.edit($node);
                            }

                        },
                        "Delete": {
                            "label": "Delete",
                            "action": function(obj){
                                swal({
                                    title: "Are you sure?",
                                    text: "You will not be able to recover this data!",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes, delete it!",
                                    closeOnConfirm: false
                                },
                                function(isConfirm){
                                    if (isConfirm) {
                                        tree.delete_node($node);
                                    }else{
                                        return false;
                                    }
                                });
                            }
                        },
                        "Edit": false,
                    };
                }
            },
            "plugins" : [ "themes", "html_data", "ui", "crrm", "dnd", "contextmenu" ]

        }).on('create_node.jstree', function (e, data) {
            var node = data;    
            var parent_node = $.jstree.reference(treeContentCategory).get_node(data.node.parent);
        
            $.post(APP_URL+'/admin/cms/getcategories', { 'parent_id' : parent_node.data.id, 'position' : data.position, 'name' : data.node.text, 'operation':'create_node' })
            .done(function (d) {
                if(data.node.data == null){
                    data.node.data = {};
                    //inputAttr.type = 'text';
                    var response_data = jQuery.parseJSON(d);
                    data.node.data.id = response_data.id;
                    data.instance.set_id(data.node.id, response_data.id);
                }else{
                    data.instance.set_id(data.node.id, response_data.id);
                }
            })
            .fail(function () {
              data.instance.refresh();
            });
        }).on("rename_node.jstree", function (event, data) {

            $.post(APP_URL+'/admin/cms/getcategories', { 'id' : data.node.data.id, 'position' : data.position, 'text' : data.node.text, 'operation':'rename_node' })
            .fail(function () {
                data.instance.refresh();
            });
            
        }).on('delete_node.jstree', function (e, data) {
           
            $.post(APP_URL+'/admin/cms/getcategories', { 'id' : data.node.data.id, 'operation':'delete_node' })
            .done(function(response) { 
                var response_data = jQuery.parseJSON(response);
                if(response_data.status == "success"){
                    swal({
                       title: 'Deleted!', 
                       text: 'The Category has been deleted successfully.',
                       type: 'success'
                    }); 
                }else if(response_data.status == "Error"){
                    swal({
                       title: 'Warning!', 
                       //text: response_data.msg,
                       text: "There occurs some error while deleting the category. Please try again! Also check if there is any manage entry inside this category. Make sure there does not exist any manage entry inside the category.",
                       type: 'warning'
                    }); 
                }
            })
            .fail(function (res) {
               
                data.instance.refresh();
            });
            
            //swal.close();
        }).on("move_node.jstree", function (e, data) {
           
            var current_node = data.node.data.id;
            var parent_node = $.jstree.reference(treeContentCategory).get_node(data.parent);
            $.post(APP_URL+'/admin/cms/getcategories', { 'id' : current_node, 'parent_id' : parent_node.data.id, 'position' : data.position, 'text' : data.node.text, 'operation':'move_node' })
            .fail(function () {
                data.instance.refresh();
            });
        });

        //Initialize catgeory tree (create an instance)
        var treeMain = $('.mtf-tree').jstree({
            "core": {
                'check_callback': true,
                "themes":{
                    "icons":false
                },
            },
            "contextmenu": {
                "items": function ($node) {
                    var tree = $(".mtf-tree").jstree(true);
                    return {
                        "Rename": {
                            "label": "Rename",
                            "action" : function (data) {
                                var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                                inst.edit(obj);                                
                            }
                        },
                        "Create": {
                            "label": "Create",
                            "action": function (obj) { 
                                $node = tree.create_node($node);
                                tree.edit($node);
                            }

                        },
                        "Delete": {
                            "label": "Delete",
                            "action": function(obj){
                                swal({
                                    title: "Are you sure?",
                                    text: "You will not be able to recover this data!",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Yes, delete it!",
                                    closeOnConfirm: false
                                },
                                function(isConfirm){
                                    if (isConfirm) {
                                        tree.delete_node($node);
                                    }else{
                                        return false;
                                    }
                                });
                            }
                        },
                        "Edit": false,
                    };
                }
            },
            "plugins" : [ "themes", "html_data", "ui", "crrm", "dnd", "contextmenu" ]

        }).on('create_node.jstree', function (e, data) {
            var node = data;    
            var parent_node = $.jstree.reference(treeMain).get_node(data.node.parent);
        
            $.post(APP_URL+'/admin/show/getcategories', { 'parent_id' : parent_node.data.id, 'position' : data.position, 'name' : data.node.text, 'type':'show', 'operation':'create_node' })
            .done(function (d) {
                if(data.node.data == null){
                    data.node.data = {};
                    //inputAttr.type = 'text';
                    var response_data = jQuery.parseJSON(d);
                    data.node.data.id = response_data.id;
                    data.instance.set_id(data.node.id, response_data.id);
                }else{
                    data.instance.set_id(data.node.id, response_data.id);
                }
            })
            .fail(function () {
              data.instance.refresh();
            });
        }).on("rename_node.jstree", function (event, data) {

            $.post(APP_URL+'/admin/show/getcategories', { 'id' : data.node.data.id, 'position' : data.position, 'text' : data.node.text, 'operation':'rename_node' })
            .fail(function () {
                data.instance.refresh();
            });
            
        }).on('delete_node.jstree', function (e, data) {
           
            $.post(APP_URL+'/admin/show/getcategories', { 'id' : data.node.data.id, 'operation':'delete_node' })
            .fail(function () {
                data.instance.refresh();
            });
            swal.close();
        }).on("move_node.jstree", function (e, data) {
           
            var current_node = data.node.data.id;
            var parent_node = $.jstree.reference(treeMain).get_node(data.parent);
            $.post(APP_URL+'/admin/show/getcategories', { 'id' : current_node, 'parent_id' : parent_node.data.id, 'position' : data.position, 'text' : data.node.text, 'operation':'move_node' })
            .fail(function () {
                data.instance.refresh();
            });
        });

        // bind to events triggered on the tree
        $('.mtf-tree').on("changed.jstree", function (e, data) {
          //console.log(data.selected);
        });

        $("#HostProfileUrl").TouchSpin({
            min: false,
            max: false,
            prefix: APP_URL
        });
       
        if($('.radio_profile_url:checked').val()){
            $('#HostProfileUrl').val($('.radio_profile_url:checked').val());
        }
        
        var maxLength = 300;
        $(".show-read-more").each(function(){
            var myStr = $(this).html();
            if($.trim(myStr).length > maxLength){
                var newStr = myStr.substring(0, maxLength);
                var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
                $(this).empty().html(newStr);
                $(this).append(' <a href="javascript:void(0);" class="read-more">read more...</a>');
                $(this).append('<span class="more-text">' + removedStr + '</span>');
            }
        });
        $(".read-more").click(function(){
            $(this).siblings(".more-text").contents().unwrap();
            $(this).remove();
        });

        $("#edit_toogle").click(function(){
            $("#toogle").toggle();
        });

        //podcast Stats
        $("#podcastStatsChart").click(function(){
            podcastStatsChart7();
            podcastStatsChart31();
            podcastStatsChartAll();

        });

        //Favorite Stats
        $("#FavoriteStatsChart").click(function(){
           favoritesStatsEpisode();
           favoritesStatsShow();
           favoritesStatsHost();
           favoritesStatsGuest();
        });

        // Intialize the selected hosts in the show edit form
        if($('#show_edit_id').length != 0){
            var show_id = $('#show_edit_id').val();
            initializeHostSelect(show_id);    
            initializeCoHostSelect(show_id);    
            initializePodcastHostSelect(show_id);
        }

        if($('#episode_edit_id').length != 0){
            var episode_id = $('#episode_edit_id').val();
            initializeHostSelect(episode_id);    
            initializeCoHostSelect(episode_id);    
            initializePodcastHostSelect(episode_id);
            initializeGuestSelect(episode_id);
        }
        if($('#episode_shows_id').length != 0){
            var show_id = $('#episode_shows_id').val();
            initializeHostSelect(show_id);    
            initializeCoHostSelect(show_id);    
            // initializePodcastHostSelect(episode_id);
        }
        if($('#sponsor_edit_id').length != 0){
            var sponsor_id = $('#sponsor_edit_id').val();
            initializeHostSelect(sponsor_id);
        }
        $('.summernote').summernote({
            height: 350,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });

        $('#ScheduleTime').timepicker({
            defaultTIme: false
        });
        $('.colorpicker-element').colorpicker({
            format: 'hex'
        });
        if($("#users_datatable").length != 0) {
            initializeUserTable();
        }
        if($("#hosts_datatable").length != 0) {
            initializeHostTable();
        }
        if($("#partners_datatable").length != 0) {
            initializePartnerTable();
        }
        if($("#shows_datatable").length != 0) {
            initializeShowTable();
        }
        if($("#banners_datatable").length != 0) {
            initializeBannerTable();
        }
        if($("#sponsors_datatable").length != 0) {
            initializeSponsorTable();
        }
        if($("#sponsor_episodes_datatable").length != 0) {
            initializeSponsorEpisodesTable();
        }
        if($("#guests_datatable").length != 0) {
            initializeGuestTable();
        }
        if($("#inventory_items_datatable").length != 0) {
            initializeInventoryItemsTable();
        }
        if($("#monthly_inventory_items_datatable").length != 0) {
            initializeMonthlyInventoryItemsTable();
        }
        if($("#partners_items_datatable").length != 0) {
            initializePartnerItemsTable();
        }
        if($("#memberships_datatable").length != 0) {
            initializeMembershipTable();
        }
        if($("#channel_datatable").length != 0) {
            initializeChannelTable();
        }
        if($("#studio_channels_datatable").length != 0) {
            initializeStudioChannelTable();
        }
        if($("#archived_episodes_datatable").length != 0) {
            initializeArchivedEpisodesTable();
        }
        if($("#upcoming_episodes_datatable").length != 0) {
            initializeUpcomingEpisodesTable();
        }
        if($("#encores_datatable").length != 0) {
            initializeEncoresTable();
        }
        if($("#user_files").length != 0) {
            initializeFileUpload($('#user_files'));
        }
        if($("#episode_id").length != 0) {
            initializePodcastStats();
        }
        if($("#content_data_datatable").length != 0) {
            initializeContentDataTable();
        }
        if($("#cms_pages_datatable").length != 0) {
            initializePageDataTable();
        }
        if($("#cms_themes_datatable").length != 0) {
            initializeThemeDataTable();
        }
        /************** Start hosts export form ****************/
        $("#hosts_export").click(function(){
           $("#hosts_export_option").toggle();
        });
        $("#guests_export").click(function(){
           $("#guests_export_option").toggle();
            var selected = $('#GuestExportFilter :selected').val();
            var optgroup = selected.closest('optgroup').attr('label');
            if(optgroup == undefined){
                $("#guest_eport_lebel").val('undefined');    
            } else{
                $("#guest_eport_lebel").val(optgroup);
            }
        });
        $('#HostExportFilter').change(function() {

            var selected = $(':selected', this);
            var optgroup = selected.closest('optgroup').attr('label');
            if(optgroup == undefined){

               //$("#HostsExportForm").attr('action', 'admin/user/hosts/export');
                $("#eport_lebel").val('undefined');        

            } else{

               $("#eport_lebel").val(optgroup);
            } 
        });        
        /************** End hosts export form ****************/

        /************** Start sponsors export form ****************/
        $("#sponsors_export").click(function(){
           $("#sponsors_export_option").toggle();
        });

        //Select Social Media 
        $(document).on('change', '.SelectSocialMediaType', function(){
            var type_ = this.value;
            var findurl= $(this).parents('tr').find('.SelectSocialMediaURL');
            if(type_ =='facebook'){
                findurl.val('https://www.facebook.com/');
            } else if(type_ =='twitter'){
                findurl.val('https://twitter.com/');
            }else if(type_ =='google'){
                 findurl.val('https://www.google.com/');
            }else if(type_ =='linkedin'){
                findurl.val('https://www.linkedin.com/');
            } else if(type_ =='itunes'){
                findurl.val('https://itunes.apple.com/');
            } else if(type_ =='youtube'){
               findurl.val('https://www.youtube.com/');
            } else if(type_ =='instagram'){
               findurl.val('https://www.instagram.com/');
            } else if(type_ =='pinterest'){
                findurl.val('https://in.pinterest.com/');
            } else if(type_ =='website'){
                findurl.val('http://www.example.com/');
            } else if(type_ =='blog'){
                findurl.val('');
            } else if(type_ =='rss'){
                findurl.val('');
            }else{
                //other
                findurl.val('');
            }
        });

        $('#SponsorExportFilter').change(function() {
  
            var selected = $(':selected', this);
            var optgroup = selected.closest('optgroup').attr('label');
            if(optgroup == undefined){

               //$("#HostsExportForm").attr('action', 'admin/user/hosts/export');
                $("#sponsor_eport_lebel").val('undefined');        

            } else{

               $("#sponsor_eport_lebel").val(optgroup);
            }      
        }); 
        /************** End sponsors export form ****************/

        if($("#trans_report_datatable").length != 0) {
            transaction_report_datatable = $('#trans_report_datatable').DataTable( {
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/user/activities",
                    "type": "POST"
                },
                "columns": [
                    { "name": "created" },
                    { "name": "username" },
                    { "name": "activity" }
                ]
            });
        }

       /* if($("#partners_datatable").length != 0) {
           
        }*/

        if($("#members_datatable").length != 0) {
            memberTable = $('#members_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/user/members",
                    "type": "POST"
                },
                "columns": [
                    { "name": "profiles.firstname" },
                    { "name": "profiles.lastname" },
                    { "name": "membership" },
                    { "name": "membership_end" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
        }
        
        if($("#items_categories_datatable").length != 0) {
            itemsCategoriesDatatable = $('#items_categories_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/inventory/categories",
                    "type": "POST"
                },
                "columns": [
                    { "name": "name" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
        }

        if($("#banner_categories_datatable").length != 0) {

            bannerCategoriesTable = $('#banner_categories_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/show/categories/banner",
                    "type": "POST",
                    "data": {type: 'banner'}
                },
                "columns": [
                    { "name": "name" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
        }

        // Adding Editor

        /*if($("#ProfileBio").length > 0 || $("#SponsorDescription").length > 0 || $("#InventoryItemDescription").length > 0 || $("#HostBio").length > 0 || $("#GuestBio").length > 0 || $("#ShowDescription").length > 0 || $('#EpisodeDescription').length > 0){*/

        if($("#ProfileBio").length > 0 || $("#SponsorDescription").length > 0 || $("#InventoryItemDescription").length > 0 || $("#HostBio").length > 0 || $("#GuestBio").length > 0 || $("#ShowDescription").length > 0 || $("#EpisodeDescription").length > 0 || $("#EpisodeSegment1").length > 0 || $("#EpisodeSegment2").length > 0 || $("#EpisodeSegment3").length > 0 || $("#EpisodeSegment4").length > 0 || $("#ContentEmailBody").length > 0 || $("#ContentDataContent").length > 0){

            if($("#ProfileBio").length > 0){
                var select_id = "textarea#ProfileBio";
            }else if($("#SponsorDescription").length > 0){
                var select_id = "textarea#SponsorDescription";
            }else if($("#InventoryItemDescription").length > 0){
                var select_id = "textarea#InventoryItemDescription";
            }else if($("#HostBio").length > 0){
                var select_id = "textarea#HostBio";
            }else if($("#GuestBio").length > 0){
                var select_id = "textarea#GuestBio";
            }else if($("#ShowDescription").length > 0){
                var select_id = "textarea#ShowDescription";
            }else if($("#EpisodeDescription").length > 0){
                var select_id = "textarea#EpisodeDescription";

            }else if($("#EpisodeInternalHostNotes").length > 0){
                var select_id = "textarea#EpisodeInternalHostNotes";
            }else if($("#EpisodeEventsPromote").length > 0){
                var select_id = "textarea#EpisodeEventsPromote";
            }else if($("#EpisodeSpecialOffers").length > 0){
                var select_id = "textarea#EpisodeSpecialOffers";
            }else if($("#EpisodeListenerInteraction").length > 0){
                var select_id = "textarea#EpisodeListenerInteraction";
            }else if($("#EpisodeSegment1").length > 0){
                var select_id = "textarea#EpisodeSegment1";
            }else if($("#EpisodeSegment2").length > 0){
                var select_id = "textarea#EpisodeSegment2";
            }else if($("#EpisodeSegment3").length > 0){
                var select_id = "textarea#EpisodeSegment3";
            }else if($("#EpisodeSegment4").length > 0){
                var select_id = "textarea#EpisodeSegment4";
            }else if($("#ContentEmailBody").length > 0){
                var select_id = "textarea#ContentEmailBody";
            }else if($("#ContentDataContent").length > 0){
                var select_id = "textarea#ContentDataContent";
            }
            
            editor = tinymce.init({
                selector: select_id,
                theme: "modern",
                height:300,
                menubar: "tools",

                plugins: [ 
                    //"image media link tinydrive code imagetools",
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor code"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | code |bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
            // editor.addButton('print', {
            //     title: 'Print',
            //     cmd: 'mcePrint',
            //     text: 'Custom Print'
            // });
        }
        // Editor on Host Page
        if($("#HostItunesDesc").length > 0){

            tinymce.init({
                selector: "textarea#HostItunesDesc",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
        // Datepicker Initialize
        //var date = new Date();
        //date.setMonth(date.getMonth() + 1, 1);
        //$('#txtStartDate').datepicker({ defaultDate: date });
        // var date = new Date();
        //date.setMonth(date.getMonth() + 1);
        // date.setDate(date.getDate()+ 29);
        var d = new Date($.now());
        var m =d.getMonth()+1;
        var y =d.getFullYear();
        var dd =day = d.getDate();
        var date =m+'-'+dd+'-'+y;
        $('#datepicker-autoclose, #BannerViewPeriodFrom, #BannerViewPeriodTo, #ScheduleStartDate, #ScheduleEndDate, #SponsorStartDate, #SponsorEndDate, .ContentDataDate').datepicker("setDate", date);
        $('#datepicker-autoclose, #BannerViewPeriodFrom, #BannerViewPeriodTo, #ScheduleStartDate, #ScheduleEndDate, #SponsorStartDate, #SponsorEndDate, .ContentDataDate, .ContentDataDateEdit').datepicker({
            //defaultDate: false,
            autoclose: true,
            todayHighlight: true,
            pickDate:true
        });

        $('#encoreDate').datepicker({
            autoclose: true,
            todayHighlight: true,
            pickDate:true            
        }).on("change", function() {
            encoresTable.destroy();
            initializeEncoresTable();
        });

        $('#filterMediaByDate').datepicker({
            autoclose: true,
            todayHighlight: true,
            pickDate:true            
        }).on("change", function() {
            $('.jFiler-items-list').html('');
            var page      = "media";
            var type      = '';
            var dimension = '';
            if($(this).data('page')){
                page = $(this).data('page');
            } 
        
            type = $('#media_type').attr('data-type');
                
            if(type == "image"){
                if($('#allMediaDimension').length > 0){
                    dimension = $('#allMediaDimension option:selected').val();
                }
            }
            //Fresh new media
            nextpage = 2;
            pageArray  = [];
            getAllMedia(type, dimension, page);
            //getAllMedia($('.tab-pane.active').attr('id'));
        });
        
        $('#datatable-responsive').DataTable();     
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
        /*var table = $('#datatable-fixed-col').DataTable({
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            }
        });*/

       

        //called when key is pressed in textbox
        $(".ProfileCellphone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $(".Cellphone").html("Digits Only").show().fadeOut("slow");
                 return false;
            }//max Length limt
            var textlen = maxLength - $(this).val().length;
            if(!(textlen >0)){
                $('.Cellphone').text("character limit 45");
            }else{
                $('.Cellphone').text(' ');
            }
                 
       });    

        //called when key is pressed in textbox
        $(".ProfilePhone").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $(".Landlinephone").html("Digits Only").show().fadeOut("slow");
                   return false;
            }
        });  

        //called when key is pressed in textbox for add theme module
        $("#CustomThemeName").keypress(function (e) {
            var kcode = e.keyCode;
            //console.log(kcode);
            if (kcode == 8 || kcode == 9 || kcode == 95 || (kcode > 47 && kcode < 58) || (kcode > 64 && kcode < 91) || (kcode > 96 && kcode < 123))
            {
                return true;
            }
            else
            {
                return false;
            }
        });
        //getallHosts();

        $('#HostExportFilter').change(function() {

            var selected = $(':selected', this);
            var optgroup = selected.closest('optgroup').attr('label');
            if(optgroup == undefined){
                $("#eport_lebel").val('undefined');    
            } else{
                $("#eport_lebel").val(optgroup);
            }
        });
        $('#GuestExportFilter').change(function() {

            var selected = $(':selected', this);
            var optgroup = selected.closest('optgroup').attr('label');
            if(optgroup == undefined){
                $("#guest_eport_lebel").val('undefined');    
            } else{
                $("#guest_eport_lebel").val(optgroup);
            }
        });

        $('#SponsorExportFilter').change(function() {

            var selected = $(':selected', this);
            var optgroup = selected.closest('optgroup').attr('label');
            if(optgroup == undefined){
                //$("#HostsExportForm").attr('action', 'admin/user/hosts/export');
                 $("#sponsor_eport_lebel").val('undefined');        

            } else{

                $("#sponsor_eport_lebel").val(optgroup);
            }           
        });

        //hosts  name
        $('.HostHost').change(function() {
            var hostname =[];
            var data = $(this).select2('data');
            $.each(data, function( index, value ) {
              hostname.push(value.text);
            });
            $("#HostHostName").val(hostname);
        });
        //guests name
        $('.ShowGuestHost').change(function() {
            var guestname =[];
            var data = $(this).select2('data');
            $.each(data, function( index, value ) {
              guestname.push(value.text);
            });
            $("#ShowGuestHostName").val(guestname);
        });
        //Cohost name
        $('#CohostCohost').change(function() {
          var cohostname =[];
            var data = $(this).select2('data');
            $.each(data, function( index, value ) {
              cohostname.push(value.text);
            });
            $("#ShowCoHostName").val(cohostname);
        }); 
        //Podcasthost name
        $('#PodcasthostPodcasthost').change(function() {
            var podcashostname =[];
            var data = $(this).select2('data');
            $.each(data, function( index, value ) {
              podcashostname.push(value.text);
            });
            $("#ShowPodcastHostName").val(podcashostname);
        }); 
        //guests name
        $('#GuestGuest').change(function() {
            var guestname =[];
            var data = $(this).select2('data');
            $.each(data, function( index, value ) {
              guestname.push(value.text);
            });
            $("#ShowGuestHostName").val(guestname);
        }); 

        $('.user-name').keyup(function() {
          var textlen = maxLength - $(this).val().length;
          $('#rchars').text(textlen);
        });

        $('#ContentCategoryName').focusout(function() {
            var ContentCategoryName = $(this).val();
            $.ajax({
                type: "GET",
                url : APP_URL+"/admin/cms/content/validation",
                data: { 'name':ContentCategoryName },
                success: function(response)
                {  
                    console.log(response);
                    if(response.status =='error'){
                        $('#ContentCategoryName').addClass('parsley-error');
                        $('.error_text').text(response.message);
                        $('.error_text').css('display','block').addClass('error-block');
                        $("#ContentCategoryBtnAdd").prop("disabled", true);
                    }
                    if(response.status =='success'){
                        $('#ContentCategoryName').removeClass('parsley-error');
                        $('.error_text').css('display','none');
                        $('#ContentCategoryBtnAdd').removeAttr('disabled');
                        $('.error_text').text('');
                    }
                    
                }
            })         
        });

        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    }); 
    //document read end
    // Add/Edit the info of the media
    function Edit_Media(id){        
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/cms/media/get_media_info",
            data: { 'id':id },
            success: function(response){ 
                $('.Media_Title_').text(' '+response.data.title);
                $('#MediaTitle_').val(response.data.title);
                $('#MediaAlt_').val(response.data.alt);
                $('#MediaCaption_').val(response.data.caption);
                $('#MediaDescription_').val(response.data.description);
                $('#MediaId_').val(response.data.id);
                $('#myModal2').modal('show');
            }
        });
    }
    // Add/Edit the extra info of the media
    function editMedia(id){
        
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/cms/media/get_media_info",
            data: { 'id':id },
            success: function(response){ 

                $('#fileUrl').val(response.data.path);
                $('#mediaFile').val(response.data.filename);
                var html ='<div><i class="fa fa-calendar" aria-hidden="true"></i><b>  Uploaded on : </b>'+response.data.created_at+'</div><div class="form-group row"><label class="col-lg-1" for"fileUrl" style="margin-top: 10px;">URL:</label><div class="col-lg-11"><input class="form-control" type="text" name="" id="fileUrl" value=""></div></div><div><b>Type : </b>'+response.data.meta_json.filetype+'</div><div><b>Size : </b>'+formatFileSize(response.data.meta_json.filesize)+'</div><div id="FileDimensions"><b>Dimensions : </b>'+response.data.width+' X '+response.data.height+'px'+'</div><div><b>Uploaded area : </b>'+response.data.uploaded_area+'</div>';
                $('.mediaInfo').html(html);

                if(response.data.type =='image'){
                    $('.Edit_Media_Image').html('<br><a href="javascript:void(0);" onclick="editMediaImageOnly()">Edit Image</a>');
                    $('.Media_Perview_Div').html('<img src="'+response.data.path+'" id="previewImg">');
                    $('#FileDimensions, .Edit_Media_Image').show();
                }else{
                    $('#FileDimensions, .Edit_Media_Image').hide();
                }
                if(response.data.type =='video'){
                   //video player
                   // $('.Media_Perview_Div').html('<video controls><source src="'+response.data.path+'" type="video/mp4"><source src="'+response.data.path+'" type="video/webm"><source src="'+response.data.path+'" type="video/ogg">Your browser does not support the video tag.</video>');
                   $('.Media_Perview_Div').html('<b>Video Url :</b> <a href="'+response.data.path+'" target="_blank">'+response.data.path+'</a><br><div><img src="'+response.data.src+'" style="border: 1px solid #d8d2d2;padding: 10px;"></div>');
                }
                if(response.data.type =='audio'){
                   //audio player
                   // $('.Media_Perview_Div').html('<audio controls><source src="'+response.data.path+'" type="audio/ogg"><source src="'+response.data.path+'" type="audio/wav"><source src="'+response.data.path+'" type="audio/mpeg">Your browser does not support the audio tag.</audio>');
                   $('.Media_Perview_Div').html('<b>Audio Url :</b> <a href="'+response.data.path+'" target="_blank">'+response.data.path+'</a><div><img src="'+response.data.src+'" style="border: 1px solid #d8d2d2;padding: 10px;"></div>');
                }
                //$('#previewImg').attr('src', response.data.path);
                $('.Media_Title_').text(' '+response.data.title);
                $('#fileUrl').val(response.data.path);
                $('#MediaTitle').val(response.data.title);
                $('#MediaAlt').val(response.data.alt);
                $('#MediaCaption').val(response.data.caption);
                $('#MediaDescription').val(response.data.description);
                $('#MediaId').val(response.data.id);
                $('#edit_media_modal').trigger('click');
            }
        });
    } 
    $(document).on('click', '#edit_media_btn', function(){
        var that = $(this);
        var form_data = $('#MediaAdminEditForm').serialize();
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/cms/media/edit",
            data: { 'data':form_data },
            success: function(response)
            {
                //console.log(response.status);
                //$('.jFiler-item-assets.jFiler-row').find('p').html('<b></b>');
                if($('#myModal').is(':visible')){
                    $('#myModal').modal('hide');
                }
                else if($('#myModal2').is(':visible')){
                    $('#myModal2').modal('hide');
                }
                $('#msg').html(response.message).fadeIn('slow');
                $('#msg').delay(5000).fadeOut('slow');
                if(response.status == "success"){
                    // console.log($('li.jFiler-item').find('[data-id="'+response.media.id+'"]').find('.jFiler-item-assets.jFiler-row').find('p').html());
                    console.log($('li.jFiler-item[data-id="'+response.media.id+'"]').find('.jFiler-item-assets.jFiler-row').find('p'));
                    $('li.jFiler-item[data-id="'+response.media.id+'"]').find('.jFiler-item-assets.jFiler-row').find('p').html('<b>'+response.media.title+'</b>'+response.media.description);
                  
                    swal({
                       title: 'Successfully Updated!', 
                       text: response.message,
                       type: 'success'
                    });
                    setTimeout(function(){
    
                        var div = $('li.jFiler-item[data-id="'+response.media.id+'"]').find('.jFiler-item-container');
                        $({alpha:1}).animate({alpha:0}, {
                            duration: 7000,
                            step: function(){
                                div.css('background-color','rgba(232, 246, 241,'+this.alpha+')');
                            }
                        });
                        
                    }, 500);
                                         
                }else if(response.status == "error"){
                    swal({
                       title: 'Warning!', 
                       text: response.message,
                       type: 'warning'
                    });
                }

                $('html, body').animate({
                      scrollTop: $('li.jFiler-item[data-id="'+response.media.id+'"]').offset().top-200,
                    },
                    500, 'linear'
                );
                /*$(that).closest('.jFiler-item-container').css('border-color', '#0f0').animate({'border-color': '#fff'}, 500);*/
            }
        });        
    });




    //Select profile url
    $(document).on('click','.radio_profile_url', function(){
        var radioValue = $(this).val();
        if(radioValue){
            $('#HostProfileUrl').val(radioValue);
        }
    });    
    //share Scheduled form validation
    $(document).on('click','#EpisodeScheduledShareIsTwitter1', function(){
        if($('#EpisodeScheduledShareIsTwitter1').is(':checked') ||$('#EpisodeScheduledShareIsfacebook1').is(':checked')){
            $('#btnSharedSheduleEpisode').removeAttr('disabled');
        }else{
            $("#btnSharedSheduleEpisode").prop("disabled", true);
        }
        $('#twitter_date, #twitter_time, #EpisodeScheduledShareTwitterDesc').addClass('required');
    }); 
    $(document).on('click','#EpisodeScheduledShareIsfacebook1', function(){
        if($('#EpisodeScheduledShareIsTwitter1').is(':checked') ||$('#EpisodeScheduledShareIsfacebook1').is(':checked')){
            $('#btnSharedSheduleEpisode').removeAttr('disabled');
        }else{
            $("#btnSharedSheduleEpisode").prop("disabled", true);
        }
        $('#facebook_date, #facebook_time, #EpisodeScheduledShareFacebookDesc').addClass('required');
    });
    // Add more periods in Membership 
    $(document).on('click','#MembershipHasManyMembershipPeriodsAdd', function(){
        var last_count = $('.mtf-dg-table tbody tr:last').data('off');
        var next_count = last_count+1;        
        var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;" data-rnd=""><td><input id="MembershipPeriod'+next_count+'Id" name="data[MembershipPeriod]['+next_count+'][id]" type="hidden" value="0"><input id="MembershipPeriod'+next_count+'Name" class="required" required name="data[MembershipPeriod]['+next_count+'][name]" type="text"></td><td><input step="1" min="1" id="MembershipPeriod'+next_count+'Days" name="data[MembershipPeriod]['+next_count+'][days]" type="number" value="30"></td><td><input id="MembershipPeriod'+next_count+'QuoteLabel" placeholder="$ 0.00" value= "0.00" pattern="/^([0-9]+|[0-9]+\.[0-9]{1,2})$/" name="data[MembershipPeriod]['+next_count+'][quote]" type="text"></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_membership_period" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
        $('.mtf-dg-table tbody tr:last').after(row);
    });

    jQuery.fn.selectpicker.Constructor.prototype.removeDiv = function () {
      
        this.$newElement.remove();
        this.$element.show(); 
      
    };
    // Add more Urls and Social Media in Sponsors
    $(document).on('click','#showAddMoreChannels', function(){
        var last_count = $('.add_multiple_channels').find('.portlet').length;
        var next_count = last_count+1; 

        $('.add_multiple_channels').find('.portlet').last().find('.show_channels').select2('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleWeek').select2('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleMonth').selectpicker('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleYear').selectpicker('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleDuration').selectpicker('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleShowsId').selectpicker('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleSchedulesChannelsId').selectpicker('destroy');
        $('.add_multiple_channels').find('.portlet').last().find('.scheduleSchedulesId').selectpicker('destroy');

        var clone = $('.add_multiple_channels').find('.portlet').last().clone();
        $(clone).find('.close_channel').css('visibility', 'visible');
        $(clone).find('.panel-collapse').attr('id', 'bg-teal-'+next_count);
        $(clone).find('.close_icon_portlet').attr('href', '#bg-teal-'+next_count);
        $(clone).find('.show_channels').attr('id', 'scheduleChannels'+next_count);
        
        //Change name of the fields
        clone.find('.weekdays').attr("name", "data[Schedule][data]["+next_count+"][dow][]");
        clone.find('.ScheduleDowSun').attr("id", "input-btn-switch-custom"+next_count);
        clone.find('.ScheduleDowSun').next('label').attr("for", "input-btn-switch-custom"+next_count);
        clone.find('.ScheduleDowMon').attr("id", "input-btn-switch-primary"+next_count);
        clone.find('.ScheduleDowMon').next('label').attr("for", "input-btn-switch-primary"+next_count);
        clone.find('.ScheduleDowTue').attr("id", "input-btn-switch-success"+next_count);
        clone.find('.ScheduleDowTue').next('label').attr("for", "input-btn-switch-success"+next_count);
        clone.find('.ScheduleDowWed').attr("id", "input-btn-switch-info"+next_count);
        clone.find('.ScheduleDowWed').next('label').attr("for", "input-btn-switch-info"+next_count);
        clone.find('.ScheduleDowThu').attr("id", "input-btn-switch-warning"+next_count);
        clone.find('.ScheduleDowThu').next('label').attr("for", "input-btn-switch-warning"+next_count);
        clone.find('.ScheduleDowFri').attr("id", "input-btn-switch-pink"+next_count);
        clone.find('.ScheduleDowFri').next('label').attr("for", "input-btn-switch-pink"+next_count);
        clone.find('.ScheduleDowSat').attr("id", "input-btn-switch-inverse"+next_count);
        clone.find('.ScheduleDowSat').next('label').attr("for", "input-btn-switch-inverse"+next_count);

        clone.find('.show_channels').attr("name", "data[Schedule][data]["+next_count+"][channels_id]");
        clone.find('.scheduleStartDate').attr("name", "data[Schedule][data]["+next_count+"][start_date]");
        clone.find('.scheduleEndDate').attr("name", "data[Schedule][data]["+next_count+"][end_date]");
        clone.find('.scheduleTime').attr("name", "data[Schedule][data]["+next_count+"][time]");
        clone.find('.scheduleDuration').attr("name", "data[Schedule][data]["+next_count+"][duration]");
        clone.find('.scheduleDuration').attr("id", "ScheduleDuration"+next_count);
        clone.find('.scheduleWeek').attr("name", "data[Schedule][data]["+next_count+"][week][]");
        clone.find('.scheduleWeek').attr("id", "ScheduleWeek"+next_count);
        clone.find('.scheduleMonth').attr("name", "data[Schedule][data]["+next_count+"][month]");
        clone.find('.scheduleMonth').attr("id", "ScheduleMonth"+next_count);
        clone.find('.scheduleYear').attr("name", "data[Schedule][data]["+next_count+"][year]");
        clone.find('.scheduleYear').attr("id", "ScheduleYear"+next_count);
        clone.find('.scheduleTypeLabel').attr("name", "data[Schedule][data]["+next_count+"][type_label]");
        clone.find('.scheduleType').attr("name", "data[Schedule][data]["+next_count+"][type]");
        clone.find('.scheduleShowsId').attr("name", "data[Schedule][data]["+next_count+"][shows_id]");
        clone.find('.scheduleShowsId').attr("id", "ScheduleShowsId"+next_count);
        clone.find('.scheduleSchedulesChannelsId').attr("name", "data[Schedule][data]["+next_count+"][schedules_channels_id]");
        clone.find('.scheduleSchedulesId').attr("name", "data[Schedule][data]["+next_count+"][schedules_id]");
        clone.find('.schedule_stream').attr("name", "data[Schedule][data]["+next_count+"][stream]");
        

        //Copy the previously selected channel value
        var select_chn_id = $('.add_multiple_channels').find('.portlet').last().find('.show_channels').attr('id');
        // Select previous selected channel
        var selectedValue = $("#"+select_chn_id+" option:selected").val();
        clone.find('.show_channels').find("option[value = '" + selectedValue + "']").attr("selected", "selected");

        //Copy the previously selected week values
        var select_week_id = $('.add_multiple_channels').find('.portlet').last().find('.scheduleWeek').attr('id');

        // Select previous selected week values
        var selectedValue = $("#"+select_week_id).val();
        clone.find('.scheduleWeek').val(selectedValue);

        //Copy the previously selected month values
        var select_month_id = $('.add_multiple_channels').find('.portlet').last().find('.scheduleMonth').attr('id');
        // Select previous selected month value
        var selectedValue = $("#"+select_month_id).val();
        clone.find('.scheduleMonth').find("option[value = '" + selectedValue + "']").attr("selected", "selected");

        //Copy the previously selected year value
        var select_year_id = $('.add_multiple_channels').find('.portlet').last().find('.scheduleYear').attr('id');
        // Select previous selected year value
        var selectedValue = $("#"+select_year_id).val();
        clone.find('.scheduleYear').find("option[value = '" + selectedValue + "']").attr("selected", "selected");

        //Copy the previously selected duration value
        var select_duration_id = $('.add_multiple_channels').find('.portlet').last().find('.scheduleDuration').attr('id');
        // Select previous selected duration value
        var selectedValue = $("#"+select_duration_id).val();
        clone.find('.scheduleDuration').find("option[value = '" + selectedValue + "']").attr("selected", "selected");       

        //Copy the previously selected show value
        var select_show_id = $('.add_multiple_channels').find('.portlet').last().find('.scheduleShowsId').attr('id');
        // Select previous selected show value
        var selectedValue = $("#"+select_show_id).val();
        clone.find('.scheduleShowsId').find("option[value = '" + selectedValue + "']").attr("selected", "selected");       

       
        clone.find('input.scheduleStartDate').datepicker();
        clone.find('input.scheduleEndDate').datepicker();
        clone.find('input.scheduleTime').timepicker();
        
        var channe_id = $(clone).find('.scheduleWeek').val();
       
        //append clone on the end
        $('.add_multiple_channels').append(clone);

        //Intialize the select boxes
        /* $('.show_channels').val(value);
        $('.show_channels').select2().trigger('change');*/

        $('.show_channels').select2();
        $('.scheduleWeek').select2();
        $('.scheduleMonth').selectpicker();
        $('.scheduleYear').selectpicker();
        $('.scheduleDuration').selectpicker();
        $('.scheduleShowsId').selectpicker();
        $('.scheduleSchedulesChannelsId').selectpicker();
        $('.scheduleSchedulesId').selectpicker();
        
    });
    
    // 
    $(document).on('click', '#importHost', function(){
        var host_id = $('#ImportHostFId').val();
        var url     = '/admin/show/guests/import_host';
        var data    = {'id': host_id};
        var host_detail = '';
        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            
            if(response.status == "success"){
                host_detail = response.data;

                $('#host_user_id').val(host_detail.host_id);

                $('#GuestTitle').val(host_detail.title).trigger('change');
                $('#GuestSufix').val(host_detail.sufix).trigger('change');
                $('#GuestFirstname').val(host_detail.firstname);
                $('#GuestLastname').val(host_detail.lastname);

                $('#GuestEmail').val(host_detail.email);
                $('#GuestPhone').val(host_detail.phone);
                $('#GuestCellphone').val(host_detail.cellphone);
                $('#GuestSkype').val(host_detail.skype);
                $('#GuestSkypePhone').val(host_detail.skype_phone);


                if(host_detail.pr_type == "pr"){
                    $('#GuestPrTypePr').attr('checked', true);
                    $('#GuestPrTypePr').prop('checked', true);
                }else if(host_detail.pr_type == "publisher"){
                    $('#GuestPrTypePublisher').attr('checked', true);
                    $('#GuestPrTypePublisher').prop('checked', true);
                }else if(host_detail.pr_type == "assistant"){
                    $('#GuestPrTypeAssistant').attr('checked', true);
                    $('#GuestPrTypeAssistant').prop('checked', true);
                }else if(host_detail.pr_type == "other"){
                    $('#GuestPrTypeOther').attr('checked', true);
                    $('#GuestPrTypeOther').prop('checked', true);
                }
                $('#GuestPrName').val(host_detail.pr_name);
                $('#GuestPrCompanyName').val(host_detail.pr_company_name);
                $('#GuestPrEmail').val(host_detail.pr_email);
                $('#GuestPrPhone').val(host_detail.pr_phone);
                $('#GuestPrCellphone').val(host_detail.pr_cellphone);
                $('#GuestPrSkype').val(host_detail.pr_skype);
                $('#GuestPrSkypePhone').val(host_detail.pr_skype_phone);

                if(host_detail.is_online == 1){
                    $('#GuestIsOnline1').attr('checked', true);
                    $('#GuestIsOnline1').prop('checked', true);
                }else if(host_detail.is_online == 0){
                    $('#GuestIsOnline0').attr('checked', true);
                    $('#GuestIsOnline0').prop('checked', true);
                }
                //$('#GuestSkype').val(host_detail.lastname);
                //$('#GuestBio').val(host_detail.bio);
               
                tinymce.activeEditor.setContent(host_detail.bio);  
                if(host_detail.videos){
                    if(host_detail.urls.length !== 0){                    
                        var next_count             = 1;
                        var checked_attr           = '';
                        var website_checked_attr   = '';
                        var blog_checked_attr      = '';
                        var rss_checked_attr       = '';
                        var twitter_checked_attr   = '';
                        var facebook_checked_attr  = '';
                        var google_checked_attr    = '';
                        var linkedin_checked_attr  = '';
                        var itunes_checked_attr    = '';
                        var youtube_checked_attr   = '';
                        var instagram_checked_attr = '';
                        var pinterest_checked_attr = '';
                        var other_checked_attr     = '';

                        $.each(host_detail.urls, function( index, value ) {
                            if(value.type == "website"){
                                website_checked_attr   = "selected=selected";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "blog"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "selected=selected";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "rss"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "selected=selected";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "twitter"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "selected=selected";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "facebook"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "selected=selected";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "google"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "selected=selected";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "linkedin"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = "selected=selected";
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";
                            }else if(value.type == "itunes"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "selected=selected";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "youtube"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "selected=selected";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "instagram"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "selected=selected";
                                pinterest_checked_attr = "";
                                other_checked_attr     = "";

                            }else if(value.type == "pinterest"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "selected=selected";
                                other_checked_attr     = "";

                            }else if(value.type == "other"){
                                website_checked_attr   = "";
                                blog_checked_attr      = "";
                                rss_checked_attr       = "";
                                twitter_checked_attr   = "";
                                facebook_checked_attr  = "";
                                google_checked_attr    = "";
                                linkedin_checked_attr  = '';
                                itunes_checked_attr    = "";
                                youtube_checked_attr   = "";
                                instagram_checked_attr = "";
                                pinterest_checked_attr = "";
                                other_checked_attr = "selected=selected";
                            }

                            var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><select id="Url'+next_count+'Type" class="form-control" name="data[Url]['+next_count+'][type]"><option value="website" '+website_checked_attr+'>website</option><option value="blog" '+blog_checked_attr+'>blog</option><option value="rss" '+rss_checked_attr+'>rss</option><option value="twitter" '+twitter_checked_attr+'>twitter</option><option value="facebook" '+facebook_checked_attr+'>facebook</option><option value="google" '+google_checked_attr+'>google</option><option value="linkedin" '+linkedin_checked_attr+'>linkedin</option><option value="itunes" '+itunes_checked_attr+'>itunes</option><option value="youtube" '+youtube_checked_attr+'>youtube</option><option value="instagram" '+instagram_checked_attr+'>instagram</option><option value="pinterest" '+pinterest_checked_attr+'>pinterest</option><option value="other" '+other_checked_attr+'>other</option></select></td><td><input step="1" min="1" id="Url'+next_count+'Url" class="form-control" placeholder="http://www.example.com" name="data[Url]['+next_count+'][url]" type="url" value="'+value.url+'"></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_host_extra_urls" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
                            $('.mtf-dg-table.url_table tbody tr:last').after(row);
                            if(next_count==1){
                                $('.url_table').find('tbody tr:first').html('');
                            }
                            next_count++;
                        });
                    }
                }
                
                if(host_detail.videos){
                    if(host_detail.videos.length !== 0){                    
                        var next_count   = 1;
                        var checked_attr = '';
                        $.each(host_detail.videos, function( index, value ) {
                            if(value.is_enabled){
                                checked_attr = "checked=checked";
                            }else{
                                checked_attr = '';
                            }
                            var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><input class="form-control" id="Video'+next_count+'Title" name="data[Video]['+next_count+'][title]" type="text" value="'+value.title+'"></td><td><textarea step="1" min="1" class="form-control" rows="2" cols="30" id="Video'+next_count+'Code" name="data[Video]['+next_count+'][code]">'+value.code+'</textarea></td><td><div class="checkbox checkbox-pink checkbox-inline"><input id="Video'+next_count+'IsEnabled'+next_count+'" name="data[Video]['+next_count+'][is_enabled]" type="checkbox" value="1" '+checked_attr+'><label for="Video'+next_count+'IsEnabled'+next_count+'" class="col-sm-12 form-control-label"></label></div></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_host_videos" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
                            $('.mtf-dg-table.video_table tbody tr:last').after(row);
                            if(next_count==1){
                                $('.video_table').find('tbody tr:first').html('');
                            }
                            next_count++;
                        });
                    }
                }             
            }     
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });
        $('#custom-modal1').find('.close').trigger('click');
    });

    // Add more Urls and Social Media in Sponsors
    $(document).on('click','#SponsorHasManySponsorSocialUrlsJsonsAdd', function(){
        var last_count = $('.mtf-dg-table tbody tr:last').data('off');
        var next_count = last_count+1;

        var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><select id="SponsorSocialUrlsJson'+next_count+'Type" class="form-control" name="data[Sponsor][social_urls_json]['+next_count+'][type]"><option value="website">website</option><option value="blog">blog</option><option value="rss">rss</option><option value="twitter">twitter</option><option value="facebook">facebook</option><option value="linkedin">linkedin</option><option value="google">google</option><option value="itunes">itunes</option><option value="youtube">youtube</option><option value="instagram">instagram</option><option value="pinterest">pinterest</option><option value="other">other</option></select></td><td><input step="1" min="1" id="SponsorSocialUrlsJson'+next_count+'Url" class="form-control" name="data[Sponsor][social_urls_json]['+next_count+'][url]" type="url" value="http://"></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_sponsor_extra_urls" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
        $('.mtf-dg-table tbody tr:last').after(row);
    });

    // Add more Urls and Social Media in Hosts
    $(document).on('click','#HostHasManyUrlsAdd', function(){
        var last_count = $('.mtf-dg-table.url_table tbody tr:last').data('off');
        var next_count = last_count+1;
       
        var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><select id="Url'+next_count+'Type" class="form-control SelectSocialMediaType" name="data[Url]['+next_count+'][type]"><option value="website">website</option><option value="blog">blog</option><option value="rss">rss</option><option value="twitter">twitter</option><option value="facebook">facebook</option><option value="google">google</option><option value="linkedin">linkedin</option><option value="itunes">itunes</option><option value="youtube">youtube</option><option value="instagram">instagram</option><option value="pinterest">pinterest</option><option value="other">other</option></select></td><td><input step="1" min="1" id="Url'+next_count+'Url" class="form-control SelectSocialMediaURL" placeholder="http://www.example.com/" name="data[Url]['+next_count+'][url]" type="url" value=""></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_host_extra_urls" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
        $('.mtf-dg-table.url_table tbody tr:last').after(row);
    });
    // Add more Urls and Social Media in guset
    $(document).on('click','#GuestHasManyUrlsAdd', function(){
        var last_count = $('.mtf-dg-table.url_table tbody tr:last').data('off');
        var next_count = last_count+1;
       
        var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><select id="Url'+next_count+'Type" class="form-control" name="data[Url]['+next_count+'][type]"><option value="website">website</option><option value="blog">blog</option><option value="rss">rss</option><option value="twitter">twitter</option><option value="facebook">facebook</option><option value="google">google</option><option value="linkedin">linkedin</option><option value="itunes">itunes</option><option value="youtube">youtube</option><option value="instagram">instagram</option><option value="pinterest">pinterest</option><option value="other">other</option></select></td><td><input step="1" min="1" id="Url'+next_count+'Url" class="form-control" placeholder="http://www.google.com" name="data[Url]['+next_count+'][url]" type="url" value=""></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_guest_extra_urls" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
        $('.mtf-dg-table.url_table tbody tr:last').after(row);
    });
    // Add more Urls and Social Media in Hosts
    $(document).on('click','#HostHasManyVideosAdd', function(){
        var last_count = $('.mtf-dg-table.video_table tbody tr:last').data('off');
        var next_count = last_count+1;
       
        var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><input class="form-control" id="Video'+next_count+'Title" name="data[Video]['+next_count+'][title]" type="text"></td><td><textarea step="1" min="1" class="form-control" rows="2" cols="30" id="Video'+next_count+'Code" name="data[Video]['+next_count+'][code]"></textarea></td><td><div class="checkbox checkbox-pink checkbox-inline"><input id="Video'+next_count+'IsEnabled'+next_count+'" name="data[Video]['+next_count+'][is_enabled]" type="checkbox" value="1" checked><label for="Video'+next_count+'IsEnabled'+next_count+'" class="col-sm-12 form-control-label"></label></div></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_host_videos" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
        $('.mtf-dg-table.video_table tbody tr:last').after(row);
    });
    // Add more Urls and Social Media in guset
    $(document).on('click','#GuestHasManyVideosAdd', function(){
        var last_count = $('.mtf-dg-table.video_table tbody tr:last').data('off');
        var next_count = last_count+1;
    
        var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><input class="form-control" id="Video'+next_count+'Title" name="data[Video]['+next_count+'][title]" type="text"></td><td><textarea step="1" min="1" class="form-control" rows="2" cols="30" id="Video'+next_count+'Code" name="data[Video]['+next_count+'][code]"></textarea></td><td><div class="checkbox checkbox-pink checkbox-inline"><input id="Video'+next_count+'IsEnabled'+next_count+'" name="data[Video]['+next_count+'][is_enabled]" type="checkbox" value="1"><label for="Video'+next_count+'IsEnabled'+next_count+'" class="col-sm-12 form-control-label"></label></div></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_guest_videos" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';
        $('.mtf-dg-table.video_table tbody tr:last').after(row);
    });
    // Add more Itunes Category and Subcategory in Hosts
    $(document).on('click','#HostHasManyItunesCategoriesAdd', function(){
        var last_count = $('.outside_itunes_cat:last').data('off');
        var next_count = last_count+1;
        var row = '<div class="outside_itunes_cat" data-off="'+next_count+'"><div class="col-sm-5"><select id="ItunesCategory'+next_count+'Category" class="form-control itunes_main_cat" data-style="btn-default" name="data[ItunesCategory]['+next_count+'][category]" tabindex="-98"><option value="Arts">Arts</option><option value="Business" selected="selected">Business</option><option value="Comedy">Comedy</option><option value="Education">Education</option><option value="Games &amp; Hobbies">Games &amp; Hobbies</option><option value="Government &amp; Organizations">Government &amp; Organizations</option><option value="Health">Health</option><option value="Kids &amp; Family">Kids &amp; Family</option><option value="Music">Music</option><option value="News &amp; Politics">News &amp; Politics</option><option value="Religion &amp; Spirituality">Religion &amp; Spirituality</option><option value="Science &amp; Medicine">Science &amp; Medicine</option><option value="Society &amp; Culture">Society &amp; Culture</option><option value="Sports &amp; Recreation">Sports &amp; Recreation</option><option value="Technology">Technology</option><option value="TV &amp; Film">TV &amp; Film</option></select></div><div class="col-sm-5"><select id="ItunesCategory'+next_count+'Subcategory" class="form-control itunes_sub_cat" data-style="btn-default" name="data[ItunesCategory]['+next_count+'][subcategory]"><option value="None">None</option><option value="Design">Design</option><option value="Fashion &amp; Beauty">Fashion &amp; Beauty</option><option value="Food">Food</option><option value="Literature">Literature</option><option value="Performing Arts">Performing Arts</option><option value="Visual Arts">Visual Arts</option></select></div><div class="col-sm-2 delete_itunes_cat" style="display: block;"><i class="typcn typcn-delete"></i></div></div>';
        $('.outside_itunes_cat:last').after(row);
    });
    // Add more Testimonials in Hosts edit profile
    $(document).on('click','#HostHasManyHostTestimonialsAdd', function(){
        var last_count = $('.host_testimonials:last').data('off');
        var next_count = last_count+1;
        var row = '<div class="form-group row host_testimonials" data-off="'+next_count+'"><input id="host_content_id'+next_count+'" name="data[HostTestimonial]['+next_count+'][id]" type="hidden" value=""><div class="col-md-5"><textarea class="form-control" rows="2" cols="30" id="HostTestimonial1Testimonial'+next_count+'" name="data[HostTestimonial]['+next_count+'][testimonial]"></textarea></div><div class="col-md-3"><input class="form-control" id="HostTestimonial1Author'+next_count+'" maxlength="255" name="data[HostTestimonial]['+next_count+'][author]" type="text" value=""></div><div class="col-md-2"><div style="float:left;"><input id="HostTestimonial1IsEnabled'+next_count+'"name="data[HostTestimonial]['+next_count+'][is_enabled]" type="checkbox" value=""></div></div><div class="col-sm-2 delete_Testimonial" style="display: block;"><i class="typcn typcn-delete"></i></div></div> </div>';
        $('.host_testimonials:last').after(row);
    });
    // Add more Testimonials in Hosts edit profile
    $(document).on('click','#EpisodeHasManyGiveawaysAdd', function(){
        var last_count = $('.episode_has_giveways:last').data('off');
        var next_count = last_count+1;
        var row = '<div class="form-group row episode_has_giveways" data-off="'+ next_count +'"><div class="col-sm-12 "><div class="form-group"><textarea name="data[Giveaway]['+ next_count +'][description]" id="Giveaway'+ next_count +'Description" class="form-control form_ui_input" row="1"  autocomplete="off"> </textarea></div></div></div>';
        $('.episode_has_giveways:last').after(row);
    });

    // Add more Custom Contents in Hosts edit profile 
    $(document).on('click','#HostHasManyHostContentsAdd', function(){
        var last_count = $('.host_contents:last').data('off');
        var next_count = last_count+1;
        var row = '<div class="col-md-6 card-box host_contents" data-off="'+next_count+'"><div class="form-group row"><div class="col-md-3 delete_Contents" style="display: block;"><i class="typcn typcn-delete"></i></div><div class="p-20"><input id="host_content_id'+next_count+'" name="data[HostContent]['+next_count+'][id]" type="hidden" value=""><input type="hidden" name="data[HostContent]['+next_count+'][offset]" value="image_'+next_count+'"><div class="col-md-12"><label for="HostContent1Title'+next_count+'" class="col-sm-4 form-control-label">Title</label> <input class="form-control" maxlength="255" id="HostContent1Title'+next_count+'" name="data[HostContent]['+next_count+'][title]" type="text" value=""></div><div class="col-md-12"><label for="HostContent1Url'+next_count+'" class="col-sm-4 form-control-label">Url</label> <input class="form-control" id="HostContent1Url'+next_count+'" name="data[HostContent]['+next_count+'][url]" type="url" value=""></div><div class="col-md-12"><label for="HostContent1Description'+next_count+'" class="col-sm-4 form-control-label">Description</label> <textarea class="form-control" maxlength="180" row="4" col="30" id="HostContent1Description'+next_count+'" name="data[HostContent]['+next_count+'][description]" cols="50" rows="10"></textarea></div><div class="col-md-12"><div class="form-group"><div class="row"><div class="col-sm-4 media_HostContent_image_1" data-off="1"><img src ="" style="width:200px;"/></a></div><div class="media_HostContent_image_'+next_count+'_outer hidden" data-off="'+next_count+'" style="display: none;"></div></div><br><span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>Only jpg/jpeg/png/gif files.</span><span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span><span>Clik on image to edit.</span><div class="mtf-buttons" style="clear:both;float:left;"><button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="HostContent" data-submodule="image_'+next_count+'" data-media_type="image" data-limit="1" data-dimension="" data-module_id="0"data-file_crop="1">Upload files</button></div></div></div><div class="col-sm-12"><div class="radio radio-info radio-inline"><input id="HostContent'+next_count+'IsEnabled1" name="data[HostContent][1][is_enabled_radio]" type="radio" value="1"><label for="HostContent'+next_count+'IsEnabled1" class="col-sm-4 form-control-label">Enabled</label></div><div class="radio radio-warning radio-inline"><input id="HostContent'+next_count+'IsEnabled0" name="data[HostContent]['+next_count+'][is_enabled_radio]" type="radio" value="0"><label for="HostContent'+next_count+'IsEnabled0" class="col-sm-4 form-control-label">Disabled</label></div></div></div></div></div>';
        $('.host_contents:last').after(row);
        /*Dropzone.autoDiscover = false;
        $("form div .dropzone").each(function(i){*/
            createDropZoneInstance($('#HostContentImage'+next_count));
       // / });
    });   
    // Edit more guest Category 
   /* $(document).on('click','#GuestHasManyCategoriesAdd', function(){
        var last_count = $('.outside_itunes_cat:last').data('off');
        var next_count = last_count+1;
        var row = '<div class="outside_itunes_cat" data-off="'+next_count+'"><div class="col-sm-5"><select id="Category'+next_count+'Category0" class="form-control guests_main_cat" data-style="btn-default" name="data[Category][Category][]" tabindex="-98"><option value=" ">Please Select</option><option value="25">Animals</option><option value="4">Business</option><option value="44">Education</option><option value="70">Energy</option><option value="21">Entertainment</option><option value="1">Health</option><option value="34">Hypnotherapy</option><option value="75">Lifestyle</option><option value="2">Men</option><option value="14">Metaphysical</option><option value="12">Political</option><option value="8">Prosperity</option><option value="30">Science</option><option value="17">Self-Help</option><option value="5" >Spirituality</option><option value="3">Women</option></select></div><div class="col-sm-5"><select id="Category'+next_count+'Category1" class="form-control guest_sub_cate" data-style="btn-default" name="data[Category][Category][]"><option value="None">None</option></select></div><div class="col-sm-2 delete_itunes_cat" style="display: block;"><i class="typcn typcn-delete"></i></div></div>';
        $('.outside_itunes_cat:last').after(row);
        $('#select_id').val('Category'+next_count+'Category0');   
       var h  = $('#Category'+last_count+'Category0').find(':selected').val();  
       $("#Category"+next_count+"Category0 option[value=" + h + "]").hide();
       console.log(h); 
    });*/

    // Add more Inventory Item Urls and Social Media in Sposnors
    $(document).on('click','#InventoryItemHasManyInventoryItemUrlsAdd', function(){
        var last_count = $('.mtf-dg-table tbody tr:last').data('off');
        if(last_count == undefined){
            last_count = 0;
            var next_count = last_count+1;
            var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><input step="1" min="1" id="InventoryItemUrl'+next_count+'Url" class="form-control" placeholder="http://" name="data[InventoryItemUrl]['+next_count+'][url]" type="url" value=""></td><td><textarea id="InventoryItemUrl'+next_count+'Description" class="form-control" rows="1" cols="50" name="data[InventoryItemUrl]['+next_count+'][description]"></textarea></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_items_extra_urls" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';

             $('.mtf-dg-table > tbody').prepend(row);
        }else{
            var next_count = last_count+1;
            var row = '<tr data-id="" data-off="'+next_count+'" style="display: table-row;"><td><input step="1" min="1" id="InventoryItemUrl'+next_count+'Url" class="form-control" placeholder="http://" name="data[InventoryItemUrl]['+next_count+'][url]" type="url" value=""></td><td><textarea id="InventoryItemUrl'+next_count+'Description" class="form-control" rows="1" cols="50" name="data[InventoryItemUrl]['+next_count+'][description]"></textarea></td><td class="actions"><div class="col-sm-6 col-md-4 col-lg-3 delete_items_extra_urls" style="display: block;"><i class="typcn typcn-delete"></i></div></td></tr>';

            $('.mtf-dg-table tbody tr:last').after(row); 
        }
        
    });

    $(document).on('click', '.delete_membership_period, .delete_sponsor_extra_urls, .delete_items_extra_urls, .delete_host_extra_urls, .delete_guest_extra_urls, .delete_host_videos, .delete_guest_videos',function(){
        $(this).closest('tr').remove();
    });

    $(document).on('click', '.delete_itunes_cat', function(){
        $(this).closest('.outside_itunes_cat').remove();
    });
    $(document).on('click', '.delete_Testimonial', function(){
        $(this).closest('.host_testimonials').remove();
    });    
    $(document).on('click', '.delete_Contents', function(){
        $(this).closest('.host_contents').remove();
    });

    // channel module
    $(document).on('click', '.delete_Channel_player', function(){
        //$(this).closest('.media_Channel_player').remove();
        //$('.open_choose_media').attr("disabled", false);
    });
    $(document).on('click', '.delete_Channel_zapbox', function(){
        //$(this).closest('.media_Channel_zapbox').remove();
        $('.open_choose_media').attr("disabled", false);
    });
    $(document).on('click', '.delete_Channel_zapboxsmall', function(){
        //$(this).closest('.media_Channel_zapboxsmall').remove();
        $('.open_choose_media').attr("disabled", false);
    });
    $(document).on('click', '.delete_Channel_widget', function(){
        //$(this).closest('.media_Channel_widget').remove();
        $('.open_choose_media').attr("disabled", false);
    });
    $(document).on('click', '.delete_Channel_cover', function(){
        //$(this).closest('.media_Channel_cover').remove();
        $('.open_choose_media').attr("disabled", false);
    });

    //banner module
    $(document).on('click', '.delete_Banner_120x300, .delete_Banner_300x250, .delete_Banner_625x258, .delete_Banner_728x90', function(){
        var main_module = $(this).data('mainmodule');
        var sub_module = $(this).data('submodule');
        $(this).closest('.media_'+main_module+'_'+sub_module).remove();
        $('.open_choose_media').attr("disabled", false);
    });

    //sponsores module
    $(document).on('click', '.delete_Sponsor_player', function(){
        var main_module = $(this).data('mainmodule');
        var sub_module = $(this).data('submodule');
        $(this).closest('.media_'+main_module+'_'+sub_module).remove();
        $('.open_choose_media').attr("disabled", false);
    });

    $(document).on('click', '#MembershipTypeWebsite-user', function(){
        $('.MembershipTypeWebsite-user').show();
        $('.MembershipTypePodcast-host').hide();
    });

    $(document).on('click', '#MembershipTypePodcast-host', function(){
        $('.MembershipTypeWebsite-user').hide();
        $('.MembershipTypePodcast-host').show();
    });
    // Show Unchecked options
    $(document).on('click', '.mdi-arrow-expand-all', function(){
        $(this).removeClass('mdi-arrow-expand-all');
        $(this).addClass('mdi-arrow-compress-all');
        $('.total_inventory_items').find('.checkbox').css('display','block');
    });

    // Show Checked options
    $(document).on('click', '.mdi-arrow-compress-all', function(){
        $(this).removeClass('mdi-arrow-compress-all');
        $(this).addClass('mdi-arrow-expand-all');
        $('.total_inventory_items').find('.md-check').not(':checked').closest('.checkbox').hide();
    });

    //
    $(document).on('click', '#HostIsFeedbackEmailNotification1', function(){
        $('#HostFeedbackNotificationEmail').show();
    });
    $(document).on('click', '#HostIsFeedbackEmailNotification0', function(){
        $('#HostFeedbackNotificationEmail').hide();
    });

    //Change the folder on host add/editp ages to save images into different folders
    $(document).on('click', '#HostFileItunes0', function(){
        $('#getCropped').data('image_for', 'itunes');
        $('#yes_crop').data('image_for', 'itunes');
        $('#yes_crop').data('module', 'Host');
    });
    $(document).on('click', '#HostPhoto0', function(){
        $('#getCropped').data('image_for', 'photo');
         $('#yes_crop').data('image_for', 'photo');
        $('#yes_crop').data('module', 'Host');
    });
    $(document).on('click', '#partner_image', function(){
        $('#yes_crop').data('image_for', 'photo');
        $('#yes_crop').data('module', 'Partner');
        //$('#yes_crop').data('limit', 1);
    });
    $(document).on('click', '#sponsor_image', function(){
        $('#yes_crop').data('image_for', 'photo');
        $('#yes_crop').data('module', 'Sponsor');
    });
    $(document).on('click', '#InventoryItemImage0', function(){
        $('#yes_crop').attr('data-image_for', 'image');
        $('#yes_crop').attr('data-module', 'InventoryItem');
    });
    $(document).on('click', '#UserPhoto0', function(){
        $('#yes_crop').attr('data-image_for', 'photo');
        $('#yes_crop').attr('data-module', 'User');
    });
    $(document).on('click', '.custom_content_image', function(){  
        $('#yes_crop').removeAttr('data-image_for');
        $('#yes_crop').removeAttr('data-module');
        var offset = $(this).data('offset');
        $('#yes_crop').attr('data-image_for', 'image_'+offset);
        $('#yes_crop').attr('data-module', 'HostContent');
        //$('#custom-modal').find('#getCropped').data('image_for','image_'+offset);
        //$('#custom-modal').find('#getCropped').data('module','HostContent');
    });
    $(document).on('click', '#Segment11', function(){
        $('#Segment1').toggle();
    });
    $(document).on('click', '#Segment22', function(){
        $('#Segment2').toggle();
    }); 
    $(document).on('click', '#Segment33', function(){
        $('#Segment3').toggle();
    }); 
    $(document).on('click', '#Segment44', function(){
        $('#Segment4').toggle();
    });     

    // Filter according to the aplhabets
    $(document).on('click', '.guest_aplha_filter input[type="radio"]', function(){
        var alphabet = $(this).val();
        
        guestTable.destroy();
        initializeGuestTable();        
    });     

    $(document).on('click', '#ScheduleTypeReplay', function(){
       
        $('.schedule_replays').show();
        $('.schedule_shows').hide();
    }); 

    $(document).on('click', '#ScheduleTypeFileOrStream, #ScheduleTypeLive', function(){
       
        $('.schedule_replays').hide();
        $('.schedule_shows').show();
    });     
    // Initialize the hosts for the show 
    function initializeHostSelect(id){
        var url = "";
        var data = {};
        if($('#sponsor_edit_id').length != 0){
            url = '/admin/advertising/sponsors/get_hosts';
            data = {'sponsor_id': id};
        }else if($('#show_edit_id').length != 0 || $('#episode_shows_id').length != 0){
            url = '/admin/show/shows/get_hosts';
            data = {'show_id': id, 'type'   : 'host'};
        }else if($('#episode_edit_id').length != 0){
            url = '/admin/show/episodes/get_hosts';
            data = {'episode_id': id, 'type'   : 'host'};
        }

        if($('.HostHost').length != 0){
            var host_suggestion = '';
            
            makeAjaxRequest('POST', url, data)
            .done(function(response) {
                
                for (i=0; i<response.length; i++) {
                    $(".HostHost").append($("<option/>", {
                        value: response[i].id,
                        text: response[i].text,
                        selected: true
                    }));
                }
                
            })
            .fail(function(xhr) {
                console.log('error callback ', xhr);
            });
        }
    }

    // Initialize the cohosts for the show 
    function initializeCoHostSelect(id){
        var url  = "";
        var data = {};
        if($('#show_edit_id').length != 0 || $('#episode_shows_id').length != 0){
            url  = '/admin/show/shows/get_hosts';
            data = {'show_id': id, 'type'   : 'cohost'};
        }
        if($('#episode_edit_id').length != 0){
            url = '/admin/show/episodes/get_hosts';
            data = {'episode_id': id, 'type'   : 'cohost'};
            
        }

        if($('.ShowCoHost').length != 0){
            var host_suggestion = '';    
            makeAjaxRequest('POST', url, data)
            .done(function(response) {
               for (i=0; i<response.length; i++) {
                    $(".ShowCoHost").append($("<option/>", {
                        value: response[i].id,
                        text: response[i].text,
                        selected: true
                    }));
                }
            })
            .fail(function(xhr) {
                console.log('error callback ', xhr);
            });
        }
    }
    // Initialize the podcast hosts for the show 
    function initializePodcastHostSelect(id){
        var host_suggestion = '';
        var data = {};
        var url  = '';
        if($('#ShowPodcastHost').length != 0){
            data = {'show_id': id, 'type'   : 'podcasthost'}; 
            url  ='/admin/show/shows/get_hosts';  
        }
        if($('#episode_edit_id').length != 0){
            url  = '/admin/show/episodes/get_hosts';
            data = {'episode_id': id, 'type'   : 'podcasthost'};
        }
        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            for (i=0; i<response.length; i++) {
                $('.ShowPodcastHost').append($("<option/>", {
                    value: response[i].id,
                    text: response[i].text,
                    selected: true
                }));
            }
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });       
    }
    // Initialize the podcast hosts for the show 
    function initializeGuestSelect(id){
        if($('.ShowGuestHost').length != 0){
            var host_suggestion = '';
            var data = {'episode_id': id,'type':'guest'};
            makeAjaxRequest('POST', '/admin/show/episodes/get_guests', data)
            .done(function(response) {
                console.log(response);
                for (i=0; i<response.length; i++) {
                    console.log(response);
                    $('.ShowGuestHost').append($("<option/>", {
                        value: response[i].id,
                        text: response[i].text,
                        selected: true
                    }));
                }
            })
            .fail(function(xhr) {
                console.log('error callback ', xhr);
            });
        }
    }
   
    $('.guest_sub_cate').select2({  maximumSelectionLength: 3 });
    $('.show_sub_cate').select2({  maximumSelectionLength: 3 });
    $('.episode_sub_cate').select2({  maximumSelectionLength: 3 });
    $('.scheduleWeek').select2();
    $('.show_channels').select2();
   
    var val = '';
        
    // Auto suggestion for Hosts
    host_suggestion = $(".HostHost").select2({

        ajax: { 
            url: APP_URL+"/admin/advertising/host/multisearch",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    text: params.term, // search term
                    type: 'host'
                };
            },
            processResults: function (response) {
                
                return {
                    results: response
                };                
            },
            cache: true ,      
        },
        triggerChange: true
    });
    
    // Auto suggestion for CoHosts
    $(".ShowCoHost").select2({
        ajax: { 
            url: APP_URL+"/admin/advertising/host/multisearch",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    text: params.term, // search term
                    type: 'cohost'
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true          
        }
    });

    // Auto suggestion for Podcast Hosts
    $(".ShowPodcastHost").select2({
        ajax: { 
            url: APP_URL+"/admin/advertising/host/multisearch",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    text: params.term, // search term
                    type: 'podcast-host'
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true          
        }
    });
    // Auto suggestion for Guest Hosts
    $(".ShowGuestHost").select2({
        ajax: { 
            url: APP_URL+"/admin/advertising/host/multisearch",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    text: params.term, // search term
                    type: 'Guest'
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true          
        }
    });   
    // Auto suggestion for Episode Type 
    $(".SponsorSponsor").select2({
            ajax: { 
                url: APP_URL+"/admin/advertising/host/multisearch",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        text: params.term, // search term
                        type: 'Sponsor'
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true          
            }
        });          
    /* Get URL paramaters */
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    // Fetch the banners according by filtering the channels.
    $("#ChannelFId").change(function() {

        var chanl_id = $(this).val();
        if($("#banners_datatable").length != 0) {
            updateQueryStringParam('channel_id',chanl_id, 'banner');
            bannerTable.destroy();
            initializeBannerTable();
        }else if($("#shows_datatable").length != 0){
            updateQueryStringParam('channel_id',chanl_id, 'show');
            showTable.destroy();
            initializeShowTable();
        }else if($("#sponsors_datatable").length != 0){
            updateQueryStringParam('channel_id',chanl_id, 'sponsor');
            sponsorTable.destroy();
            initializeSponsorTable();
        }
    });

    // Fetch the episodes acc. to the shows.
    $("#EpisodeFShowsId").change(function() {
        var show_id = $(this).val();
        updateQueryStringParam('show_id',show_id, 'encore');
        encoresTable.destroy();
        initializeEncoresTable();
    });

    // Fetch the episodes acc. to the hosts.
    $("#HostFId").change(function() {
        var host_id = $(this).val();
        updateQueryStringParam('host_id',host_id, 'encore');
        encoresTable.destroy();
        initializeEncoresTable();
    });

    // Fetch the episodes acc. to the guests.
    $("#GuestFId").change(function() {
        var guest_id = $(this).val();
        updateQueryStringParam('guest_id',guest_id, 'encore');
        encoresTable.destroy();
        initializeEncoresTable();
    });

    // Fetch the banners according by filtering the category.
    $("#BannerCategoryId").change(function() {
        var cat_id = $(this).val();
        if($("#banners_datatable").length != 0) {
            updateQueryStringParam('cat_id',cat_id, 'banner');
            bannerTable.destroy();
            initializeBannerTable();
        }
    });

    //
    $("#HostSId").change(function() {
        var host_id = $(this).val();
        updateQueryStringParam('host_id',host_id, 'sponsor');
        sponsorTable.destroy();
        initializeSponsorTable();
    });

    // Fetch the hosts by filtering the channels.
    $("#hostChannelId").change(function() {
        var chanl_id = $(this).val();
        updateQueryStringParam('channel_id',chanl_id, 'host');
        hostTable.destroy();
        initializeHostTable();
    });

    // Fetch the upcoming episodes by filtering the channels.
    $("#UpcomingFChannelsId").change(function() {
        var chanl_id = $(this).val();
        updateQueryStringParam('channel_id', chanl_id, 'upcoming');
        upcomingEpisodesTable.destroy();
        initializeUpcomingEpisodesTable();
    });

    // Fetch the upcoming episodes by filtering the sponsors.
    $("#UpcomingFSponsorsId").change(function() {
        var sponsor_id = $(this).val();
        updateQueryStringParam('sponsor_id', sponsor_id, 'upcoming');
        upcomingEpisodesTable.destroy();
        initializeUpcomingEpisodesTable();
    });

    // Fetch the upcoming episodes by filtering the sponsors.
    $("#UpcomingFHostIds").change(function() {
        var host_id = $(this).val();
        updateQueryStringParam('host_id', host_id, 'upcoming');
        upcomingEpisodesTable.destroy();
        initializeUpcomingEpisodesTable();
    });


    // Fetch the archive episodes by filtering the channels.
    $("#ArchiveFChannelsId").change(function() {
        var chanl_id = $(this).val();
        updateQueryStringParam('channel_id', chanl_id, 'archive');
        archivedEpisodesTable.destroy();
        initializeArchivedEpisodesTable();
    });

    // Fetch the archive episodes by filtering the shows.
    $("#ArchiveFShowsId").change(function() {
        var show_id = $(this).val();
        updateQueryStringParam('show_id', show_id, 'archive');
        archivedEpisodesTable.destroy();
        initializeArchivedEpisodesTable();
    });

    // Fetch the archive episodes by filtering the year.
    $("#ArchiveFYear").change(function() {
        var year = $(this).val();
        updateQueryStringParam('year',  year, 'archive');
        archivedEpisodesTable.destroy();
        initializeArchivedEpisodesTable();
    });

    // Fetch the archive episodes by filtering the months.
    $("#ArchiveFMonth").change(function() {
        var month = $(this).val();
        updateQueryStringParam('month', month, 'archive');
        archivedEpisodesTable.destroy();
        initializeArchivedEpisodesTable();
    });

    // Fetch the archive episodes by filtering the hosts.
    $("#ArchiveFHostIds").change(function() {
        var host_id = $(this).val();
        updateQueryStringParam('host_id', host_id, 'archive');
        archivedEpisodesTable.destroy();
        initializeArchivedEpisodesTable();
    });

    // Fetch the archive episodes by filtering the hosts.
    $("#ArchiveFCategoriesIds").change(function() {
        var category_id = $(this).val();
        updateQueryStringParam('category_id', category_id, 'archive');
        archivedEpisodesTable.destroy();
        initializeArchivedEpisodesTable();
    });

    //select 
    $("#ContentCategoryParentId").change(function() {
        var category_id = $(this).val();
        $('#content_type_title').text($(this).find("option:selected").text());
        var href = APP_URL+"/admin/cms/content_data/add";
        if(category_id != ''){
           
            //Reorder only for these categories
            /*var reorder_cats = ['1', '12', '14', '15', '38', '42', '45', '51', '61', '63', '70' ];
            if($.inArray( category_id, reorder_cats ) >  -1){
            
                $('#drag_drop').attr('data-drag', 1);
            }else{
                $('#drag_drop').attr('data-drag', 0);
            } */  
            href = APP_URL+"/admin/cms/content_data/add/"+category_id;
        }/*else{
            $('#drag_drop').attr('data-drag', 0);
        }*/
        
        var url = APP_URL+"/admin/cms/content_categories/edit/"+category_id;
        if(category_id != ''){
            $('#content_cate_btn').attr("href", url);
            $('#content_cate_btn').removeAttr("disabled");

            $('#content_data_url').attr('href', href);
            $('#content_data_url').removeAttr("disabled");
        }else{
             $('#content_cate_btn').attr("disabled", true);
             $('#content_data_url').attr("disabled", true);
             $('#content_cate_btn').attr("href", '#');
             $('#content_data_url').attr("href", '#');
        }

        var content_category = $(this).val();
        if($("#content_data_datatable").length != 0) {
            //Change the url acc. to the selected content type id
            var url = window.location.href;
            var queryString = url.split('/');
            var new_url = '';
            var drag = 0;
            if( content_category != '') { 
                //Reorder only for these categories
                var reorder_cats = ['1', '12', '14', '15', '38', '42', '45', '51', '61', '63', '70' ];
                if($.inArray( content_category, reorder_cats ) >  -1){
                    drag = 1;
                    $('#drag_drop').attr('data-drag', 1);
                }else{
                    drag = 0;
                    $('#drag_drop').attr('data-drag', 0);
                }   
                // If catgeory id already exists 
                if("8" in queryString){
                    new_url = url.replace("/"+queryString[8],"");
                    new_url = new_url+'/'+content_category;
                                   
                }else{

                    // Add when category id doesn't exist
                    new_url = url+'/'+content_category;
                }
            }else{
                drag = 0;
                $('#drag_drop').attr('data-drag', 0);
                // If there is not any category selected
                new_url = url.replace("/"+queryString[8],"");
            }

            //Change the state of the url
            window.history.pushState({}, document.title, new_url);

            //Fetch the content data
            //fetchContentData(content_category);
        
            contentDataTable.destroy();
            initializeContentDataTable(drag); 
        }

    });

    // get email templete by url
    $("#ContentEmailTemplate").change(function() {
        var select = $("#ContentEmailTemplate option:selected").val();
        $.ajax({
            type: 'post',
            url: APP_URL+"/admin/cms/contents/getfile",
            data: {path:select},
            success: function(response){
                $("#ContentEmailSubject").removeAttr("disabled");
                tinymce.activeEditor.setContent(response);
            }
       });

    });

    $("#ContentBtnPreview").click(function(){
           $('#action_type').val('preview');
    });
    $("#ContentBtnSend").click(function(){
           $('#action_type').val('send');
    });
    $("#ContentBtnSave").click(function(){
           $('#action_type').val('edit');
    });

    $(".ScheduleForm").click(function(e){
        var formid = '#'+$(this).data('forms-id');
        var formtype =$(this).data('form-type');
        var forr = $(this).data('form-for');
        var time =$('#'+formtype+'_'+forr+'_time').val();
        var date =$('#'+formtype+'_'+forr+'_date').val();
        
        if(time =='' || date ==''){
            if(time ==''){
                $('#'+formtype+'_'+forr+'_time').addClass('parsley-error');
            }else{
                $('#'+formtype+'_'+forr+'_time').removeClass('parsley-error');
            }

            if(date ==''){
                $('#'+formtype+'_'+forr+'_date').addClass('parsley-error');
            }else{
                $('#'+formtype+'_'+forr+'_date').removeClass('parsley-error');
            }

        }else{
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: APP_URL+"/admin/show/episodes/ajax_episode_schedule",
                data: $(formid).serialize(),
                success: function (response) {
                  //alert(response);
                },
                error:function(err){
                    //alert(err);
                }
            });
        }
    });

    //$('#ItunesCategory1Category').change(function() {
    $(document).on('change', '.itunes_main_cat', function() {
        var itune_categories = $('#itunes_cat_combo').val();
        var itune_main_category = $(this).val();
        var sub_category = "";
        $.each($.parseJSON(itune_categories), function(idx, obj) {
            if(idx == 1){
                sub_category = obj[itune_main_category];
            }
        });
        if (sub_category == null){
            //var $el = $("#ItunesCategory1Subcategory");
            var $el = $(this).parent().next().find(".itunes_sub_cat");
           
            $el.empty(); // remove old options
            $el.append($("<option></option>").attr("value", "").text("None"));
        }else{
            var $el = $(this).parent().next().find(".itunes_sub_cat");
            $el.empty(); // remove old options
            $.each(sub_category, function(key,value) {
                $el.append($("<option></option>").attr("value", value).text(key));
            });
        }
       // $('#ItunesCategory1Subcategory').selectpicker('refresh');
    });

    //guest has categories
    $(document).on('change', '.guests_main_cat', function() {
        var guest__main_category = $(this).val();
        var selected_sub_cats = $('.guest_sub_cate').find(':selected').val();
       
        var that = $(this);
        var html1 ='';
        $.ajax({
            type: 'post',
            url: APP_URL+"/admin/show/search/categories",
            data: {parent_id:guest__main_category, type:'show'},
            success: function(response){
               
                // Remove the current subcategories
                $('.guest_sub_cate').empty().trigger("change");
                $.each(response, function(key, value) { 
                    html1 +='<option value="'+value.id+'">'+value.name+'</option>';
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(value.name, value.id, true, false);
                    // Append it to the select
                    $('.guest_sub_cate').append(newOption).trigger('change');
                });
            }
        });
    });  

    //episode has categories
    $(document).on('change', '.episodes_main_cat', function() {
        var guest__main_category = $(this).val();
        var selected_sub_cats = $('.episode_sub_cate').find(':selected').val();
       
        var that = $(this);
        var html1 ='';
        $.ajax({
            type: 'post',
            url: APP_URL+"/admin/show/search/categories",
            data: {parent_id:guest__main_category, type:'show'},
            success: function(response){
               
                // Remove the current subcategories
                $('.episode_sub_cate').empty().trigger("change");
                $.each(response, function(key, value) { 
                    html1 +='<option value="'+value.id+'">'+value.name+'</option>';
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(value.name, value.id, true, false);
                    // Append it to the select
                    $('.episode_sub_cate').append(newOption).trigger('change');
                });
            }
        });
    });  

    //show has categories
    $(document).on('change', '.shows_main_cat', function() {
        var guest__main_category = $(this).val();
        var selected_sub_cats = $('.show_sub_cate').find(':selected').val();
        var that = $(this);
        var html1 ='';
        $.ajax({
            type: 'post',
            url: APP_URL+"/admin/show/search/categories",
            data: {parent_id:guest__main_category, type:'show'},
            success: function(response){
                
                // Remove the current subcategories
                $('.show_sub_cate').empty().trigger("change");
                $.each(response, function(key, value) { 
                    html1 +='<option value="'+value.id+'">'+value.name+'</option>';
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(value.name, value.id, true, false);
                    // Append it to the select
                    $('.show_sub_cate').append(newOption).trigger('change');
                });
            }
        });
    }); 

    // Select image from library
    function openMediaInfo(this_obj) {
        $('.selected-media').removeClass('selected-media');
        $(this_obj).parent().closest('.jFiler-item .jFiler-item-container').addClass('selected-media');
        if($(this_obj).parent().closest('.jFiler-item .jFiler-item-container').hasClass('selected-media')){
            $('.select_choose_media').removeAttr("disabled");
        }else{
            $('.select_choose_media').attr("disabled", true);
        }
        var that = $(this_obj);
        $('.file_name').val(that.data('name'));
        var html ='';
        html ='<div class="form-group">ATTACHMENT DETAILS<div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Name:</label></div><div class="col-sm-10"><input type="text" name="imgname" class="form-control" value="'+that.data('title')+'" readonly/></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Url:</label></div><div class="col-sm-10"><input type="text" name="imgsrc" id="" class="form-control selectedMediaSrc" data-fileName="'+that.data('filename')+'" data-src="'+that.attr('src')+'" value="'+that.data('url')+'" readonly/></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Alt:</label></div><div class="col-sm-10"><input type="text" name="imgalt" class="form-control" value="'+that.attr('alt')+'"/></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Caption:</label></div><div class="col-sm-10"><input type="text" name="imgcaption" class="form-control" name="'+that.data('caption')+'"></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Description:</label></div><div class="col-sm-10"><textarea type="text" name="imgdescription" class="form-control">'+that.data('description')+'</textarea></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Created at:</label></div><div class="col-sm-10"><input type="text" name="imgcreated_at" class="form-control" value="'+that.data('created_at')+'" readonly/></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">Updated at:</label></div><div class="col-sm-10"><input type="text" name="imgupdated_at" class="form-control" value="'+that.data('updated_at')+'" readonly/></div></div><div class="form-group row"><div class="col-sm-2"><label class="form-control-label">uploaded area:</label></div><div class="col-sm-10"><input type="text" name="imguploaded_area" class="form-control" value="'+that.data('uploaded_area')+'" readonly/><input type="hidden" name="imgid" id="" class="form-control selectedMediaId" value="'+that.attr('id')+'"></div></div></div>';
        $('#AttachmentDetail').html(html);
    }

    // Filter users according to their types
    function filterUsers(type=0){
        var data = {
            '_token' : csrf_token, 
            'type':type
        };
        //Append url with query parameter
        if(type != 0){
            updateQueryStringParam('group',type, 'users');
        }else{
            var new_url = window.location.href;
            var a = new_url.indexOf("?");
            var b =  new_url.substring(a);
            var c = new_url.replace(b,"");
            var new_url = c;
            window.history.pushState({}, document.title, new_url);
        }
        userTable.destroy();
        initializeUserTable();
    } 

    function getMedia(type, dimension='', change_type=false){

        var date = '';
        var url  = "/admin/cms/media/get_more_media";
        var title = $('#valueMediaByTitle').val();
        var data =  {'page':1, 'type':type, 'title':title, 'dimension':dimension, 'date':date, 'page_content':'popup'};
        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            if(change_type){
                $('#media_append_section').find('ul.jFiler-items-list').html('');
            }
            //$('.jFiler-items').find('ul').html('');
            $("#media_append_section").find('ul.jFiler-items-list').append(response);
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });
    }
    // Filter hosts according to their types
    function filterHosts(type=0){
        updateQueryStringParam('type', type, 'host');
        hostTable.destroy();
        initializeHostTable();
    }

    // Filter sponsors according to their activation
    function filterSponsors(type=2){
        updateQueryStringParam('is_online',type);
        sponsorTable.destroy();
        initializeSponsorTable();
    }

    // Filter media file according to the type
    function filterMedia(type){
        var url = "/admin/cms/media"
        var data = {'type':type};
        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            if(response.status == 'success'){
                $('#grid').html('');
                var html = '';
                $.each(response.data, function( index, value ) {
                   html += '<li><a href="'+value.url+'" target="_blank"><img src="'+value.url+'" alt="'+value.name+'"><p>'+value.name+'</p></a></li>';
                });
                $('#grid').html(html);
                new AnimOnScroll(document.getElementById( 'grid' ), {
                    minDuration : 0.4,
                    maxDuration : 0.7,
                    viewportFactor : 0.2
                } );
            }else{
              
            }
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });
    }

    // Explicitly save/update a url parameter using HTML5's replaceState().
    function updateQueryStringParam(key, value, page='') {

        var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

        // If the "search" string exists, then build params from it
        if (urlQueryString) {

            updateRegex = new RegExp('([\?&])' + key + '[^&]*');
            removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

           // if( typeof value == 'undefined' || value == null || value == '' ) { // Remove param if value is empty
            if(page == "host" || page == "archive" || page=="users" || page=="banner" || page=="upcoming" || page=="encore"){
                if( value == 0  || typeof value == 'undefined' || value == null) { 
                    params = urlQueryString.replace(removeRegex, "$1");
                    params = params.replace( /[&;]$/, "" );

                } else if (urlQueryString.match(updateRegex) !== null) { 
                    // If param exists already, update it
                    params = urlQueryString.replace(updateRegex, "$1" + newParam);
                } else { 
                    // Otherwise, add it to end of query string
                    params = urlQueryString + '&' + newParam;
                }
            }else{
                if( value == 2  || typeof value == 'undefined' || value == null) { 
                    params = urlQueryString.replace(removeRegex, "$1");
                    params = params.replace( /[&;]$/, "" );

                } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
                    
                    params = urlQueryString.replace(updateRegex, "$1" + newParam);

                } else { // Otherwise, add it to end of query string
                    params = urlQueryString + '&' + newParam;
                }
            }
            
            if(params == "?"){
                removeRegex = new RegExp('([\??])');
                params = params.replace( removeRegex, "" );
            }
        }
        window.history.replaceState({}, "", baseUrl + params);
    }

    // Filter banners according to their activation
    function filterBanners(type=2){
        //Append url with query parameter
        updateQueryStringParam('is_online', type);
        bannerTable.destroy();
        initializeBannerTable();  
    }

    // Filter banners according to their activation
    function filterInventoryItems(type=2, column="is_online"){
     
        //Append url with query parameter
        updateQueryStringParam(column, type);
        
        var url = "";
        if(column == "is_accepted"){

            partnerItemsTable.destroy();
            initializePartnerItemsTable();  
        }else{

            inventoryItemsTable.destroy();
            initializeInventoryItemsTable();  
        }
    }

    // Filter Memberships according to their types
    function filterMemberships(type=''){
   
        //Append url with query parameter
        updateQueryStringParam('type', type);
        membershipTable.destroy();
        initializeMembershipTable();  
    }

    // Common AJAX function
    function makeAjaxRequest(method, url, data){

        return $.ajax({
            type : method,
            url : APP_URL+url,
            data : data
        })
    }

    // Initialize users table
    function initializeUserTable(){       
        var type = 0;
        if($('#partner').is(':checked')) { 
            type = 4;
        }else if($('#admin').is(':checked')){
            type = 1;
        }else if($('#host').is(':checked')){
            type = 2;
        }else if($('#cohost').is(':checked')){
            type = 5;
        }else if($('#podcast_host').is(':checked')){
            type = 6;
        }else if($('#member').is(':checked')){
            type = 3;
        }else if($('#all').is(':checked')){
            type = 0;
        }
        userTable = $('#users_datatable').DataTable( {
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/user/users",
                "type": "POST",
                "data": {'type': type}
            },
            "columns": [
                { "name": "profiles.firstname" },
                { "name": "profiles.lastname" },
                { "name": "email" },
                { "name": "last_login" },
                { "name": "status" },
                { className: "action_btns", "name": "actions" }
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ]
        });        
    }

    function initializeHostTable(){
        var type = 0;
        var channel_id = 0;

        if($('#host').is(':checked')) { 
            type = 1;
        }else if($('#co-host').is(':checked')){
            type = 2;
        }else if($('#podcast-host').is(':checked')){
            type = 3;
        }else if($('#all').is(':checked')){
            type = 0;
        }

        channel_id = $('#hostChannelId').find(":selected").val();
        hostTable = $('#hosts_datatable').DataTable( {
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/user/hosts",
                "type": "POST",
                "data": {'type':type, 'channel_id':channel_id}
            },
            "columns": [
                { "name": "image", "bSortable":false },
                { "name": "profiles.lastname" },
                { "name": "is_online" },
                { className: "action_btns", "name": "actions" }
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ]
        });
    }

    function initializePartnerTable(){
        partnerTable = $('#partners_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/user/partners",
                    "type": "POST"
                },
                "columns": [
                    { className: "partner_photo", "name": "photo", "bSortable":false },
                    { "name": "profiles.lastname" },
                    { "name": "status" },
                    { className: "action_btns", "name": "actions", "bSortable":false }
                ]/*,
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]*/
            });
    }

    function initializeShowTable(){
            
        var channel_id = 0;
        channel_id = $('#ChannelFId').find(":selected").val();
        showTable = $('#shows_datatable').DataTable( {
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/show/shows",
                "type": "POST",
                "data": {'channel_id':channel_id}
            },
            "columns": [
                { "name": "shows.name", },
                { "name": "profiles.name" },
                { "name": "channels.name" },
                { className: "action_btns", "name": "actions" }
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ]
        });
    }

    function initializeBannerTable(){

        var type = 0;
        var channel_id  = 0;
        var category_id = 0;

        if($('#online_banners').is(':checked')) { 
            type = $('#online_banners').val();
        }else if($('#offline_banners').is(':checked')){
            type = $('#offline_banners').val();
        }else if($('#all_banners').is(':checked')){
            type = $('#all_banners').val();
        }
        channel_id  = $('#ChannelFId').find(":selected").val();
        category_id = $('#BannerCategoryId').find(":selected").val();
        bannerTable = $('#banners_datatable').DataTable( {
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/advertising/banners",
                "type": "POST",
                "data": {'is_online': type, 'channel_id':channel_id, 'category_id':category_id}
            },
            "columns": [
                { "name": "banners.title" },
                { "name": "banners.url" },
               // { "name": "sponsors.name" },
                { "name": "banners.clicks" },
                { "name": "banners.is_online" },
                { "name": "banners.view_period_from" },
                { "name": "banners.view_period_to" },
                { className: "action_btns", "name": "actions" }
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ]
        });
    }

    function initializeSponsorTable(){

        var type = 0;
        var channel_id = 0;
        var host_id    = 0;
        if($('#online_sponsors').is(':checked')) { 
            type = $('#online_sponsors').val();
        }else if($('#offline_sponsors').is(':checked')){
            type = $('#offline_sponsors').val();
        }else if($('#all_sponsors').is(':checked')){
            type = $('#all_sponsors').val();
        }
        channel_id = $('#ChannelFId').find(":selected").val();
        host_id = $('#HostSId').find(":selected").val();
        sponsorTable = $('#sponsors_datatable').DataTable( {
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/advertising/sponsors",
                "type": "POST",
                "data": {'is_online': type, 'channel_id':channel_id, 'host_id':host_id}
            },
            "columns": [
                { "name": "firstname" },
                { "name": "lastname" },
                { "name": "company" },
                { "name": "is_online" },
                { className: "td_center","name": "is_featured" },
                { "name": "start_date" },
                { "name": "end_date" },
                { className: "action_btns", "name": "actions" }
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ]
        });
    }

    function initializeSponsorEpisodesTable(){
        var sponsor_id = $('#sponsor_id').text();
        sponsorTable = $('#sponsor_episodes_datatable').DataTable( {
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/advertising/sponsors/episodes/"+sponsor_id,
                "type": "POST"
            },
            "columns": [
                { "name": "date" },
                { "name": "title" },
                { className: "action_btns", "name": "actions" }
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ]
        });
    }

    function initializeGuestTable(){

        var radioValue   = $(".guest_aplha_filter").find("input[type='radio']:checked").val();
        /*var search_field_col = 'general';
        if($('#searchGuestByFields option:selected').val() !== undefined){
            search_field_col = $('#searchGuestByFields option:selected').val();
        }*/
        
        //alert(search_field);
        guestTable = $('#guests_datatable').DataTable({
            "aaSorting" : [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/show/guests",
                "type": "POST",
                /* "data": {'is_online': type, 'channel_id':channel_id, 'host_id':host_id}*/
                data: function ( d ) {
                    d.alphabet     = radioValue; 
                    d.search_column = $('#searchGuestByFields option:selected').val();
                }
            },
            "columns": [
                { "name": "image" , "bSortable": false },
                { "name": "guests.lastname" },
                { className: "action_btns", "name": "actions", "bSortable": false }
            ]
        });

        //Add the dropdown for search acc. to the column names
        $('<div class="pull-right">' +
            '<select class="form-control input-sm" id="searchGuestByFields" onchange="searchGuestByFields(this)">'+
                '<option value="" selected="selected">Filter by</option>'+
                '<option value="firstname">First Name</option>'+
                '<option value="lastname">Last Name</option>'+
                '<option value="bio">Description</option>'+
                    '</select>' + 
            '</div>').appendTo("#guests_datatable_wrapper .dataTables_filter");

        $('#guests_datatable').find(".dataTables_filter label").addClass("pull-right");
    }

    function initializeInventoryItemsTable(){

        var type = 2;
        if($('#online_inventory_items').is(':checked')) { 
            type = $('#online_inventory_items').val();
        }else if($('#offline_inventory_items').is(':checked')){
            type = $('#offline_inventory_items').val();
        }
        
        inventoryItemsTable = $('#inventory_items_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/inventory/items",
                    "type": "POST",
                    "data": {'column': 'is_online', 'value':type}
                },
                "initComplete":function( settings, json){
                    if(json){
                        $('#total_items').text(json.recordsTotal);
                    }
                },
                "columns": [
                    { className: "item_image", "name": "image", "bSortable": false },
                    { "name": "inventory_items.name" },
                    { "name": "profiles.name" },
                    { "name": "inventory_items.created_at" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
    }
    
    function initializeMonthlyInventoryItemsTable(){

        
        var membership_id = $('#membership_id').data('id'); 

        inventoryItemsTable = $('#monthly_inventory_items_datatable').DataTable( {
                "aaSorting" : [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/inventory/months/index",
                    "type": "POST",
                    "data": {'id': membership_id}
                },
                "initComplete":function( settings, json){
                    if(json){
                        $('#total_items').text(json.recordsTotal);
                    }
                },
                "columns": [
                    { "name": "date" },
                    { "name": "InventoryMonth__items" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
    }

    function initializePartnerItemsTable(){

        var type = 0;
        if($('#new_inventory_items').is(':checked')) { 
            type = $('#new_inventory_items').val();
        }else if($('#accepted_inventory_items').is(':checked')){
            type = $('#accepted_inventory_items').val();
        }
        
        partnerItemsTable = $('#partners_items_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/inventory/items/partners",
                    "type": "POST",
                    "data": {'is_accepted': type}
                },
                "initComplete":function( settings, json){
                    if(json){
                        $('#total_items').text(json.recordsTotal);
                    }
                },
                "columns": [
                    { className: "item_image", "name": "image", "bSortable": false },
                    { "name": "inventory_items.name" },
                    { "name": "profiles.name" },
                    { "name": "inventory_items.created_at" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
    }

    // Initialize Members table
    function initializeMembershipTable(){

        var type = 2;
        if($('#website-user').is(':checked')) { 
            type = $('#website-user').val();
        }else if($('#podcast-host').is(':checked')){
            type = $('#podcast-host').val();
        }else if($('#all').is(':checked')){
            type = $('#all').val();
        }
        
        membershipTable = $('#memberships_datatable').DataTable( {
                "aaSorting": [],
                "processing": true,
                "serverSide": true,
                "stateSave" : true,
                "ajax": {
                    "url": APP_URL+"/admin/membership/memberships",
                    "type": "POST",
                    "data": {'type': type}
                },
                "columns": [
                    { "name": "name" },
                    { className: "action_btns", "name": "actions" }
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ]
            });
    }

    // Intialize channel table
    function initializeChannelTable(){
        
        channelTable = $('#channel_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/channel/channels",
                "type": "POST"
            },
            "columns": [
                { className: "channel_name_column","name": "name" },
                { "name": "description" },
                { className: "channel_action_btns", "name": "actions", orderable: false, }
            ]
        });
    }

    // Intialize Studio channels table
    function initializeStudioChannelTable(){
        
        studioChannelTable = $('#studio_channels_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,
            "ajax": {
                "url": APP_URL+"/admin/studio/channel/select",
                "type": "POST"
            },
            "columns": [
                { "name": "name" },
                { "name": "actions", orderable: false, }
            ]
        });
    }

    // Intialize Archived Episodes table
    function initializeArchivedEpisodesTable(){
        
        var year       = 0;
        var month      = 0;
        var show_id    = 0;
        var channel_id = 0;
        var host_id    = 0;
        var cat_id     = 0;
        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
        show_id        = $('#ArchiveFShowsId').find(":selected").val();
        year           = $('#ArchiveFYear').find(":selected").val();
        month          = $('#ArchiveFMonth').find(":selected").val();
        host_id        = $('#ArchiveFHostIds').find(":selected").val();
        cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();
        var column_def = '';
        
        // Role 2 - host
        if(ROLE == 2){
            column_def =  [
                { "name": "date" },
                { "name": "title" },
                { "name": "guests" },            
                { className: "action_btns", "name": "actions", orderable: false, }
            ]
        }else{
            column_def =  [
                { "name": "date" },
                { "name": "title" },
                { "name": "encores_number" },
                { "name": "media_file", orderable: false, },
                { className: "action_btns_more_width", "name": "actions", orderable: false, }
            ];
        }

        archivedEpisodesTable = $('#archived_episodes_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,           
            //"searchHighlight": true,
            "ajax": {
                "url": APP_URL+"/admin/show/episodes/archived",
                "type": "POST",
                "data": {'show_id':show_id, 'channel_id':channel_id, 'year':year, 'month':month, 'host_id':host_id, 'category_id':cat_id}
            },
            "columns": column_def
        });
       // archivedEpisodesTable.highlight($('input[type=search]').val());
    }

    // Intialize Upcoming Episodes table
    function initializeUpcomingEpisodesTable(){
        
        var sponsor_id = 0;
        var channel_id = 0;
        var host_id    = 0;
        var cat_id     = 0;
        channel_id     = $('#UpcomingFChannelsId').find(":selected").val();
        sponsor_id     = $('#UpcomingFSponsorsId').find(":selected").val();
        host_id        = $('#UpcomingFHostIds').find(":selected").val();
        //cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();
        var column_def = '';
        
        // Role 2 - host
        if(ROLE == 2){
            column_def =  [
                { "name": "date" },
                { "name": "title" },
                { "name": "guests" },            
                { "name": "Sponsor.name" },            
                { className: "action_btns", "name": "actions", orderable: false, }
            ]
        }else{
            column_def =  [
                { "name": "date" },
                { "name": "title" },
                { "name": "guests" },
                { "name": "Sponsor.name" },         
                //{ "name": "media_file", orderable: false, },
                { className: "action_btns_more_width", "name": "actions", orderable: false, }
            ];
        }

        upcomingEpisodesTable = $('#upcoming_episodes_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,           
            //"searchHighlight": true,
            "ajax": {
                "url": APP_URL+"/admin/show/episodes/upcoming",
                "type": "POST",
                "data": {'sponsor_id':sponsor_id, 'channel_id':channel_id, 'host_id':host_id}
            },
            "columns": column_def
        });
        // archivedEpisodesTable.highlight($('input[type=search]').val());
    }

    // Intialize the encore episodes table
    function initializeEncoresTable(){
        
        var guest_id      = 0;
        var show_id       = 0;
        var host_id       = 0;
        var cat_id        = 0;
        var episode_date  = '';
        show_id           = $('#EpisodeFShowsId').find(":selected").val();
        guest_id          = $('#GuestFId').find(":selected").val();
        host_id           = $('#HostFId').find(":selected").val();
        episode_date      = $('#encoreDate').val();
        var column_def    = '';        
        var selected_date = $('#selected_date').text();
        var schedule_id   = $('#schedule_id').text();
        column_def =  [
            { "name": "date" },
            { "name": "Episode__title" },
            { "name": "archives.encores_number"}
        ]/*
        }else{
            column_def =  [
                { "name": "date" },
                { "name": "title" },
                { "name": "guests" },
                //{ "name": "media_file", orderable: false, },
                { className: "action_btns_more_width", "name": "actions", orderable: false, }
            ];
        }*/
        var url = '';
        if(ROLE == 2){
            url = APP_URL+"/host/show/episodes/select_encore/"+schedule_id+"/"+selected_date;
        }else{
            url = APP_URL+"/admin/show/episodes/select_encore/"+schedule_id+"/"+selected_date;
        }
        encoresTable = $('#encores_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,           
            //"searchHighlight": true,
            "ajax": {
                "url": url,
                "type": "POST",
                "data": {'schedule_id':schedule_id, 'selected_date':selected_date, 'show_id':show_id, 'guest_id':guest_id, 'host_id':host_id, 'episode_date':episode_date}
            },
            "columns": column_def
        });
        // archivedEpisodesTable.highlight($('input[type=search]').val());
    }

    $.extend(true, $.fn.dataTable.defaults, {
        mark: true
    });
    function deleteUser(id=0, that, user_type, module=''){
        if(id != '' || id != 0){
            var url = "";
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id' : id
            };
            
            if(user_type == "host"){
                if(module != '' && module == "guest"){
                    user_type = "guest";
                    // Unlink the host from the guest module
                    url = '/admin/show/guests/guest_host_unlink';
                }else{
                    url = '/admin/user/hosts/delete';
                }                
            }else{
                url = '/admin/user/users/delete';
            }

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', url, data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The '+user_type+' has been deleted successfully.',
                               type: 'success'
                            });
                          
                            if(user_type == "partner"){
                                partnerTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }else if(user_type == "user"){
                                userTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }else if(user_type == "member"){
                                memberTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }else if(user_type == "host"){
                                hostTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }else if(user_type == "guest"){
                                guestTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }
                            
                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Partner could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }
    //Delete Membership
    function deleteMembership(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id':id
            };
           
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/membership/memberships/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Membership has been deleted successfully.',
                               type: 'success'
                            });
                            membershipTable.row( $(this_obj).parents('tr') ).remove().draw();

                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Partner could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }

    //Delete Sponsor
    function deleteSponsor(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id':id
            };
           
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/advertising/sponsors/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Sponsor has been deleted successfully.',
                               type: 'success'
                            });
                            sponsorTable.row( $(this_obj).parents('tr') ).remove().draw();

                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Sponsor could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }

    //Delete Channel
    function deleteChannel(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id':id
            };
           
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/channel/channels/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Channel has been deleted successfully.',
                               type: 'success'
                            });
                            channelTable.row( $(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Channel could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }

    //Delete Content Data
    function deleteContentData(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id':id
            };
           
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/cms/content_data/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Entry has been deleted successfully.',
                               type: 'success'
                            });
                            contentDataTable.row($(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Entry could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }

    //Delete cms pages
    function deleteCmsPages(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id':id
            };
           
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/cms/pages/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Entry has been deleted successfully.',
                               type: 'success'
                            });
                            contentDataTable.row($(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Entry could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }

    //Delete Inventory Item
    function deleteInventoryItem(id=0, that, user_type="admin"){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
                '_token' : csrf_token, 
                'id':id
            };
           
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/inventory/items/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Bonus Item has been deleted successfully.',
                               type: 'success'
                            });
                            if(user_type == "partner"){
                                partnerItemsTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }else{
                                inventoryItemsTable.row( $(this_obj).parents('tr') ).remove().draw();
                            }                   

                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Bonus Item could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
            // End delete partner //           
        }
    }

    // Delete Inventory Category
    function deleteInventoryCategory(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
               title: "Are you sure?",
               text: "You will not be able to recover this data!",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Yes, delete it!",
               closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/inventory/delete/categories', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                                title: 'Deleted!', 
                                text: 'The Item cateory has been deleted successfully.',
                                type: 'success'
                            });
                            itemsCategoriesDatatable.row( $(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                              title: 'Deleted!', 
                              text: 'The Item cateory could not be deleted.',
                              type: 'error'
                            });
                       }
                    })
                    .fail(function(xhr) {
                       console.log('error callback ', xhr);
                    });
                }
            });
        }
    }

    // Delete Show
    function deleteShow(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
               title: "Are you sure?",
               text: "You will not be able to recover this data!",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Yes, delete it!",
               closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/show/shows/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                                title: 'Deleted!', 
                                text: 'The Show has been deleted successfully.',
                                type: 'success'
                            });
                            showTable.row( $(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                              title: 'Deleted!', 
                              text: 'The Show could not be deleted.',
                              type: 'error'
                            });
                       }
                    })
                    .fail(function(xhr) {
                       console.log('error callback ', xhr);
                    });
                }
            });
        }
    }

    // Delete Guest 
    function deleteGuest(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
               title: "Are you sure?",
               text: "You will not be able to recover this data!",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Yes, delete it!",
               closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/show/guests/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                                title: 'Deleted!', 
                                text: 'The Item guests has been deleted successfully.',
                                type: 'success'
                            });
                            guestTable.row( $(this_obj).parents('tr') ).remove().draw();
                            //itemsCategoriesDatatable.row( $(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                              title: 'Deleted!', 
                              text: 'The Item guests could not be deleted.',
                              type: 'error'
                            });
                       }
                    })
                    .fail(function(xhr) {
                       console.log('error callback ', xhr);
                    });
                }
            });
           // End delete guest //           
        }
    }

    // Delete Archive 
    function deleteArchived(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
               title: "Are you sure?",
               text: "You will not be able to recover this data!",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Yes, delete it!",
               closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/show/episodes/archived/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                                title: 'Deleted!', 
                                text: 'The Item archive has been deleted successfully.',
                                type: 'success'
                            });
                            archivedEpisodesTable.row( $(this_obj).parents('tr') ).remove().draw();

                        }else{
                            swal({
                              title: 'Deleted!', 
                              text: 'The Item archive could not be deleted.',
                              type: 'error'
                            });
                       }
                    })
                    .fail(function(xhr) {
                       console.log('error callback ', xhr);
                    });
                    if($(that).data('type') =='episode'){
                        window.location.replace(APP_URL+'/admin/show/schedules/agenda');
                    }             
                }
            });         
        }
    }  

    //delete upcoming
    function deleteUpcoming(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
               title: "Are you sure?",
               text: "You will not be able to recover this data!",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Yes, delete it!",
               closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/show/episodes/archived/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                                title: 'Deleted!', 
                                text: 'The Item archive has been deleted successfully.',
                                type: 'success'
                            });
                            upcomingEpisodesTable.row( $(this_obj).parents('tr') ).remove().draw();

                        }else{
                            swal({
                              title: 'Deleted!', 
                              text: 'The Item archive could not be deleted.',
                              type: 'error'
                            });
                       }
                    })
                    .fail(function(xhr) {
                       console.log('error callback ', xhr);
                    });
                    if($(that).data('type') =='episode'){
                        window.location.replace(APP_URL+'/admin/show/schedules/agenda');
                    }             
                }
            });         
        }
    }  
    //Add Item Catgories button toggele script by parmod
    $("#AddCateBtn").click(function(){
         $('#AddItemCatgories').css("display","block");
         $('#AddCateBtn').css("display","none");
         $('#AddCateCancel').css("display","block");
    }); 

    $("#AddCateCancel").click(function(){
         $('#AddItemCatgories').css("display","none");
         $('#AddCateBtn').css("display","block");
         $('#AddCateCancel').css("display","none");
         $("#alert_ItemCatName").css("display","none");
    });

    $("#ItemCatSubmit").click(function(){

        var ItemName = $('#InventoryCategoryName').val();

        $.ajax({ 

            type:"POST",
            url: APP_URL+"/admin/inventory/add/categories",
            data:{
             _token : csrf_token,
              name: ItemName,
            },  

            success: function (response) {
                if(response.validation_error == true){
                    $("#alert_ItemCatName").css("display","block").html(response.message);
                    $("#InventoryCategoryName").css("border-color","#fab8bd");

                    $("#InventoryCategoryName").click(function(){

                        $("#alert_ItemCatName").css("display","none");  
                        $("#InventoryCategoryName").css("border-color","");
                    });
                }
                if(response.seccess == true){
                    itemsCategoriesDatatable.ajax.reload();
                    $('#AddItemCatgories').css("display","none");
                    $('#AddCateBtn').css("display","block");
                    $('#AddCateCancel').css("display","none");                            
                    $("#success_alert_ItemCatName").css("display","block").html(response.message);
                    $("#addItemform")[0].reset();
                    $(document).click(function(){
                        $("#success_alert_ItemCatName").css("display","none");  
                    });
                }
            }
        });
    }); 

    $("#MediaFilesUrl").click(function(){
        var type      = $("input[name='filterMedia']:checked").data('type');
        var dimension = $("#mediaDimension option:selected").val();
        if (!$('.fixed-left.widescreen').hasClass("modal-open")) {
            $('.fixed-left.widescreen').addClass('modal-open');
        }
        getMedia(type, dimension, true);
    });

    //On Reschedule area on upcoming show
    $('.on_reschedule').click(function(){
        $('.reschedule_outer').slideToggle();
    });
    
    // Show field according to the type
    function BonusItemType(type, that){
       
        if(type == "file"){
            $('.type_file').show();
            $('.type_link').hide();
            $('.type_code').hide();
            //createDropZoneInstanceForFile(that);
        }else if(type == "link"){
            $('.type_file').hide();
            $('.type_link').show();
            $('.type_code').hide();
        }else if(type == "code"){
            $('.type_file').hide();
            $('.type_link').hide();
            $('.type_code').show();
        }
       /* $(this).removeClass('mdi-arrow-compress-all');
        $(this).addClass('mdi-arrow-expand-all');
        $('.total_inventory_items').find('.md-check').not(':checked').closest('.checkbox').hide();*/
    }
    
    //Add Banner Categories
    $("#AddBannerCateBtn").click(function(){
        $('#AddBannerCatgories').css("display","block");
        $('#AddBannerCateBtn').css("display","none");
        $('#CancelBannerCateBtn').css("display","block");
    }); 

    $("#CancelBannerCateBtn").click(function(){
        $('#AddBannerCatgories').css("display","none");
        $('#AddBannerCateBtn').css("display","block");
        $('#CancelBannerCateBtn').css("display","none");
        $("#alert_BannerCatName").css("display","none");
    });

    $("#BannerCatSubmit").click(function(){
        var ItemName = $('#BannerCategoryName').val();
        $.ajax({ 
            type:"POST",
            url: APP_URL+"/admin/advertising/add/banner_categories",
            data:{
                _token : csrf_token,
                name: ItemName,
            },  
            success: function (response) {
                if(response.validation_error == true){
                    $("#alert_BannerCatName").css("display","block").html(response.message);
                    $("#BannerCategoryName").css("border-color","#fab8bd");
                    $("#BannerCategoryName").click(function(){
                       $("#alert_BannerCatName").css("display","none");  
                       $("#BannerCategoryName").css("border-color","");
                    });
                }
                if(response.seccess == true){

                    bannerCategoriesTable.ajax.reload();

                    $('#AddBannerCatgories').css("display","none");
                    $('#AddBannerCateBtn').css("display","block");
                    $('#CancelBannerCateBtn').css("display","none");                            
                    $("#success_alert_BannerCatName").css("display","block").html(response.message);
                    $("#addBannerCateform")[0].reset();
                    $(document).click(function(){
                       $("#success_alert_BannerCatName").css("display","none");  
                    });
                }   
            }
        });
    }); 

    // Delete Banner category
    function deleteBannerCate(id=0, that){
        if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/advertising/banner_categories/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The Banner Category has been deleted successfully.',
                               type: 'success'
                            });
                            bannerCategoriesTable.row( $(this_obj).parents('tr') ).remove().draw();
                        }else{
                            swal({
                               title: 'Deleted!', 
                               text: 'The Banner Category could not be deleted.',
                               type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });        
        }
    }
    // Delete Banners
    function deleteBanner(id=0, that){
       if(id != '' || id != 0){
            var this_obj = that;            
            var data = {
               '_token' : csrf_token, 
               'id':id
            };
          
            swal({
               title: "Are you sure?",
               text: "You will not be able to recover this data!",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Yes, delete it!",
               closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', '/admin/advertising/banners/delete', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text: 'The banners has been deleted successfully.',
                               type: 'success'
                            });
                            bannerTable.row( $(this_obj).parents('tr') ).remove().draw();

                        }else{
                            swal({
                                title: 'Deleted!', 
                                text: 'The banners could not be deleted.',
                                type: 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                       console.log('error callback ', xhr);
                    });
                }
            });        
        }
    }
   
    // Distributed series on view archived page
    function initializePodcastStats(){
        var episode_id   = $('#episode_id').val();
        var year  = "";
        var month = "";
        year  = $('#StatMonth').val();
        month = $('#StatYear').val();
        podcastStats(episode_id, year, month);
    }

    // Distributed series on view archived page
    function initializeContentDataTable(drag=2){
        var content_category = $('#ContentCategoryParentId').val();        
        // Check categories for the ordering
        var make_reorder = false;
        if(drag !=2 ){
            if(drag == 1){
                make_reorder = "{update: false, dataSrc: 'DT_RowId'}";
            }
        }else{
            if($('#drag_drop').data('drag') == 1){
                make_reorder = "{update: false, dataSrc: 'DT_RowId'}";
            }
        }
       
       
        //alert($('#drag_drop').data('drag'));
        //Fetch the content data
        contentDataTable = $('#content_data_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,           
            //"searchHighlight": true,
            "ajax": {
                "url": APP_URL+"/admin/cms/content_data/index",
                "type": "POST",
                "data": {'content_cat_id':content_category}
            },
             "columns": [

                /* { data: null, render: function ( data, type, row ) {
                  
                    // Combine the first and last names into a single table field
                        return data[0];
                    } 
                },*/
                { "name": "title", className: 'reorder' },
                { "name": "created_at" },
                { "name": "updated_at" },
                { className: "action_btns", "name": "actions", orderable: false, }
            ],
            rowReorder:make_reorder,
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                // Assign offset to the rows
                $(nRow).attr('data-offset', aData['DT_RowClass'].replace("off_", ""));
            }
        });
        
        var move_offset = move_id ='';
        contentDataTable.on( 'pre-row-reorder', function ( e, node, index ) {
            move_offset = node.node.dataset.offset;
            move_id     = node.node.id;
        });
        contentDataTable.on( 'row-reorder.dt', function ( e, diff, edit ) {
          
            var category_id     = $('#ContentCategoryParentId option:selected').val();
            next_id             = $(e.target.rows[diff.length]).next('tr').attr('id');
            next_element_offset = $(e.target.rows[diff.length]).next('tr').data('offset');
            $.ajax({
                url: APP_URL+'/admin/cms/content_data/reorder',
                type: "POST",
                data:  {'ajax': 1, 'movedElementId': move_id, 'moveElementOffset': move_offset, 'movedElementNextId':next_id, 'movedElementNextOffset':next_element_offset, 'categoryId':category_id },
                beforeSend: function(){
                    //$("#"+type).find('#media_loading_image_gif').show();
                },
                complete: function(sed){
                    console.log('complete');
                    console.log(sed);
                },
                success: function(data){
                    console.log('success');
                    console.log(data);
                },              
                error: function(err){
                    console.log(err);
                }                 
            }); 
        }); 
        return false;   
    }

    // Distributed series on view archived page
    function initializePageDataTable(drag=2){
        /*var content_category = $('#ContentCategoryParentId').val();*/
        // Check categories for the ordering
        var make_reorder = false;
        if(drag !=2 ){
            if(drag == 1){
                make_reorder = "{update: false, dataSrc: 'DT_RowId'}";
            }
        }else{
            if($('#drag_drop').data('drag') == 1){
                make_reorder = "{update: false, dataSrc: 'DT_RowId'}";
            }
        }
       
       
        //alert($('#drag_drop').data('drag'));
        //Fetch the content data
        contentDataTable = $('#cms_pages_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,           
            //"searchHighlight": true,
            "ajax": {
                "url": APP_URL+"/admin/cms/pages/index",
                "type": "POST",
                //"data": {'content_cat_id':content_category}
            },
             "columns": [

                /* { data: null, render: function ( data, type, row ) {
                  
                    // Combine the first and last names into a single table field
                        return data[0];
                    } 
                },*/
                { "name": "title", className: 'reorder' },
                { "name": "created_at" },
                { "name": "updated_at" },
                { className: "action_btns", "name": "actions", orderable: false, }
            ],
            rowReorder:make_reorder,
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                // Assign offset to the rows
                $(nRow).attr('data-offset', aData['DT_RowClass'].replace("off_", ""));
            }
        });
        
        var move_offset = move_id ='';
        contentDataTable.on( 'pre-row-reorder', function ( e, node, index ) {
            move_offset = node.node.dataset.offset;
            move_id     = node.node.id;
        });
        contentDataTable.on( 'row-reorder.dt', function ( e, diff, edit ) {
          
            var category_id     = $('#ContentCategoryParentId option:selected').val();
            next_id             = $(e.target.rows[diff.length]).next('tr').attr('id');
            next_element_offset = $(e.target.rows[diff.length]).next('tr').data('offset');
            $.ajax({
                url: APP_URL+'/admin/cms/content_data/reorder',
                type: "POST",
                data:  {'ajax': 1, 'movedElementId': move_id, 'moveElementOffset': move_offset, 'movedElementNextId':next_id, 'movedElementNextOffset':next_element_offset, 'categoryId':category_id },
                beforeSend: function(){
                    //$("#"+type).find('#media_loading_image_gif').show();
                },
                complete: function(sed){
                    console.log('complete');
                    console.log(sed);
                },
                success: function(data){
                    console.log('success');
                    console.log(data);
                },              
                error: function(err){
                    console.log(err);
                }                 
            }); 
        }); 
        return false;   
    }


    // Distributed series on view Theme
    function initializeThemeDataTable(drag=2){

        var make_reorder = false;
        if(drag !=2 ){
            if(drag == 1){
                make_reorder = "{update: false, dataSrc: 'DT_RowId'}";
            }
        }else{
            if($('#drag_drop').data('drag') == 1){
                make_reorder = "{update: false, dataSrc: 'DT_RowId'}";
            }
        }
       
        contentDataTable = $('#cms_themes_datatable').DataTable({
            "aaSorting": [],
            "processing": true,
            "serverSide": true,
            "stateSave" : true,           

            "ajax": {
                "url": APP_URL+"/admin/cms/themes/index",
                "type": "POST",
            },
             "columns": [

                { "name": "theme_name", className: 'reorder' },
                { "name": "channel_name" },
                { className: "action_btns", "name": "actions", orderable: false, }
            ],
            rowReorder:make_reorder,
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                // Assign offset to the rows
                $(nRow).attr('data-offset', aData['DT_RowClass'].replace("off_", ""));
            }
        }); 
        return false;   
    }




    // dashborad: Last 7 days most popular podcasts chart 
    function podcastStatsChart7(){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/podcastStats",
            data: {type:'last_week'},
            beforeSend: function(){
                $(".loader").show();
            },
            complete: function(){
                $(".loader").hide();
           },
            success: function(response){  
            $.each(response.episode, function(key,value) {
                list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="admin/show/episodes/view_archived/'+value.id+'">'+value.chart_series+' |'+value.title+' ('+value.show_date+')</a></li>';               
            });
            $('#chart_view_7').html(list);              
            new Chartist.Bar('#podcastStatsChart7', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                }, {
                    stackBars: true,
                    plugins: [
                        Chartist.plugins.tooltip()
                    ],
                    showGridBackground: true,
                    //seriesBarDistance:10,
                    low:30,/*
                    high:200,*/
                    axisY: {
                        // On the y-axis start means left and end means right
                        //position: 'start',
                        //onlyInteger: true,
                        //type: Chartist.FixedScaleAxis,
                        type: Chartist.AutoScaleAxis,
                    },
                    height:'300px'
                }).on('draw', function(data) {
                    if(data.type === 'bar') {
                        var col = data.series[i].color;
                        data.element.attr({
                          style:'stroke-width: 25px;stroke:'+col+';',
                        });
                        i++;
                    }
                }); 
            }
        });
    }
    //dashborad: Last 31 days most popular podcasts chart
    function podcastStatsChart31(){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/podcastStats",
            data: {type:'last_month'},
            success: function(response){  
                    $.each(response.episode, function(key,value) {
                        list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="admin/show/episodes/view_archived/'+value.id+'">'+value.chart_series+' |'+value.title+' ('+TimeFormat(value.date,value.time)+')</a></li>';                                      
                    });
                    $('#chart_view_31').html(list);
                    new Chartist.Bar('#podcastStatsChart31', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                  }, {
                    stackBars: true,
                    plugins: [
                        Chartist.plugins.tooltip()
                    ],
                    showGridBackground: true,
                    low:30,
                    axisY: {
                        // On the y-axis start means left and end means right
                        //position: 'start',
                        //onlyInteger: true,
                        //type: Chartist.FixedScaleAxis,
                        type: Chartist.AutoScaleAxis,
                    },
                    height:'300px'
                }
                    ).on('draw', function(data) {
                      if(data.type === 'bar') {
                        var col = data.series[i].color;
                        data.element.attr({
                          style:'stroke-width: 25px;stroke:'+col+';',
                        });
                        i++;
                      }
                    }); 
                      
            }
        });
    }
        //dashborad: Last total days most popular podcasts chart
    function podcastStatsChartAll(){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/podcastStats",
            data: {type:'total'},
            success: function(response){ 
                    $.each(response.episode, function(key,value) {
                    // list +='<li><a href="admin/show/episodes/view_archived/'+value.id+'">'+value.chart_series+' |'+value.title+' ('+TimeFormat(value.date,value.time)+')</a></li>';
                        list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="admin/show/episodes/view_archived/'+value.id+'">'+value.chart_series+' |'+value.title+' ('+TimeFormat(value.date,value.time)+')</a></li>';
                       
                    });
                    $('#chart_view_all').html(list);   
                    new Chartist.Bar('#podcastStatsChartAll', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                }, {
                    stackBars: true,
                    plugins: [
                        Chartist.plugins.tooltip()
                    ],
                    showGridBackground: true,
                    low:30,
                    axisY: {
                        // On the y-axis start means left and end means right
                        //position: 'start',
                        //onlyInteger: true,
                        //type: Chartist.FixedScaleAxis,
                        type: Chartist.AutoScaleAxis,
                    },
                    height:'300px'
                }
                ).on('draw', function(data) {
                    if(data.type === 'bar') {
                        var col = data.series[i].color;
                        data.element.attr({
                          style:'stroke-width: 25px;stroke:'+col+';',
                        });
                        i++;
                    }
                });                      
            }
        });
    } 

    // dashborad: Most favorited episodes chart 
    function favoritesStatsEpisode(start='', end=''){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/favoritesStats",
            data: {type:'episode', 'startdate':start, 'enddate':end},
            success: function(response){  
            $.each(response.episode, function(key,value) {
                list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="admin/show/episodes/view_archived/'+value.id+'">'+value.episode_favorites+' |'+value.name+' ('+value.episode_date+')</a></li>';                
            });
            $('#favoritesEpisodeList').html(list);              
                new Chartist.Bar('#favoritesStatsEpisode', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                },
                {
                    plugins: [
                        Chartist.plugins.tooltip()
                    ],
                    stackBars: true,
                    showGridBackground: true,
                    showGridBackground: true,
                }
                ).on('draw', function(data) {
                    if(data.type === 'bar') {
                        var col = data.series[i].color;
                        data.element.attr({
                          style:'stroke-width: 25px;stroke:'+col+';',
                        });
                        i++;
                    }
                });                     
            }
        });
    } 

    // dashborad: Most favorited show chart 
    function favoritesStatsShow(start='', end=''){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/favoritesStats",
            data: {type:'show', 'startdate':start, 'enddate':end},
            success: function(response){  
            $.each(response.episode, function(key,value) {
                list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="'+APP_URL+'/admin/show/shows/edit/'+value.id+'">'+value.favorites+' |'+value.name+'</a></li>';                
            });
            $('#favoritesShowList').html(list);              
                new Chartist.Bar('#favoritesStatsShow', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                },
                {
                    plugins: [
                        Chartist.plugins.tooltip()
                    ],
                    stackBars: true,
                    showGridBackground: true,
                    showGridBackground: true,
                }
                ).on('draw', function(data) {
                  if(data.type === 'bar') {
                    var col = data.series[i].color;
                    data.element.attr({
                      style:'stroke-width: 25px;stroke:'+col+';',
                    });
                    i++;
                  }
                });                      
            }
        });
    } 

    // dashborad: Most favorited Hosts chart 
    function favoritesStatsHost(start='', end=''){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/favoritesStats",
            data: {type:'host', 'startdate':start, 'enddate':end},
            success: function(response){  
            $.each(response.episode, function(key,value) {
                list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="'+APP_URL+'/admin/user/hosts/edit/'+value.id+'">'+value.favorites+' |'+value.name+'</a></li>';                
            });
            $('#favoritesHostList').html(list);              
                new Chartist.Bar('#favoritesStatsHost', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                },
                {
                    plugins: [
                        Chartist.plugins.tooltip()
                    ],
                    stackBars: true,
                    showGridBackground: true,
                    showGridBackground: true,
                }
                ).on('draw', function(data) {
                    if(data.type === 'bar') {
                        var col = data.series[i].color;
                        data.element.attr({
                          style:'stroke-width: 25px;stroke:'+col+';',
                        });
                        i++;
                    }
                });                      
            }
        });
    } 
    // dashborad: Most favorited Guests chart 
    function favoritesStatsGuest(start='', end=''){
        var list = '';
        var i=0;
        $.ajax({
            type: 'get',
            url: APP_URL+"/admin/favoritesStats",
            data: {type:'guest', 'startdate':start, 'enddate':end},
            success: function(response){  
            $.each(response.episode, function(key,value) {
                list +='<li><i class="fa fa-square" aria-hidden="true" style="color:'+value.bar_color +'; margin:5px;"></i><a href="'+APP_URL+'/admin/show/guests/edit/'+value.id+'">'+value.favorites+' |'+value.name+'</a></li>';           
            });
            $('#favoritesGuestList').html(list);              
                new Chartist.Bar('#favoritesStatsGuest', {
                    labels: [],
                    series: [ response.chart_series,
                    ]
                },
                {   
                    axisY: {
                        low: 0,
                        high: 20,
                    },
                    plugins: [
                            Chartist.plugins.tooltip()
                    ],
                    stackBars: true,
                    showGridBackground: true,
                }
                    ).on('draw', function(data) {
                      if(data.type === 'bar') {
                        var col = data.series[i].color;
                        data.element.attr({
                          style:'stroke-width: 25px;stroke:'+col+';',
                        });
                        i++;
                      }
                    }); 
                      
            }
        });
    } 

    //return time formate(Thu Sep 03 2015 10:00:00 AM)
   function TimeFormat(date,time){
        let date_ = new Date(date);
        let time_ = new Date(date_.toDateString()+' '+time);
        var get = date_.toDateString()+' '+time_.toLocaleTimeString();
        return get;
    }  

    // Refresh the canvas of the podcast stat
    function refreshPodcastStat(chart_labels, chart_series){

        /*new Chartist.Bar('#distributed-series', {
          labels: ['Q1', 'Q2', 'Q3', 'Q4'],
          series: [
            [800000, 1200000, 1400000, 1300000],
            [200000, 400000, 500000, 300000],
            [160000, 290000, 410000, 600000]
          ]
        }, {
          stackBars: true,
          axisY: {
            labelInterpolationFnc: function(value) {
              return (value / 1000) + 'k';
            }
          },
          plugins: [
            Chartist.plugins.tooltip()
          ]
        }).on('draw', function(data) {
          if(data.type === 'bar') {
            data.element.attr({
              style: 'stroke-width: 30px'
            });
          }
        });*/


        //Overlapping bars on mobile

        var data = {
          labels: chart_labels,
          series: chart_series
        };

        var options = {
          seriesBarDistance: 1,
          distributeSeries: true,
          plugins: [
                        Chartist.plugins.tooltip()
                    ]
        };
        new Chartist.Bar('#distributed-series', data, options);
    }

    // Fetch the podcast stat according to the month, year
    function podcastStats(episode_id, getyear='', getmonth=''){
       
        var chart_labels = [];
        var chart_series = [];
    
        $.ajax({
            type: 'post',
            url: APP_URL+"/admin/show/episodes/podcastStats",
            data: {'episode_id':episode_id, 'year':getyear, 'month':getmonth},
            dataType:'json', 
            success: function(response){                
                if(response.status == "success"){                   
                    $.each(response.data, function(index, value) {             
                        chart_labels.push(index);
                        chart_series.push(value);
                    });
                    
                    //refreshPodcastStat(chart_labels, chart_series);
                    $("#distributed-series").empty();
                    $.ChartJs.destroys();
                    $.ChartJs.init(chart_labels, chart_series);
                    
                }else{
                    //refreshPodcastStat(chart_labels, chart_series);
                    $("#distributed-series").empty();
                    //$.ChartJs.destroys();
                    //$.ChartJs.init(chart_labels, chart_series);
                }
            }
        });
    }

    // Preview src image
    function PreviewImg(input,placeToInsertImagePreview){
        
       if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    // Delete media linking
    function mediaLinkDelete(id, this_obj, delDiv = '.col-sm-4'){
        var url  = "/admin/cms/media/delete_media_link";
        var data = {'id': id};
        var that = this_obj;     
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) {
                makeAjaxRequest('POST', url, data)
                .done(function(response) {
                    if(response.status == 'success'){
                        swal({
                           title: 'Deleted!', 
                           text : 'Media has been deleted successfully.',
                           type : 'success'
                        });
                        //$(that).closest('.col-sm-4').remove();
                        $(that).closest(delDiv).remove();
                        
                    }else{
                        swal({
                           title: 'Deleted!', 
                           text : 'Media could not be deleted.',
                           type : 'error'
                        });
                    }
                })
                .fail(function(xhr) {
                    console.log('error callback ', xhr);
                });
            }else{
            }
        });
    }
    // Add media linking
    function addMediaLink(module_id, main_module, sub_module, media_id, operation="add", existing_media_id=''){
       
        //var sub_module = $('.sub_module').val();
        //var media_id   =  $('.tab-pane.active').find('.selectedMediaId').val();        
        var id ='';
        if(operation == 'edit'){
            id = existing_media_id;
            $.ajax({
                type: 'POST',
                'async': false,
                url: APP_URL+"/admin/cms/media/edit_media_link",
                data: {'media_id':media_id, 'id':id},
                success: function(response){                
                    if(response.status == "success"){              
                        id = response.id;
                    }else{
                       
                    }
                }
            });
        }else{
            $.ajax({
                type: 'post',
                'async': false,
                url: APP_URL+"/admin/cms/media/add_media_link",
                data: {'media_id':media_id,'module_id':module_id,'main_module':main_module,'sub_module':sub_module,},
                success: function(response){                
                    if(response.status == "success"){              
                        id = response.id;
                    }else{
                       
                    }
                }
            });
        }
        
        return(id);
    }

    // Save rescheduling for the upcoming episodes
    $('.save_reschedule').click(function(){
        var schedule_data = $('#RescheduleEpisode').val();
        console.log(schedule_data);
        var url = "/admin/show/episodes/reschedule_upcoming";
        var data = {'id': $('#episode_edit_page').data('episode_id'), 'data':schedule_data};
        makeAjaxRequest('POST', url, data)
            .done(function(response) {
                if(response.status == 'success'){
                    $('.episode_date div').text(response.date);

                    swal({
                       title: 'Rescheduled!', 
                       text : 'Episode has been scheduled successfully.',
                       type : 'success'
                    });                            
                    
                }else{
                    swal({
                       title: 'Error!', 
                       text : 'There is some problem while scheduling the episode. Please try again.',
                       type : 'error'
                    });
                }
            })
            .fail(function(xhr) {
                console.log('error callback ', xhr);
            });
    });

    //Fetch podcast by selecting the year and month
    $(document).on('change', '#StatMonth, #StatYear', function(){
        var episode_id = $('#episode_id').val();
        var year  = "";
        var month = "";
        if($(this).attr("name") == "data[Stat][year]"){
            year  = $(this).val();
            month = $('#StatMonth').val();
        }else if($(this).attr("name") == "data[Stat][month]"){
            month = $(this).val();
            year  = $('#StatYear').val(); 
        }       
        podcastStats(episode_id, year, month);
    });

    //Fetch media by dimensions
    $(document).on('change', '#StatMonth', function(){

    });

    // Toggle shows for override the show in the episodes
    function toggleShows(){
        $("#override_with_shows").toggle();
    } 
    $(document).on('change', '#OverrideShowsId', function(){
        $('#ShowNameView').val($(this).find('option:selected').text());
        $('#episode_shows_id').val($(this).find('option:selected').val());
        
        var encore_btn = $('#btnAddEncore').attr('href');       
        var newurl = updateQueryStringParameter(encore_btn, 'show_id', $(this).find('option:selected').val());
        $('#btnAddEncore').attr('href', '');
        $('#btnAddEncore').attr('href', newurl);
    });
    
    $(document).on('change', '#mediaDimension', function(){
        var dimension  = $(this).val();
        var radioValue = $("input[name='filterMedia']:checked").val();

        var type ='';
        if(radioValue =='2'){
            type ='video';
        }else if(radioValue =='1'){
            type ='audio';
        }else{
            type ='image';
        }
        //Fresh new media
        nextpage = 2;
        pageArray  = [];
        getMedia(type, dimension, true);
    });

    $(document).on('change', '#allMediaDimension', function(){
        var dimension  = $(this).val();
        var type = 'image';
        getAllMedia(type, dimension);
    });

    // Image added in the pages from the media popup 
    $(document).on('click', '.select_choose_media', function(){
        var next_count      = 0;
        var last_count      = 0;
        var submodule       = $('.sub_module').val();
        var file_name       = $('.file_name').val();
        var fileCropOrNot   = $('.file_crop').val();
        var mainmodule      = $('.main_module').val();
        var module_id       = $('.module_id').val();
        var media_limit     = $('.media_limit').val();
        var imgDiv          = '';
        var imgSrc          = $('.tab-pane.active').find('.selectedMediaSrc').data('src');
        var fileName        = $('.tab-pane.active').find('.selectedMediaSrc').data('filename');
        var imgUrl          = $('.tab-pane.active').find('.selectedMediaSrc').val();
        var media_id        = $('.tab-pane.active').find('.selectedMediaId').val();  
        //last_count = $('.media_'+mainmodule+'_'+submodule+':last').data('off');

        var obj = jQuery.parseJSON(media_id);
        if($.isArray(obj)){
            jQuery.each(obj, function(index, value){
                last_count = $('.media_'+mainmodule+'_'+submodule).length;
                next_count = last_count+1;
                var id = addMediaLink(module_id, mainmodule, submodule, value);
                /*alert(mainmodule)
                alert(submodule);*/
                if(submodule == "audio" || (mainmodule == "User" && submodule == "attachments") || (mainmodule == "Show") || (mainmodule == "Host" && submodule == "itunes")){
                    var row = '<div class="col-sm-4 adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="'+imgUrl+'" target="_blank"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a onclick="Edit_Media('+media_id+');"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+mainmodule+'_'+submodule+'" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                }else if(mainmodule == "InventoryItem"){
                    //alert(uploadedMediaObj.path);
                    if(submodule == "files"){
                        var row = '<div class="col-sm-10 type_file adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-bonus-item-thumb"><a href="'+APP_URL+'/download/app/public/'+uploadedMediaObj.path+'" ><div class="jFiler-bonus-item-thumb"><img class="bonu-item-img" src="'+imgSrc+'" draggable="false">&nbsp;'+uploadedMediaObj.file_name+'</div></a><a class="list-delete"><i class="icon-jfi-trash color_icon" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this, \'.col-sm-10\');"></a></div></div></li></ul></div></div>';
                    }else{
                        var row = '<div class="col-sm-4 adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="javascript:void(0)" onclick="editMediaImage(this, &quot;'+mainmodule+'&quot;, &quot;'+submodule+'&quot;, &quot;image&quot;, '+module_id+', &quot;'+file_name+'&quot;, '+id+')"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a onclick="Edit_Media('+media_id+');"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+mainmodule+'_'+submodule+'" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                    }
                    
                }else{
                    var row = '<div class="col-sm-4 adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="javascript:void(0)" onclick="editMediaImage(this, &quot;'+mainmodule+'&quot;, &quot;'+submodule+'&quot;, &quot;image&quot;, '+module_id+', &quot;'+file_name+'&quot;, '+id+')"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a onclick="Edit_Media('+media_id+');"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+mainmodule+'_'+submodule+'" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                }
              
                if(last_count == 0){
                    $('.media_'+mainmodule+'_'+submodule+'_outer').after(row); 
                }else{
                    $('.media_'+mainmodule+'_'+submodule+':last').after(row); 
                }
            });
        }else{
                last_count = $('.media_'+mainmodule+'_'+submodule).length;
                next_count = last_count+1;
                var id = addMediaLink(module_id, mainmodule, submodule, media_id);
                if(mainmodule == "InventoryItem"){
                    //alert(uploadedMediaObj.path);
                    if(submodule == "files"){
                        var row = '<div class="col-sm-10 type_file adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-bonus-item-thumb"><a href="'+imgUrl.replace('storage', 'download')+'" ><div class="jFiler-bonus-item-thumb"><img class="bonu-item-img" src="'+imgSrc+'" draggable="false">&nbsp;'+fileName+'</div></a><a class="list-delete"><i class="icon-jfi-trash color_icon" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this, \'.col-sm-10\');"></a></div></div></li></ul></div></div>';
                    }else{
                        var row = '<div class="col-sm-4 adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="javascript:void(0)" onclick="editMediaImage(this, &quot;'+mainmodule+'&quot;, &quot;'+submodule+'&quot;, &quot;image&quot;, '+module_id+', &quot;'+file_name+'&quot;, '+id+')"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a onclick="Edit_Media('+media_id+');"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+mainmodule+'_'+submodule+'" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                    }
                }
                else if(submodule == "files" || submodule == "audio" || (mainmodule == "User" && submodule == "attachments") || (mainmodule == "Show")){
                    var row = '<div class="col-sm-4 adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="'+imgUrl+'" target="_blank"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a onclick="Edit_Media('+media_id+');"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+mainmodule+'_'+submodule+'" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                }else{
                    var row = '<div class="col-sm-4 adding-medias media_'+mainmodule+'_'+submodule+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="javascript:void(0)" onclick="editMediaImage(this, &quot;'+mainmodule+'&quot;, &quot;'+submodule+'&quot;, &quot;image&quot;, '+module_id+', &quot;'+file_name+'&quot;, '+id+')"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a onclick="Edit_Media('+media_id+');"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+mainmodule+'_'+submodule+'" data-mainmodule="'+mainmodule+'" data-submodule="'+submodule+'" onclick="mediaLinkDelete('+id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                }
              
                if(last_count == 0){
                    $('.media_'+mainmodule+'_'+submodule+'_outer').after(row); 
                }else{
                    $('.media_'+mainmodule+'_'+submodule+':last').after(row); 
                }
        }
        $('#myModal').modal('toggle');
        $('.select_choose_media').attr("disabled", true);
    });

    //Execute the dropzone for adding new media dfrom the modules.
    $(document).on('click', '.open_choose_media', function(){
        var submodule   = $(this).data('submodule');
        var mainmodule  = $(this).data('mainmodule');
        var media_limit = $(this).data('limit');

        // Set the main module and submodule inside the modal
        $('#myModal').find('#banner_image').attr('data-module', mainmodule);
        $('#myModal').find('#banner_image').attr('data-image-for', submodule);

        if($('.main_module').val() == ''){
            $('.main_module').val(mainmodule);
        }
        if($('.module_id').val() == ''){
            $('.module_id').val($(this).data('module_id'));
        }
        //if($('.file_crop').val() == ''){
            $('.file_crop').val($(this).data('file_crop'));
        //}
        //var last_count = $('.media_'+mainmodule+'_'+submodule+':last').data('off');
        var last_count = $('.media_'+mainmodule+'_'+submodule).length;

        var remainingoff = media_limit-last_count;
        $('.remaining_file').val(remainingoff);
        if(last_count >= media_limit){
            $(this).data('target','');
            var msg = '';  
            if(media_limit == 1){
                msg = 'You can upload only '+media_limit+' file.';  
            }else{        
                msg = 'You can upload only '+media_limit+' files.';  
            }
                
            swal({
               title: 'Warning!', 
               text : msg,
               type : 'warning'
            });

        }else{

            //$(this).attr('data-target', '#myModal');
            $('#myModal').modal('toggle');
            // Clear the media first and show selected upload from dropzone by default
            $('#media_append_section').find('ul.jFiler-items-list').html('');
            $('#UploadFiles').addClass('active');
            $('#MediaFiles').removeClass('active');
            $('#myModal').find('.nav.nav-tabs').find('li:nth-child(1)').removeClass('active');
            $('#myModal').find('.nav.nav-tabs').find('li:nth-child(2)').addClass('active');

            $('.sub_module').val($(this).data('submodule'));
            $('.main_module').val($(this).data('mainmodule'));
            var media_type  = $(this).data('media_type');
            var media_limit = $(this).data('limit');
            var dimension   = $(this).data('dimension');
            
            $('.media_limit').val(media_limit);            
            if(dimension != ''){
                $('#mediaDimension').val(dimension).attr('disabled', true);
            }else{
                $('#mediaDimension').val(dimension).attr('disabled', false);
            }           

            $('#images').attr('checked', false);
            $('#audios').attr('checked', false);
            $('#videos').attr('checked', false);

            if (typeof myDropzone !== "undefined") {
                myDropzone.destroy();
            }
            if(media_type == "image"){
                $('#images').closest('.radio').show();
                $('#images').attr('checked', 'checked').prop('checked',true).attr('disabled', true);
                $('#audios').closest('.radio').hide();
                $('#videos').closest('.radio').hide();
            } else if(media_type == "audio"){
                $('#images').closest('.radio').hide();
                $('#audios').closest('.radio').show();
                $('#audios').attr('checked', 'checked').prop('checked', true).attr('disabled', true);
                $('#videos').closest('.radio').hide();
            } else if(media_type == "video"){
                $('#images').closest('.radio').hide();
                $('#audios').closest('.radio').hide();
                $('#videos').closest('.radio').show();
                $('#videos').attr('checked', 'checked').prop('checked',true).attr('disabled', true);
            } else{
                $('#images').closest('.radio').show();
                $('#audios').closest('.radio').show();
                $('#videos').closest('.radio').show();
                $('#images').attr('checked', 'checked').prop('checked',true);
            }
            createDropZoneInstance($('#myModal').find('.dropzone'), media_type);
        }
    });

    //Execute the dropzone for adding new media dfrom the modules.
    function editMediaImage(this_obj, folder_name, module_name, media_type, id, media_name,old_media_id=''){

        var that = $(this_obj);
        if(folder_name != "Banner"){
            var modalTemplate = '<div id="custom-modal" class="modal-demo modal">'
                +'<button type="button" class="close" onclick="$(this).closest(&quot;#custom-modal&quot;).modal(&quot;hide&quot;);$(&quot;.modal-backdrop.in&quot;).remove();$(&quot;body&quot;).removeClass(&quot;remove_scroll&quot;);">'
                    +'<span>&times;</span><span class="sr-only">Close</span>'
                +'</button>'
                +'<h4 class="custom-modal-title">Edit Image</h4>'
                +'<div class="custom-modal-text">'
                   +'<!-- Content -->'
                    +'<div class="container">'
                        +'<div class="row">'
                            +'<div class="col-md-12">'
                                +'<div class="image-container">'
                                    +'<img id="image" src="" alt="Picture" style="width:100%">'
                                +'</div>'
                            +'</div>'
                            
                        +'</div>'
                        +'<div class="row" style="margin-top: 10px;">'
                            +'<div class="col-md-12 docs-buttons">'
                                +'<div class="btn-group">'
                                    +'<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">'
                                            +'<span class="fa fa-search-plus"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">'
                                            +'<span class="fa fa-search-minus"></span>'
                                        +'</span>'
                                    +'</button>'
                                +'</div>'

                                +'<div class="btn-group">'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, -10, 0)">'
                                            +'<span class="fa fa-arrow-left"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 10, 0)">'
                                            +'<span class="fa fa-arrow-right"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, -10)">'
                                            +'<span class="fa fa-arrow-up"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, 10)">'
                                            +'<span class="fa fa-arrow-down"></span>'
                                       +'</span>'
                                    +'</button>'
                                +' </div>'

                                +'<div class="btn-group">'
                                    +'<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">'
                                            +'<span class="fa fa-rotate-left"></span>'
                                        +'</span>'
                                +'</button>'
                              +'<button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">'
                               +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">'
                                  +'<span class="fa fa-rotate-right"></span>'
                                +'</span>'
                              +'</button>'
                           +' </div>'

                            +'<div class="btn-group">'
                              +'<button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleX&quot;, -1)">'
                                  +'<span class="fa fa-arrows-h"></span>'
                                +'</span>'
                              +'</button>'
                              +'<button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleY&quot;, -1)">'
                                  +'<span class="fa fa-arrows-v"></span>'
                                +'</span>'
                              +'</button>'
                            +'</div>'

                           +'<div class="btn-group">'
                              +'<button type="button" class="btn btn-primary" data-method="crop" title="Crop">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;crop&quot;)">'
                                  +'<span class="fa fa-check"></span>'
                                +'</span>'
                              +'</button>'
                              +'<button type="button" class="btn btn-primary" data-method="clear" title="Clear">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;clear&quot;)">'
                                  +'<span class="fa fa-remove"></span>'
                                +'</span>'
                              +'</button>'
                            +'</div>'

                            +'<div class="btn-group">'
                              +'<button type="button" class="btn btn-primary" data-method="reset" title="Reset">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;reset&quot;)">'
                                  +'<span class="fa fa-refresh"></span>'
                                +'</span>'
                              +'</button>'
                              +'<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">'
                                +'<input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Import image with Blob URLs">'
                                  +'<span class="fa fa-upload"></span>'
                                +'</span>'
                              +'</label>'
                             
                            +'</div>'

                            +'<div class="btn-group btn-group-crop">'
                                +'<button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" data-image_for="'+folder_name+'" data-module="'+module_name+'" data-form_type="add" data-crop="1" id="getCropped" data-old_image="" data-outer_dropzone="HostPhoto0" data-dropzone_obj="">'
                                    +'<span class="docs-tooltip crop-upload" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { maxWidth: 4096, maxHeight: 4096 })">'
                                        +'Apply'
                                    +'</span>'
                                +'</button>'
                            +'</div>'

                            //Set cropped file data
                            //+'<input type="file" name="files" id="setFileData" value="">'

                            +'<!-- Show the cropped image in modal -->'
                            +'<div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">'
                              +'<div class="modal-dialog">'
                                +'<div class="modal-content">'
                                  +'<div class="modal-header">'
                                    +'<h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>'
                                    +'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                                      +'<span aria-hidden="true">&times;</span>'
                                    +'</button>'
                                  +'</div>'
                                  +'<div class="modal-body"></div>'
                                  +'<div class="modal-footer">'
                                    +'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
                                    +'<a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>'
                                  +'</div>'
                                +'</div>'
                              +'</div>'
                            +'</div><!-- /.modal -->'
                          +'</div><!-- /.docs-buttons -->'

                         +'<div class="col-md-12 docs-toggles">'
                            +'<!-- <h3>Toggles:</h3> -->'
                            +'<div class="btn-group d-flex flex-nowrap" data-toggle="buttons">'
                              +'<label class="btn btn-primary active">'
                                +'<input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">'
                                  +'16:9'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                                +'<input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">'
                                 +'4:3'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                                +'<input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">'
                               +' <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">'
                                 +' 1:1'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                               +' <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">'
                               +' <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">'
                                +'  2:3'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                                +'<input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">'
                                  +'Free'
                                +'</span>'
                              +'</label>'
                            +'</div>'

                          +'</div><!-- /.docs-toggles -->'
                        +'</div>'
                      +'</div>'
                +'</div>'
            +'</div>'
            +'<!-- End Modal -->';

            // dynamically create modals to allow multiple files processing
            var $cropperModal = $(modalTemplate);
            // 'Crop and Upload' button in a modal
            var $uploadCrop = $cropperModal.find('.crop-upload');
            var $img = $('<img />'); 
            $img.attr('src', that.find('img').attr('src'));
            $cropperModal.find('.image-container').html($img);
            var cropper  = initializeCropper($img);     

            $cropperModal.modal('show');

            // listener for 'Crop and Upload' button in modal
            $uploadCrop.on('click', function() {
                 
                // get cropped image data
                var blob = $img.cropper('getCroppedCanvas').toDataURL();
                // transform it to Blob object
                var newFile = dataURItoBlob(blob);
              
                $('#image').attr('src',$img.attr('src'));
                // set 'cropped to true' (so that we don't get to that listener again)
                newFile.cropped = true;
            
                /* ---------------- SEND IMAGE TO THE SERVER-------------------------*/

                var formData = new FormData();
                formData.append('files[]', newFile, media_name);
                formData.append('image_for', module_name);
                formData.append('module', folder_name);
                formData.append('form_type', 'edit');
                formData.append('id', id);
             
                // Use `jQuery.ajax` method
                $.ajax(APP_URL+"/admin/cms/media/add/"+media_type, {
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        var next_count  = 0;
                        var last_count  = 0;                    
                        var module_id   = id;
                        var imgDiv      = '';
                        var imgSrc      = response.data.url;
                        var media_id    = response.data.id;
                                       
                        last_count = $('.media_'+folder_name+'_'+module_name).length;
                        next_count = last_count+1;

                        var new_id = addMediaLink(module_id, folder_name, module_name, media_id, 'edit', old_media_id); 
                        //console.log(folder_name);
                        //console.log(module_name);

                        /*if(module_name == "itunes" && folder_name == "Host"){
                            var row = '<div class="col-sm-4 media_'+folder_name+'_'+module_name+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="'+imgSrc+'"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+folder_name+'_'+module_name+'" data-mainmodule="'+folder_name+'" data-submodule="'+module_name+'" onclick="mediaLinkDelete('+new_id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                        }else{*/
                            var row = '<div class="col-sm-4 adding-medias media_'+folder_name+'_'+module_name+'" data-off="'+ next_count +'"><div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid"><li class="jFiler-item"><div class="jFiler-item-container"><div class="jFiler-item-inner"><div class="jFiler-item-thumb"><a href="javascript:void(0)" onclick="editMediaImage(this, &quot;'+folder_name+'&quot;, &quot;'+module_name+'&quot;, &quot;image&quot;, '+module_id+', &quot;'+response.data.file_name+'&quot;, '+id+')"><div class="jFiler-item-thumb-image"><img src="'+imgSrc+'" draggable="false"></div></a></div><div class="jFiler-item-assets jFiler-row"><ul class="list-inline pull-right"><li><a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_'+folder_name+'_'+module_name+'" data-mainmodule="'+folder_name+'" data-submodule="'+module_name+'" onclick="mediaLinkDelete('+new_id+', this);"></a></li></ul></div></div></div></li></ul></div></div>';
                        //}

                        if(last_count == 0){
                            $('.media_'+folder_name+'_'+module_name+'_outer').after(row); 
                        }else{
                            $('.media_'+folder_name+'_'+module_name+':last').after(row); 
                            that.closest('.col-sm-4').remove();    
                        }
                    },
                    error: function () {
                      console.log('Upload error');
                    }
                });

                /* ---------------------------------------------------- */

                $cropperModal.modal('hide');
                $('.modal-backdrop.in').remove();
            });
        }
        
    }

    // toggle Scheduled Share model
    $("#btnEpisodeScheduledShare").click(function(){
        $("#episodeScheduledShare").toggle();
    });

    // Delete media from media section
    function deleteMedia(id=0, this_obj){

        if(id != '' && id !=0){
            var that = this_obj;
            var url = '/admin/cms/media/delete_media';                
            var data = {'id':id};
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm) {
                    makeAjaxRequest('POST', url, data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            swal({
                               title: 'Deleted!', 
                               text : 'Media has been deleted successfully.',
                               type : 'success'
                            });                            
                            $(that).closest('.jFiler-item').remove();
                            
                        }else{
                            swal({
                               title: 'Deleted!', 
                               text : 'Media could not be deleted.',
                               type : 'error'
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                }
            });
        }
    }   

    function fetchContentData(content_cat_id=''){
        
        var url = '/admin/cms/content_data/index';                
        var data = {'content_cat_id':content_cat_id};

        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            if(response.status == 'success'){
                /*swal({
                   title: 'Deleted!', 
                   text : 'Media has been deleted successfully.',
                   type : 'success'
                });                            
                $(that).closest('.jFiler-item').remove();*/
                
            }else{
               /* swal({
                   title: 'Deleted!', 
                   text : 'Media could not be deleted.',
                   type : 'error'
                });*/
            }
        })
        .fail(function(xhr) {
            console.log('error callback ', xhr);
        });
    }

    // Update the query string in the url
    // Here url is the old url, key is the query parameter and the value is the new value assigned to that query parameter.
    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    $(function(){
        var d = new Date($.now());
        var m =d.getMonth()+1;
        var y =d.getFullYear();
        var dd =day = d.getDate();
        var date =m+'-'+dd+'-'+y;
    
        $('.datepicker').datepicker({
            defaultDate: date,
            format: 'mm-dd-yyyy',
            startDate: '+0d',
            autoclose: true
        });
    });
    $(function(){
        var t = new Date($.now());
        $('.timepicker').timepicker({
            defaultTime:t.getHours()+":"+t.getMinutes()+":"+t.getSeconds(),
        });
        $('.timepickerEdit').timepicker({
            //defaultTime:false,
        });
    });
    


    /*!function($) {*/
    
    if($('#distributed-series').length > 0){
        var barcharts = '';
        var ChartJs = function() {};
        ChartJs.prototype.destroys = function() {
            barcharts.destroy();
        }
        ChartJs.prototype.respChart = function(selector,type,data, options) {
            // get selector by context
            var ctx = selector.get(0).getContext("2d");
            // pointing parent container to make chart js inherit its width
            var container = $(selector).parent();

            // enable resizing matter
            $(window).resize( generateChart );

            // this function produce the responsive Chart JS
            function generateChart(){
                // make chart width fit with its container
                var ww = selector.attr('width', $(container).width() );
                switch(type){
                    case 'Line':
                        barcharts = new Chart(ctx, {type: 'line', data: data, options: options});
                        break;
                    case 'Doughnut':
                        barcharts = new Chart(ctx, {type: 'doughnut', data: data, options: options});
                        break;
                    case 'Pie':
                        barcharts = new Chart(ctx, {type: 'pie', data: data, options: options});
                        break;
                    case 'Bar':
                        barcharts = new Chart(ctx, {type: 'bar', data: data, options: options});
                        break;
                    case 'Radar':
                        barcharts = new Chart(ctx, {type: 'radar', data: data, options: options});
                        break;
                    case 'PolarArea':
                        barcharts = new Chart(ctx, {data: data, type: 'polarArea', options: options});
                        break;
                }
                // Initiate new chart or Redraw

            };
            // run function - render chart at first load
            generateChart();
        },
        //init
        ChartJs.prototype.init = function(chart_labels, chart_series) {
            //barchart
            var barChart = {
                labels: chart_labels,
                // scaleBeginAtZero : true,
                
                datasets: [
                    {
                        label: "Podcast statistics",
                        backgroundColor: "rgba(127, 193, 252, 0.3)",
                        borderColor: "#7fc1fc",
                        borderWidth: 1,
                        showTooltips: false,
                        // scaleStartValue:0,
                        scaleBeginAtZero : true,
                        hoverBackgroundColor: "rgba(127, 193, 252, 0.6)",
                        hoverBorderColor: "#7fc1fc",
                        data: chart_series,
                        /*scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }*/
                        
                    }
                ]
            };
            this.respChart($("#distributed-series"),'Bar',barChart);
        },
        $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
    }

    // Use `jQuery.ajax` method
    
    function fetchMedia(type='image'){
        //$('#media_type').remove();
        // On cropping
        if(type != 'image'){
            if($('#yes_crop').length > 0){
                $('#yes_crop').remove();
                $('#getCropped').remove();
                $('#allMediaDimension').hide();
            }
        }else{
            $('#image').prepend(' <div id="yes_crop"></div>');
            $('#allMediaDimension').show();
        }

        var url = [location.protocol, '//', location.host, location.pathname].join(''),
            parts = url.split("/"),
            last_part = parts[parts.length-1];
            new_url = url.replace(last_part, type);
        
        history.pushState({}, "", new_url);

        var dropzone_element = $('#media_image_'+type);
        //console.log(dropzone_element);
        $('.channel_image').removeClass('dropzone');
        if (typeof myDropzone !== "undefined") {
            myDropzone.destroy();
        }
        $('#media_image_'+type).addClass('dropzone');
        createDropZoneInstance(dropzone_element);
               
        $.ajax(APP_URL+"/admin/cms/media/add/"+type, {
            method: "GET",
            data: {'ajax':1},
            dataType:'json',
            // processData: false,
            //  contentType: false,
            success: function (response) {
               
                nextpage   = 2;
                totalpages = response.total_pages;
                pageArray  = [];

                $('#media_type').attr('data-type',response.type);
                $('#media_type').attr('data-total_media',response.total_pages);
                $('#'+response.type).find('ul.jFiler-items-list.jFiler-items-grid').html(response.html);
            },
            error: function () {
              console.log('Display error');
            }
        });
    }
    //Filter media by title
    $('#filterMediaByTitle').click(function(){
        var page      = "media";
        var type      = '';
        var dimension = '';
        if($(this).data('page')){
            page = $(this).data('page');
        } 
        
        if($('#media_type').length > 0){
            type  = $('#media_type').attr('data-type');
        }if($('.media_popup').length > 0){
            type  =  $("input[name='filterMedia']:checked").data('type');
        }       
        if(type == "image"){
            if($('#mediaDimension').length > 0){
                dimension = $('#mediaDimension option:selected').val();
            }
        }
        //Fresh new media
        nextpage = 2;
        pageArray  = [];
        getAllMedia(type, dimension, page);
    });
    //Remove media search title
    $('#remove_search').click(function(){
        $('#valueMediaByTitle').val('');
        var page      = "media";
        var type      = '';
        var dimension = '';
        if($(this).data('page')){
            page  = $(this).data('page');
        } 
        
        if($('#media_type').length > 0){
            type  = $('#media_type').attr('data-type');
        }if($('.media_popup').length > 0){
            type  =  $("input[name='filterMedia']:checked").data('type');
        }  

        if(type == "image"){
            if($('#mediaDimension').length > 0){
                dimension = $('#mediaDimension option:selected').val();
            }
        }
        //Fresh new media
        nextpage = 2;
        pageArray  = [];
        getAllMedia(type, dimension, page);
    });

    $(document).on('click','.list-thumb', function(){
        $('.list-thumb').parent().parent().addClass('list-item');
        $('.list-thumb').addClass('list-active');
        $('.thumbs').removeClass('list-active');
    });

    $(document).on('click','.thumbs', function(){
        $('.thumbs').parent().parent().removeClass('list-item');
        $('.thumbs').addClass('list-active');
        $('.list-thumb').removeClass('list-active');
    });

    //Fetch the info of the section inside the module
    function infoModule(module='', section='', this_obj){
        if(module != ''){
            $('#info-modal').find('.custom-modal-title').html('Add info for - '+section);
            $('#info-modal').find('#info_module').val(module);
            $('#info-modal').find('#info_section').val(section);
            //$('#info-modal').find('.custom-modal-text').html('Add something here');

            var url  = "/admin/user/get_module_info";
            var data =  { 'module':module, 'section':section};
            makeAjaxRequest('POST', url, data)
            .done(function(response) {
              
                if(response.status == "success"){
                    $('.module_info_desc').val(response.data.description);
                    if($('#sa-title').length > 0){
                        swal(response.data.section, response.data.description);
                    }
                }if(response.status == "error"){
                    $('.module_info_desc').val('');
                    if($('#sa-title').length > 0){
                        swal(section);
                    }
                }
            })
            .fail(function(xhr) {
                $('.module_info_desc').val('');                
            });
        }
    }

    //Save infomartion of section of modules
    $(document).on('click', '.save_module_info', function(){

        var module      = $('#info_module').val();
        var section     = $('#info_section').val();
        var description = $('.module_info_desc').val();
        var url         = "/admin/user/add_module_info";
        var data        =  { 'module':module, 'section':section,'description':description };
        makeAjaxRequest('POST', url, data)
        .done(function(response) {
            $('#info-modal').find('.close').trigger('click');
        })
        .fail(function(xhr) {
            $('#info-modal').find('.close').trigger('click');
        });
    });

    //set Repeated weekend
    $(document).on('change', '#ChannelSFId', function(){
        //var channel_id = $(this).val();
        // var channel_name = $( "#ChannelSFId option:selected" ).text();
        // $( ".channel_name" ).text(channel_name);
        $('.HasNotShows').css('display','none');
        $('#calendar').css('display','block');
        ShowsToChannel();
    });
    //get show of channel
    $(document).on('change', '.scheduleWeek', function(e){
        $(this).on("select2:select", function(e) {
            var data = e.params.data;
            selID = parseInt(data.id);
            var thisvalue = $('.scheduleWeek').val();
            if(selID == 0){
                thisvalue = ['0'];
                $('.scheduleWeek').select2('val', thisvalue);
            }
            else{
                if($.inArray('0', thisvalue) !== -1){
                    thisvalue = jQuery.grep(thisvalue, function(value) {
                      return value != '0';
                    });
                    $('.scheduleWeek').select2('val', thisvalue);
                }
            }
        });
    });

    function getEncoreDetail(episode_id=''){
        if(episode_id != ''){
            var url  = "/admin/show/episodes/encores_details";
            var data =  { 'episode_id': episode_id };
            makeAjaxRequest('POST', url, data)
            .done(function(response) {
                
                $('#encore_detail').find('.show_title').text(response.data.show);
                $('#encore_detail').find('.episode_title').text(response.data.episode);
                $('#encore_detail').find('.episode_date').text(response.data.premiereDateTime);
                $('#encore_detail').find('.episode_encores').html(response.data.encoresTxt);
                //Trigger to open the encore detail modal
                $('#open_encore_detail').trigger('click');
                
                
               
            })
            .fail(function(xhr) {
                //$('#info-modal').find('.close').trigger('click');
            });
        }
    }

    function searchGuestByFields(this_obj){
        console.log($(this_obj).val());
        var column = $(this_obj).val();
        guestTable.columns(column ).search( this.value ).draw();
        // guestTable.ajax.reload();
        //guestTable.search(this.value).draw();  
        //$('#guests_datatable').DataTable().ajax.reload();
        //$('#guests_datatable').DataTable().ajax.reload(null, false);
    }

    function ShowsToChannel(){
        var channel_id = $('#ChannelSFId').val();
        var channel_name = $( "#ChannelSFId option:selected" ).text();
        $( ".channel_name" ).text(channel_name);
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_channel_shows",
            data: { 'channel_id':channel_id },
            dataType: 'json',
            success: function(response){
                $('.ShowExtForThisChannel').val(response.length);
            }
        });
    }