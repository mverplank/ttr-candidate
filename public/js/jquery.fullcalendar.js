/**
* @author: Contriverz
* @since : 13-02-2019
* @component: Calendar
* 
*/
if($("#channel_schedule").length != 0) {
    right_btns = '';
}else{
    right_btns = 'month,agendaWeek,agendaDay';
}
//right_btns = 'month,agendaWeek,agendaDay';
all_schedules = [];
if($("#calendar").length != 0) {
    !function($) {
        "use strict";
   
        var CalendarApp = function() {
            this.$body = $("body")
            this.$modal = $('#event-modal'),
            this.$event = ('#external-events div.external-event'),
            this.$calendar = $('#calendar'),
            this.$saveCategoryBtn = $('.save-category'),
            this.$categoryForm = $('#add-category form'),
            this.$extEvents = $('#external-events'),
            this.$calendarObj = null
        };

        /* on drop */
        CalendarApp.prototype.onDrop = function (eventObj, date) { 
            var $this = this;
                // retrieve the dropped element's stored Event Object
                var originalEventObject = eventObj.data('eventObject');
                var $categoryClass = eventObj.attr('data-class');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                if ($categoryClass)
                    copiedEventObject['className'] = [$categoryClass];
                // render the event on the calendar
                $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    eventObj.remove();
                }
        },
        /* On click on event */
        CalendarApp.prototype.onEventClick =  function (calEvent, jsEvent, view) {
            
            var $this = this; 
            // console.log(calEvent);
            // alert();
            $('.add_multiple_channels').find('.portlet').not(':first').remove();
            if (calEvent.data.Slots === undefined) {
                //alert();
                getScheduleData(calEvent.id);
                var form = $('#ScheduleAdminAddForm');

                $this.$modal.modal({
                    backdrop: 'static'
                });           
               
                $this.$modal.find('.delete-event').show().end().find('.save-event').show().end().find('.save-event').unbind('click').click(function () {
                    form.submit();
                }); 
                $this.$modal.find('form').on('submit', function (event) {
                    event.stopImmediatePropagation();
                    var form_data = $( this ).serialize();
                    var schedule_url = "";
                    if($('#form_type').val() == "edit"){
                        schedule_url = APP_URL+"/admin/channel/schedules/edit";
                        $.ajax({
                            type: "POST",
                            url : schedule_url,
                            data: { 'data':form_data },
                            dataType:"json",
                            success: function(response)
                            {
                                $.CalendarApp.refresh();
                            },
                            error: function(error){
                                console.log('There is some error');
                                console.log(error);
                            }
                        });
                    } 
                    /*  else if($('#form_type').val() == "add"){
                        schedule_url = APP_URL+"/admin/channel/schedules/add";
                    }*/
                  
                    $this.$modal.modal('hide');
                    return false;                
                });

            }else{
                //console.log(calEvent);
            
                var title = calEvent.data.Title;
                var body = '';
                for (var off in calEvent.data.Slots) {
                    var slot = calEvent.data.Slots[off];
                    //getScheduleData(slot.id);
                    var form = $('#ScheduleAdminAddForm');
                    //console.log(slot);
                    body += '<a href="javacsript:void(0);" onclick="getScheduleData('+slot.id+');">' +
                            '<div class="sub-slot" style="background:' + slot.backgroundColor + ';border-color:' + slot.borderColor + ';">' +
                            slot.title + ' <br>' +
                            slot.data.Slot.repeatStr + ' <br>' +
                            "<span>" + slot.data.Slot.sd + ' - ' + slot.data.Slot.ed + "</span>" +
                            '</div>' +
                            '</a>';
                    $("#multiple-events-modal").find('#openScheduleBtn').data('schedule_id',slot.id);
                }
                $("#multiple-events-modal").find('.modal-title').html(title);
                $("#multiple-events-modal").find('.modal-body').html(body);
                $('#multiple-events-modal').modal('show');

                $this.$modal.find('.delete-event').show().end().find('.save-event').show().end().find('.save-event').unbind('click').click(function () {
                    form.submit();
                }); 
                $this.$modal.find('form').on('submit', function (event) {
                    event.stopImmediatePropagation();
                    var form_data = $( this ).serialize();
                    var schedule_url = "";
                    if($('#form_type').val() == "edit"){
                        schedule_url = APP_URL+"/admin/channel/schedules/edit";
                        $.ajax({
                            type: "POST",
                            url : schedule_url,
                            data: { 'data':form_data },
                            dataType:"json",
                            success: function(response)
                            {
                                $.CalendarApp.refresh();
                            },
                            error: function(error){
                                console.log('There is some error');
                                console.log(error);
                            }
                        });
                    } 
                    /*  else if($('#form_type').val() == "add"){
                        schedule_url = APP_URL+"/admin/channel/schedules/add";
                    }*/
                  
                    $this.$modal.modal('hide');
                    return false;                
                });

                  
            }
            /*if($.inArray('cal-episode', calEvent.className ) >= 0){  
                window.location.href=APP_URL+"/admin/show/episodes/view_agenda/"+calEvent.id+"/";
            }*/
        },
        /* on select */
        CalendarApp.prototype.onSelect = function (start, end, allDay) {
            var $this = this;
            var form = $('#ScheduleAdminAddForm');
            var show = $('.ShowExtForThisChannel').val();
            console.log(show);
            if(show > 0){
                $('.HasNotShows').css('display','none');
                $('#calendar').css('display','block');
                $this.$modal.modal({
                    backdrop: 'static'
                }); 
            }else{
               $('.HasNotShows').css('display','block');
               $('#calendar').css('display','none');
            }
                      
               
            $this.$modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.save-event').unbind('click').click(function () {
                form.submit();
                $this.$calendarObj.fullCalendar('unselect');
                //$this.$modal = $('#event-modal');
            });

            $this.$modal.find(form).on('submit', function (event) {

                event.stopImmediatePropagation();
                var error = 0;
                $( ".portlet" ).each(function( index, elem ) {                   
                    
                    // Check weekday is selected or not
                    var checked = $(elem).find(".weekdays:checkbox:checked").length > 0;
                    if (!checked){
                        swal({
                           title: '', 
                           text: 'Please select at least one week day',
                           type: 'warning'
                        });
                        error = 1;
                        return false;
                    }else{
                        error = 0;
                    }

                    // Check start date are filled or not                    
                    if(!$(elem).find(".scheduleStartDate").val()){
                        swal({
                           title: '', 
                           text: 'Please select the start date',
                           type: 'warning'
                        });
                        error = 1;
                        return false;
                    }else{
                        error = 0;
                    }

                    // Check end date are filled or not                    
                    if(!$(elem).find(".scheduleEndDate").val()){
                        swal({
                           title: '', 
                           text: 'Please select the end date',
                           type: 'warning'
                        });
                        error = 1;
                        return false;
                    }else{
                        error = 0;
                    }

                    // Check at least for the weeks selected
                    var weeks = $(elem).find(".scheduleWeek");
                    if (weeks.val() == null || weeks.val() == '') {
                        swal({
                           title: '', 
                           text: 'Please select the weeks',
                           type: 'warning'
                        });
                        error = 1;
                        return false;
                    } else{
                        error = 0;
                    }                                                 
                });
                if(error == 1){
                    //alert();
                    return false;
                }
                //alert();
                var form_data = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url : APP_URL+"/admin/channel/schedules/add",
                    data: { 'data':form_data },
                    success: function(response)
                    {
                        $.CalendarApp.refresh();
                    }
                });

                $this.$modal.modal('hide');
                return false;       
            });

        },
        CalendarApp.prototype.enableDrag = function() {
            //init events
            $(this.$event).each(function () {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });
        }
        CalendarApp.prototype.refresh = function() {
            console.log("come to refresh");
            var $this = this;
            $this.$calendar.fullCalendar('destroy');
            //$this.$modal = null;
            var date  = new Date();
            var d     = date.getDate();
            var m     = date.getMonth()+1;
            var y     = date.getFullYear();
            var channel_id = 0;
            channel_id     = $('#ChannelSFId').find(":selected").val();

            // Re-load events on calender
            $.ajax({
                type: "POST",
                url : APP_URL+"/admin/channel/schedules/get",
                data: { 'day':d, 'month':m, 'year':y, 'channel_id':channel_id },
                success: function(response)
                {
                    // Destroy object 
                    // Redraw the new schedules
                    callDefault($this, response.calendarEvents);
                }
            });
        }
        /* Initializing */
        CalendarApp.prototype.init = function() {
            this.enableDrag();
            var $this = this;

            /*  Initialize the calendar  */
            var date  = new Date();
            var d     = date.getDate();
            var m     = date.getMonth()+1;
            var y     = date.getFullYear();
            var today = new Date($.now());
            var channel_id = 0;
            channel_id     = $('#ChannelSFId').find(":selected").val();
           
            $.ajax({
                type: "POST",
                url : APP_URL+"/admin/channel/schedules/get",
                data: { 'day':d, 'month':m, 'year':y, 'channel_id':channel_id },
                success: function(response)
                {
                    // Display default show schedules in the calendar
                    callDefault($this, response.calendarEvents);
                }
            });

            //on new event
            this.$saveCategoryBtn.on('click', function(){
                var categoryName = $this.$categoryForm.find("input[name='category-name']").val();
                var categoryColor = $this.$categoryForm.find("select[name='category-color']").val();
                if (categoryName !== null && categoryName.length != 0) {
                    $this.$extEvents.append('<div class="external-event bg-' + categoryColor + '" data-class="bg-' + categoryColor + '" style="position: relative;"><i class="mdi mdi-checkbox-blank-circle m-r-10 vertical-middle"></i>' + categoryName + '</div>')
                    $this.enableDrag();
                }
            });
        },

        //init CalendarApp
        $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp
        
    }(window.jQuery),

    //initializing CalendarApp
    function($) {
        "use strict";
        $.CalendarApp.init()
    }(window.jQuery);


    function callDefault($this, defaultValues){
       
        $this.$calendarObj = $this.$calendar.fullCalendar({
            slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
            minTime: '00:00:00',
            maxTime: '24:00:00', 
            defaultView: 'agendaWeek',
            eventDurationEditable: false,
            handleWindowResize: true,   
            height: $(window).height() - 100,   
            /*header: {
                left: 'prev,next today',
                center: 'title',
                right: right_btns
            },*/
            allDaySlot: false,
            header:false,
            columnFormat: {
                month: 'ddd',
                week: 'ddd ',
                day: 'dddd '
            },
            events: defaultValues,
            eventAfterRender: function (event, element, view) {
              
                var $el = $(element);
                var channel_name = $('#ChannelSFId').find(":selected").text();
                var time = moment(event.start).format("h:mm A");
                var day  = moment(event.start).format('dddd');
                
                var $a  = $('<a class="cal-btn-add-show" data-channel="'+channel_name+'" data-time="'+time+'" data-day="'+day+'" title="Add Show" href="#"><i class="glyphicon glyphicon-plus"></i></a>');
               
                //$el.parent().append($a);
                $el.after($a);
               
                $a.attr('style', $el.attr('style'));
                /* $a.css({
                    left: parseInt($el.css('left')) + $el.width() - $a.width() - 5,
                    top: parseInt($el.css('top')) + $el.height() - $a.height() - 5
                });*/
                $el.find('.fc-event-title').after('<h2>test</h2>');
              
                if (event.data.Slots) { // mulitple shows defined for this slot         
                   
                    // Build sub-slots representing all of assfined shows
                    for (var off in event.data.Slots) {
                        var slot = event.data.Slots[off];
                        $el.children('.fc-content').children('div').filter(':last').after('<div class="sub-slot" style="background:' + slot.backgroundColor + ';border-color:' + slot.borderColor + ';">' + slot.title + '</div>');
                    }
                }
                /*var $el = $(element), 
                addUrl = $.mtsoft.url('admin/channel/schedules/add/' + $.ChannelId + '/' + event.start.toString('ddd') + '/' + ($.fullCalendar.formatDate(event.start, "HH:mm")).replace(/\:/ig, '')),
                        $a = $('<a class="cal-btn-add-show" title="Add Show" href="' + addUrl + '">' + $.mtsoft.ui.icon('add', {size: 16}, true) + '</a>');
                $el.parent().append($a);
                $a.css({
                    left: parseInt($el.css('left')) + $el.width() - $a.width() - 5,
                    top: parseInt($el.css('top')) + $el.height() - $a.height() - 5
                });
                
               */
            },
            //fixedWeekCount: false,
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events
            selectable: true,
            drop: function(date) { $this.onDrop($(this), date); },
            select: function (start, end, allDay) {              
                $this.onSelect(start, end, allDay); 
            },
            eventClick: function(calEvent, jsEvent, view) { 
                $this.onEventClick(calEvent, jsEvent, view); 
            },
            dayClick: function(date, jsEvent, view) {
                //Remove the weekdays selected options
                $('.weekdays:checkbox').removeAttr('checked');

                // Remove the select2 selected options from the model
                $("#ScheduleWeek").val('0').trigger("change"); 
                $("#ScheduleMonth").val('0').trigger("change"); 
                $("#ScheduleYear").val('0').trigger("change"); 
                $('.add_multiple_channels').find('.portlet').not(':first').remove();
                $('.scheduleTypeLabel').prop('checked', false);
                $('.scheduleType').prop('checked', false);
                $('#ScheduleTypeLabelLive').prop('checked', true);
                $('#ScheduleTypeFileOrStream').prop('checked', true);
                //$('.scheduleMonth').select2();
                $(".scheduleMonth").val('0').trigger("change"); 
                $('.schedule_stream').val('');

                var get_day = date._d.getDay();

                $('#schedule_channel').html($('#ChannelSFId :selected').html());
                $('#scheduleChannels').val($('#ChannelSFId :selected').val()).select2();
                $('.add_multiple_channels').find('.portlet').attr('data-id', $('#ChannelSFId :selected').val());
                $('.add_multiple_channels').find('.portlet-titles').val($('#ChannelSFId :selected').val());
                //$("#event-modal").find('.modal-title').html('Add Show - <span id="">'+$('#ChannelSFId :selected').html()+'</span>');
                $("#event-modal").find('.modal-title').html('Add Show');
                $("#event-modal").find('#ScheduleTime').val(date.format('h:mm A'));
                $("#event-modal").find('.btn-switch').find("input[type='radio']").prop("checked", false);
                if(get_day == 0){
                    $("#event-modal").find(".ScheduleDowSun").prop("checked", true); 
                    //$("#event-modal").find(".ScheduleDowSun").attr("checked", true); 
                }else if(get_day == 1){
                    $("#event-modal").find(".ScheduleDowMon").prop("checked", true); 
                    //$("#event-modal").find(".ScheduleDowMon").attr("checked", true);
                }else if(get_day == 2){
                    $("#event-modal").find(".ScheduleDowTue").prop("checked", true);
                    //$("#event-modal").find(".ScheduleDowTue").attr("checked", true); 
                }else if(get_day == 3){
                    $("#event-modal").find(".ScheduleDowWed").prop("checked", true);
                    //$("#event-modal").find(".ScheduleDowWed").attr("checked", true); 
                }else if(get_day == 4){
                    $("#event-modal").find(".ScheduleDowThu").prop("checked", true); 
                    //$("#event-modal").find(".ScheduleDowThu").attr("checked", true);
                }else if(get_day == 5){
                    $("#event-modal").find(".ScheduleDowFri").prop("checked", true); 
                    // $("#event-modal").find(".ScheduleDowFri").attr("checked", true);
                }else if(get_day == 6){
                    $("#event-modal").find(".ScheduleDowSat").prop("checked", true); 
                    //$("#event-modal").find(".ScheduleDowSat").attr("checked", true);
                }
                
                //Set Channel
                var channel_id   = $('#ChannelSFId :selected').val();
                $('#ScheduleStartDate').val('');
                $('#ScheduleEndDate').val('');
                $('#channel_schedule_id').val(channel_id);
                $('#scheduleSaveBtn').html('Add');
                $('#form_type').val('add');

                // Show the add more channels button on edit screen
                $('#showAddMoreChannels').show();

                getShows(channel_id);       
                getReplays(channel_id);
            }
        });
    }

    // Get Replays on channel change
    $(document).on('change', 'select.scheduleSchedulesChannelsId', function(){
        getReplays($(this).val(), '', this);
    });

    // Get Shows on channel change when additional channels are added
    $(document).on('change', 'select.show_channels', function(){
        getShows($(this).val(), '', this);
    });
    
    // Fetch the schedule acc. to the channel selected
    $(document).on('change', '#ChannelSFId', function(){
        //Append url with query parameter
        updateQueryStringParam('channel_id', $(this).val());
        $.CalendarApp.refresh();
    });

    // Fetch the shows acc. to the channel
    function getShows(channel_id, show_id="", that=''){

        //Create shows select box        
        var $el = '';
        if(that != ''){            
            $el = $(that).closest('.portlet').find('select.scheduleShowsId');
        }else{
            $el = $('#ScheduleShowsId');
        }

        $el.empty(); // remove old options
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_channel_shows",
            data: { 'channel_id':channel_id },
            dataType: 'json',
            success: function(response)
            {
                console.log(response.length);
                if(response.length > 0){
                    $.each(response, function(key,value) {
                        if(show_id != ''){
                            if(value.id == show_id){
                                $el.append($("<option selected></option>").attr("value", value.id).text(value.name));
                            }else{
                                $el.append($("<option></option>").attr("value", value.id).text(value.name));
                            }
                        }else{
                            $el.append($("<option></option>").attr("value", value.id).text(value.name));
                        }                   
                    });
                }                
                $el.selectpicker('refresh');
            }
        });
    }

    // Fetch the replays
    function getReplays(channel_id, schedule_id='', that=''){

        //Create replay select box
        var $e2 = $e3 = '';
        if(that != ''){            
            $el2 = $(that).closest('.schedule_replays').find('select.scheduleSchedulesId');
            $el3 = $(that);
        }else{
            $el2 = $('#ScheduleSchedulesId');
            $el3 = $('#ScheduleSchedulesChannelsId');
        }
        
        $el2.empty(); // remove old options
        $el3.empty(); // remove old options
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_shows_replays",
            data: { 'channel_id':channel_id },
            success: function(response)
            {
                $.each(response.replays, function(key,value) {
                    if(schedule_id != ''){
                        if(schedule_id == value.id){
                            $el2.append($("<option selected='selected'></option>").attr("value", value.id).text(value.Schedule__name));
                        }else{
                            $el2.append($("<option></option>").attr("value", value.id).text(value.Schedule__name));
                        }                    
                    }else{
                        $el2.append($("<option></option>").attr("value", value.id).text(value.Schedule__name));
                    }
                });
                $el2.selectpicker('refresh');

                // Show selected option in the channels for replay
                $.each(response.channels, function(key,value) {
                    if(key == channel_id){
                        $el3.append($("<option selected='selected'></option>").attr("value", key).text(value));
                    }else{
                        $el3.append($("<option></option>").attr("value", key).text(value));
                    }
                    
                });
                $el3.selectpicker('refresh');
            }
        });
    }

    /* Fetch the schedule data by ID */
    function getScheduleData(id){
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_selected_schedule",
            data: { 'schedule_id':id },
            dataType: 'json',
            success: function(response)
            {                
                var channel_name = $('#ChannelSFId :selected').html();
                $("#event-modal").find('.modal-title').html('Edit Show - <span id="schedule_channel">'+channel_name+'</span>');
                $("#event-modal").find('#scheduleChannels').val($('#ChannelSFId :selected').val()).select2();
                $("#event-modal").find("input[type='radio']").prop("checked",false);
                $("#event-modal").find("input[type='checkbox']").prop("checked",false);
                $("#event-modal").find('#ScheduleStartDate').val(response.start_date);
                $("#event-modal").find('#ScheduleEndDate').val(response.end_date);
                $("#event-modal").find('#ScheduleTime').val(response.time);
                $("#event-modal").find('.btn-switch').find("input[type='radio']").prop("checked", false);
                
                // split string on comma space
                var dowarray = response.dow.split(','); 
                $.each(dowarray, function( index, value ) {
                    if(value == "Sun"){
                        $("#event-modal").find(".ScheduleDowSun").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowSun").attr("checked", true); 
                    }else if(value == "Mon"){
                        $("#event-modal").find(".ScheduleDowMon").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowMon").attr("checked", true); 
                    }else if(value == "Tue"){
                        $("#event-modal").find(".ScheduleDowTue").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowTue").attr("checked", true); 
                    }else if(value == "Wed"){
                        $("#event-modal").find(".ScheduleDowWed").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowWed").attr("checked", true); 
                    }else if(value == "Thu"){
                        $("#event-modal").find(".ScheduleDowThu").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowThu").attr("checked", true); 
                    }else if(value == "Fri"){
                        $("#event-modal").find(".ScheduleDowFri").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowFri").attr("checked", true); 
                    }else if(value == "Sat"){
                        $("#event-modal").find(".ScheduleDowSat").prop("checked", true); 
                        //$("#event-modal").find(".ScheduleDowSat").attr("checked", true); 
                    } 
                });
                               

                if(response.type_label == "live"){
                    $("#ScheduleTypeLabelLive").prop("checked", true); 
                }else if(response.type_label == "replay"){
                    $("#ScheduleTypeLabelReplay").prop("checked", true); 
                }

                if(response.type == "file_or_stream"){
                    $("#ScheduleTypeFileOrStream").prop("checked", true); 
                }else if(response.type == "live"){
                    $("#ScheduleTypeLive").prop("checked", true); 
                }else if(response.type == "replay"){
                    $("#ScheduleTypeReplay").prop("checked", true); 
                    $('.schedule_shows').hide();
                    $('.schedule_replays').show();
                }

                $("#ScheduleDuration").val(response.duration).selectpicker('refresh');
                $("#domain").val(response.stream);
                $('#channel_schedule_id').val(response.channels_id);
                $('#schedule_id').val(response.id);
                $('#ScheduleWeek').val(response.week.split(',')).select2();
                $('#ScheduleMonth').val(response.month).selectpicker('refresh');
                $('#ScheduleYear').val(response.year).selectpicker('refresh');

                // Change the button text and form type
                $('#scheduleSaveBtn').html('Edit');
                $('#form_type').val('edit');

                // Remove the add more channels button on edit screen
                $('#showAddMoreChannels').hide();

                // Get shows and replays related to the channels and shows
                getShows(response.channels_id, response.shows_id);
                getReplays(response.channels_id, response.schedules_id);
            }
        });
    }

    $(document).on('click', '.delete-event', function(event){
        //alert($('#schedule_id').val());
        event.preventDefault();
        var data = {
                    '_token' : csrf_token, 
                    'id':$('#schedule_id').val()
                };
               
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) { 
                makeAjaxRequest('POST', '/admin/channel/schedules/delete', data)
                .done(function(response) {
                   // console.log(response);
                    if(response.status == 'success'){
                        swal({
                           title: 'Deleted!', 
                           text: 'The schedule has been deleted successfully.',
                           type: 'success'
                        });
                        $('#event-modal').modal('hide');
                        $.CalendarApp.refresh();
                     
                    }else{
                        swal({
                           title: 'Deleted!', 
                           text: 'The schedule could not be deleted.',
                           type: 'error'
                        });
                         $('#event-modal').modal('hide');
                    }
                })
                .fail(function(xhr) {
                    console.log('error callback ', xhr);
                });
            }
        });
    });
}

///********************** Calendar for the episodes *********************************///

if($("#shows_calendar").length != 0) {
    !function($) {
        "use strict";
   
        var CalendarApp = function() {
            this.$body  = $("body")
            this.$modal = $('#episode-modal'),
            this.$event = ('#external-events div.external-event'),
            this.$calendar = $('#shows_calendar'),
            this.$saveCategoryBtn = $('.save-category'),
            this.$categoryForm = $('#add-category form'),
            this.$extEvents = $('#external-events'),
            this.$calendarObj = null
        };

        /* on drop */
        CalendarApp.prototype.onDrop = function (eventObj, date) { 
            var $this = this;
                // retrieve the dropped element's stored Event Object
                var originalEventObject = eventObj.data('eventObject');
                var $categoryClass = eventObj.attr('data-class');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                if ($categoryClass)
                    copiedEventObject['className'] = [$categoryClass];
                // render the event on the calendar
                $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    eventObj.remove();
                }
        },
        /* On click on event */
        CalendarApp.prototype.onEventClick =  function (calEvent, jsEvent, view) {
            // click on calendar empty date/time slot  
            if($.inArray( 'no-edit', calEvent.className ) >= 0){
                // do noting 
                return true;
            }
            /*console.log(ROLE);
            return false;*/
            // For HOST
            if(ROLE == 2){
                if($.inArray('cal-episode', calEvent.className ) >= 0){  
                    window.location.href=APP_URL+"/host/show/episodes/view_agenda/"+calEvent.id+"/";
                }else if($.inArray('cal-encore', calEvent.className ) >= 0){
                    var schedule_id = calEvent.id;
                    var start_day = calEvent.start.format('YYYY-MM-DD');
                    window.location.href=APP_URL+"/host/show/episodes/select_encore/"+schedule_id+"/"+start_day;
                }else{                
                    var schedule_id = calEvent.id;
                    var start_day = calEvent.start.format('YYYY-MM-DD');
                    var day = calEvent.start.format('ddd');                
                    window.location.href=APP_URL+"/host/show/episodes/add_agenda/"+schedule_id+"/"+start_day+"/"+day;
                }
            }else{
                if($.inArray('cal-episode', calEvent.className ) >= 0){  
                    window.location.href=APP_URL+"/admin/show/episodes/view_agenda/"+calEvent.id+"/";
                }else if($.inArray('cal-encore', calEvent.className ) >= 0){
                    var schedule_id = calEvent.id;
                    var start_day = calEvent.start.format('YYYY-MM-DD');
                    window.location.href=APP_URL+"/admin/show/episodes/select_encore/"+schedule_id+"/"+start_day;
                }else{                
                    var schedule_id = calEvent.id;
                    var start_day = calEvent.start.format('YYYY-MM-DD');
                    var day = calEvent.start.format('ddd');                
                    window.location.href=APP_URL+"/admin/show/episodes/add_agenda/"+schedule_id+"/"+start_day+"/"+day;
                }
            }
            
        },
        /* on select */
        CalendarApp.prototype.onSelect = function (start, end, allDay) {
            return false;
        },
        CalendarApp.prototype.enableDrag = function() {
            //init events
            $(this.$event).each(function () {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });
        }
        /* Refreshing */
        CalendarApp.prototype.refresh = function() {
           
            var $this = this;
            // Fetch current view
            var view       = $this.$calendar.fullCalendar('getView');
            var start_date = view.start.subtract(1, "days").format();
            var end_date   = view.end.format();
            
            var channel_id = 0;
            channel_id     = $('#ChannelSFId').find(":selected").val();
            $this.$calendar.fullCalendar('destroy');
            // Re-load events on calender
            /*$.ajax({
                type: "POST",
                url : APP_URL+"/admin/show/schedules/get",
                data: { 'start':start_date, 'end':end_date, 'channel_id':channel_id },
                success: function(response)
                {
                    // Destroy object 
                    
                    // Redraw the new schedules
                    callDefault($this, response.calendarEvents);
                }
            });*/
            $this.$calendarObj = $this.$calendar.fullCalendar({
                slotDuration: '00:15:00', 
                minTime: '00:00:00',
                maxTime: '24:00:00',   
                defaultView: 'agendaWeek',
                eventDurationEditable: false,
                handleWindowResize: true,   
                height: $(window).height() - 100,   
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                allDaySlot: false,
                // header:false,
                columnFormat: {
                    month: 'ddd M/D',
                    week: 'ddd M/D',
                    day: 'dddd M/D'
                },
                //events:  APP_URL+"/admin/show/schedules/get",
                eventSources: [
                    // your event source
                    {
                      url: APP_URL+"/admin/show/schedules/get",
                      type: 'GET',
                      data: {
                        'channel_id': channel_id
                      },
                      error: function() {
                        //alert('there was an error while fetching events!');
                      },
                      //color: 'yellow',   // a non-ajax option
                      //textColor: 'black' // a non-ajax option
                    }
                    
                ],
                //fixedWeekCount: false,
                editable: false,
                droppable: false, 
                eventLimit: true, 
                selectable: true,
                drop: function(date) { $this.onDrop($(this), date); },
                select: function (start, end, allDay) { 
                    $this.onSelect(start, end, allDay); 
                },
                eventClick: function(calEvent, jsEvent, view) { 
                    $this.onEventClick(calEvent, jsEvent, view); 
                },
                dayClick: function(date, allDay, jsEvent, view) {
                    return false;
                }
            });
        }
        /* Initializing */
        CalendarApp.prototype.init = function() {
            this.enableDrag();
            var $this = this;
            /*  Initialize the calendar  */
            var date  = new Date();
            var d     = date.getDate();
            var m     = date.getMonth()+1;
            var y     = date.getFullYear();
            var today = new Date($.now());
            
            var channel_id = 0;
            channel_id     = $('#ChannelSFId').find(":selected").val();

            $this.$calendarObj = $this.$calendar.fullCalendar({
                slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
                minTime: '00:00:00',
                maxTime: '24:00:00',   
                defaultView: 'agendaWeek',
                eventDurationEditable: false,
                handleWindowResize: true,   
                height: $(window).height() - 100,   
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                allDaySlot: false,
                // header:false,
                columnFormat: {
                    month: 'ddd',
                    week: 'ddd D/M',
                    day: 'dddd D/M'
                },
                //events:  APP_URL+"/admin/show/schedules/get",
                eventSources: [
                    // your event source
                    {
                        url: APP_URL+"/admin/show/schedules/get",
                        type: 'GET',
                        data: {
                            'channel_id': channel_id
                        },
                        error: function() {
                            //alert('there was an error while fetching events!');
                        },
                        //color: 'yellow',   // a non-ajax option
                        //textColor: 'black' // a non-ajax option
                    }                    
                ],
                eventRender: function( event, element, view ) {
                    var title = element.find( '.fc-title' );
                    title.html( title.text() );
                },
                //fixedWeekCount: false,
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar !!!
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                drop: function(date) { $this.onDrop($(this), date); },
                select: function (start, end, allDay) { 
                    $this.onSelect(start, end, allDay); 
                },
                eventClick: function(calEvent, jsEvent, view) { 
                    $this.onEventClick(calEvent, jsEvent, view); 
                }
            });
            // Fetch current view
            var view       = $this.$calendar.fullCalendar('getView');
            var start_date = view.start.subtract(1, "days").format();
            var end_date   = view.end.format();

            /*******************************************************************************/

            /*$.ajax({
                type: "POST",
                url : APP_URL+"/admin/show/schedules/get",
                data: { 'start':start_date, 'end':end_date, 'channel_id':channel_id },
                success: function(response)
                {
                    // Display default show schedules in the calendar
                    $this.$calendar.fullCalendar('destroy');
                    callDefault($this, response.calendarEvents);
                }
            });*/

            //on new event
            /* this.$saveCategoryBtn.on('click', function(){
                var categoryName = $this.$categoryForm.find("input[name='category-name']").val();
                var categoryColor = $this.$categoryForm.find("select[name='category-color']").val();
                if (categoryName !== null && categoryName.length != 0) {
                    $this.$extEvents.append('<div class="external-event bg-' + categoryColor + '" data-class="bg-' + categoryColor + '" style="position: relative;"><i class="mdi mdi-checkbox-blank-circle m-r-10 vertical-middle"></i>' + categoryName + '</div>')
                    $this.enableDrag();
                }

            });*/
        },

        //init CalendarApp
        $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp
        
    }(window.jQuery),

    //initializing CalendarApp
    function($) {
        "use strict";
        $.CalendarApp.init()
    }(window.jQuery);


    function callDefault($this, defaultValues){
        
        $this.$calendarObj = $this.$calendar.fullCalendar({
            slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
            minTime: '00:00:00',
            maxTime: '24:00:00',   
            defaultView: 'agendaWeek',
            eventDurationEditable:false,
            handleWindowResize: false,   
            eventDurationEditable:true,
            height: $(window).height() - 100,   
            header: {
                right : 'prev,next today',
                center: 'title',
                left  : 'month,agendaWeek,agendaDay'
            },
            allDaySlot: false,
            columnFormat: {
                month: 'ddd',
                week: 'ddd D/M',
                day: 'dddd D/M'
            },
            events: defaultValues,
            eventAfterRender: function (event, element, view) {
                /*console.log(event);
                console.log(element);
                console.log(view);
                console.log('After');*/
                /*var $el = $(element), addUrl = $.mtsoft.url('admin/channel/schedules/add/' + $.ChannelId + '/' + event.start.toString('ddd') + '/' + ($.fullCalendar.formatDate(event.start, "HH:mm")).replace(/\:/ig, '')),
                        $a = $('<a class="cal-btn-add-show" title="Add Show" href="' + addUrl + '">' + $.mtsoft.ui.icon('add', {size: 16}, true) + '</a>');
                $el.parent().append($a);
                $a.css({
                    left: parseInt($el.css('left')) + $el.width() - $a.width() - 5,
                    top: parseInt($el.css('top')) + $el.height() - $a.height() - 5
                });
                //$el.find('.fc-event-title').after('<h2>test</h2>');

                if (event.data.Slots) { // mulitple shows defined for this slot         

                        // Build sub-slots representing all of assfined shows
                    for (var off in event.data.Slots) {
                        var slot = event.data.Slots[off];
                        $el.children('.fc-event-inner').children('div').filter(':last').after('<div class="sub-slot" style="background:' + slot.backgroundColor + ';border-color:' + slot.borderColor + ';">' + slot.title + '</div>');
                    }
                }*/
            },
            //fixedWeekCount: false,
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events
            selectable: true,
            drop: function(date) { $this.onDrop($(this), date); },
            select: function (start, end, allDay) { 
                $this.onSelect(start, end, allDay); 
            },
            eventClick: function(calEvent, jsEvent, view) { 
                $this.onEventClick(calEvent, jsEvent, view); 
            },
            dayClick: function(date, jsEvent, view) {
                return false;
            }
        });
    }

    // Get Replays on channel change
    $(document).on('change', '.scheduleSchedulesChannelsId', function(){
        getReplays($(this).val(), '', this);
    });

    $(document).on('change', '#ChannelSFId', function(){
        updateQueryStringParam('channel_id', $(this).val(), 'encore');
        $.CalendarApp.refresh();
    });

    // Fetch the shows acc. to the channel
    function getShows(channel_id, show_id=""){
        //Create shows select box
        var $el = $('#ScheduleShowsId');
        $el.empty(); // remove old options
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_channel_shows",
            data: { 'channel_id':channel_id },
            success: function(response)
            {
                console.log(response);
                $.each(response, function(key,value) {
                    if(show_id != ''){
                        if(key == show_id){
                            $el.append($("<option selected></option>").attr("value", key).text(value));
                        }else{
                            $el.append($("<option></option>").attr("value", key).text(value));
                        }
                    }else{
                        $el.append($("<option></option>").attr("value", key).text(value));
                    }
                   
                });
                $el.selectpicker('refresh');
            }
        });
    }

    // Fetch the replays
    function getReplays(channel_id, schedule_id='', that){
        //Create replay select box
        var $el2 = $('#ScheduleSchedulesId');
        var $el3 = $('#ScheduleSchedulesChannelsId');
       /* alert();
        console.log(that);
        var $el2 = $(that).closest('.schedule_replays').find('.scheduleSchedulesId');
        var $el3 = $(that);*/
        $el2.empty(); // remove old options
        $el3.empty(); // remove old options
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_shows_replays",
            data: { 'channel_id':channel_id },
            success: function(response)
            {               
                $.each(response.replays, function(key,value) {
                    if(schedule_id != ''){
                        if(schedule_id == value.id){
                            $el2.append($("<option selected='selected'></option>").attr("value", value.id).text(value.Schedule__name));
                        }else{
                            $el2.append($("<option></option>").attr("value", value.id).text(value.Schedule__name));
                        }                    
                    }else{
                        $el2.append($("<option></option>").attr("value", value.id).text(value.Schedule__name));
                    }
                });
                $el2.selectpicker('refresh');

                // Show selected option in the channels for replay
                $.each(response.channels, function(key,value) {
                    if(key == channel_id){
                        $el3.append($("<option selected='selected'></option>").attr("value", key).text(value));
                    }else{
                        $el3.append($("<option></option>").attr("value", key).text(value));
                    }
                    
                });
                $el3.selectpicker('refresh');
            }
        });
    }

    /* Fetch the schedule data by ID */
    function getScheduleData(id){
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/get_selected_schedule",
            data: { 'schedule_id':id },
            dataType: 'json',
            success: function(response)
            {
                //console.log(response);
                var channel_name = $('#ChannelSFId :selected').html();
                $('.modal-title').html('Add Episode - <span id="schedule_channel">'+channel_name+'</span>');
                $("#episode-modal").find("input[type='radio']").prop("checked",false);

                $("#episode-modal").find('#ScheduleStartDate').val(response.start_date);
                $("#episode-modal").find('#ScheduleEndDate').val(response.end_date);
                $("#episode-modal").find('#ScheduleTime').val(response.time);
                $("#episode-modal").find('.btn-switch').find("input[type='radio']").prop("checked", false);
                if(response.dow == "Sun"){
                    $("#episode-modal").find(".ScheduleDowSun").prop("checked", true); 
                    $("#episode-modal").find(".ScheduleDowSun").attr("checked", true); 
                }else if(response.dow == "Mon"){
                    $("#episode-modal").find(".ScheduleDowMon").prop("checked", true); 
                }else if(response.dow == "Tue"){
                    $("#episode-modal").find(".ScheduleDowTue").prop("checked", true); 
                }else if(response.dow == "Wed"){
                    $("#episode-modal").find(".ScheduleDowWed").prop("checked", true); 
                }else if(response.dow == "Thu"){
                    $("#episode-modal").find(".ScheduleDowThu").prop("checked", true); 
                }else if(response.dow == "Fri"){
                    $("#episode-modal").find(".ScheduleDowFri").prop("checked", true); 
                }else if(response.dow == "Sat"){
                    $("#episode-modal").find(".ScheduleDowSat").prop("checked", true); 
                }
                

                if(response.type_label == "live"){
                    $("#ScheduleTypeLabelLive").prop("checked", true); 
                }else if(response.type_label == "replay"){
                    $("#ScheduleTypeLabelReplay").prop("checked", true); 
                }

                if(response.type == "file_or_stream"){
                    $("#ScheduleTypeFileOrStream").prop("checked", true); 
                }else if(response.type == "live"){
                    $("#ScheduleTypeLive").prop("checked", true); 
                }else if(response.type == "replay"){
                    $("#ScheduleTypeReplay").prop("checked", true); 
                    $('.schedule_shows').hide();
                    $('.schedule_replays').show();
                }

                $("#ScheduleDuration").val(response.duration).selectpicker('refresh');
                $("#domain").val(response.stream);
                $('#channel_schedule_id').val(response.channels_id);
                $('#schedule_id').val(response.id);
                $('#ScheduleWeek').val(response.week);
                $('#ScheduleMonth').val(response.month);
                $('#ScheduleYear').val(response.year);

                $('#scheduleSaveBtn').html('Edit');
                $('#form_type').val('edit');
                
                getShows(response.channels_id, response.shows_id);
                getReplays(response.channels_id, response.schedules_id);
            }
        });
    }

    $(document).on('click', '.delete-event', function(event){
        
        event.preventDefault();
        var data = {
                    '_token' : csrf_token, 
                    'id':$('#schedule_id').val()
                };
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) { 
                makeAjaxRequest('POST', '/admin/channel/schedules/delete', data)
                .done(function(response) {
                    //console.log(response);
                    if(response.status == 'success'){
                        swal({
                           title: 'Deleted!', 
                           text: 'The schedule has been deleted successfully.',
                           type: 'success'
                        });
                        $('#episode-modal').modal('hide');
                     
                    }else{
                        swal({
                           title: 'Deleted!', 
                           text: 'The schedule could not be deleted.',
                           type: 'error'
                        });
                         $('#episode-modal').modal('hide');
                    }
                })
                .fail(function(xhr) {
                    console.log('error callback ', xhr);
                });
            }
        });
    });
}

$(document).on('click', '#openScheduleBtn', function(){
    $('#multiple-events-modal').modal('hide');
    //$('#modal').modal('toggle');
    setTimeout(function(){ 
       $('#event-modal').modal('show');
    },700);
    
    // Open the edit show modal
});
$(document).on('click', '#multiple-events-modal .sub-slot', function(){
    $('#multiple-events-modal').modal('hide');
    setTimeout(function(){ 
       $('#event-modal').modal('show');
    }, 700);
    
});

$(document).on('click', '.cal-btn-add-show', function(e){
    e.stopPropagation();
    // Remove the select2 selected options from the model
    $("#ScheduleWeek").val('0').trigger("change"); 
    $('.add_multiple_channels').find('.portlet').not(':first').remove();
    $('#multiple-events-modal').modal('hide');
    $('#event-modal').find('#ScheduleTime').val($(this).data('time'));
    //$('#event-modal').find('.modal-title').html('Add Show - '+$(this).data('channel'));
    $('#event-modal').find('.modal-title').html('Add Show');
    $('#event-modal').find('.radio-inline').find('input[type=radio]').attr('checked',false);
    $('#event-modal').find('#ScheduleTypeLabelLive').prop('checked',true);
    $('#event-modal').find('#ScheduleTypeFileOrStream').prop('checked',true);
    $('#event-modal').find('option:selected', $('#ScheduleShowsId')).removeAttr('selected').selectpicker('refresh');
    $('#event-modal').find('option:selected', $('#ScheduleDuration')).removeAttr('selected').selectpicker('refresh');

    var get_day = $(this).data('day');
    if(get_day == 'Sunday'){
        $("#event-modal").find(".ScheduleDowSun").prop("checked", true); 
    }else if(get_day == 'Monday'){
        $("#event-modal").find(".ScheduleDowMon").prop("checked", true); 
    }else if(get_day == 'Tuesday'){
        $("#event-modal").find(".ScheduleDowTue").prop("checked", true); 
    }else if(get_day == 'Wednesday'){
        $("#event-modal").find(".ScheduleDowWed").prop("checked", true); 
    }else if(get_day == 'Thursday'){
        $("#event-modal").find(".ScheduleDowThu").prop("checked", true); 
    }else if(get_day == 'Friday'){
        $("#event-modal").find(".ScheduleDowFri").prop("checked", true); 
    }else if(get_day == 'Saturday'){
        $("#event-modal").find(".ScheduleDowSat").prop("checked", true); 
    }
    var channel_id = $('#ChannelSFId :selected').val();
    $('#event-modal').find('#scheduleChannels').val(channel_id).select2();
    $('#event-modal').find('#ScheduleStartDate').val('');
    $('#event-modal').find('#ScheduleEndDate').val('');
    $('#event-modal').find('#domain').val('');
    $('#event-modal').find('#scheduleSaveBtn').html('Add');
    $('#event-modal').find('#scheduleSaveBtn').addClass('plus_schedule');
    $('#event-modal').find('.delete-event').hide();
    $('#event-modal').find('#channel_schedule_id').val(channel_id);
    $('#event-modal').find('#schedule_id').val(0);
    $('#event-modal').find('#form_type').val('add');

    // Show the add more channels button on edit screen
    $('#showAddMoreChannels').show();

    getShows(channel_id);
    getReplays(channel_id);
    //$('#event-modal').find('#schedule_channel').html($('#ChannelSFId :selected').html());
    $('#event-modal').modal('show');
    //$('#calendar').fullCalendar('select', '2019-03-13');
    //CalendarApp.dayClick();
    
});

$(document).on('click', '.plus_schedule', function(event) {
    
    if($('#event-modal').find('#form_type').val() == "add"){
        var form = $('#ScheduleAdminAddForm');
        event.stopImmediatePropagation();
        var error = 0;
        $( ".portlet" ).each(function( index, elem ) {                   
            
            // Check weekday is selected or not
            var checked = $(elem).find(".weekdays:checkbox:checked").length > 0;
            if (!checked){
                swal({
                   title: '', 
                   text: 'Please select at least one week day',
                   type: 'warning'
                });
                error = 1;
                return false;
            }else{
                error = 0;
            }

            // Check start date are filled or not                    
            if(!$(elem).find(".scheduleStartDate").val()){
                swal({
                   title: '', 
                   text: 'Please select the start date',
                   type: 'warning'
                });
                error = 1;
                return false;
            }else{
                error = 0;
            }

            // Check end date are filled or not                    
            if(!$(elem).find(".scheduleEndDate").val()){
                swal({
                   title: '', 
                   text: 'Please select the end date',
                   type: 'warning'
                });
                error = 1;
                return false;
            }else{
                error = 0;
            }

            // Check at least for the weeks selected
            var weeks = $(elem).find(".scheduleWeek");
            if (weeks.val() == null || weeks.val() == '') {
                swal({
                   title: '', 
                   text: 'Please select the weeks',
                   type: 'warning'
                });
                error = 1;
                return false;
            } else{
                error = 0;
            }                                                 
        });
        if(error == 1){
            //alert();
            return false;
        }



        var form_data = form.serialize();
        $.ajax({
            type: "POST",
            url : APP_URL+"/admin/channel/schedules/add",
            data: { 'data':form_data },
            success: function(response)
            {
                $.CalendarApp.refresh();
            }
        });

        $('#event-modal').modal('hide');
        event.preventDefault();
        return false;   
    }
});

/*function openAddShowModal(){
    $('#multiple-events-modal').modal('hide');
    $('#event-modal').modal('show');
    CalendarApp.prototype.init();
}*/