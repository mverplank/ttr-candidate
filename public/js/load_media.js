/**********  Load more media on scroll down start *******************/ 	

	/*var nextpage   = 2;
	var totalpages = $('#media_type').data('total_media');
	var pageArray  = [];*/
	
	$(window).scroll(function(){
		if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            if(typeof totalpages  !== "undefined"){
                if(totalpages >= nextpage){
                    getresult(nextpage);
                }
            }		
		}
	});

	// Fetch the more media in modal popup scrolling
	//if($('#myModal').length > 0) {	
        // $("#idOfUl").on( 'scroll', function(){
		$('#myModal').find('#media_append_section').on('scroll', function() {
        //$(document).on('scroll', '#media_append_section', function() {
        //$('#media_append_section').on('scroll', function() {
        //$('#myModal').scroll(function() {
			var totalpages = $('.media_module_type').data('total_media');
            //console.log(totalpages);
            console.log($(this).scrollTop() + $(this).innerHeight() )
            console.log(this.scrollHeight);
			if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
                //console.log('yes yes ');
                console.log(totalpages);
                console.log(nextpage);
			    if(totalpages >= nextpage){
                    console.log('START');
					getModuleResult(nextpage);
				}
			}
		});
	//}
	
	
	//code for increasing view as per scrool down end
	function getresult(pageno) {
		if (pageArray.indexOf(pageno) == -1) { 
			pageArray.push(pageno);
			var date = $('.filterMediaByDate').val();
			var dimension = $('#allMediaDimension').val();
			var type = $('.tab-pane.active').attr('id');
			var title = $('#valueMediaByTitle').val();
            //var title = $('#valueMediaByTitle').val();

			$.ajax({
				url: APP_URL+'/admin/cms/media/get_more_media',
				type: "POST",
				data:  {'page':pageno, 'type':type, 'date':date, 'title':title, 'dimension':dimension},
				beforeSend: function(){
					$("#"+type).find('#media_loading_image_gif').show();
				},
				complete: function(){
					$("#"+type).find('#media_loading_image_gif').hide();
				},
				success: function(data){
					$("#"+type).find("ul.jFiler-items-list.jFiler-items-grid").append(data);
					//$("#media_append_section").find('ul.jFiler-items-list').append(data);
					nextpage++;					
				},				
				error: function(){
                    $('#media_loading_image_gif').hide();
                }					
			}); 
		}
	}

	function getModuleResult(pageno) {
        /*console.log("tahh "+pageno);
        console.log("pageArray "+pageArray);*/
		if (pageArray.indexOf(pageno) == -1) { 
			pageArray.push(pageno);
			var date      = $('.filterMediaByDate').val();
			//var dimension = $('#allMediaDimension').val();
            // Changes made on 13-05-2019 
            var dimension = $('#mediaDimension').val();
            var title = $('#valueMediaByTitle').val();
			$.ajax({
				url: APP_URL+'/admin/cms/media/get_more_media',
				type: "POST",
				data:  {'page':pageno, 'title':title, 'type':$('.media_module_type').data('type'), 'date':date, 'dimension':dimension, 'page_content':'popup'},
				beforeSend: function(){
					$('#media_loading_image_gif').show();
				},
				complete: function(){
					$('#media_loading_image_gif').hide();
				},
				success: function(data){
					$("#media_append_section").find('ul.jFiler-items-list.jFiler-items-grid').append(data);
					nextpage++;					
				},				
				error: function(){
                    $('#media_loading_image_gif').hide();
                }					
			}); 
		}
	}
	
	//Edit Media Image - mainly do cropping and save the cropped image into existing one
	function editMediaImageOnly(){
		
		var modalTemplate = '<div id="custom-modal" class="modal-demo modal">'
                +'<button type="button" class="close" onclick="$(this).closest(&quot;#custom-modal&quot;).modal(&quot;hide&quot;);$(&quot;.modal-backdrop.in&quot;).remove();$(&quot;body&quot;).removeClass(&quot;remove_scroll&quot;);">'
                    +'<span>&times;</span><span class="sr-only">Close</span>'
                +'</button>'
                +'<h4 class="custom-modal-title">Edit Image</h4>'
                +'<div class="custom-modal-text">'
                   +'<!-- Content -->'
                    +'<div class="container">'
                        +'<div class="row">'
                            +'<div class="col-md-12">'
                                +'<div class="image-container">'
                                    +'<img id="image" src="" alt="Picture" style="width:100%">'
                                +'</div>'
                            +'</div>'
                            
                        +'</div>'
                        +'<div class="row" style="margin-top: 10px;">'
                            +'<div class="col-md-12 docs-buttons">'
                                +'<div class="btn-group">'
                                    +'<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">'
                                            +'<span class="fa fa-search-plus"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">'
                                            +'<span class="fa fa-search-minus"></span>'
                                        +'</span>'
                                    +'</button>'
                                +'</div>'

                                +'<div class="btn-group">'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, -10, 0)">'
                                            +'<span class="fa fa-arrow-left"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 10, 0)">'
                                            +'<span class="fa fa-arrow-right"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, -10)">'
                                            +'<span class="fa fa-arrow-up"></span>'
                                        +'</span>'
                                    +'</button>'
                                    +'<button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, 10)">'
                                            +'<span class="fa fa-arrow-down"></span>'
                                       +'</span>'
                                    +'</button>'
                                +' </div>'

                                +'<div class="btn-group">'
                                    +'<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">'
                                            +'<span class="fa fa-rotate-left"></span>'
                                        +'</span>'
                                +'</button>'
                              +'<button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">'
                               +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">'
                                  +'<span class="fa fa-rotate-right"></span>'
                                +'</span>'
                              +'</button>'
                           +' </div>'

                            +'<div class="btn-group">'
                              +'<button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleX&quot;, -1)">'
                                  +'<span class="fa fa-arrows-h"></span>'
                                +'</span>'
                              +'</button>'
                              +'<button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleY&quot;, -1)">'
                                  +'<span class="fa fa-arrows-v"></span>'
                                +'</span>'
                              +'</button>'
                            +'</div>'

                           +'<div class="btn-group">'
                              +'<button type="button" class="btn btn-primary" data-method="crop" title="Crop">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;crop&quot;)">'
                                  +'<span class="fa fa-check"></span>'
                                +'</span>'
                              +'</button>'
                              +'<button type="button" class="btn btn-primary" data-method="clear" title="Clear">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;clear&quot;)">'
                                  +'<span class="fa fa-remove"></span>'
                                +'</span>'
                              +'</button>'
                            +'</div>'

                            +'<div class="btn-group">'
                              +'<button type="button" class="btn btn-primary" data-method="reset" title="Reset">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;reset&quot;)">'
                                  +'<span class="fa fa-refresh"></span>'
                                +'</span>'
                              +'</button>'
                              +'<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">'
                                +'<input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Import image with Blob URLs">'
                                  +'<span class="fa fa-upload"></span>'
                                +'</span>'
                              +'</label>'
                             
                            +'</div>'

                            +'<div class="btn-group btn-group-crop">'
                                +'<button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" data-image_for="" data-module="" data-form_type="add" data-crop="1" id="getCropped" data-old_image="" data-outer_dropzone="HostPhoto0" data-dropzone_obj="">'
                                    +'<span class="docs-tooltip crop-upload" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { maxWidth: 4096, maxHeight: 4096 })">'
                                        +'Apply'
                                    +'</span>'
                                +'</button>'
                            +'</div>'

                            +'<!-- Show the cropped image in modal -->'
                            +'<div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">'
                              +'<div class="modal-dialog">'
                                +'<div class="modal-content">'
                                  +'<div class="modal-header">'
                                    +'<h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>'
                                    +'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                                      +'<span aria-hidden="true">&times;</span>'
                                    +'</button>'
                                  +'</div>'
                                  +'<div class="modal-body"></div>'
                                  +'<div class="modal-footer">'
                                    +'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
                                    +'<a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>'
                                  +'</div>'
                                +'</div>'
                              +'</div>'
                            +'</div><!-- /.modal -->'
                          +'</div><!-- /.docs-buttons -->'

                         +'<div class="col-md-12 docs-toggles">'
                            +'<!-- <h3>Toggles:</h3> -->'
                            +'<div class="btn-group d-flex flex-nowrap" data-toggle="buttons">'
                              +'<label class="btn btn-primary active">'
                                +'<input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">'
                                  +'16:9'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                                +'<input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">'
                                 +'4:3'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                                +'<input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">'
                               +' <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">'
                                 +' 1:1'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                               +' <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">'
                               +' <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">'
                                +'  2:3'
                                +'</span>'
                              +'</label>'
                              +'<label class="btn btn-primary">'
                                +'<input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">'
                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">'
                                  +'Free'
                                +'</span>'
                              +'</label>'
                            +'</div>'

                          +'</div><!-- /.docs-toggles -->'
                        +'</div>'
                      +'</div>'
                +'</div>'
            +'</div>'
            +'<!-- End Modal -->';

        // Dynamically create modals to allow multiple files processing
        var $cropperModal = $(modalTemplate);

        // 'Crop and Upload' button in a modal
        var $uploadCrop = $cropperModal.find('.crop-upload');
        var $img = $('<img />');       
        $img.attr('src',  $('.media-size').find('img').attr('src'));
        $cropperModal.find('.image-container').html($img);
       

		if($img.attr('src').length > 0){
			$('#myModal').modal('toggle');
			$('.modal-backdrop.in').remove();
			initializeCropper($img);
        	$cropperModal.modal('show');
        	$('body').addClass('remove_scroll');
		}
        var cachedFilename = 'Test title';
         
	    // listener for 'Crop and Upload' button in modal
	    $uploadCrop.on('click', function() {
	        //var canvas = $(".cropper-canvas");
	        //var trimmedCanvas = trimCanvas($img.cropper('getCroppedCanvas'));

	        // get cropped image data
	        var blob = $img.cropper('getCroppedCanvas').toDataURL();
	        //var blob = trimmedCanvas.toDataURL();
	        // transform it to Blob object

	        var newFile = dataURItoBlob(blob);
	        $('#image').attr('src',newFile);
	        // set 'cropped to true' (so that we don't get to that listener again)
	        newFile.cropped = true;
	        // assign original filename
	        newFile.name = cachedFilename;
	      
	        //Edit the image using ajax
	 		$img.cropper('getCroppedCanvas').toBlob(function (blob) {

	 			//Make dynamic form
	            var formData = new FormData();
	            formData.append('croppedImage', blob);
	            formData.append('id', $('#MediaId').val());
	            formData.append('file_name', $('#mediaFile').val());

	            var url  = APP_URL+"/admin/cms/media/add/image";
	            $.ajax(url, {
	                method: "POST",
	                data: formData,
	                processData: false,
	                contentType: false,
	                success: function (response) {
	                   	$('#image').find('[data-id="'+response.data.id+'"]').find('.jFiler-item-thumb-image').find('img').removeAttr('src');
	                   	$('#image').find('[data-id="'+response.data.id+'"]').find('.jFiler-item-thumb-image').find('img').attr('src',response.data.new_src);

	                   	swal({
			               title: 'Successfully Updated!', 
			               text: 'Media has been updated successfully.',
			               type: 'success'
			            });

			            $('html, body').animate({
			                  scrollTop: $('#image').find('[data-id="'+response.data.id+'"]').offset().top-300,
			                },
			                500, 'linear'
			            );
	                },
	                error: function () {
	                    console.log('Upload error');
	                }
	            });
	        });
	        $cropperModal.modal('hide');
	        $('.modal-backdrop.in').remove();
	        $('body').removeClass('remove_scroll');

	    });
	}
