$(document).ready(function(){
	/*On input element hover activate its help block*/
	$('.form_ui_input').on('focus', function(e){
		/*Hide Other help block Before Showng Current Acitve Element*/
		hideHelpBlock();
		checkWasHavingError();
		$(this).addClass('form-control-warning');
		$(this).parent().find('.help-block').addClass('help-block-active');
		// Remove Error class From Input and Hide Error Message Block
		$(this).parent().parent().parent().find('.error-block').hide();
		$(this).removeClass('parsley-error');
		//Add A Class so that can rememner input having Error
		$(this).addClass('was-having-error');
	})

	// On Hover Checkbox element help block Active
	$('.checkbox_button').mouseover(function() {
	   $(this).parent().find('.help-block').addClass('help-block-active');
	}).mouseout(function() {
	   $(this).parent().find('.help-block').removeClass('help-block-active');
	});

	// On Hover Add files elemtnt help block Active
	$('.jFiler').mouseover(function() {
	   $(this).parent().find('.help-block').addClass('help-block-active');
	}).mouseout(function() {
	   $(this).parent().find('.help-block').removeClass('help-block-active');
	});

	//Input Keyup Form Validation
	$("input").focusout(function(){
		var cur_obj = $(this); 
		if($(this).data('validation') == '1'){
			if($(this).data('maxlength') != undefined){
				var check = maxLength(cur_obj.val(), $(this).data('maxlength'));
				if(!check){
					addErrorToElement(cur_obj, 'Max Size Limit Excceded');
				}

			}
			if($(this).data('unique') == '1'){
				uniqueElementValue(cur_obj);
			}
		}
	});

	//submit form Button Click
	$('.submit_form').on('click', function(){
		var form_id = $(this).data('form-id');
		var first = '';
		$("form#"+form_id+" :input").each(function(i){
			if($(this).hasClass('required')){
				if($(this).val() == ''){
					$(this).parent().parent().parent().find('.error-block').show();
					$(this).addClass('parsley-error');
					if(first == ''){
						first = $(this);
					}
				}
			}
		});
		if(first != ''){
			$("html, body").animate({
		        scrollTop: first.offset().top - 90 
		    }, 1000);
		}else{
			$('#'+form_id).submit();
		}
	})
})

//Validate Element For Unique Value
var uniqueElementValue = function($this){
	var details = _getInputDetails($this);
	var name    = $this.val();
	if(name != ''){
		var data = {'details':details, 'value':name, 'type':'unique'};
		makeAjaxRequest('POST', '/admin/forms/validate_input', data)
		.done(function(response) {
		    if(response.status == 'error'){
              	if(response.alert){
        			$this.parent().parent().parent().find('.error-block').show();
        			$this.addClass('parsley-error');
        			$this.parent().parent().parent().find('.error-block p').html(response.alert);
        		}
            }
		})
		.fail(function(xhr) {
		   	console.log('error callback ', xhr);
		});
	}
}

var requiredElementValue = function($this){
	var min = $this.data('min');
	var max = $this.data('max');
}

// Hide Help Block Of Other Element Other Then Active
var hideHelpBlock = function(){
	$(".help-block").each(function() {
		$(this).removeClass('help-block-active');
	});
}

//Check Element Having Error Validate Them Again Before Moving Other Element
var checkWasHavingError = function(){
	$("form :input").each(function(i){
		if($(this).hasClass('was-having-error')){
			if($(this).val() == ''){
				$(this).parent().parent().parent().find('.error-block').show();
				$(this).addClass('parsley-error');
				$(this).removeClass('was-having-error');
			}
		}
	});
}

//Add Error To Element And Chnage The Error Message If Provided
var addErrorToElement = function($this, msg){
    $this.parent().parent().parent().find('.error-block p').html(msg);
    $this.parent().parent().parent().find('.error-block').show();
    $this.addClass('parsley-error');
    return false;
}

//Get input details: model, fieldname, attributes: name, id
var _getInputDetails = function ($this) {
    // get model and field name
    var n = $this.attr('name'), // data[Model][field_name]
            rexp = new RegExp(/data\[([^\]]+)\]\[([^\]]+)\]/gi), // rip model and fieldname
            matches = rexp.exec(n);
    return {model: matches[1], fieldname: matches[2], name: n, id: $this.attr('id')};
}

var notEmpty = function(v) {            
    return !((v === null) || (v.length === 0));
}

var minLength = function(v, min) {
    return v.length >= min;
}

var maxLength = function(v, max) {
    if(v.length > max){
    	return false;
    }
}

var between = function(v, min, max) {
    return v.length >= min && v.length <= max;
}

var valRange = function(v, min, max) {
    var r = true, min = parseFloat(min), max = parseFloat(max);
    if (min) {
        r = v >= min;
    }

    if (max) {
        r = r && v <= max;
    }
    
    return r;
}

var numeric = function(v) {
    var r = /^([0-9\.]+)$/;
    return r.test(v);
}

var alphaNumeric = function(v) {
    var r = /^([0-9a-z]+)$/i;
    return r.test(v);
}

var url = function(v) { // http(s), ftp, media protocols
    var r = /^(https?|ftp|mms|rtsp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
    //var r = /^\S$|^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/; // short way            
    return r.test(v);
}

/**
 * Apply regex test
 *
 * @param String strExp     regular expression AS STRING or JS regex!
 *                              Must contain leading and ending slash (/)
 *                              If any flags - should be placed directly after last slash (/)
 *                              ex.  /^[0-9a-zA-Z]+$/i
 */
var regex = function(v, strRexp, flags) {
    var r;
    if (typeof(strRexp) === 'string') {

        var ch = strRexp.split("/"), rexp;
        if (ch[ch.length - 1] !== '') { // flags given in regex string

            flags = ch[ch.length - 1];
            rexp = strRexp.slice(1).slice(0, -(flags.length + 1)); // remove first and last slahs and flags

        } else {    // no flags in regex string

            rexp = strRexp.slice(1).slice(0, -1);
        }
        r = new RegExp(rexp, flags);
    } else {

        r = strRexp;
    }
    try {
        return r.test(v);
    } catch (err) {
        return true;
    }
}

//
// special types
//
var email = function(v, strict) {
    var r = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

    if (strict === undefined || strict) {   // only allowed: user@host.ext

        return r.test(v);

    } else {    // allowed:  user@host.ext OR User Name <user@host.ext>

        if (!r.test(v)) {

            _r = /([^\ <]+\@[^\ >]+)/;   // find first email address
            var emails = _r.exec(v);
            return emails ? r.test(emails[0]) : false; // check against first found email address
        } else {

            return true;
        }
    }

}

var password = function(v) {
    // Password must contain 8 characters and at least one number, one letter and one unique character such as !#$%&? "
    //var r = /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/;
    var r = /^.*(?=.{4,}).*$/; // minimum 4 characters
    return r.test(v);
}

//
// files
//
var filesCount = function(v, min, max) {
    if (max !== undefined) {
        return v.filesCount >= min && v.filesCount <= max;
    } else {
        return v.filesCount >= min;
    }
}

var fileType = function(v, allowedTypes) {
    for (var i in v.files) {
        if ($.inArray(v.files[i].type, allowedTypes) < 0) {
            return false;
        }
    }
    return true;
}

var fileSize = function(v, min, max) { // min & max sizes in [ MB ]
    if (max !== undefined) {

        for (var i in v.files) {
            var sizeMB = (v.files[i].size / 1048576).toFixed(1);
            if (sizeMB < min || sizeMB > max) {
                return false;
            }
        }
    } else {
        for (var i in v.files) {
            var sizeMB = (v.files[i].size / 1048576).toFixed(1);
            if (sizeMB < min) {
                return false;
            }
        }
    }
    return true;
}

var time = function(v) {
    // check date value is valid 
    var _d = Date.parse(v);
    if (_d || $.trim(v) === '') { // valid syntax 
        return true;
    } else {
        return false;
    }
}

/**
 * Check time is between two values
 * @param {type} v
 * @param {type} format
 * @param {type} min
 * @param {type} max
 * @returns {Boolean}
 */
var timeBetween = function(v, min, max) {
    if ($.trim(v) === '') {
        return true;
    }
    // check date value is valid 
    var _t = Date.parse(v);
    if (_t) { // valid syntax 
        var t = _t.toString('HH:mm:ss');
        // min 
        if (min !== undefined) {
            if (min > t) {
                return false;
            }
        }
        // max 
        if (max !== undefined) {
            if (t > max) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

var date = function(v) {
    // check date value is valid 
    var _d = Date.parseExact(v, 'yyyy-MM-dd');
    if (_d || $.trim(v) === '') { // valid syntax 
        return true;
    } else {
        return false;
    }
}

/**
 * Check date is between two values
 * @param {type} v
 * @param {type} format
 * @param {type} min
 * @param {type} max
 * @returns {Boolean}
 */
var dateBetween = function(v, format, min, max) {
    if ($.trim(v) === '') {
        return true;
    }
    // check date value is valid 
    var _d = Date.parseExact(v, format);
    if (_d) { // valid syntax 
        var d = _d.toString('yyyy-MM-dd');
        // min 
        if (min !== undefined) {
            if (min > d) {
                return false;
            }
        }
        // max 
        if (max !== undefined) {
            if (d > max) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

/**
 * Check period is valid (stop AFTER start time) 
 * @param {type} v
 * @param {type} endTimeNodeName
 * @param {Boolean} allowSingle     allow single day selection (otherwise minimum 2 days)
 * @returns {Boolean}
 */
var isPeriod = function(v, endTimeNodeName, allowSingle, unit) {
    if ($.trim(v) === '') {
        return true;
    }
    if (unit === 'day') {
        var start = Date.parseExact(v, 'yyyy-MM-dd'), stop = Date.parseExact($('[name="' + endTimeNodeName + '"]').val(), 'yyyy-MM-dd');
    } else {
        var start = Date.parse(v), stop = Date.parse($('[name="' + endTimeNodeName + '"]').val());
    }

    if (start && stop) {
        if (stop.isAfter(start) || (allowSingle && (stop.isAfter(start) || Date.equals(start, stop)))) { // valid syntax 
        return true;
    } else {
        return false;
        }
    } else {
        return true;
    }
}

/**
 * Check time period is between two values
 * @param {type} v
 * @param {type} format
 * @param {type} min
 * @param {type} max
 * @returns {Boolean}
 */
var periodBetween = function(v, endTimeNodeName, unit, minLen, maxLen) {
    if ($.trim(v) === '') {
        return true;
    }
    var start, stop, period;
    // check date value is valid
    if (unit === 'day') {
        start = Date.parseExact(v, 'yyyy-MM-dd');
        stop = Date.parseExact($('[name="' + endTimeNodeName + '"]').val(), 'yyyy-MM-dd');
    } else {
        start = Date.parse(v);
        stop = Date.parse($('[name="' + endTimeNodeName + '"]').val());
    }
    if (start && stop) { // valid syntax 
        if (unit === 'day') {
            period = Math.floor(((stop.getTime() - start.getTime()) / 1000) / 60 / 60 / 24 + 1);
        } else {
            period = ((stop.getTime() - start.getTime()) / 1000) / 60;
        }
        // min 
        if (minLen !== undefined) {
            if (minLen > period) {
                return false;
            }
        }
        // max 
        if (maxLen !== undefined) {
            if (period > maxLen) {
                return false;
            }
        }
        return true;
    } else {
        return true;
    }
}