$(document).ready(function() {
	$("input[type='password']").not('#current_password, #confirm_password').keyup(function() {
		$('#result').html(checkStrength($(this).val()))
	})
	function checkStrength(password) {
		var strength = 0
		if (password.length <= 2) {
			$('#result').removeClass()
			$('#result').addClass('passwordstrength')
			return 'Quality 0% (use letters and numbers)'
		}
		if (password.length < 6) {
			$('#result').removeClass()
			$('#result').addClass('short')
			$('#result').css({'background-size':'10% auto'});
			return 'Quality 10% (use letters and numbers)'
		}
		if (password.length > 7) strength += 1
		// If password contains both lower and uppercase characters, increase strength value.
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
		// If it has numbers and characters, increase strength value.
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
		// If it has one special character, increase strength value.
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		// If it has two special characters, increase strength value.
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		// Calculated strength value, we can return messages
		// If value is less than 2
		if (strength == 1) {
			$('#result').removeClass()
			$('#result').addClass('weak')
			$('#result').css({'background-size':'30% auto'});
			return 'Quality 30% (use lower and uppercase characters)'
		}else if (strength < 2) {
			$('#result').removeClass()
			$('#result').addClass('good')
			$('#result').css({'background-size':'50% auto'});
			return 'Quality 50% (use special charecters)'
		}else if (strength == 2) {
			$('#result').removeClass()
			$('#result').addClass('good')
			return 'Quality 60% (use special charecters)'
			$('#result').css({'background-size':'60% auto'});
		}else if (strength == 3) {
			$('#result').removeClass()
			$('#result').addClass('good')
			$('#result').css({'background-size':'80% auto'});
			return 'Quality 80% (Strong pasword)'
		}else if (strength == 4) {
			$('#result').removeClass()
			$('#result').addClass('strong')
			$('#result').css({'background-size':'90% auto'});
			return 'Quality 90% (Strong pasword)'
		}
		else {
			$('#result').removeClass()
			$('#result').addClass('strong')
			$('#result').css({'background-size':'100% auto'});
			return 'Quality 100% (Strong pasword)'
		}
	}
});