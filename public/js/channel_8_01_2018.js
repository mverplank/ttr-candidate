$(document).ready(function() {
    Dropzone.autoDiscover = false;
    $("form div .dropzone").each(function(i){
        var file_limit  = $(this).data('limit');
        var image_for   = $(this).data('image-for');
        var pre_element = $(this).data('preview-ele');
        var element_id  = $(this).attr('id');
        var module      = $(this).data('module');
        var form_type   = $('#form_type').val();
        if(form_type == 'edit'){
            var item_id     = $('#edit_id').val();
        }else{
            var item_id     = 0;
        }
        
        var myDropzone  = new Dropzone("#"+element_id, {
            url: APP_URL+"/admin/uploads/upload",
            paramName: "image", // The name that will be used to transfer the file
            autoProcessQueue: true,
            maxFilesize: 5, // MB
            parallelUploads: 4,
            uploadMultiple: true,
            params: {'image_for': image_for, 'module': module, 'form_type': form_type, 'item_id': item_id, 'element_id': element_id},
            autoQueue: true,
            acceptedFiles: "image/*",
            previewsContainer: pre_element,
            maxFiles: file_limit,
            maxfilesexceeded: function(file) {
                alert('You Can Upload Only '+this.options.maxFiles+' Files....!');
                this.removeFile(file);
            },
            init: function() { 
                $('.sortable').sortable('enable');
                // Capture the Dropzone instance as closure.
                var myDropzone = this;
                //if(form_type == 'edit'){
                    var data = {'module' : module, 'folder' : image_for, 'id' : myDropzone.options.params.item_id, 'form_type': form_type};
                    makeAjaxRequest('POST', '/admin/uploads/get_files', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            $.each(response.data, function( index, value ) {
                                var mockFile = response.data[index];
                                // Call the default addedfile event handler
                                myDropzone.emit("addedfile", mockFile);
                                // And optionally show the thumbnail of the file:
                                myDropzone.emit("thumbnail", mockFile, response.data[index].url);
                                myDropzone.files.push(mockFile);
                            });
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                //}
                makeDropZoneSortable(myDropzone.options.params.element_id);
                this.on("addedfile", function(file) {
                    
                    //console.log(file.name);
                    // Create the remove button
                    $('.dz-progress').show();
                    $('.dz-message').show();
                    $('.dz-size').hide();
                    $('.dz-filename').show();
                    $('.dz-error-mark').show();
                    $('.dz-remove').show();
                    $('.dz-success-mark').show();
                    var total_count = myDropzone.files.length;
                    /*if(total_count > file_limit ){
                        alert('You Can Upload Only'+file_limit+' Files....!');
                        this.removeFile(file);
                    }*/
                    var removeButton = Dropzone.createElement("<button class='btn-danger btn-block' title='Remove'><i class='fa fa-trash' aria-hidden='true'></i></button>");
                   
                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();
                        
                        if(file.xhr != undefined){
                            $response = JSON.parse(file.xhr.response);
                            var data = {'file_path': $response.path, 'ajax': true};
                        }else{
                            var data = {'file_path' : file.path, 'ajax': false};
                        }
                        //first remove the file from server
                        makeAjaxRequest('POST', '/admin/uploads/delete_files', data)
                        .done(function(response) {
                            if(response.status == 'success'){
                                //Remove the file preview.
                                myDropzone.removeFile(file);
                            }
                        })
                        .fail(function(xhr) {
                            console.log('error callback ', xhr);
                        });
                    });
                    file.previewElement.appendChild(removeButton);
                });
                
                this.on('error', function(file, errorMessage) {
                    var mypreview = document.getElementsByClassName('dz-error');
                    mypreview = mypreview[mypreview.length - 1];
                    mypreview.classList.toggle('dz-error');
                    mypreview.classList.toggle('dz-success');
                });
                
                /*var submitButton = document.querySelector("#add_album_images")
                submitButton.addEventListener("click", function(e) { 
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue(); 
                    var files = myDropzone.files;
                });
                */
                this.on('sendingmultiple', function (data, xhr, formData, response) {
                    formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    formData.append("image_for", myDropzone.options.params.image_for);
                    formData.append("module", myDropzone.options.params.module);
                    formData.append("form_type", myDropzone.options.params.form_type);
                    formData.append("item_id", myDropzone.options.params.item_id);
                });
                
                this.on('success', function (file, responseText) {
                    console.log(responseText);
                    if(responseText.upload == 'success')
                    {
                        myDropzone.removeFile(file);
                        var mockFile = responseText.data;
                        // Call the default addedfile event handler
                        myDropzone.emit("addedfile", mockFile);
                        // And optionally show the thumbnail of the file:
                        myDropzone.emit("thumbnail", mockFile, responseText.data.url);
                        myDropzone.files.push(mockFile);
                    }
                });
            },
        });
    });

    //sort images
    /*$("form div .dropzone").each(function(i){
        var element_id  = $(this).attr('id');
        $("#"+element_id).sortable({
            items:'.dz-preview',
            cursor: 'move',
            opacity: 0.5,
            containment: "parent",
            distance: 20,
            tolerance: 'pointer',
            update: function(e, ui){
                console.log(e);
                //console.log(e.target.dropzone.options.params);
               //console.log(e);
                //console.log(e.target.dropzone.element.id);
                //console.log(e.target.dropzone.files);
                //console.log(ui.items);
                //do what you want
                // on the webpage search for all the images that have been uploaded
                var element_id    = e.target.dropzone.element.id;
                var imageTags     = $('#'+element_id).children().find('.dz-image-preview');
                var current_queue = [];
                //iterate through all the images that have been uploaded by the user
                imageTags.each(function (index, imageTag) {
                    var file_name = $(this).children().children().find('span').text();
                    current_queue[index] = file_name;
                });
                var params = e.target.dropzone.options.params;
                var data = {'module' : params.module, 'folder' : params.image_for, 'id' : params.item_id, new_order: current_queue};
                makeAjaxRequest('POST', '/admin/uploads/sort_files', data)
                .done(function(response) {
                    console.log(response);
                    if(response.status == 'success'){
                       
                    }
                })
                .fail(function(xhr) {
                    console.log('error callback ', xhr);
                });
            }
        });
    });*/
});

function createDropZoneInstance(element_id){
    
}

function makeDropZoneSortable(element_id){
    $("#"+element_id).sortable({
        items:'.dz-preview',
        cursor: 'move',
        opacity: 0.5,
        containment: "parent",
        distance: 20,
        tolerance: 'pointer',
        update: function(e, ui){
            console.log(e);
            //console.log(e.target.dropzone.options.params);
           /* console.log(e);
            console.log(e.target.dropzone.element.id);
            console.log(e.target.dropzone.files);
            console.log(ui.items);*/
            //do what you want
            // on the webpage search for all the images that have been uploaded
            var element_id    = e.target.dropzone.element.id;
            var imageTags     = $('#'+element_id).children().find('.dz-image-preview');
            var current_queue = [];
            //iterate through all the images that have been uploaded by the user
            imageTags.each(function (index, imageTag) {
                var file_name = $(this).children().children().find('span').text();
                current_queue[index] = file_name;
            });
            var params = e.target.dropzone.options.params;
            var data = {'module' : params.module, 'folder' : params.image_for, 'id' : params.item_id, new_order: current_queue};
            makeAjaxRequest('POST', '/admin/uploads/sort_files', data)
            .done(function(response) {
                console.log(response);
                if(response.status == 'success'){
                   
                }
            })
            .fail(function(xhr) {
                console.log('error callback ', xhr);
            });
        }
    });
}