
$(document).ready(function() {
    Dropzone.autoDiscover = false;
    /*$("form div .dropzone").each(function(i){
        createDropZoneInstance(this);
    });*/
    var myDropzone = '';
    $("div .dropzone").each(function(i){
        createDropZoneInstance(this);
    });

});

function createDropZoneInstance(obj, type=''){
   /* console.log('again');
    console.log(obj);*/
    // console.log(type);
    var  accept_files = "audio/*, image/*, video/";
    if($(obj).hasClass('bonus_item_file')){
       accept_files = "audio/*, image/*, video/*";
    }
    
    //var file_limit   = $(obj).data('limit');
    var file_limit     = $('.media_limit').val();
    var file_crop      = $('.file_crop').val();
    var remaining_file = $('.remaining_file').val();
    var media_type     = '';   
    //var image_for   = $(obj).data('image-for');
    if(type != ''){
        media_type = type;
    }else{
        if(typeof($(obj).data('media_type')) !== "undefined"){
            if($(obj).data('media_type').length > 0){
                media_type = $(obj).data('media_type');
            }
        }
    }
    //console.log(media_type);
        
    var image_for   = $('.sub_module').val();
    var pre_element = $(obj).data('preview-ele');
    var element_id  = $(obj).attr('id');
    //var module      = $(obj).data('module');
    var module      = $('.main_module').val();
    //alert(module);
    if ( $( "#yes_crop" ).length > 0 || $( "#getCropped" ).length > 0) {
        if(module == "Host" && image_for == "itunes"){
            var procesQueue = true;
            var autoqueue   = false;
        }
        else if(module == "InventoryItem"){
            if(media_type == "files"){
                var procesQueue = true;
                var autoqueue   = false;
            }
            else
            {
                var procesQueue = false;
                var autoqueue   = false;
            }
        }else{
            var procesQueue = false;
            var autoqueue   = true;
        }        
    }else{
        var procesQueue = true;
        var autoqueue   = false;
    }
   /* console.log(procesQueue);
    console.log(autoqueue);*/

    var forceWidth  = null;
    var forceHeight = null;
    var forceMethod = 'contain';

    var form_type   = $('#form_type').val();
    if(media_type != ''){
        //alert(media_type);
        if(media_type == "image"){
            accept_files = "image/*";
        }else if(media_type == "audio"){
            accept_files = "audio/*";
        }else if(media_type == "video"){
            accept_files = "video/*";
        }
        else if(media_type == "files"){
            accept_files = "video/*, audio/*, image/*,application/pdf,.psd,.sql,.txt";
        }
    }   
   
    if(module == "Banner" || module == "Channel" || module == "Show" || module == "Host"){
        
        autoqueue   = false;
        forceMethod = "crop";
        if(image_for == "120x300"){
            forceWidth  = 120;
            forceHeight = 300;
        }else if(image_for == "300x250"){
            forceWidth  = 300;
            forceHeight = 250;
        }else if(image_for == "625x258"){
            forceWidth  = 625;
            forceHeight = 258;
        }else if(image_for == "728x90"){
            forceWidth  = 728;
            forceHeight = 90;
        }else if(image_for == "player"){
            forceWidth  = 500;
            forceHeight = 200;
        }else if(image_for == "zapbox"){
            forceWidth  = 170;
            forceHeight = 300;
        }else if(image_for == "zapboxsmall"){
            forceWidth  = 170;
            forceHeight = 137;
        }else if(image_for == "schedule"){
            forceWidth  = 120;
            forceHeight = 300;
        }else if(image_for == "player"){
            forceWidth  = 625;
            forceHeight = 258;
        }else if(image_for == "featured"){
            forceWidth  = 120;
            forceHeight = 150;
        }else if(image_for == "itunes"){
            forceWidth  = 1400;
            forceHeight = 1400;
        }       
    }
   
    myDropzone  = new Dropzone("#"+element_id, {
        //url: APP_URL+"/admin/uploads/upload",
        url: APP_URL+"/admin/cms/media/add/"+media_type,
        //paramName: "image", // The name that will be used to transfer the file
        paramName: "files", // The name that will be used to transfer the file
        autoProcessQueue: procesQueue,
        maxFilesize: 120, // MB
        parallelUploads: 10,
        uploadMultiple: true,
        params: {'image_for': image_for, 'module': module, 'form_type': form_type, /*'item_id': item_id,*/ 'element_id': element_id, 'crop':0,'file_limit':file_limit,'file_crop':file_crop,'remaining_file':remaining_file},
        autoQueue: true,
        acceptedFiles: accept_files,
        previewsContainer: pre_element,
        maxFiles: remaining_file,
        resizeHeight: forceHeight,
        resizeWidth: forceWidth,
        resizeMethod: forceMethod,       
        /*headers: {
            "Accept": "application/json",
            "Cache-Control": "no-cache",
            "X-Requested-With": ""
        },*/      
        maxfilesexceeded: function(file) {  
            swal({
               title: '', 
               text: 'You Can Upload Only '+this.options.maxFiles+' Files....!',
               type: 'warning'
            });
            this.removeFile(file);
        },
        init: function() { 
            // Capture the Dropzone instance as closure.
            var myDropzone = this;   
            $('.sortable').sortable('enable');
           
                   
            /*var data = {'module' : module, 'folder' : image_for, 'id' : myDropzone.options.params.item_id, 'form_type': form_type};
            makeAjaxRequest('POST', '/admin/uploads/get_files', data)
            .done(function(response) {
                if(response.status == 'success'){                
                    $.each(response.data, function( index, value ) {                          
                        var mockFile = response.data[index];     
                        // Call the default addedfile event handler
                        myDropzone.emit("addedfile", mockFile);
                        // And optionally show the thumbnail of the file:
                        myDropzone.emit("thumbnail", mockFile, response.data[index].url);
                        myDropzone.files.push(mockFile);     
                    });
                }
            })
            .fail(function(xhr) {
                console.log('error callback ', xhr);
            });*/
            
            makeDropZoneSortable(myDropzone.options.params.element_id);
            this.on("addedfile", function(file) {
               
                var src = file.url;
                file.previewElement.addEventListener("click", function() {
                    var win = window.open(src, '_blank');
                    win.focus();
                });

                // Create the remove button
                $('.dz-progress').show();
                $('.dz-message').show();
                $('.dz-size').hide();
                $('.dz-filename').hide();
                $('.dz-error-mark').show();
                $('.dz-remove').show();
                $('.dz-success-mark').show();
                var total_count  = myDropzone.files.length;
                var removeButton = Dropzone.createElement("<button class='btn-danger btn-block' title='Remove'><i class='fa fa-trash' aria-hidden='true'></i></button>");
                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();
                   
                    if(file.xhr != undefined){
                        $response = JSON.parse(file.xhr.response);
                        var data = {'file_path': $response.path, 'ajax': true, id:file.serverId};
                    }else{
                        var data = {'file_path' : file.path, 'ajax': false, id:file.serverId};
                    }
                    //first remove the file from server
                    //makeAjaxRequest('POST', '/admin/uploads/delete_files', data)
                    makeAjaxRequest('POST', '/admin/cms/media/delete_media', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            //Remove the file preview.
                            myDropzone.removeFile(file);
                            var all_files = myDropzone.files;
                            //after delete change the file sequence
                            var current_queue = [];
                            $.each(all_files, function( index, value ) {
                                current_queue[index] = value.name;
                            });
                            var params = myDropzone.options.params;
                            var data = {'module': params.module, 'folder': params.image_for, 'id': params.item_id, 'form_type': params.form_type, new_order: current_queue};
                            makeRequest('POST', '/admin/uploads/sort_files', data, false);
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                });
                file.previewElement.appendChild(removeButton);
            });
           
            this.on('error', function(file, errorMessage) {
                alert('error');
                console.log(file);
                console.log(errorMessage);
                var mypreview = document.getElementsByClassName('dz-error');
                mypreview = mypreview[mypreview.length - 1];
                mypreview.classList.toggle('dz-error');
                mypreview.classList.toggle('dz-success');
               
                swal({
                   title: '', 
                   text: errorMessage,
                   type: 'warning'
                });
                this.removeFile(file); 
                
            });
           
            this.on('sendingmultiple', function (data, xhr, formData, response) {
                formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
                formData.append("image_for", myDropzone.options.params.image_for);
                formData.append("module", myDropzone.options.params.module);
                formData.append("form_type", myDropzone.options.params.form_type);
                formData.append("item_id", myDropzone.options.params.item_id);
              
            });
            
            this.on('success', function (file, responseText) {
                console.log(responseText)
                if(responseText.upload == 'success')
                {
                    uploadedMediaObj = responseText.data[0];
                    var mockFile = responseText.data[0];
                    file.serverId = responseText.data[0].id;
                    $('#getCropped').data('old_image',responseText.data[0].url);
                    $('#getCropped').data('dropzone_obj',myDropzone);
                    $('.selectedMediaTitle').val(responseText.data[0].title);
                    $('.selectedMediaSrc').val(responseText.data[0].url);
                    $('.selectedMediaSrc').data('src',responseText.data[0].src);                   
                    $('.selectedMediaId').val(responseText.mediaids);
                    $('.select_choose_media').removeAttr("disabled");                   
                }
            });
        }
    });

    if ( $( "#yes_crop" ).length || $( "#getCropped" ).length) {
        var module_name = '';    
        var folder_name = '';
        // listen to thumbnail event
        var drop_element = myDropzone.element;
        //console.log($(drop_element).context.dataset);
        myarray = [];
        myarray.push('itunes');
        myarray.push('InventoryItem');
        var offset = $(drop_element).data('image-for');
        //console.log($(drop_element).context.dataset.imageFor)
        var dataImageFor = $(drop_element).context.dataset.imageFor;
        //var exceptionFiles = $(drop_element).context.dataset.imageFor;
        // Disable cropper for itunes on Host module        
        //if(($(drop_element).context.dataset.imageFor !== "itunes")) {
        //if($.inArray($(drop_element).context.dataset.imageFor, myarray) !== -1){
        if(dataImageFor !==  "itunes"){
            if(dataImageFor !==  "InventoryItem" && media_type !== 'files'){
                myDropzone.on('thumbnail', function (file) {
                   
                    module_name = $(drop_element).context.dataset.module;    
                    folder_name = $(drop_element).context.dataset.imageFor;  
                   
                    //module_name = $(drop_element).data('module');      
                    //folder_name = $(drop_element).data('image-for');    
                   
                    // ignore files which were already cropped and re-rendered
                    // to prevent infinite loop
                    if (file.cropped) {
                        return;
                    } 
                    if(file.status == "error"){
                        return;
                    }
                   
                    var modalTemplate = '<div id="custom-modal" class="modal-demo modal">'
                        +'<button type="button" class="close" onclick="$(this).closest(&quot;#custom-modal&quot;).modal(&quot;hide&quot;);$(&quot;.modal-backdrop.in&quot;).remove();$(&quot;body&quot;).removeClass(&quot;remove_scroll&quot;);">'
                            +'<span>&times;</span><span class="sr-only">Close</span>'
                        +'</button>'
                        +'<h4 class="custom-modal-title">Edit Image</h4>'
                        +'<div class="custom-modal-text">'
                           +'<!-- Content -->'
                            +'<div class="container">'
                                +'<div class="row">'
                                    +'<div class="col-md-12">'
                                        +'<div class="image-container">'
                                            +'<img id="image" src="" alt="Picture" style="width:100%">'
                                        +'</div>'
                                    +'</div>'
                                    
                                +'</div>'
                                +'<div class="row" style="margin-top: 10px;">'
                                    +'<div class="col-md-12 docs-buttons">'
                                        +'<div class="btn-group">'
                                            +'<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">'
                                                    +'<span class="fa fa-search-plus"></span>'
                                                +'</span>'
                                            +'</button>'
                                            +'<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">'
                                                    +'<span class="fa fa-search-minus"></span>'
                                                +'</span>'
                                            +'</button>'
                                        +'</div>'

                                        +'<div class="btn-group">'
                                            +'<button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, -10, 0)">'
                                                    +'<span class="fa fa-arrow-left"></span>'
                                                +'</span>'
                                            +'</button>'
                                            +'<button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 10, 0)">'
                                                    +'<span class="fa fa-arrow-right"></span>'
                                                +'</span>'
                                            +'</button>'
                                            +'<button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, -10)">'
                                                    +'<span class="fa fa-arrow-up"></span>'
                                                +'</span>'
                                            +'</button>'
                                            +'<button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, 10)">'
                                                    +'<span class="fa fa-arrow-down"></span>'
                                               +'</span>'
                                            +'</button>'
                                        +' </div>'

                                        +'<div class="btn-group">'
                                            +'<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">'
                                                +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">'
                                                    +'<span class="fa fa-rotate-left"></span>'
                                                +'</span>'
                                        +'</button>'
                                      +'<button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">'
                                       +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">'
                                          +'<span class="fa fa-rotate-right"></span>'
                                        +'</span>'
                                      +'</button>'
                                   +' </div>'

                                    +'<div class="btn-group">'
                                      +'<button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleX&quot;, -1)">'
                                          +'<span class="fa fa-arrows-h"></span>'
                                        +'</span>'
                                      +'</button>'
                                      +'<button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleY&quot;, -1)">'
                                          +'<span class="fa fa-arrows-v"></span>'
                                        +'</span>'
                                      +'</button>'
                                    +'</div>'

                                   +'<div class="btn-group">'
                                      +'<button type="button" class="btn btn-primary" data-method="crop" title="Crop">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;crop&quot;)">'
                                          +'<span class="fa fa-check"></span>'
                                        +'</span>'
                                      +'</button>'
                                      +'<button type="button" class="btn btn-primary" data-method="clear" title="Clear">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;clear&quot;)">'
                                          +'<span class="fa fa-remove"></span>'
                                        +'</span>'
                                      +'</button>'
                                    +'</div>'

                                    +'<div class="btn-group">'
                                      +'<button type="button" class="btn btn-primary" data-method="reset" title="Reset">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;reset&quot;)">'
                                          +'<span class="fa fa-refresh"></span>'
                                        +'</span>'
                                      +'</button>'
                                      +'<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">'
                                        +'<input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Import image with Blob URLs">'
                                          +'<span class="fa fa-upload"></span>'
                                        +'</span>'
                                      +'</label>'
                                     
                                    +'</div>'

                                    +'<div class="btn-group btn-group-crop">'
                                        +'<button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" data-image_for="'+folder_name+'" data-module="'+module_name+'" data-form_type="add" data-crop="1" id="getCropped" data-old_image="" data-outer_dropzone="HostPhoto0" data-dropzone_obj="">'
                                            +'<span class="docs-tooltip crop-upload" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { maxWidth: 4096, maxHeight: 4096 })">'
                                                +'Apply'
                                            +'</span>'
                                        +'</button>'
                                    +'</div>'

                                    +'<!-- Show the cropped image in modal -->'
                                    +'<div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">'
                                      +'<div class="modal-dialog">'
                                        +'<div class="modal-content">'
                                          +'<div class="modal-header">'
                                            +'<h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>'
                                            +'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                                              +'<span aria-hidden="true">&times;</span>'
                                            +'</button>'
                                          +'</div>'
                                          +'<div class="modal-body"></div>'
                                          +'<div class="modal-footer">'
                                            +'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
                                            +'<a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>'
                                          +'</div>'
                                        +'</div>'
                                      +'</div>'
                                    +'</div><!-- /.modal -->'
                                  +'</div><!-- /.docs-buttons -->'

                                 +'<div class="col-md-12 docs-toggles">'
                                    +'<!-- <h3>Toggles:</h3> -->'
                                    +'<div class="btn-group d-flex flex-nowrap" data-toggle="buttons">'
                                      +'<label class="btn btn-primary active">'
                                        +'<input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">'
                                          +'16:9'
                                        +'</span>'
                                      +'</label>'
                                      +'<label class="btn btn-primary">'
                                        +'<input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">'
                                         +'4:3'
                                        +'</span>'
                                      +'</label>'
                                      +'<label class="btn btn-primary">'
                                        +'<input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">'
                                       +' <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">'
                                         +' 1:1'
                                        +'</span>'
                                      +'</label>'
                                      +'<label class="btn btn-primary">'
                                       +' <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">'
                                       +' <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">'
                                        +'  2:3'
                                        +'</span>'
                                      +'</label>'
                                      +'<label class="btn btn-primary">'
                                        +'<input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">'
                                        +'<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">'
                                          +'Free'
                                        +'</span>'
                                      +'</label>'
                                    +'</div>'

                                  +'</div><!-- /.docs-toggles -->'
                                +'</div>'
                              +'</div>'
                        +'</div>'
                    +'</div>'
                    +'<!-- End Modal -->';

                    /* if (file.width < 800) {
                        // validate width to prevent too small files to be uploaded
                        // .. add some error message here
                        return;
                    }*/
                    // cache filename to re-assign it to cropped file
                    var cachedFilename = file.name;
                    // remove not cropped file from dropzone (we will replace it later)
                    myDropzone.removeFile(file);
                   
                    // dynamically create modals to allow multiple files processing
                    var $cropperModal = $(modalTemplate);
                 
                    // 'Crop and Upload' button in a modal
                    var $uploadCrop = $cropperModal.find('.crop-upload');
                    var $img = $('<img />');
                   
                    // initialize FileReader which reads uploaded file
                    var reader = new FileReader();
                   
                    reader.onloadend = function () {

                        // add uploaded and read image to modal
                        $img.attr('src', reader.result);
                        $cropperModal.find('.image-container').html($img);
                        initializeCropper($img);
                        $cropperModal.modal('show');

                    };

                    // read uploaded file (triggers code above)
                    reader.readAsDataURL(file);           
                    // listener for 'Crop and Upload' button in modal
                    $uploadCrop.on('click', function() {
                       
                        // get cropped image data
                        var blob = $img.cropper('getCroppedCanvas').toDataURL();
                        //var blob = trimmedCanvas.toDataURL();
                        // transform it to Blob object

                        var newFile = dataURItoBlob(blob);
                        $('#image').attr('src',newFile);
                        // set 'cropped to true' (so that we don't get to that listener again)
                        newFile.cropped = true;
                        // assign original filename
                        newFile.name = cachedFilename;

                        // add cropped file to dropzone
                        myDropzone.addFile(newFile);
                       
                        // upload cropped file with dropzone
                        myDropzone.processQueue();
                        $cropperModal.modal('hide');
                        $('.modal-backdrop.in').remove();
                        // cropper.destroy();

                        $('#yes_crop').removeAttr('data-module');
                        $('#yes_crop').removeAttr('data-image_for');
                    });
                   
                });
            }
        }
    }
}

function makeDropZoneSortable(element_id){
    $("#"+element_id).sortable({
        items:'.dz-preview',
        cursor: 'move',
        opacity: 0.5,
        containment: "parent",
        distance: 20,
        tolerance: 'pointer',
        update: function(e, ui){
            //console.log(e);
            var element_id    = e.target.dropzone.element.id;
            var imageTags     = $('#'+element_id).children().find('.dz-image-preview');
            var current_queue = [];
            imageTags.each(function (index, imageTag) {
                var file_name = $(this).children().children().find('span').text();
                current_queue[index] = file_name;
            });
            var params = e.target.dropzone.options.params;
            var data = {'module': params.module, 'folder': params.image_for, 'id': params.item_id, 'form_type': params.form_type, new_order: current_queue};
            makeRequest('POST', '/admin/uploads/sort_files', data, false);
        }
    });
}

function makeRequest(type, requrl, data, return_type){
    makeAjaxRequest(type, requrl, data)
    .done(function(response) {
        //console.log(response);
        if(response.status == 'success'){
           
        }
    })
    .fail(function(xhr) {
        console.log('error callback ', xhr);
    });
}

