$(document).ready(function() {
    Dropzone.autoDiscover = false;
    $("form div .dropzone").each(function(i){
        createDropZoneInstance(this);
    });

});

// modal window template
var modalTemplate = '<div id="custom-modal" class="modal-demo">'
            +'<button type="button" class="close" onclick="Custombox.close();">'
               +'<span>&times;</span><span class="sr-only">Close</span>'
            +'</button>'
            +'<h4 class="custom-modal-title">Modal title</h4>'
            +'<div class="custom-modal-text">'
                +"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            +'</div>'
        +'</div>';

function createDropZoneInstance(obj){
    var file_limit  = $(obj).data('limit');
    var image_for   = $(obj).data('image-for');
    var pre_element = $(obj).data('preview-ele');
    var element_id  = $(obj).attr('id');
    var module      = $(obj).data('module');
    var form_type   = $('#form_type').val();
    if(form_type == 'edit'){
        var item_id     = $('#edit_id').val();
    }else{
        var item_id     = 0;
    }
    
    var myDropzone  = new Dropzone("#"+element_id, {
        url: APP_URL+"/admin/uploads/upload",
        paramName: "image", // The name that will be used to transfer the file
        autoProcessQueue: true,
        maxFilesize: 5, // MB
        parallelUploads: 4,
        uploadMultiple: true,
        params: {'image_for': image_for, 'module': module, 'form_type': form_type, 'item_id': item_id, 'element_id': element_id},
        autoQueue: true,
        acceptedFiles: "image/*",
        previewsContainer: pre_element,
        maxFiles: file_limit,
        maxfilesexceeded: function(file) {
            alert('You Can Upload Only '+this.options.maxFiles+' Files....!');
            this.removeFile(file);
        },
        init: function() { 
            $('.sortable').sortable('enable');
            // Capture the Dropzone instance as closure.
            var myDropzone = this;
            //if(form_type == 'edit'){
                var data = {'module' : module, 'folder' : image_for, 'id' : myDropzone.options.params.item_id, 'form_type': form_type};
                makeAjaxRequest('POST', '/admin/uploads/get_files', data)
                .done(function(response) {
                    if(response.status == 'success'){
                        $.each(response.data, function( index, value ) {
                            var mockFile = response.data[index];
                            // Call the default addedfile event handler
                            myDropzone.emit("addedfile", mockFile);
                            // And optionally show the thumbnail of the file:
                            myDropzone.emit("thumbnail", mockFile, response.data[index].url);
                            myDropzone.files.push(mockFile);
                            
                        });
                    }
                })
                .fail(function(xhr) {
                    console.log('error callback ', xhr);
                });
            //}
            makeDropZoneSortable(myDropzone.options.params.element_id);
            this.on("addedfile", function(file) {
                //console.log(file.name);
                // Create the remove button
                $('.dz-progress').show();
                $('.dz-message').show();
                $('.dz-size').hide();
                $('.dz-filename').hide();
                $('.dz-error-mark').show();
                $('.dz-remove').show();
                $('.dz-success-mark').show();
                var total_count = myDropzone.files.length;
                var removeButton = Dropzone.createElement("<button class='btn-danger btn-block' title='Remove'><i class='fa fa-trash' aria-hidden='true'></i></button>");
                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();
                    if(file.xhr != undefined){
                        $response = JSON.parse(file.xhr.response);
                        var data = {'file_path': $response.path, 'ajax': true};
                    }else{
                        var data = {'file_path' : file.path, 'ajax': false};
                    }
                    //first remove the file from server
                    makeAjaxRequest('POST', '/admin/uploads/delete_files', data)
                    .done(function(response) {
                        if(response.status == 'success'){
                            //Remove the file preview.
                            myDropzone.removeFile(file);
                            var all_files = myDropzone.files;
                            //after delete change the file sequence
                            var current_queue = [];
                            $.each(all_files, function( index, value ) {
                                current_queue[index] = value.name;
                            });
                            var params = myDropzone.options.params;
                            var data = {'module': params.module, 'folder': params.image_for, 'id': params.item_id, 'form_type': params.form_type, new_order: current_queue};
                            makeRequest('POST', '/admin/uploads/sort_files', data, false);
                        }
                    })
                    .fail(function(xhr) {
                        console.log('error callback ', xhr);
                    });
                });
                file.previewElement.appendChild(removeButton);
            });
            
            this.on('error', function(file, errorMessage) {
                var mypreview = document.getElementsByClassName('dz-error');
                mypreview = mypreview[mypreview.length - 1];
                mypreview.classList.toggle('dz-error');
                mypreview.classList.toggle('dz-success');
            });
            
            /*var submitButton = document.querySelector("#add_album_images")
            submitButton.addEventListener("click", function(e) { 
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue(); 
                var files = myDropzone.files;
            });
            */
            this.on('sendingmultiple', function (data, xhr, formData, response) {
                formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
                formData.append("image_for", myDropzone.options.params.image_for);
                formData.append("module", myDropzone.options.params.module);
                formData.append("form_type", myDropzone.options.params.form_type);
                formData.append("item_id", myDropzone.options.params.item_id);
            });
            
            this.on('success', function (file, responseText) {
                console.log(responseText);
                if(responseText.upload == 'success')
                {
                    myDropzone.removeFile(file);
                    var mockFile = responseText.data;
                    // Call the default addedfile event handler
                    myDropzone.emit("addedfile", mockFile);
                    // And optionally show the thumbnail of the file:
                    myDropzone.emit("thumbnail", mockFile, responseText.data.url);
                    myDropzone.files.push(mockFile);
                    $('#open_modal').trigger('click');
                    $('#popupCropImg').attr('src',responseText.data.url);
                    initializeCropper($('#popupCropImg'));
                }
            });
        }
    });

    
   
}

function initializeCropper(obj){

    // init cropper
    $.$cropImg = obj;
    //var canvasData, cropBoxData;
    /*$.$cropImg.cropper({
        aspectRatio: 4 / 3,
        crop: function (data) {

        },
        zoomable : false,
        scalable : false,
        movable : false,
        background : true,
        viewMode : 2,
        built : function(){
            $(this).cropper('getCroppedCanvas').toBlob(function (blob) {
                console.log("ENtereedddd");
                var formData = new FormData();
                formData.append('croppedImage', blob);
                console.log(formData);
            });
        }
    });*/

    var console = window.console || { log: function () {} };
    var URL = window.URL || window.webkitURL;
    var $image = obj;
    console.log($image);
      //var $download = $('#download');
    var $dataX = $('#dataX');
    console.log($dataX);
    var $dataY = $('#dataY');
    console.log($dataY);
    var $dataHeight = $('#dataHeight');
    var $dataWidth = $('#dataWidth');
    var $dataRotate = $('#dataRotate');
    var $dataScaleX = $('#dataScaleX');
    var $dataScaleY = $('#dataScaleY');
    var options = {
        aspectRatio: 16 / 9,
        preview: '.img-preview',
        crop: function (e) {
            // $dataX.val(Math.round(e.detail.x));
            // $dataY.val(Math.round(e.detail.y));
            // $dataHeight.val(Math.round(e.detail.height));
            // $dataWidth.val(Math.round(e.detail.width));
            // $dataRotate.val(e.detail.rotate);
            // $dataScaleX.val(e.detail.scaleX);
            // $dataScaleY.val(e.detail.scaleY);
        }
    };
    var originalImageURL = $image.attr('src');
    //alert(originalImageURL);
    var uploadedImageName = 'cropped.jpg';
    var uploadedImageType = 'image/jpeg';
    var uploadedImageURL;

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Cropper
    $image.on({
        ready: function (e) {
          console.log(e.type);
        },
        cropstart: function (e) {
          console.log(e.type, e.detail.action);
        },
        cropmove: function (e) {
            console.log(e);
            console.log(e.detail);
            console.log(e.type, e.detail.action);
        },
        cropend: function (e) {
          console.log(e.type, e.detail.action);
        },
        crop: function (e) {
          console.log(e.type);
        },
        zoom: function (e) {
          console.log(e.type, e.detail.ratio);
        }
    }).cropper(options);
    // Buttons
    if (!$.isFunction(document.createElement('canvas').getContext)) {
        $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
    }

    if (typeof document.createElement('cropper').style.transition === 'undefined') {
        $('button[data-method="rotate"]').prop('disabled', true);
        $('button[data-method="scale"]').prop('disabled', true);
    }

    // Options
    $('.docs-toggles').on('change', 'input', function () {
        var $this = $(this);
        var name = $this.attr('name');
        var type = $this.prop('type');
        var cropBoxData;
        var canvasData;

        if (!$image.data('cropper')) {
          return;
        }

        if (type === 'checkbox') {
          options[name] = $this.prop('checked');
          cropBoxData = $image.cropper('getCropBoxData');
          canvasData = $image.cropper('getCanvasData');

          options.ready = function () {
            $image.cropper('setCropBoxData', cropBoxData);
            $image.cropper('setCanvasData', canvasData);
          };
        } else if (type === 'radio') {
          options[name] = $this.val();
        }

        $image.cropper('destroy').cropper(options);
    });

    // Methods
    $('.docs-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var cropper = $image.data('cropper');
        var cropped;
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
          return;
        }

        if (cropper && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                    console.log(e.message);
                }
            }
        }

        cropped = cropper.cropped;

        switch (data.method) {
            case 'rotate':
                if (cropped && options.viewMode > 0) {
                    $image.cropper('clear');
                }

            break;

            case 'getCroppedCanvas':
                if (uploadedImageType === 'image/jpeg') {
                    if (!data.option) {
                        data.option = {};
                    }

                    data.option.fillColor = '#fff';
                }

            break;
        }

        result = $image.cropper(data.method, data.option, data.secondOption);

        switch (data.method) {
            case 'rotate':
                if (cropped && options.viewMode > 0) {
                    $image.cropper('crop');
                }

            break;

            case 'scaleX':
            case 'scaleY':
              $(this).data('option', -data.option);
              break;

            case 'getCroppedCanvas':
                if (result) {
                    // Bootstrap's Modal
                    $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                    if (!$download.hasClass('disabled')) {
                      download.download = uploadedImageName;
                      $download.attr('href', result.toDataURL(uploadedImageType));
                    }
                }

            break;

            case 'destroy':
                if (uploadedImageURL) {
                    URL.revokeObjectURL(uploadedImageURL);
                    uploadedImageURL = '';
                    $image.attr('src', originalImageURL);
                }

                break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                console.log(e.message);
            }
        }
    }
});

    // Keyboard
  /*  (document.body).on('keydown', function (e) {
        if (e.target !== this || !$image.data('cropper') || this.scrollTop > 300) {
          return;
        }

        switch (e.which) {
          case 37:
            e.preventDefault();
            $image.cropper('move', -1, 0);
            break;

          case 38:
            e.preventDefault();
            $image.cropper('move', 0, -1);
            break;

          case 39:
            e.preventDefault();
            $image.cropper('move', 1, 0);
            break;

          case 40:
            e.preventDefault();
            $image.cropper('move', 0, 1);
            break;
        }
    });*/

    // Import image
    var $inputImage = $('#inputImage');

    if (URL) {
        $inputImage.change(function () {
        var files = this.files;
        var file;

        if (!$image.data('cropper')) {
            return;
        }

        if (files && files.length) {
            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                uploadedImageName = file.name;
                uploadedImageType = file.type;

                if (uploadedImageURL) {
                    URL.revokeObjectURL(uploadedImageURL);
                }

               uploadedImageURL = URL.createObjectURL(file);
               $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
               $inputImage.val('');
            } else {
               window.alert('Please choose an image file.');
            }
        }
    });
  } else {
    $inputImage.prop('disabled', true).parent().addClass('disabled');
  }

    
}

function makeDropZoneSortable(element_id){
    $("#"+element_id).sortable({
        items:'.dz-preview',
        cursor: 'move',
        opacity: 0.5,
        containment: "parent",
        distance: 20,
        tolerance: 'pointer',
        update: function(e, ui){
            //console.log(e);
            var element_id    = e.target.dropzone.element.id;
            var imageTags     = $('#'+element_id).children().find('.dz-image-preview');
            var current_queue = [];
            imageTags.each(function (index, imageTag) {
                var file_name = $(this).children().children().find('span').text();
                current_queue[index] = file_name;
            });
            var params = e.target.dropzone.options.params;
            var data = {'module': params.module, 'folder': params.image_for, 'id': params.item_id, 'form_type': params.form_type, new_order: current_queue};
            makeRequest('POST', '/admin/uploads/sort_files', data, false);
        }
    });
}

function makeRequest(type, requrl, data, return_type){
    makeAjaxRequest(type, requrl, data)
    .done(function(response) {
        //console.log(response);
        if(response.status == 'success'){
           
        }
    })
    .fail(function(xhr) {
        console.log('error callback ', xhr);
    });
}