$(document).ready(function(){
        /*$('.owl-carousel').owlCarousel({
			loop: true,
			autoplay: true,
			autoplayTimeout: 5000,
			autoplaySpeed: 1000,
			touchDrag: true,			
			margin: 10,
			responsiveClass: true,
			responsive: {
			  0: {
				items: 1,
				nav: true,
				loop: true,
			  },
			  600: {
				items: 3,
				nav: false
			  },
			  1000: {
				items: 5,
				nav: true,
				loop: true,
				margin: 20
			  }
			}
		});*/
});
    $('.carousel[data-type="multi"] .item').each(function() {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i = 0; i < 2; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });
    
    $('.blog-column-wrapper').isotope({
        itemSelector: '.col-md-4',
        percentPosition: true,
        masonry: {
            columnWidth: '.col-md-4'
        }
    });
    
    /* START - Common pagination for the frontedn listings */

    var stateObj = { foo: "bar" };
    function ajax_pagination(href, obj, qstring, paginationObj = $('.page-navigation'), data={'ajax':1}){
        
        $.ajax({
            url:        href,
            method:     'POST',
            data:       data,
            dataType:   'JSON',
            beforeSend: function(){
                obj.append('<div class="ui-lock" style="position:absolute; z-index:9;left:0;right:0;top:0;bottom:0;text-align:center; background:rgb(239,239,239);opacity:0.3;overflow:hidden"><span class="ui-lock-loader" style="position:relative;display:inline-block;vertical-align:middle;"></span></div>');
            },
            success:    function(response){
                obj.html(response.html);
                $('.page-navigation').html(response.pagination);
                qstring1 = qstring.replace('?', '&');
                console.log(qstring);
                if(href.indexOf(qstring) != -1){

                    //First check the page variable
                    if(href.indexOf('page=') != -1)//If page variable is found
                    {
                        qstring = qstring.replace('?', '');
                        href = href.replace(qstring, "");
                        if(href.indexOf('?&') != -1)
                        {
                            href = href.replace('?&', '?');
                        }
                    }
                    else{
                        href = href.replace(qstring, "");
                    }
                    history.pushState(stateObj, "xyz", href);
                }
                else if(href.indexOf(qstring1) != -1){

                    //First check the page variable
                    if(href.indexOf('page=') != -1)//If page variable is found
                    {
                        qstring = qstring1.replace('?', '');
                        href = href.replace(qstring, "");
                        if(href.indexOf('?&') != -1)
                        {
                            href = href.replace('?&', '?');
                        }
                    }
                    else{
                        href = href.replace(qstring, "");
                    }
                  history.pushState(stateObj, "xyz", href);

                }
                else
                    history.pushState(stateObj, "xyz", href);
            }
        });
    }
    /* END - Common pagination for the frontend listings */

    // Explicitly save/update a url parameter using HTML5's replaceState().
    /*function updateQueryStringParam(key, value, page='') {

        var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

        // If the "search" string exists, then build params from it
        if (urlQueryString) {

            updateRegex = new RegExp('([\?&])' + key + '[^&]*');
            removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

           // if( typeof value == 'undefined' || value == null || value == '' ) { // Remove param if value is empty
            if(page == "host" || page == "archive" || page=="users" || page=="banner" || page=="upcoming" || page=="encore"){
                if( value == 0  || typeof value == 'undefined' || value == null) { 
                    params = urlQueryString.replace(removeRegex, "$1");
                    params = params.replace( /[&;]$/, "" );

                } else if (urlQueryString.match(updateRegex) !== null) { 
                    // If param exists already, update it
                    params = urlQueryString.replace(updateRegex, "$1" + newParam);
                } else { 
                    // Otherwise, add it to end of query string
                    params = urlQueryString + '&' + newParam;
                }
            }else{
                if( value == 2  || typeof value == 'undefined' || value == null) { 
                    params = urlQueryString.replace(removeRegex, "$1");
                    params = params.replace( /[&;]$/, "" );

                } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
                    
                    params = urlQueryString.replace(updateRegex, "$1" + newParam);

                } else { // Otherwise, add it to end of query string
                    params = urlQueryString + '&' + newParam;
                }
            }
            
            if(params == "?"){
                removeRegex = new RegExp('([\??])');
                params = params.replace( removeRegex, "" );
            }
        }
        window.history.replaceState({}, "", baseUrl + params);
    }*/

    /* Common function to get zapbox this week/upcoming schedules starts here*/
    var html = '<div class="ui-lock" style="z-index: 9; left: 0px; right: 0px; top: 0px; bottom: 0px; text-align: center; background: rgb(239, 239, 239); opacity: 0.3; overflow: hidden; transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);"><span style="display:inline-block;vertical-align:middle;width:0;border:0;padding:0;margin:0;height:100%;opacity:0.3;"></span><span class="ui-lock-loader" style="position: relative; display: inline-block; vertical-align: middle;"><i class="ico wait ico-wait" style="width:6rem;height:6rem;fill:#C5C5C5;color:#C5C5C5;"><svg viewBox="0 0 27 32"><path d="M26.982 18.857q0 0.089-0.018 0.125-1.143 4.786-4.786 7.759t-8.536 2.973q-2.607 0-5.045-0.982t-4.348-2.804l-2.304 2.304q-0.339 0.339-0.804 0.339t-0.804-0.339-0.339-0.804v-8q0-0.464 0.339-0.804t0.804-0.339h8q0.464 0 0.804 0.339t0.339 0.804-0.339 0.804l-2.446 2.446q1.268 1.179 2.875 1.821t3.339 0.643q2.393 0 4.464-1.161t3.321-3.196q0.196-0.304 0.946-2.089 0.143-0.411 0.536-0.411h3.429q0.232 0 0.402 0.17t0.17 0.402zM27.429 4.571v8q0 0.464-0.339 0.804t-0.804 0.339h-8q-0.464 0-0.804-0.339t-0.339-0.804 0.339-0.804l2.464-2.464q-2.643-2.446-6.232-2.446-2.393 0-4.464 1.161t-3.321 3.196q-0.196 0.304-0.946 2.089-0.143 0.411-0.536 0.411h-3.554q-0.232 0-0.402-0.17t-0.17-0.402v-0.125q1.161-4.786 4.821-7.759t8.571-2.973q2.607 0 5.071 0.991t4.375 2.795l2.321-2.304q0.339-0.339 0.804-0.339t0.804 0.339 0.339 0.804z"></path></svg></i></span></div>';
    // href = (Sun - sat) or upcoming
    // type = upcoming or get (for this week)
    // channel_id = Current Channel ID
    function get_zapbox_results(href, type, channel_id){
        $.ajax({
            url:        APP_URL+'/schedule/zapbox',
            method:     'POST',
            data:       'param='+href+'&type='+type+'&channel_id='+channel_id,
            beforeSend: function(){
                $('#'+href).append(html);
            },
            success:    function(response){
                $('body').find('#'+href).append(response);
            }
        });
    }
    /* Common function to get zapbox this week/upcoming schedules ends here*/
    /* Common function to get banners at regular interval starts here*/
    setInterval(function(){ refresh_banners(); }, 5000);
    function refresh_banners(){
        var sizes = 'sizes[728x90]=2&sizes[120x300]=6';
        if (typeof resolutions !== 'undefined')
            sizes = resolutions;
        $.ajax({
            url:        APP_URL+'/advertising/banners/get?json=true&'+sizes+'&channel_id='+channel_id,
            dataType : 'JSON',
            success:    function(response){
                if(response['728x90'].length)
                {
                    var bannerType = '728x90';
                    $.each(response[bannerType], function( index, value ) {
                        bannerHtml(index, value, bannerType);
                    });
                }
                if(response['120x300'].length)
                {
                    var bannerType = '120x300';
                    $.each(response[bannerType], function( index, value ) {
                        bannerHtml(index, value, bannerType)
                    });
                }
            }
        })
    }
    function bannerHtml(index, value, bannerType)
    {
        var t = $('body').find('[data-size="'+bannerType+'"]:eq('+(index)+')');
        if(t.html() !== undefined)
        {

            t.attr('data-id', value.id);
            t.find('a').attr('href', APP_URL+'/rdr/'+value.id+'?trg='+value.url);
            var defaultSrc = APP_URL+'/public/images/'+bannerType+'.png';
            if(value.mid !== null){
                var meta = JSON.parse(value.meta_json);
                defaultSrc = APP_URL+'/storage/app/public/media/'+value.mid+'/image/'+meta.filename;
            }
            t.find('img').attr({'src' : defaultSrc, 'alt' : value.title});
        }
    }
    /* Common function to get banners at regular interval ends here*/
