Hi %name%,
<br/>
<br/>
New podcasts are available for your favorite content:  
<br/>
<br/>
%podcast_urls%
<br/>
<br/>
<br/>
To unsubscribe go to your preferences and uncheck unwanted email notifications.
<br/>
<br/>
---
<br/>
<br/>
This message has been generated automatically - please do not reply.

