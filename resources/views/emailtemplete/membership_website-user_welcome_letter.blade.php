<table class="container">
    <tr>
        <td>          
            <table class="row">
                <tr>
                    <td class="wrapper last">

                        <table class="twelve columns">
                            <tr>
                                <td>
                                    <h1 class="head">
                                        Transformation Radio Network
                                    </h1>
                                </td>
                            </tr>
                        </table>

                        <table class="twelve columns">
                            <tr>
                                <td>                                    
                                    <p>
                                        Hi %Member.fullname%,<br><br>
                                      Your membership <b> %Membership.name% </b> 
                                      has been activated and will be valid to <b>%Membership.expire%</b><br>
                                      Once expired you will be able to re-new it or change to the other.
                                        <br><br>
                                        Check some of bonus items from your current memebrship:  
                                    </p>
                                </td>
                                <td class="expander"></td>
                            </tr>
                            <tr>
                                <td>                                    
                                    <?php //echo $this->Membership->viewBonusItemsForEmail('alltime'); ?>
                                    All Time Bonus
                                </td>
                                <td class="expander"></td>
                            </tr>
                        </table>
                        <table class="twelve columns">
                            <tr>
                                <td>
                                    <p>
                                        All your bonus items you will find on this url:<br> 
                                        <a href="{{ url('/membership-bonus-items.html') }} ">
                                            {{ url('/membership-bonus-items.html') }}
                                        </a>
                                        <br>
                                    </p>
                                </td>
                                <td class="expander"></td>
                            </tr>  
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table class="footer-wrap">
    <tr>
        <td></td>
        <td class="container">
            <div class="content">
                <table>
                    <tr>
                        <td align="center">
                            <p class="foot">&copy; 2018 The Dr. Pat Show Network &amp; Transformation Talk Radio Network ALL RIGHTS RESERVED</p>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table>
