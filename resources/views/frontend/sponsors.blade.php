@extends('frontend.layouts.front')

@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-title-box">
                    <h4 class="page-title">Sponsors </h4>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>	   
	    <div class="List__sponsor__index">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-20">
                        <div class="row blog-column-wrapper">
                        	@if($sponsors->count() > 0)
	                        	@foreach($sponsors as $sponsor)
	                            <div class="col-md-4">
	                                <!-- Image Post -->
	                                <div class="blog-post blog-post-column m-b-20">
	                                    <div class="post-image col-md-5">	                                    	
	                                        @if(!empty($sponsor->media_id))
	                                    		<img src="{{asset('storage/app/public/media/'.$sponsor->media_id.'/'.$sponsor->media_type.'/'.$sponsor->media_file)}}" alt="" class="img-responsive">
	                                    	@else
	                                        	<img src="{{asset('public/assets/images/blog/1.jpg')}}" alt="" class="img-responsive">
	                                        @endif	                                       
	                                       
	                                    </div>
	                                    <div class="p-20">
	                                       	<div class="text-muted">
	                                       		<span>{{$sponsor->name}} </span> 
	                                       	</div>
	                                        <div class="post-title">
	                                            <h3><a href="javascript:void(0);" data-id="{{$sponsor->id}}">{{$sponsor->title}}</a></h3>
	                                        </div>
	                                       
	                                        <div>{{strip_tags($sponsor->Sponsor__short_description)}}
	                                        </div>
	                                        <div class="text-right">
	                                            <a href="javascript:void(0);" class="btn btn-success btn-sm waves-effect waves-light">View Sponsor <i class="mdi mdi-arrow-right m-l-5"></i></a>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            @endforeach
                            @endif
                        </div>
                        <!-- end row -->
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->
	    </div>
	    <!-- Pagination Navs-->
	    <div class="row page-navigation">
	      	<div class="col-md-12">
	       		{{ $sponsors->links() }}
	      	</div>
	    </div>
	</div>
</div>

<script type="text/javascript">
  	$(function() {
	// Ajax pagination code starts here
    $('body').on('click', '.page-link', function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if(href != undefined)
        {
            // Parameters information
            // 1 - Url to be hit in ajax
            // 2 - Object of DOM element to which html will be replaced from ajax response
            // 3 - Additional querystring added in url
            // 4 - Object of DOM element to which pagination will be replaced from ajax response
            ajax_pagination(href, $('.List__sponsor__index'), '?m=sponsor');

        }
    })

});
</script>
@endsection
