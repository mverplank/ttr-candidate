@push('my_scripts')
    <script type="text/javascript">
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-title-box">
                    <h4 class="page-title">Archived Episodes </h4>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>	
		<!-- Filters -->
		<div class="row">   
			<div class="card-box">            
	            <div class="form-group row"> 
	                <div class="col-md-3">
	                    {{Form::select('data[Archive][f][channels_id]', (!empty($all_channels) ? $all_channels : ''), !empty($selected_channel) ? $selected_channel : '', $attributes=array('id'=>'ArchiveFChannelsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
	                </div>                    
	                <div class="col-md-3">
	                    {{Form::select('data[Archive][f][shows_id]', (!empty($all_shows) ? $all_shows : ''), !empty($selected_show) ? $selected_show : '', $attributes=array('id'=>'ArchiveFShowsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-custom'))}}
	                </div>
	                <div class="col-md-3">
	                    {{Form::select('data[Archive][f][host_ids]', (!empty($all_hosts) ? $all_hosts : ''), !empty($selected_host) ? $selected_host : '', $attributes=array('id'=>'ArchiveFHostIds', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
	                </div>
	                <div class="col-md-3">
	                    {{Form::select('data[Archive][f][year]', (!empty($years) ? $years : ''), !empty($selected_year) ? $selected_year : '', $attributes=array('id'=>'ArchiveFYear', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-pink'))}}
	                </div>
	            </div>
	           
	            <div class="form-group row page_top_btns">                
	                <div class="col-md-3">
	                    {{Form::select('data[Archive][f][month]', (!empty($months) ? $months : ''), !empty($selected_month) ? $selected_month : '', $attributes=array('id'=>'ArchiveFMonth', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-info'))}}
	                </div>
	                <div class="col-md-3">                        
	                    {!!$all_categories!!}
	                </div>    
	                <div class="col-md-5">   
	                	<div class="form-group">		                	
	                        <div class="input-group">
                                <span class="input-group-btn">
                               		<button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-times" id="remove_search"></i></button>
                                </span>
                                <input type="text" id="search_archive" name="example-input1-group2" class="form-control" placeholder="Search for topic">                                
                                <span class="input-group-btn">
                                	<button type="button" class="btn waves-effect waves-light btn-primary" id="search_archive_btn"><i class="fa fa-search"></i></button>
                                </span>
                                
                            </div>
	                    </div>
	                </div>            
	            </div>   
	        </div>
	    </div>
        <!-- End Row -->
	    <div class="List__archive__index">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-20">
                        <div class="row blog-column-wrapper">
                        	@if($archived_episodes->count() > 0)
	                        	@foreach($archived_episodes as $archived_episode)
	                        	@php
	                        	/*echo '<pre>';
	                        	print_r($archived_episode);
	                        	echo '<pre>';
	                        		die;*/
	                        	//$hostURL = url('/archive/'.str_slug($host->User__fullname, '-').','.$host->id.'.html');
	                        	//$episodeURL = url('/archive/'.str_slug($host->User__fullname, '-').','.$host->id.'.html');
	                        	@endphp
	                            <div class="col-md-4">
	                                <!-- Image Post -->
	                                <div class="blog-post blog-post-column m-b-20">
	                                    <div class="post-image">
	                                    	@if(!empty($archived_episode->media_id))
	                                    		<img src="{{asset('storage/app/public/media/'.$archived_episode->media_id.'/'.$archived_episode->media_type.'/'.$archived_episode->media_file)}}" alt="" class="img-responsive">
	                                    	@else
	                                        	<img src="{{asset('public/assets/images/blog/1.jpg')}}" alt="" class="img-responsive">
	                                        @endif
	                                        <!-- <span class="label label-danger">Lifestyle</span> -->
	                                    </div>
	                                    <div class="p-20">
	                                        <div class="text-muted"><span>Host: <a class="text-dark font-secondary">{{$archived_episode->hosts}}</a></span> <span>{{date("M d,Y", strtotime($archived_episode->datetime))}}</span></div>
	                                        <div class="post-title">
	                                            <h3><a href="javascript:void(0);" data-id="{{$archived_episode->id}}">{{$archived_episode->title}}</a></h3>
	                                        </div>
	                                        @if($archived_episode->guests)
	                                        	<div class="text-muted">
                                        			<span>Guests: <a class="text-dark font-secondary">{{$archived_episode->guests}}</a>
                                        			</span>
                                        		</div>
	                                        @endif
	                                        <div>{{strip_tags(str_limit($archived_episode->description, $limit = 150, $end = '...'))}}
	                                        </div>
	                                        <div class="text-right">
	                                            <a href="javascript:void(0);" class="btn btn-success btn-sm waves-effect waves-light">Read More <i class="mdi mdi-arrow-right m-l-5"></i></a>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            @endforeach
                            @endif
                        </div>
                        <!-- end row -->
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->
	    </div>
	    <!-- Pagination Navs-->
	    <div class="row page-navigation">
	      	<div class="col-md-12">
	       		{{ $archived_episodes->links() }}
	      	</div>
	    </div>
	</div>
</div>
<script type="text/javascript">
  	$(function() {
  		var query_str = {};
  		//if(href.indexOf(qstring) != -1){
		// Ajax pagination code starts here
		    $('body').on('click', '.page-link', function(e){
		        e.preventDefault();
		        var href = $(this).attr('href');
		        var year       = 0;
		        var month      = 0;
		        var show_id    = 0;
		        var channel_id = 0;
		        var host_id    = 0;
		        var cat_id     = 0;
		        var search     = ''; 
		        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
		        show_id        = $('#ArchiveFShowsId').find(":selected").val();
		        year           = $('#ArchiveFYear').find(":selected").val();
		        month          = $('#ArchiveFMonth').find(":selected").val();
		        host_id        = $('#ArchiveFHostIds').find(":selected").val();
		        cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();

		        // Check for the channel selected
		        if(channel_id != ''){
		    		query_str['channel_id'] = channel_id;
		    	}else{
		    		if('channel_id' in query_str){
		    			delete query_str['channel_id'];
		    		}
		    	} 
		    	// Check for the show selected
		    	if(show_id != ''){
		    		query_str['show_id'] = show_id;
		    	}else{
		    		if('show_id' in query_str){
		    			delete query_str['show_id'];
		    		}
		    	} 
		    	// Check for the host selected
		    	if(host_id != ''){
		    		query_str['host_id'] = host_id;
		    	}else{
		    		if('host_id' in query_str){
		    			delete query_str['host_id'];
		    		}
		    	}
		    	// Check for the year selected
		    	if(year != ''){
		    		query_str['year'] = year;
		    	}else{
		    		if('year' in query_str){
		    			delete query_str['year'];
		    		}
		    	} 

		    	// Check for the month selected
		    	if(month != ''){
		    		query_str['month'] = month;
		    	}else{
		    		if('month' in query_str){
		    			delete query_str['month'];
		    		}
		    	} 

		    	// Check for the category selected
		    	if(cat_id != ''){
		    		query_str['cat_id'] = cat_id;
		    	}else{
		    		if('cat_id' in query_str){
		    			delete query_str['cat_id'];
		    		}
		    	} 	

				// Check for the search selected
		    	if(search != ''){
		    		query_str['search'] = search;
		    	}else{
		    		if('search' in query_str){
		    			delete query_str['search'];
		    		}
		    	} 
		    	make_query_str = $.param(query_str);
		    	console.log(make_query_str);
		    	//href = $.param(query_str);
		    	
		    	//console.log(query_str);
		    	//Add querystring to url
		    	/*if(make_query_str.length > 0){
			    	if (history.pushState) {
				        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
				        window.history.pushState({path:newurl},'',newurl);
				    }
				}*/
				//var href = APP_URL+'/archives?'+make_query_str;
				href = href + make_query_str
		        var data = {'channel_id':channel_id, 'show_id':show_id,  'host_id':host_id, 'year':year, 'month':month, 'search':search, 'ajax':1};
		        
		        if(href != undefined)
		        {
		            // Parameters information
		            // 1 - Url to be hit in ajax
		            // 2 - Object of DOM element to which html will be replaced from ajax response
		            // 3 - Additional querystring added in url
		            // 4 - Object of DOM element to which pagination will be replaced from ajax response
		            ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
		        }
		    });
		//}
		// Fetch the archive episodes by filtering the channels.
	    $("#ArchiveFChannelsId").change(function() {
	    	var channel_id = $(this).val();
	    	var show_id  = '';
	    	var host_id  = '';
	    	var year     = '';
	    	var month    = '';
	    	var cat_id   = '';
	    	var search   = ''; 
	        show_id      = $('#ArchiveFShowsId').find(":selected").val();
	        host_id      = $('#ArchiveFHostIds').find(":selected").val();
	        year         = $('#ArchiveFYear').find(":selected").val();
		    month        = $('#ArchiveFMonth').find(":selected").val();
		    cat_id       = $('#ArchiveFCategoriesIds').find(":selected").val();
	       
	        // Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
	    		if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
		    	}
	    	}*/
	    	
	    	
	        var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	        
	        var href = APP_URL+'/archives?'+make_query_str;

	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $("#ArchiveFShowsId").change(function() {
	        var show_id    = $(this).val();
	        var channel_id = '';
	        var host_id    = '';
	        var year       = '';
	        var month      = '';
	        var cat_id     = '';
	        var search     = ''; 
	        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
	        host_id        = $('#ArchiveFHostIds').find(":selected").val();
	        year           = $('#ArchiveFYear').find(":selected").val();
		    month          = $('#ArchiveFMonth').find(":selected").val();
		    cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();
	       
	        // Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}*/

	        var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	
	        var href = APP_URL+'/archives?'+make_query_str;
	        /* if(channel_id != ''){
	        	href +='?channel_id';
	        } */       
	        

	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $("#ArchiveFHostIds").change(function() {
	        var host_id    = $(this).val();
	        var channel_id = '';
	        var show_id    = '';
	        var year       = '';
	        var month      = '';
	        var cat_id     = '';
	        var search     = ''; 
	        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
	        show_id        = $('#ArchiveFShowsId').find(":selected").val();
	        year           = $('#ArchiveFYear').find(":selected").val();
		    month          = $('#ArchiveFMonth').find(":selected").val();
		    cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();
	        
	        // Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}*/

	        var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	        
	        var href = APP_URL+'/archives?'+make_query_str;

	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $("#ArchiveFYear").change(function() {
	        var year       = $(this).val();
	        var channel_id = '';
	        var show_id    = '';
	        var host_id    = '';
	        var month      = '';
	        var cat_id     = '';
	        var search     = ''; 
	        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
	        show_id        = $('#ArchiveFShowsId').find(":selected").val();
	        host_id        = $('#ArchiveFHostIds').find(":selected").val();
	        month          = $('#ArchiveFMonth').find(":selected").val();
	        cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();

	        // Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}*/

	        var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	        
	        var href = APP_URL+'/archives?'+make_query_str;

	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $("#ArchiveFMonth").change(function() {
	        var month      = $(this).val();
	        var channel_id = '';
	        var show_id    = '';
	        var host_id    = '';
	        var year       = '';
	        var cat_id     = '';
	        var search     = ''; 
	        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
	        show_id        = $('#ArchiveFShowsId').find(":selected").val();
	        host_id        = $('#ArchiveFHostIds').find(":selected").val();
	        year           = $('#ArchiveFYear').find(":selected").val();
	        cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();

	        // Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}*/
	        var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	        
	        var href = APP_URL+'/archives?'+make_query_str;

	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $("#ArchiveFCategoriesIds").change(function() {
	        var cat_id     = $(this).val();
	        var channel_id = '';
	        var show_id    = '';
	        var host_id    = '';
	        var year       = '';
	        var month      = '';
	        var search     = ''; 
	        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
	        show_id        = $('#ArchiveFShowsId').find(":selected").val();
	        host_id        = $('#ArchiveFHostIds').find(":selected").val();
	        year           = $('#ArchiveFYear').find(":selected").val();
	        month          = $('#ArchiveFMonth').find(":selected").val();

	        // Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}

	        var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	        
	        var href = APP_URL+'/archives?'+make_query_str;

	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $('#search_archive_btn').click(function(){
	    	var search     = $('#search_archive').val();
	    	var cat_id     = '';
	        var channel_id = '';
	        var show_id    = '';
	        var host_id    = '';
	        var year       = '';
	        var month      = '';
	        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
	        show_id        = $('#ArchiveFShowsId').find(":selected").val();
	        host_id        = $('#ArchiveFHostIds').find(":selected").val();
	        year           = $('#ArchiveFYear').find(":selected").val();
	        month          = $('#ArchiveFMonth').find(":selected").val();
	        cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();

	    	// Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}*/

	    	var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};	        
	        var href = APP_URL+'/archives?'+make_query_str;
	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);
	    });

	    $('#remove_search').click(function(){
	    	if($('#search_archive').val() != ''){
	    		$('#search_archive').val('');
		    	var search     = '';
		    	var cat_id     = '';
		        var channel_id = '';
		        var show_id    = '';
		        var host_id    = '';
		        var year       = '';
		        var month      = '';
		        channel_id     = $('#ArchiveFChannelsId').find(":selected").val();
		        show_id        = $('#ArchiveFShowsId').find(":selected").val();
		        host_id        = $('#ArchiveFHostIds').find(":selected").val();
		        year           = $('#ArchiveFYear').find(":selected").val();
		        month          = $('#ArchiveFMonth').find(":selected").val();
		        cat_id         = $('#ArchiveFCategoriesIds').find(":selected").val();

		    	// Check for the channel selected
	        if(channel_id != ''){
	    		query_str['channel_id'] = channel_id;
	    	}else{
	    		if('channel_id' in query_str){
	    			delete query_str['channel_id'];
	    		}
	    	} 
	    	// Check for the show selected
	    	if(show_id != ''){
	    		query_str['show_id'] = show_id;
	    	}else{
	    		if('show_id' in query_str){
	    			delete query_str['show_id'];
	    		}
	    	} 
	    	// Check for the host selected
	    	if(host_id != ''){
	    		query_str['host_id'] = host_id;
	    	}else{
	    		if('host_id' in query_str){
	    			delete query_str['host_id'];
	    		}
	    	}
	    	// Check for the year selected
	    	if(year != ''){
	    		query_str['year'] = year;
	    	}else{
	    		if('year' in query_str){
	    			delete query_str['year'];
	    		}
	    	} 

	    	// Check for the month selected
	    	if(month != ''){
	    		query_str['month'] = month;
	    	}else{
	    		if('month' in query_str){
	    			delete query_str['month'];
	    		}
	    	} 

	    	// Check for the category selected
	    	if(cat_id != ''){
	    		query_str['cat_id'] = cat_id;
	    	}else{
	    		if('cat_id' in query_str){
	    			delete query_str['cat_id'];
	    		}
	    	} 	

			// Check for the search selected
	    	if(search != ''){
	    		query_str['search'] = search;
	    	}else{
	    		if('search' in query_str){
	    			delete query_str['search'];
	    		}
	    	} 
	    	make_query_str = $.param(query_str);
	    	
	    	//Add querystring to url
	    	/*if(make_query_str.length > 0){
		    	if (history.pushState) {
			        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + make_query_str;
			        window.history.pushState({path:newurl},'',newurl);
			    }
			}*/

	    	var data = {'channel_id':channel_id, 'show_id':show_id, 'host_id':host_id, 'year':year, 'month':month, 'category_id':cat_id, 'search':search, 'ajax':1};        
	        var href = APP_URL+'/archives?'+make_query_str;
	        ajax_pagination(href, $('.List__archive__index'), '?m=archive', '', data);

	    	}
	    });
   });
</script>

@endsection
