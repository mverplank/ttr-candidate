@push('my_scripts')
    <link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 8;
            }
            $('.slider3').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:4,
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                           <a class="menu-item" href="{{url('/')}}">
                                <i class="fa fa-home" aria-hidden="true"></i> &nbsp;Home
                            </a>
                        </li>
                        <li class="active">
                            <a class="menu-item" href="#">
                                Company History
                            </a>
                       
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-6">
               {!! $companyhistory !!}
            </div>
            <div class="col-md-6">
                    @isset($slider2)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($slider2 as $key => $slide)
                                @php
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                            <li>
                                <div class="post-image col-md-12 slide_image">
                                    <a href="{{ $slide->url }}">
                                        <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                    </a>
                                </div>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                @endisset
                {!! $slider_h2 !!}
                @isset($slider3)                            
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider slider3 ">
                        <ul class="slides">
                            @foreach($slider3 as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                            <li>
                                <div class="slide_image">
                                    <a href="{{ $slide->url }}">
                                        <img class="max_height" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                    </a>
                                </div>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                @endisset
            </div>
        </div>
    </div>
@endsection
