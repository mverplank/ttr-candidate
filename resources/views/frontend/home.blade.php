@push('my_scripts')
    <link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 5;
            }
            $('#leader').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 5,
                animationLoop: true,
                move:4
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title"> Dashboard </h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @isset($top_banner)
            <div class="row">
                <div class="col-md-8">
                    @foreach($top_banner as $key => $tb)
                        {!! $tb !!}
                    @endforeach
                </div>
            </div>
        @endisset
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable boxed parentTabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#this_week">This Week</a></li>
                        <li><a href="#upcoming">Upcoming</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="this_week">
                            <div class="tabbable">
                                <ul  class="nav nav-pills">
                                    @foreach($this_week as $k => $v)
                                    <li @if($v == $today) class="active" @endif>
                                        <a href="#{{ strtolower($k) }}" data-toggle="tab">{{ ucfirst($k) }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content clearfix">
                                    @foreach($this_week as $k => $v)
                                        <div class="tab-pane @if($v == $today) active @endif" id="{{ strtolower($k) }}">
                                            @isset($fetch_schedule[$v])
                                                @if(count($fetch_schedule[$v]) > 0)
                                                    <div class="row" data-equalizer="data-equalizer" data-equalize-on="medium" data-resize="data-equalizer" data-mutate="data-equalizer" data-events="mutate">
                                                        @foreach($fetch_schedule[$v] as $sc)
                                                            @php
                                                                // If episode id is not empty, then show episode title else show show name
                                                                // This is already checked in module function

                                                                $title = preg_replace("/\[(.*?)\]/", '', $sc['title']);
                                                                
                                                                $time = date('g a', strtotime($sc['start']));
                                                                
                                                                // Generate manually SEO friendly slug from title
                                                                $slug = str_slug($title, '-'); 
                                                                $url = $slug.','.$sc['show_id'].'.html';

                                                                // Set default url to show detail page 
                                                                $url = url('/show-details/'.$url);

                                                                if(!empty($sc['episode_id']))
                                                                {
                                                                  //If this is episode, URL is set to episode detail page
                                                                  $url = $slug.','.$sc['episode_id'].'.html';
                                                                  $url = url('/episode/'.$url);
                                                                }

                                                                $imgURL = asset('storage/app/public/media/120x320.png');
                                                                if(isset($all_media[$sc['show_id']])){
                                                                  $media = $all_media[$sc['show_id']];
                                                                  if(file_exists(storage_path().'/app/public/media/'.$media['mid'].'/image/'.$media['meta_json']->filename)){
                                                                      $imgURL = asset("storage/app/public/media/".$media['mid'].'/image/'.$media['meta_json']->filename);
                                                                  }
                                                                }
                                                            @endphp
                                                            <div class="col-md-4 small-12 medium-6 large-4 columns end">
                                                                <div class="episode box" data-equalizer-watch="data-equalizer-watch" style="height: 350px;">
                                                                    <a href="{{ $url }}">
                                                                        <div class="image">
                                                                            <img alt="{{ $title }}" data-original="/thumb/Spirit-Fire-Radio-with-Hosts-Steve-Kramer-Dorothy-Riddle_Show_159_player.jpg" src="{{ $imgURL }}" class="lldef lld" style="">
                                                                        </div>
                                                                        <div class="show-content">
                                                                            <h3 class="time"><i class="fa fa-clock-o"></i> {{ $time }} PDT</h3>
                                                                            <h3 class="title">{{ $title }}</h3>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            @endisset
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="upcoming">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
            $carousel_titles = ['Special Offers From Our Hosts!', 'Don\'t Miss! New Premiere Shows!'];
            $i = 0
        @endphp
        @foreach($slider as $index => $slider)
            @isset($slider)
                <div class="row">

                    <div class="col-md-4">
                        <h3>{{ $carousel_titles[$i] }}</h3>
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider sliders">
                            <ul class="slides">
                                @foreach($slider as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                    @endphp
                                    <li>
                                        <div class="slide_image">
                                            <a href="{{ $slide->url }}">
                                                <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div>
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                </div>
            @endisset
            @php
                $i++;
            @endphp
        @endforeach
        
        @isset($leaders_of_the_month)
            <div class="row">
                <div class="col-md-6">
                    <h3>Transformational Leaders of the Month</h3>
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider" id="leader">
                        <ul class="slides">
                            @foreach($leaders_of_the_month as $key => $leader)
                                @php
                                    /*echo '<pre>';
                                    print_r($leader);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/150x200.jpg');
                                    if(!empty($leader->meta_json)){
                                        $t = json_decode($leader->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$leader->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$leader->mid.'/image/'.$t->filename);
                                        }
                                    }
                                    $url = url('/guest/'.str_slug($leader->fullname, '-').','.$leader->id.'.html');
                                @endphp
                                <li>
                                    <div class="slide_image">
                                        <a href="{{ $url }}">
                                            <img src="{{ $imgURL }}" alt="{{ $leader->fullname }}" />
                                        </a>
                                    </div>
                                    <div class="slide_content">
                                        <a href="{{ $url }}">
                                            {{ $leader->fullname }}
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
            </div>
        @endisset

        @isset($network_sponsors)
            <div class="row">
                <div class="col-md-8">
                    <h3>Thank You Network Sponsors!</h3>
                    @foreach($network_sponsors as $key => $sponsor)
                        {!! $sponsor !!}
                    @endforeach
                </div>
            </div>
        @endisset
    </div>
</div>
<script type="text/javascript">
    var ajaxArr = [];
    var channel_id = '{{ $channel_id }}';
    $("ul.nav-tabs a").click(function (e) {
        e.preventDefault();  
        $(this).tab('show');

        var href = $(this).attr('href');
        href = href.replace('#', '', href);
        if(href == 'upcoming')
        {
            if($.inArray(href, ajaxArr) === -1)
            {
                ajaxArr.push(href);
                //Fetch results from DB
                // 1 = Div ID
                // 2 = type ( sun-sat or upcoming)
                // 3 = channel_id
                var type = 'upcoming'
                get_zapbox_results(href, type, channel_id);
            }
        }
    });

    // Fetch other tabs scheduled episodes/shows
    $("ul.nav-pills a").click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        href = href.replace('#', '', href);
        $(this).tab('show');
        if($.inArray(href, ajaxArr) === -1)
        {
            ajaxArr.push(href);
            //Fetch results from DB
            // 1 = Div ID
            // 2 = type ( sun-sat or upcoming)
            // 3 = channel_id
            var type = 'upcoming'
            if($.inArray(href, ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']) !== -1)
                type = 'get'
            get_zapbox_results(href, type, channel_id);
        }
    });
</script>
@endsection