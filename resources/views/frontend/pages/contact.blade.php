@extends('frontend.layouts.front')

@section('content')
<div class="content">
	<div class="container">
	    <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Contact Us</h4>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
               <p style="font-size: 160%;">Thank you for your interest in Transformation Talk Radio. We appreciate your comments and your feedback about this site, and we also look forward to answering any questions you may have. Whether you're interested in our hosting options, sponsorship, or just wanting to leave feedback, your opinion matters to us. Please either email us at comments@thedrpatshow.com or call 855-393-3742.</p>
           </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <script type="text/javascript" src="https://form.jotform.us/jsform/50904850226149"></script>
            </div>
        </div>
	</div>
</div>
@endsection
