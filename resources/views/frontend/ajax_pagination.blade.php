    {{ $paginator->links() }}
    @if(!$paginator->onFirstPage())
        <script type="text/javascript">
            $(document).ready(function(){
                //if(!$('body').find('a.page-link:eq(1)').hasClass('filter-link'))
                //console.log($('body').find('.pagination').children().children('.page-link'));
                $('body').find('.pagination').children().children('.page-link').eq(0).attr('href', '{{ $route }}?m={{$page_type}}')
                $('body').find('.pagination').children().children('.page-link').eq(1).attr('href', '{{ $route }}?m={{$page_type}}')
            });
        </script>
    @endif