@extends('frontend.layouts.front')

@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-title-box">
                    <h4 class="page-title">Archived Episodes </h4>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>	   
	    <div class="List__archive__index">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-20">
                        <div class="row blog-column-wrapper">
                        	@if($archived_episodes->count() > 0)
	                        	@foreach($archived_episodes as $archived_episode)
	                            <div class="col-md-4">
	                                <!-- Image Post -->
	                                <div class="blog-post blog-post-column m-b-20">
	                                    <div class="post-image">
	                                    	@if(!empty($archived_episode->media_id))
	                                    		<img src="{{asset('storage/app/public/media/'.$archived_episode->media_id.'/'.$archived_episode->media_type.'/'.$archived_episode->media_file)}}" alt="" class="img-responsive">
	                                    	@else
	                                        	<img src="{{asset('public/assets/images/blog/1.jpg')}}" alt="" class="img-responsive">
	                                        @endif
	                                        <!-- <span class="label label-danger">Lifestyle</span> -->
	                                    </div>
	                                    <div class="p-20">
	                                        <div class="text-muted"><span>Host: <a class="text-dark font-secondary">{{$archived_episode->hosts}}</a></span> <span>{{date("M d,Y", strtotime($archived_episode->datetime))}}</span></div>
	                                        <div class="post-title">
	                                            <h3><a href="javascript:void(0);" data-id="{{$archived_episode->id}}">{{$archived_episode->title}}</a></h3>
	                                        </div>
	                                        @if($archived_episode->guests)
	                                        	<div class="text-muted">
                                        			<span>Guests: <a class="text-dark font-secondary">{{$archived_episode->guests}}</a>
                                        			</span>
                                        		</div>
	                                        @endif
	                                        <div>{{strip_tags(str_limit($archived_episode->description, $limit = 150, $end = '...'))}}
	                                        </div>
	                                        <div class="text-right">
	                                            <a href="javascript:void(0);" class="btn btn-success btn-sm waves-effect waves-light">Read More <i class="mdi mdi-arrow-right m-l-5"></i></a>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            @endforeach
                            @endif
                        </div>
                        <!-- end row -->
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->
	    </div>
	    <!-- Pagination Navs-->
	    <div class="row page-navigation">
	      	<div class="col-md-12">
	       		{{ $archived_episodes->links() }}
	      	</div>
	    </div>
	</div>
</div>
<script type="text/javascript">
  	$(function() {
		// Ajax pagination code starts here
	    $('body').on('click', '.page-link', function(e){
	        e.preventDefault();
	        var href = $(this).attr('href');
	        if(href != undefined)
	        {
	            // Parameters information
	            // 1 - Url to be hit in ajax
	            // 2 - Object of DOM element to which html will be replaced from ajax response
	            // 3 - Additional querystring added in url
	            // 4 - Object of DOM element to which pagination will be replaced from ajax response
	            ajax_pagination(href, $('.List__archive__index'), '?m=archive');
	        }
	    })
   });
</script>

@endsection
