@push('my_scripts')
	<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.guests').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Testimonials</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h2 style="font-size: 200%; text-align: center;">Stories of Transformation from Our Blog</h2>
                <h3 style="font-size: 150%; text-align: center;"><em>The goal of our blog is to highlight stories of transformation. Our hosts and co-hosts share thoughts, ideas, and solutions you may not have received in their shows. We also feature other enlightened sources from around the globe to acknowledge the great work they do in the world. Our listeners and followers are our number one priority. We do this and show up everyday because of you all.&nbsp;</em></h3>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<h3>What Listeners Have to Say</h3>
                <div class="p-20">
                    <div class="row blog-column-wrapper">
                        @isset($blogs)
                            @foreach($blogs as $key => $slide)
                                @php
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                    $url = url(str_slug($slide->title, '-').','.$slide->id.'.html');
                                @endphp
                                <div class="col-md-3">
                                    <!-- Image Post -->
                                    <div class="blog-post blog-post-column m-b-20">
                                        <div class="post-image col-md-12">                                           
                                            <a href="{{ $url }}">
                                                <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div>
                                        <div class="p-20">
                                            <div class="post-title">
                                                <h3><a href="{{$url}}">{{$slide->title}}</a></h3>
                                            </div>
                                        </div>
                                        <div class="block-with-text">{!! $slide->content !!}</div>
                                    </div>
                                </div>
                            @endforeach
                                <!-- </ul>
                            </div> -->
                        @endisset
                    </div>
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h3>Sponsors and Advertisers</h3>
        		@isset($network_sponsors)
                    @foreach($network_sponsors as $key => $sponsor)
                        {!! $sponsor !!}
                    @endforeach
		        @endisset
        	</div>
        </div>
    </div>
</div>
@endsection