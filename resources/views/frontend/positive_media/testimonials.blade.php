@push('my_scripts')
	<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.guests').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Testimonials</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @isset($top_slider)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($top_slider as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    <div class="post-image col-md-12 slide_image">
                                        <a href="{{ $slide->url }}">
                                            <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-9">
        		<h3>What Listeners Have to Say</h3>
                <div class="p-20">
                    <div class="row blog-column-wrapper">
                        @isset($what_clients_are_saying)
                            <!-- Place somewhere in the <body> of your page -->
                            <!-- <div class="">
                                <ul class="slides"> -->
                            @foreach($what_clients_are_saying as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                    $url = url(str_slug($slide->title, '-').','.$slide->id.'.html');
                                @endphp
                                <div class="col-md-3">
                                    <!-- Image Post -->
                                    <div class="blog-post blog-post-column m-b-20">
                                        <div class="post-image col-md-5">                                           
                                            <a href="{{ $url }}">
                                                <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div>
                                        <div class="p-20">
                                            <div class="post-title">
                                                <h3><a href="{{$url}}">{{$slide->title}}</a></h3>
                                            </div>
                                            <div>{!! $slide->content !!}</div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                                <!-- </ul>
                            </div> -->
                        @endisset
                    </div>
                </div>
        	</div>
            <div class="col-md-3">
                <h3>What Listeners Have to Say</h3>
                @isset($what_listeners_have_to_say)
                            
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($what_listeners_have_to_say as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    {{-- <div class="slide_image">
                                        <a href="{{ $slide->url }}">
                                            <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div> --}}
                                    <div class="slide_content">
                                        <a href="{{ $slide->url }}">
                                            {{ $slide->title }}
                                        </a>
                                    </div>
                                    <div class="slide_content">
                                        <a href="{{ $slide->url }}">
                                            {!! $slide->content !!}
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        
        <div class="row">
            <h3>Testimonials From Network Guests</h3>
            <div class="col-md-12">
                @isset($testimonials_from_network_guests)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider guests">
                        <ul class="slides">
                            @foreach($testimonials_from_network_guests as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    <div class="post-image col-md-12 slide_image max_height">
                                        <a href="{{ $slide->url }}">
                                            <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                    <div class="slide_content">
                                        <a href="{{ $slide->url }}">
                                            {{ $slide->title }}
                                        </a>
                                    </div>
                                    <div class="slide_content block-with-text">
                                        {!! $slide->content !!}
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h3>Thank You Network Sponsors!</h3>
        		@isset($network_sponsors)
                    @foreach($network_sponsors as $key => $sponsor)
                        {!! $sponsor !!}
                    @endforeach
		        @endisset
        	</div>
        </div>
    </div>
</div>
@endsection