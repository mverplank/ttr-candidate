@push('my_scripts')
	<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 4;
            }
            $('.featured_articles').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:4
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Positive Media</h4>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h2>Welcome to the Transformation Talk Radio Newsletter!</h2>
        		<p>It is our goal to continue to spread positive messages in any way that we can. Here you will find out about upcoming events, amazing articles we've found on the web, who is new to the network, and any special offerings exclusive to our listeners! We hope you enjoy listening and browsing the tools and resources available.</p>
        		<p>If you have questions or comments, we LOVE to hear from you! Tell us how we are doing, share information and resources you think we need to have. We are an open door and encourage anyone and everyone to be involved.</p>
        		<p>Stay tuned... our Positive Media Mojo project is right around the corner. A new innovative way to share positive messages through a community based app.</p>
        	</div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-12">
            	<div class="col-md-4">
                   	<a class="btn btn-primary" href="{{ route('front_events') }}">Events</a>
               	</div>
               	<div class="col-md-4">
               		<a class="btn btn-primary" href="{{ route('front_resources') }}">Resources</a>
               	</div>
               	<div class="col-md-4">
               		<a class="btn btn-primary" href="{{ route('front_blog') }}">Blog</a>
               	</div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		
	        	<div class="col-md-8">
			        @foreach($dont_want_to_miss as $index => $miss)
			            @isset($miss)
	                        <h3>Things You Don't Want to Miss!</h3>
	                        <!-- Place somewhere in the <body> of your page -->
	                        <div class="flexslider sliders">
	                            <ul class="slides">
	                                @foreach($miss as $key => $slide)
	                                    @php
	                                        /*echo '<pre>';
	                                        print_r($slide);
	                                        echo '</pre>';*/
	                                        $imgURL = asset('public/images/600x600.jpg');
	                                        if(!empty($slide->meta_json)){
	                                            $t = json_decode($slide->meta_json);
	                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
	                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
	                                            }
	                                        }
	                                    @endphp
	                                    <li>
	                                         <div class="slide_image">
	                                            <a href="{{ $slide->url }}">
	                                                <img height="300" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
	                                            </a>
	                                        </div> 
	                                    </li>
	                                @endforeach
	                                </ul>
	                            </div>
	                        </div>
			            @endisset
			        @endforeach
			    </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
		        @foreach($hot_off_the_press as $index => $press)
		            @isset($press)
						<h3>Hot Off The Press!</h3>
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider featured_articles">
                            <ul class="slides">
                                @foreach($press as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                    @endphp
                                    <li>
                                         <div class="slide_image">
                                            <a href="{{ $slide->url }}">
                                                <img width="200px" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div> 
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
		            @endisset
		        @endforeach
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-4">
				@foreach($spesial_offers as $index => $so)
		            @isset($so)
                        <h3>Special Offers For YOU!</h3>
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider sliders">
                            <ul class="slides">
                                @foreach($so as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';
                                        die;*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                    @endphp
                                    <li>
                                         <div class="slide_image">
                                            <a href="{{ $slide->url }}">
                                                <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div> 
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                        <div class="slide_content">
                                           {!! $slide->short_description !!}
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                        </div>
		            @endisset
		        @endforeach
			</div>
			<div class="col-md-8">
		        @foreach($featured_articles as $index => $fa)
		            @isset($fa)
                        <h3>Featured Articles</h3>
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider featured_articles">
                            <ul class="slides">
                                @foreach($fa as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                    @endphp
                                    <li>
                                         <div class="slide_image">
                                            <a href="{{ $slide->url }}">
                                                <img width="200px" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div> 
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {!! $slide->short_description !!}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
		            @endisset
		        @endforeach
		    </div>
		</div>
        <div class="row">
        	<div class="col-md-3">
        		<a class="btn btn-primary" href="{{ route('front_testimonials') }}"><h3>More Testimonials</h3></a>
        	</div>
        	<div class="col-md-9">
        		@php
		            $testimonial_titles = ['What People Are Saying!', 'Client Reviews!'];
		            $i = 0
		        @endphp
        		@foreach($testimonials as $index => $testimonial)
		            @isset($testimonial)
		                <div class="row">

		                    <div class="col-md-4">
		                        <h3>{{ $testimonial_titles[$i] }}</h3>
		                        <!-- Place somewhere in the <body> of your page -->
		                        <div class="flexslider sliders">
		                            <ul class="slides">
		                                @foreach($testimonial as $key => $slide)
		                                    @php
		                                        /*echo '<pre>';
		                                        print_r($slide);
		                                        echo '</pre>';*/
		                                        $imgURL = asset('public/images/600x600.jpg');
		                                        if(!empty($slide->meta_json)){
		                                            $t = json_decode($slide->meta_json);
		                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
		                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
		                                            }
		                                        }
		                                    @endphp
		                                    <li>
		                                        {{-- <div class="slide_image">
		                                            <a href="{{ $slide->url }}">
		                                                <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
		                                            </a>
		                                        </div> --}}
		                                        <div class="slide_content">
		                                            <a href="{{ $slide->url }}">
		                                                {{ $slide->title }}
		                                            </a>
		                                        </div>
		                                        <div class="slide_content">
		                                            <a href="{{ $slide->url }}">
		                                                {!! $slide->content !!}
		                                            </a>
		                                        </div>
		                                    </li>
		                                @endforeach
		                                </ul>
		                            </div>
		                        </div>
		                </div>
		            @endisset
		            @php
		                $i++;
		            @endphp
		        @endforeach
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-3">
        		<a class="btn btn-primary" href="{{ route('front_sponsors') }}"><h3>View Sponsors</h3></a>
        	</div>
        	<div class="col-md-9">
        		<h3>Thank You Network Sponsors!</h3>
        		@isset($network_sponsors)
                    @foreach($network_sponsors as $key => $sponsor)
                        {!! $sponsor !!}
                    @endforeach
		        @endisset
        	</div>
        </div>
    </div>
</div>
@endsection