@push('my_scripts')
	<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.guests').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Events</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p style="font-size: 160%;">The Transformation Radio Network supports and promote Transformational Global in-person and online events! The hosts and co-hosts who participate on the radio are leaders in the fields of human potential. Browse below the various events presented by these leaders! Participating in an event can open up your mind body spirit to a whole new experience and belief of "what's possible". We encourage you to come back often to see what's happening in the world of transformation. When you see something you resonate with take action!! If you want to get the inside scoop on upcoming events and promotions from our hosts, sign up for our newsletter!</p>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<h3>What Listeners Have to Say</h3>
                <div class="p-20">
                    <div class="row blog-column-wrapper">
                        @isset($events)
                            @foreach($events as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                    $url = !empty($slide->external_url)?$slide->external_url:(!empty($slide->url)?$slide->url:url(str_slug($slide->title, '-').','.$slide->id.'.html'));
                                @endphp
                                <div class="col-md-3">
                                    <!-- Image Post -->
                                    <div class="blog-post blog-post-column m-b-20">
                                        <div class="post-image col-md-12">                                           
                                            <a href="{{ $url }}" target="_blank">
                                                <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div>
                                        <div class="p-20">
                                            <div class="post-title">
                                                <h3><a href="{{$url}}" target="_blank">{{$slide->title}}</a></h3>
                                            </div>
                                        </div>
                                        <div class="block-with-text">{!! $slide->content !!}</div>
                                    </div>
                                </div>
                            @endforeach
                                <!-- </ul>
                            </div> -->
                        @endisset
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@endsection