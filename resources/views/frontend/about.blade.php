@push('my_scripts')
    <link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 8;
            }
            $('.outlets').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:8
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                           <a class="menu-item" href="{{url('/')}}">
                                <i class="fa fa-home" aria-hidden="true"></i> &nbsp;Home
                            </a>
                        </li>
                        <li class="active">
                            <a class="menu-item" href="#">
                                About Transformation Radio
                            </a>
                       
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @isset($top_slider)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($top_slider as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    <div class="post-image col-md-12 slide_image">
                                        <a href="{{ $slide->url }}">
                                            <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                @php /*
                @if($media->count() > 0)
                    @php ($url_count = 1)
                    @foreach($media as $med)
                        @if($med->sub_module == 'images')
                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:100%">
                            @php ($url_count++)
                        @endif
                    @endforeach
                @endif
                 */
                @endphp 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                   {!! $ourvision !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                   {!! $ourmission !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Reaching Nearly 100 Different Outlets!</h3>
                @isset($outlets)
                            
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider outlets ">
                        <ul class="slides">
                            @foreach($outlets as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    <div class="slide_image">
                                        <a href="{{ $slide->url }}">
                                            <img class="max_height" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        {{-- 
        <div class="row">
            <div class="col-xs-11 col-md-10 col-centered">
                <div id="about-silder1" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="5000">
                    <div class="carousel-inner">
                        @if($silder1->count() > 0)
                        @foreach($silder1 as $key =>$med)
                        @if(!($med->filename ==''))
                        <div class="item {{$key==0 ? 'active' : '' }}">
                            <div class="carousel-col">
                                <div class="img-responsive">
                                    <img src="{{asset('storage/app/public/media/'.$med->id.'/'.$med->type.'/'.$med->filename)}}" alt="" style="width:100%;">
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                    <!-- Controls -->
                    <div class="left carousel-control">
                        <a href="#about-silder1" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                    <div class="right carousel-control">
                        <a href="#about-silder1" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    {!! $yourvision !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    {!! $ourlisteners !!}
                </div>
            </div>
        </div>
        {{-- 
        <div class="row">
            <div class="col-xs-11 col-md-10 col-centered">
                <div id="about-silder2" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="5000">
                    <div class="carousel-inner">
                        @if($silder1->count() > 0)
                        @foreach($silder1 as $key =>$med)
                        @if(!($med->filename ==''))
                        <div class="item {{$key==0 ? 'active' : '' }}">
                            <div class="carousel-col">
                                <div class="img-responsive">
                                    <img src="{{asset('storage/app/public/media/'.$med->id.'/'.$med->type.'/'.$med->filename)}}" alt="" style="width:100%;">
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                    <!-- Controls -->
                    <div class="left carousel-control">
                        <a href="#about-silder2" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                    <div class="right carousel-control">
                        <a href="#about-silder2" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        --}}

        <div class="row">
            <div class="col-md-12">
                <h3>Thank You Network Sponsors!</h3>
                @isset($network_sponsors)
                    @foreach($network_sponsors as $key => $sponsor)
                        {!! $sponsor !!}
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
</div>
@endsection
