@extends('frontend.layouts.front')

@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-title-box">
                    <h4 class="page-title">Guests </h4>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>	
		<!-- End row -->
		<div class="row">
            <div class="small-12 columns">       
                <div class="list-top-filters">
                    @php
                        $lists = range('A', 'Z');
                    @endphp
                    @foreach($lists as $l)
                        <a href="{{ url('guests') }}?page=1&lastname={{ $l }}" class="page-link filter-link">
                            {{ $l }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
		<div class="row">
			<div class="List__guest__index">
			@if($guests->count() > 0)
	            @foreach($guests as $guest)
		            <div class="col-md-3 check_guest" data-id="{{$guest->guest_id}}">
                        <a href="{{ url('/guest/'.str_slug($guest->name, '-').','.$guest->guest_id.'.html') }}"> 
    		                <div class="thumbnail">
    		                	@if(!empty($guest->media_id))
                            		<img src="{{asset('storage/app/public/media/'.$guest->media_id.'/'.$guest->media_type.'/'.$guest->media_file)}}" alt="" class="img-responsive">
                            	@else
                                	<img src="{{asset('public/assets/images/small/img-2.jpg')}}" alt="" class="img-responsive">
                                @endif
                                <div class="caption">
                                    <h3>{{$guest->name}}</h3>
    		                    </div>
                            </div>
                        </a>
		            </div>
		        @endforeach
            @endif
        	</div>
        </div>
       	<!-- End row -->
       	<!-- Pagination Navs-->
	    <div class="row page-navigation">
	      	<div class="col-md-12">
	       		{{ $guests->links() }}
	      	</div>
	    </div>
	</div>
</div>
<script type="text/javascript">
    $(function(){
        // Ajax pagination code starts here
        
        $('body').on('click', '.page-link', function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if(href != undefined)
            {
                // Parameters information
                // 1 - Url to be hit in ajax
                // 2 - Object of DOM element to which html will be replaced from ajax response
                // 3 - Additional querystring added in url
                // 4 - Object of DOM element to which pagination will be replaced from ajax response
                ajax_pagination(href, $('.List__guest__index'), '?m=guests');

            }
        })
    })
</script>
    @if(!$guests->onFirstPage())
        <script type="text/javascript">
            $(document).ready(function(){
            
                $('body').find('.pagination').children().children('.page-link').eq(1).attr('href', '{{ $route }}')
            });
        </script>
    @endif
@endsection
    