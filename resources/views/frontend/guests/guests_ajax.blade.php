@foreach($guests as $guest)
  <div class="col-md-3 check_guest" data-id="{{$guest->guest_id}}">
    <a href="{{ url('/guest/'.str_slug($guest->name, '-').','.$guest->guest_id.'.html') }}"> 
      <div class="thumbnail">
        @if(!empty($guest->media_id))
          <img src="{{asset('storage/app/public/media/'.$guest->media_id.'/'.$guest->media_type.'/'.$guest->media_file)}}" alt="" class="img-responsive">
        @else
          <img src="{{asset('public/assets/images/small/img-2.jpg')}}" alt="" class="img-responsive">
        @endif
        <div class="caption">
          <h3>{{$guest->name}}</h3>
        </div>
      </div>
    </a>
  </div>    
@endforeach