@push('my_scripts')
    <script type="text/javascript">
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<style type="text/css">#wrapper{height: 100%; overflow: visible;}</style>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <nav class="breadcrumbs">
                      <ul>
                        <li>
                          <a href="{{ url('/') }}" title="Home">
                            <i class="ico">
                              <svg id="i-home" viewBox="0 0 24 24">
                                <path d="M24 14.25l-4.5-4.5v-6.75h-3v3.75l-4.5-4.5-12 12v0.75h3v7.5h7.5v-4.5h3v4.5h7.5v-7.5h3z"></path>
                              </svg>&nbsp;
                            </i>
                          </a>
                        </li>
                        <li>
                          <span>»</span>
                          <a href="{{ route('front_guests') }}" title="Featured Shows">Network Guests</a>
                        </li>
                        <li class="current">
                          <span>»</span>
                          <a href="#">Guest Profile</a>
                        </li>
                      </ul>
                    </nav>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7">

                <!-- Image Post -->
                <div class="row">
                    <div class="col-md-2">                                       
                        @if(!empty($media_info))
                        <img src="{{asset('storage/app/public/media/'.$media_info[0]['mid'].'/image/'.$media_info[0]['media_file'])}}" alt="" class="img-responsive">
                      @else
                          <img src="{{asset('public/assets/images/blog/1.jpg')}}" alt="" class="img-responsive">
                        @endif                                         
                       
                    </div>
                    <div class="col-md-10">
                      <div class="row">
                        <div class="">
                          <span>{{$guest_details['name']}} </span> 
                        </div>
                        <div class="">
                            <h3><a href="javascript:void(0);" data-id="{{--$sponsor->id--}}">{{$guest_details['name']}}</a></h3>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div>{!! $guest_details['bio'] !!}</div>
                        </div>
                        @if(!empty($guest_urls))
                          <div class="col-md-12">
                            <div>{!! $guest_urls !!}</div>
                          </div>
                          @endif
                      </div>
                    </div>
                </div>
                @if(!empty($hosts))
                <div class="row">
                  <div class="col-md-12">
                    <h2>Host@php echo count($hosts)>1?'s':''; @endphp</h2>
                  </div>
                  <div class="col-md-12">
                    @foreach($hosts as $host)
                      <div class="small-12 columns">
                        <div class="host-card">'
                          <div class="row">
                            <div class="small-3 columns">
                              @php
                                $host_image = asset('public/images/200x250.jpg');
                                if(!empty($host->filename))
                                  if(file_exists(storage_path().'/app/public/media/'.$host->mid.'/image/'.$host->filename))
                                    $host_image = asset("storage/app/public/media/".$host->mid.'/image/'.$host->filename);
                              @endphp
                              <img src="{{$host_image}}" alt="$host->fullname" />
                            </div>
                            <div class="small-9 columns">
                              <h2>{{ $host->fullname }}</h2>
                              @php
                                $string = strip_tags($host->bio);
                                $description = strlen($string) > 176 ? substr($string,0,176).'...' : $string;
                              @endphp
                              <p class="person-desc">{{ $description }}</p>
                              <a class="button person-view-more small" href="{{ url('host/'.str_slug($host->fullname).','. $host->id.'.html')}}">Find out more &raquo;</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach                      
                  </div>
                </div>
                @endif
                @if(!empty($cohosts))
                <div class="row">
                  <div class="col-md-12">
                    <h2>Host@php echo count($cohosts)>1?'s':''; @endphp</h2>
                  </div>
                  <div class="col-md-12">
                    @foreach($cohosts as $cohost)
                      <div class="small-12 columns">
                        <div class="host-card">'
                          <div class="row">
                            <div class="small-3 columns">
                              @php
                                $host_image = asset('public/images/200x250.jpg');
                                if(!empty($cohost->filename))
                                  if(file_exists(storage_path().'/app/public/media/'.$cohost->mid.'/image/'.$cohost->filename))
                                    $host_image = asset("storage/app/public/media/".$cohost->mid.'/image/'.$cohost->filename);
                              @endphp
                              <img src="{{$host_image}}" alt="$cohost->fullname" />
                            </div>
                            <div class="small-9 columns">
                              <h2>{{ $cohost->fullname }}</h2>
                              @php
                                $string = strip_tags($cohost->bio);
                                $description = strlen($string) > 176 ? substr($string,0,176).'...' : $string;
                              @endphp
                              <p class="person-desc">{{ $description }}</p>
                              <a class="button person-view-more small" href="{{ url('cohost/'.str_slug($cohost->fullname).','. $cohost->id.'.html')}}">Find out more &raquo;</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach                      
                  </div>
                </div>
                @endif
                @if(!empty($podcast_hosts))
                <div class="row">
                  <div class="col-md-12">
                    <h2>Host@php echo count($podcast_hosts)>1?'s':''; @endphp</h2>
                  </div>
                  <div class="col-md-12">
                    @foreach($podcast_hosts as $phost)
                      <div class="small-12 columns">
                        <div class="host-card">'
                          <div class="row">
                            <div class="small-3 columns">
                              @php
                                $host_image = asset('public/images/200x250.jpg');
                                if(!empty($phost->filename))
                                  if(file_exists(storage_path().'/app/public/media/'.$phost->mid.'/image/'.$phost->filename))
                                    $host_image = asset("storage/app/public/media/".$phost->mid.'/image/'.$phost->filename);
                              @endphp
                              <img src="{{$host_image}}" alt="$phost->fullname" />
                            </div>
                            <div class="small-9 columns">
                              <h2>{{ $phost->fullname }}</h2>
                              @php
                                $string = strip_tags($phost->bio);
                                $description = strlen($string) > 176 ? substr($string,0,176).'...' : $string;
                              @endphp
                              <p class="person-desc">{{ $description }}</p>
                              <a class="button person-view-more small" href="{{ url('host/'.str_slug($phost->fullname).','. $phost->id.'.html')}}">Find out more &raquo;</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach                      
                  </div>
                </div>
                @endif
            </div>
            <div class="col-sm-5">
            </div>
        </div>
    </div>
</div>
@endsection
