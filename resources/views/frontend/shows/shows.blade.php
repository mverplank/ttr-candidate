@push('my_scripts')
  <link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.guests').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title"> Radio Shows </h4>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @isset($top_slider)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($top_slider as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                    $url = url('/host/'.str_slug($slide->title, '-').','.$slide->id.'.html');
                                @endphp
                                <li>
                                    <div class="post-image col-md-12 slide_image">
                                        <a href="{{ $url }}">
                                            <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
          	<div class="col-md-3">
                 		<a href="{{ route('front_featured_shows') }}">Browse Radio Shows</a>
             	</div>
             	<div class="col-md-3">
             		<a href="{{ route('front_schedule') }}">Weekly Schedule</a>
             	</div>
             	<div class="col-md-3">
             		<a href="{{ route('front_podcasts') }}">Browse Podcasts</a>
             	</div>
             	<div class="col-md-3">
             		<a href="{{ route('front_guests') }}">Browse Guests</a>
             	</div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h1>Featured This Week!</h1>
            <div class="row">
              <h3>Featured Episodes!</h3>
              <div class="col-md-12">
                @isset($featured_episodes)
                  <!-- Place somewhere in the <body> of your page -->
                  <div class="flexslider guests">
                    <ul class="slides">
                      @foreach($featured_episodes as $key => $slide)
                        @php
                            /*echo '<pre>';
                            print_r($slide);
                            echo '</pre>';
                            die;*/
                            $imgURL = asset('public/images/600x600.jpg');
                            if(!empty($slide->meta_json)){
                                $t = json_decode($slide->meta_json);
                                if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                    $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                }
                            }
                            $url = url(str_slug($slide->title, '-').','.$slide->id.'.html');
                        @endphp
                        <li>
                          <div class="post-image col-md-12 slide_image max_height">
                            <a href="{{ $url }}">
                              <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->name }}" />
                            </a>
                          </div>
                          <div class="slide_content">
                            <a href="{{ $url }}">{{ $slide->name }}</a>
                          </div>
                        </li>
                      @endforeach
                    </ul>
                  </div>
                @endisset
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h3>Featured Hosts!</h3>
            @isset($featured_hosts)
              <!-- Place somewhere in the <body> of your page -->
              <div class="flexslider guests">
                <ul class="slides">
                  @foreach($featured_hosts as $key => $slide)
                    @php
                      /*echo '<pre>';
                      print_r($slide);
                      echo '</pre>';*/
                      $imgURL = asset('public/images/600x600.jpg');
                      if(!empty($slide->meta_json)){
                          $t = json_decode($slide->meta_json);
                          if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                              $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                          }
                      }
                    @endphp
                    <li>
                      <div class="post-image col-md-12 slide_image max_height">
                        <a href="{{ $url }}">
                          <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->name }}" />
                        </a>
                      </div>
                      <div class="slide_content">
                        <a href="{{ $url }}">
                          {{ $slide->name }}
                        </a>
                      </div>
                    </li>
                  @endforeach
                </ul>
              </div>
            @endisset
          </div>
        </div>
        <div class="row">
          <div class="col-md-9">
            <h3>Thank you Sponsors & Partners!</h3>
            @isset($network_sponsors)
              @foreach($network_sponsors as $key => $sponsor)
                  {!! $sponsor !!}
              @endforeach
            @endisset
          </div>
          <div class="col-md-3">
            <a class="btn btn-primary" href="{{route('front_sponsors')}}">View Sponsors</a>
          </div>
        </div>
    </div>
</div>
@endsection
