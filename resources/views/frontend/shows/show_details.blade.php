@push('my_scripts')
    <!--<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>-->
    <script type="text/javascript">
        /*$(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.guests').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })*/
        var channel_id = '{{ $channel_id }}';
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<style type="text/css">#wrapper{height: 100%; overflow: visible;}</style>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <nav class="breadcrumbs">
                      <ul>
                        <li>
                          <a href="{{ url('/') }}" title="Home">
                            <i class="ico">
                              <svg id="i-home" viewBox="0 0 24 24">
                                <path d="M24 14.25l-4.5-4.5v-6.75h-3v3.75l-4.5-4.5-12 12v0.75h3v7.5h7.5v-4.5h3v4.5h7.5v-7.5h3z"></path>
                              </svg>&nbsp;
                            </i>
                          </a>
                        </li>
                        <li>
                          <span>»</span>
                          <a href="{{ route('front_featured_shows') }}" title="Featured Shows">Featured Shows</a>
                        </li>
                        <li class="current">
                          <span>»</span>
                          <a href="#">Show</a>
                        </li>
                      </ul>
                    </nav>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7">

                <!-- Image Post -->
                <div class="row">
                    <div class="col-md-2">                                       
                        @if(!empty($media_info))
                        <img src="{{asset('storage/app/public/media/'.$media_info[0]['mid'].'/image/'.$media_info[0]['media_file'])}}" alt="" class="img-responsive">
                      @else
                          <img src="{{asset('public/assets/images/blog/1.jpg')}}" alt="" class="img-responsive">
                        @endif                                         
                       
                    </div>
                    <div class="col-md-10">
                      <div class="row">
                        <div class="">
                          <span>{{$show_details['name']}} </span> 
                        </div>
                        <div class="">
                            <h3><a href="javascript:void(0);" data-id="{{--$sponsor->id--}}">{{$show_details['name']}}</a></h3>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div>{!! $show_details['description'] !!}</div>
                        </div>
                        <div class="">
                            <a href="javascript:void(0);" class="btn btn-success btn-sm waves-effect waves-light">View Sponsor <i class="mdi mdi-arrow-right m-l-5"></i></a>
                        </div>
                      </div>
                    </div>
                </div>
                @if(!empty($hosts))
                <div class="row">
                  <div class="col-md-12">
                    <h2>Host@php echo count($hosts)>1?'s':''; @endphp</h2>
                  </div>
                  <div class="col-md-12">
                    @foreach($hosts as $host)
                      <div class="small-12 columns">
                        <div class="host-card">'
                          <div class="row">
                            <div class="small-3 columns">
                              @php
                                $host_image = asset('public/images/200x250.jpg');
                                if(!empty($host->filename))
                                  if(file_exists(storage_path().'/app/public/media/'.$host->mid.'/image/'.$host->filename))
                                    $host_image = asset("storage/app/public/media/".$host->mid.'/image/'.$host->filename);
                              @endphp
                              <img src="{{$host_image}}" alt="$host->fullname" />
                            </div>
                            <div class="small-9 columns">
                              <h2>{{ $host->fullname }}</h2>
                              @php
                                $string = strip_tags($host->bio);
                                $description = strlen($string) > 176 ? substr($string,0,176).'...' : $string;
                              @endphp
                              <p class="person-desc">{{ $description }}</p>
                              <a class="button person-view-more small" href="{{ url('host/'.str_slug($host->fullname).','. $host->id.'.html')}}">Find out more &raquo;</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach                      
                  </div>
                </div>
                @endif
                @if(!empty($cohosts))
                <div class="row">
                  <div class="col-md-12">
                    <h2>Host@php echo count($cohosts)>1?'s':''; @endphp</h2>
                  </div>
                  <div class="col-md-12">
                    @foreach($cohosts as $cohost)
                      <div class="small-12 columns">
                        <div class="host-card">'
                          <div class="row">
                            <div class="small-3 columns">
                              @php
                                $host_image = asset('public/images/200x250.jpg');
                                if(!empty($cohost->filename))
                                  if(file_exists(storage_path().'/app/public/media/'.$cohost->mid.'/image/'.$cohost->filename))
                                    $host_image = asset("storage/app/public/media/".$cohost->mid.'/image/'.$cohost->filename);
                              @endphp
                              <img src="{{$host_image}}" alt="$cohost->fullname" />
                            </div>
                            <div class="small-9 columns">
                              <h2>{{ $cohost->fullname }}</h2>
                              @php
                                $string = strip_tags($cohost->bio);
                                $description = strlen($string) > 176 ? substr($string,0,176).'...' : $string;
                              @endphp
                              <p class="person-desc">{{ $description }}</p>
                              <a class="button person-view-more small" href="{{ url('cohost/'.str_slug($cohost->fullname).','. $cohost->id.'.html')}}">Find out more &raquo;</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach                      
                  </div>
                </div>
                @endif
                @if(!empty($podcast_hosts))
                <div class="row">
                  <div class="col-md-12">
                    <h2>Host@php echo count($podcast_hosts)>1?'s':''; @endphp</h2>
                  </div>
                  <div class="col-md-12">
                    @foreach($podcast_hosts as $phost)
                      <div class="small-12 columns">
                        <div class="host-card">'
                          <div class="row">
                            <div class="small-3 columns">
                              @php
                                $host_image = asset('public/images/200x250.jpg');
                                if(!empty($phost->filename))
                                  if(file_exists(storage_path().'/app/public/media/'.$phost->mid.'/image/'.$phost->filename))
                                    $host_image = asset("storage/app/public/media/".$phost->mid.'/image/'.$phost->filename);
                              @endphp
                              <img src="{{$host_image}}" alt="$phost->fullname" />
                            </div>
                            <div class="small-9 columns">
                              <h2>{{ $phost->fullname }}</h2>
                              @php
                                $string = strip_tags($phost->bio);
                                $description = strlen($string) > 176 ? substr($string,0,176).'...' : $string;
                              @endphp
                              <p class="person-desc">{{ $description }}</p>
                              <a class="button person-view-more small" href="{{ url('host/'.str_slug($phost->fullname).','. $phost->id.'.html')}}">Find out more &raquo;</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach                      
                  </div>
                </div>
                @endif
            </div>
            <div class="col-sm-5">
            </div>
        </div>
    </div>
</div>
@endsection
