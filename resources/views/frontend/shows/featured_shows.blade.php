@extends('frontend.layouts.front')

@section('content')
    <div class="content">
      <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title"> Featured Shows </h4>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <h4>Full Weekly Schedule</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card-body">
              <div class="tabs">
                <div class="tab-button-outer">
                  <ul id="tab-button">
                    <li><a href="#tab01">This Week</a></li>
                    <li><a href="#tab02">Upcoming</a></li>
                  </ul>
                </div>
                <div class="tab-select-outer">
                  <select id="tab-select">
                    <option value="#tab01">Tab 1</option>
                    <option value="#tab02">Tab 2</option>
                    <option value="#tab03">Tab 3</option>
                    <option value="#tab04">Tab 4</option>
                    <option value="#tab05">Tab 5</option>
                  </select>
                </div>

                <div id="tab01" class="tab-contents">
                  <p>{{ $upcoming_shows }}</p>
                </div>
                <div id="tab02" class="tab-contents">
                  <p>{{ $this_week_shows }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <h2>Browse shows below, click for more details about each show and a full listing of their podcasts.</h2>
            <div class="featured-shows row">
              @foreach ($featured_shows as $s)
                <div class="col-md-4">
                  <div class="show-image">
                    @php
                      $imgURL = asset('storage/app/public/media/120x320.png');
                    @endphp
                    @if(!empty($s->meta_json))
                      @php
                      $t = json_decode($s->meta_json);
                      if(file_exists(storage_path().'/app/public/media/'.$s->mid.'/image/'.$t->filename)){
                        $imgURL = asset("storage/app/public/media/".$s->mid.'/image/'.$t->filename);
                      }
                      @endphp
                      <img src="@php echo $imgURL @endphp">
                    @else
                      <img src="{{ $imgURL }}" alt="no image">
                    @endif
                  </div>
                  <div class="show-title">
                    <strong>{{ $s->name }}</strong>
                  </div>
                  <div class="clearfix"></div>
                  <div>&nbsp;</div>
                  <div class="show-description">
                    @php 
                      $des = substr(strip_tags($s->description),0,200).'...';
                    @endphp
                    {{ $des }}
                  </div>
                  <div class="show-details">
                    <a class="btn btn-primary" href="{{ url('show-details/'.str_slug($s->name, '-').','.$s->id.'.html') }}">View Details</a>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="row page-navigation">
          <div class="col-md-12">
            {{ $featured_shows->links() }}
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(function() {
        var $tabButtonItem = $('#tab-button li'),
            $tabSelect = $('#tab-select'),
            $tabContents = $('.tab-contents'),
            activeClass = 'is-active';

        $tabButtonItem.first().addClass(activeClass);
        $tabContents.not(':first').hide();

        $tabButtonItem.find('a').on('click', function(e) {
          var target = $(this).attr('href');

          $tabButtonItem.removeClass(activeClass);
          $(this).parent().addClass(activeClass);
          $tabSelect.val(target);
          $tabContents.hide();
          $(target).show();
          e.preventDefault();
        });

        $tabSelect.on('change', function() {
          var target = $(this).val(),
              targetSelectNum = $(this).prop('selectedIndex');

          $tabButtonItem.removeClass(activeClass);
          $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
          $tabContents.hide();
          $(target).show();
        });
        // Ajax pagination code starts here
        $('body').on('click', '.page-link', function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if(href != undefined)
            {
                // Parameters information
                // 1 - Url to be hit in ajax
                // 2 - Object of DOM element to which html will be replaced from ajax response
                // 3 - Additional querystring added in url
                // 4 - Object of DOM element to which pagination will be replaced from ajax response
                ajax_pagination(href, $('.featured-shows'), '?m=show');

            }
        })

      });

    </script>
    @if(!$featured_shows->onFirstPage())
        <script type="text/javascript">
            $(document).ready(function(){
                //if(!$('body').find('a.page-link:eq(1)').hasClass('filter-link'))
                console.log($('body').find('.pagination').children().children('.page-link').eq(0).attr('href'));
                $('body').find('.pagination').children().children('.page-link').eq(0).attr('href', '{{ $route }}')
                $('body').find('.pagination').children().children('.page-link').eq(1).attr('href', '{{ $route }}')
            });
        </script>
    @endif
@endsection

