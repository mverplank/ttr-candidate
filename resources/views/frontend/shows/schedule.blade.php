@extends('frontend.layouts.front')

@section('content')
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="page-title-box">
          <h4 class="page-title"> Schedule </h4>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div id="exTab1" class="container"> 
          <ul  class="nav nav-pills">
            @foreach($days as $k => $v)
              <li @if($v == $today) class="active" @endif>
                <a href="#{{ strtolower($k) }}" data-toggle="tab">{{ ucfirst($k) }}</a>
              </li>
            @endforeach
          </ul>

          <div class="tab-content clearfix">
            @foreach($days as $k => $v)
              <div class="tab-pane @if($v == $today) active @endif" id="{{ strtolower($k) }}">
                @if(count($fetch_schedule[$v]) > 0)
                  <div class="row" data-equalizer="data-equalizer" data-equalize-on="medium" data-resize="data-equalizer" data-mutate="data-equalizer" data-events="mutate">
                    @foreach($fetch_schedule[$v] as $sc)
                      @php

                        // If episode id is not empty, then show episode title else show show name
                        // This is already checked in module function

                        $title = preg_replace("/\[(.*?)\]/", '', $sc['title']);
                        
                        $time = date('g a', strtotime($sc['start']));
                        
                        // Generate manually SEO friendly slug from title
                        $slug = str_slug($title, '-'); 
                        $url = $slug.','.$sc['show_id'].'.html';

                        // Set default url to show detail page 
                        $url = url('/show-details/'.$url);

                        if(!empty($sc['episode_id']))
                        {
                          //If this is episode, URL is set to episode detail page
                          $url = $slug.','.$sc['episode_id'].'.html';
                          $url = url('/episode/'.$url);
                        }

                        $imgURL = asset('storage/app/public/media/120x320.png');
                        if(isset($all_media[$sc['show_id']])){
                          $media = $all_media[$sc['show_id']];
                          if(file_exists(storage_path().'/app/public/media/'.$media['mid'].'/image/'.$media['meta_json']->filename)){
                              $imgURL = asset("storage/app/public/media/".$media['mid'].'/image/'.$media['meta_json']->filename);
                          }
                        }
                      @endphp
                      <div class="col-md-4 small-12 medium-6 large-4 columns end">
                        <div class="episode box" data-equalizer-watch="data-equalizer-watch" style="height: 350px;">
                          <a href="{{ $url }}">
                            <div class="image">
                              <img alt="{{ $title }}" data-original="/thumb/Spirit-Fire-Radio-with-Hosts-Steve-Kramer-Dorothy-Riddle_Show_159_player.jpg" src="{{ $imgURL }}" class="lldef lld" style="">
                            </div>
                            <div class="show-content">
                                <h3 class="time"><i class="fa fa-clock-o"></i> {{ $time }} PDT</h3>
                                <h3 class="title">{{ $title }}</h3>
                            </div>
                          </a>
                        </div>
                      </div>
                    @endforeach
                  </div>
                @endif
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
