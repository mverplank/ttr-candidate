@foreach ($featured_shows as $s)
  <div class="col-md-4">
    <div class="show-image">
      @php
        $imgURL = asset('storage/app/public/media/120x320.png');
      @endphp
      @if(!empty($s->meta_json))
        @php
        $t = json_decode($s->meta_json);
        if(file_exists(storage_path().'/app/public/media/'.$s->mid.'/image/'.$t->filename)){
          $imgURL = asset("storage/app/public/media/".$s->mid.'/image/'.$t->filename);
        }
        @endphp
        <img src="@php echo $imgURL @endphp">
      @else
        <img src="{{ $imgURL }}" alt="no image">
      @endif
    </div>
    <div class="show-title">
      <strong>{{ $s->name }}</strong>
    </div>
    <div class="clearfix"></div>
    <div>&nbsp;</div>
    <div class="show-description">
      @php 
        $des = substr(strip_tags($s->description),0,200).'...';
      @endphp
      {{ $des }}
    </div>
    <div class="show-details">
      <a class="btn btn-primary" href="{{ url('show-details/'.$s->id) }}">View Details</a>
    </div>
  </div>
@endforeach