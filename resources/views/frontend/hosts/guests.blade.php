@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title"> Hosts </h4>
                   
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">       
                <div class="list-top-filters">
                    @php
                        $lists = range('A', 'Z');
                    @endphp
                    @foreach($lists as $l)
                        <a href="{{ url('guests') }}?page=1&lastname={{ $l }}" class="page-link filter-link">
                            {{ $l }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            
        </div>
        <div class="row">
            <div class="small-12 columns">       
                <div class="List__host__index">
                    @foreach ($all_hosts as $host)
                        <div class="small-6 medium-3 large-2 columns end">
                            <div class="box bg">
                                @php
                                    //print_r($host);
                                    $imgURL = asset('storage/app/public/media/120x320.png');
                                    $guestURL = url('/guest/'.str_slug($host->User__fullname, '-').','.$host->id.'.html');
                                @endphp
                                @if(!empty($host->meta_json))
                                    @php
                                        $t = json_decode($host->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$host->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$host->mid.'/image/'.$t->filename);
                                        }
                                    @endphp
                                @endif
                                <a href="{{ $guestURL }}">
                                    <img alt="Deb Acker" data-original="/thumb/Deb-Acker_Host_129_photo_200x250.jpg" src="@php echo $imgURL @endphp" class="lldef lld" style="">
                                    <div class="row-box-name">{{ $host->name }}</div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row page-navigation">
            <div class="col-md-12">
                {{ $all_hosts->links() }}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        // Ajax pagination code starts here
        /*$('body').on('click', '.filter-link', function(e){
            e.preventDefault();
            $('.page-link').trigger('click');
        })*/
        $('body').on('click', '.page-link', function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if(href != undefined)
            {
                // Parameters information
                // 1 - Url to be hit in ajax
                // 2 - Object of DOM element to which html will be replaced from ajax response
                // 3 - Additional querystring added in url
                // 4 - Object of DOM element to which pagination will be replaced from ajax response
                ajax_pagination(href, $('.List__host__index'), '?m=co-hosts');

            }
        })
    })
</script>
    @if(!$all_hosts->onFirstPage())
        <script type="text/javascript">
            $(document).ready(function(){
                //if(!$('body').find('a.page-link:eq(1)').hasClass('filter-link'))
                //console.log($('body').find('.pagination').children().children('.page-link'));
                $('body').find('.pagination').children().children('.page-link').eq(1).attr('href', '{{ $route }}')
            });
        </script>
    @endif
@endsection
