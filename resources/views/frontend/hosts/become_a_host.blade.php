@push('my_scripts')
	<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.solutions').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })
        var channel_id = '{{ $channel_id }}';
        @isset($resolutions)
            var resolutions = '{!! $resolutions !!}';
        @endisset
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Become A Host</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @isset($top_slider)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($top_slider as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    <div class="post-image col-md-12 slide_image">
                                        <a href="{{ $slide->url }}">
                                            <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size: 200%; text-align: center;">Your Voice Has Power! Become Part of our Radio Family Today!</h1>
                <h2 style="font-size: 120%; text-align: center;">We've created a platform to get your message out to the world. The Ripple Effect... our state-of-the-art online radio and podcasting platform embeds your message &amp; brand in over 90 digital outlets. Automatically guaranteeing increased searchability for you.&nbsp;</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Higher Conscious Solutions</h3>
                @isset($solutions)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider solutions">
                        <ul class="slides">
                            @foreach($solutions as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                    $url = !empty($slide->external_url)?$slide->external_url:(!empty($slide->url)?$slide->url:url('channel,'.str_slug($slide->title, '-').','.$slide->id.'.html'));
                                @endphp
                                <li>
                                    <div class="slide_image">
                                        <a href="{{ $url }}" target="_blank">
                                            <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                    <div class="slide_content">
                                        <a href="{{ $url }}" target="_blank">
                                            {{ $slide->title }}
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h2 style="font-size: 150%; text-align: center;"><em>Host your own show or partner with Dr. Pat as a monthly co-host!</em></h2>
                <p style="font-size: 110%; text-align: center;">Join Us to share your powerful message with listeners who not only need it, but want to hear from you. With our network expansion we have&nbsp;Hosting&nbsp;and Co-Hosting spots open at an INCREDIBLE rate. No radio experience? No problem! Dr. Pat will teach you everything she&rsquo;s learned in 15 years on how to create a successful radio show.</p>
                <p style="font-size: 110%; text-align: center;">&nbsp;</p>
                <p style="font-size: 110%; text-align: center;">There are several reasons why people decide to host a show. The one underlying reason is that they have a powerful positive message and want to help create a better world. When you host a show on an independent network nobody is telling you what you can and cannot say. Your message is unfiltered to your listening audience and you own your content.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h2 itemprop="name">Why Join Us?</h2><div><center><iframe style="width: 100%; max-width: 560px;" src="https://www.youtube.com/embed/9ZOdE3-TzOE" height="315" frameborder="0" allowfullscreen=""></iframe></center></div>
            </div>
            <div class="col-sm-6">
                <h2 itemprop="name">Click To View Our Full Media Kit</h2>
                <a href="/files/custom/media/The-Transformation-Network-2018.pdf" target="_blank" itemprop="contentUrl">
                    <img src="{{ asset('/storage/app/public/pdf.png') }}" alt="Click To View Our Full Media Kit" />
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <h2 itemprop="name">Hosting or Co-Hosting Features</h2>
                <div>
                    <ul>
                        <li>Professionally produced live or prerecorded shows</li>
                        <li>Options for monthly, bi-weekly, or weekly</li>
                        <li>Options for 30 or 60 minute shows</li>
                        <li>Show replays during week and weekends</li>
                        <li>Distribution of show to major podcasting outlets</li>
                        <li>Podcast listing of each show</li>
                        <li>Full production &amp; support from award winning Transformation Team</li>
                        <li>Branding support for show</li>
                        <li>Social media marketing of show</li>
                        <li>Technology training and consulting</li>
                        <li>Live call in or pre-recorded formats</li>
                        <li>Professionally produced commercials for show</li>
                        <li>Professionally produced commercials for your business</li>
                        <li>Commercials played across entire network plus Dr. Pat Show Network including multiple AM/FM stations</li>
                        <li>Professionally produced show openers and closers</li>
                        <li>Custom show graphics</li>
                        <li>Customizable host profile page</li>
                        <li>Website consults for Search Engine Optimization</li>
                        <li>Strategy sessions for content repurposing to blogs, books, articles, YouTube, training courses, etc&hellip;</li>
                        <li>Email blasts to over 10K subscribers</li>
                        <li>You control your content&nbsp;</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 itemprop="name">Upgrade Options - Full Integrated Marketing</h2>
                <div>
                    <ul>
                        <li>Full search engine analysis &amp; optimization</li>
                        <li>WordPress Website Design</li>
                        <li>Website maintenance</li>
                        <li>Website upgrades and updates</li>
                        <li>Social media business page setup</li>
                        <li>Social media business page management</li>
                        <li>Blog and article writing</li>
                        <li>Press Releases</li>
                        <li>Building a brand coaching</li>
                        <li>Radio &amp; media coaching</li>
                        <li>Book proposal and marketing</li>
                        <li>Speaker one page</li>
                        <li>Advertising media kit</li>
                        <li>Promotional videos</li>
                        <li>Speaker videos</li>
                        <li>Company, product, and audience analysis</li>
                        <li>Facebook Live broadcasts</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <script type="text/javascript" src="https://form.jotform.us/jsform/50904850226149"></script>
            </div>
            <div class="col-sm-6">
                <div class="col-md-12">
                    <h3>What People Are Saying!</h3>
                    @isset($people_saying)
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider sliders">
                            <ul class="slides">
                                @foreach($people_saying as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                    @endphp
                                    <li>
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                        <div class="slide_content block-with-text">
                                            <a href="{{ $slide->url }}">
                                                {!! $slide->content !!}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endisset
                </div>

                <div class="col-md-12">
                    <h3>Excellent Client Reviews!</h3>
                    @isset($client_reviews)
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider sliders">
                            <ul class="slides">
                                @foreach($client_reviews as $key => $slide)
                                    <li>
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                        <div class="slide_content block-with-text">
                                            <a href="{{ $slide->url }}">
                                                {!! $slide->content !!}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@endsection