@foreach ($all_hosts as $host)
  <div class="small-6 medium-3 large-2 columns end">
    <div class="box bg">
      @php
          //print_r($host);
          $imgURL = asset('storage/app/public/media/120x320.png');
          $guestURL = url('/co-host/'.str_slug($host->User__fullname, '-').','.$host->id.'.html');
      @endphp
      @if(!empty($host->meta_json))
          @php
              $t = json_decode($host->meta_json);
              if(file_exists(storage_path().'/app/public/media/'.$host->mid.'/image/'.$t->filename)){
                  $imgURL = asset("storage/app/public/media/".$host->mid.'/image/'.$t->filename);
              }
          @endphp
      @endif
      <a href="{{ $guestURL }}">
          <img alt="Deb Acker" data-original="/thumb/Deb-Acker_Host_129_photo_200x250.jpg" src="@php echo $imgURL @endphp" class="lldef lld" style="">
          <div class="row-box-name">{{ $host->name }}</div>
      </a>
    </div>
  </div>
@endforeach