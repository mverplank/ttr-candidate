
<script src="{{ asset('public/assets/js/popper.js')}}"></script>
<script src="{{ asset('public/assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('public/assets/js/detect.js')}}"></script>
<script src="{{ asset('public/assets/js/fastclick.js')}}"></script>
<script src="{{ asset('public/assets/js/jquery.blockUI.js')}}"></script>
<script src="{{ asset('public/assets/js/waves.js')}}"></script>
<script src="{{ asset('public/assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{ asset('public/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{ asset('public/plugins/switchery/switchery.min.js')}}"></script>

<!-- isotope plugin -->
<script src="{{ asset('public/plugins/isotope/js/isotope.pkgd.min.js')}}"></script>

<!-- App js -->
<script src="{{ asset('public/assets/js/jquery.core.js')}}"></script>
<script src="{{ asset('public/assets/js/jquery.app.js')}}"></script>

<!-- SELECT BOX JQUERY-->
<!-- <script src="{{ asset('public/plugins/select2/js/select2.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/assets/js/owl.carousel.js')}}"></script> -->
<script src="{{ asset('public/bootstrap/js/carousel.js')}}">
<script src="{{ asset('public/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>      
<!-- Custom js -->
<script src="{{ asset('public/js/custom_frontend.js')}}"></script>



