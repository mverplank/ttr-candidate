<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico') }}">
        <!-- App title -->
        <title>Transformation Talk Radio</title>

        <!-- ========== Left Sidebar Start ========== -->
        @include('frontend.layouts.include_css')
        <!-- ==========  Left Sidebar End  ===========-->
        <!-- jQuery  -->
        <script src="{{ asset('public/assets/js/jquery.min.js')}}"></script>
        <script src="{{ asset('public/js/modernizr.custom.js') }}"></script>
        <script>
            var APP_URL = {!! json_encode(url('/')) !!}
        </script>
        @stack('my_scripts')
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                   <!--  <a href="index.html" class="logo"><span>Zir<span>cos</span></span><i class="mdi mdi-layers"></i></a> -->
                    
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left">
                          
                            <!-- Home -->
                            <li class="hidden-xs">
                                <a class="menu-item" href="{{route('front_home')}}">
                                    <i class="fa fa-home" aria-hidden="true"></i>  Home
                                </a>
                            </li>
                            <!-- About Us -->
                            <li class="hidden-xs">
                                <a class="menu-item" href="{{route('front_about')}}">
                                    <i class="fa fa-home" aria-hidden="true"></i> About
                                </a>
                            </li>
                            <!-- Radio Shows -->
                            <li class="dropdown hidden-xs">
                                <a data-toggle="dropdown" class="dropdown-toggle menu-item" href="{{route('front_shows')}}" aria-expanded="false">Radio Shows
                                    <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="{{route('front_shows')}}">Radio Shows</a></li>
                                    <li><a href="#">Live Shows</a></li>
                                    <li><a href="{{route('front_podcasts')}}">Podcasts</a></li>
                                </ul>
                            </li>
                            <!-- Hosts -->

                            <li class="dropdown hidden-xs">
                                <a data-toggle="dropdown" class="dropdown-toggle menu-item" href="{{route('front_hosts')}}" aria-expanded="false">Our Hosts
                                    <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="{{route('front_hosts')}}">Our Hosts</a></li>
                                    <li><a href="{{route('front_co_hosts')}}">Our Co-hosts</a></li>
                                </ul>
                            </li>


                            <!-- Advertisement -->
                            <li class="hidden-xs">
                                <a class="menu-item" href="{{route('front_become_a_sponsor')}}">
                                    <i class="fa fa-home" aria-hidden="true"></i> Advertise
                                </a>
                            </li>
                            
                            <!-- Become a host -->
                            <li class="hidden-xs">
                                <a class="menu-item" href="{{route('front_become_a_host')}}">
                                    <i class="fa fa-home" aria-hidden="true"></i> Become A Host
                                </a>
                            </li>
                           
                            <!-- Positive Media -->
                            <li class="hidden-xs">
                                <a class="menu-item" href="{{route('front_positive_media')}}">
                                    <i class="fa fa-home" aria-hidden="true"></i> Positive Media
                                </a>
                            </li>

                            <!-- Contact Us -->
                            <li class="hidden-xs">
                                <a class="menu-item" href="{{route('front_contact')}}">
                                    <i class="fa fa-home" aria-hidden="true"></i> Contact Us
                                </a>
                            </li>
                        </ul>
                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>

            <!-- <div class="left side-menu"></div> -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">              
                <!-- Start content -->
                @yield('content')
            </div>

            <footer class="footer text-right">
                2018 - 2020 © <a target="_blank" href="http://www.contriverz.com/">Contriverz</a>.
            </footer>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->
        <!-- ========== Left Sidebar Start ========== -->
            @include('frontend.layouts.include_js')
        <!-- ==========  Left Sidebar End  ===========-->
        <script>
            var resizefunc = [];
        </script>
        @if(Session::has('success'))
            @include('backend.flash_message.success', array('msg' => Session::get('success')))
        @elseif(Session::has('error'))
            @include('backend.flash_message.error', array('msg' => Session::get('error')))
        @endif  
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                if($("#ChannelDescription").length > 0){
                    tinymce.init({
                        selector: "textarea#ChannelDescription",
                        theme: "modern",
                        height:300,
                        menubar: false,
                        plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "save table contextmenu directionality emoticons template paste textcolor"
                        ]
                    });
                }
            });
            function makeAjaxRequest(method, url, data){
                return $.ajax({
                      type : method,
                      url : APP_URL+url,
                      data : data
                   })
            }
        </script>

        <script type="text/javascript">
            var APP_URL = {!! json_encode(url('/')) !!};
            csrf_token  = '{!! csrf_token() !!}';
            //var ROLE    = {!! getCurrentUserType() !!};
        </script>
    </body>
</html>