@push('my_scripts')
	<link href="{{ asset('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('public/js/jquery.flexslider-min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sliders').flexslider({
                animation: "slide"
            });
            // tiny helper function to add breakpoints
            function getGridSize() {
                return (window.innerWidth < 600) ? 2 :
                   (window.innerWidth < 800) ? 3 : 6;
            }
            $('.guests').flexslider({
                animation: "slide",
                animationLoop: false,
                minItems:getGridSize(),
                maxItems:getGridSize(),
                itemWidth: 210,
                itemMargin: 4,
                animationLoop: true,
                move:6
            });
        })
        var channel_id = '{{ $channel_id }}';
        @isset($resolutions)
            var resolutions = '{!! $resolutions !!}';
        @endisset
    </script>
@endpush
@extends('frontend.layouts.front')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Advertise</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @isset($top_slider)
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider sliders">
                        <ul class="slides">
                            @foreach($top_slider as $key => $slide)
                                @php
                                    /*echo '<pre>';
                                    print_r($slide);
                                    echo '</pre>';*/
                                    $imgURL = asset('public/images/600x600.jpg');
                                    if(!empty($slide->meta_json)){
                                        $t = json_decode($slide->meta_json);
                                        if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                            $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                        }
                                    }
                                @endphp
                                <li>
                                    <div class="post-image col-md-12 slide_image">
                                        <a href="{{ $slide->url }}">
                                            <img class="img-responsive" src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endisset
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <h1>Become A Sponsor!</h1>
                <h2>Why Us?</h2>
                <ul>
                    <li>Your association with Transformation Radio, The Dr. Pat Show, Epic Living Media, ETV, Events, etc. as a sponsor and/or co-host.</li>
                    <li>Exceptional visibility &amp; traffic with continuous promotion across multiple channels/websites</li>
                    <li>Target market of affinity listeners</li>
                    <li>Competitive pricing</li>
                </ul>
                <h3>Look at what is included in our packages!</h3>
                <h3>The Ripple Effect</h3>
                <ul>
                    <li>Automatic syndication through 90 digital outlets including places like: iTunes, iTunes Radio, Spreaker, TuneIn, iHeart Radio, SoundCloud, YouTube</li>
                    <li>Social Media Marketing of your show</li>
                    <li>Replays throughout the week/weekends</li>
                    <li>Co-ownership rights to content</li>
                    <li>24/7 on demand library</li>
                    <li>Additional promotion across The Dr. Pat Show Network reaching AM/FM across US &amp; Australia</li>
                </ul>
                <h3>Leading Edge</h3>
                <ul>
                    <li>Detailed show descriptions and co-host/sponsor &amp; guest profile page control</li>
                    <li>Mobile app for listeners</li>
                    <li>Promotional network material distributed at live events</li>
                    <li>Custom archive players for you to use on your websites &ldquo;The TTR Widget&rdquo;</li>
                    <li>Weekly Newsletter to 10K subscribers</li>
                    <li>RSS Feeds</li>
                    <li>Website links to website and TTR</li>
                    <li>You control the content for your shows</li>
                </ul>
                <h3>Professional Production&nbsp;</h3>
                <ul>
                    <li>128  kbps sound quality</li>
                    <li>Live engineer/producer in studio</li>
                    <li>Call screening and messaging</li>
                    <li>Free 800 call in numbers callers</li>
                    <li>Digital &amp; analog capability (Skype, VoIP, phone)</li>
                </ul>
                <h3>Contact Us Today!</h3>
                <ul>
                    <li>Email: comments@thedrpatshow.com</li>
                    <li>Office Phone: 855-393-3742</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="col-md-12">
                    <a href="{{ route('front_sponsors') }}" class="btn btn-primary">View Sponsors</a>
                </div>
                <div class="col-md-12">
                    <h3>Videos</h3>
                    @isset($videos)
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider sliders">
                            <ul class="slides">
                                @foreach($videos as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                        $url = !empty($slide->external_url)?$slide->external_url:(!empty($slide->url)?$slide->url:'javascript:void(0)');
                                    @endphp
                                    <li>
                                        <div class="slide_image">
                                            <a href="{{ $url }}" target="_blank">
                                                <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div>
                                        <div class="slide_content">
                                            <a href="{{ $url }}" target="_blank">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endisset
                </div>
                <div class="col-md-12">
                    <h3>Highlights</h3>
                    @isset($highlights)
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider sliders">
                            <ul class="slides">
                                @foreach($highlights as $key => $slide)
                                    @php
                                        /*echo '<pre>';
                                        print_r($slide);
                                        echo '</pre>';*/
                                        $imgURL = asset('public/images/600x600.jpg');
                                        if(!empty($slide->meta_json)){
                                            $t = json_decode($slide->meta_json);
                                            if(file_exists(storage_path().'/app/public/media/'.$slide->mid.'/image/'.$t->filename)){
                                                $imgURL = asset("storage/app/public/media/".$slide->mid.'/image/'.$t->filename);
                                            }
                                        }
                                    @endphp
                                    <li>
                                        <div class="slide_image">
                                            <a href="{{ $slide->url }}">
                                                <img src="{{ $imgURL}}" alt="{{ $slide->title }}" />
                                            </a>
                                        </div>
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {{ $slide->title }}
                                            </a>
                                        </div>
                                        <div class="slide_content">
                                            <a href="{{ $slide->url }}">
                                                {!! $slide->content !!}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    @endisset
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<h3>Resources</h3>
                
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h3>Sponsors and Advertisers</h3>
        		@isset($network_sponsors)
                    @foreach($network_sponsors as $key => $sponsor)
                        {!! $sponsor !!}
                    @endforeach
		        @endisset
        	</div>
        </div>
    </div>
</div>
@endsection