@foreach($sponsors as $sponsor)
<div class="col-md-4">
    <!-- Image Post -->
    <div class="blog-post blog-post-column m-b-20">
        <div class="post-image col-md-5">	                                    	
            @if(!empty($sponsor->media_id))
        		<img src="{{asset('storage/app/public/media/'.$sponsor->media_id.'/'.$sponsor->media_type.'/'.$sponsor->media_file)}}" alt="" class="img-responsive">
        	@else
            	<img src="{{asset('public/assets/images/blog/1.jpg')}}" alt="" class="img-responsive">
            @endif	                                       
           
        </div>
        <div class="p-20">
           	<div class="text-muted">
           		<span>{{$sponsor->name}} </span> 
           	</div>
            <div class="post-title">
                <h3><a href="javascript:void(0);" data-id="{{$sponsor->id}}">{{$sponsor->title}}</a></h3>
            </div>
           
            <div>{{strip_tags($sponsor->Sponsor__short_description)}}
            </div>
            <div class="text-right">
                <a href="{{ url('/sponsor/'.str_slug($sponsor->name, '-').str_slug($sponsor->title, '-').','.$sponsor->id.'.html') }}" class="btn btn-success btn-sm waves-effect waves-light">View Sponsor <i class="mdi mdi-arrow-right m-l-5"></i></a>
            </div>
        </div>
    </div>
</div>
@endforeach
                            