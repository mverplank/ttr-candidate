@extends('backend.layouts.main')

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Change Password</h4>
                        <ol class="breadcrumb p-0 m-0">
                            
                            <li>
                                <a href="{{url('admin')}}">Dashboard</a>
                            </li>
                            <li class="active">
                                <a href="{{url('admin/advertising/banners')}}">Admin</a>
                            </li>
                            <li class="active">
                               Change Password
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="card-box">
                    {{ Form::open(array('route' =>'changepassword', 'method' => 'POST', 'id'=>'AdminChangePasword')) }}
                     <form method="POST" action="{{ route('changepassword') }}">
                        @csrf
                        <div class="p-20">
                            <div class="form-group row">   
                                {{Form::label('current_password', 'Current Password', array('class' => 'col-sm-4 form-control-label'))}}
                                <div class="col-sm-7"> 
                                    {{ Form::password('current_password', array('id' => 'current_password', "class" => "form-control input $errors->has('current_password') ? ' is-danger' : ''",'placeholder' =>"Current Password")) }}
                                    @if ($errors->has('current_password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('current_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="email">New password:</label>
                                 </div>
                                <div class="col-sm-7">
                                {{ Form::password('new_password', array('id' => 'new_password', "class" => "form-control password-field $errors->has('new_password') ? ' is-invalid' : '' ",'placeholder' =>"New Password")) }}
                                    <!-- password strength metter  -->
                                     <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                    @if ($errors->has('new_password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('new_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <span toggle=".password-field" class="fa fa-fw fa-eye field-icon toggle-password" style="font-size: 20px;"></span>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="email">Confirm password:</label>
                                </div>
                                <div class="col-sm-7">
                            {{ Form::password('confirm_password', array('id' => 'confirm_password', "class" => "form-control $errors->has('confirm_password') ? ' is-invalid' : '' ",'placeholder' =>"Confirm Password")) }}                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-6 col-xs-offset-6">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>                    
                            </div>                    
                        </div>                    
                    </form>               
                </div>
            </div>
        </div>   
    </div>
</div>
@endsection