@php
  //
  // Set right channel details
  //
  // ---
  // Used urls (check Config/routes.php)
  $url = array(
      'viewEpisode' => '/episode/',
      'viewShow' => '/show-details/',
      'viewHost' => '/host/'
  );
  // ---


  $mediaFilename = 'show-episode.mp3';
  $charsLimit = 4000; // <itunes:summary> limit
  $title = '';
  $description = '';
  $channelUrl = null;
  $channelImg = array('url' => '', 'title' => '', 'link' => '');
  $subtitle = null;

  if (!empty($host)) { // if Host defiend - use his details as rss channel properties (prority over channel)
    $title        = $host->itunes_title;
    $description  = preg_replace('/\s\s+/', ' ', (strip_tags($host->itunes_desc)));
    $subtitle     = $host->itunes_subtitle;
    $channelUrl   = url($url['viewHost'].str_slug($host->name, '-').','.$host->id.'.html');

    $img          = get_media($host->id, 'Host', 'itunes');
    if(empty($img))
      $img        = get_media($host->id, 'Host', 'photo');

    if(empty($img))
      $channelImg = array(
            'url' => str_replace('https://', 'http://', asset('storage/app/public/media/'.$img[0]['mid'].'/image/'.$img[0]['media_file'])),
            'title' => $host->name,
            'link' => $channelUrl
        );
    else
      $channelImg = array(
            'url' => str_replace('https://', 'http://', asset('storage/app/public/media/'.$img[0]['mid'].'/image/'.$img[0]['media_file'])),
            'title' => $host->name,
            'link' => $channelUrl
        );
    $ituneCategories = array();
    $_cats = json_decode($host->itunes_categories, true);
    foreach ($_cats as $cat) {

        //$cats[] = $cat['category'];

        if (!empty($cat['subcategory'])) {

            if (!isset($ituneCategories[$cat['category']])) {
                $ituneCategories[$cat['category']] = array();
            }
            $ituneCategories[$cat['category']][] = $cat['subcategory'];
        } elseif (!isset($ituneCategories[$cat['category']])) {

            $ituneCategories[$cat['category']] = array();
        }
    }
    //$ituneCategories[] = 'dummy';
  } elseif (isset($guest)) { // if Guest defiend - use his details as rss channel properties (prority over channel)
      $title = $guest[0]['fullname'];
      $description = preg_replace('/\s\s+/', ' ', (strip_tags($guest['Guest']['bio'])));
      //$channelUrl = $this->Html->url($url['viewHost'].$this->Base->slug($host[0]['fullname']).','.$host['Host']['id'], true);
      $channelUrl = $this->Base->slug($guest[0]['fullname'], $guest['Guest']['id'], $url['viewGuest'], '', ',', true);

      if (file_exists($files . DS . 'Guest' . DS . $guest['Guest']['id'] . DS . 'photo' . DS . '001.jpg')) {

          $channelImg = array(
              'url' => str_replace('https://', 'http://', $this->Html->url('/files/Guest/' . $guest['Guest']['id'] . '/photo/001.jpg', true)),
              'title' => $guest[0]['fullname'],
              'link' => $channelUrl
          );
      }
  } elseif (isset($channel) && !empty($channel)) {    // if no Host, but Channel defiend - use it's details as rss channel properties
      //print_r($channel);die;
      $title = $channel[0]->name;
      $description = preg_replace('/\s\s+/', ' ', (strip_tags($channel[0]->description)));
      $channelUrl = !empty($channel[0]->domain) ? '//' . $channel[0]->domain : str_replace('https://', 'http://', url('/'));
      /*if (file_exists($files . DS . 'Channel' . DS . $channel[0]->id . DS . 'cover' . DS . '001.jpg')) {

          $channelImg = array(
              'url' => str_replace('https://', 'http://', $this->Html->url('/files/Channel/' . $channel['Channel']['id'] . '/cover/001.jpg', true)),
              'title' => $title,
              'link' => $channelUrl
          );
      }*/
  } else { // all channels episodes - use common details
    //$title = Configure::read('Settings.meta_title');
    //$description = (strip_tags(Configure::read('Settings.meta_description')));
    $channelUrl = url('/');
    //$channelImg = array('url' => '', 'title' => '', 'link' => ''); // no image
  }

  // Remove & escape any HTML to make sure the feed content will validate.
  $description = preg_replace(array('/\&nbsp\;/i', "/[^\da-z\.\,\-\+\ \'\"\\\:\/]/i"), "", (strip_tags($description)));
  $desc = strlen($description)>$charsLimit?str_limit($description, $charsLimit).'...':$description;
  

  if (trim($desc) == '') {


    $desc = 'No description available';
  }
  //echo $desc;
  if (preg_match("/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i", 'producer@transformationtalkradio.com', $matches)) {

      $email = $matches[0];
  } else {
      $email = null;
  }

  $query = !empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : '';
  
  $channelData =  [
                    'atom__link' => array('href' => route('feed_itunes'). $query, 'rel' => "self", 'type' => "application/rss+xml"),
                    'title' => strip_tags($title),
                    'description' => $desc,
                    'link' => $channelUrl,
                    'image' => $channelImg,
                    'language' => 'en-us'
                    // itunes
                    , 'itunes__owner' => array('itunes__name' => (strip_tags('Transformation Talk Radio - Positive Talk Radio')), 'itunes__email' => $email)
                    , 'itunes__author' => (strip_tags('Transformation Talk Radio - Positive Talk Radio'))
                    , 'itunes__title' => strip_tags($title)
                    , 'itunes__subtitle' => strip_tags($subtitle)
                    , 'itunes__summary' => $desc    
                    , 'itunes__explicit' => 'clean'    
                    // workaround - we have to use 'itunes__' instead of 'itunes:' beasue RSS CakePHP helper don't support names with ":"
                    , "itunes__image" => array('href' => $channelImg['url'])
                    // CakePHP don't allow to apply mulitple items with this same name - see layout file
                    , 'itunesCategories' => ''
                  ];
@endphp
{!! '<'.'?'.'xml version="1.0" encoding="UTF-8" ?>' !!}
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
  <channel>
    @foreach($channelData as $key => $val)
      @if(!is_array($val))
        {!! createRss($key, $val) !!}
      @else
        @if($key == 'itunes__image')
          <{{ str_replace('__', ':', $key) }} href="{{ $val['href'] }}" />
        @elseif($key == 'atom__link')
          <{{ str_replace('__', ':', $key) }} href="{{ $val['href'] }}" rel="{{ $val['rel'] }}" type="{{ $val['type'] }}" />
        @elseif($key == 'image' && empty($val['url']) && empty($val['title']) && empty($val['url']) && empty($val['link']))
          <{{ str_replace('__', ':', $key) }} />
        @else
          <{{ str_replace('__', ':', $key) }}>
          @foreach($val as $key1 => $val1)
            {{ createRss($key1, $val1) }}
            @php
              //print_r($val1)
            @endphp
          @endforeach
          </{{ str_replace('__', ':', $key) }}>
        @endif

        
      @endif
      @php
        //print_r($cd);
      @endphp
    @endforeach
    @php $i = 0; $str = ''; $total = isset($ituneCategories)?count($ituneCategories):0; @endphp
    @if(isset($ituneCategories) && !empty($ituneCategories))
      @foreach($ituneCategories as $val)
        @if($i == 0)
          @php $str = str_replace('__', ':', $key) @endphp
          <{{ $str }} text="{!! $val[$i] !!}">
        @else
          <{{ $str }} text="{!! $val !!}" />
        @endif
        @php $i++ @endphp
        @if($i == $total)
          </{{ $str }}>
        @endif
      @endforeach
    @endif
    @foreach($episodes as $episode)
      @php
        /*echo '<pre>';
        print_r($episode);
        die;*/
        
        if ($episode->Episode_id > 0) { // Episode defined
          $episodeTime = strtotime($episode->date . ' ' . $episode->time);
          $episodeLink = url($url['viewEpisode'].urlencode(str_slug($episode->Episode__title, '-').','.$episode->Episode_id.'.html'));
          $mode = 'Episode';
          $episodeDescription = $episode->description !== '' ? $episode->description : $episode->Show_description;
        } else {    // Show defined (Episode not defined)
          $episodeTime = ''; //strtotime($episode['Show']['date'] . ' ' . $episode['Show']['time']);
          $episodeLink = url($url['viewShow'].urlencode($episode->Show_name).','.$episode->Show_id.'.html');
          $episodeDescription = $episode->Show_description;
          $mode = 'Show';
        }
        //die; 
          // Remove & escape any HTML to make sure the feed content will validate.
          $bodyTextFull = preg_replace(array('/\&nbsp\;/i', "/[^\da-z\.\,\-\+\ \'\"\\\:\/]/i"), "", (strip_tags($episodeDescription)));

          $bodyText = strlen($description)>$charsLimit?str_limit($bodyTextFull, $charsLimit).'...':$bodyTextFull;
          
          
          // convert spaces to commas
          $keywords = str_replace([" ", "  ", "\s"], ',', $episode->Show_name);

          $firstGuest = !empty($episode->Guest_name) ? $episode->Guest_name : ($episode->Guest_title . ' ' . $episode->Guest_firstname . ' ' . $episode->Guest_lastname . ' ' . $episode->Guest_postfix);
          $firstHost = !empty($episode->Host_fullname) ? $episode->Host_fullname : ($episode->Profile_title. ' ' . $episode->Profile_firstname. ' ' . $episode->Profile_lastname . ' ' . $episode->Profile_postfix);

          //
          // Find right image to view for item.
          //
          // If any guest - use his image (first guest if more), if no guest or guest image don't exists - use host image (first if more)
          $img = '';
          $_img = false;
          $_imgOf = null;
          $w = null;
          $h = null;

          if (!$_img && $mode == 'Episode') {   // Episode image
            //if (isset($episode['EpisodesHasHost']['hosts_id'])) { // try to get Host photo
            //$subPath = DS . 'Host' . DS . $episode['EpisodesHasHost'][$i]['id'] . DS . 'photo' . DS . '001.jpg';
            /*$subPath = DS . 'Episode' . DS . $episode['Episode']['id'] . DS . 'cover' . DS . '001.jpg';
            $imgFile = $files . $subPath;*/

            $episode_image = get_media($episode->Episode_id, 'Episode', 'cover');
            if(!empty($episode_image))
            {
              //print_r($episode_image);
              if(file_exists(storage_path().'/app/public/media/'.$episode_image[0]['mid'].'/image/'.$episode_image[0]['media_file'])){
                $_img = asset("storage/app/public/media/".$episode_image[0]['mid'].'/image/'.$episode_image[0]['media_file']);
                $w = 120;
                $_imgOf = 'Episode';
              }
            }
          }
          if (!$_img && !isset($guest) || (isset($host) && isset($guest))) {   // show Guest image ONLY if not showing episodes for guest; in this case show host image
            if (isset($episode->guests_id) && $episode->guests_id) { // try to get Guest photo
              $guest_image = get_media($episode->guests_id, 'Guest', 'photo');
              if(!empty($guest_image))
              {
                if(file_exists(storage_path().'/app/public/media/'.$guest_image[0]['mid'].'/image/'.$guest_image[0]['media_file'])){
                  $_img = asset("storage/app/public/media/".$guest_image[0]['mid'].'/image/'.$guest_image[0]['media_file']);
                  $_imgOf = 'Guest';
                }
              }
            }
          }
          
          if (!$_img) {   // show Host image
            if (isset($episode->hosts_id) && $episode->hosts_id) { // try to get Host photo
              $host_image = get_media($episode->hosts_id, 'Host', 'photo');
              if(!empty($host_image))
              {
                if(file_exists(storage_path().'/app/public/media/'.$host_image[0]['mid'].'/image/'.$host_image[0]['media_file'])){
                  $_img = asset("storage/app/public/media/".$host_image[0]['mid'].'/image/'.$host_image[0]['media_file']);
                  $_imgOf = 'Host';
                }
              }
            }
          }
          
          if ($_img) {
              $imgUrl = str_replace('https://', 'http://', $_img);
          }
          
          //
          // Check media (mp3 file) exisits for this episode
          //
          $enclosure = array();
          $duration = array();
          if ($type == 'archived') {
            $mediaFiles = get_media($episode->Episode_id, 'Episode', 'media');
            if (count($mediaFiles) < 0) {   // file exists
              if(file_exists(storage_path().'/app/public/media/'.$mediaFiles[0]['mid'].'/audio/'.$mediaFiles[0]['media_file'])){
                $file = asset("storage/app/public/media/".$mediaFiles[0]['mid'].'/audio/'.$mediaFiles[0]['media_file']);
                $file_path = storage_path().'/app/public/media/'.$mediaFiles[0]['mid'].'/audio/'.$mediaFiles[0]['media_file'];
                $enclosure = array(
                              'url' => str_replace('https://', 'http://', $file)
                              , 'type' => 'audio/mp3'
                              , 'length' => filesize($file_path)
                            );
              }
            } else {
              // archived episodes located at remote server or other directory
              if (!empty($channel[0]->url_archived_episodes) && !empty($episode->stream)){
                // try to get media file params
                /*$subPath = DS . 'shows' . DS;
                $mediaFilesDir = APP . 'webroot' . $subPath;
                $mediaFile = $mediaFilesDir . $episode->stream;*/
                $length = 0;
                if (file_exists($episode->stream)) {
                  $length = filesize($episode->stream)?filesize($episode->stream):0;
                }

                $enclosure = array(
                              'url' => str_replace('https://', 'http://', $episode->stream)
                              , 'type' => 'audio/mp3'
                              , 'length' => $length
                            );
              }
            }
          }
      @endphp
      @if($type == 'upcoming')
        <item>
          @php
            $item_name = strip_tags($episode->Episode_id > 0 && !empty($episode->Episode__title) ? trim($episode->Episode__title) : trim($episode->Show_name));
            $description = $img . $bodyText;
            $subtitle = strlen($bodyText)>255?str_limit($bodyText, 255).'...':$bodyText;
          @endphp
          <title>{{ $item_name }}</title>
          <description>{{ $description }}</description>
          <link>{{ $episodeLink }}</link>
          <guid isPermaLink="true">{{ $episodeLink }}</guid>
          <pubDate>{{ $episodeTime }}</pubDate>
          <itunes:author>{{ $title }}</itunes:author>
          <itunes:explicit>clean</itunes:explicit>
          <itunes:subtitle>{{$subtitle}}</itunes:subtitle>
          <itunes:keywords>@php echo isset($episode->Tag_name) ? str_replace([" ", "  ", "\s"], ',', $episode->Tag_name) : $keywords @endphp</itunes:keywords>
          <itunes:image href="@isset($imgUrl){!! $imgUrl !!}@endisset"/>
          <itunes:summary>{{ $bodyTextFull }}</itunes:summary>
          @if(!empty($duration))
            <itunes:duration></itunes:duration>
          @endif
        </item>
      @else
        @if(count($enclosure))
          <item>
            @php
              $item_name = strip_tags($episode->Episode_id > 0 && !empty($episode->Episode__title) ? trim($episode->Episode__title) : trim($episode->Show_name));
              $description = $img . $bodyText;
              $subtitle = strlen($bodyText)>255?str_limit($bodyText, 255).'...':$bodyText;
            @endphp
            <title>{{ $item_name }}</title>
            <description>{{ $description }}</description>
            <link>{{ $episodeLink }}</link>
            <guid isPermaLink="true">{{ $episodeLink }}</guid>
            <pubDate>{{ $episodeTime }}</pubDate>
            <itunes:author>{{ $title }}</itunes:author>
            <itunes:explicit>clean</itunes:explicit>
            <itunes:subtitle>{{$subtitle}}</itunes:subtitle>
            <itunes:keywords>@php echo isset($episode->Tag_name) ? str_replace([" ", "  ", "\s"], ',', $episode->Tag_name) : $keywords @endphp</itunes:keywords>
            <itunes:image href="@isset($imgUrl){!! $imgUrl !!}@endisset"/>
            <itunes:summary>{{ $bodyTextFull }}</itunes:summary>
            @if(!empty($duration))
              <itunes:duration></itunes:duration>
            @endif
            @if(!empty($enclosure)) 
              <enclosure url="{{$enclosure['url']}}" type="{{$enclosure['type']}}" length="{{$enclosure['length']}}"/>
            @endif
          </item>
        @endif
      @endif
    @endforeach
  </channel>
</rss>