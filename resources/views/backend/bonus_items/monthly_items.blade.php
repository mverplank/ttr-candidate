@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Monthly / Weekly Bonus Items </h4> 
                        
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{route('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Configuration
                        </li>
                        <li class="active">
                            <a href="{{route('memberships')}}">Memberships</a>
                        </li>
                        <li class="active">
                            Monthly-Weekly Bonus Items
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box" style="padding:5px 20px;">
                <div class="row page_top_btns">
                    <div class="col-md-12">
                        <h4>
                        <p class="text-pink">
                            <strong>Membership</strong> - <span id=""> {{$membership_name}} </span>
                        </p>
                        </h4>
                    </div>
                </div>
            </div>
        </div>     
        <!-- end row -->   
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <span id="membership_id" data-id="{{$id}}"></span>
                    <table id="monthly_inventory_items_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Month</th>
                                <th>Bonus Items</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
@endsection
