@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Item</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                <a href="{{url('partner')}}">Dashboard</a>
                            @else
                                <a href="{{url('admin')}}">Dashboard</a>
                            @endif    
                        </li>
                        @if(Config::get('constants.PARTNER') == getCurrentUserType())
                            <li class="active">
                            <a href="{{url('partner/inventory/items/my')}}">Bonus Items</a>
                            </li>
                        @else
                            <li class="active">
                                Advertising
                            </li>
                            <li class="active">
                                <a href="{{url('admin/inventory/items')}}">All Items</a>
                            </li>
                        @endif
                        <li class="active">
                            Edit Item
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div id="yes_crop"></div>
        <div class="row">
            <!-- Form Starts-->
            {{ Form::open(array('url' => 'admin/inventory/items/edit/'.$inventory_item->id, 'id'=>'InventoryItemAdminEditForm')) }}
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        {{Form::hidden('form_type', 'edit', array('id'=>'form_type'))}}
                        {{Form::hidden('edit_id', $inventory_item->id, array('id'=>'edit_id'))}}
                        @if(Config::get('constants.ADMIN') == getCurrentUserType())
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Settings</h4>
                            {{Form::hidden('form_type', 'add', ['id'=>'form_type'])}}
                            <div class="form-group row">
                                {{Form::label('InventoryItemIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                <div class="col-sm-7">
                                    <div class="radio radio-info radio-inline">
                                        {{Form::radio('data[InventoryItem][is_online]', '1', ($inventory_item->is_online == 1 ) ? true : false, array('id'=>'InventoryItemIsOnline1'))}}
                                        {{Form::label('InventoryItemIsOnline1', 'Online')}}
                                    </div>
                                    <div class="radio radio-inline">
                                        {{Form::radio('data[InventoryItem][is_online]', '0', ($inventory_item->is_online == 0 ) ? true : false, array('id'=>'InventoryItemIsOnline0'))}}
                                        {{Form::label('InventoryItemIsOnline0', 'offline')}}
                                    </div>
                                </div>
                            </div><!-- end row -->
                            
                            <div class="form-group row">
                                {{Form::label('InventoryItemIsOnInitialEmail', 'Include on welcome email', array('class' => 'col-sm-4 form-control-label'))}}
                                <div class="col-sm-7">
                                    <div class="radio radio-purple radio-inline">
                                        {{Form::radio('data[InventoryItem][is_on_initial_email]', '1', ($inventory_item->is_on_initial_email == 1 ) ? true : false, array('id'=>'InventoryItemIsOnInitialEmail1'))}}
                                        {{Form::label('InventoryItemIsOnInitialEmail1', 'Yes')}}
                                    </div>
                                    <div class="radio radio-inline">
                                        {{Form::radio('data[InventoryItem][is_on_initial_email]', '0', ($inventory_item->is_on_initial_email == 0 ) ? true : false, array('id'=>'InventoryItemIsOnInitialEmail0'))}}
                                        {{Form::label('InventoryItemIsOnInitialEmail0', 'No')}}
                                    </div>
                                </div>
                            </div><!-- end row -->
                                
                            <div class="form-group row">
                                {{Form::label('InventoryItemInventoryCategoriesId', 'Category', array('class' => 'col-sm-4 form-control-label'))}}
                                <div class="col-sm-7">
                                    {{Form::select('data[InventoryItem][inventory_categories_id]', (!empty($inventory_categories) ? $inventory_categories : ''), (($inventory_item->inventory_categories_id) ? $inventory_item->inventory_categories_id :""), $attributes=array('id'=>'InventoryItemInventoryCategoriesId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                                </div>
                            </div><!-- end row -->
                        </div> <!-- end card-box -->
                        @endif
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Details</h4>
                            <div class="p-20">
                                @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                    <div class="form-group row">
                                        {{Form::label('InventoryItemInventoryCategoriesId', 'Category', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::select('data[InventoryItem][inventory_categories_id]', (!empty($inventory_categories) ? $inventory_categories : ''), (($inventory_item->inventory_categories_id) ? $inventory_item->inventory_categories_id :""), $attributes=array('id'=>'InventoryItemInventoryCategoriesId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                                        </div>
                                    </div>
                                @endif
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[InventoryItem][name]', $inventory_item->name, $attributes = array('class'=>'form-control required', 'id'=> 'InventoryItemName', 'data-parsley-maxlength'=>'255'))}}
                                        
                                        @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                        {{Form::hidden('data[InventoryItem][is_accepted]', 0, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemIsAccepted'))}}
                                        @endif

                                        @if(Config::get('constants.ADMIN') == getCurrentUserType())
                                        {{Form::hidden('data[InventoryItem][is_accepted]', 1, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemIsAccepted'))}}
                                        @endif

                                       
                                    </div>
                                </div><!-- end row -->
                               
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                                {{ Form::textarea('data[InventoryItem][description]', $inventory_item->description, ['id' => 'InventoryItemDescription', 'class'=>'form-control']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End editor -->      
                                <div class="form-group row">
                                    {{Form::label('InventoryItemImage0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'image')
                                                      <div class="col-sm-4 adding-medias media_InventoryItem_image" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'InventoryItem', 'image', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_InventoryItem_image" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="InventoryItem" data-submodule="image" ></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_InventoryItem_image_outer hidden" data-off="0" style="display: none;">
                                            </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="InventoryItem" data-submodule="image" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemValue', 'Value', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[InventoryItem][value]', $inventory_item->value, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemValue', 'data-parsley-maxlength'=>'45', 'placeholder'=>"0.00" ))}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemType', 'Type', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="InventoryItemTypeFile" value="file" name="data[InventoryItem][type]" {{$inventory_item->type == "file" ? "checked" : ""}} onclick="BonusItemType('file', this)">
                                            <label for="InventoryItemTypeFile"> File(s) </label>
                                        </div>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="InventoryItemTypeLink" value="link" name="data[InventoryItem][type]" {{$inventory_item->type == "link" ? "checked" : ""}} onclick="BonusItemType('link', this)">
                                            <label for="InventoryItemTypeLink"> Link(s) </label>
                                        </div> 
                                        <div class="radio radio-purple radio-inline">
                                            <input type="radio" id="InventoryItemTypeCode" value="code" name="data[InventoryItem][type]" {{$inventory_item->type == "code" ? "checked" : ""}} onclick="BonusItemType('code', this)">
                                            <label for="InventoryItemTypeCode"> Code/Coupon </label>
                                        </div>
                                    </div>
                                    <?php
                                        $file_style = "";
                                        $link_style = "";
                                        $code_style = "";
                                        if(!empty($inventory_item->type)){
                                            // echo $inventory_item->type."<br>";
                                            if($inventory_item->type == "file"){
                                                $file_style = "style=display:block";
                                                $link_style = "style=display:none";
                                                $code_style = "style=display:none";
                                            }else if($inventory_item->type == "link"){
                                                $file_style = "style=display:none";
                                                $link_style = "style=display:block";
                                                $code_style = "style=display:none";
                                            }else if($inventory_item->type == "code"){
                                                $file_style = "style=display:none";
                                                $link_style = "style=display:none";
                                                $code_style = "style=display:block";
                                            }
                                        } 
                                    ?>
                                    <div class="col-sm-8 type_file" {{$file_style}}>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'files')
                                                    <div class="col-sm-4 adding-medias media_InventoryItem_files" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                                     
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-right"> 
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_InventoryItem_files" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="InventoryItem" data-submodule="files"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_InventoryItem_files_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                Maximum 10 files. Maximum file size: 120MB.
                                            </span>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="InventoryItem" data-submodule="files" data-media_type="files" data-limit="10"data-dimension="" data-file_crop="1">Upload files</button>
                                            </div>
                                        </div> 
                                    </div> 
                                    <div class="col-sm-7 type_link" {{$link_style}}>
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Url</div>
                                                        </th>
                                                        <th>
                                                            <div>Description</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($inventory_item->inventory_item_urls->count() > 0)
                                                        @php($count = 1)
                                                        @foreach($inventory_item->inventory_item_urls as $url)
                                                            <tr data-id="" data-off="{{$count}}" style="display: table-row;" >
                                                                <td>
                                                                    {{Form::url('data[InventoryItemUrl]['.$count.'][url]', $url->url,  array('id' => 'InventoryItemUrl'.$count.'Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                                    {{Form::hidden('data[InventoryItemUrl]['.$count.'][id]', $url->id)}}
                                                                </td>
                                                                <td>
                                                                    {{ Form::textarea('data[InventoryItemUrl]['.$count.'][description]', $url->description, ['id' => 'InventoryItemUrl'.$count.'Description', 'class'=>'form-control','rows'=>1, 'cols'=>50]) }}
                                                                </td>
                                                                <td class="actions">
                                                                    <div class="col-sm-6 col-md-4 col-lg-3 delete_sponsor_extra_urls" style="display: block;">
                                                                        <i class="typcn typcn-delete"></i> 
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @php($count++)
                                                        @endforeach
                                                    @else
                                                        <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                                            <td>
                                                                {{Form::url('data[InventoryItemUrl][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'InventoryItemUrl1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                                {{Form::hidden('data[InventoryItemUrl][1][id]', '')}}
                                                            </td>
                                                            <td>
                                                                {{ Form::textarea('data[InventoryItemUrl][1][description]','', ['id' => 'InventoryItemUrl1Description', 'class'=>'form-control','rows'=>1, 'cols'=>50]) }}
                                                            </td>
                                                            <td class="actions">
                                                                <div class="col-sm-6 col-md-4 col-lg-3 delete_sponsor_extra_urls" style="display: block;">
                                                                    <i class="typcn typcn-delete"></i> 
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="InventoryItemHasManyInventoryItemUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7 type_code" {{$code_style}}>
                                        {{Form::text('data[InventoryItem][code]', $inventory_item->code, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemCode', 'data-parsley-maxlength'=>'45'))}}
                                    </div>
                                </div><!-- end row -->
                            </div>
                        </div> <!-- end card-box -->
                    </div>
                   
                </div>
                <!-- end row -->
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[InventoryItem][btnAdd]" id="InventoryItemAdminEditForm" data-form-id="InventoryItemAdminEditForm">
                            @if(Config::get('constants.ADMIN') == getCurrentUserType())
                                @if($inventory_item->is_accepted == 0)
                                    Accept
                                @else
                                    Update
                                @endif
                            @else
                                Update
                            @endif
                        </button>
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
        </div> <!-- end row -->
    </div> <!-- container -->
     @include('backend.cms.upload_media_popup',['module_id' =>$inventory_item->id,'main_module' => "InventoryItem"])
</div> <!-- content -->

@endsection
