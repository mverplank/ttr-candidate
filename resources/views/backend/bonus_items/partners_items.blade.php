@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Partners Bonus Items
                        <strong> - <span id="total_items">  </span></strong>
                    </h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                <a href="{{url('partner')}}">Dashboard</a>
                            @else
                                <a href="{{url('admin')}}">Dashboard</a>
                            @endif    
                        </li>
                        <li class="active">
                            Bonus Items
                        </li>
                        <li class="active">
                            Partner Items
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
       <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif

            @if(Config::get('constants.ADMIN') == getCurrentUserType())
            <div class="card-box">
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="new_inventory_items" value="0" name="filterInventoryItems" onclick="filterInventoryItems(0, 'is_accepted')" {{ ($is_accepted == 0) ? "checked" : "" }}>
                    <label for="new_inventory_items"> New </label>
                </div> 
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="accepted_inventory_items" value="1" name="filterInventoryItems" onclick="filterInventoryItems(1, 'is_accepted')" {{ ($is_accepted == 1) ? "checked" : "" }}>
                    <label for="accepted_inventory_items"> Accepted </label>
                </div>
            </div>
            @endif
            @if(Config::get('constants.PARTNER') == getCurrentUserType())
            <div style="float:right;">
                <a href="{{url('/partner/inventory/items/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">     Add Item
                </a>
            </div>
            @endif
        </div>     
        <!-- end row -->   
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="partners_items_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Item</th>
                                <th>Submitted By</th>
                                <th>Submitted At</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
