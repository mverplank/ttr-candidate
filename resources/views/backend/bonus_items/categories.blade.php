@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Item Categories</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            Item Categories
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="alert alert-success" style="display: none;" id="success_alert_ItemCatName"> </div>
            <div class="alert alert-danger" style="display: none;" id="alert_ItemCatName"></div>
            <!--  <div class="card-box"> -->
               
            <div style="float:right;">
                <a href="javascript:void(0)" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" id="AddCateBtn">Add Catgeory</a>
                <a href="javascript:void(0)" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5" id="AddCateCancel" style="display: none;">Cancel</a>                   
           </div>
            <!--  </div> -->
        </div>     
        <!-- end row -->  
        <div class="row" style="display: none;" id="AddItemCatgories">
           <div class="col-sm-12">
               <div class="card-box">
                   <h4 class="header-title m-t-0">New Category</h4>
                   <div class="p-20">
                       {{ Form::open(array('method' => 'post', 'id'=>'addItemform')) }}
                           <div class="form-group row"> 
                           {{Form::label('ItemCatName', 'Name', array('class' => 'form-control-label col-sm-1'))}}             
                              <div class="col-sm-10">
                              
                               {{Form::text('data[InventoryCategory][name]',$value = null, $attributes = array('class'=>'form-control', 'id'=>'InventoryCategoryName','autocomplete'=>'off'))}}  
                                                         
                              </div>
                           </div>
                           
                           <div class="form-group row"> 
                                <div class="col-sm-7 col-sm-offset-5">
                                   <button type="button" class="btn btn-primary waves-effect waves-light submit_form" value="Add" id="ItemCatSubmit">Add</button>
                                </div>                           
                           </div>
           
                        {{ Form::close() }}
                   </div>
               </div>
           </div>
        </div>
        <!-- end row -->  

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="items_categories_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Category Name</th>
                                <th></th>
                            </tr>
                        </thead>

                       
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
