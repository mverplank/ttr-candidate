@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Monthly Bonus Item</h4> 
                        
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{route('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="{{route('memberships')}}">Memberships</a>
                        </li>
                        <li class="active">
                            Monthly-Weekly Bonus Items
                        </li>
                        <li class="active">
                           Edit
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                {!! Form::open(['url' => '/admin/inventory/months/edit/'.$month->id.'/'.$membership->id, 'id' => 'InventoryMonthAdminEditForm']) !!}
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Membership:{{$membership->name}}</h4>
                <div class="form-group row">
                    {{Form::label('InventoryMonthDate', 'Month', array('class' => 'col-sm-4 form-control-label'))}}
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php $date = strtotime($month->date);?>
                            {{Form::text('data[InventoryMonthDate]',date('m-Y',$date), ['id'=>'InventoryMonthDate', 'class'=>'form-control form_ui_input', 'readonly'])}}
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    {{Form::label('InventoryMonthItemInventoryItemsId', 'Assigned Items', array('class' => 'col-sm-4 form-control-label'))}}
                    <div class="col-sm-7">  
                        <?php
                            $items = array();
                            if(isset($all_items) && !empty($all_items)){
                                foreach($all_items as $item){
                                    $items[$item->id] = '('.$item->category_name.') '.$item->name;
                                }
                            }
                        ?>
                        
                        @if(!empty($items))
                        @php ($itemlist = 0)
                        @foreach ( $items as $i =>$item )
                        <div class="row">
                            <div class="checkbox checkbox-pink col-sm-9">
                                {!! Form::checkbox( 'data[InventoryMonthItem]['.$itemlist.'][inventory_items_id]', $i, (array_key_exists($i, $get_inventory_relation_ids)) ? 'checked' : '' , ['class' => 'md-check ItemClass', 'id' =>'InventoryMonthItem'.$itemlist.'InventoryItemsId'.$i] ) !!}
                                {!! Form::label('InventoryMonthItem'.$itemlist.'InventoryItemsId'.$i,  $item) !!}
                                @if(array_key_exists($i, $get_inventory_relation_ids))
                                    {{Form::hidden('data[InventoryMonthItem]['.$itemlist.'][id]', $get_inventory_relation_ids[$i], ['class'=>'inventory_relation_id'])}}
                                @else
                                    {{Form::hidden('data[InventoryMonthItem]['.$itemlist.'][id]', 0, ['class'=>'inventory_relation_id'])}}
                                @endif
                            </div>
                           
                            @if(array_key_exists($i, $get_inventory_relation_ids))
                            <div class="col-sm-3 ItemSetOpt">
                                {{Form::select('data[InventoryMonthItem]['.$itemlist.'][week]',$weekly, $get_inventory_week[$i], ['id'=>'InventoryMonthItem'.$itemlist.'Week', 'class'=>'form-control form_ui_select'])}}
                            </div>
                            @else
                                <div class="col-sm-3 ItemSetOpt" style="display: none;">
                                    {{Form::select('data[InventoryMonthItem]['.$itemlist.'][week]',$weekly, null, ['id'=>'InventoryMonthItem'.$itemlist.'Week', 'class'=>'form-control form_ui_select'])}}
                                </div>
                            @endif
                        </div>
                        @php ($itemlist++)
                        @endforeach
                        @else
                            No Bonus Items assigned yet.
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" data-form-id="InventoryMonthAdminEditForm" class="btn btn-primary waves-effect waves-light submit_form" name="data[InventoryMonth][btnSave]"> Edit
                        </button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>     
        <!-- end row -->   
    </div> <!-- container -->
</div> <!-- content -->
@endsection
