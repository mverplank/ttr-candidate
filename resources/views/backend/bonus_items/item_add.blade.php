@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
    a.list-delete {
        position: absolute;
        right: 0;
        top: -2px;
        font-size: 22px;
        color: #d23f3f;
        cursor: pointer;
    }
    .type_file img.bonu-item-img {
        width: 15px;
    }
    .type_file .jFiler-item-inner {
        position: relative;
    }
    .type_file a.icon-jfi-trash.jFiler-item-trash-action.color_icon.delete_InventoryItem_files {
        position: absolute;
        top: 0;
        right: 0;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Item</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                <a href="{{url('partner')}}">Dashboard</a>
                            @else
                                <a href="{{url('admin')}}">Dashboard</a>
                            @endif    
                        </li>
                        <li class="active">
                            Advertising
                        </li>
                        <li class="active">
                            @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                <a href="{{url('partner/inventory/items/my')}}">All Items</a>
                            @else
                                <a href="{{url('admin/inventory/items')}}">All Items</a>
                            @endif                            
                        </li>
                        <li class="active">
                            Add Item
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div id="yes_crop"></div>
        <div class="row">
            <!-- Form Starts-->
            {{ Form::open(array('url' => 'admin/inventory/items/add', 'id'=>'InventoryItemAdminAddForm')) }}
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        {{Form::hidden('form_type', 'add', array('id'=>'form_type'))}}
                        @if(Config::get('constants.ADMIN') == getCurrentUserType())
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Settings</h4>
                                {{Form::hidden('form_type', 'add', ['id'=>'form_type'])}}
                                <div class="form-group row">
                                    {{Form::label('InventoryItemIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            {{Form::radio('data[InventoryItem][is_online]', '1', true, array('id'=>'InventoryItemIsOnline1'))}}
                                            {{Form::label('InventoryItemIsOnline1', 'Online')}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{Form::radio('data[InventoryItem][is_online]', '0', false, array('id'=>'InventoryItemIsOnline0'))}}
                                            {{Form::label('InventoryItemIsOnline0', 'offline')}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemIsOnInitialEmail', 'Include on welcome email', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">

                                        <div class="radio radio-purple radio-inline">
                                            {{Form::radio('data[InventoryItem][is_on_initial_email]', '1', true, array('id'=>'InventoryItemIsOnInitialEmail1'))}}
                                            {{Form::label('InventoryItemIsOnInitialEmail1', 'Yes')}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{Form::radio('data[InventoryItem][is_on_initial_email]', '0', false, array('id'=>'InventoryItemIsOnInitialEmail0'))}}
                                            {{Form::label('InventoryItemIsOnInitialEmail0', 'No')}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemInventoryCategoriesId', 'Category', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::select('data[InventoryItem][inventory_categories_id]', (!empty($inventory_categories) ? $inventory_categories : ''), '', $attributes=array('id'=>'InventoryItemInventoryCategoriesId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                                    </div>
                                </div><!-- end row -->
                            </div> <!-- end card-box -->
                        @endif
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Details</h4>
                            <div class="p-20">
                                @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                    <div class="form-group row">
                                        {{Form::label('InventoryItemInventoryCategoriesId', 'Category', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::select('data[InventoryItem][inventory_categories_id]', (!empty($inventory_categories) ? $inventory_categories : ''), '', $attributes=array('id'=>'InventoryItemInventoryCategoriesId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                                        </div>
                                        {{Form::hidden('data[InventoryItem][is_online]', 0)}}
                                    </div>
                                @endif
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[InventoryItem][name]', $value = null, $attributes = array('class'=>'form-control required', 'id'=> 'InventoryItemName', 'data-parsley-maxlength'=>'255'))}}
                                        
                                        @if(Config::get('constants.PARTNER') == getCurrentUserType())
                                        {{Form::hidden('data[InventoryItem][is_accepted]', 0, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemIsAccepted'))}}
                                        @endif

                                        @if(Config::get('constants.ADMIN') == getCurrentUserType())
                                        {{Form::hidden('data[InventoryItem][is_accepted]', 1, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemIsAccepted'))}}
                                        @endif

                                        
                                        {{Form::hidden('data[InventoryItem][users_id]', $current_user_id, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemUserid'))}}
                                    </div>
                                </div><!-- end row -->
                               
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                                {{ Form::textarea('data[InventoryItem][description]', null, ['id' => 'InventoryItemDescription', 'class'=>'form-control']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End editor --> 
                                <div class="form-group row">
                                    {{Form::label('InventoryItemImage0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'image')
                                                    <div class="col-sm-4 adding-medias media_InventoryItem_image" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'InventoryItem', 'image', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_InventoryItem_image" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="InventoryItem" data-submodule="image" ></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach                           
                                            @endif
                                            <div class="media_InventoryItem_image_outer hidden" data-off="0" style="display: none;">
                                            </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="InventoryItem" data-submodule="image" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemValue', 'Value', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[InventoryItem][value]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemValue', 'data-parsley-maxlength'=>'45', 'placeholder'=>"0.00" ))}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('InventoryItemType', 'Type', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="InventoryItemTypeFile" value="file" name="data[InventoryItem][type]" checked="" onclick="BonusItemType('file', this)">
                                            <label for="InventoryItemTypeFile"> File(s) </label>
                                        </div>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" id="InventoryItemTypeLink" value="link" name="data[InventoryItem][type]" onclick="BonusItemType('link', this)">
                                            <label for="InventoryItemTypeLink"> Link(s) </label>
                                        </div> 
                                        <div class="radio radio-purple radio-inline">
                                            <input type="radio" id="InventoryItemTypeCode" value="code" name="data[InventoryItem][type]" onclick="BonusItemType('code', this)">
                                            <label for="InventoryItemTypeCode"> Code/Coupon </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 type_file">
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'files')
                                                    <div class="col-sm-10 adding-medias media_InventoryItem_files" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">
                                                                    <div class="jFiler-item-container">
                                                                        <div class="jFiler-item-inner">             
                                                                            <div class="jFiler-bonus-item-thumb">    
                                                                                <a href="{{ url('download/app/public/media/'.$med->media->id.'/'.(empty($med->media->type)?'files':$med->media->type).'/'.$med->media->filename)}}">                                     
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                       <img class="bonu-item-img" src="{{asset('storage/app/public/media/default/file.png')}}" draggable="false">&nbsp; {!! $med->media->filename !!}
                                                                                    </div> 
                                                                                </a>  
                                                                                <a class="list-delete" onclick="mediaLinkDelete({{$med->id}}, this, '.col-sm-10');"data-mainmodule="InventoryItem" data-submodule="files"><i class="icon-jfi-trash color_icon"></i></a>
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_InventoryItem_files_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                Maximum 10 files. Maximum file size: 120MB.
                                            </span>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="InventoryItem" data-submodule="files" data-media_type="files" data-limit="10"data-dimension="" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>     
                                    <div class="col-sm-8 type_link">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Url</div>
                                                        </th>
                                                        <th>
                                                            <div>Description</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                                        <td>
                                                            {{Form::url('data[InventoryItemUrl][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'InventoryItemUrl1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td>
                                                            {{ Form::textarea('data[InventoryItemUrl][1][description]','', ['id' => 'InventoryItemUrl1Description', 'class'=>'form-control','rows'=>1, 'cols'=>50]) }}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 delete_items_extra_urls" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="InventoryItemHasManyInventoryItemUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 type_code">
                                        {{Form::text('data[InventoryItem][code]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'InventoryItemCode', 'data-parsley-maxlength'=>'45'))}}
                                    </div>
                                </div><!-- end row -->
                            </div>
                        </div> <!-- end card-box -->
                    </div>
                </div>
                <!-- end row -->
                <div class="form-group">
                        <div class="fixed_btn">
                            <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[InventoryItem][btnAdd]" id="InventoryItemBtnAdd" data-form-id="InventoryItemAdminAddForm">
                                Add
                            </button>
                           <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                        </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
        </div> <!-- end row -->
      
    </div> <!-- container -->
    @include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "InventoryItem"])
</div> <!-- content -->

@endsection
