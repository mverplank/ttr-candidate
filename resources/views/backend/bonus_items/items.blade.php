@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Bonus Items                    
                        <strong> - <span id="total_items">  </span></strong>
                    </h4> 
                        
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            All Items
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                
                <!-- <div class="col-md-2">
                    <p class="text-orange">
                        <strong>Total</strong> - <span id="total_items">  </span>
                    </p>
                </div> -->
                
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="online_inventory_items" value="1" name="filterInventoryItems" onclick="filterInventoryItems(1)" {{ ($is_online == 1) ? "checked" : "" }}>
                    <label for="online_sponsors"> Online </label>
                </div> 
                <div class="radio radio-inline">
                    <input type="radio" id="offline_inventory_items" value="0" name="filterInventoryItems" onclick="filterInventoryItems(0)" {{ ($is_online == 0) ? "checked" : "" }}>
                    <label for="offline_sponsors"> Offline </label>
                </div>
                <div style="float:right;">
                    <a href="{{url('/admin/inventory/items/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">     Add Item
                    </a>
                </div>
                    
            </div>
        </div>     
        <!-- end row -->   
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="inventory_items_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Item</th>
                                <th>Submitted By</th>
                                <th>Submitted At</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
@endsection
