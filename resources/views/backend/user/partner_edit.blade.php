@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
</style>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Partner</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/partners')}}">All Partners</a>
                        </li>
                        <li class="active">
                            Edit Partner
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div id="yes_crop"></div>
        
        <div class="row">
            <?php 
            // echo $user->username;
           
            //echo "<pre>";print_R($partner);die('success');
            ?>
            {{ Form::open(array('url' => 'admin/user/partners/edit/'.$partner->id, 'method' => 'PUT', 'id'=>'PartnerAdminEditForm')) }}
            <div class="col-xs-12">
                <?php echo $partner->title?>
                <!-- Form Starts-->
                <div class="row">

                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <input type="hidden" name="form_type" id="form_type" value="edit">
                            <input type="hidden" name="edit_id" id="edit_id" value="{{ $partner->id }}">
                            <h4 class="header-title m-t-0">Personal Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('name', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{$partner->title}}
                                        <div class="form-group col-sm-3">
                                            <?php 
                                                $prefixes = array('' => 'Choose prefix..', 'Dr.' => 'Dr.', 'Prof' => 'Prof', 'Pir' => 'Pir', 'Colonel' => 'Colonel');
                                            ?>
                                            {{Form::select('data[Profile][title]', $prefixes, (isset($partner->profiles[0]->title)) ? $partner->profiles[0]->title : '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                                            {{ Form::hidden('data[Partner][groups_id_radio]', '4') }}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][firstname]', (isset($partner->profiles[0]->firstname))  ? $partner->profiles[0]->firstname : '', $attributes = array('class'=>'form-control', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][lastname]', (isset($partner->profiles[0]->lastname))  ? $partner->profiles[0]->lastname : '', $attributes = array('class'=>'form-control', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][sufix]', array('' => 'Choose suffix..', 'Np' => 'Np', 'Jd' => 'Jd', 'M.s.' => 'M.s.'), (isset($partner->profiles[0]->sufix))  ? $partner->profiles[0]->sufix : '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                            {{ Form::hidden('data[Profile][id]', $partner->profiles[0]->id) }}
                                        </div>
                                    </div>
                                </div>
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('ProfileBio', 'Bio', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                                {{ Form::textarea('data[Profile][bio]', $partner->profiles[0]->bio, ['id' => 'ProfileBio']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- End editor -->
                                <div class="form-group row">
                                    {{Form::label('PartnerPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'photo')
                                                        <div class="col-sm-4 adding-medias media_Partner_photo" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="javascript:void(0)" onclick="editMediaImage(this, 'Partner', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-right">
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_InventoryItem_image" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Partner" data-submodule="photo"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_Partner_photo_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Partner" data-submodule="photo" data-limit="1" data-dimension="" data-media_type="image" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Account & Contact</h4>
                            <div class="p-20">
                                
                                <div class="form-group row">
                                    {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Partner][username]', (isset($partner->username))  ? $partner->username : '', $attributes = array('class'=>'form-control required', 'data-unique'=> '1','data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>$partner->id, 'data-parsley-maxlength'=>'45', 'id'=>'UserUsername'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::email('data[Partner][email]', (isset($partner->email))  ? $partner->email : '', $attributes = array('class'=>'form-control required', 'data-unique'=> '1','data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>$partner->id, 'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserPassword', 'Password', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::password('data[Partner][password]', $attributes = array('class'=>'form-control password-field', 'id'=>'UserPassword'))}}
                                        <!-- password strength metter  -->
                                        <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                    </div>
                                    <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfilePhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][phone]', (isset($partner->profiles[0]->phone))  ? $partner->profiles[0]->phone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfilePhone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileCellphone', 'Cell Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][cellphone]', (isset($partner->profiles[0]->cellphone))  ? $partner->profiles[0]->cellphone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype]', (isset($partner->profiles[0]->skype))  ? $partner->profiles[0]->skype : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileSkype'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype_phone]', (isset($partner->profiles[0]->skype_phone))  ? $partner->profiles[0]->skype_phone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'ProfileSkypePhone'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Settings</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio4" value="1" name="data[UserSetting][mailing_newsletter_radio]" {{(isset($partner->user_settings[0]->mailing_newsletter))  ? ($partner->user_settings[0]->mailing_newsletter == 1 ? 'checked' : '') : ''}} >
                                            <label for="inlineRadio4"> Enabled </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio5" value="0" name="data[UserSetting][mailing_newsletter_radio]" {{(isset($partner->user_settings[0]->mailing_newsletter))  ? ($partner->user_settings[0]->mailing_newsletter == 0 ? 'checked' : '') : ''}} >
                                            <label for="inlineRadio5"> Disabled </label>
                                        </div>
                                        {{ Form::hidden('data[UserSetting][id]', isset($partner->user_settings[0]->id) ? $partner->user_settings[0]->id : '') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" class="btn btn-primary waves-effect waves-light submit_form" value="edit" name="data[Partner][btnUpdate]" id="PartnerBtnEdit" data-form-id="PartnerAdminEditForm">
                            Update
                        </button>
                       <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                     
                    </div>
                </div>
                <!-- end row -->
            </div><!-- end col-->
            {{ Form::close() }}
        </div>
        <!-- end row -->
    @include('backend.cms.upload_media_popup',['module_id' => $partner->id,'main_module' => "Partner"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
