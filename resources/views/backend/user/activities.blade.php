@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Transaction Report</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            Transaction Report
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div><!-- end row --> 
        <div style="float:right;">
            <a href="{{url('/admin/user/activities/export')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>
        </div>        

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="trans_report_datatable" class="table table-striped display">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>User</th>
                                <th>Activity</th>
                            </tr>
                        </thead>
                        
                       

                    </table><!-- end datatable --> 
                    
                </div><!-- end card-box -->  
            </div><!-- end col-sm-12 -->  
        </div><!-- end row -->     
    </div> <!-- container -->
</div> <!-- content -->

@endsection
