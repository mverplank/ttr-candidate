@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Host</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/hosts')}}">All Hosts</a>
                        </li>
                        <li class="active">
                            Edit Host
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
		<!-- start row --> 
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">                
                    <div class="form-group row page_top_btns">
                        <div style="float:right;margin-right: 5px">
                            <a href="{{url('/')}}/admin/user/hosts/banners/{{$host->id}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" title ="Add Banner">Banner
                            </a>                           							
                        </div>
                        <div style="float:right; margin-right: 5px">
                            <a href="{{url('/')}}/admin/user/hosts/edit/custom_content/{{$host->id}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" title ="Add Custom Content">
                                Custom Content
                               <!--  <i class="fa fa-tasks" aria-hidden="true"></i> -->
                            </a>                                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- end row -->
        <!-- Modal -->
        <div id="custom-modal" class="modal-demo modal" style="width:1000px;">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Edit Image</h4>
            <div class="custom-modal-text">
                <!-- Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="image-container">
                                <img id="image" src="" alt="Picture" style="width:100%">
                            </div>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 docs-buttons">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">
                                        <span class="fa fa-search-plus"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">
                                        <span class="fa fa-search-minus"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, -10, 0)">
                                        <span class="fa fa-arrow-left"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 10, 0)">
                                        <span class="fa fa-arrow-right"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, -10)">
                                        <span class="fa fa-arrow-up"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, 10)">
                                        <span class="fa fa-arrow-down"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">
                                        <span class="fa fa-rotate-left"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">
                                        <span class="fa fa-rotate-right"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleX&quot;, -1)">
                                        <span class="fa fa-arrows-h"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleY&quot;, -1)">
                                        <span class="fa fa-arrows-v"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="crop" title="Crop">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;crop&quot;)">
                                        <span class="fa fa-check"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="clear" title="Clear">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;clear&quot;)">
                                        <span class="fa fa-remove"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;reset&quot;)">
                                        <span class="fa fa-refresh"></span>
                                    </span>
                                </button>
                                <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                    <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Import image with Blob URLs">
                                        <span class="fa fa-upload"></span>
                                    </span>
                                </label>
                            </div>

                            <div class="btn-group btn-group-crop">
                                <button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" data-image_for="photo" data-module="Host" data-form_type="add" data-crop="1" id="getCropped" data-old_image="" data-outer_dropzone="HostPhoto0" data-dropzone_obj="">
                                    <span class="docs-tooltip crop-upload" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { maxWidth: 4096, maxHeight: 4096 })">
                                        Apply
                                    </span>
                                </button>
                            </div>

                            <!-- Show the cropped image in modal -->
                            <div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.modal -->
                        </div><!-- /.docs-buttons -->

                        <div class="col-md-3 docs-toggles">
                        <!-- <h3>Toggles:</h3> -->
                        <div class="btn-group d-flex flex-nowrap" data-toggle="buttons">
                          <label class="btn btn-primary active">
                            <input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">
                              16:9
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">
                              4:3
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">
                              1:1
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">
                              2:3
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">
                              Free
                            </span>
                          </label>
                        </div>

                      </div><!-- /.docs-toggles -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <a href="#custom-modal" class="" data-animation="blur" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" id="open_modal"></a>
        <div class="row">
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/user/hosts/edit/'.$host->id, 'id' => 'HostAdminEditForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Personal Details</h4>
                            <div class="p-20">
								<!-- Last updated by -->
								@if($updatedby)
                                <?php 
							$date = date_format($host->updated_at,"dS M, Y");
							$nameby = $updatedby->title.' '.$updatedby->firstname.' '.$updatedby->lastname.' '.$updatedby->sufix
							?>
                                <div class="form-group row">
                                    {{Form::label('updateby', 'Last updated by', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
										{{ Form::text('data[Host][updateby]',$nameby, ['id' => 'updateby','class'=>'form-control','readonly']) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('updated_at', 'Last updated at', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{ Form::text('data[Host][updatedat]',$date, ['id' => 'updated_at','class'=>'form-control','readonly']) }}
                                    </div>
                                </div>
								@endif
                                <div class="form-group row">
                                    {{Form::hidden('form_type', 'edit', array('id'=>'form_type'))}}
                                    {{Form::hidden('edit_id', $host->id, array('id'=>'edit_id'))}}                                  
                                    {{Form::label('HostTypeHostTypeSet', 'Type', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-10">
                                        <div class="checkbox checkbox-info checkbox-inline">
                                            {{ Form::checkbox('data[HostType][HostType][]', '1' , (!empty($host_types_array) ? (in_array(1,$host_types_array) ? true : false) : false), array('id'=>'HostTypeHostType1')) }}
                                            {{Form::label('HostTypeHostType1', 'Host', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                        <div class="checkbox checkbox-purple checkbox-inline">
                                            {{ Form::checkbox('data[HostType][HostType][]', '2' , (!empty($host_types_array) ? (in_array(2,$host_types_array) ? true : false) : false), array('id'=>'HostTypeHostType2')) }}
                                            {{Form::label('HostTypeHostType2', 'Co-Host', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                        <div class="checkbox checkbox-custom checkbox-inline">
                                            {{ Form::checkbox('data[HostType][HostType][]', '3' , (!empty($host_types_array) ? (in_array(3,$host_types_array) ? true : false) : false), array('id'=>'HostTypeHostType3')) }}
                                            {{Form::label('HostTypeHostType3', 'Podcast-Host', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                    </div>   
                                </div>
                                <!-- End row -->                                
                                <div class="form-group row">
                                    {{Form::label('HostD', 'Name', array('class' => 'col-sm-2 form-control-label req'))}}
                                    <div class="col-sm-9">
                                        <div  class="row">
                                            <div class="form-group col-sm-3">
                                                {{Form::hidden('data[Profile][id]', ($host->user->count() > 0) ? ($host->user->profiles->count() > 0 ? $host->user->profiles[0]->id :"") : "")}}
                                                
                                                {{Form::select('data[Profile][title]', $prefix_suffix['prefix'], ($host->user->count() > 0) ? ($host->user->profiles->count() > 0 ? $host->user->profiles[0]->title :"") : "", $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::text('data[Profile][firstname]', ($host->user->count() > 0) ? ($host->user->profiles->count() > 0 ? $host->user->profiles[0]->firstname :"") : "", $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true, 'id'=>'ProfileFirstname'))}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::text('data[Profile][lastname]', ($host->user->count() > 0) ? ($host->user->profiles->count() > 0 ? $host->user->profiles[0]->lastname :"") : "", $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true, 'id'=>'ProfileLastname'))}}
                                            </div>
                                            <div class="form-group col-sm-3">
                                                {{Form::select('data[Profile][sufix]', $prefix_suffix['suffix'], ($host->user->count() > 0) ? ($host->user->profiles->count() > 0 ? $host->user->profiles[0]->sufix :"") : "", $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('HostProfileUrl', 'Host Profile Url', array('class' => 'col-sm-2 form-control-label req'))}}
                                    <div class="col-sm-9 profile_url">                        
                                        @if(!empty($profile_urls))
                                            <div class="form-group">
                                                {{Form::text('data[Host][profile_url]', '', $attributes = array('class'=>'form-control required', 'placeholder'=>(!empty($host->profile_url) ? $host->profile_url :'/{username}' ), 'id'=>'HostProfileUrl'))}}
                                            </div>                                           
                                            <div class="form-group col-sm-3">
                                            @foreach($profile_urls as $key=>$profile_url)
                                                <div class="radio radio-purple radio-inline"> 
                                                    {{Form::radio('data[Host][profile_urls]', $profile_url, !empty($host->profile_url) ? (($host->profile_url == $profile_url) ? true : false) : false, array('id'=>'HostProfileUrls'.$key, 'class'=>'radio_profile_url'))}}
                                                    {{Form::label('HostProfileUrls'.$key, $profile_url, array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                            @endforeach
                                            </div>
                                        @endif                                       
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- Start Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('HostBio', 'Bio', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <div class="card-box">
                                            <form method="post">
                                                {{ Form::textarea('data[Host][bio]', $host->bio, ['id' => 'HostBio']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- End editor -->
                                <div class="form-group row">
                                    {{Form::label('PartnerPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'photo')
                                                        <div class="col-sm-4 adding-medias media_Host_photo" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="javascript:void(0)" onclick="editMediaImage(this, 'Host', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-right">
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_photo" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="photo"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_Host_photo_outer hidden" data-off="0" style="display: none;">
                                            </div>
                                        </div>
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" id="profile_img_media_btn" data-toggle="modal" data-target="" data-mainmodule="Host" data-submodule="photo" data-limit="1" data-dimension="" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                                 <div class="form-group row">
                                    {{Form::label('HostHasManyUrls', 'Urls and Social Media', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-10">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table url_table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Type</div>
                                                        </th>
                                                        <th>
                                                            <div>Url</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($host->urls->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($host->urls as $url)
                                                    <tr data-off="{{$url_count}}" style="display: table-row;">
                                                        <td>
                                                            {{Form::hidden('data[Url]['.$url_count.'][id]', $url->id)}}
                                                            {{Form::select('data[Url]['.$url_count.'][type]', $social_media, $url->type, $attributes=array('id'=>'Url'.$url_count.'Type', 'class'=>'form-control'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::url('data[Url]['.$url_count.'][url]', $url->url,  array('step' => '1', 'min' => '1', 'id' => 'Url'.$url_count.'Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 {{($url_count > 1) ? 'delete_host_extra_urls' : 'host_extra_urls'}}" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php ($url_count++)
                                                    @endforeach
                                                    @else
                                                    <tr data-off="1" style="display: table-row;" data-rnd="">
                                                        <td>
                                                            {{Form::select('data[Url][1][type]', $social_media, '', $attributes=array('id'=>'Url1Type', 'class'=>'form-control'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::url('data[Url][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'Url1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 host_extra_urls" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End row -->
                                
                                <div class="form-group row">
                                    {{Form::label('HostHasManyVideos', 'Video Clips', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-10">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table video_table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Title</div>
                                                        </th>
                                                        <th>
                                                            <div>Video HTML code</div>
                                                        </th>
                                                        <th>
                                                            <div>Enabled</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($host->videos->count() > 0)
                                                    @php ($video_count = 1)
                                                    @foreach($host->videos as $video)
                                                    <tr data-off="{{$video_count}}" style="display: table-row;">
                                                        <td>
                                                            {{Form::hidden('data[Video]['.$video_count.'][id]', $video->id)}}
                                                            {{Form::text('data[Video]['.$video_count.'][title]', $video->title, $attributes = array('class'=>'form-control', 'id'=>'Video'.$video_count.'Title'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::textarea('data[Video]['.$video_count.'][code]', $video->code,  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video'.$video_count.'Code' ))}}
                                                        </td>
                                                         <td>
                                                            <div class="checkbox checkbox-pink checkbox-inline">
                                                                {{ Form::checkbox('data[Video]['.$video_count.'][is_enabled]', '1' , ($video->is_enabled == 1 ? true : false), array('id'=>'Video'.$video_count.'IsEnabled1')) }}
                                                                {{Form::label('Video'.$video_count.'IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                            </div>
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 {{($video_count > 1) ? 'delete_host_videos' : 'host_videos'}}" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php ($video_count++)
                                                    @endforeach
                                                    @else
                                                    <tr data-id="" data-off="1" style="display: table-row;">
                                                        <td>
                                                            {{Form::text('data[Video][1][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'Video1Title'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::textarea('data[Video][1][code]', '',  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video1Code' ))}}
                                                        </td>
                                                         <td>
                                                            <div class="checkbox checkbox-pink checkbox-inline">
                                                                {{ Form::checkbox('data[Video][1][is_enabled]', '1' , true, array('id'=>'Video1IsEnabled1')) }}
                                                                {{Form::label('Video1IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                            </div>
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 host_videos" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyVideosAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('HostTags', 'Tags', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">                                     
                                        <div class="form-group">
                                            {{Form::text('data[Host][tags]', $host->tags, ['id'=>'HostTags_tagsinput', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                                        </div>                                       
                                    </div>
                                </div> <!-- End row -->                             
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Account & Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-2 form-control-label req'))}}
                                    <div class="col-sm-9">
                                        {{Form::hidden('data[User][id]', $host->user->id)}}
                                        {{Form::text('data[User][username]', $host->user->username, $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-form'=> 'userprofile', 'data-userid'=>$host->user->id, 'data-validation' => '1','data-parsley-maxlength'=>'255', 'id'=>'UserUsername', 'required'=>true))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                    
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-2 form-control-label req'))}}
                                    <div class="col-sm-9">
                                        {{Form::email('data[User][email]', $host->user->email, $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-form'=> 'userprofile', 'data-userid'=>$host->user->id,'data-validation' => '1', 'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                   {{Form::label('UserAlternateEmail', 'Alternate Email', array('class' => 'col-sm-2 form-control-label'))}}
                                   <div class="col-sm-9">
                                      {{Form::email('data[Host][alternate_email]',$host->alternate_email, $attributes = array('class'=>'form-control','id'=>'UserAlternateEmail',))}}
                                      <span class="error-block" style="display:none;"></span>
                                   </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserPassword', 'Password', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::password('data[User][password]', $attributes = array('class'=>'form-control password-field', 'id'=>'UserPassword'))}}
                                        <!-- password strength metter  -->
                                        <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                    </div>
                                    <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfilePhone', 'Landline Phone', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Profile][phone]', ($host->user->profiles->count() > 0) ? $host->user->profiles[0]->phone : null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfilePhone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileCellphone', 'Cell Phone', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Profile][cellphone]', ($host->user->profiles->count() > 0) ? $host->user->profiles[0]->cellphone : null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Profile][skype]', ($host->user->profiles->count() > 0) ? $host->user->profiles[0]->skype : null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileSkype'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('ProfileSkypePhone', 'Skype phone', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Profile][skype_phone]', ($host->user->profiles->count() > 0) ? $host->user->profiles[0]->skype_phone : null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'ProfileSkypePhone'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                   {{Form::label('ZoomLink', 'Zoom Link', array('class' => 'col-sm-2 form-control-label'))}}
                                   <div class="col-sm-9 tag-input-type">
                                      <div class="form-group">
                                         {{Form::url('data[Host][zoom_link]',$host->zoom_link, $attributes = array('class'=>'form-control', 'id'=>'ZoomLink'))}}
                                      </div>
                                   </div>
                                </div><!-- End row --> 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Membership / Podcasts Host settings</h4>
                            <div class="p-20">            
                                <div class="form-group row">
                                    {{Form::label('UserSettingMembershipDataJsonMaxUploadsPerWeek', 'Max weekly uploads', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::number('data[UserSetting][membership_data_json][max_uploads_per_week]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->membership_data_json) ? $host->user->user_settings[0]->membership_data_json->max_uploads_per_week : 0) : 0, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'UserSettingMembershipDataJsonMaxUploadsPerWeek'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Maximum number of allowed podcast uploads per week (for all shows); set zero to unlimited
                                        </span>
                                    </div>
                                    {{Form::hidden('data[UserSetting][membership_data_json][max_uploads_per_week_params][min]', 0)}}
                                </div>

                                <div class="form-group row">
                                    {{Form::label('UserSettingMembershipEnd', 'Membership End', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">                          
                                         <div class="input-group">
                                            {{Form::text('data[UserSetting][membership_end]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->membership_end) ? date_format(date_create($host->user->user_settings[0]->membership_end), 'm/d/Y') : '') : '', $attributes = array('class'=>'form-control', 'placeholder'=>'mm/dd/yyyy', 'id'=>'datepicker-autoclose'))}}
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                        </div><!-- input-group -->

                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Membership expiration date; leave empty to never expire
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Other Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('HostPrType', 'Type', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-10">
                                        <div class="radio radio-info radio-inline">
                                            {{Form::radio('data[Host][pr_type]', 'pr', ($host->pr_type) ? (($host->pr_type == "pr") ? true : false) : false, array('id'=>'HostPrTypePr'))}}
                                            {{Form::label('HostPrTypePr', 'Pr', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-warning radio-inline">
                                            {{Form::radio('data[Host][pr_type]', 'publisher', ($host->pr_type) ? (($host->pr_type == "publisher") ? true : false) : false, array('id'=>'HostPrTypePublisher'))}}
                                            {{Form::label('HostPrTypePublisher', 'Publisher', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-purple radio-inline">
                                            {{Form::radio('data[Host][pr_type]', 'assistant', ($host->pr_type) ? (($host->pr_type == "assistant") ? true : false) : false, array('id'=>'HostPrTypeAssistant'))}}
                                            {{Form::label('HostPrTypeAssistant', 'Assistant', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-custom radio-inline">
                                            {{Form::radio('data[Host][pr_type]', 'other', ($host->pr_type) ? (($host->pr_type == "other") ? true : false) : false, array('id'=>'HostPrTypeOther'))}}
                                            {{Form::label('HostPrTypeOther', 'Other', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('HostPrName', 'Name', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][pr_name]', $host->pr_name, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrName'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('HostPrCompanyName', 'Company Name', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][pr_company_name]', $host->pr_company_name,  $attributes = array('class'=>'form-control', 'id'=>'HostPrCompanyName'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('HostPrEmail', 'Email', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::email('data[Host][pr_email]', $host->pr_email, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrEmail'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('HostPrPhone', 'Landline Phone', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][pr_phone]', $host->pr_phone, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrPhone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('HostPrCellphone', 'Cell phone', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][pr_cellphone]', $host->pr_cellphone, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('HostPrSkype', 'Skype', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][pr_skype]', $host->pr_skype, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'HostPrSkype'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('HostPrSkypePhone', 'Skype phone', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][pr_skype_phone]', $host->pr_skype_phone, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'HostPrSkypePhone'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">iTunes Profile Setup</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('HostRssFeedUrl', 'iTunes feed url', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <a href="{{url('feed/episodes/itunes.rss?host='.$host->id)}}" target="_blank">{{url('feed/episodes/itunes.rss?host='.$host->id)}}</a>
                                      
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('HostItunesTitle', 'Title', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][itunes_title]', $host->itunes_title, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'225', 'id'=>'HostItunesTitle'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('HostItunesSubtitle', 'Subtitle', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[Host][itunes_subtitle]', $host->itunes_subtitle,  $attributes = array('class'=>'form-control', 'id'=>'HostItunesSubtitle'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('HostItunesDesc', 'Description', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{ Form::textarea('data[Host][itunes_desc]', $host->itunes_desc, ['id' => 'HostItunesDesc']) }}
                                    </div>
                                </div>
                                <div class="form-group row">                                 
                                    {{Form::label('HostHasManyItunesCategories', 'Category / Sub-Category', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <div class="row">
                                            {{Form::hidden('itunes_cat_combo', $itunes_categories, ['id'=>'itunes_cat_combo'] )}}
                                            @if(!empty($host->itunes_categories))
                                            @php ($itunes_cat_count = 1)
                                            @foreach($host->itunes_categories as $itunes_cat)
                                            <div class="outside_itunes_cat" data-off="{{$itunes_cat_count}}">
                                                <div class="col-sm-6">
                                                    {{Form::select('data[ItunesCategory]['.$itunes_cat_count.'][category]', (!empty($itunes_categories) ? json_decode($itunes_categories)[0] : ''), $itunes_cat->category, $attributes=array('id'=>'ItunesCategory'.$itunes_cat_count.'Category', 'class'=>'form-control itunes_main_cat', 'data-style'=>'btn-default'))}}
                                                </div>
                                                <div class="col-sm-6">
                                                    {{Form::select('data[ItunesCategory]['.$itunes_cat_count.'][subcategory]', (!empty($itunes_categories) ? json_decode($itunes_categories)[1]->{$itunes_cat->category} : ''), !empty($itunes_cat->subcategory) ? $itunes_cat->subcategory : '', $attributes=array('id'=>'ItunesCategory'.$itunes_cat_count.'Subcategory', 'class'=>'form-control itunes_sub_cat', 'data-style'=>'btn-default'))}}
                                                </div>
                                            </div>
                                            @php ($itunes_cat_count++)
                                            @endforeach
                                            @else
                                            <div class="outside_itunes_cat row" data-off="1">
                                                <div class="col-sm-6">
                                                    {{Form::select('data[ItunesCategory][1][category]', (!empty($itunes_categories) ? json_decode($itunes_categories)[0] : ''), (count( (array)$host->itunes_categories) > 0) ? 'Arts' : '', $attributes=array('id'=>'ItunesCategory1Category', 'class'=>'form-control itunes_main_cat', 'data-style'=>'btn-default'))}}
                                                </div>
                                                <div class="col-sm-6">                                     
                                                    {{Form::select('data[ItunesCategory][1][subcategory]', (!empty($itunes_categories) ? json_decode($itunes_categories)[1]->Arts : ''), count( (array)$host->itunes_categories) > 0 ? $host->itunes_categories->{1}->subcategory : '', $attributes=array('id'=>'ItunesCategory1Subcategory', 'class'=>'form-control itunes_sub_cat', 'data-style'=>'btn-default'))}}
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('HostItunes0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'itunes')
                                                        <div class="col-sm-4 adding-medias media_Host_itunes" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_itunes" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="itunes"></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_Host_itunes_outer hidden" data-off="0" style="display: none;">
                                            </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.<br>iTunes requires an exact 1400x1400 pixel image.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" id="itunes_media_btn" data-toggle="modal" data-target="" data-mainmodule="Host" data-submodule="itunes" data-limit="1" data-dimension="" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Settings</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('HostArchivedRssFeedUrl', 'Archived Shows RSS feed url', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                       <a href="{{url('feed/episodes/archived.rss?host='.$host->id)}}" target="_blank">{{url('feed/episodes/archived.rss?host='.$host->id)}}</a>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('HostUpcomingRssFeedUrl', 'Upcoming Shows RSS feed url', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <a href="{{url('feed/episodes/upcoming.rss?host='.$host->id)}}" target="_blank">{{url('feed/episodes/upcoming.rss?host='.$host->id)}}</a>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('HostIsOnline', 'Status', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <div class="radio radio-info radio-inline">
                                            {{ Form::radio('data[Host][is_online]', '1', ($host->is_online == '1') ? true : false, array('id'=>'HostIsOnline1'))}}
                                            {{Form::label('HostIsOnline1', 'Active', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{ Form::radio('data[Host][is_online]', '2', ($host->is_online == '2') ? true : false, array('id'=>'HostIsOnline2'))}}
                                            {{Form::label('HostIsOnline2', 'Login Inactive', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{ Form::radio('data[Host][is_online]', '0', ($host->is_online == '0') ? true : false, array('id'=>'HostIsOnline0'))}}
                                            {{Form::label('HostIsOnline0', 'Totally Inactive', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::hidden('data[UserSetting][id]',($host->user->user_settings->count() > 0) ? $host->user->user_settings[0]->id : 0)}} 
                                        <div class="radio radio-info radio-inline">
                                            {{ Form::radio('data[UserSetting][mailing_newsletter]', '1', ($host->user->user_settings->count() > 0) ? (($host->user->user_settings[0]->mailing_newsletter == 1) ? true : false) : false, array('id'=>'UserSettingMailingNewsletter1'))}}
                                            {{Form::label('HostIsOnline1', 'Enabled', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{ Form::radio('data[UserSetting][mailing_newsletter]', '0', ($host->user->user_settings->count() > 0) ? (($host->user->user_settings[0]->mailing_newsletter == 0) ? true : false) : false, array('id'=>'UserSettingMailingNewsletter0'))}}
                                            {{Form::label('UserSettingMailingNewsletter0', 'Disabled', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('HostD', 'Listener feedback email notification', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="radio radio-inline">
                                                    {{ Form::radio('data[Host][is_feedback_email_notification]', '0', ($host->is_feedback_email_notification == 0) ? true : false, array('id'=>'HostIsFeedbackEmailNotification0'))}}
                                                    {{Form::label('HostIsFeedbackEmailNotification0', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                                <div class="radio radio-info radio-inline">
                                                    {{ Form::radio('data[Host][is_feedback_email_notification]', '1', ($host->is_feedback_email_notification == 1) ? true : false, array('id'=>'HostIsFeedbackEmailNotification1'))}}
                                                    {{Form::label('HostIsFeedbackEmailNotification1', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <?php 
                                                    $style = "";
                                                    if($host->is_feedback_email_notification == 1){
                                                        $style="display:block";
                                                    } else{
                                                        $style="display:none";
                                                    }
                                                ?>
                                                {{Form::email('data[Host][feedback_notification_email]', $host->feedback_notification_email, ['id'=>'HostFeedbackNotificationEmail', 'placeholder'=>'email address', 'class'=>'form-control', 'style'=>$style] )}}
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('HostDefaultChannelId', 'Default channel', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        {{Form::select('data[Host][default_channel_id]', (!empty($all_channels) ? $all_channels : ''), $host->default_channel_id, $attributes=array('id'=>'HostDefaultChannelId', 'class'=>'selectpicker', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('ChannelChannelSet', 'Channels', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        @if(!empty($all_channels))
                                            @foreach ( $all_channels as $i =>$item )
                                            <div class="checkbox checkbox-pink">
                                                {!! Form::checkbox( 'data[Channel][Channel][]', $i, (!empty($host_has_channels) ? (in_array($i, $host_has_channels) ? $i : '') : ''), ['class' => 'md-check', 'id' => 'ChannelChannel'.$i] ) !!}
                                                {!! Form::label('ChannelChannel'.$i,  $item) !!}
                                            </div>
                                            @endforeach
                                        @else
                                            There does not exist any channel.
                                        @endif
                                    </div>
                                </div><!-- end row -->
                                {{Form::hidden('data[User][groups_id]', 2, ['id'=>'UserGroupsId'])}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Studio App Settings</h4>
                            <div class="p-20">
                                <div class="form-group row ">
                                    {{Form::label('UserSettingStudioappEnabled', 'Enabled', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-10">
                                        <div class="radio radio-info radio-inline">
                                            {{Form::radio('data[UserSetting][studioapp_enabled]', 1, ($host->user->user_settings->count() > 0) ? (($host->user->user_settings[0]->studioapp_enabled == 1) ? true : false) : false, array('id'=>'UserSettingStudioappEnabled1'))}}
                                            {{Form::label('UserSettingStudioappEnabled1', 'Yes', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{Form::radio('data[UserSetting][studioapp_enabled]', 0, ($host->user->user_settings->count() > 0) ? (($host->user->user_settings[0]->studioapp_enabled == 0) ? true : false) : false, array('id'=>'UserSettingStudioappEnabled0'))}}
                                            {{Form::label('UserSettingStudioappEnabled0', 'No', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div><!-- end row -->       
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonIcecastProtocol', 'Protocol', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9">
                                        <div class="form-group col-sm-4 tag-input-type">
                                            {{Form::select('data[UserSetting][studioapp_settings_json][icecast][protocol]', ['http'=>'http', 'https'=>'https'], ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->icecast->protocol : '') : '', $attributes=array('id'=>'UserSettingStudioappSettingsJsonIcecastProtocol', 'class'=>'form-control'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast protocol
                                            </span>  
                                        </div>   
                                    </div>
                                </div><!-- end row -->   
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonIcecastHost', 'Host name', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::text('data[UserSetting][studioapp_settings_json][icecast][host]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->icecast->host : '') : '',  $attributes = array('class'=>'form-control', 'id'=>'UserSettingStudioappSettingsJsonIcecastHost'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Icecast hostname (ex. google.com) or IP number (ex. 453.345.13.234)
                                        </span> 
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonIcecastPort', 'Port number', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::number('data[UserSetting][studioapp_settings_json][icecast][port]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->icecast->port : 8000) : 8000,  $attributes = array('class'=>'form-control', 'id'=>'UserSettingStudioappSettingsJsonIcecastPort'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                           Icecast port number (ex. 8000)
                                        </span> 
                                        {{Form::hidden('data[UserSetting][studioapp_settings_json][icecast][port_params][min]',0)}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonIcecastMountpoint', 'Mount point', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::text('data[UserSetting][studioapp_settings_json][icecast][mountpoint]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->icecast->mountpoint : '') : '',  $attributes = array('class'=>'form-control', 'id'=>'UserSettingStudioappSettingsJsonIcecastMountpoint'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Icecast mountpoint (ex. stream; without slash)
                                        </span> 
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonIcecastUser', 'User', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::text('data[UserSetting][studioapp_settings_json][icecast][user]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->icecast->user : '') : '',  $attributes = array('class'=>'form-control', 'id'=>'UserSettingStudioappSettingsJsonIcecastUser'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Icecast username
                                        </span> 
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonIcecastPassword', 'Password', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::text('data[UserSetting][studioapp_settings_json][icecast][password]', ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->icecast->password : '') : '',  $attributes = array('class'=>'form-control', 'id'=>'UserSettingStudioappSettingsJsonIcecastPassword'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Icecast password
                                        </span> 
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonStreamEncoderFormat', 'Format', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::select('data[UserSetting][studioapp_settings_json][streamEncoder][format]', ['mp3'=>'mp3', 'aac'=>'aac'], ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->streamEncoder->format : '') : '', $attributes=array('id'=>'UserSettingStudioappSettingsJsonStreamEncoderFormat', 'class'=>'form-control'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Icecast input audio stream format
                                        </span>  
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('UserSettingStudioappSettingsJsonStreamEncoderBitrate', 'Bitrate', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-9 tag-input-type">
                                        {{Form::select('data[UserSetting][studioapp_settings_json][streamEncoder][bitrate]', $bitrate, ($host->user->user_settings->count() > 0) ? (!empty($host->user->user_settings[0]->studioapp_settings_json) ? $host->user->user_settings[0]->studioapp_settings_json->streamEncoder->bitrate : '') : '', $attributes=array('id'=>'UserSettingStudioappSettingsJsonStreamEncoderBitrate', 'class'=>'form-control'))}}
                                        <span class="help-block"> 
                                            <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                            Icecast bitrate
                                        </span>  
                                    </div>
                                </div><!-- end row -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="form-group">
                    <div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[User][btnEdit]" id="HostBtnEdit" data-form-id="HostAdminEditForm">
                            Update
                        </a>                   
                    </div>
                </div>
                <!-- End row -->
            </div><!-- end col-->
            {{ Form::close() }}
        </div>
        <!-- end row -->
         @include('backend.cms.upload_media_popup',['module_id' =>$host->id,'main_module' => "Host"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
