@extends('backend.layouts.main')

@section('content')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Partners</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            Partners
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->     
        <div class="row">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <!-- <div class="card-box"> -->
                
            <div style="float:right;">
                <a href="{{url('/admin/user/partners/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">  Add Partner
                </a>
                <a href="{{url('/admin/user/partners/export')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>
            </div>
            <!-- </div> -->
        </div> 
        <!-- end row -->    
       
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->
                    <table id="partners_datatable" class="table table-striped partners_table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div> <!-- container -->
</div> <!-- content -->

@endsection
