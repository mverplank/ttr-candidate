@extends('backend.layouts.main')
@section('content')
<style>
   #mceu_21-body{display:none;}
   .cropper-container.cropper-bg {
   width: 100%;
   }
</style>
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add Host</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="active">
                     Users
                  </li>
                  <li class="active">
                     <a href="{{url('admin/user/hosts')}}">All Hosts</a>
                  </li>
                  <li class="active">
                     Add Host
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <!-- end row -->
      <div class="row">
        <!-- Info Modal -->
        <div id="info-modal" class="modal-demo">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Title</h4>
            <div class="custom-modal-text">
               <!--  <div class="p-20"> -->
                    <div class="form-group row">                      
                        {{Form::hidden('module', '', array('id'=>'info_module'))}}
                        {{Form::hidden('section', '', array('id'=>'info_section'))}}                        
                        <div class="col-md-12">
                            <textarea class="form-control module_info_desc" rows="5" placeholder="Add info here..."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- <div class="col-md-10"></div>
                        <div class="col-md-2"> -->
                            <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5 save_module_info">Save</button>
                        <!-- </div> -->
                    </div>
                <!-- </div> -->
            </div>
        </div>
        <div id="yes_crop"></div>
        <!-- Form Starts-->  
        {{ Form::open(array('url' => 'admin/user/hosts/add', 'id' => 'HostAdminAddForm')) }}
        <div class="col-xs-12">
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                    <div class="info_title">
                        <div class="col-md-10">
                           <h4 class="header-title m-t-0">Personal Details</h4>
                        </div>
                        <div class="col-md-2"> 
                            <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'Personal Details', this)" title="Section Info">What is this?</a>
                        </div>                       
                    </div>
                    <div class="p-20">
                        <div class="form-group row">
                           {{Form::hidden('form_type', 'add', array('id'=>'form_type'))}}
                           {{Form::label('HostTypeHostTypeSet', 'Type', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <div class="checkbox checkbox-info checkbox-inline">
                                 {{ Form::checkbox('data[HostType][HostType][]', '1' , false, array('id'=>'HostTypeHostType1')) }}
                                 {{Form::label('HostTypeHostType1', 'Host', array('class' => 'col-sm-12 form-control-label'))}}
                              </div>
                              <div class="checkbox checkbox-purple checkbox-inline">
                                 {{ Form::checkbox('data[HostType][HostType][]', '2' , false, array('id'=>'HostTypeHostType2')) }}
                                 {{Form::label('HostTypeHostType2', 'Co-Host', array('class' => 'col-sm-12 form-control-label'))}}
                              </div>
                              <div class="checkbox checkbox-custom checkbox-inline">
                                 {{ Form::checkbox('data[HostType][HostType][]', '3' , false, array('id'=>'HostTypeHostType3')) }}
                                 {{Form::label('HostTypeHostType3', 'Podcast-Host', array('class' => 'col-sm-12 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostD', 'Name', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-10 row">
                              <div class="form-group col-sm-3">
                                 {{Form::select('data[Profile][title]', $prefix_suffix['prefix'], '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                              </div>
                              <div class="col-sm-3">
                                 {{Form::text('data[Profile][firstname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true, 'id'=>'ProfileFirstname'))}}
                              </div>
                              <div class="col-sm-3">
                                 {{Form::text('data[Profile][lastname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true, 'id'=>'ProfileLastname'))}}
                              </div>
                              <div class="form-group col-sm-3">
                                 {{Form::select('data[Profile][sufix]', $prefix_suffix['suffix'], '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                              </div>
                           </div>
                        </div>
                        <!-- Editor for Bio-->
                        <div class="form-group row">
                           {{Form::label('HostBio', 'Bio', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <div class="card-box">
                                 <form method="post">
                                    <!--  <textarea id="elm1" name="data[Profile][bio]"></textarea> -->
                                    {{ Form::textarea('data[Host][bio]', null, ['id' => 'HostBio']) }}
                                 </form>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <!-- End editor -->
                        <div class="form-group row">
                           {{Form::label('HostPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0)
                                 @php ($url_count = 1)
                                 @foreach($media as $med)
                                 @if($med->sub_module == 'photo')
                                 <div class="col-sm-4 adding-medias media_Host_photo" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="javascript:void(0)" onclick="editMediaImage(this, 'Host', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a onclick="Edit_Media({{$med->media_id}});">
                                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                                            </a>
                                                         </li>
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_photo" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="photo"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++)
                                 @endif
                                 @endforeach
                                 @endif
                                 <div class="media_Host_photo_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                              Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="profile_img_media_btn" data-target="" data-mainmodule="Host" data-submodule="photo" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="1">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('HostHasManyUrls', 'Urls and Social Media', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <div class="mtf-dg">
                                 <table class="mtf-dg-table url_table default">
                                    <thead>
                                       <tr>
                                          <th>
                                             <div>Type</div>
                                          </th>
                                          <th>
                                             <div>Url</div>
                                          </th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                          <td>
                                             {{Form::select('data[Url][1][type]', $social_media, '', $attributes=array('id'=>'Url1Type', 'class'=>'form-control SelectSocialMediaType'))}}
                                          </td>
                                          <td>
                                             {{Form::url('data[Url][1][url]', 'http://www.example.com/',  array('step' => '1', 'min' => '1', 'id' => 'Url1Url','class'=>'form-control SelectSocialMediaURL', 'placeholder'=>'http://www.example.com/' ))}}
                                          </td>
                                          <td class="actions">
                                             <div class="col-sm-6 col-md-4 col-lg-3 host_extra_urls" style="display: block;">
                                                <i class="typcn typcn-delete"></i> 
                                             </div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <div class="mtf-buttons" style="clear:both;float:right;">
                                    <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                    <span>add</span>
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('HostHasManyVideos', 'Video Clips', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <div class="mtf-dg">
                                 <table class="mtf-dg-table video_table default">
                                    <thead>
                                       <tr>
                                          <th>
                                             <div>Title</div>
                                          </th>
                                          <th>
                                             <div>Video HTML code</div>
                                          </th>
                                          <th>
                                             <div>Enabled</div>
                                          </th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr data-id="" data-off="1" style="display: table-row;">
                                          <td>
                                             {{Form::text('data[Video][1][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'Video1Title'))}}
                                          </td>
                                          <td>
                                             {{Form::textarea('data[Video][1][code]', '',  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video1Code' ))}}
                                          </td>
                                          <td>
                                             <div class="checkbox checkbox-pink checkbox-inline">
                                                {{ Form::checkbox('data[Video][1][is_enabled]', '1' , true, array('id'=>'Video1IsEnabled1')) }}
                                                {{Form::label('Video1IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                             </div>
                                          </td>
                                          <td class="actions">
                                             <div class="col-sm-6 col-md-4 col-lg-3 host_videos" style="display: block;">
                                                <i class="typcn typcn-delete"></i> 
                                             </div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <div class="mtf-buttons" style="clear:both;float:right;">
                                    <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyVideosAdd"><i class="glyphicon glyphicon-plus"></i>
                                    <span>add</span>
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <?php $defulttags = "transformation talk radio, positive talk radio, positive podcasts, online radio";  ?>
                        <div class="form-group row">
                           {{Form::label('HostTags', 'Tags', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9 tag-input-type">
                              <div class="form-group">
                                 {{Form::text('data[Host][tags]',$defulttags, ['id'=>'HostTags_tagsinput', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                 <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                              </div>
                           </div>
                        </div><!-- End row -->                               
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                    <div class="info_title">
                        <div class="col-md-10">
                           <h4 class="header-title m-t-0">Account & Contact</h4>
                        </div>
                        <div class="col-md-2"> 
                            <!--  <span id="tooltip-events" class="btn btn-success" title="Press any key on your keyboard or click anywhere in the page to close this">What is this?</span> -->
                            <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'Account & Contact', this)" title="Section Info">What is this?</a>
                        </div>                       
                    </div>
                    <div class="p-20">
                        <div class="form-group row">
                           {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[User][username]', $value = null, $attributes = array('class'=>'form-control required user-name', 'data-unique'=> '1', 'data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=> 0, 'data-parsley-maxlength'=>'45', 'id'=>'UserUsername', 'required'=>true,'maxlength' =>'45'))}}
                              <span id="rchars">45</span> Character(s) Remaining
                              <div class="error-block" style="display:none;"></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-9">
                              {{Form::email('data[User][email]', $value = null, $attributes = array('class'=>'form-control required', 'data-unique'=> '1','data-validation' => '1',  'data-form'=> 'userprofile', 'data-userid'=> 0,'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true))}}
                              <span class="error-block" style="display:none;"></span>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('UserAlternateEmail', 'Alternate Email', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::email('data[Host][alternate_email]', $value = null, $attributes = array('class'=>'form-control','id'=>'UserAlternateEmail',))}}
                              <span class="error-block" style="display:none;"></span>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('UserPassword', 'Password', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-9">
                              {{Form::password('data[User][password]', $attributes = array('class'=>'form-control required password-field', 'id'=>'UserPassword', 'required'=>true))}}
                              <!-- password strength metter  -->
                              <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                           </div>
                           <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ProfilePhone', 'Landline Phone', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Profile][phone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfilePhone'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ProfileCellphone', 'Cell Phone', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Profile][cellphone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileCellphone'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Profile][skype]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileSkype'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ProfileSkypePhone', 'Skype phone', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Profile][skype_phone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'ProfileSkypePhone'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ZoomLink', 'Zoom Link', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9 tag-input-type">
                              <div class="form-group">
                                 {{Form::url('data[Host][zoom_link]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'ZoomLink'))}}
                                 <span class="error-block" style="display:none;"></span>
                              </div>
                           </div>
                        </div><!-- End row -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                    <div class="info_title">
                        <div class="col-md-10">
                           <h4 class="header-title m-t-0">Other Contact</h4>
                        </div>
                        <div class="col-md-2"> 
                            <!--  <span id="tooltip-events" class="btn btn-success" title="Press any key on your keyboard or click anywhere in the page to close this">What is this?</span> -->
                            <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'Other Contact', this)" title="Section Info">What is this?</a>
                        </div>                       
                    </div>
                    <div class="p-20">
                        <div class="form-group row">
                           {{Form::label('HostPrType', 'Type', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <div class="radio radio-info radio-inline">
                                 {{Form::radio('data[Host][pr_type]', 'pr', true, array('id'=>'HostPrTypePr'))}}
                                 {{Form::label('HostPrTypePr', 'Pr', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio-warning radio-inline">
                                 {{Form::radio('data[Host][pr_type]', 'publisher', false, array('id'=>'HostPrTypePublisher'))}}
                                 {{Form::label('HostPrTypePublisher', 'Publisher', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio-purple radio-inline">
                                 {{Form::radio('data[Host][pr_type]', 'assistant', false, array('id'=>'HostPrTypeAssistant'))}}
                                 {{Form::label('HostPrTypeAssistant', 'Assistant', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio-custom radio-inline">
                                 {{Form::radio('data[Host][pr_type]', 'other', false, array('id'=>'HostPrTypeOther'))}}
                                 {{Form::label('HostPrTypeOther', 'Other', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrName', 'Name', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][pr_name]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrName'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrCompanyName', 'Company Name', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][pr_company_name]', '',$attributes = array('class'=>'form-control', 'id'=>'HostPrCompanyName'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrEmail', 'Email', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::email('data[Host][pr_email]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrEmail'))}}
                              <div class="error-block" style="display:none;"></div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrPhone', 'Landline Phone', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][pr_phone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrPhone'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrCellphone', 'Cell phone', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][pr_cellphone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrCellphone'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrSkype', 'Skype', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][pr_skype]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'HostPrSkype'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostPrSkypePhone', 'Skype phone', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][pr_skype_phone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'HostPrSkypePhone'))}}
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                    <div class="info_title">
                        <div class="col-md-10">
                           <h4 class="header-title m-t-0">iTunes Profile Setup</h4>
                        </div>
                        <div class="col-md-2"> 
                            <!--  <span id="tooltip-events" class="btn btn-success" title="Press any key on your keyboard or click anywhere in the page to close this">What is this?</span> -->
                            <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'iTunes Profile Setup', this)" title="Section Info">What is this?</a>
                        </div>                       
                    </div>
                    <div class="p-20">
                        <div class="form-group row">
                           {{Form::label('HostItunesTitle', 'Title', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][itunes_title]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'225', 'id'=>'HostItunesTitle'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostItunesSubtitle', 'Subtitle', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::text('data[Host][itunes_subtitle]', $value=null,  $attributes = array('class'=>'form-control', 'id'=>'HostItunesSubtitle'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostItunesDesc', 'Description', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              {{ Form::textarea('data[Host][itunes_desc]', null, ['id' => 'HostItunesDesc']) }}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostHasManyItunesCategories', 'Category / Sub-Category', array('class' => 'col-sm-2 form-control-label'))}}
                           <?php //echo "<pre>";print_R(json_decode($itunes_categories));?>
                           <div class="col-sm-9">
                              {{Form::hidden('itunes_cat_combo', $itunes_categories, ['id'=>'itunes_cat_combo'] )}}
                              <div class="outside_itunes_cat" data-off="1">
                                 <div class="col-sm-5">
                                    {{Form::select('data[ItunesCategory][1][category]', (!empty($itunes_categories) ? json_decode($itunes_categories)[0] : ''), '', $attributes=array('id'=>'ItunesCategory1Category', 'class'=>'form-control itunes_main_cat', 'data-style'=>'btn-default'))}}
                                 </div>
                                 <div class="col-sm-5">
                                    {{Form::select('data[ItunesCategory][1][subcategory]', (!empty($itunes_categories) ? json_decode($itunes_categories)[1]->Arts : ''), '', $attributes=array('id'=>'ItunesCategory1Subcategory', 'class'=>'form-control itunes_sub_cat', 'data-style'=>'btn-default'))}}
                                 </div>
                              </div>                              
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('HostItunes0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0)
                                 @php ($url_count = 1)
                                 @foreach($media as $med)
                                 @if($med->sub_module == 'itunes')
                                 <div class="col-sm-4 adding-medias media_Host_itunes" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_itunes" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="itunes"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++)
                                 @endif
                                 @endforeach
                                 @endif
                                 <div class="media_Host_itunes_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                              Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.<br>
                              iTunes requires an exact 1400x1400 pixel image.
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="itunes_media_btn" data-target="" data-mainmodule="Host" data-submodule="itunes" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                    <div class="info_title">
                        <div class="col-md-10">
                           <h4 class="header-title m-t-0">Settings</h4>
                        </div>
                        <div class="col-md-2"> 
                            <!--  <span id="tooltip-events" class="btn btn-success" title="Press any key on your keyboard or click anywhere in the page to close this">What is this?</span> -->
                            <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'Settings', this)" title="Section Info">What is this?</a>
                        </div>                       
                    </div>
                    <div class="p-20">
                        <div class="form-group row">
                           {{Form::label('HostIsOnline', 'Status', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10 row">
                              <div class="radio radio-info radio-inline col-sm-3">
                                 {{ Form::radio('data[Host][is_online]', '1', true, array('id'=>'HostIsOnline1'))}}
                                 {{Form::label('HostIsOnline1', 'Active', array('class' => 'form-control-label'))}}
                              </div>
                              <div class="radio radio-inline col-sm-3">
                                 {{ Form::radio('data[Host][is_online]', '2', false, array('id'=>'HostIsOnline2'))}}
                                 {{Form::label('HostIsOnline2', 'Login Inactive', array('class' => 'form-control-label'))}}
                              </div>
                              <div class="radio radio-inline col-sm-3">
                                 {{ Form::radio('data[Host][is_online]', '0', false, array('id'=>'HostIsOnline0'))}}
                                 {{Form::label('HostIsOnline0', 'Totally Inactive', array('class' => 'form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group row">
                           {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <div class="radio radio-info radio-inline">
                                 {{ Form::radio('data[UserSetting][mailing_newsletter]', '1', true, array('id'=>'UserSettingMailingNewsletter1'))}}
                                 {{Form::label('HostIsOnline1', 'Enabled', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio-inline">
                                 {{ Form::radio('data[UserSetting][mailing_newsletter]', '0', false, array('id'=>'UserSettingMailingNewsletter0'))}}
                                 {{Form::label('UserSettingMailingNewsletter0', 'Disabled', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group row">
                           {{Form::label('HostD', 'Listener feedback email notification', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              <div class="row">
                                 <div class="col-sm-3">
                                    <div class="radio radio-inline">
                                       {{ Form::radio('data[Host][is_feedback_email_notification]', '0', true, array('id'=>'HostIsFeedbackEmailNotification0'))}}
                                       {{Form::label('HostIsFeedbackEmailNotification0', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                       {{ Form::radio('data[Host][is_feedback_email_notification]', '1', false, array('id'=>'HostIsFeedbackEmailNotification1'))}}
                                       {{Form::label('HostIsFeedbackEmailNotification1', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                    </div>
                                 </div>
                                 <div class="col-sm-9">
                                    {{Form::email('data[Host][feedback_notification_email]','', ['id'=>'HostFeedbackNotificationEmail', 'placeholder'=>'email address', 'class'=>'form-control', 'style'=>'display:none;'] )}}
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group row">
                           {{Form::label('HostDefaultChannelId', 'Default channel', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              {{Form::select('data[Host][default_channel_id]', (!empty($all_channels) ? $all_channels : ''), '', $attributes=array('id'=>'HostDefaultChannelId', 'class'=>'selectpicker', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                           </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group row">
                           {{Form::label('ChannelChannelSet', 'Channels', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-9">
                              @if(!empty($all_channels))
                              @foreach ( $all_channels as $i =>$item )
                              <div class="checkbox checkbox-pink">
                                 {!! Form::checkbox( 'data[Channel][Channel][]', $i, '', ['class' => 'md-check', 'id' => 'ChannelChannel'.$i] ) !!}
                                 {!! Form::label('ChannelChannel'.$i,  $item) !!}
                              </div>
                              @endforeach
                              @else
                              No Bonus Items assigned yet.
                              @endif
                           </div>
                        </div>
                        <!-- end row -->
                        {{Form::hidden('data[User][groups_id]', 2, ['id'=>'UserGroupsId'])}}
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="form-group">
               <div class="fixed_btn">
                  <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[User][btnAdd]" id="HostBtnAdd" data-form-id="HostAdminAddForm">
                  Add
                  </a>
                  <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
               </div>
            </div>
         </div>
         <!-- end col-->
         {{ Form::close() }}
         <!-- Form Close -->
      </div>
      <!-- end row -->
      @include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "Host"])
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection