@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Partner</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/partners')}}">All Partners</a>
                        </li>
                        <li class="active">
                            Add Partner
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div id="yes_crop"></div>
        
        <div class="row">
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/user/partners/add', 'id' => 'PartnerAdminAddForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Personal Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    <input type="hidden" name="form_type" id="form_type" value="add">
                                    {{Form::label('name', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][title]', array('' => 'Choose prefix..', 'Dr.' => 'Dr.', 'Prof' => 'Prof', 'Pir' => 'Pir', 'Colonel' => 'Colonel'), '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}

                                            {{ Form::hidden('data[User][groups_id_radio]', '4') }}

                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][firstname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][lastname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][sufix]', array('' => 'Choose suffix..', 'Np' => 'Np', 'Jd' => 'Jd', 'M.s.' => 'M.s.'), '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                        </div>
                                       
                                    </div>
                                </div>
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('ProfileBio', 'Bio', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                               <!--  <textarea id="elm1" name="data[Profile][bio]"></textarea> -->
                                                {{ Form::textarea('data[Profile][bio]', null, ['id' => 'ProfileBio']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- End editor -->
                                <div class="form-group row">
                                    {{Form::label('PartnerPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'photo')
                                                        <div class="col-sm-4 adding-medias media_Partner_photo" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="javascript:void(0)" onclick="editMediaImage(this, 'Partner', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-right">
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_InventoryItem_image" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Partner" data-submodule="photo"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_Partner_photo_outer hidden" data-off="0" style="display: none;"></div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Partner" data-submodule="photo" data-limit="1" data-dimension="" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Account & Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[User][username]', $value = null, $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1','data-parsley-maxlength'=>'45', 'id'=>'UserUsername', 'required'=>true))}}
                                        <span id="rchars">45</span> Character(s) Remaining
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                    
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::email('data[User][email]', $value = null, $attributes = array('class'=>'form-control required', 'data-unique'=> '1','data-validation' => '1', 'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserPassword', 'Password', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::password('data[User][password]', $attributes = array('class'=>'form-control required password-field', 'data-validation-url' => 'https://178.128.179.99/forms/validate_input/User.email/uniqueEmail/User', 'id'=>'UserPassword', 'required'=>true))}}
                                        <!-- password strength metter  -->
                                        <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                    </div>
                                    <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfilePhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][phone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfilePhone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileCellphone', 'Cell Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][cellphone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileSkype'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('ProfileSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype_phone]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'ProfileSkypePhone'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Settings</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio4" value="1" name="data[UserSetting][mailing_newsletter_radio]">
                                            <label for="inlineRadio4"> Enabled </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio5" value="0" name="data[UserSetting][mailing_newsletter_radio]" checked>
                                            <label for="inlineRadio5"> Disabled </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[User][btnAdd]" id="UserBtnAdd" data-form-id="PartnerAdminAddForm">
                            Add
                        </a>
                       <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                     
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
      @include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "Partner"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
