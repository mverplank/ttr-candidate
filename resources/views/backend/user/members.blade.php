@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Members</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            Members
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
           <!--  <div class="card-box"> -->
                <div style="float:right;">
                    <a href="{{url('/admin/user/members/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">     Add Member
                    </a>
                    <a href="{{url('/admin/user/members/export')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>

                </div>
           <!--  </div> -->
        </div>     
        <!-- end row -->   
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="members_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Membership</th>
                                <th>Membership Ends/Ended</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
