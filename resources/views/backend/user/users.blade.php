@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">All Users</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            All Users
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="all" value="0" name="filterUsers" checked="" onclick="filterUsers(0)" {{ ($group_type == 0) ? "checked" : "" }}>
                    <label for="all"> All </label>
                </div>
                <div class="radio radio-primary radio-inline">
                    <input type="radio" id="admin" value="1" name="filterUsers" onclick="filterUsers(1)" {{ ($group_type == 1) ? "checked" : "" }}>
                    <label for="admin"> Administrators </label>
                </div> 
                <div class="radio radio-warning radio-inline"> 
                    <input type="radio" id="host" value="2" name="filterUsers" onclick="filterUsers(2)" {{ ($group_type == 2) ? "checked" : "" }}>
                    <label for="host"> Hosts </label>
                </div>
                <div class="radio radio-pink radio-inline"> 
                    <input type="radio" id="cohost" value="5" name="filterUsers" onclick="filterUsers(5)" {{ ($group_type == 5) ? "checked" : "" }}>
                    <label for="cohost"> Co-Hosts </label>
                </div>
                <div class="radio radio-custom radio-inline"> 
                    <input type="radio" id="podcast_host" value="6" name="filterUsers" onclick="filterUsers(6)" {{ ($group_type == 6) ? "checked" : "" }}>
                    <label for="podcast_host"> Podcast-Hosts </label>
                </div>
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="partner" value="4" name="filterUsers" onclick="filterUsers(4)" {{ ($group_type == 4) ? "checked" : "" }}>
                    <label for="partner"> Partners </label>
                </div>
                <div class="radio radio-danger radio-inline"> 
                    <input type="radio" id="member" value="3" name="filterUsers" onclick="filterUsers(3)" {{ ($group_type == 3) ? "checked" : "" }}>
                    <label for="member"> Members </label>
                </div>
                
                <div style="float:right;">
                    <a href="{{url('/admin/user/users/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">     Add User
                    </a>
                    <a href="{{url('/admin/user/users/export')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>
                </div>
            </div>
        </div>     
        <!-- end row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="users_datatable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Last Login</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div> <!-- container -->
</div> <!-- content -->

@endsection
