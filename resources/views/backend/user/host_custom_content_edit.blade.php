@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">
                        Custom Content
                    </h4>
                    <ol class="breadcrumb p-0 m-0">                       
                        <li>
                            <a href="{{url('host')}}">Dashboard</a>
                        </li>
                        <li class="active">
                          Custom Content
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div id="yes_crop"></div>
        <div class="row">
            <!-- Info Modal -->
            <div id="info-modal" class="modal-demo">
                <button type="button" class="close" onclick="Custombox.close();">
                    <span>&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="custom-modal-title">Title</h4>
                <div class="custom-modal-text">
                    <div class="form-group row">                      
                        {{Form::hidden('module', '', array('id'=>'info_module'))}}
                        {{Form::hidden('section', '', array('id'=>'info_section'))}}                        
                        <div class="col-md-12">
                            <textarea class="form-control module_info_desc" rows="5" placeholder="Add info here..."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5 save_module_info">Save</button>
                    </div>
                </div>
            </div>
            <!-- Form Starts-->
            {{ Form::open(array('url' => 'admin/user/hosts/edit/custom_content/'.$user->id, 'method' => 'PUT', 'id'=>'HostHostPageForm')) }}
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">           
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-12">              
                                    <div class="info_title">
                                        <div class="col-md-10">
                                           <h4 class="header-title m-t-0">Testimonials</h4>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'Testimonials', this)" title="Section Info">What is this?</a>
                                        </div>                       
                                    </div>
                                    <div class="p-20">
                                        <div calss= "row">
                                            <div class="col-md-5">
                                                Testimonial
                                            </div>
                                             <div class="col-md-5">
                                                Author
                                            </div>
                                            <div class="col-md-1">
                                                Enabled
                                            </div>
                                             <div class="col-md-1">
                                                 &nbsp;
                                            </div>              
                                        </div>
                                        {{Form::hidden('edit_id', $user->id, array('id'=>'edit_id'))}}
                                            @if($user->count() > 0)
                                                @if($user->host_testimonials->count() > 0)
                                                @php ($testimonial_count = 1)
                                                @foreach($user->host_testimonials as $testimonials) 
                                            <div class="form-group row host_testimonials" data-off="{{$testimonial_count}}">
                                             {{Form::hidden('data[HostTestimonial]['.$testimonial_count.'][id]', $testimonials['id'], array('id'=>'host_content_id'.$testimonial_count))}}                                          
                                            <div class="col-md-5">
                                              {{Form::textarea('data[HostTestimonial]['.$testimonial_count.'][testimonial]',$testimonials["testimonial"],  array('class'=>'form-control', 'rows'=>5, 'cols'=>30, 'id'=>'HostTestimonial'.$testimonial_count.'Testimonial' ))}}
                                            </div>
                                             <div class="col-md-5">
                                                {{Form::text('data[HostTestimonial]['.$testimonial_count.'][author]',$testimonials["author"], $attributes = array('class'=>'form-control', 'id'=>'HostTestimonial'.$testimonial_count.'Author','maxlength'=>'255'))}}
                                            </div>
                                            <div class="col-md-1">
                                                {{ Form::checkbox('data[HostTestimonial]['.$testimonial_count.'][is_enabled]', '1' ,($testimonials["is_enabled"] == "1") ? 'checked' : '', array('id'=>'HostTestimonial'.$testimonial_count.'IsEnabled1')) }}
                                            </div>
                                             <div class="col-md-1 delete_Testimonial" style="display: block;"><i class="typcn typcn-delete"></i> 
                                                </div> 
                                        </div>
                                        @php ($testimonial_count++)
                                        @endforeach
                                        @else
                                        <div class="form-group row host_testimonials" data-off="1">
                                             <div class="col-md-5">
                                              {{Form::textarea('data[HostTestimonial][0][testimonial]', '',  array('class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'HostTestimonial0Testimonial' ))}}
                                            </div>
                                             <div class="col-md-5">
                                                {{Form::text('data[HostTestimonial][0][author]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'HostTestimonial0Author','maxlength'=>'255'))}}
                                            </div>
                                            <div class="col-md-1">
                                                {{ Form::checkbox('data[HostTestimonial][0][is_enabled]', '1' , false, array('id'=>'HostTestimonial0IsEnabled1')) }}
                                            </div> 
                                            <div class="col-md-1 delete_Testimonial" style="display: block;"><i class="typcn typcn-delete"></i> 
                                            </div>                                       
                                           @endif
                                         @endif 
                                         <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyHostTestimonialsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                <span>add</span>
                                            </button>
                                        </div>                                         
                                        </div>
     
                                    </div>
                                </div>
                            </div><!-- end row -->
                        </div> <!-- end card-box -->
                    </div><!-- end col-->
                </div><!-- end row -->
                <!-- end row -->
                <div class="row">
                 <div class="col-xs-12">
                    <div class="card-box">
                       <div class="row">
                            <div class="info_title">
                                <div class="col-md-10">
                                   <h4 class="header-title m-t-0">Custom Content</h4>
                                </div>
                                <div class="col-md-2">
                                    <a href="#info-modal" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" onclick="infoModule('Host', 'Custom Content', this)" title="Section Info">What is this?</a>
                                </div>                       
                            </div>
                            
                            @if($user->count() > 0)
                                @if($user->host_contents->count() > 0)
                                @php ($contents_count = 1)
                                @foreach($user->host_contents as $contents)
                                <div class="col-md-6 card-box host_contents" data-off="{{$contents_count}}">
                                    <div class="form-group row">
                                        <div class="col-md-3 {{($contents_count ==1) ? 'custom_content' : 'delete_Contents'}}" style="display: block;"><i class="typcn typcn-delete"></i> 
                                        </div>
                                        <div class="p-20">
                                            {{Form::hidden('data[HostContent]['.$contents_count.'][id]', $contents['id'], array('id'=>'host_content_id'.$contents_count))}}    
                                            {{Form::hidden('data[HostContent]['.$contents_count.'][offset]', 'image_'.$contents_count)}}    
                                            <div class="col-md-12">
                                                {{Form::label('HostContent'.$contents_count.'Title', 'Title', array('class' => 'col-sm-4 form-control-label'))}} 
                                                {{Form::text('data[HostContent]['.$contents_count.'][title]',$contents['title'],  array('class'=>'form-control','maxlength'=>'255', 'id'=>'HostContent'.$contents_count.'Title' ))}}
                                           </div>
                                           <div class="col-md-12">
                                                {{Form::label('HostContent'.$contents_count.'Url', 'Url', array('class' => 'col-sm-4 form-control-label'))}} 
                                                {{Form::url('data[HostContent]['.$contents_count.'][url]',$contents['url'],  array('class'=>'form-control','id'=>'HostContent'.$contents_count.'Url' ))}}
                                           </div>
                                           <div class="col-md-12">
                                                {{Form::label('HostContent'.$contents_count.'Description', 'Description', array('class' => 'col-sm-4 form-control-label'))}} 
                                                {{Form::textarea('data[HostContent]['.$contents_count.'][description]',$contents['description'],  array('class'=>'form-control','maxlength'=>'180','row'=>'4','col'=>'30' , 'id'=>'HostContent'.$contents_count.'Description' ))}}
                                           </div>
                                           <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <!-- {{Form::hidden('edit_id', $user->count() > 0 ? $user->id : 0, array('id'=>'edit_id'))}} -->
                                                        <div class="row">
                                                           @if($host_content_media->count() > 0)
                                                           @php ($url_count = 1)
                                                           @foreach($host_content_media as $med)
                                                           @if($med->sub_module == 'image_'.$contents_count && $med->module_id == $contents['id'])
                                                           <div class="col-sm-4 adding-medias media_HostContent_image_{{$contents_count}}" data-off="{{$url_count}}">
                                                           <div class="jFiler-items jFiler-row">
                                                              <ul class="jFiler-items-list jFiler-items-grid">
                                                                 <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                       <div class="jFiler-item-inner">                           
                                                                          <div class="jFiler-item-thumb">    
                                                                             <a href="javascript:void(0)" onclick="editMediaImage(this, 'HostContent', 'image_'{{$contents_count}}, 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                <div class="jFiler-item-thumb-image">
                                                                                   <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                </div> 
                                                                             </a>  
                                                                          </div>                                    
                                                                          <div class="jFiler-item-assets jFiler-row">
                                                                          <ul class="list-inline pull-right">
                                                                             <li>
                                                                                <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_HostContent_image_{{$contents_count}}" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="photo"></a>
                                                                             </li>                           
                                                                          </ul>                                    
                                                                          </div>                                
                                                                       </div>                            
                                                                    </div>                        
                                                                 </li>
                                                              </ul>
                                                           </div>
                                                           </div>
                                                           @php ($url_count++)
                                                           @endif
                                                           @endforeach
                                                           @endif
                                                           <div class="media_HostContent_image_{{$contents_count}}_outer hidden" data-off="0" style="display: none;">
                                                           </div>
                                                        </div>
                                                    <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                    Only jpg/jpeg/png/gif files.</span><span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span><span>Clik on image to edit.</span>
                                                 </div>
                                                 <div class="mtf-buttons" style="clear:both;float:left;">
                                                    <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="HostContent" data-submodule="image_{{$contents_count}}" data-media_type="image" data-limit="1" data-dimension=""data-module_id="{{$contents['id']}}" data-file_crop="1">Upload files</button>
                                                 </div>
                                              </div>
                                           </div>
                                           <div class="col-sm-12">
                                              <div class="radio radio-info radio-inline">
                                                 {{Form::radio('data[HostContent]['.$contents_count.'][is_enabled]', '1',($contents["is_enabled"] == "1") ? true : false, array('id'=>'HostContent'.$contents_count.'IsEnabled1'))}}
                                                 {{Form::label('HostContent'.$contents_count.'IsEnabled1', 'Enabled', array('class' => 'col-sm-4 form-control-label'))}}
                                              </div>
                                              <div class="radio radio-warning radio-inline">
                                                 {{Form::radio('data[HostContent]['.$contents_count.'][is_enabled]', '0',($contents["is_enabled"] == "0") ? true : false, array('id'=>'HostContent'.$contents_count.'IsEnabled0'))}}
                                                 {{Form::label('HostContent'.$contents_count.'IsEnabled0', 'Disabled', array('class' => 'col-sm-4 form-control-label'))}}
                                              </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                @php ($contents_count++)
                                @endforeach
                            @else
                            <div class="col-md-6 card-box host_contents" data-off="1">
                                <div class="form-group row">
                                    <div class="p-20">
                                   {{Form::hidden('data[HostContent][1][offset]', 'image_1')}}    
                                   <div class="col-md-12">
                                      {{Form::label('HostContent0Title', 'Title', array('class' => 'col-sm-4 form-control-label'))}} 
                                      {{Form::text('data[HostContent][0][title]', '',  array('class'=>'form-control','maxlength'=>'255', 'id'=>'HostContent0Title' ))}}
                                   </div>
                                   <div class="col-md-12">
                                      {{Form::label('HostContent0Url', 'Url', array('class' => 'col-sm-4 form-control-label'))}} 
                                      {{Form::url('data[HostContent][0][url]', '',  array('class'=>'form-control','id'=>'HostContent0Url' ))}}
                                   </div>
                                   <div class="col-md-12">
                                      {{Form::label('HostContent0Description', 'Description', array('class' => 'col-sm-4 form-control-label'))}} 
                                      {{Form::textarea('data[HostContent][0][description]', '',  array('class'=>'form-control','maxlength'=>'180','row'=>'4','col'=>'30' , 'id'=>'HostContent0Description' ))}}
                                   </div>
                                   <div class="col-md-12">
                                      <div class="form-group row">
                                         <div class="col-sm-12">
                                            <!-- {{Form::hidden('edit_id', $user->count() > 0 ? $user->id : 0, array('id'=>'edit_id'))}} -->
                                             <div class="row">
                                               @if($host_content_media->count() > 0)
                                               @php ($url_count = 1)
                                               @foreach($host_content_media as $med)
                                               @if($med->sub_module == 'image_1' && $med->module_id == '1')
                                               <div class="col-sm-4 adding-medias media_HostContent_image_1" data-off="1">
                                                  <div class="jFiler-items jFiler-row">
                                                     <ul class="jFiler-items-list jFiler-items-grid">
                                                        <li class="jFiler-item" data-jfiler-index="1">          
                                                           <div class="jFiler-item-container">                       
                                                              <div class="jFiler-item-inner">                           
                                                                 <div class="jFiler-item-thumb">    
                                                                    <a href="javascript:void(0)" onclick="editMediaImage(this, 'HostContent', 'image_1', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                       <div class="jFiler-item-thumb-image">
                                                                          <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                       </div> 
                                                                    </a>  
                                                                 </div>                                    
                                                                 <div class="jFiler-item-assets jFiler-row">
                                                                 <ul class="list-inline pull-right">
                                                                    <li>
                                                                       <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_HostContent_image_1" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="HostContent" data-submodule="image_1"></a>
                                                                    </li>                           
                                                                 </ul>                                    
                                                                 </div>                                
                                                              </div>                            
                                                           </div>                        
                                                        </li>
                                                     </ul>
                                                  </div>
                                               </div>
                                               @php ($url_count++)
                                               @endif
                                               @endforeach
                                               @endif
                                               <div class="media_HostContent_image_1_outer hidden" data-off="0" style="display: none;">
                                               </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            Only jpg/jpeg/png/gif files.</span><span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span><span>Clik on image to edit.</span>
                                         </div>
                                         <div class="mtf-buttons" style="clear:both;float:left;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="HostContent" data-submodule="image_1" data-media_type="image" data-limit="1" data-dimension="" data-module_id="0">Upload files</button>
                                         </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-sm-12">
                                      <div class="radio radio-info radio-inline">
                                         {{Form::radio('data[HostContent][0][is_enabled]', '1',true, array('id'=>'HostContent0IsEnabled1'))}}
                                         {{Form::label('HostContent0IsEnabled1', 'Enabled', array('class' => 'col-sm-4 form-control-label'))}}
                                      </div>
                                      <div class="radio radio-warning radio-inline">
                                         {{Form::radio('data[HostContent][0][is_enabled]', '0',false, array('id'=>'HostContent0IsEnabled0'))}}
                                         {{Form::label('HostContent0IsEnabled0', 'Disabled', array('class' => 'col-sm-4 form-control-label'))}}
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          @endif
                          @endif
                       </div>
                       <!-- end row -->    
                    </div>
                    <!-- end card-box -->
                 </div>
                 <!-- end col-->
                 <div class="mtf-buttons" style="clear:both;float:right;">
                    <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyHostContentsAdd"><i class="glyphicon glyphicon-plus"></i>
                    <span>add</span>
                    </button>
                 </div>
                <div class="form-group row">
                    <div class="col-sm-8 col-sm-offset-4">
                        <button  type="submit" id="HostBtnSave" name="data[Host][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form"  data-form-id="UserAdminEditForm">
                            Save
                        </button>
                       <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                    </div>
                </div><!-- end row -->
            </div>
            {{ Form::close() }}
        </div><!-- end row -->
         @include('backend.cms.upload_media_popup',['module_id' =>'','main_module' =>''])
    </div> <!-- container -->
</div> <!-- content -->
@endsection
