@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add User</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/users')}}">All Users</a>
                        </li>
                        <li class="active">
                            Add User
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">

            <div class="col-xs-12">
                {{ Form::open(array('url' => 'admin/user/users/add', 'id'=>'UserAdminAddForm')) }}
                <div class="card-box">
                    <!-- Form Starts-->
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Personal Details</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('name', 'Name', array('class' => 'col-sm-2 form-control-label req'))}}
                                        <div class="col-sm-9">
                                            <div class="form-group col-sm-3">
                                                {{Form::select('data[Profile][title]', array('' => 'Choose prefix..', 'Dr.' => 'Dr.', 'Prof' => 'Prof', 'Pir' => 'Pir', 'Colonel' => 'Colonel'), '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::text('data[Profile][firstname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true,'maxlength' =>'45'))}}
                                               
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::text('data[Profile][lastname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true,'maxlength' =>'45'))}}

                                            </div>

                                            <div class="form-group col-sm-3">
                                                {{Form::select('data[Profile][sufix]', array('' => 'Choose suffix..', 'Np' => 'Np', 'Jd' => 'Jd', 'M.s.' => 'M.s.'), '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-9">
                                            {{Form::text('data[Profile][skype]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileSkype','maxlength' =>'45'))}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ProfileCellphone', 'Cell Phone', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-9">
                                            {{Form::text('data[Profile][cellphone]', $value = null, $attributes = array('class'=>'form-control ProfileCellphone', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileCellphone','maxlength' =>'45'))}}
                                        </div>
                                         <span class="Cellphone" style="color:red;"></span>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('ProfilePhone', 'Landline Phone', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-9">
                                            {{Form::text('data[Profile][phone]', $value = null, $attributes = array('class'=>'form-control ProfilePhone', 'data-parsley-maxlength'=>'45', 'id'=>'ProfilePhone','maxlength' =>'45'))}}
                                        </div>
                                        <span class="Landlinephone" style="color:red;"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Account & Contact</h4>  
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('UserGroupsId', 'Group', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-9">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" id="inlineRadio1" value="1" name="data[User][groups_id_radio]">
                                                <label for="inlineRadio1"> Administrator </label>
                                            </div>
                                            <div class="radio radio-pink radio-inline">
                                                <input type="radio" id="inlineRadio2" value="4" name="data[User][groups_id_radio]">
                                                <label for="inlineRadio2"> Partner </label>
                                            </div>
                                            <div class="radio radio-purple radio-inline">
                                                <input type="radio" id="inlineRadio3" value="3" name="data[User][groups_id_radio]" checked>
                                                <label for="inlineRadio3"> Member </label>
                                            </div>
                                            <div class="radio radio-warning radio-inline">
                                                <input type="radio" id="inlineRadio4" value="2" name="data[User][groups_id_radio]">
                                                <label for="inlineRadio4"> Host </label>
                                            </div>
                                            <div class="radio radio-custom radio-inline">
                                                <input type="radio" id="inlineRadio5" value="5" name="data[User][groups_id_radio]">
                                                <label for="inlineRadio5"> Co-host </label>
                                            </div>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="inlineRadio6" value="6" name="data[User][groups_id_radio]">
                                                <label for="inlineRadio6"> Podcast host </label>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-2 form-control-label req'))}}
                                        <div class="col-sm-9">
                                            {{Form::text('data[User][username]', $value = null, $attributes = array('class'=>'form-control required user-name', 'data-unique'=> '1',  'data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>0, 'data-parsley-maxlength'=>'45', 'id'=>'UserUsername', 'required'=>true, 'autocomplete'=>'off','maxlength' =>'45'))}}
                                            <span id="rchars">45</span> Character(s) Remaining
                                            <div class="error-block" style="display:none;"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-2 form-control-label req'))}}
                                        <div class="col-sm-9">
                                            {{Form::email('data[User][email]', $value = null, $attributes = array('class'=>'form-control required', 'data-unique'=> '1',  'data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>0, 'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true,'autocomplete'=>'off','maxlength' =>'45'))}}
                                            <div class="error-block" style="display:none;"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserPassword', 'Password', array('class' => 'col-sm-2 form-control-label req'))}}
                                        <div class="col-sm-9">
                                            {{Form::password('data[User][password]', $attributes = array('class'=>'form-control required password-field', 'id'=>'UserPassword', 'required'=>true,'autocomplete'=>'off'))}}
                                            <!-- password strength metter  -->
                                            <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                        </div>
                                        <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('ProfileNotes', 'Notes', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-9">
                                            {{Form::textarea('data[Profile][notes]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'ProfileNotes','rows'=>"2", 'cols'=>"30" ))}}
                                           
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-9">
                                             <div class="radio radio-info radio-inline">
                                                <input type="radio" id="inlineRadio4" value="1" name="data[UserSetting][mailing_newsletter_radio]" checked>
                                                <label for="inlineRadio4"> Enabled </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="inlineRadio5" value="0" name="data[UserSetting][mailing_newsletter_radio]" checked>
                                                <label for="inlineRadio5"> Disabled </label>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        {{Form::label('UserAttachments0', 'Attachments', array('class' => 'col-sm-2 form-control-label'))}}
                                        
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'attachments')
                                                    <div class="col-sm-4 adding-medias media_User_attachments" data-off="{{$url_count}}">
                                                         <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">  
                                                                             @if($med->media->type == "image")  
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'User', 'attachments', 'all', 0, '{{$med->media->filename}}', {{$med->id}})">                     
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>
                                                                             @elseif($med->media->type == "audio")
                                                                             <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/default/audio.png')}}" />
                                                                            </a>
                                                                            @elseif($med->media->type == "video")
                                                                            <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                                                <img src ="{{asset('storage/app/public/media/default/video.png')}}" />
                                                                            </a>
                                                                            @endif  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">                                             
                                                                                <ul class="list-inline pull-right"> 
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_User_attachments" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="User" data-submodule="attachments"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>           
                                                    </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_User_attachments_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                You can attach maximum five files.
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="User" data-submodule="attachments" data-media_type="all" data-limit="5" data-dimension="" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fixed_btn">
                                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[User][btnAdd]" id="UserBtnAdd" data-form-id="UserAdminAddForm">
                                    Add
                                </button>
                               <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                             
                            </div>
                        </div>
                    <!-- end row -->
                </div> <!-- end card-box -->
                {{ Form::close() }}
            </div><!-- end col-->
        </div>
        <!-- end row -->
@include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "User"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
