@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Member</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/members')}}">All Members</a>
                        </li>
                        <li class="active">
                            Edit Member
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div class="col-xs-12">
                    <!-- Form Starts-->
                    {{ Form::open(array('url' => 'admin/user/members/edit/'.$member->id, 'id'=>'MemberAdminEditForm')) }}
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0">Membership</h4>
                                    <div class="p-20">
                                      
                                        <div class="form-group row">
                                            {{Form::label('UserSettingMembershipsId', 'Membership', array('class' => 'col-sm-4 form-control-label'))}}
                                            <div class="col-sm-7">
                                                {{ Form::hidden('data[UserSetting][id]', (isset($member->user_settings[0]->id)) ? $member->user_settings[0]->id : "", array('id'=>'UserSettingId')) }}
                                                {{Form::select('data[UserSetting][memberships_id]', (!empty($memberships) ? $memberships : ''), (isset($user_membership->membership)) ? $user_membership->membership->id : '' , $attributes=array('id'=>'UserSettingMembershipsId', 'class'=>'selectpicker', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                                {{ Form::hidden('data[Member][groups_id_radio]', '3', array('id'=>'MemberGroupsId')) }}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{Form::label('UserSettingMembershipEnd', 'Membership End', array('class' => 'col-sm-4 form-control-label req'))}}
                                            <div class="col-sm-7">
                                                <?php //echo "";$member->user_settings;?> 
                                                <div class="input-group">
                                                    {{Form::text('data[UserSetting][membership_end]', (isset($member->user_settings[0]->membership_end))  ? dateMembershipEnd($member->user_settings[0]->membership_end) : '', $attributes = array('class'=>'form-control required', 'placeholder'=>'mm/dd/yyyy', 'id'=>'datepicker-autoclose'))}}
                                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                                </div><!-- input-group -->
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end card-box -->
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0">Personal Details</h4>
                                    <div class="p-20">
                                        <div class="form-group row">
                                            {{Form::label('name', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                            <div class="col-sm-7">
                                                <div class="form-group col-sm-3">
                                                    {{ Form::hidden('data[Profile][id]', (isset($member->profiles[0]->id))  ? $member->profiles[0]->id : '', array('id'=>'UserProfileId')) }}
                                                    {{Form::select('data[Profile][title]', array('' => 'Choose prefix..', 'Dr.' => 'Dr.', 'Prof' => 'Prof', 'Pir' => 'Pir', 'Colonel' => 'Colonel'), (isset($member->profiles[0]->title)) ? $member->profiles[0]->title : '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                                                </div>
                                                <div class="col-sm-3">
                                                    {{Form::text('data[Profile][firstname]', (isset($member->profiles[0]->firstname))  ? $member->profiles[0]->firstname : '', $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                                </div>
                                                <div class="col-sm-3">
                                                    {{Form::text('data[Profile][lastname]', (isset($member->profiles[0]->lastname))  ? $member->profiles[0]->lastname : '', $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                                </div>

                                                <div class="form-group col-sm-3">
                                                    {{Form::select('data[Profile][sufix]', array('' => 'Choose suffix..', 'Np' => 'Np', 'Jd' => 'Jd', 'M.s.' => 'M.s.'), (isset($member->profiles[0]->sufix))  ? $member->profiles[0]->sufix : '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                                </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{Form::label('MemberLastLogin', 'Last login', array('class' => 'col-sm-4 form-control-label'))}}
                                            <div class="col-sm-7">
                                                {{Form::text('', (isset($member->last_login_at))  ? $member->last_login_at : '', $attributes = array('class'=>'form-control', 'readonly'=>true))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{Form::label('MemberCreated', 'Created', array('class' => 'col-sm-4 form-control-label'))}}
                                            <div class="col-sm-7">
                                                {{Form::text('', (isset($member->created_at))  ? $member->created_at : '', $attributes = array('class'=>'form-control', 'readonly'=>true))}}
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end card-box -->
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0">Account</h4>                         
                                    <div class="p-20">
                                        <div class="form-group row">
                                            {{Form::label('MemberStatus', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                            <div class="col-sm-7">
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="MemberStatusActive" value="active" name="data[Member][status]" {{ $member->status == 'active' ? 'checked' : '' }}>
                                                    <label for="MemberStatusActive"> Active </label>
                                                </div>
                                                <div class="radio radio-inline">
                                                    <input type="radio" id="MemberStatusInactive" value="inactive" name="data[Member][status]" {{ $member->status == 'inactive' ? 'checked' : '' }}>
                                                    <label for="MemberStatusInactive"> Inactive </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <span class="font-13 text-muted"><i class="glyphicon glyphicon-info-sign"></i> Inactive users can't login to theirs accounts</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{Form::label('MemberUsername', 'Username', array('class' => 'col-sm-4 form-control-label req'))}}
                                            <div class="col-sm-7">
                                                {{Form::text('data[Member][username]', (isset($member->username))  ? $member->username : '', $attributes = array('class'=>'form-control required', 'data-unique'=> '1','data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>$member->id, 'data-parsley-maxlength'=>'45', 'id'=>'MemberUsername', 'required'=>true, 'autocomplete'=>'off'))}}
                                                {{ Form::hidden('data[Member][id]', (isset($member->id))  ? $member->id : 0, array('id'=>'MemberId')) }}
                                                <div class="error-block" style="display:none;"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{Form::label('MemberEmail', 'Email', array('class' => 'col-sm-4 form-control-label req'))}}
                                            <div class="col-sm-7">
                                                {{Form::email('data[Member][email]', (isset($member->email))  ? $member->email : '', $attributes = array('class'=>'form-control required','data-unique'=> '1', 'data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>$member->id, 'data-parsley-maxlength'=>'45', 'id'=>'MemberEmail', 'required'=>true, 'autocomplete'=>'off'))}}
                                                <div class="error-block" style="display:none;"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{Form::label('MemberPassword', 'Password', array('class' => 'col-sm-4 form-control-label'))}}
                                            <div class="col-sm-7">
                                                {{Form::password('data[Member][password]', $attributes = array('class'=>'form-control required password-field', 'id'=>'MemberPassword', 'required'=>true,'autocomplete'=>'off'))}}
                                                <div class="error-block" style="display:none;"></div>
                                                <!-- password strength metter  -->
                                                <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                            </div>
                                            <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span>
                                        </div>

                                        <div class="form-group row">
                                            {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-4 form-control-label'))}}
                                            <div class="col-sm-7">
                                                 <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="UserSettingMailingNewsletter1" value="1" name="data[UserSetting][mailing_newsletter_radio]" {{ $member->user_settings[0]->mailing_newsletter == 1 ? 'checked' : '' }}>
                                                    <label for="UserSettingMailingNewsletter1"> Enabled </label>
                                                </div>
                                                <div class="radio radio-inline">
                                                    <input type="radio" id="UserSettingMailingNewsletter0" value="0" name="data[UserSetting][mailing_newsletter_radio]" {{ $member->user_settings[0]->mailing_newsletter == 0 ? 'checked' : '' }}>
                                                    <label for="UserSettingMailingNewsletter0"> Disabled </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                    </div>
                                </div>
                                <!-- end card-box -->
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group">
                            <div class="fixed_btn">
                                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Member][btnAdd]" id="MemberBtnUpdate" data-form-id="MemberAdminEditForm">
                                    Update
                                </button>
                               <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                             
                            </div>
                        </div>
                    {{ Form::close() }}
                    <!-- end row -->
            </div><!-- end col-->
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
