@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit User</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/users')}}">All Users</a>
                        </li>
                        <li class="active">
                            Edit User
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div class="col-xs-12">
                <?php echo $user->title?>
                <div class="card-box">
                    <!-- Form Starts-->
                    {{ Form::open(array('url' => 'admin/user/users/edit/'.$user->id, 'method' => 'PUT', 'id'=>'UserAdminEditForm')) }}
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Personal Details</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('name', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                        <div class="col-sm-7">
                                            {{$user->title}}
                                            <div class="form-group col-sm-3">
                                                <?php 
                                                    $prefixes = array('' => 'Choose prefix..', 'Dr.' => 'Dr.', 'Prof' => 'Prof', 'Pir' => 'Pir', 'Colonel' => 'Colonel');
                                                ?>
                                                {{Form::select('data[Profile][title]', $prefixes, (isset($user->profiles[0]->title)) ? $user->profiles[0]->title : '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::text('data[Profile][firstname]', (isset($user->profiles[0]->firstname))  ? $user->profiles[0]->firstname : '', $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::text('data[Profile][lastname]', (isset($user->profiles[0]->lastname))  ? $user->profiles[0]->lastname: '', $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                            </div>

                                            <div class="form-group col-sm-3">
                                                {{Form::select('data[Profile][sufix]', array('' => 'Choose suffix..', 'Np' => 'Np', 'Jd' => 'Jd', 'M.s.' => 'M.s.'), (isset($user->profiles[0]->sufix))  ? $user->profiles[0]->sufix : '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                               <!--  <select name="data[Profile][sufix]" id="ProfileSufix" class="form-control">
                                                    <option value="">Choose suffix..</option>
                                                    <option value="Np">Np</option>
                                                    <option value="Jd">Jd</option>
                                                    <option value="M.s.">M.s.</option>
                                                    <option value="Mba">Mba</option>
                                                    <option value="Jr">Jr</option>
                                                    <option value="III">III</option>
                                                    <option value="Mda">Mda</option>
                                                    <option value="ND">ND</option>
                                                    <option value="LCSW">LCSW</option>
                                                    <option value="M.A.">M.A.</option>
                                                    <option value="Ph.D.">Ph.D.</option>
                                                    <option value="Ed.D.">Ed.D.</option>
                                                    <option value="CHt">CHt</option>
                                                    <option value="LPC">LPC</option>
                                                    <option value="MBA">MBA</option>
                                                    <option value="MDA">MDA</option>
                                                    <option value="B.Ed.">B.Ed.</option>
                                                    <option value="BSc">BSc</option>
                                                    <option value="LMFT">LMFT</option>
                                                    <option value="B.S.">B.S.</option>
                                                    <option value="CNM">CNM</option>
                                                </select> -->
                                                {{ Form::hidden('data[Profile][id]', (isset($user->profiles[0]->id)) ? $user->profiles[0]->id : '', $attributes=array( 'class'=>'form-control')) }}

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Profile][skype]', (isset($user->profiles[0]->skype))  ? $user->profiles[0]->skype : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileSkype'))}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ProfileCellphone', 'Cell Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Profile][cellphone]', (isset($user->profiles[0]->cellphone))  ? $user->profiles[0]->cellphone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfileCellphone'))}}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('ProfilePhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Profile][phone]', (isset($user->profiles[0]->phone))  ? $user->profiles[0]->phone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'ProfilePhone'))}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserLastLogin', 'Last Login', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-md-7">
                                            {{Form::text('', (isset($user->last_login)) ? $user->last_login : '', $attributes = array('class'=>'form-control', 'id'=>'UserLastLoginView', 'readonly'=>true))}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserCreated', 'Created', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-md-7">
                                            {{Form::text('', (isset($user->created_at)) ? $user->created_at : '', $attributes = array('class'=>'form-control', 'id'=>'UserCreatedView', 'readonly'=>true))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Account & Contact</h4>
                               
                                <div class="p-20">
                                     <div class="form-group row">
                                        {{ Form::hidden('data[User][id]', $user->id) }}
                                        {{Form::label('UserStatus', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            <div class="radio radio-inline radio-info">
                                                <input type="radio" id="inlineRadio6" value="1" name="data[User][status]" {{ $user->status == 1 ? 'checked' : '' }}>
                                                <label for="inlineRadio6"> Active </label>
                                            </div>
                                            <div class="radio radio-inline radio-warning">
                                                <input type="radio" id="inlineRadio8" value="2" name="data[User][status]" {{ $user->status == 2 ? 'checked' : '' }}>
                                                <label for="inlineRadio7"> Login Inactive </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="inlineRadio7" value="0" name="data[User][status]" {{ $user->status == 0 ? 'checked' : '' }}>
                                                <label for="inlineRadio7"> Totally Inactive </label>
                                            </div>
                                           
                                            <div class="clearfix"></div>
                                            <span class="font-13 text-muted"><i class="glyphicon glyphicon-info-sign"></i> Inactive users can't login to theirs accounts</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('UserGroupsId', 'Group', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" id="inlineRadio1" value="1" name="data[User][groups_id_radio]"  {{ $user->groups_id == 1 ? 'checked' : '' }}>
                                                <label for="inlineRadio1"> Administrator </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="inlineRadio2" value="4" name="data[User][groups_id_radio]" {{ $user->groups_id == 4 ? 'checked' : '' }}>
                                                <label for="inlineRadio2"> Partner </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="inlineRadio3" value="3" name="data[User][groups_id_radio]" {{ $user->groups_id == 3 ? 'checked' : '' }}>
                                                <label for="inlineRadio3"> Member </label>
                                            </div>
                                           
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-4 form-control-label req'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[User][username]', (isset($user->username))  ? $user->username : '', $attributes = array('class'=>'form-control required', 'data-unique'=> '1','data-validation' => '1', 'data-form'=> 'userprofile', 'data-userid'=>$user->id, 'data-parsley-maxlength'=>'45', 'id'=>'UserUsername'))}}
                                             <div class="error-block" style="display:none;"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-4 form-control-label req'))}}
                                        <div class="col-sm-7">
                                            {{Form::email('data[User][email]', (isset($user->email))  ? $user->email : '', $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1', 'data-form'=> 'userprofile','data-userid'=>$user->id, 'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true))}}
                                             <div class="error-block" style="display:none;"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('UserPassword', 'Password', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::password('data[User][password]', $attributes = array('class'=>'form-control password-field', 'id'=>'UserPassword'))}}
                                            <!-- password strength metter  -->
                                            <div class="passwordstrength" id="result">&nbsp;&nbsp;</div>
                                        </div>
                                       <span toggle=".password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="font-size: 20px;"></span> 
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ProfileNotes', 'Notes', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::textarea('data[Profile][notes]', (isset($user->profiles[0]->notes))  ? $user->profiles[0]->notes : '', $attributes = array('class'=>'form-control', 'id'=>'ProfileNotes','rows'=>"2", 'cols'=>"30" ))}}
                                           
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('UserSettingMailingNewsletter', 'Newsletter', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                             <div class="radio radio-info radio-inline">
                                                <input type="radio" id="inlineRadio4" value="1" name="data[UserSetting][mailing_newsletter_radio]" {{ (isset($user->user_settings[0]->mailing_newsletter)) ? ($user->user_settings[0]->mailing_newsletter == 1 ? 'checked' : '' )  : '' }}>
                                                <label for="inlineRadio4"> Enabled </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="inlineRadio5" value="0" name="data[UserSetting][mailing_newsletter_radio]" {{ (isset($user->user_settings[0]->mailing_newsletter)) ? ($user->user_settings[0]->mailing_newsletter == 0 ? 'checked' : '' )  : '' }}>
                                                <label for="inlineRadio5"> Disabled </label>
                                            </div>
                                            {{ Form::hidden('data[UserSetting][id]',(isset($user->user_settings[0]->id)) ? $user->user_settings[0]->id : '') }}
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        {{Form::label('UserAttachments0', 'Attachments', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'attachments')
                                                        <div class="col-sm-4 adding-medias media_User_attachments" data-off="{{$url_count}}">
                                                         <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">  
                                                                             @if($med->media->type == "image")  
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'User', 'attachments', 'all', 0, '{{$med->media->filename}}', {{$med->id}})">                     
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>
                                                                             @elseif($med->media->type == "audio")
                                                                             <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/default/audio.png')}}" />
                                                                            </a>
                                                                            @elseif($med->media->type == "video")
                                                                            <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                                                <img src ="{{asset('storage/app/public/media/default/video.png')}}" />
                                                                            </a>
                                                                            @endif  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">                                             
                                                                                <ul class="list-inline pull-right"> 
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_User_attachments" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="User" data-submodule="attachments"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>           
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_User_attachments_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="User" data-submodule="attachments" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                        </div> 
                                    </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fixed_btn">
                                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" value="edit" name="data[User][btnUpdate]" id="UserBtnAdd" data-form-id="UserAdminEditForm">
                                    Update
                                </button>
                               <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                             
                            </div>
                        </div>
                    {{ Form::close() }}
                    <!-- end row -->
                </div> <!-- end ard-box -->
            </div><!-- end col-->
        </div>
        <!-- end row -->
    @include('backend.cms.upload_media_popup',['module_id' =>$user->id,'main_module' => "User"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
