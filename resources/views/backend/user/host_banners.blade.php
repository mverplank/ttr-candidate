@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Edit Host</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="active">
                     Users
                  </li>
                  <li class="active">
                     <a href="{{url('admin/user/hosts')}}">All Hosts</a>
                  </li>
                  <li class="active">
                     Edit Host
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <!-- start row --> 
      <div class="row">
         <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="card-box">
               <div class="form-group row">
                  {{Form::label('HostFullname', 'Host', array('class' => 'col-sm-2 form-control-label'))}}  
                  <div class="col-sm-10">
                     {{Form::text('data[Host][Name]',$profile[0]['name'], ['id'=>'HostFullname', 'class'=>'form-control', 'readonly' => ''])}}
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end row -->
      <div class="row">
         <div class="col-sm-12 col-xs-12 col-md-12">
            <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Default banners</h4>
            <div class="p-20">
               <div class="form-group row">
                  <div class="form-group col-sm-12">
                     {{Form::label('EpisodeFileMedia0', 'Banner 120x300 [px] on the schedule', array('class' => 'col-sm-2 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0)
                           @php ($url_count = 1)
                           @foreach($media as $med)
                           @if($med->sub_module == 'banner_show_default_schedule')
                           <div class="col-sm-4 adding-medias media_Host_banner_show_default_schedule" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_banner_show_default_schedule" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="banner_show_default_schedule" ></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++)
                           @endif
                           @endforeach
                           @endif
                           <div class="media_Host_banner_show_default_schedule_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 3 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="banner_show_default_schedule" data-media_type="image" data-limit="3" data-dimension=""data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="p-20">
               <div class="form-group row">
                  <div class="form-group col-sm-12">
                     {{Form::label('EpisodeFileMedia0', 'Banner 625x258 [px] on the player', array('class' => 'col-sm-2 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0)
                           @php ($url_count = 1)
                           @foreach($media as $med)
                           @if($med->sub_module == 'banner_show_default_player')
                           <div class="col-sm-4 adding-medias media_Host_banner_show_default_player" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_banner_show_default_player" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="banner_show_default_player" ></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++)
                           @endif
                           @endforeach
                           @endif
                           <div class="media_Host_banner_show_default_player_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 3 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="banner_show_default_player" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="p-20">
               <div class="form-group row">
                  <div class="form-group col-sm-12">
                     {{Form::label('EpisodeFileMedia0', 'Banner 120x150 [px] on the featured shows', array('class' => 'col-sm-2 form-control-label'))}}                                  
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0)
                           @php ($url_count = 1)
                           @foreach($media as $med)
                           @if($med->sub_module == 'banner_show_default_featured')
                           <div class="col-sm-4 adding-medias media_Host_banner_show_default_featured" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_banner_show_default_featured" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="banner_show_default_featured" ></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++)
                           @endif
                           @endforeach
                           @endif
                           <div class="media_Host_banner_show_default_featured_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 3 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="banner_show_default_featured" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @foreach($host[0]['shows'] as $shows)
      <div class="row">
         <div class="col-sm-12 col-xs-12 col-md-12">
            <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">{{$shows['name']}}</h4>
            <div class="p-20">
               <div class="form-group row">
                  <div class="form-group col-sm-12">
                     {{Form::label('EpisodeFileMedia0', 'Banner 120x300 [px] on the schedule', array('class' => 'col-sm-2 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0)
                           @php ($url_count = 1)
                           @foreach($media as $med)
                           @if($med->sub_module == 'banner_show_'.$shows['id'].'_schedule')
                           <div class="col-sm-4 adding-medias media_Host_banner_show_{{$shows['id']}}_schedule" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_banner_show_{{$shows['id']}}_schedule" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="banner_show_{{$shows['id']}}_schedule" ></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++)
                           @endif
                           @endforeach
                           @endif
                           <div class="media_Host_banner_show_{{$shows['id']}}_schedule_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 3 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="banner_show_{{$shows['id']}}_schedule" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="p-20">
               <div class="form-group row">
                  <div class="form-group col-sm-12">
                     {{Form::label('EpisodeFileMedia0', 'Banner 625x258 [px] on the player', array('class' => 'col-sm-2 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0)
                           @php ($url_count = 1)
                           @foreach($media as $med)
                           @if($med->sub_module == 'banner_show_'.$shows['id'].'_player')
                           <div class="col-sm-4 adding-medias banner_show_{{$shows['id']}}_player" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_banner_show_{{$shows['id']}}_player" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="banner_show_{{$shows['id']}}_player" ></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++)
                           @endif
                           @endforeach
                           @endif
                           <div class="media_Host_banner_show_{{$shows['id']}}_player_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 3 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="banner_show_{{$shows['id']}}_player" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="p-20">
               <div class="form-group row">
                  <div class="form-group col-sm-12">
                     {{Form::label('EpisodeFileMedia0', 'Banner 120x150 [px] on the featured shows', array('class' => 'col-sm-2 form-control-label'))}}                                  
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0)
                           @php ($url_count = 1)
                           @foreach($media as $med)
                           @if($med->sub_module == 'banner_show_'.$shows['id'].'_featured')
                           <div class="col-sm-4 adding-medias banner_show_{{$shows['id']}}_featured" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_banner_show_{{$shows['id']}}_featured" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="banner_show_{{$shows['id']}}_featured" ></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++)
                           @endif
                           @endforeach
                           @endif
                           <div class="media_Host_banner_show_{{$shows['id']}}_featured_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 3 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="banner_show_{{$shows['id']}}_featured" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endforeach
      @include('backend.cms.upload_media_popup',['module_id' =>$host[0]['id'],'main_module' => "Host"])
   </div>
</div>
@endsection