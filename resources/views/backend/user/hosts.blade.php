@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Hosts</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            Hosts
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->    
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="all" value="0" name="filterHosts" {{ ($host_type == 0) ? "checked" : "" }} onclick="filterHosts(0)" checked>
                    <label for="all"> All types</label>
                </div>
                <div class="radio radio-primary radio-inline">
                    <input type="radio" id="host" value="1" name="filterHosts" onclick="filterHosts(1)" {{ ($host_type == 1) ? "checked" : "" }}>
                    <label for="admin"> Hosts </label>
                </div> 
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="co-host" value="2" name="filterHosts" onclick="filterHosts(2)" {{ ($host_type == 2) ? "checked" : "" }}>
                    <label for="partner"> Co-Hosts </label>
                </div>
                <div class="radio radio-custom radio-inline"> 
                    <input type="radio" id="podcast-host" value="3" name="filterHosts" onclick="filterHosts(3)"  {{ ($host_type == 3) ? "checked" : "" }}>
                    <label for="member"> Podcast Hosts </label>
                </div>
                <div style="float:right;">
                    <a href="{{url('/admin/user/hosts/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Host / Co-Host</a>
                    <a href="javascript:void(0);" id="hosts_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>
                </div>
            </div>
        </div> 
         <!--  add hosts export form by parmod -->
        <div class="row" style="display: none;" id="hosts_export_option">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="p-20">
                        {{ Form::open(array('url' => 'admin/user/hosts/export','method' => 'get', 'id'=>'HostsExportForm')) }}
                            <div class="form-group row"> 
                            {{Form::label('Banner', 'Name', array('class' => 'form-control-label col-sm-1'))}}             
                               <div class="col-sm-10">
                               
                                {{Form::select('data[Host][export_filter]', array('All'=>'All','Online'=>'Online','Offline'=>'Offline','Featured'=>'Featured','Channels'=>$all_export_channels,'Type'=>array('Host'=>'Host','Cohost'=>'Cohost','Both'=>'Both') ),'', $attributes=array('id'=>'HostExportFilter', 'class'=>'selectpicker m-b-0', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'select'))}} 
                                <input type="hidden" name="lebel" value="undefined" id="eport_lebel"> 
                                <!-- <input type="hidden" name="selectopn" value="" id="eport_selected_option"> -->                         
                               </div>
                            </div>
                            
                            <div class="form-group row"> 
                                 <div class="col-sm-7 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light submit_form" value="Add" id="HostsExportSubmit">Export</button>
                                 </div>                           
                            </div>
            
                         {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- form end  -->             
        <!-- end row -->

        <div class="row">
            <div class="card-box">
            {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : ''), !empty($selected_channel) ? $selected_channel: '', $attributes=array('id'=>'hostChannelId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
            </div>
        </div> <!-- end row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="hosts_datatable" class="table table-striped table-hover hosts_table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
