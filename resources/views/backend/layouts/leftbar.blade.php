@php
    $channel_main     = '';
    $channel_list     = '';
    $schedule_list    = '';

    $episode_main     = '';
    $episode_list     = '';
    $upcoming_main    = '';
    $upcoming_list    = '';
    $archive_main     = '';
    $archive_list     = '';
    $show_main        = '';
    $show_list        = '';
    $guest_main       = '';
    $guest_list       = '';

    $banner_main      = '';
    $banner_list      = '';
    $sponsor_main     = '';
    $sponsor_list     = '';

    $items_main       = '';
    $items_list       = '';
    $pitems_main      = '';
    $pitems_list      = '';
    
    $user_main        = '';
    $user_list        = '';
    $host_main        = '';
    $host_list        = '';
    $partner_main     = '';
    $partner_list     = '';
    $member_main      = '';
    $member_list      = '';
    
    $membership_main  = '';
    $membership_list  = '';

    $content_data_main = '';
    $content_data_list = '';
    $media_main = '';
    $media_list = '';
@endphp

@if(Request::path() == 'admin/channel/channels/add' || strpos( Request::path(), 'admin/channel/channels/edit' ) !== false)
    @php
        $channel_main = "style='display:block'";
        $channel_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/channel/schedules/agenda' ) !== false)
    @php
        $channel_main = "style='display:block'";
        $schedule_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/channel/schedules/agenda' ) !== false || strpos( Request::path(), 'admin/show/episodes/add_agenda' ) !== false || strpos( Request::path(), 'admin/show/episodes/view_agenda' ) !== false || strpos( Request::path(), 'admin/show/episodes/edit_view' ) !== false || strpos( Request::path(), 'admin/show/episodes/select_encore' ) !== false)  
    @php
        $episode_main = "style='display:block'";
        $episode_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/show/episodes/edit_upcoming' ) !== false || strpos( Request::path(), 'admin/show/episodes/view_upcoming' ) !== false) 
    @php
        $upcoming_main = "style='display:block'";
        $upcoming_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/show/episodes/edit_archived' ) !== false || strpos( Request::path(), 'admin/show/episodes/view_archived' ) !== false)
    @php
        $archive_main = "style='display:block'";
        $archive_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/show/shows/add' ) !== false || strpos( Request::path(), 'admin/show/shows/edit' ) !== false) 
    @php
        $show_main = "style='display:block'";
        $show_list = "active";
    @endphp
@elseif( strpos( Request::path(), '/show/guests/add' ) !== false || strpos( Request::path(), '/show/guests/edit' ) !== false) 
    @php
        $guest_main = "style='display:block'";
        $guest_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/advertising/banners/add' ) !== false || strpos( Request::path(), 'admin/advertising/banners/edit' ) !== false) 
    @php
        $banner_main = "style='display:block'";
        $banner_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/advertising/sponsors/add' ) !== false || strpos( Request::path(), 'admin/advertising/sponsors/episodes' ) !== false || strpos( Request::path(), 'admin/advertising/sponsors/edit' ) !== false) 
    @php
        $sponsor_main = "style='display:block'";
        $sponsor_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/inventory/items/add' ) !== false || strpos( Request::path(), 'admin/inventory/items/edit' ) !== false || strpos( Request::path(), 'admin/advertising/sponsors/edit' ) !== false || strpos( Request::path(), 'partner/inventory/items/edit' ) !== false) 
    @php
        $items_main = "style='display:block'";
        $items_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/inventory/items/accept' ) !== false ) 
    @php
        $pitems_main = "style='display:block'";
        $pitems_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/user/users/add' ) !== false  || strpos( Request::path(), 'admin/user/users/edit' ) !== false) 
    @php
        $user_main = "style='display:block'";
        $user_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/user/hosts/add' ) !== false  || strpos( Request::path(), 'admin/user/hosts/edit' ) !== false) 
    @php
        $host_main = "style='display:block'";
        $host_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/user/partners/add' ) !== false  || strpos( Request::path(), 'admin/user/partners/edit' ) !== false) 
    @php
        $partner_main = "style='display:block'";
        $partner_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/user/members/add' ) !== false  || strpos( Request::path(), 'admin/user/members/edit' ) !== false) 
    @php
        $member_main = "style='display:block'";
        $member_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/membership/memberships/add' ) !== false || strpos( Request::path(), 'admin/inventory/months/edit' ) !== false || strpos( Request::path(), 'admin/inventory/months/index' ) !== false) 
    @php
        $membership_main = "style='display:block'";
        $membership_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/cms/content_data/add' ) !== false || strpos( Request::path(), 'admin/cms/content_categories/add' ) !== false || strpos( Request::path(), 'admin/cms/content_categories/edit' ) !== false || strpos( Request::path(), 'admin/cms/content_categories/tree' ) !== false || strpos( Request::path(), 'admin/cms/content_data/edit' ) !== false) 
    @php
        $content_data_main = "style=display:block";
        $content_data_list = "active";
    @endphp
@elseif( strpos( Request::path(), 'admin/cms/media/add/audio' ) !== false || strpos( Request::path(), 'admin/cms/media/add/video' ) !== false) 
    @php
        $media_main = "style=display:block";
        $media_list = "active";
    @endphp
@endif

<div class="left side-menu">
    <?php //echo Request::path(); ?>
    <div class="sidebar-inner slimscrollleft">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>     
                <!-- Dashboard -->                       
                <li class="has_sub">
                    @if(Config::get('constants.PARTNER') == getCurrentUserType())
                        <a href="{{ url('partner') }}" class="waves-effect">
                            <i class="mdi mdi-view-dashboard"></i> 
                            <span> Dashboard </span> 
                        </a>
                    @elseif(Config::get('constants.HOST') == getCurrentUserType())
                        <a href="{{ url('host') }}" class="waves-effect">
                            <i class="mdi mdi-view-dashboard"></i> 
                            <span> Dashboard </span> 
                        </a>
                    @else
                        <a href="{{ url('admin') }}" class="waves-effect">
                            <i class="mdi mdi-view-dashboard"></i> 
                            <span> Dashboard </span> 
                        </a>
                    @endif
                </li>
                <!-- Channels -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">           
                        <i class="mdi mdi-access-point-network"></i>
                        <span> Channels </span> 
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled" {{$channel_main}}>
                        <li class="{{$channel_list}}"><a href="{{route('channels')}}" class="{{$channel_list}}"><i class="mdi mdi-view-list"></i>List</a></li>
                        <li class="{{$schedule_list}}"><a href="{{url('admin/channel/schedules/agenda')}}"><i class="ti-timer"></i>Schedule</a></li>
                    </ul>
                </li>
                @endif
                <!-- Shows -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType() || Config::get('constants.HOST') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="fa fa-microphone"></i>
                        <span> Shows </span> 
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled" {{$episode_main}} {{$upcoming_main}} {{$archive_main}} {{$show_main}} {{$guest_main}}>
                        @if(Config::get('constants.ADMIN') == getCurrentUserType())
                            <li class="{{$episode_list}}"><a href="{{url('admin/show/schedules/agenda')}}" class="{{$episode_list}}"> <i class="mdi mdi-cast"></i> Add Episode</a></li>
                            <li class="{{$upcoming_list}}"><a href="{{route('upcoming_episodes')}}" class="{{$upcoming_list}}"><i class="mdi mdi-calendar-clock"></i>Upcoming</a></li>
                            <li class="{{$archive_list}}"><a href="{{url('admin/show/episodes/archived')}}" class="{{$archive_list}}"><i class="mdi mdi-archive"></i>Archive</a></li>
                            <li class="{{$show_list}}"><a class="{{$show_list}}" href="{{url('admin/show/shows')}}"><i class="mdi mdi-view-list"></i>Shows</a></li>                    
                            <li class="{{$guest_list}}"><a class="{{$guest_list}}" href="{{url('admin/show/guests')}}"><i class="mdi mdi-account-multiple-outline"></i>Guests</a></li>
                        @endif
                        @if(Config::get('constants.HOST') == getCurrentUserType())
                            <li><a href="{{url('host/show/schedules/agenda')}}"><i class="mdi mdi-cast"></i> Add Episode</a></li>
                            <li class="{{$upcoming_list}}"><a class="{{$upcoming_list}}" href="{{route('upcoming_episodes')}}"><i class="mdi mdi-calendar-clock"></i>Upcoming</a></li>
                            <li class="{{$archive_list}}"><a class="{{$archive_list}}" href="{{url('host/show/episodes/archived')}}"><i class="mdi mdi-archive"></i>Archive</a></li>
                            <li class="{{$guest_list}}"><a class="{{$guest_list}}" href="{{route('host_guests')}}"><i class="mdi mdi-account-multiple-outline"></i>Guests</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                <!-- Content -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect {{$content_data_list}} {{$media_list}}"><i class="fa fa-tv"></i><span> Content </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" {{$content_data_main}} {{$media_main}}>
                        <li class="{{$content_data_list}} {{$media_list}}"><a href="{{url('admin/cms/content_data/index')}}"> <i class="mdi mdi-view-list"></i>Manage Entries</a></li>
                        <li><a href="{{url('admin/cms/media/add/image')}}"> <i class="ion-images"></i>Media Library</a></li>
                        <!-- <li><a href="{{url('cms/public/en/backend/media/media')}}"> Media Library</a></li> -->
                        <li><a href="{{route('get_cms_pages')}}"> <i class="mdi mdi-file-multiple"></i>Pages</a></li>
                        <li><a href="{{route('get_cms_themes')}}"> <i class="mdi mdi-brush"></i>Themes</a></li>
                        <li><a href="#"> <i class="mdi mdi-auto-fix"></i>Featured</a></li>
                    </ul>
                </li>
                @endif
                <!-- Advertising -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn"></i><span> Advertising </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" {{$banner_main}} {{$sponsor_main}}>
                        <li class="{{$banner_list}}"><a class="{{$banner_list}}" href="{{url('admin/advertising/banners')}}"><i class="mdi mdi-view-carousel"></i>Banners</a></li>
                        <li><a href="{{url('admin/show/categories/banner')}}"><i class="mdi mdi-more"></i>Banner Categories</a></li>
                        <li class="{{$sponsor_list}}"><a class="{{$sponsor_list}}" href="{{url('admin/advertising/sponsors')}}"><i class="mdi mdi-coin"></i>Sponsors</a></li>
                    </ul>
                </li>
                @endif
                <!-- Bonus Items -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType() || Config::get('constants.PARTNER') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-gift"></i> <span> Bonus Items </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" {{$items_main}} {{$pitems_main}}>
                        @if(Config::get('constants.ADMIN') == getCurrentUserType())
                            <li class="{{$items_list}}"><a class="{{$items_list}}" href="{{url('admin/inventory/items/index')}}"><i class="mdi mdi-view-list"></i>All Items</a></li>
                            <li class="{{$pitems_list}}"><a class="{{$pitems_list}}" href="{{url('admin/inventory/items/partners')}}"><i class="mdi mdi-view-list"></i>Partners Items</a></li>
                            <li><a href="{{url('admin/inventory/categories')}}"><i class="fa fa-folder-open-o"></i>Item Categories</a></li>
                        @endif
                        @if(Config::get('constants.PARTNER') == getCurrentUserType())
                            <li class="{{$items_list}}"><a class="{{$items_list}}" href="{{url('partner/inventory/items/my')}}" title="List of items"><i class="mdi mdi-view-list"></i>My Items</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                <!-- Users -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-group"></i> <span> Users </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" {{$user_main}} {{$host_main}} {{$partner_main}}>
                        <li class="{{$user_list}}"><a class="{{$user_list}}" href="{{url('admin/user/users')}}"><i class="ion-person-stalker"></i>All</a></li>
                        <li class="{{$host_list}}"><a class="{{$host_list}}" href="{{url('admin/user/hosts')}}"><i class="ion-man"></i>Hosts</a></li>
                        <li class="{{$partner_list}}"><a class="{{$partner_list}}" href="{{url('admin/user/partners')}}"><i class="ion-man"></i>Partners</a></li>
                        <li class="{{$member_list}}"><a class="{{$member_list}}" href="{{url('admin/user/members')}}"><i class="ion-person-stalker"></i>Members</a></li>
                        <li><a href="{{url('admin/user/activities')}}"><i class="ion-clipboard"></i>Transaction Report</a></li>
                    </ul>
                </li>
                @endif
                <!-- Base -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-gears"></i> <span> Configuration </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" {{$membership_main}}>
                        <li><a href="{{route('get_base_cofig')}}">Base</a></li>
                        <li class="{{$membership_list}}"><a class="{{$membership_list}}" href="{{url('admin/membership/memberships')}}">Memberships</a></li>
                        <li><a href="{{route('get_payments')}}">Payments</a></li>
                        <li><a href="{{route('all_categories')}}/show">Categories</a></li>
                        <li><a href="{{route('prefix_sufix')}}">Pre-fixes/suffixes</a></li>
                        <li><a href="{{route('get_content_email')}}">Email Contents</a></li>
                        <!-- <li><a href="#">Email Styles</a></li> -->
                    </ul>
                </li>
                @endif
                <!-- Tools -->
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-wrench"></i> <span> Tools </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('get_news_letter')}}">Newsletter</a></li>
                        <li><a href="{{route('tools_clear_cache')}}">Clear Cache</a></li>
                        <li><a href="#">SAM Log</a></li>
                        <li><a href="{{route('display_index')}}">Help</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->
</div>
