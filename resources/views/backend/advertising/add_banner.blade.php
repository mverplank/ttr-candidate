@extends('backend.layouts.main')

@section('content')

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Banner</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="{{url('admin/advertising/banners')}}">Banners</a>
                        </li>
                        <li class="active">
                           Add Banners
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div class="col-xs-12">              
                <div class="card-box">
                    <!-- Form Starts-->
                    {!! Form::open(['url' => 'admin/advertising/banners/add', 'files' => true, 'id' => 'BannerAdminAddForm']) !!}
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Base</h4>
                                <input type="hidden" name="form_type" id="form_type" value="add">
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('BannerTitle', 'Title', array('class' => 'col-sm-3 form-control-label req'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Banner][title]', $value = null, $attributes =array('id'=>'BannerTitle', 'class'=>'form-control required form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => '255','data-required' => '1'))}}
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        {{Form::label('BannerUrl', 'Target url', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Banner][url]',$value = null, $attributes =array('id'=>'BannerUrl', 'class'=>'form-control form_ui_input',   'data-maxlength' => '255', 'data-forma' => '1','data-forma-def' => '1','data-type' => 'url', 'model' => 'Banner'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Where to redirect after banner click - full (with "http://"); ex. http://domain.com/sub-dir/path/document.html</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <span class="error-block"></span>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        {{Form::label('BannerNotices', 'Notices', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{ Form::textarea('data[Banner][notices]', null, ['id' => 'BannerNotices', 'class'=>'form-control','rows'=>'2', 'cols'=>'30',
                                                'data-forma' => "1",'data-forma-def'=>"1", 'data-type'=>"textarea",'data-gramm'=>"true",'data-txt_gramm_id'=>" ",'data-gramm_id'=>" ",'spellcheck'=>"false",'data-gramm_editor'=>"true"]) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('BannerIsOnline', 'Status', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7">
                                            <div class="radio radio-info radio-inline">
                                                {{Form::radio('data[Banner][is_online]', '1', true, array('id'=>'BannerIsOnline1'))}}
                                                {{Form::label('BannerIsOnline1', 'Online', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                            <div class="radio radio-warning radio-inline">
                                                {{Form::radio('data[Banner][is_online]', '0', false, array('id'=>'BannerIsOnline0'))}}
                                                {{Form::label('BannerIsOnline0', 'Offline', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                        </div>
                                    </div> 
                                            
                                </div>
                            </div>
                       
                         <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Banners</h4>                             
                                <div class="p-20">
                                    <div class="form-group row">
                                       {{Form::label('BannerIs120x300', 'Use size 120x300', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-7">
                                           <div class="radio radio-info radio-inline">
                                               {{Form::radio('data[Banner][is_120x300]', '1', true, array('id'=>'BannerIs120x300','value'=>'1'))}}
                                               {{Form::label('BannerIs120x300', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                           <div class="radio radio-warning radio-inline">
                                               {{Form::radio('data[Banner][is_120x300]', '0', false, array('id'=>'BannerIs120x300','value'=>'0'))}}
                                               {{Form::label('BannerIs120x300', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                       </div>
                                    </div>              
                                    <div class="form-group row">
                                        {{Form::label('BannerLink120x300', 'Link on 120x300', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Banner][link_120x300]', $value=null, $attributes =array('id'=>'BannerLink120x300', 'data-validation' => '1', 'class'=>'form-control form_ui_input', 'placeholder'=>'https://example.com','data-type' => 'url'))}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner 120x300 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == '120x300')
                                                        <div class="col-sm-4 adding-medias media_Banner_120x300" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                              
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-right">
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Banner_120x300" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Banner" data-submodule="120x300" ></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Banner_120x300_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                This banner will be shown on player when current show banner is not defined or currently playing media/stream is not defined on the Banner schedule (set of queued adverts for example). First banner will be used.
                                                <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Banner" data-submodule="120x300" data-media_type="image" data-limit="3" data-dimension="120x300" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                       {{Form::label('BannerIs300x250', 'Use size 300x250', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-7">
                                           <div class="radio radio-info radio-inline">
                                               {{Form::radio('data[Banner][is_300x250]', '1', true, array('id'=>'BannerIs300x250','value'=>'1'))}}
                                               {{Form::label('BannerIs300x250', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                           <div class="radio radio-warning radio-inline">
                                               {{Form::radio('data[Banner][is_300x250]', '0', false, array('id'=>'BannerIs300x250','value'=>'0'))}}
                                               {{Form::label('BannerIs300x250', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                       </div>
                                    </div>  
                                    <div class="form-group row">
                                        {{Form::label('BannerLink300x250', 'Link on 300x250', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Banner][link_300x250]', $value=null, $attributes =array('id'=>'BannerLink300x250', 'data-validation' => '1', 'class'=>'form-control form_ui_input', 'placeholder'=>'https://example.com', 'data-type' => 'url'))}}
                                            </div>
                                        </div>
                                    </div>                                   
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner 300x250 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == '300x250')
                                                        <div class="col-sm-4 adding-medias media_Banner_300x250" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                              
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-right">
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Banner_300x250" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Banner" data-submodule="300x250" ></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>    
                                                        </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Banner_300x250_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                                <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Banner" data-submodule="300x250" data-media_type="image" data-limit="3" data-dimension="300x250" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                       {{Form::label('BannerIs625x258', 'Use size 625x258', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-7">
                                           <div class="radio radio-info radio-inline">
                                               {{Form::radio('data[Banner][is_625x258]', '1', true, array('id'=>'BannerIs625x2581','value'=>'1'))}}
                                               {{Form::label('BannerIs625x258', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                           <div class="radio radio-warning radio-inline">
                                               {{Form::radio('data[Banner][is_625x258]', '0', false, array('id'=>'BannerIs625x2580','value'=>'0'))}}
                                               {{Form::label('BannerIs625x258', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                       </div>
                                    </div> 
                                    <div class="form-group row">
                                        {{Form::label('BannerLink625x258', 'Link on 625x258', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Banner][link_625x258]', $value=null, $attributes =array('id'=>'BannerLink625x258', 'data-validation' => '1', 'class'=>'form-control form_ui_input', 'placeholder'=>'https://example.com','data-type' => 'url'))}}
                                            </div>
                                        </div>
                                    </div>                         
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner 625x258 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                         <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == '625x258')
                                                    <div class="col-sm-4 adding-medias media_Banner_625x258" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Banner_625x258" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Banner" data-submodule="625x258" ></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Banner_625x258_outer hidden" data-off="0" style="display: none;"></div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Banner" data-submodule="625x258" data-media_type="image" data-limit="3" data-dimension="625x258" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                       {{Form::label('BannerIs728x90', 'Use size 728x90', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-7">
                                           <div class="radio radio-info radio-inline">
                                               {{Form::radio('data[Banner][is_728x90]', '1', true, array('id'=>'BannerIs728x90','value'=>'1'))}}
                                               {{Form::label('BannerIs728x90', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                           <div class="radio radio-warning radio-inline">
                                               {{Form::radio('data[Banner][is_728x90]', '0', false, array('id'=>'BannerIs728x90','value'=>'0'))}}
                                               {{Form::label('BannerIs728x90', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                           </div>
                                       </div>
                                    </div> 
                                    <div class="form-group row">
                                        {{Form::label('BannerLink728x90', 'Link on 728x90', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Banner][link_728x90]', $value=null, $attributes =array('id'=>'BannerLink728x90', 'data-validation' => '1', 'class'=>'form-control form_ui_input', 'placeholder'=>'https://example.com', 'data-type' => 'url'))}}
                                            </div>
                                        </div>
                                    </div>                           
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner on widget player', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == '728x90')
                                                    <div class="col-sm-4 adding-medias media_Banner_728x90" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Banner_728x90" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Banner" data-submodule="728x90" ></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Banner_728x90_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                                <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Banner" data-submodule="728x90" data-media_type="image" data-limit="3" data-dimension="728x90" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">View parameters</h4>
                                <div class="form-group row">
                                    {{Form::label('ChannelChannelSet', 'Channels', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        @if(!empty($all_channels))
                                            @foreach ( $all_channels as $i =>$item )
                                            <div class="checkbox checkbox-pink">
                                                {!! Form::checkbox( 'data[Channel][Channel][]', $i, '', ['class' => 'md-check', 'id' => 'ChannelChannel'.$i] ) !!}
                                                {!! Form::label('ChannelChannel'.$i,  $item) !!}
                                            </div>
                                            @endforeach
                                        @else
                                            No Channel assigned yet.
                                        @endif
                                    </div>
                                </div><!-- end row -->
                                <hr>
                                 <div class="form-group row">
                                    {{Form::label('CategoryCategorySet', 'Categories', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="row">
                                            @if(!empty($all_category))
                                                @foreach ( $all_category as $i =>$item )
                                                <div class="col-sm-4">
                                                    <div class="checkbox checkbox-pink">
                                                        {!! Form::checkbox( 'data[Category][Category][]', $i, '', ['class' => 'md-check', 'id' => 'CategoryCategory'.$i] ) !!}
                                                        {!! Form::label('CategoryCategory'.$i,  $item) !!}
                                                    </div>
                                               </div>
                                                @endforeach
                                            @else
                                                No Channel assigned yet.
                                            @endif                                           
                                        </div>                                          
                                    </div>
                                </div><!-- end row -->    
                                <hr>
                                 <div class="form-group row">
                                    {{Form::label('BannerViewPeriodFrom', 'Start date', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            
                                            {{Form::text('data[Banner][view_period_from]',$value = null, $attributes = array('class'=>'form-control', 'placeholder'=>'mm/dd/yyyy', 'id'=>'BannerViewPeriodFrom', 'value'=>"" ,'data-forma'=>"1" ,'data-forma-def'=>"1" ,'data-type'=>"date"))}}
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                        </div><!-- input-group -->
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Banner will be shown from this date; leave empty if not required</span>
                                    </div>
                                </div>     
                                <hr>
                                <div class="form-group row">
                                    {{Form::label('BannerViewPeriodTo', 'End date', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            
                                            {{Form::text('data[Banner][view_period_to]',$value = null, $attributes = array('class'=>'form-control', 'placeholder'=>'mm/dd/yyyy', 'id'=>'BannerViewPeriodTo', 'value'=>"" ,'data-forma'=>"1" ,'data-forma-def'=>"1" ,'data-type'=>"date"))}}
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                        </div><!-- input-group -->
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>  Banner will be shown to this date; leave empty if not required</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('BannerMaxNumberOfViews', 'Max. number of views', array('class' => 'col-sm-4 form-control-label'))}} 
                                    <div class="col-sm-7">
                                        <div class="input-group">  
                                   {{Form::number('data[Banner][max_number_of_views]', $value = null, $attributes = array('class'=>'vertical-spin form-control','id'=>'BannerMaxNumberOfViews','data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"int", 'autocomplete'=>"off",'type'=>"number", 'min'=>"0",'step'=>"1"))}}
                                </div>
                                <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>   Banner will be shown until view number less than this value; leave empty or zero if not required</span>
                                </div>
                                </div>  
                                <div class="form-group row">
                                    {{Form::label('BannerMaxNumberOfClicks', 'Max. number of clicks', array('class' => 'col-sm-4 form-control-label'))}} 
                                    <div class="col-sm-7">
                                        <div class="input-group">                                   
                                    {{Form::number('data[Banner][max_number_of_clicks]', $value = null, $attributes = array('class'=>'vertical-spin form-control','id'=>'BannerMaxNumberOfClicks','data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"int", 'autocomplete'=>"off",'type'=>"number", 'min'=>"0",'step'=>"1"))}}
                                   </div>
                                   <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>  Banner will be shown until clicks number less than this value; leave empty or zero if not required</span>
                                 </div>
                                </div>                                                                 
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="fixed_btn">
                                <button type="button" id='BannerBtnAdd' data-form-id="BannerAdminAddForm" class="btn btn-primary waves-effect waves-light submit_form" name="data[Banner][btnAdd]">
                                     Add
                                </button>
                            </div>
                        </div>
                    {{ Form::close() }}
                    <!-- end row -->
                </div> <!-- end ard-box -->
            </div><!-- end col-->
        </div>
        <!-- end row -->
    <!-- Media Library Modal  -->
    @include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "Banner"])
    <!--  <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog dialog-width">   
          <div class="modal-content">
            <div class="">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Featured Image</h4>
            </div>
                <ul class="nav nav-tabs">
                    <li class=" ">
                        <a href="#MediaFiles" data-toggle="tab" aria-expanded="true" id="MediaFilesUrl">
                            <span class="visible-xs"><i class="fa fa-user"></i></span>
                            <span class="hidden-xs">Media Library</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#UploadFiles" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-home"></i></span>
                            <span class="hidden-xs">Upload Files</span>
                        </a>
                    </li>
                </ul>   
                <div class="tab-content">
                    <div class="tab-pane active" id="UploadFiles">
                       <div class="form-group row">
                            <div class="col-sm-9">
                                <div class="padding-left-0 padding-right-0">
                                    <div id="banner_image" class="dropzone channel_image" data-limit="3" data-media_type="image" data-image-for="player" data-module="Channel" data-preview-ele="#banner_prev">
                                        <div class="col-sm-9 previewDiv" id="banner_prev"></div>
                                        <div class="col-sm-3 dz-message d-flex flex-column">
                                           <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                            Drag &amp; Drop here or click
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="MediaFiles">
                        <div class="row">
                            <div class="col-lg-8" style="border-right:1px solid #e4e3e3;">
                                <div class="">
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="images" value="0" name="filterMedia" onclick="getMedia('image','')" checked>
                                        <label for="images"> Images </label>
                                    </div>
                                    <div class="radio radio-pink radio-inline">
                                        <input type="radio" id="audios" value="1" name="filterMedia" onclick="getMedia('image','')" >
                                        <label for="audios"> Audios </label>
                                    </div> 
                                    <div class="radio radio-purple radio-inline">
                                        <input type="radio" id="videos" value="2" name="filterMedia" onclick="getMedia('image','')" >
                                        <label for="videos"> Videos </label>
                                    </div> 
                                    <div class="radio radio-purple radio-inline">
                                       <select id ='mediaDimension' name='mediaDimension' class="form-control">
                                          <option value="">Size</option>
                                          <option value="500x200">500x200</option>
                                          <option value="170x300">170x300</option>
                                          <option value="170x137">170x137</option>
                                        </select>
                                    </div> 
                                    <ul class="grid row" id="mediagrid" style="overflow-y: scroll; height:650px;">
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div id="AttachmentDetail">
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>                          
            <div class="modal-footer">
                <input type="hidden" class="module_id" id="" name="module_id" value="0">
                <input type="hidden" class="main_module" id="" name="main_module" value="Banner">
                <input type="hidden" class="sub_module" id="" name="sub_module" value="">
               <button type="button" class="btn btn-info btn-lg select_choose_media" id="" disabled>Add media</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>      
        </div>
    </div>  -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
