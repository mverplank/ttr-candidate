@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Sponsor Episodes</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Advertising
                        </li>
                        <li>
                            <a href="{{route('sponsors')}}">Sponsors</a>
                        </li>
                        <li class="active">
                            Sponsors Episodes
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="col-lg-12">
                        <div class="panel panel-border panel-pink">
                            <div class="panel-heading" style="padding-bottom:15px;">
                                <h3 class="panel-title">{{$sponsor->firstname." ".$sponsor->lastname." (".$sponsor->company.")"}}</h3>
                            </div>
                        </div>
                    </div>
                    <span style="display:none;" id="sponsor_id">{{$id}}</span>
                    <table id="sponsor_episodes_datatable" class="table table-striped table-bordered">
                        <thead>                       
                            <tr>
                                <th>Date</th>
                                <th>Title</th>
                                <th></th>
                            </tr>
                        </thead>                       
                    </table>
                </div>
            </div>
        </div>
        <!-- end row --> 
    </div> <!-- container -->
</div> <!-- content -->

@endsection
