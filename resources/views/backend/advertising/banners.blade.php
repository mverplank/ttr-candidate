@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Banners</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Advertising
                        </li>
                        <li class="active">
                            Banners
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="all_banners" value="2" name="filterBanners" checked="" onclick="filterBanners(2)" {{ ($is_online == 2) ? "checked" : "" }}>
                    <label for="all_banners"> All </label>
                </div>
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="online_banners" value="1" name="filterBanners" onclick="filterBanners(1)" {{ ($is_online == 1) ? "checked" : "" }}>
                    <label for="online_banners"> Online </label>
                </div> 
                <div class="radio radio-inline">
                    <input type="radio" id="offline_banners" value="0" name="filterBanners" onclick="filterBanners(0)" {{ ($is_online == 0) ? "checked" : "" }}>
                    <label for="offline_banners"> Offline </label>
                </div>
                <div style="float:right;">
                    <a href="{{url('/admin/advertising/banners/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5"  title="Add Banner">     Add Banner
                    </a>
                </div>
            </div>
        </div>     
        <!-- end row --> 
        <!-- Channel Filters --> 
         <div class="row">
            <div class="card-box">
                <div class="form-group row page_top_btns">   
                    <div class="col-md-6">
	                   {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : []), $selected_channel, $attributes=array('id'=>'ChannelFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                   </div>
                   <div class="col-md-6">
                        {{Form::select('data[Banner][f][id]', (!empty($all_category) ? $all_category : []), $selected_category, $attributes=array('id'=>'BannerCategoryId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                   </div>
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="banners_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>URL</th>
                          
                                <th>Clicked</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
