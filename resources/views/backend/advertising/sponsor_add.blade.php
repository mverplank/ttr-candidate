@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Sponsor</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Advertising
                        </li>
                        <li class="active">
                            <a href="{{url('admin/advertising/sponsors')}}">All Sponsors</a>
                        </li>
                        <li class="active">
                            Add Sponsor
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div id="yes_crop"></div>
        <!-- Modal -->
     
        <div class="row">
            <!-- Form Starts-->
            {{ Form::open(array('url' => 'admin/advertising/sponsors/add', 'id'=>'SponsorAdminAddForm')) }}
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <input type="hidden" name="form_type" id="form_type" value="add">
                            <h4 class="header-title m-t-0">Base</h4>
                                <div class="form-group row">
                                    {{Form::label('SponsorIsOnline1', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            {{Form::radio('data[Sponsor][is_online]', '1', true, array('id'=>'SponsorIsOnline1'))}}
                                            {{Form::label('SponsorIsOnline1', 'Online')}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{Form::radio('data[Sponsor][is_online]', '0', false, array('id'=>'SponsorIsOnline0'))}}
                                            {{Form::label('SponsorIsOnline0', 'offline')}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('SponsorIsFeatured', 'Featured', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-purple radio-inline">
                                            {{Form::radio('data[Sponsor][is_featured]', '1', true, array('id'=>'SponsorIsFeatured1'))}}
                                            {{Form::label('SponsorIsFeatured1', 'Yes')}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{Form::radio('data[Sponsor][is_featured]', '0', false, array('id'=>'SponsorIsFeatured0'))}}
                                            {{Form::label('SponsorIsFeatured0', 'No')}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                <!--  <div class="form-group row">
                                    {{Form::label('SponsorTitle', 'Title', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Sponsor][title]', $value = null, $attributes = array('class'=>'form-control required', 'id'=> 'SponsorTitle', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                    </div>
                                </div> --> <!-- end row -->
                                
                                <div class="form-group row">
                                    {{Form::label('SponsorName', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-3">
                                        {{Form::text('data[Sponsor][firstname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255', 'required'=>true,'maxlength' =>'45'))}}
                                    </div>
                                    <div class="col-sm-3">
                                        {{Form::text('data[Sponsor][lastname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255', 'required'=>true,'maxlength' =>'45'))}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('SponsorCompany', 'Company', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Sponsor][company]', $value = null, $attributes = array('class'=>'form-control required', 'id'=> 'SponsorCompany', 'data-parsley-maxlength'=>'255', 'required'=>true))}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('SponsorStartDate', 'Start Date', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        <div class="input-group">                   
                                            {{Form::text('data[Sponsor][start_date]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'mm/dd/yyyy', 'id'=>'SponsorStartDate'))}}
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                </div> <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('SponsorEndDate', 'End Date', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        <div class="input-group">                   
                                            {{Form::text('data[Sponsor][end_date]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'mm/dd/yyyy', 'id'=>'SponsorEndDate'))}}
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                        </div><!-- input-group -->
                                    </div>
                                </div> <!-- end row -->
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('SponsorDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                               <!--  <textarea id="elm1" name="data[Profile][bio]"></textarea> -->
                                                {{ Form::textarea('data[Sponsor][description]', null, ['id' => 'SponsorDescription']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End editor -->
                                <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('SponsorNotes', 'Notes', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{ Form::textarea('data[Sponsor][notes]', null, ['id' => 'SponsorNotes', 'class'=>'form-control','rows'=>3, 'cols'=>50]) }}
                                    </div>
                                </div><!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('ChannelChannelSet', 'Assigned to channels', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        @if(!empty($all_channels))
                                            @foreach ( $all_channels as $i =>$item )
                                            <div class="checkbox checkbox-pink">
                                                {!! Form::checkbox( 'data[Channel][Channel][]', $i, '', ['class' => 'md-check', 'id' => 'ChannelChannel'.$i] ) !!}
                                                {!! Form::label('ChannelChannel'.$i,  $item) !!}
                                            </div>
                                            @endforeach
                                        @else
                                            Not exist any channels.
                                        @endif
                                    </div>
                                </div><!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('HostHost', 'Hosts', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                       {{Form::select('data[Host][Host][]', [],'', $attributes = array('class'=>'form-control HostHost select2-multiple', 'id'=> 'HostHost','list'=>'host-list', 'multiple'=>'multiple','data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose ...'))}}
                                    </div>
                                     <span style="display: none;" class="loading">
                                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                                    </span>                                   
                                </div><!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('HostHost', 'Images', array('class' => 'col-sm-2 form-control-label'))}}
                                    <!-- <div class="col-sm-7">
                                        <div class="padding-left-0 padding-right-0">
                                            <div id="sponsor_image" class="dropzone sponsor_image" data-limit="3" data-image-for="player" data-module="Sponsor" data-preview-ele="#sponsor_image_prev">
                                                <div class="col-sm-9 previewDiv" id="sponsor_image_prev"></div>
                                                <div class="col-sm-3 dz-message d-flex flex-column">
                                                   <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                                    Drag &amp; Drop here or click
                                                </div>
                                            </div>
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                To change order drag and drop thumbanils. Clik on image to edit.
                                            </span>   
                                        </div>
                                    </div> -->
										<div class="col-sm-8">
                                            <div class="row">

                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'player')
                                                        <div class="col-sm-4 adding-medias media_Sponsor_player" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'Sponsor', 'player', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Sponsor_player" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Sponsor" data-submodule="player" ></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        </div>
                                                        @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Sponsor_player_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                               <!--  To change order drag and drop thumbnails. -->
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Sponsor" data-submodule="player" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="1">Upload files</button>
                                            </div> 
                                        </div>
                                </div><!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('SponsorHasManySponsorSocialUrlsJsons', 'Urls and Social Media', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Type</div>
                                                        </th>
                                                        <th>
                                                            <div>Url</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                                        <td>
                                                            {{Form::select('data[Sponsor][social_urls_json][1][type]', $social_media, '', $attributes=array('id'=>'SponsorSocialUrlsJson1Type', 'class'=>'form-control'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::url('data[Sponsor][social_urls_json][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'SponsorSocialUrlsJson1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 delete_sponsor_extra_urls" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="SponsorHasManySponsorSocialUrlsJsonsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End row -->
                            </div> <!-- end card-box -->
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 col-md-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Contact</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('SponsorEmail', 'Email', array('class' => 'col-sm-4 form-control-label req'))}}
                                        <div class="col-sm-7">
                                            {{Form::email('data[Sponsor][email]', $value = null, $attributes = array('class'=>'form-control required', 'id'=> 'SponsorEmail', 'data-parsley-maxlength'=>'255'))}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Sponsor][skype]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'SponsorSkype', 'data-parsley-maxlength'=>'tel'))}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorPhone', 'Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::tel('data[Sponsor][phone]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'SponsorPhone', 'data-parsley-maxlength'=>'45'))}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorCellphone', 'Cellphone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::tel('data[Sponsor][cellphone]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'SponsorCellphone', 'data-parsley-maxlength'=>'45'))}}
                                        </div>
                                    </div><!-- end row -->
                                </div>
                            </div> <!-- end card-box -->
                        </div>
                        <div class="col-sm-6 col-xs-6 col-md-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Address</h4>                         
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('SponsorAddress', 'Address', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::textarea('data[Sponsor][address]', null, ['id' => 'SponsorAddress', 'class'=>'form-control','rows'=>3, 'cols'=>50]) }}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorCity', 'Cellphone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::tel('data[Sponsor][city]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'SponsorCity', 'data-parsley-maxlength'=>'255'))}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorState', 'State', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::tel('data[Sponsor][state]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'SponsorState', 'data-parsley-maxlength'=>'255'))}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorZip', 'Zip', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Sponsor][zip]', $value = null, $attributes = array('class'=>'form-control', 'id'=> 'SponsorZip', 'data-parsley-maxlength'=>'45'))}}
                                        </div>
                                    </div><!-- end row -->
                                </div><!-- end p-20-->
                            </div><!-- end card-box -->
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 col-md-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">PR Contact</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('SponsorContactName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][contact_name]', null, ['id' => 'SponsorContactName', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorContactEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::email('data[Sponsor][contact_email]', null, ['id' => 'SponsorContactEmail', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorContactSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][contact_skype]', null, ['id' => 'SponsorContactSkype', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorContactPhone', 'Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][contact_phone]', null, ['id' => 'SponsorContactPhone', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorContactCellphone', 'Cell Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][contact_cellphone]', null, ['id' => 'SponsorContactCellphone', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6 col-md-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Billing informations</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('SponsorBillingName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][billing_name]', null, ['id' => 'SponsorBillingName', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorBillingEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::email('data[Sponsor][billing_email]', null, ['id' => 'SponsorBillingEmail', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorBillingSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][billing_skype]', null, ['id' => 'SponsorBillingSkype', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorBillingPhone', 'Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][billing_phone]', null, ['id' => 'SponsorBillingPhone', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('SponsorBillingCellphone', 'Cell Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{ Form::text('data[Sponsor][billing_cellphone]', null, ['id' => 'SponsorBillingCellphone', 'class'=>'form-control'])}}
                                        </div>
                                    </div><!-- end row -->
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                    <div class="form-group">
                        <div class="fixed_btn">
                            <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Sponsor][btnAdd]" id="SponsorBtnAdd" data-form-id="SponsorAdminAddForm">
                                Add
                            </button>
                           <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                         
                        </div>
                    </div>
                </div><!-- end row -->
            </div><!-- end col-->
            {{ Form::close() }}
        </div> <!-- end row -->
      <!-- model -->
      @include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "Sponsor"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
