@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Sponsors</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Advertising
                        </li>
                        <li class="active">
                            Sponsors
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->   
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="all_sponsors" value="2" name="filterSponsors" checked="" onclick="filterSponsors(2)" {{ ($is_online == 2) ? "checked" : "" }}>
                    <label for="all_sponsors"> All </label>
                </div>
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="online_sponsors" value="1" name="filterSponsors" onclick="filterSponsors(1)" {{ ($is_online == 1) ? "checked" : "" }}>
                    <label for="online_sponsors"> Online </label>
                </div> 
                <div class="radio radio-inline">
                    <input type="radio" id="offline_sponsors" value="0" name="filterSponsors" onclick="filterSponsors(0)" {{ ($is_online == 0) ? "checked" : "" }}>
                    <label for="offline_sponsors"> Offline </label>
                </div>
                <div style="float:right;">
                    <a href="{{url('/admin/advertising/sponsors/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" title="Add Sponsor">Add Sponsor</a>
                    <a href="javascript:void(0);" id="sponsors_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>
                </div>
            </div>
        </div> 
        <!--  Add sponsors eport form -->
        <div class="row" style="display: none;" id="sponsors_export_option">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="p-20">
                        {{ Form::open(array('url' => 'admin/advertising/sponsors/export','method' => 'get', 'id'=>'SponsorsExportForm')) }}
                            <div class="form-group row"> 
                            {{Form::label('SponsorExportFilter', 'Name', array('class' => 'form-control-label col-sm-1'))}}             
                                <div class="col-sm-10">
                                    {{Form::select('data[Sponsor][export_filter]', array('All'=>'All','Online'=>'Online','Offline'=>'Offline','Featured'=>'Featured','Channels'=>$all_export_channels),'', $attributes=array('id'=>'SponsorExportFilter', 'class'=>'selectpicker m-b-0', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'select'))}} 
                                    <input type="hidden" name="lebel" value="undefined" id="sponsor_eport_lebel"> 
                                    <!-- <input type="hidden" name="selectopn" value="" id="eport_selected_option"> -->                         
                                </div>
                            </div>
                            <div class="form-group row"> 
                                 <div class="col-sm-7 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light submit_form" value="Add" id="SponsorsExportSubmit">Export</button>
                                 </div>                           
                            </div>
                         {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- form end  -->            
        <!-- end row --> 		
        <!-- Channel Filters --> 
        <div class="row">
            <div class="card-box">
                <div class="form-group row page_top_btns">   
                    <div class="col-md-6">
                        {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : ''), $selected_channel, $attributes=array('id'=>'ChannelFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>
                    <div class="col-md-6">
                        {{Form::select('data[Host][f][id]', (!empty($all_hosts) ? $all_hosts : ''), $selected_host, $attributes=array('id'=>'HostSId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                    </div>
                </div>
            </div>
        </div> <!-- end row -->  
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="sponsors_datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Company</th>
                                <th>Online</th>
                                <th>Featured</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
