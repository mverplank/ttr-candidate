@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Channel Schedule</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Channel Schedule
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->  
        <div class="row">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <input type="hidden" name="showlist" class="ShowExtForThisChannel" value="">
                <div class="form-group row page_top_btns">
                    <div class="col-md-4 channels_schedule">
                        @if(empty($channel_id))
                            @if(!empty($get_channel_id))
                                {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : []), $get_channel_id, $attributes=array('id'=>'ChannelSFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                            @else
                                {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : []), '', $attributes=array('id'=>'ChannelSFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                            @endif

                        @else
                            {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : []), $channel_id, $attributes=array('id'=>'ChannelSFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple', 'disabled'))}}                            
                        @endif
                    </div>
                    <div class="col-md-6">   
                    </div>
                    <div class="col-md-2"> 
                       <!--  <div style="float:right;">                       
                            <a href="{{url('/admin/user/users/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">     Add Show
                            </a>    
                        </div>   -->          
                    </div>
                </div>
            </div>
        </div>     
        <!-- end row -->   
        <div class="row">
            <div class="card-box">
                <div class="form-group row">
                    <sapn id="channel_schedule"></sapn>
                    <div id="calendar"></div>
                    <div class="HasNotShows" style="display: none;">
                        <span class="help-block">
                            <i class="fa fa-info-circle" aria-hidden="true" style="color:#3a87ad; font-size: 30px;"></i>
                            <b  style="font-size:20px;"> 
                                No shows defined or assigned for this channel (<span class="channel_name"> </span>).
                                Go to <a href="{!! url('/admin/show/shows'); !!}"> Shows > Shows </a>and add new or assgin existing shows to this channel.
                            </b>
                        </span>
                    </div>
                </div>
            </div>
        </div><!-- End row -->
        <!-- BEGIN MODAL -->
        <div class="modal fade none-border" id="event-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Show - <span id="schedule_channel"></span></h4>
                    </div>
                    <div class="modal-body p-20">
                        {{ Form::open(array('url' => '', 'id'=>'ScheduleAdminAddForm')) }}

                        {{Form::hidden('data[Schedule][channels_id]', '0', ['id'=>'channel_schedule_id'])}}
                        {{Form::hidden('data[Schedule][id]', '0', ['id'=>'schedule_id'])}}
                        {{Form::hidden('data[Schedule][form_type]', 'add', ['id'=>'form_type'])}}                        
                        <!-- <div class="form-group row">
                            {{Form::label('ScheduleStartDate', 'Channels', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{Form::select('data[Schedule][channels][]', $all_channels, '', $attributes=array('id'=>'scheduleChannels', 'class'=>'form-control show_channels', 'multiple'=>'multiple', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                            </div> 
                        </div> -->
                        <!-- End Row -->
                        <div class="form-group row add_multiple_channels">
                            <div class="portlet">
                                <div class="portlet-heading bg-teal">
                                    <div class="portlet-titles col-sm-8">
                                        {{Form::select('data[Schedule][data][1][channels_id]', $all_channels, '', $attributes=array('id'=>'scheduleChannels', 'class'=>'form-control show_channels', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                    </div>
                                    <div class="portlet-widgets">
                                       <!--  <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a> -->
                                        <span class="divider"></span>
                                        <a data-toggle="collapse" class="close_icon_portlet" data-parent="#accordion1" href="#bg-teal"><i class="ion-minus-round"></i></a>
                                        <span class="divider"></span>
                                        <a href="#" data-toggle="remove" class="close_channel" style="visibility:hidden;"><i class="ion-close-round"></i></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div id="bg-teal" class="panel-collapse collapse in">
                                    <div class="portlet-body">
                                        <div class="form-group row">                      
                                            {{Form::label('ScheduleDow', 'Days of week', array('class' => 'col-sm-2 form-control-label'))}}
                                            <div class="col-sm-10">
                                                <div class="button-list">
                                                    <div class="btn-switch btn-switch-custom">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-custom" class="ScheduleDowSun weekdays" value="Sun"/>
                                                        <label for="input-btn-switch-custom"
                                                               class="btn btn-rounded btn-custom waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Sunday</strong>
                                                        </label>
                                                    </div>

                                                    <div class="btn-switch btn-switch-primary">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-primary" class="ScheduleDowMon weekdays" value="Mon"/>
                                                        <label for="input-btn-switch-primary"
                                                               class="btn btn-rounded btn-primary waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Monday</strong>
                                                        </label>
                                                    </div>

                                                    <div class="btn-switch btn-switch-success">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-success" class="ScheduleDowTue weekdays" value="Tue"/>
                                                        <label for="input-btn-switch-success"
                                                               class="btn btn-rounded btn-success waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Tuesday</strong>
                                                        </label>
                                                    </div>

                                                    <div class="btn-switch btn-switch-info">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-info" class="ScheduleDowWed weekdays" value="Wed"/>
                                                        <label for="input-btn-switch-info"
                                                               class="btn btn-rounded btn-info waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Wednesday</strong>
                                                        </label>
                                                    </div>

                                                    <div class="btn-switch btn-switch-warning">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-warning" class="ScheduleDowThu weekdays" value="Thu"/>
                                                        <label for="input-btn-switch-warning"
                                                               class="btn btn-rounded btn-warning waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Thursday</strong>
                                                        </label>
                                                    </div>

                                                    <div class="btn-switch btn-switch-pink">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-pink" class="ScheduleDowFri weekdays" value="Fri"/>
                                                        <label for="input-btn-switch-pink"
                                                               class="btn btn-rounded btn-pink waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Friday</strong>
                                                        </label>
                                                    </div>

                                                    <div class="btn-switch btn-switch-inverse">
                                                        <input type="checkbox" name="data[Schedule][data][1][dow][]" id="input-btn-switch-inverse" class="ScheduleDowSat weekdays" value="Sat"/>
                                                        <label for="input-btn-switch-inverse"
                                                               class="btn btn-rounded btn-inverse waves-effect waves-light">
                                                            <em class="glyphicon glyphicon-ok"></em>
                                                            <strong> Saturday</strong>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                        <div class="form-group row">
                                            {{Form::label('ScheduleStartDate', 'Start Date', array('class' => 'col-sm-2 form-control-label'))}}
                                            <div class="col-sm-4">
                                                <div class="input-group">        
                                                    {{Form::text('data[Schedule][data][1][start_date]', $value = null, $attributes = array('class'=>'form-control required scheduleStartDate', 'placeholder'=>'mm/dd/yyyy', 'id'=>'ScheduleStartDate'))}}
                                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>  
                                                </div>
                                            </div>
                                            {{Form::label('ScheduleEndDate', 'End Date', array('class' => 'col-sm-2 form-control-label'))}}
                                            <div class="col-sm-4">
                                                <div class="input-group">        
                                                    {{Form::text('data[Schedule][data][1][end_date]', $value = null, $attributes = array('class'=>'form-control required scheduleEndDate', 'placeholder'=>'mm/dd/yyyy', 'id'=>'ScheduleEndDate'))}}      
                                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>  
                                                </div>                        
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                        <div class="form-group row">
                                            {{Form::label('ScheduleTime', 'Time', array('class' => 'col-sm-2 form-control-label'))}}
                                            <div class="col-sm-4">
                                                <div class="input-group">                  
                                                    {{Form::text('data[Schedule][data][1][time]', $value = null, $attributes = array('class'=>'form-control scheduleTime required', 'placeholder'=>'HH:mm am', 'id'=>'ScheduleTime'))}}
                                                    <span class="input-group-addon"><i class="mdi mdi-clock"></i></span> 
                                                </div>                        
                                            </div>
                                            {{Form::label('ScheduleDuration', 'Duration', array('class' => 'col-sm-2 form-control-label'))}}
                                            <div class="col-sm-4">
                                                {{Form::select('data[Schedule][data][1][duration]', $minutes, 60, $attributes=array('id'=>'ScheduleDuration', 'class'=>'selectpicker scheduleDuration', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                        <div class="form-group row">
                                            {{Form::label('ScheduleWeek', 'Repeat', array('class' => 'col-sm-2 form-control-label'))}}
                                            <div class="col-sm-4">                          
                                                {{Form::select('data[Schedule][data][1][week][]', $weeks, ['0'], $attributes=array('id'=>'ScheduleWeek', 'class'=>'scheduleWeek', 'multiple'=>'multiple', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                            </div> 
                                            <div class="col-sm-3">
                                                <div class="input-group">           
                                                    {{Form::select('data[Schedule][data][1][month]', $months, '', $attributes=array('id'=>'ScheduleMonth', 'class'=>'selectpicker scheduleMonth', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                                </div>   
                                            </div> 
                                           
                                            <div class="col-sm-3">
                                                <div class="input-group">           
                                                    {{Form::select('data[Schedule][data][1][year]', $years, '', $attributes=array('id'=>'ScheduleYear', 'class'=>'selectpicker scheduleYear', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                                </div>   
                                            </div>   
                                        </div>
                                        <!-- End Row -->
                                        <div class="form-group row">
                                            {{Form::label('ScheduleTypeLabel', 'Type of show', array('class' => 'col-sm-3 form-control-label'))}}
                                            <div class="col-sm-9">
                                                <div class="radio radio-info radio-inline">
                                                    {{Form::radio('data[Schedule][data][1][type_label]', 'live', true, array('id'=>'ScheduleTypeLabelLive','value'=>'live', 'class'=>'scheduleTypeLabel'))}}
                                                    {{Form::label('ScheduleTypeLabelLive', 'Live', array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                                <div class="radio radio radio-inline">
                                                    {{Form::radio('data[Schedule][data][1][type_label]', 'replay', false, array('id'=>'ScheduleTypeLabelReplay','value'=>'replay', 'class'=>'scheduleTypeLabel'))}}
                                                   {{Form::label('ScheduleTypeLabelReplay', 'Replay', array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                        <div class="form-group row">
                                            {{Form::label('ScheduleTypeLabel', 'Type of show source', array('class' => 'col-sm-3 form-control-label'))}}
                                            <div class="col-sm-9">
                                                <div class="radio radio-info radio-inline">
                                                    {{Form::radio('data[Schedule][data][1][type]', 'file_or_stream', true, array('id'=>'ScheduleTypeFileOrStream','value'=>'file_or_stream', 'class'=>'scheduleType'))}}
                                                    {{Form::label('ScheduleTypeFileOrStream', 'File or Stream', array('class' => 'col-sm-12 form-control-label'))}}
                                                </div>
                                                <div class="radio radio-warning radio-inline">
                                                    {{Form::radio('data[Schedule][data][1][type]', 'live', false, array('id'=>'ScheduleTypeLive','value'=>'live', 'class'=>'scheduleType'))}}
                                                   {{Form::label('ScheduleTypeLive', 'Live', array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                                <div class="radio radio radio-inline">
                                                    {{Form::radio('data[Schedule][data][1][type]', 'replay', false, array('id'=>'ScheduleTypeReplay','value'=>'replay', 'class'=>'scheduleType'))}}
                                                   {{Form::label('ScheduleTypeReplay', 'Replay', array('class' => 'col-sm-4 form-control-label'))}}
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- End Row -->  
                                        <div class="form-group row schedule_shows">
                                            {{Form::label('ScheduleShowsId', 'Show', array('class' => 'col-sm-3 form-control-label'))}}
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    {{Form::select('data[Schedule][data][1][shows_id]', [0 => 'None'], '', $attributes=array('id'=>'ScheduleShowsId', 'class'=>'selectpicker scheduleShowsId form_ui_input', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                                    <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> Only shows of current channel are shown.
                                                    To assign show to channel go to: Shows » Edit Show » Settings / Channels section.</span>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- End Row -->
                                        <div class="form-group row schedule_replays">
                                            {{Form::label('ScheduleSchedulesChannelsId', 'Replay of Show from', array('class' => 'col-sm-3 form-control-label'))}}
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    {{Form::select('data[Schedule][data][1][schedules_channels_id]', (!empty($all_channels) ? $all_channels : [0=>'None']), '', $attributes=array('id'=>'ScheduleSchedulesChannelsId', 'class'=>'selectpicker scheduleSchedulesChannelsId form_ui_input', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                                </div>
                                                <div class="form-group">
                                                    {{Form::select('data[Schedule][data][1][schedules_id]', [0 => 'None'], '', $attributes=array('id'=>'ScheduleSchedulesId', 'class'=>'selectpicker form_ui_input scheduleSchedulesId', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                                    <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Listed shows are shows scheduled for this week. Show names may vary depending week (in case when schedule entry for given day of week and time has different shows for different weeks assigned).</span>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- End Row -->
                                        <hr>
                                        <h4 class="header-title m-t-0">Media</h4>
                                        <div class="form-group row">
                                            {{Form::label('ScheduleStream', 'Remote file or stream url', array('class' => 'col-sm-3 form-control-label'))}}
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    {{Form::text('data[Schedule][data][1][stream]', '', ['id'=>'domain', 'class'=>'form-control schedule_stream form_ui_input required', 'data-validation' => '1', 'data-maxlength' => '255', 'data-unique' => '1', 'model' => 'Channel'])}}
                                                    <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> Full URL of remote static media file or stream.
                                                    If empty, file don't exists or can't connect to stream - below defined local file is used.
                                                    Examples:
                                                    static file: "custom-name.mp3" or "http://channel-domain.com/files/Shows/custom-name.mp3"
                                                    live stream: "http://205.188.234.38:8002"</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="mtf-buttons" style="clear:both;float:right;">
                            <button type="button" class="btn btn-teal btn-rounded w-md waves-effect waves-light m-b-5" id="showAddMoreChannels"><i class="glyphicon glyphicon-plus"></i> <span> add</span>
                            </button>
                        </div>  
                    <!-- End Row -->
                        
                        
                        <!-- <div class="form-group row">
                            {{Form::label('ScheduleEndDate', 'End Date', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-9">
                                <div class="input-group">        
                                    {{Form::text('data[Schedule][end_date]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'mm/dd/yyyy', 'id'=>'ScheduleEndDate'))}}      
                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>  
                                </div>                        
                            </div>
                        </div> -->
                        <!-- End Row -->
                                            
                        
                        
                        <!-- <div class="form-group row">
                            {{Form::label('ScheduleDuration', 'Duration', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-9">
                                {{Form::select('data[Schedule][duration]', $minutes, 60, $attributes=array('id'=>'ScheduleDuration', 'class'=>'selectpicker', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                            </div>
                        </div> -->
                        <!-- End Row -->
                        
                        {{ Form::close() }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success save-event waves-effect waves-light" id="scheduleSaveBtn">Add</button>
                        <button type="button" class="btn btn-danger delete-event waves-effect waves-light">Delete</button>
                    </div>
                </div>
            </div>
        </div><!-- End Modal -->
        <!-- BEGIN MODAL -->
        <div class="modal fade none-border" id="multiple-events-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body p-20">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                       <!--  <button type="button" class="btn btn-success save-event waves-effect waves-light" id="openScheduleBtn" data-schedule_id="">Add Show</button> -->
                    </div>
                </div>
            </div>
        </div><!-- End Modal -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
