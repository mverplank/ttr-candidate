@extends('backend.layouts.main')

@section('content')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Channel</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="{{url('admin/channel/channels')}}">Channels</a>
                        </li>
                        <li class="active">
                            Edit Channels
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div class="col-xs-12">              
                <div class="card-box">
                    <!-- Form Starts-->
                    {!! Form::model($channel, ['url' => 'admin/channel/channels/update/'.$channel->id, 'files' => true, 'id' => 'update_channel']) !!}
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Base</h4>
                                <input type="hidden" name="form_type" id="form_type" value="edit">
                                <input type="hidden" name="edit_id" id="edit_id" value="{{ $channel->id }}">

                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('name', 'Name', array('class' => 'col-sm-3 form-control-label req'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][name]',$channel->name, ['id'=>'name', 'class'=>'form-control form_ui_input required', 'data-validation' => '1'])}}
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        {{Form::label('domain name', 'Domain name', array('class' => 'col-sm-3 form-control-label req'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][domain]',$channel->domain, ['id'=>'domain', 'class'=>'form-control form_ui_input required', 'data-validation' => '1', 'data-maxlength' => '255', 'data-unique' => '1', 'model' => 'Channel'])}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> Only domain name without protocol and www prefix (no "http://www"); ex. google.com</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                       {{Form::label('ChannelThemeName', 'Theme', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-6">
                                          {{Form::select('data[Channel][theme_name]',$themes, '', $attributes=array('id'=>'ChannelThemeName','class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('status', 'Status', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                            {{ Form::checkbox('data[Channel][is_online_radio]', $channel->is_online_radio, true, ['id' => 'switch3', 'data-switch' => 'bool']) }}
                                                <label for="switch3" data-on-label="Online" data-off-label="Offline"  data-size="large"></label class="checkbox_button">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('missing episode', 'Missing episodes reminder', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                               {{ Form::checkbox('data[Channel][is_missing_episodes_reminder]', $channel->is_missing_episodes_reminder, true, ['id' => 'switch4', 'data-switch' => 'bool']) }}
                                                <label for="switch4" data-on-label="On" data-off-label="Off" class="checkbox_button"></label>
                                                
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> If enabled each Sunday evenig send email remander to all hosts which has not entered episodes for current week</span>
                                            </div>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        {{Form::label('stream url', 'Live Stream URL', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][stream]', $channel->stream, ['id'=>'stream', 'class'=>'form-control form_ui_input'])}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('archive prefix', 'Archives URL prefix', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][url_archived_episodes]',$channel->url_archived_episodes, ['id'=>'url_archived_episodes', 'class'=>'form-control form_ui_input'])}}
                                            </div>
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        {{Form::label('description', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{Form::textarea('data[Channel][description]',$channel->description, ['rows' => 4, 'cols' => 54, 'id'=>'ChannelDescription', 'class'=>'form-control form_ui_textarea'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
                         <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">SEO</h4>
                               
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][seo_title]',$channel->seo_title, ['id'=>'seo_title', 'class'=>'form-control form_ui_input', 'maxlength' => 255])}}
                                                 
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> HTML HEAD &lt;title&gt; tag value; page title shown on browser</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('keywords', 'Keywords', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6 tag-input-type">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][seo_keywords]',$channel->seo_keywords, ['id'=>'ChannelSeoKeywords', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                                 
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('description', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][seo_description]',$channel->seo_description, ['id'=>'ChannelSeoDescription', 'class'=>'form-control form_ui_input'])}}
                                                 
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> HTML HEAD meta description for search engines.
                                                Recommended length: aprox. 160 characters.</span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="form-group row">
                                        {{Form::label('banner', 'Banner 500x200 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                       <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'player')
                                                            <div class="col-sm-4 adding-medias media_Channel_player" data-off="{{$url_count}}"><div class="delete_Channel_player"onclick="mediaLinkDelete({{$med->id}}, this);" style="display:block;font-size:20px;"><i class="typcn typcn-delete delete_media_icon" title="Delete"></i></div><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_player_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> This banner will be shown on player when current show banner is not defined or currently playing media/stream is not defined on the channel schedule (set of queued adverts for example). First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-submodule="player" data-media_type="image" data-limit="3" data-dimension="500x200">Upload files</button>
                                            </div> 
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner 500x200 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'player')
                                                    <div class="col-sm-4 adding-medias media_Channel_player" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                                     
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">                                             
                                                                                <ul class="list-inline pull-right"> 
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_player" onclick="mediaLinkDelete({{$med->id}}, this);"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                        @php ($url_count++)
                                                        @endif
                                                    @endforeach                        
                                                @endif
                                                <div class="media_Channel_player_outer hidden" data-off="0" style="display: none;">
                                                </div>                                
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> This banner will be shown on player when current show banner is not defined or currently playing media/stream is not defined on the channel schedule (set of queued adverts for example). First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Channel" data-submodule="player" data-media_type="image" data-limit="3" data-dimension="500x200" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        {{Form::label('banner', 'Banner 170x300 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                       <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'zapbox')
                                                            <div class="col-sm-4 adding-medias media_Channel_zapbox" data-off="{{$url_count}}"><div class="delete_Channel_zapbox"onclick="mediaLinkDelete({{$med->id}}, this);" style="display:block;font-size:20px;"><i class="typcn typcn-delete delete_media_icon" title="Delete"></i></div><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_zapbox_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-submodule="zapbox" data-media_type="image" data-limit="3" data-dimension="170x300">Upload files</button>
                                            </div> 
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner 170x300 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'zapbox')
                                                        <div class="col-sm-4 adding-medias media_Channel_zapbox" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                                     
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">                                             
                                                                                    <ul class="list-inline pull-right"> 
                                                                                        <li>
                                                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_zapbox" onclick="mediaLinkDelete({{$med->id}}, this);"></a>
                                                                                        </li>                           
                                                                                    </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_zapbox_outer hidden" data-off="0" style="display: none;"></div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Channel" data-submodule="zapbox" data-media_type="image" data-limit="3" data-dimension="170x300" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        {{Form::label('banner', 'Banner 170x137 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'zapboxsmall')
                                                        <div class="col-sm-4 adding-medias media_Channel_zapboxsmall" data-off="{{$url_count}}"><div class="delete_Channel_zapboxsmall"onclick="mediaLinkDelete({{$med->id}}, this);" style="display:block;font-size:20px;"><i class="typcn typcn-delete delete_media_icon" title="Delete"></i></div><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></div>
                                                        @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_zapboxsmall_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                                <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-submodule="zapboxsmall" data-media_type="image" data-limit="3" data-dimension="170x137">Upload files</button>
                                            </div> 
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner 170x137 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                                         <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'zapboxsmall')
                                                    <div class="col-sm-4 adding-medias media_Channel_zapboxsmall" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                                     
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">                                             
                                                                                <ul class="list-inline pull-right"> 
                                                                                    <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_zapboxsmall" onclick="mediaLinkDelete({{$med->id}}, this);"></a>
                                                                                    </li>                           
                                                                                </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                    </div>
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_zapboxsmall_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Channel" data-submodule="zapboxsmall" data-media_type="image" data-limit="3" data-dimension="170x137" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        {{Form::label('banner', 'Banner on widget player', array('class' => 'col-sm-2 form-control-label'))}}
                                         <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'widget')
                                                            <div class="col-sm-4 adding-medias media_Channel_widget" data-off="{{$url_count}}"><div class="delete_Channel_widget"onclick="mediaLinkDelete({{$med->id}}, this);" style="display:block;font-size:20px;"><i class="typcn typcn-delete delete_media_icon" title="Delete"></i></div><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_widget_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-submodule="widget" data-media_type="image" data-limit="3" data-dimension="">Upload files</button>
                                            </div> 
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        {{Form::label('banner', 'Banner on widget player', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'widget')
                                                        <div class="col-sm-4 adding-medias media_Channel_widget" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                                     
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">                                             
                                                                                    <ul class="list-inline pull-right"> 
                                                                                        <li>
                                                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_widget" onclick="mediaLinkDelete({{$med->id}}, this);"></a>
                                                                                        </li>                           
                                                                                    </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_widget_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                First banner will be used.
                                               <span style="color:#f9c851;"> You can upload only 3 files.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Channel" data-submodule="widget" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">RSS Channel</h4>                               
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ChannelRssFeedUrl', 'RSS feed url', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <a href="{{url('feed/episodes/archived.rss')}}" target="_blank">{{url('feed/episodes/archived.rss')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        {{Form::label('cover_image', 'Cover image', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'cover')
                                                            <div class="col-sm-4 adding-medias media_Channel_cover" data-off="{{$url_count}}"><div class="delete_Channel_cover"onclick="mediaLinkDelete({{$med->id}}, this);" style="display:block;font-size:20px;"><i class="typcn typcn-delete delete_media_icon" title="Delete"></i></div><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach                      
                                                @endif
                                                <div class="media_Channel_cover_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Only jpg/jpeg/png/gif files. Maximum file size: 120MB.
                                               <span style="color:#f9c851;"> You can upload only 1 file.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-submodule="cover" data-media_type="image" data-limit="1" data-dimension="">Upload files</button>
                                            </div> 
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        {{Form::label('cover_image', 'Cover image', array('class' => 'col-sm-2 form-control-label'))}}
                                        <div class="col-sm-8">
                                            <div class="row">
                                                @if($media->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($media as $med)
                                                        @if($med->sub_module == 'cover')
                                                        <div class="col-sm-4 adding-medias media_Channel_cover" data-off="{{$url_count}}">
                                                            <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <li class="jFiler-item" data-jfiler-index="1">          
                                                                        <div class="jFiler-item-container">                       
                                                                            <div class="jFiler-item-inner">                           
                                                                                <div class="jFiler-item-thumb">    
                                                                                    <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">                                     
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                    </a>  
                                                                                </div>                                    
                                                                                <div class="jFiler-item-assets jFiler-row">                                             
                                                                                    <ul class="list-inline pull-right"> 
                                                                                        <li>
                                                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_cover" onclick="mediaLinkDelete({{$med->id}}, this);"></a>
                                                                                        </li>                           
                                                                                    </ul>                                    
                                                                                </div>                                
                                                                            </div>                            
                                                                        </div>                        
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                            @php ($url_count++)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="media_Channel_cover_outer hidden" data-off="0" style="display: none;">
                                                    </div>
                                            </div>
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Only jpg/jpeg/png/gif files. Maximum file size: 120MB.
                                                <span style="color:#f9c851;"> You can upload only 1 file.</span>
                                            </span>
                                        </div>  
                                        <div class="col-sm-2">
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Channel" data-submodule="cover" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">SAM Broadcaster</h4>
                               
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('show file', 'Show files directory', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{Form::text('data[Channel][sam_dir_shows]', $channel->sam_dir_shows,  ['id'=>'ChannelSamDirShows', 'class'=>'form-control form_ui_input'])}}
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        {{Form::label('min duration', 'Minimum show duration [min]', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {{Form::select('data[Channel][sam_min_show_duration]',['5' => 5, '10' => 10, '15' => 15, '20' => 20, '25' => 25, '30' => 30, '45' => 45, '60' => 60], $channel->sam_min_show_duration, ['id'=>'ChannelSamMinShowDuration', 'class'=>'form-control form_ui_select'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- End row -->
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Studio App Settings
                                </h4>                                   
                                <div class="p-20">
                                    <div class="form-group row ">
                                        {{Form::label('ChannelStudioappEnabled', 'Enabled', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7">
                                          
                                            <div class="radio radio-info radio-inline">
                                                {{Form::radio('data[Channel][studioapp_enabled]', 1, $channel->studioapp_enabled == 1 ? true : false, array('id'=>'ChannelStudioappEnabled1'))}}
                                                {{Form::label('ChannelStudioappEnabled1', 'Yes', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                            <div class="radio radio-inline">
                                                {{Form::radio('data[Channel][studioapp_enabled]', 0,  $channel->studioapp_enabled == 0 ? true : false, array('id'=>'ChannelStudioappEnabled0'))}}
                                                {{Form::label('ChannelStudioappEnabled0', 'No', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>

                                        </div>
                                    </div><!-- end row -->       
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonIcecastProtocol', 'Protocol', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7">
                                            <div class="form-group col-sm-4 tag-input-type">
                                                {{Form::select('data[Channel][studioapp_settings_json][icecast][protocol]', ['http'=>'http', 'https'=>'https'], !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->icecast->protocol : '', $attributes=array('id'=>'ChannelStudioappSettingsJsonIcecastProtocol', 'class'=>'form-control'))}}
                                                <span class="help-block"> 
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                    Protocol
                                                </span>  
                                            </div>   
                                        </div>
                                    </div><!-- end row -->   
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonIcecastHost', 'Host name', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7 tag-input-type">
                                            {{Form::text('data[Channel][studioapp_settings_json][icecast][host]', !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->icecast->host : '',  $attributes = array('class'=>'form-control', 'id'=>'ChannelStudioappSettingsJsonIcecastHost'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast hostname (ex. google.com) or IP number (ex. 453.345.13.234)
                                            </span> 
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonIcecastPort', 'Port number', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-3 tag-input-type">
                                            {{Form::number('data[Channel][studioapp_settings_json][icecast][port]', !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->icecast->port : 8000,  $attributes = array('class'=>'form-control', 'id'=>'ChannelStudioappSettingsJsonIcecastPort'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                               Icecast port number (ex. 8000)
                                            </span> 
                                            {{Form::hidden('data[Channel][studioapp_settings_json][icecast][port_params][min]',0)}}
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonIcecastMountpoint', 'Mount point', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7 tag-input-type">
                                            {{Form::text('data[Channel][studioapp_settings_json][icecast][mountpoint]', !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->icecast->mountpoint : '',  $attributes = array('class'=>'form-control', 'id'=>'UserSettingStudioappSettingsJsonIcecastMountpoint'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast mountpoint (ex. stream; without slash)
                                            </span> 
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonIcecastUser', 'User', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7 tag-input-type">
                                            {{Form::text('data[Channel][studioapp_settings_json][icecast][user]', !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->icecast->user : '',  $attributes = array('class'=>'form-control', 'id'=>'ChannelStudioappSettingsJsonIcecastUser'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast username
                                            </span> 
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonIcecastPassword', 'Password', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7 tag-input-type">
                                            {{Form::text('data[Channel][studioapp_settings_json][icecast][password]', !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->icecast->password : '',  $attributes = array('class'=>'form-control', 'id'=>'ChannelStudioappSettingsJsonIcecastPassword'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast password
                                            </span> 
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonStreamEncoderFormat', 'Format', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7 tag-input-type">
                                            {{Form::select('data[Channel][studioapp_settings_json][streamEncoder][format]', ['mp3'=>'mp3', 'aac'=>'aac'], !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->streamEncoder->format : '', $attributes=array('id'=>'ChannelStudioappSettingsJsonStreamEncoderFormat', 'class'=>'form-control'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast input audio stream format
                                            </span>  
                                        </div>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ChannelStudioappSettingsJsonStreamEncoderBitrate', 'Bitrate', array('class' => 'col-sm-3 form-control-label'))}}
                                        <div class="col-sm-7 tag-input-type">
                                            {{Form::select('data[Channel][studioapp_settings_json][streamEncoder][bitrate]', $bitrate, !empty($channel->studioapp_settings_json) ? $channel->studioapp_settings_json->streamEncoder->bitrate : '', $attributes=array('id'=>'ChannelStudioappSettingsJsonStreamEncoderBitrate', 'class'=>'form-control'))}}
                                            <span class="help-block"> 
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                Icecast bitrate
                                            </span>  
                                        </div>
                                    </div><!-- end row -->
                                </div>
                            </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group">
                            <div class="fixed_btn">
                                <button type="button" data-form-id="update_channel" class="btn btn-primary waves-effect waves-light submit_form" name="data[User][btnAdd]"> Update
                                </button>
                            </div>
                        </div>
                    {{ Form::close() }}
                    <!-- end row -->
                </div> <!-- end ard-box -->
            </div><!-- end col-->
        </div>
        <!-- end row -->
    </div> <!-- container -->
        <!-- Media Library Modal  -->
        @include('backend.cms.upload_media_popup',['module_id' =>$channel->id,'main_module' => "Channel"])
<!--     <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog dialog-width">   
          <div class="modal-content">
            <div class="">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Featured Image</h4>
            </div>
                <ul class="nav nav-tabs">
                    <li class=" ">
                        <a href="#MediaFiles" data-toggle="tab" aria-expanded="true" id="MediaFilesUrl">
                            <span class="visible-xs"><i class="fa fa-user"></i></span>
                            <span class="hidden-xs">Media Library</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#UploadFiles" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-home"></i></span>
                            <span class="hidden-xs">Upload Files</span>
                        </a>
                    </li>
                </ul>   
                <div class="tab-content">
                    <div class="tab-pane active" id="UploadFiles">
                       <div class="form-group row">
                            <div class="col-sm-9">
                                <div class="padding-left-0 padding-right-0">
                                    <div id="banner_image" class="dropzone channel_image" data-limit="3" data-image-for="player" data-module="Channel" data-preview-ele="#banner_prev">
                                        <div class="col-sm-9 previewDiv" id="banner_prev"></div>
                                        <div class="col-sm-3 dz-message d-flex flex-column">
                                           <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                            Drag &amp; Drop here or click
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="MediaFiles">
                        <div class="row">
                            <div class="col-lg-8" style="border-right:1px solid #e4e3e3;">
                                <div class="">
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="images" value="0" name="filterMedia" onclick="getMedia('image','')" checked>
                                        <label for="images"> Images </label>
                                    </div>
                                    <div class="radio radio-pink radio-inline">
                                        <input type="radio" id="audios" value="1" name="filterMedia" onclick="getMedia('image','')" >
                                        <label for="audios"> Audios </label>
                                    </div> 
                                    <div class="radio radio-purple radio-inline">
                                        <input type="radio" id="videos" value="2" name="filterMedia" onclick="getMedia('image','')" >
                                        <label for="videos"> Videos </label>
                                    </div> 
                                    <div class="radio radio-purple radio-inline">
                                       <select id ='mediaDimension' name='mediaDimension' class="form-control">
                                          <option value="">Size</option>
                                          <option value="500x200">500x200</option>
                                          <option value="170x300">170x300</option>
                                          <option value="170x137">170x137</option>
                                        </select>
                                    </div> 
                                    <ul class="grid row" id="mediagrid" style="overflow-y: scroll; height:650px;">
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div id="AttachmentDetail">
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>                          
            <div class="modal-footer">
                <input type="hidden" class="module_id" id="" name="module_id" value="{{$channel->id}}">
                <input type="hidden" class="main_module" id="" name="main_module" value="Channel">
                <input type="hidden" id="" class="sub_module" name="sub_module" value="">
                <button type="button" class="btn btn-info btn-lg select_choose_media" id="" disabled>Add media</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
    </div> --> 
</div> <!-- content -->

@endsection
