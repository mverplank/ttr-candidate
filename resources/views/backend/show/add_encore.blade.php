@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Encore</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li>
                            <a href="{{url('admin/show/schedules/agenda')}}">Episode</a>
                        </li>
                        <li class="active">
                            View Encore
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row --> 
        <div class="row">
            <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Conscious Business Radio</h4>
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">  
                	{{ Form::open(array('url' => 'admin/show/episodes/add_encore/'.$showSheduleId.'/'.$date.'/'.$episode->id, 'method' => 'PUT', 'id'=>'EpisodeAdminAddEncoreForm')) }}
                    <div class="p-20">                                      
                        <div class="form-group row">
                            {{Form::hidden('episode_id', $episode->id, ['id'=>'episode_id'])}}
                            {{Form::label('EpisodeD', 'Encore Date', array('class' => 'col-sm-3 form-control-label'))}}
                          	<div class="col-md-2">
                                @php ($day_name='')
                                @if($schedule->dow == 'Sun')
                                    @php ($day_name = 'Sunday')
                                @elseif($schedule->dow == 'Mon')
                                    @php ($day_name = 'Monday')
                                @elseif($schedule->dow == 'Tue')
                                    @php ($day_name = 'Tuesday')
                                @elseif($schedule->dow == 'Wed')
                                    @php ($day_name = 'Wednesday')
                                @elseif($schedule->dow == 'Thu')
                                    @php ($day_name = 'Thursday')
                                @elseif($schedule->dow == 'Fri')
                                    @php ($day_name = 'Friday')
                                @elseif($schedule->dow == 'Sat')
                                    @php ($day_name = 'Saturday')
                                @endif
                                {{Form::text('data[Schedule][day]', $day_name, $attributes = array('class'=>'form-control', 'id'=>'EpisodeDayView', 'readonly'=>''))}}
                            </div>
                            <div class="col-md-2">
                            	{{Form::text('data[Schedule][date]', Carbon::parse($date)->format('m/d/Y'), $attributes = array('class'=>'form-control', 'id'=>'EpisodeDateView', 'readonly'=>''))}} 
                            </div>   
                            <div class="col-md-2">
                                {{Form::text('data[Schedule][time]', Carbon::parse($schedule->time)->format('g:i a'), $attributes = array('class'=>'form-control', 'id'=>'EpisodeDateView', 'readonly'=>''))}} 
                            </div>       
                            <!-- Schedule ID of encore -->
                            {{Form::hidden('data[Schedule][schedules_id]', $showSheduleId, $attributes = array('class'=>'form-control'))}}

                            <!-- Show ID of encore -->
                            {{Form::hidden('data[Schedule][shows_id]', $schedule->shows_id, $attributes = array('class'=>'form-control'))}}

                        </div><!-- end row -->
                        <div class="form-group row">
                            {{Form::hidden('episode_id', $episode->id, ['id'=>'episode_id'])}}
                            {{Form::label('EpisodeTitle', 'Encore Title', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-7">   
                               {{ Form::textarea('data[Episode][title]',"Encore:$episode->title", ['id' => 'EpisodeTitle', 'class'=>'form-control','rows'=>'2', 'cols'=>'30','data-forma' => "1",'data-forma-def'=>"1", 'data-type'=>"textarea"]) }}                                
                            </div>
                        </div><!-- end row -->
                        <!-- Editor for Bio-->
                        <div class="form-group row">
                            {{Form::label('Encore Description', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-7">
                                <div class="card-box">
                                    {{ Form::textarea('data[Episode][description]', $episode->title, ['id' => 'EpisodeDescription']) }}
                                </div>
                            </div>
                        </div><!-- end row -->
                        
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Episode ID / Date', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-2">   
                               {{Form::text('data[Episode][episode_id]',$episode->id, $attributes = array('class'=>'col-sm-2 form-control', 'id'=>'EpisodeDayView', 'readonly'=>''))}}                             
                            </div>
                            <div class="col-sm-2">   
                               {{Form::text('data[Episode][date]', Carbon::parse($episode->date)->format('m/d/Y'), $attributes = array('class'=>'col-sm-2 form-control', 'id'=>'EpisodeDayView', 'readonly'=>''))}}                             
                            </div>
                            <div class="col-sm-2">   
                               {{Form::text('data[Episode][time]', Carbon::parse($episode->time)->format('g:i a'), $attributes = array('class'=>'col-sm-2 form-control', 'id'=>'EpisodeDayView', 'readonly'=>''))}}                             
                            </div>
                        </div><!-- end row -->
                                              
                        @if($episode->show->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeShow', 'Show', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {{$episode->show->name}}          
                            </div>
                        </div><!-- end row -->
                        @endif
                        
                        <div class="form-group row">
                            {{Form::label('EpisodeTitle', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {{$episode->title}}
                            </div>
                        </div>
                        <!-- end row -->
                        @if(!empty($episode->description))
                        <div class="form-group row">
                            {{Form::label('EpisodeDescription', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->description!!}
                            </div>
                        </div><!-- end row -->
                        @endif
                        
                        <div class="form-group row">
                            {{Form::label('EpisodeSegment1', 'Segment 1', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8"> 
                                <div>
                                   <div>
                                        @if(!empty($episode->segment1))
                                            {!!$episode->segment1!!}
                                        @endif
                                    </div>
                                     <div>
                                        @if(!empty($episode->segment2))
                                            {!!$episode->segment1!!}
                                        @endif
                                    </div>
                                    <div>
                                        @if(!empty($episode->segment3))
                                            {!!$episode->segment1!!}
                                        @endif
                                    </div>
                                     <div>
                                        @if(!empty($episode->segment4))
                                            {!!$episode->segment1!!}
                                        @endif
                                    </div>
                                </div>   
                            </div>
                        </div>  
                        <!-- End Row-->
                        <!-- Talking Points -->
                        @if(!empty($episode->talking_points))
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Talking Points', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->talking_points!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Event Promote -->
                        @if(!empty($episode->special_offers))
                        <div class="form-group row">
                            {{Form::label('EpisodeEventsPromote', 'Events Promote', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->events_promote!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Special Offers -->
                        @if(!empty($episode->special_offers))
                        <div class="form-group row">
                            {{Form::label('EpisodeSpecialOffers', 'Special Offers', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->special_offers!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Listener Interaction -->
                        @if(!empty($episode->listener_interaction))
                        <div class="form-group row">
                            {{Form::label('EpisodeListenerInteraction', 'Listener Interaction', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->listener_interaction!!}
                            </div>
                        </div>
                        @endif
                        
                        <!-- End Row-->
                        @if($episode->episodes_has_categories->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Categories', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->episodes_has_categories as $cat)
                                    @if($episode->episodes_has_categories->count() > 0)
                                        <button class="btn btn-purple disabled m-b-5">{{$cat->category->name}}</button>                 
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <?php //echo "<pre>";print_R($episode->tags());?>
                        @if($episode->tags->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Tags', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->tags as $tag)
                                    <button class="btn btn-teal disabled m-b-5">{{$tag->name}}</button>             
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            {{Form::label('EpisodeSponsors', 'Sponsors', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                @if($episode->sponsors->count() > 0)
                                    @foreach($episode->sponsors as $sponsor) 
                                    {{$sponsor->name}}
                                    @endforeach
                                @endif                              
                            </div>
                        </div>
                        <!-- End Row-->
                        @if($episode->hosts->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Hosts', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->hosts as $host)
                                <div class="panel panel-border panel-info">
                                    <div class="panel-heading archive_page">
                                        @if($host->user->count() > 0)
                                            @if($host->user->profiles->count() > 0)
                                                @foreach($host->user->profiles as $profile)
                                                    <div class="lft_profile_host">
                                                        {!!html_entity_decode(showHostProfileImage($host->id, 'Host', 'photo', '001.jpg'))!!}
                                                    </div> 
                                                    <div class="rght_profile_host">
                                                        <h3 class="panel-title"> 
                                                            {{$profile->name}}
                                                        </h3>
                                                        @if($host->urls->count() > 0)
                                                            @foreach($host->urls as $url)                            
                                                                <div class="rght_links">
                                                                    @if($url->type=="website") 
                                                                        <i class="mdi mdi-web"></i>
                                                                    @elseif($url->type=="blog") 
                                                                        <i class="mdi mdi-blogger"></i>
                                                                    @elseif($url->type=="rss") 
                                                                        <i class="mdi mdi-rss"></i>  
                                                                    @elseif($url->type=="twitter")
                                                                        <i class=" mdi mdi-twitter"></i> 
                                                                    @elseif($url->type=="facebook")
                                                                        <i class="mdi mdi-facebook"></i> 
                                                                    @elseif($url->type=="instagram")
                                                                        <i class="mdi mdi-instagram"></i>  
                                                                    @elseif($url->type=="youtube")
                                                                        <i class="mdi mdi-youtube-play"></i>
                                                                    @elseif($url->type=="google")
                                                                        <i class="mdi mdi-google-plus"></i>  
                                                                    @elseif($url->type=="pinterest")
                                                                        <i class="mdi mdi-pinterest"></i> 
                                                                    @elseif($url->type=="blog")
                                                                        <i class="mdi mdi-blogger"></i>  
                                                                    @elseif($url->type=="vimeo")
                                                                        <i class="mdi mdi-vimeo"></i>
                                                                    @endif
                                                                    <a target="_blank" href="{{$url->url}}">{{$url->url}}</a>
                                                                </div>                     
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                @endforeach
                                            @endif                                            
                                        @endif
                                    </div>
                                    <div class="panel-body">
                                        <div class="show-read-more"> {!! $host->bio !!} </div>
                                    </div>
                                    <div class="panel-footer">
                                        @if($host->user->count() > 0)
                                            @if($host->user->profiles->count() > 0)
                                                @foreach($host->user->profiles as $profile)
                                                    @if(!empty($profile->name))
                                                        <div>
                                                            <i class="typcn typcn-user"></i><strong>{{$profile->name}}:</strong>
                                                        </div>
                                                    @endif
                                                @endforeach
                                                <div>

                                                    @if($host->user->count() > 0)
                                                        @if($host->user->profiles->count() > 0)
                                                            @foreach($host->user->profiles as $profile)                       
                                                                @if(!empty($profile->cellphone))
                                                                    <i class="mdi mdi-cellphone-android"></i>
                                                                    <a href="mailto:{{$profile->cellphone}}">{{$profile->cellphone}}</a>
                                                                @endif @if(!empty($profile->phone))
                                                                    <i class="mdi mdi-phone"></i>
                                                                    <a href="mailto:{{$profile->phone}}">{{$profile->phone}}</a>
                                                                @endif
                                                                @if(!empty($profile->skype))
                                                                <i class="mdi mdi-skype"></i>
                                                                    {{$profile->skype}}
                                                                @endif
                                                            @endforeach     
                                                        @endif
                                                    @endif
                                                    @if(!empty($host->user->email))
                                                        <i class="mdi mdi-email"></i>
                                                        <a href="mailto:{{$host->user->email}}">{{$host->user->email}}</a>
                                                    @endif   
                                                </div>  
                                            @endif                                            
                                        @endif
                                        @if(!empty($host->pr_name))
                                            <hr>
                                            <div>
                                                <i class="typcn typcn-user"></i><strong>{{$host->pr_name}} (Other):</strong>
                                            </div>
                                        @endif
                                        <div>
                                            @if(!empty($host->pr_cellphone))
                                                <i class="mdi mdi-cellphone-android"></i>
                                                <a href="mailto:{{$host->pr_cellphone}}">{{$host->pr_cellphone}}</a>
                                            @endif
                                            @if(!empty($host->pr_skype))
                                                <i class="mdi mdi-skype"></i>
                                                <a href="mailto:{{$host->pr_skype}}">{{$host->pr_skype}}</a>
                                            @endif
                                            @if(!empty($host->pr_email))
                                                <i class="mdi mdi-email"></i>
                                                <a href="mailto:{{$host->pr_email}}">{{$host->pr_email}}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Guests information -->
                        @if($episode->guests->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Guests', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->guests as $guest)
                                <div class="panel panel-border panel-pink">
                                    <div class="panel-heading archive_page">
                                        <div class="lft_profile_host">
                                            {!!html_entity_decode(showHostProfileImage($guest->id, 'Guest', 'photo', '001.jpg'))!!}
                                        </div> 
                                        <div class="rght_profile_host">
                                            <h3 class="panel-title"> 
                                                {{$guest->name}}
                                            </h3>
                                            @if($guest->urls->count() > 0)
                                                @foreach($guest->urls as $url)                           
                                                    <div class="rght_links">
                                                        @if($url->type=="website") 
                                                            <i class="mdi mdi-web"></i>
                                                        @elseif($url->type=="blog") 
                                                            <i class="mdi mdi-blogger"></i>
                                                        @elseif($url->type=="rss") 
                                                            <i class="mdi mdi-rss"></i>  
                                                        @elseif($url->type=="twitter")
                                                            <i class=" mdi mdi-twitter"></i> 
                                                        @elseif($url->type=="facebook")
                                                            <i class="mdi mdi-facebook"></i> 
                                                        @elseif($url->type=="instagram")
                                                            <i class="mdi mdi-instagram"></i>
                                                        @elseif($url->type=="youtube")
                                                            <i class="mdi mdi-youtube-play"></i>
                                                        @elseif($url->type=="google")
                                                            <i class="mdi mdi-google-plus"></i>
                                                        @elseif($url->type=="pinterest")
                                                            <i class="mdi mdi-pinterest"></i> 
                                                        @elseif($url->type=="blog")
                                                            <i class="mdi mdi-blogger"></i>
                                                        @elseif($url->type=="vimeo")
                                                            <i class="mdi mdi-vimeo"></i>
                                                        @endif
                                                        <a target="_blank" href="{{$url->url}}">{{$url->url}}</a>
                                                    </div>                     
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="show-read-more">{!! $guest->bio !!}</div>
                                    </div>
                                    <div class="panel-footer">
                                        @if(!empty($guest->name))
                                            <div>
                                                <i class="typcn typcn-user"></i><strong>{{$guest->name}}:</strong>
                                            </div>
                                        @endif
                                        <div>
                                            @if(!empty($guest->cellphone))
                                                <i class="mdi mdi-email"></i>
                                                <a href="mailto:{{$guest->cellphone}}">{{$guest->cellphone}}</a>
                                            @endif
                                            @if(!empty($guest->phone))
                                                <i class="mdi mdi-phone"></i>
                                                <a href="callto:{{$guest->phone}}">{{$guest->phone}}</a>                    
                                            @endif
                                            @if(!empty($guest->skype))
                                                <i class="mdi mdi-skype"></i>
                                                {{$guest->skype}}
                                            @endif
                                            @if(!empty($guest->skype_phone))
                                                <i class="mdi mdi-skype"></i>
                                                <a href="callto:{{$guest->skype_phone}}">{{$guest->skype_phone}}</a> 
                                            @endif
                                            @if(!empty($guest->email))
                                                <i class="mdi mdi-email"></i>
                                                <a href="mailto:{{$guest->email}}">{{$guest->email}}</a>             
                                            @endif
                                        </div> 
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                    </div>  <!-- end p-20 -->        
                    <div class="form-group">
                        <div class="fixed_btn">
                            <button  type="button" id="EpisodeBtnAdd" name="data[Episode][btnAdd]" value="edit"class="btn btn-primary waves-effect waves-light submit_form" data-form-id="EpisodeAdminAddEncoreForm">Add
                             </button>
                        </div>
                    </div><!-- end row -->
                    {{ Form::close() }}
                </div><!-- end card-box -->
            </div><!-- end col-sm-12 col-xs-12 col-md-12 -->
        </div><!-- end row -->
    </div> <!-- end container -->
</div> <!-- end content -->

@endsection
