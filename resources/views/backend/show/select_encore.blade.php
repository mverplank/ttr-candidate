@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Select Episode</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li class="active">
                            <a href="{{route('shows_agenda')}}">Shows Schedule</a>
                        </li>
                        <li class="active">
                            Select Episode
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row --> 

        <!-- Channel Filter-->
        <div class="row">
            <div class="card-box">                 
                @if(Config::get('constants.ADMIN') == getCurrentUserType())  
                <div class="form-group row"> 
                    <div class="col-md-3">
                        {{Form::select('data[Episode][f][shows_id]', (!empty($all_shows) ? $all_shows : []), !empty($selected_show) ? $selected_show : '', $attributes=array('id'=>'EpisodeFShowsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>   
                    <div class="col-md-3">
                        {{Form::select('data[Host][f][id]', (!empty($all_hosts) ? $all_hosts : []), !empty($selected_host) ? $selected_host : '', $attributes=array('id'=>'HostFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                    </div>                 
                    <div class="col-md-3">
                        {{Form::select('data[Guest][f][id]', (!empty($all_guests) ? $all_guests : []), !empty($selected_guest) ? $selected_guest : '', $attributes=array('id'=>'GuestFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-custom'))}}
                    </div>  
                    <div class="col-md-3">
                         <div class="input-group">        
                            {{Form::text('data[Date][f][date]', $value = null, $attributes = array('class'=>'form-control required encoreDate', 'placeholder'=>'mm/dd/yyyy', 'id'=>'encoreDate'))}}
                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>  
                        </div>
                    </div>                    
                </div>
                @endif  
                @if(Config::get('constants.HOST') == getCurrentUserType())
                <div class="form-group row page_top_btns"> 
                    <div class="col-md-4">
                        {{Form::select('data[Upcoming][f][channels_id]', (!empty($all_channels) ? $all_channels : []), !empty($selected_channel) ? $selected_channel : '', $attributes=array('id'=>'ArchiveFChannelsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>
                </div>
                @endif            
            </div>
        </div> 
        <!-- end row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <span style="display: none;" id="schedule_id">{{$schedule_id}}</span>
                    <span style="display: none;" id="selected_date">{{$selected_date}}</span>
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->
                    <table id="encores_datatable" class="table table-striped table-bordered">
                        <thead>                       
                            <tr>
                                <th>Date</th>
                                <th>Title</th>
                                <th>Encores</th>
                            </tr>
                        </thead>                       
                    </table>
                </div>
            </div>
        </div>
        <!-- end row --> 

    </div> <!-- container -->
</div> <!-- content -->
<!-- Encore Detail Modal -->
<div id="encore_detail" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Encores Details</h4>
    <div class="custom-modal-text">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-orange">Show</div>
                        <div class="col-md-9 show_title"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-primary">Episode</div>
                        <div class="col-md-9 episode_title"></div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-pink">Orginal Air Date</div>
                        <div class="col-md-9 episode_date"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-teal">Encore Dates</div>
                        <div class="col-md-9 episode_encores"></div>
                    </div>
                </div>
            </div>  
        </div>      
    </div>
</div>
<a href="#encore_detail" style="display:none;" id="open_encore_detail" class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" data-animation="blur" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a">
@endsection
