@extends('backend.layouts.main')
@section('content')
<!-- Modal -->
<div id="custom-modal" class="modal-demo episode_edit">
   <!-- <button type="button" class="close" onclick="Custombox.close();">
      <span>&times;</span><span class="sr-only">Close</span>
      </button> -->
   <h4 class="custom-modal-title">Please Wait!</h4>
   <div class="custom-modal-text">
   </div>
</div>
<div class="content">
<div class="container">
<div class="row">
   <div class="col-xs-12">
      <div class="page-title-box">
         <h4 class="page-title"> Edit Episode</h4>
         <ol class="breadcrumb p-0 m-0">
            <li>
               <a href="{{url('admin')}}">Dashboard</a>
            </li>
            <li class="active">
               show
            </li>
            <li class="active">
               Edit Episode
            </li>
         </ol>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<!-- end row -->
<span id="episode_edit_page" data-episode_id="{{$episode->id}}"></span>
<a style="display:none;" href="#custom-modal" class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" data-animation="blur" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" id="open_confliction_message"></a>
<div class="row">
   <div class="col-xs-12">
      <div class="card-box">
         <!-- Form Starts-->
         {!! Form::model('', ['url' => 'admin/show/episodes/edit_view/'.$episode->id, 'files' => true, 'id' => 'EpisodeAdminEditViewForm']) !!}
         <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
               <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Transformation Talk Radio</h4>
               <input type="hidden" name="form_type" id="form_type" value="edit">
               <input type="hidden" name="form_episode" id="episode_edit_id" value="{{$episode->id}}">
               {{Form::hidden('edit_id', $episode->id, array('id'=>'edit_id'))}}
               <div class="p-20">
                  <div class="form-group row">
                     {{Form::label('EpisodeD', 'ID / Date', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-2">
                        <div style="border: solid 1px;padding: 10px;">{{$episode->id}}</div>
                     </div>
                     <div class="col-sm-2">
                        <div style="border: solid 1px;padding: 10px;">
                           <?php
                              echo date("D",strtotime($episode->date));
                              ?>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div style="border: solid 1px;padding: 10px;">{{$episode->date}}</div>
                     </div>
                     <div class="col-sm-2">
                        <div style="border: solid 1px;padding: 10px;">{{$episode->date }}</div>
                     </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-sm-12">
                        {{Form::label('EpisodeAssignedType', 'Type', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-9">
                           <div class="radio radio-info radio-inline">
                              {{Form::radio('data[Episode][assigned_type]', 'live', ($episode->assigned_type == 'live')? true:false, array('id'=>'EpisodeAssignedTypeLive'))}}
                              {{Form::label('EpisodeAssignedTypeLive', 'Live', array('class' => 'col-sm-12 form-control-label'))}}
                           </div>
                           <div class="radio radio-warning radio-inline">
                              {{Form::radio('data[Episode][assigned_type]', 'pre-recorded',($episode->assigned_type == 'pre-recorded')? true:false, array('id'=>'EpisodeAssignedType'))}}
                              {{Form::label('EpisodeAssignedType', 'pre-recorded', array('class' => 'col-sm-12 form-control-label'))}}
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group row">
                     {{Form::label('ShowName', 'Show', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-8">
                        <div style="border: solid 1px;padding: 10px;">
                           @if($episode->show->count() > 0)
                           {{$episode->show->name}}
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
               <div class="p-20">
                  <div class="form-group row">
                     {{Form::label('EpisodeTitle', 'Episode Title', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-6">
                        <div class="form-group">
                           {{Form::textarea('data[Episode][title]',$episode->title, ['id'=>'EpisodeTitle', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"textarea", 'rows'=>"2", 'autocomplete'=>"off"])}}
                        </div>
                     </div>
                  </div>
                  <div class="form-group row">
                     {{Form::label('EpisodeDescription', 'Episode Description', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="form-group">
                           {{Form::textarea('data[Episode][description]',$episode->description, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeDescription', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                        </div>
                     </div>
                  </div>
                  <div class="form-group row">
                     {{Form::label('TagTag', 'Tags', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-9 tag-input-type">
                        <div class="form-group">
                           {{Form::text('data[Tag][Tag]', $tags, ['id'=>'TagTag', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                           {{Form::hidden('data[Tag][TagIds]', $tag_ids)}}
                           <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="p-20">
                     <div class="form-group row">
                        {{Form::label('EpisodeCover0', 'Episode Image', array('class' => 'col-sm-2 form-control-label'))}}
                        <div class="col-sm-8">
                           <div class="row">
                              @if($media->count() > 0)
                              @php ($url_count = 1)
                              @foreach($media as $med)
                              @if($med->sub_module == 'cover')
                              <div class="col-sm-4 adding-medias media_Episode_cover" data-off="{{$url_count}}">
                                 <div class="jFiler-items jFiler-row">
                                    <ul class="jFiler-items-list jFiler-items-grid">
                                       <li class="jFiler-item" data-jfiler-index="1">
                                          <div class="jFiler-item-container">
                                             <div class="jFiler-item-inner">
                                                <div class="jFiler-item-thumb">
                                                   <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'cover', 'image', 0, '{{$med->media->filename}}',{{$med->id}})" target="_blank">
                                                      <div class="jFiler-item-thumb-image">
                                                         <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                      </div>
                                                   </a>
                                                </div>
                                                <div class="jFiler-item-assets jFiler-row">
                                                   <ul class="list-inline pull-right">
                                                      <li>
                                                         <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_cover" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="cover"></a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              @php ($url_count++)
                              @endif
                              @endforeach
                              @endif
                              <div class="media_Episode_cover_outer hidden" data-off="0" style="display: none;"></div>
                           </div>
                           <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                           </span>
                        </div>
                        <div class="col-sm-2">
                           <div class="mtf-buttons" style="clear:both;float:right;">
                              <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="cover" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="p-20">
                     <div class="form-group row">
                        {{Form::label('HostHost', 'Hosts', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                           {{Form::select('data[Host][Host][]', [],'', $attributes = array('class'=>'form-control HostHost select2-multiple', 'id'=> 'HostHost','list'=>'host-list', 'multiple'=>'multiple','data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose ...'))}}
                        </div>
                        <span style="display: none;" class="loading">
                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                        </span>                                   
                     </div>
                     <!-- End row -->
                     <div class="form-group row">
                        {{Form::label('CohostCohost', 'Co-Hosts', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                           {{Form::select('data[Cohost][Cohost][]', [],'', $attributes = array('class'=>'form-control ShowCoHost select2-multiple', 'id'=> 'CohostCohost','list'=>'cohost-list', 'multiple'=>'multiple','data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose ...'))}}
                        </div>
                        <span style="display: none;" class="loading">
                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                        </span>                                   
                     </div>
                     <div class="form-group row">
                        {{Form::label('PodcasthostPodcasthost', 'Podcast-host', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                           {{Form::select('data[Podcasthost][Podcasthost][]', [],'', $attributes = array('class'=>'form-control ShowPodcastHost select2-multiple', 'id'=> 'PodcasthostPodcasthost','list'=>'podcasthost-list', 'multiple'=>'multiple','data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose ...'))}}
                        </div>
                        <span style="display: none;" class="loading">
                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                        </span>                                   
                     </div>
                     <div class="form-group row">
                        {{Form::label('GuestGuest', 'Guests', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                           {{Form::select('data[Guest][Guest][]', [],'', $attributes = array('class'=>'form-control ShowGuestHost select2-multiple', 'id'=> 'GuestGuest','list'=>'guest-list', 'multiple'=>'multiple','data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose ...'))}}
                        </div>
                        <span style="display: none;" class="loading">
                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                        </span>                                   
                     </div>
                     <div class="form-group row">
                        {{Form::label('EpisodeHasManyGiveaways', 'Giveaways', array('class' => 'col-sm-3 form-control-label'))}}
                        @if($episode->giveaways->count() > 0)
                        @php ($give = 1)
                        @foreach($episode->giveaways as $giveaways)
                        <div class="form-group row episode_has_giveways" data-off="{{$give}}">
                           <div class="col-sm-8">
                              <div class="form-group">
                                 {{Form::textarea('data[Giveaway]['.$give.'][description]',$giveaways->description, ['id'=>'Giveaway'.$give.'Description', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"textarea", 'rows'=>"1", 'autocomplete'=>"off"])}}
                              </div>
                           </div>
                        </div>
                        @php ($give++)                                  
                        @endforeach
                        @else
                        <div class="col-sm-8">
                           <div class="form-group episode_has_giveways" data-off="1">
                              {{Form::textarea('data[Giveaway][1][description]','', ['id'=>'Giveaway1Description', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"textarea", 'rows'=>"1", 'autocomplete'=>"off"])}}
                           </div>
                        </div>
                        @endif 
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="EpisodeHasManyGiveawaysAdd"><i class="glyphicon glyphicon-plus"></i> <span> add</span>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Internal details (not visible on site)</h4>
                  <div class="p-20">
                     <div class="tabs-vertical-env">
                        <div class="card-box">
                           <!-- <h4 class="header-title m-t-0 m-b-30">Default Tabs</h4> -->
                           <ul class="nav tabs-vertical">
                              <li class="active">
                                 <a href="#internal_host_notes" data-toggle="tab" aria-expanded="false">
                                 <span class="visible-xs"><i class="fa fa-home"></i></span>
                                 <span class="hidden-xs">Notes</span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="#events_promote" data-toggle="tab" aria-expanded="true">
                                 <span class="visible-xs"><i class="fa fa-user"></i></span>
                                 <span class="hidden-xs">Events Promote</span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="#special_offers" data-toggle="tab" aria-expanded="false">
                                 <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                 <span class="hidden-xs">Special Offers</span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="#listener_interaction" data-toggle="tab" aria-expanded="false">
                                 <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                 <span class="hidden-xs">Listener Interaction</span>
                                 </a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="internal_host_notes">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][internal_host_notes]',$episode->internal_host_notes, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeInternalHostNotes', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                              <div class="tab-pane" id="events_promote">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][events_promote]',$episode->events_promote, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeEventsPromote', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                              <div class="tab-pane" id="special_offers">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][special_offers]',$episode->special_offers, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeSpecialOffers', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                              <div class="tab-pane" id="listener_interaction">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][listener_interaction]',$episode->listener_interaction, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeListenerInteraction', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- end col -->                                    
                  </div>
               </div>
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Social Sharing Feature</h4>
                  <div class="p-20">
                     <div class="row">
                        <div class="tabs-vertical-env">
                           <div class="card-box">
                              <!-- <h4 class="header-title m-t-0 m-b-30">Default Tabs</h4> -->
                              <ul class="nav tabs-vertical">
                                 <li class="active">
                                    <a href="#segment1" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                    <span class="hidden-xs">post 1</span>
                                    </a>
                                 </li>
                                 <li class="">
                                    <a href="#segment2" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                                    <span class="hidden-xs">post 2</span>
                                    </a>
                                 </li>
                                 <li class="">
                                    <a href="#segment3" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                    <span class="hidden-xs">post 3</span>
                                    </a>
                                 </li>
                                 <li class="">
                                    <a href="#segment4" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                    <span class="hidden-xs">post 4</span>
                                    </a>
                                 </li>
                              </ul>
                              <div class="tab-content">
                                 <div class="tab-pane active" id="segment1">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment1Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment1_title]',$episode->segment1_title, ['id'=>'EpisodeSegment1Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment1', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment1]',$episode->segment1, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment1', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment1VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::url('data[Episode][segment1_video_url]',$episode->segment1_video_url, ['id'=>'EpisodeSegment1VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 col-md-12">
                                          <div class="p-20">
                                             <div class="form-group row">
                                                {{Form::label('EpisodeCover1', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                <div class="col-sm-8">
                                                   <div class="row">
                                                      @if($media->count() > 0)
                                                      @php ($url_count = 1)
                                                      @foreach($media as $med)
                                                      @if($med->sub_module == 'segment1')
                                                      <div class="col-sm-4 adding-medias media_Episode_segment1" data-off="{{$url_count}}">
                                                         <ul class="jFiler-items-list jFiler-items-grid">
                                                            <li class="jFiler-item" data-jfiler-index="1">
                                                               <div class="jFiler-item-container">
                                                                  <div class="jFiler-item-inner">
                                                                     <div class="jFiler-item-thumb">
                                                                        <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment1', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                           <div class="jFiler-item-thumb-image">
                                                                              <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                           </div>
                                                                        </a>
                                                                     </div>
                                                                     <div class="jFiler-item-assets jFiler-row">
                                                                        <ul class="list-inline pull-right">
                                                                           <li>
                                                                              <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment1" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment1"></a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      @php ($url_count++)
                                                      @endif
                                                      @endforeach
                                                      @endif
                                                      <div class="media_Episode_segment1_outer hidden" data-off="0" style="display: none;"></div>
                                                   </div>
                                                   <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                   Click on image to edit.
                                                   </span>
                                                </div>
                                                <div class="col-sm-2">
                                                   <div class="mtf-buttons" style="clear:both;float:right;">
                                                      <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment1" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="segment2">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment2Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment2_title]',$episode->segment2_title, ['id'=>'EpisodeSegment2Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment2', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment2]',$episode->segment2, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment2', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment2VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::url('data[Episode][segment2_video_url]',$episode->segment2_video_url, ['id'=>'EpisodeSegment2VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 col-md-12">
                                          <div class="p-20">
                                             <div class="form-group row">
                                                {{Form::label('EpisodeCover2', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                <div class="col-sm-8">
                                                   <div class="row">
                                                      @if($media->count() > 0)
                                                      @php ($url_count = 1)
                                                      @foreach($media as $med)
                                                      @if($med->sub_module == 'segment2')
                                                      <div class="col-sm-4 adding-medias media_Episode_segment2" data-off="{{$url_count}}">
                                                         <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                               <li class="jFiler-item" data-jfiler-index="1">
                                                                  <div class="jFiler-item-container">
                                                                     <div class="jFiler-item-inner">
                                                                        <div class="jFiler-item-thumb">
                                                                           <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment2', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                              <div class="jFiler-item-thumb-image">
                                                                                 <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <div class="jFiler-item-assets jFiler-row">
                                                                           <ul class="list-inline pull-right">
                                                                              <li>
                                                                                 <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment1" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment2"></a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      @php ($url_count++)
                                                      @endif
                                                      @endforeach
                                                      @endif
                                                      <div class="media_Episode_segment2_outer hidden" data-off="0" style="display: none;"></div>
                                                   </div>
                                                   <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                   Click on image to edit.
                                                   </span>
                                                </div>
                                                <div class="col-sm-2">
                                                   <div class="mtf-buttons" style="clear:both;float:right;">
                                                      <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment2" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="segment3">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment3Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment3_title]',$episode->segment3_title, ['id'=>'EpisodeSegment3Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment3', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment3]',$episode->segment3, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment3', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment3VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::url('data[Episode][segment3_video_url]',$episode->segment3_video_url, ['id'=>'EpisodeSegment3VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 col-md-12">
                                          <div class="p-20">
                                             <div class="form-group row">
                                                {{Form::label('EpisodeCover3', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                <div class="col-sm-8">
                                                   <div class="row">
                                                      @if($media->count() > 0)
                                                      @php ($url_count = 1)
                                                      @foreach($media as $med)
                                                      @if($med->sub_module == 'segment3')
                                                      <div class="col-sm-4 adding-medias media_Episode_segment3" data-off="{{$url_count}}">
                                                         <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                               <li class="jFiler-item" data-jfiler-index="1">
                                                                  <div class="jFiler-item-container">
                                                                     <div class="jFiler-item-inner">
                                                                        <div class="jFiler-item-thumb">
                                                                           <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment3', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                              <div class="jFiler-item-thumb-image">
                                                                                 <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <div class="jFiler-item-assets jFiler-row">
                                                                           <ul class="list-inline pull-right">
                                                                              <li>
                                                                                 <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment3" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment3"></a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      @php ($url_count++)
                                                      @endif
                                                      @endforeach
                                                      @endif
                                                      <div class="media_Episode_segment3_outer hidden" data-off="0" style="display: none;"></div>
                                                   </div>
                                                   <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                   Click on image to edit.
                                                   </span>
                                                </div>
                                                <div class="col-sm-2">
                                                   <div class="mtf-buttons" style="clear:both;float:right;">
                                                      <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment3" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="segment4">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment4Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment4_title]',$episode->segment4_title, ['id'=>'EpisodeSegment4Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment4', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment4]',$episode->segment4, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment4', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment4VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::text('data[Episode][segment4_video_url]',$episode->segment4_video_url, ['id'=>'EpisodeSegment4VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-12 col-xs-12 col-md-12">
                                             <div class="p-20">
                                                <div class="form-group row">
                                                   {{Form::label('EpisodeCover4', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                   <div class="col-sm-8">
                                                      <div class="row">
                                                         @if($media->count() > 0)
                                                         @php ($url_count = 1)
                                                         @foreach($media as $med)
                                                         @if($med->sub_module == 'segment4')
                                                         <div class="col-sm-4 adding-medias media_Episode_segment4" data-off="{{$url_count}}">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                               <li class="jFiler-item" data-jfiler-index="1">
                                                                  <div class="jFiler-item-container">
                                                                     <div class="jFiler-item-inner">
                                                                        <div class="jFiler-item-thumb">
                                                                           <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment4', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                              <div class="jFiler-item-thumb-image">
                                                                                 <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <div class="jFiler-item-assets jFiler-row">
                                                                           <ul class="list-inline pull-right">
                                                                              <li>
                                                                                 <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment4" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment4"></a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         @php ($url_count++)
                                                         @endif
                                                         @endforeach
                                                         @endif
                                                         <div class="media_Episode_segment4_outer hidden" data-off="0" style="display: none;"></div>
                                                      </div>
                                                      <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                      Click on image to edit.
                                                      </span>
                                                   </div>
                                                   <div class="col-sm-2">
                                                      <div class="mtf-buttons" style="clear:both;float:right;">
                                                         <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment4" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12 col-xs-12 col-md-12">
                     <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Media</h4>
                     <div class="p-20">
                        <div class="form-group row">
                           <div class="form-group col-sm-12">
                              {{Form::label('EpisodeFileMedia0', 'Episode Archive', array('class' => 'col-sm-2 form-control-label'))}}
                              <div class="col-sm-8">
                                 <div class="row">
                                    @if($media->count() > 0)
                                    @php ($url_count = 1)
                                    @foreach($media as $med)
                                    @if($med->sub_module == 'media')
                                    <div class="col-sm-4 adding-medias media_Episode_media" data-off="{{$url_count}}">
                                       <div class="jFiler-items jFiler-row">
                                          <ul class="jFiler-items-list jFiler-items-grid">
                                             <li class="jFiler-item" data-jfiler-index="1">
                                                <div class="jFiler-item-container">
                                                   <div class="jFiler-item-inner">
                                                      <div class="jFiler-item-thumb">
                                                         <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'media', 'image', 0, '{{$med->media->filename}}',{{$med->id}})" target="_blank">
                                                            <div class="jFiler-item-thumb-image">
                                                               <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                            </div>
                                                         </a>
                                                      </div>
                                                      <div class="jFiler-item-assets jFiler-row">
                                                         <ul class="list-inline pull-right">
                                                            <li>
                                                               <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_media" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="media"></a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                    @php ($url_count++)
                                    @endif
                                    @endforeach
                                    @endif
                                    <div class="media_Episode_media_outer hidden" data-off="0" style="display: none;"></div>
                                 </div>
                                 <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                 Maximum 1 file. Maximum file size: 120MB.
                                 </span>
                              </div>
                              <div class="col-sm-2">
                                 <div class="mtf-buttons" style="clear:both;float:right;">
                                    <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="media" data-media_type="audio" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12 col-xs-12 col-md-12">
                     <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Settings</h4>
                     <div class="p-20">
                        <div class="form-group row">
                           <div class="col-sm-12">
                              {{Form::label('EpisodeIsOnline', 'Status', array('class' => 'col-sm-3 form-control-label'))}}
                              <div class="col-sm-9">
                                 <div class="radio radio-info radio-inline">
                                    {{Form::radio('data[Episode][is_online]', '1',($episode->is_online == '1')? true:false, array('id'=>'EpisodeIsOnline1'))}}
                                    {{Form::label('EpisodeIsOnline1', 'Online', array('class' => 'col-sm-12 form-control-label'))}}
                                 </div>
                                 <div class="radio radio-warning radio-inline">
                                    {{Form::radio('data[Episode][is_online]', '0', ($episode->is_online == '0')? true:false, array('id'=>'EpisodeIsOnline0'))}}
                                    {{Form::label('EpisodeIsOnline0', 'Offline', array('class' => 'col-sm-12 form-control-label'))}}
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('FeaturedOnChannelFeaturedOnChannel', 'Featured on channels', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-7">
                              @if(!empty($all_channels))
                              @foreach ( $all_channels as $i =>$item )
                              <div class="checkbox checkbox-pink">
                                 {!! Form::checkbox( 'data[FeaturedOnChannel][FeaturedOnChannel][]', $i, (!empty($episode_has_channels) ? (in_array($i, $episode_has_channels) ? $i : '') : ''), ['class' => 'md-check', 'id' => 'FeaturedOnChannelFeaturedOnChannel'.$i] ) !!}
                                 {!! Form::label('FeaturedOnChannelFeaturedOnChannel'.$i,  $item) !!}
                              </div>
                              @endforeach
                              @else
                              Not any Channel existing yet.
                              @endif
                           </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group row">
                           {{Form::label('SponsorSponsor', 'Sponsor', array('class' => 'col-sm-3 form-control-label'))}}
                           @if($episode->sponsors->count() > 0)
                           @foreach($episode->sponsors as $sponsor) 
                           <div class="col-sm-9">
                              {{Form::select('data[Sponsor][Sponsor]',[$sponsor->id=>$sponsor->name],$sponsor->name, $attributes = array('class'=>'form-control SponsorSponsor select2-multiple', 'id'=> 'SponsorSponsor','list'=>'host-list', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Sponsor...'))}}
                           </div>
                           @endforeach
                           @else
                           <div class="col-sm-9">
                              {{Form::select('data[Sponsor][Sponsor]', [],'', $attributes = array('class'=>'form-control SponsorSponsor select2-multiple', 'id'=> 'SponsorSponsor','list'=>'host-list', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Sponsor...'))}}
                           </div>
                           @endif
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>                                   
                        </div>
                        <div class="form-group row">
                           {{Form::label('guestHasManyCategories', 'Category / Sub-Category', array('class' => 'col-sm-3 form-control-label'))}}
                           <?php //echo "<pre>";print_R(json_decode($itunes_categories));?>
                           <div class="col-sm-9">
                              @if(!empty($assigned_categories))
                              @php ($guests_cat_count = 1)
                              @foreach($assigned_categories as $guests_cat)                                                                 
                              <div class="outside_guests_cat" data-off="1">
                                 @if(array_key_exists($guests_cat,$all_categories))
                                 <div class="col-sm-5">
                                    {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories,$guests_cat, $attributes=array('id'=>'Category'.$guests_cat_count.'Category0','class'=>'form-control episodes_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                 </div>
                                 <div class="col-sm-5">
                                    <?php  $fetch_subcategories = subCategories($guests_cat,$assigned_categories);
                                       ?>
                                    {{Form::select('data[Category][Category][]', $fetch_subcategories['subcategories'], (isset($fetch_subcategories['selected_cat'])) ? $fetch_subcategories['selected_cat'] : '', $attributes=array('id'=>'Category'.$guests_cat_count.'Category1','class'=>'form-control episode_sub_cate','multiple'=> 'multiple','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                 </div>
                                 @break
                                 @endif
                              </div>
                              @php ($guests_cat_count++)
                              @endforeach
                              @else
                              <div class="outside_guests_cat" data-off="1">
                                 <div class="col-sm-5">
                                    {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories, '', $attributes=array('id'=>'Category1Category0','class'=>'form-control episodes_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                 </div>
                                 <div class="col-sm-5">
                                    {{Form::select('data[Category][Category][]',[''=>'none'], '', $attributes=array('id'=>'Category1Category1','class'=>'form-control episode_sub_cate','multiple'=> 'multiple','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                 </div>
                              </div>
                              @endif   
                              <input type="hidden" name="select_id" id='select_id' value="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="fixed_btn">
                     <button type="submit" data-form-id="EpisodeAdminEditViewForm" class="btn btn-primary waves-effect waves-light submit_form" name="data[Episode][btnUpdate]" id="EpisodeBtnUpdate"> Update
                     </button>
                  </div>
               </div>
               {{ Form::close() }}
               <!-- end row -->
            </div>
            <!-- end ard-box -->
         </div>
         <!-- end col-->
      </div>
      <!-- end row -->
      @include('backend.cms.upload_media_popup',['module_id' =>$episode->id,'main_module' => "Episode"])
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection