@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Shows Upcoming</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            @if(Config::get('constants.HOST') == getCurrentUserType())
                                <a href="{{url('host')}}">Dashboard</a>
                            @endif
                            @if(Config::get('constants.ADMIN') == getCurrentUserType())
                                <a href="{{url('admin')}}">Dashboard</a>
                            @endif
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li class="active">
                            Upcoming
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row --> 

        <!-- Channel Filter-->
        <div class="row">
            <div class="card-box">                 
                @if(Config::get('constants.ADMIN') == getCurrentUserType())  
                <div class="form-group row"> 
                    <div class="col-md-4">
                        {{Form::select('data[Upcoming][f][channels_id]', (!empty($all_channels) ? $all_channels : []), !empty($selected_channel) ? $selected_channel : '', $attributes=array('id'=>'UpcomingFChannelsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>                    
                    <div class="col-md-4">
                        {{Form::select('data[Upcoming][f][sponsors_id]', (!empty($all_sponsors) ? $all_sponsors : []), !empty($selected_sponsor) ? $selected_sponsor : '', $attributes=array('id'=>'UpcomingFSponsorsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-custom'))}}
                    </div>
                    <div class="col-md-4">
                        {{Form::select('data[Upcoming][f][host_ids]', (!empty($all_hosts) ? $all_hosts : []), !empty($selected_host) ? $selected_host : '', $attributes=array('id'=>'UpcomingFHostIds', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                    </div>
                </div>
                @endif  
                @if(Config::get('constants.HOST') == getCurrentUserType())
                <div class="form-group row page_top_btns"> 
                    <div class="col-md-4">
                        {{Form::select('data[Upcoming][f][channels_id]', (!empty($all_channels) ? $all_channels : ''), !empty($selected_channel) ? $selected_channel : '', $attributes=array('id'=>'ArchiveFChannelsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>
                </div>
                @endif            
            </div>
        </div> 
        <!-- end row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->
                    <table id="upcoming_episodes_datatable" class="table table-striped table-bordered">
                        <thead>                       
                            <tr>
                                <th>Date</th>
                                <th>Title</th>
                                <th>Guests</th>
                                <th>Sponsors</th>
                                <th></th>
                            </tr>
                        </thead>                       
                    </table>
                </div>
            </div>
        </div>
        <!-- end row --> 

    </div> <!-- container -->
</div> <!-- content -->

@endsection
