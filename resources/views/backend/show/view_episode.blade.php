@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">View Episode</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li>
                            <a href="{{url('admin/show/schedules/agenda')}}">Show Episode</a>
                        </li>
                        <li class="active">
                            View Episode
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row --> 
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">                
                    <div class="form-group row page_top_btns">
                        <div style="float:right;">
                            <a href="javascript:void(0)" onclick="print()" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">  <i class="mdi mdi-printer"></i>   Print
                            </a>
                            <a href="{{url('/')}}/admin/show/episodes/edit_view/{{$episode->id}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">  Edit Episode
                            </a>
                            <a href="javascript:void(0)" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">  Preview
                            </a>
							<a href="javascript:void(0)" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" data-type="episode" onclick="deleteArchived({{$episode->id}}, this)" >  Delete
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <!-- end row --> 
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">  
                    <div class="p-20">                                      
                        <div class="form-group row">
                            {{Form::hidden('episode_id', $episode->id, ['id'=>'episode_id'])}}
                            {{Form::label('EpisodeD', 'Date', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                <button class="btn btn-info disabled m-b-5">{{($episode->date) ? Carbon\Carbon::parse($episode->date)->format('l') :'' }}</button>
                                <button class="btn btn-purple disabled m-b-5">{{($episode->date) ? Carbon\Carbon::parse($episode->date)->format('m/d/Y') :'' }}</button> 
                                <button class="btn btn-orange disabled m-b-5">{{($episode->time) ? Carbon\Carbon::parse($episode->time)->format('g:i a') :'' }}</button>                                
                            </div>
                        </div>
                        <!-- End Row-->
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Podcast statistics', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                <div class="form-group col-sm-3">
                                    {{Form::select('data[Stat][month]', $months, date('n'), $attributes=array('id'=>'StatMonth', 'class'=>'form-control'))}}
                                </div>
                                <div class="form-group col-sm-3">
                                    {{Form::select('data[Stat][year]', $years, date('Y'), $attributes=array('id'=>'StatYear', 'class'=>'form-control'))}}
                                </div>
                                <div class="p-20">
                                    <!-- <div id="distributed-series" class="ct-chart ct-golden-section"></div> -->
                                    <canvas id="distributed-series" height="300"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- End Row-->

                        <div class="form-group row">
                            {{Form::label('EpisodeCover', 'Episode Image', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8"> 
                                 @if($media->count() > 0)
                                    @php ($url_count = 1)
                                    @foreach($media as $med)
                                        @if($med->sub_module == 'cover')
                                            <div class="col-sm-4 media_Episode_cover" data-off="{{$url_count}}"><a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></a></div>
                                            @php ($url_count++)
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <!-- End Row-->

                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Share', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Facebook"> 
                                    <button type="button" class="btn btn-facebook waves-effect waves-light">
                                        <i class="fa fa-facebook m-r-5"></i> Facebook
                                    </button>
                                </a>
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Twitter"> 
                                    <button type="button" class="btn btn-twitter waves-effect waves-light">
                                        <i class="fa fa-twitter m-r-5"></i> Twitter
                                    </button>
                                </a>
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Google"> 
                                    <button type="button" class="btn btn-googleplus waves-effect waves-light">
                                        <i class="fa fa-google-plus m-r-5"></i> Google+
                                    </button>
                                </a>
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Linkedin"> 
                                    <button type="button" class="btn btn-linkedin waves-effect waves-light">
                                       <i class="fa fa-linkedin m-r-5"></i> Linkedin
                                    </button> 
                                </a>
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Pinterest"> 
                                    <button type="button" class="btn btn-pinterest waves-effect waves-light">
                                        <i class="fa fa-pinterest m-r-5"></i> Pinterest
                                    </button>
                                </a>
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Email">                                                       
                                    <button type="button" class="btn btn-dropbox waves-effect waves-light">
                                        <i class="fa fa-envelope m-r-5"></i> Email
                                    </button>
                                </a>
                                <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Tumblr"> 
                                    <button type="button" class="btn btn-tumblr waves-effect waves-light">
                                        <i class="fa fa-tumblr m-r-5"></i> Tumblr
                                    </button>
                                </a>
                            </div>
                        </div>
                        <!-- End Row-->                        
                        <div class="form-group row">
                            {{Form::label('EpisodeChannels', 'Channels', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                @if($episode->channels->count() > 0)
                                    @foreach($episode->channels as $channel) 
                                    {{$channel->name}}
                                    @endforeach
                                @endif                              
                            </div>
                        </div>
                        <!-- End Row-->
                        <!-- {{ Form::open(array('url' => '/admin/show/episodes/view_agenda/'.$episode->id, 'method' => 'PUT', 'id'=>'EpisodeAdminViewAgendaForm')) }} -->
                        <div class="form-group row">
                            {{Form::label('EpisodeB', 'Scheduled Share', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                               <button type="button" class="btn btn-teal m-b-5" id="edit_toogle">Edit</button>   
                               <div id="toogle" style="display:none;">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#twitter_" data-toggle="tab" aria-expanded="false">
                                                <span class="hidden-xs">twitter</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#facebook_" data-toggle="tab" aria-expanded="true">
                                                <span class="hidden-xs">facebook</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="twitter_">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'EpisodeTwitterScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','episode',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','twitter',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_twitter]','1',['id'=>'episodeScheduledShareIsTwitter1'])}}
                                        @if(!empty($episode_arr))
                                        <div class="form-group row">              
                                            <div class="col-sm-6">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', $episode_arr['date_twitter'],  array('class'=>'form-control required datepicker','id'=>'episode_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-6">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]',$episode_arr['time_twitter'],  array('class'=>'form-control required timepicker','id'=>'episode_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]',$episode_arr['twitter_desc'],  array('class'=>'form-control','id'=>'episode_twitter_desc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', '',  array('class'=>'form-control datepicker','id'=>'episode_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]', '',  array('class'=>'form-control timepicker','id'=>'episode_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]', '',  array('class'=>'form-control','id'=>'episode_twitter_desc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1Episode" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="EpisodeTwitterScheduleForm" data-form-type ='episode' data-form-for ='twitter'>Save
                                                 </button>
                                            </div>
                                        </div>
                                    {{ Form::close() }}
                                        </div>
                                        <div class="tab-pane" id="facebook_">
                                            {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'EpisodeFacebookScheduleForm')) }}
                                            {{Form::hidden('data[EpisodeScheduledShare][type]','episode',['id'=>''])}}
                                            {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                            {{Form::hidden('data[EpisodeScheduledShare][for]','facebook',['id'=>''])}}
                                            {{Form::hidden('data[EpisodeScheduledShare][is_facebook]','1',['id'=>'episodeScheduledShareIsfacebook1'])}}

                                            @if(!empty($episode_arr))
                                            <div class="form-group row">               
                                                <div class="col-sm-5">
                                                    {{Form::text('data[EpisodeScheduledShare][facebook_date]', $episode_arr['date_facebook'],  array('class'=>'form-control required datepicker','id'=>'episode_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                                </div>
                                                <div class="col-sm-5">
                                                    {{Form::text('data[EpisodeScheduledShare][facebook_time]',$episode_arr['time_facebook'],  array('class'=>'form-control required timepicker','id'=>'episode_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]',$episode_arr['facebook_desc'],  array('class'=>'form-control','id'=>'episodeScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                                </div>
                                            </div>
                                            @else
                                            <div class="form-group row">               
                                                <div class="col-sm-5">
                                                    {{Form::text('data[EpisodeScheduledShare][facebook_date]', '',  array('class'=>'form-control required datepicker','id'=>'episode_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                                </div>
                                                <div class="col-sm-5">
                                                    {{Form::text('data[EpisodeScheduledShare][facebook_time]', '',  array('class'=>'form-control required timepicker','id'=>'episode_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]', '',  array('class'=>'form-control','id'=>'spisodeScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                                </div>
                                            </div>

                                            @endif
                                            <div class="form-group row">
                                                <div class="col-sm-8 col-sm-offset-4">
                                                     <button  type="button" id="btnSharedShedule1Episode" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="EpisodeFacebookScheduleForm" data-form-type ='episode' data-form-for ='facebook'>Save
                                                     </button>
                                                </div>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                               </div>                            
                            </div>
                        </div>  
                       <!--  {{ Form::close() }}   -->                    
                        <!-- End Row-->
                        @if($episode->show->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Show', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {{$episode->show->name}}          
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <div class="form-group row">
                            {{Form::label('EpisodeTitle', 'Title', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {{$episode->title}}
                            </div>
                        </div>
                        <!-- End Row-->
                        <div class="form-group row">
                            {{Form::label('EpisodeAssignedType', 'Type', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {{$episode->assigned_type}}
                            </div>
                        </div>
                        <!-- End Row-->
                        @if(!empty($episode->description))
                        <div class="form-group row">
                            {{Form::label('EpisodeDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->description!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Segment 1 -->
                        @if(!empty($episode->segment1_title))
                        <div class="form-group row">
                            {{Form::label('EpisodeSegment1', 'Segment 1', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8"> 
                                <h4>{!!$episode->segment1_title!!}</h4>  
                                <div class="lft_profile_host">
                                    <div class="row">
                                        @if($media->count() > 0)
                                            @php ($url_count = 1)
                                            @foreach($media as $med)
                                                @if($med->sub_module == 'segment1')
                                                    <div class="col-sm-4 media_Episode_segment1" data-off="{{$url_count}}"><a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></a></div>
                                                    @php ($url_count++)
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    @if(!empty($episode->segment1))
                                        {!!$episode->segment1!!}
                                    @endif
                                </div>
                                <button class="btn btn-pink waves-effect waves-light m-b-5" id="Segment11"> 
                                    <i class="mdi mdi-share-variant"></i> 
                                    <span>Share</span> 
                                </button>   
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-9" id='Segment1' style="display:none;">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#share1" data-toggle="tab" aria-expanded="true">
                                            <span class="hidden-xs">Share</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#code1" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Embed</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharetwitterSegment1" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Twitter scheduled share ■</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharefacebookSegment1" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Facebook scheduled share ■</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="share1">
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Facebook/1"> 
                                            <button type="button" class="btn btn-facebook waves-effect waves-light">
                                                <i class="fa fa-facebook m-r-5"></i> Facebook
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Twitter/1"> 
                                            <button type="button" class="btn btn-twitter waves-effect waves-light">
                                                <i class="fa fa-twitter m-r-5"></i> Twitter
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Google/1"> 
                                            <button type="button" class="btn btn-googleplus waves-effect waves-light">
                                                <i class="fa fa-google-plus m-r-5"></i> Google+
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Linkedin/1"> 
                                            <button type="button" class="btn btn-linkedin waves-effect waves-light">
                                               <i class="fa fa-linkedin m-r-5"></i> Linkedin
                                            </button> 
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Pinterest/1"> 
                                            <button type="button" class="btn btn-pinterest waves-effect waves-light">
                                                <i class="fa fa-pinterest m-r-5"></i> Pinterest
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Email/1">                                                       
                                            <button type="button" class="btn btn-dropbox waves-effect waves-light">
                                                <i class="fa fa-envelope m-r-5"></i> Email
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Tumblr/1"> 
                                            <button type="button" class="btn btn-tumblr waves-effect waves-light">
                                                <i class="fa fa-tumblr m-r-5"></i> Tumblr
                                            </button>
                                        </a>
                                    </div>
                                    <div class="tab-pane" id="code1">
                                      {{Form::text('data[Episode][code1]', '<iframe width="480" height="84" src="http://178.128.179.99/embed/episode/'.$episode->id.'/1" frameborder="0"></iframe>',  array('class'=>'form-control','id'=>'EpisodeCode1','readonly'))}}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharetwitterSegment1">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment1TwitterScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment1',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','twitter',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_twitter]','1',['id'=>''])}}
                                        @if(!empty($segment1))
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', $segment1['date_twitter'],  array('class'=>'form-control required datepicker','id'=>'segment1_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]',$segment1['time_twitter'],  array('class'=>'form-control required timepicker','id'=>'segment1_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]',$segment1['twitter_desc'],  array('class'=>'form-control','id'=>'segment1ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment1_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment1_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]', '',  array('class'=>'form-control','id'=>'segment1ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1Episode" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment1TwitterScheduleForm" data-form-type ='segment1' data-form-for ='twitter'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharefacebookSegment1">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment1FacebookScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment1',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','facebook',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_facebook]','1',['id'=>'segment1ScheduledShareIsfacebook1'])}}
                                        @if(!empty($segment1))
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', $segment1['date_facebook'],  array('class'=>'form-control datepicker','id'=>'segment1_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]',$segment1['time_facebook'],  array('class'=>'form-control timepicker','id'=>'segment1_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]',$segment1['facebook_desc'],  array('class'=>'form-control','id'=>'segment1ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment1_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment1_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]', '',  array('class'=>'form-control','id'=>'segment1ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>

                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment1" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment1FacebookScheduleForm" data-form-type ='segment1' data-form-for ='facebook'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>                      
                        @endif
                        <!-- End Row-->
                        <!-- Segment 2 -->
                        @if(!empty($episode->segment2_title))
                        <div class="form-group row">
                            {{Form::label('EpisodeSegment2', 'Segment 2', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8"> 
                                <h4>{!!$episode->segment2_title!!}</h4>  
                                <div class="lft_profile_host">
                                    <div class="row">
                                        @if($media->count() > 0)
                                            @php ($url_count = 1)
                                            @foreach($media as $med)
                                                @if($med->sub_module == 'segment2')
                                                    <div class="col-sm-6 media_Episode_segment2" data-off="{{$url_count}}"><a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></a></div>
                                                    @php ($url_count++)
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    @if(!empty($episode->segment2))
                                        {!!$episode->segment2!!}
                                    @endif
                                </div>
                                <button class="btn btn-pink waves-effect waves-light m-b-5" id="Segment22"> 
                                    <i class="mdi mdi-share-variant"></i> 
                                    <span>Share</span> 
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-9" id='Segment2' style="display:none;">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#share2" data-toggle="tab" aria-expanded="true">
                                            <span class="hidden-xs">Share</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#code2" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Embed</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharetwitterSegment2" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Twitter scheduled share ■</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharefacebookSegment2" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Facebook scheduled share ■</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="share2">
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Facebook/2"> 
                                            <button type="button" class="btn btn-facebook waves-effect waves-light">
                                                <i class="fa fa-facebook m-r-5"></i> Facebook
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Twitter/2"> 
                                            <button type="button" class="btn btn-twitter waves-effect waves-light">
                                                <i class="fa fa-twitter m-r-5"></i> Twitter
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Google/2"> 
                                            <button type="button" class="btn btn-googleplus waves-effect waves-light">
                                                <i class="fa fa-google-plus m-r-5"></i> Google+
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Linkedin/2"> 
                                            <button type="button" class="btn btn-linkedin waves-effect waves-light">
                                               <i class="fa fa-linkedin m-r-5"></i> Linkedin
                                            </button> 
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Pinterest/2"> 
                                            <button type="button" class="btn btn-pinterest waves-effect waves-light">
                                                <i class="fa fa-pinterest m-r-5"></i> Pinterest
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Email/2">                                                       
                                            <button type="button" class="btn btn-dropbox waves-effect waves-light">
                                                <i class="fa fa-envelope m-r-5"></i> Email
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Tumblr/2"> 
                                            <button type="button" class="btn btn-tumblr waves-effect waves-light">
                                                <i class="fa fa-tumblr m-r-5"></i> Tumblr
                                            </button>
                                        </a>
                                    </div>
                                    <div class="tab-pane" id="code2">
                                       {{Form::text('data[Episode][code2]', '<iframe width="480" height="84" src="http://178.128.179.99/embed/episode/'.$episode->id.'/2" frameborder="0"></iframe>',  array('class'=>'form-control','id'=>'EpisodeCode2','readonly'))}}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharetwitterSegment2">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment2TwitterScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment2',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','twitter',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_twitter]','1',['id'=>'segment2ScheduledShareIsTwitter1'])}}
                                        @if(!empty($segment2))
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', $segment2['date_twitter'],  array('class'=>'form-control required datepicker','id'=>'segment2_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]',$segment2['time_twitter'],  array('class'=>'form-control required timepicker','id'=>'segment2_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]',$segment2['twitter_desc'],  array('class'=>'form-control','id'=>'segment2ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">             
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment2_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment2_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]', '',  array('class'=>'form-control','id'=>'segment2ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment2" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment2TwitterScheduleForm" data-form-type ='segment2' data-form-for ='twitter'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharefacebookSegment2">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment2FacebookScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment2',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','facebook',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_facebook]','1',['id'=>'segment2ScheduledShareIsfacebook1'])}}
                                        @if(!empty($segment2))
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', $segment2['date_facebook'],  array('class'=>'form-control required datepicker','id'=>'segment2_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]',$segment2['time_facebook'],  array('class'=>'form-control required timepicker','id'=>'segment2_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]',$segment2['facebook_desc'],  array('class'=>'form-control','id'=>'segment2ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment2_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment2_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]', '',  array('class'=>'form-control','id'=>'segment2ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>

                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment2" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment2FacebookScheduleForm" data-form-type ='segment2' data-form-for ='facebook'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        @endif
                        <!-- End Row-->
                        <!-- Segment 3 -->
                        @if(!empty($episode->segment3_title))
                        <div class="form-group row">
                            {{Form::label('EpisodeSegment3', 'Segment 3', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8"> 
                                <h4>{!!$episode->segment3_title!!}</h4>  
                                <div class="lft_profile_host">
                                    <div class="row">
                                        @if($media->count() > 0)
                                            @php ($url_count = 1)
                                            @foreach($media as $med)
                                                @if($med->sub_module == 'segment3')
                                                    <div class="col-sm-6 media_Episode_segment3" data-off="{{$url_count}}"><a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></a></div>
                                                    @php ($url_count++)
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    @if(!empty($episode->segment3))
                                        {!!$episode->segment3!!}
                                    @endif
                                </div>
                                <button class="btn btn-pink waves-effect waves-light m-b-5" id='Segment33'> 
                                    <i class="mdi mdi-share-variant"></i> 
                                    <span>Share</span> 
                                </button>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-9" id='Segment3' style="display:none;">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#share3" data-toggle="tab" aria-expanded="true">
                                            <span class="hidden-xs">Share</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#code3" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Embed</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharetwitterSegment3" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Twitter scheduled share ■</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharefacebookSegment3" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Facebook scheduled share ■</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="share3">
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Facebook/3"> 
                                            <button type="button" class="btn btn-facebook waves-effect waves-light">
                                                <i class="fa fa-facebook m-r-5"></i> Facebook
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Twitter/3"> 
                                            <button type="button" class="btn btn-twitter waves-effect waves-light">
                                                <i class="fa fa-twitter m-r-5"></i> Twitter
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Google/3"> 
                                            <button type="button" class="btn btn-googleplus waves-effect waves-light">
                                                <i class="fa fa-google-plus m-r-5"></i> Google+
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Linkedin/3"> 
                                            <button type="button" class="btn btn-linkedin waves-effect waves-light">
                                               <i class="fa fa-linkedin m-r-5"></i> Linkedin
                                            </button> 
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Pinterest/3"> 
                                            <button type="button" class="btn btn-pinterest waves-effect waves-light">
                                                <i class="fa fa-pinterest m-r-5"></i> Pinterest
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Email/3">                                                       
                                            <button type="button" class="btn btn-dropbox waves-effect waves-light">
                                                <i class="fa fa-envelope m-r-5"></i> Email
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Tumblr/3"> 
                                            <button type="button" class="btn btn-tumblr waves-effect waves-light">
                                                <i class="fa fa-tumblr m-r-5"></i> Tumblr
                                            </button>
                                        </a>
                                    </div>
                                    <div class="tab-pane" id="code3">
                                        {{Form::text('data[Episode][code3]', '<iframe width="480" height="84" src="http://178.128.179.99/embed/episode/'.$episode->id.'/3" frameborder="0"></iframe>',  array('class'=>'form-control','id'=>'EpisodeCode3','readonly'))}}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharetwitterSegment3">
                                       {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment3TwitterScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment3',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','twitter',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_twitter]','1',['id'=>'segment3ScheduledShareIsTwitter1'])}}

                                        @if(!empty($segment3))
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', $segment3['date_twitter'],  array('class'=>'form-control required datepicker','id'=>'segment3_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]',$segment3['time_twitter'],  array('class'=>'form-control required timepicker','id'=>'segment3_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]',$segment3['twitter_desc'],  array('class'=>'form-control','id'=>'segment3ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment3_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment3_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]', '',  array('class'=>'form-control','id'=>'segment3ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment3" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment3TwitterScheduleForm" data-form-type ='segment3' data-form-for ='twitter'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharefacebookSegment3">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment3FacebookScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment3',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','facebook',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_facebook]','1',['id'=>'segment3ScheduledShareIsfacebook1'])}}
                                        @if(!empty($segment3))
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', $segment3['date_facebook'],  array('class'=>'form-control required datepicker','id'=>'segment3_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]',$segment3['time_facebook'],  array('class'=>'form-control required timepicker','id'=>'segment3_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]',$segment3['facebook_desc'],  array('class'=>'form-control','id'=>'segment3ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment3_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment3_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]', '',  array('class'=>'form-control','id'=>'segment3ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>

                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment3" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment3FacebookScheduleForm" data-form-type ='segment3' data-form-for ='facebook'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>                      
                        @endif
                        <!-- End Row-->
                        <!-- Segment 4 -->
                        @if(!empty($episode->segment4_title))
                        <div class="form-group row">
                            {{Form::label('EpisodeSegment4', 'Segment 4', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8"> 
                                <h4>{!!$episode->segment4_title!!}</h4>  
                                <div class="lft_profile_host">
                                    <div class="row">
                                        @if($media->count() > 0)
                                            @php ($url_count = 1)
                                            @foreach($media as $med)
                                                @if($med->sub_module == 'segment4')
                                                    <div class="col-sm-6 media_Episode_segment4" data-off="{{$url_count}}"><a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank"><img src ="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" style="width:200px;"/></a></div>
                                                    @php ($url_count++)
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    @if(!empty($episode->segment4))
                                        {!!$episode->segment4!!}
                                    @endif
                                </div>
                                <button class="btn btn-pink waves-effect waves-light m-b-5" id='Segment44'> 
                                    <i class="mdi mdi-share-variant"></i> 
                                    <span>Share</span> 
                                </button>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-9" id='Segment4' style="display:none;">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#share4" data-toggle="tab" aria-expanded="true">
                                            <span class="hidden-xs">Share</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#code4" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Embed</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharetwitterSegment4" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Twitter scheduled share ■</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#scheudledsharefacebookSegment4" data-toggle="tab" aria-expanded="false">
                                            <span class="hidden-xs">Facebook scheduled share ■</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="share4">
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Facebook/4"> 
                                            <button type="button" class="btn btn-facebook waves-effect waves-light">
                                                <i class="fa fa-facebook m-r-5"></i> Facebook
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Twitter/4"> 
                                            <button type="button" class="btn btn-twitter waves-effect waves-light">
                                                <i class="fa fa-twitter m-r-5"></i> Twitter
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Google/4"> 
                                            <button type="button" class="btn btn-googleplus waves-effect waves-light">
                                                <i class="fa fa-google-plus m-r-5"></i> Google+
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Linkedin/4"> 
                                            <button type="button" class="btn btn-linkedin waves-effect waves-light">
                                               <i class="fa fa-linkedin m-r-5"></i> Linkedin
                                            </button> 
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Pinterest/4"> 
                                            <button type="button" class="btn btn-pinterest waves-effect waves-light">
                                                <i class="fa fa-pinterest m-r-5"></i> Pinterest
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Email/4">                                                       
                                            <button type="button" class="btn btn-dropbox waves-effect waves-light">
                                                <i class="fa fa-envelope m-r-5"></i> Email
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}/admin/show/episodes/share/{{$episode->id}}/Tumblr/4"> 
                                            <button type="button" class="btn btn-tumblr waves-effect waves-light">
                                                <i class="fa fa-tumblr m-r-5"></i> Tumblr
                                            </button>
                                        </a>
                                    </div>
                                    <div class="tab-pane" id="code4">
                                      {{Form::text('data[Episode][code4]', '<iframe width="480" height="84" src="http://178.128.179.99/embed/episode/'.$episode->id.'/4" frameborder="0"></iframe>',  array('class'=>'form-control','id'=>'EpisodeCode4','readonly'))}}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharetwitterSegment4">
                                      {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment4TwitterScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment4',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','twitter',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_twitter]','1',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_twitter]','1',['id'=>'segment4ScheduledShareIsTwitter1'])}}
                                        @if(!empty($segment4))
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', $segment4['date_twitter'],  array('class'=>'form-control required datepicker','id'=>'segment4_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]',$segment4['time_twitter'],  array('class'=>'form-control required timepicker','id'=>'segment4_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]',$segment4['twitter_desc'],  array('class'=>'form-control','id'=>'segment4ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment4_twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][twitter_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment4_twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]', '',  array('class'=>'form-control','id'=>'segment4ScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment4" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment4TwitterScheduleForm" data-form-type ='segment4' data-form-for ='twitter'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                    <div class="tab-pane" id="scheudledsharefacebookSegment4">
                                        {{ Form::open(array('url'=>'','method' => 'POST', 'id'=>'segment4FacebookScheduleForm')) }}
                                        {{Form::hidden('data[EpisodeScheduledShare][type]','segment4',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$episode->id,['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][for]','facebook',['id'=>''])}}
                                        {{Form::hidden('data[EpisodeScheduledShare][is_facebook]','1',['id'=>'segment4ScheduledShareIsfacebook1'])}}
                                        @if(!empty($segment4))
                                        <div class="form-group row">               
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', $segment4['date_facebook'],  array('class'=>'form-control required datepicker','id'=>'segment4_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]',$segment4['time_facebook'],  array('class'=>'form-control required timepicker','id'=>'segment4_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]',$segment4['facebook_desc'],  array('class'=>'form-control','id'=>'segment4ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group row">              
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_date]', '',  array('class'=>'form-control required datepicker','id'=>'segment4_facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::text('data[EpisodeScheduledShare][facebook_time]', '',  array('class'=>'form-control required timepicker','id'=>'segment4_facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]', '',  array('class'=>'form-control','id'=>'segment4ScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
                                            </div>
                                        </div>

                                        @endif
                                        <div class="form-group row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                 <button  type="button" id="btnSharedShedule1segment4" name="data[EpisodeScheduledShare][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form ScheduleForm"  data-forms-id="segment4FacebookScheduleForm" data-form-type ='segment4' data-form-for ='facebook'>Save
                                                 </button>
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        @endif
                        <!-- End Row-->
                        <!-- Notes -->
                        @if(!empty($episode->internal_host_notes))
                        <div class="form-group row">
                            {{Form::label('EpisodeInternalHostNotes', 'Notes', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->internal_host_notes!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Special Offers -->
                        @if(!empty($episode->special_offers))
                        <div class="form-group row">
                            {{Form::label('EpisodeSpecialOffers', 'Special Offers', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->special_offers!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Listener Interaction -->
                        @if(!empty($episode->listener_interaction))
                        <div class="form-group row">
                            {{Form::label('EpisodeListenerInteraction', 'Listener Interaction', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                {!!$episode->listener_interaction!!}
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- GiveAways -->
                        @if($episode->giveaways->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeGiveAways', 'Giveways', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">  
                                @foreach($episode->giveaways as $giveaway) 
                                    <div>{!!$giveaway->description!!}</div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        @if($episode->episodes_has_categories->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Categories', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->episodes_has_categories as $cat)
                                    @if($episode->episodes_has_categories->count() > 0)
                                        <button class="btn btn-purple disabled m-b-5">{{$cat->category->name}}</button>                 
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        @if($episode->tags->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Tags', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->tags as $tag)
                                    <button class="btn btn-teal disabled m-b-5">{{$tag->name}}</button>             
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            {{Form::label('EpisodeSponsors', 'Sponsors', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                @if($episode->sponsors->count() > 0)
                                    @foreach($episode->sponsors as $sponsor) 
                                    {{$sponsor->name}}
                                    @endforeach
                                @endif                              
                            </div>
                        </div>
                        <!-- End Row-->
                        @if($episode->hosts->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Hosts', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->hosts as $host)
                                <div class="panel panel-border panel-info">
                                    <div class="panel-heading archive_page">
                                        @if($host->user->count() > 0)
                                            @if($host->user->profiles->count() > 0)
                                                @foreach($host->user->profiles as $profile)
                                                    <div class="lft_profile_host">
                                                        {!!html_entity_decode(showHostProfileImage($host->id, 'Host', 'photo', '001.jpg'))!!}
                                                    </div> 
                                                    <div class="rght_profile_host">
                                                        <h3 class="panel-title"> 
                                                            {{$profile->name}}
                                                        </h3>
                                                        @if($host->urls->count() > 0)
                                                            @foreach($host->urls as $url)                            
                                                                <div class="rght_links">
                                                                    @if($url->type=="website") 
                                                                        <i class="mdi mdi-web"></i>
                                                                    @elseif($url->type=="blog") 
                                                                        <i class="mdi mdi-blogger"></i>
                                                                    @elseif($url->type=="rss") 
                                                                        <i class="mdi mdi-rss"></i>  
                                                                    @elseif($url->type=="twitter")
                                                                        <i class=" mdi mdi-twitter"></i> 
                                                                    @elseif($url->type=="facebook")
                                                                        <i class="mdi mdi-facebook"></i> 
                                                                    @elseif($url->type=="instagram")
                                                                        <i class="mdi mdi-instagram"></i>  
                                                                    @elseif($url->type=="youtube")
                                                                        <i class="mdi mdi-youtube-play"></i>
                                                                    @elseif($url->type=="google")
                                                                        <i class="mdi mdi-google-plus"></i>  
                                                                    @elseif($url->type=="pinterest")
                                                                        <i class="mdi mdi-pinterest"></i> 
                                                                    @elseif($url->type=="blog")
                                                                        <i class="mdi mdi-blogger"></i>  
                                                                    @elseif($url->type=="vimeo")
                                                                        <i class="mdi mdi-vimeo"></i>
                                                                    @endif
                                                                    <a target="_blank" href="{{$url->url}}">{{$url->url}}</a>
                                                                </div>                     
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                @endforeach
                                            @endif                                            
                                        @endif
                                    </div>
                                    <div class="panel-body">
                                        <div class="show-read-more"> {!! $host->bio !!} </div>
                                    </div>
                                    <div class="panel-footer">
                                        @if($host->user->count() > 0)
                                            @if($host->user->profiles->count() > 0)
                                                @foreach($host->user->profiles as $profile)
                                                    @if(!empty($profile->name))
                                                        <div>
                                                            <i class="typcn typcn-user"></i><strong>{{$profile->name}}:</strong>
                                                        </div>
                                                    @endif
                                                @endforeach
                                                <div>

                                                    @if($host->user->count() > 0)
                                                        @if($host->user->profiles->count() > 0)
                                                            @foreach($host->user->profiles as $profile)                       
                                                                @if(!empty($profile->cellphone))
                                                                    <i class="mdi mdi-cellphone-android"></i>
                                                                    <a href="mailto:{{$profile->cellphone}}">{{$profile->cellphone}}</a>
                                                                @endif @if(!empty($profile->phone))
                                                                    <i class="mdi mdi-phone"></i>
                                                                    <a href="mailto:{{$profile->phone}}">{{$profile->phone}}</a>
                                                                @endif
                                                                @if(!empty($profile->skype))
                                                                <i class="mdi mdi-skype"></i>
                                                                    {{$profile->skype}}
                                                                @endif
                                                            @endforeach     
                                                        @endif
                                                    @endif
                                                    @if(!empty($host->user->email))
                                                        <i class="mdi mdi-email"></i>
                                                        <a href="mailto:{{$host->user->email}}">{{$host->user->email}}</a>
                                                    @endif   
                                                </div>  
                                            @endif                                            
                                        @endif
                                        @if(!empty($host->pr_name))
                                            <hr>
                                            <div>
                                                <i class="typcn typcn-user"></i><strong>{{$host->pr_name}} (Other):</strong>
                                            </div>
                                        @endif
                                        <div>
                                            @if(!empty($host->pr_cellphone))
                                                <i class="mdi mdi-cellphone-android"></i>
                                                <a href="mailto:{{$host->pr_cellphone}}">{{$host->pr_cellphone}}</a>
                                            @endif
                                            @if(!empty($host->pr_skype))
                                                <i class="mdi mdi-skype"></i>
                                                <a href="mailto:{{$host->pr_skype}}">{{$host->pr_skype}}</a>
                                            @endif
                                            @if(!empty($host->pr_email))
                                                <i class="mdi mdi-email"></i>
                                                <a href="mailto:{{$host->pr_email}}">{{$host->pr_email}}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                        <!-- Guests information -->
                        @if($episode->guests->count() > 0)
                        <div class="form-group row">
                            {{Form::label('EpisodeD', 'Guests', array('class' => 'col-sm-4 form-control-label'))}}
                            <div class="col-sm-8">   
                                @foreach($episode->guests as $guest)
                                <div class="panel panel-border panel-pink">
                                    <div class="panel-heading archive_page">
                                        <div class="lft_profile_host">
                                            {!!html_entity_decode(showHostProfileImage($guest->id, 'Guest', 'photo', '001.jpg'))!!}
                                        </div> 
                                        <div class="rght_profile_host">
                                            <h3 class="panel-title"> 
                                                {{$guest->name}}
                                            </h3>
                                            @if($guest->urls->count() > 0)
                                                @foreach($guest->urls as $url)                           
                                                    <div class="rght_links">
                                                        @if($url->type=="website") 
                                                            <i class="mdi mdi-web"></i>
                                                        @elseif($url->type=="blog") 
                                                            <i class="mdi mdi-blogger"></i>
                                                        @elseif($url->type=="rss") 
                                                            <i class="mdi mdi-rss"></i>  
                                                        @elseif($url->type=="twitter")
                                                            <i class=" mdi mdi-twitter"></i> 
                                                        @elseif($url->type=="facebook")
                                                            <i class="mdi mdi-facebook"></i> 
                                                        @elseif($url->type=="instagram")
                                                            <i class="mdi mdi-instagram"></i>
                                                        @elseif($url->type=="youtube")
                                                            <i class="mdi mdi-youtube-play"></i>
                                                        @elseif($url->type=="google")
                                                            <i class="mdi mdi-google-plus"></i>
                                                        @elseif($url->type=="pinterest")
                                                            <i class="mdi mdi-pinterest"></i> 
                                                        @elseif($url->type=="blog")
                                                            <i class="mdi mdi-blogger"></i>
                                                        @elseif($url->type=="vimeo")
                                                            <i class="mdi mdi-vimeo"></i>
                                                        @endif
                                                        <a target="_blank" href="{{$url->url}}">{{$url->url}}</a>
                                                    </div>                     
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="show-read-more">{!! $guest->bio !!}</div>
                                    </div>
                                    <div class="panel-footer">
                                        @if(!empty($guest->name))
                                            <div>
                                                <i class="typcn typcn-user"></i><strong>{{$guest->name}}:</strong>
                                            </div>
                                        @endif
                                        <div>
                                            @if(!empty($guest->cellphone))
                                                <i class="mdi mdi-email"></i>
                                                <a href="mailto:{{$guest->cellphone}}">{{$guest->cellphone}}</a>
                                            @endif
                                            @if(!empty($guest->phone))
                                                <i class="mdi mdi-phone"></i>
                                                <a href="callto:{{$guest->phone}}">{{$guest->phone}}</a>                    
                                            @endif
                                            @if(!empty($guest->skype))
                                                <i class="mdi mdi-skype"></i>
                                                {{$guest->skype}}
                                            @endif
                                            @if(!empty($guest->skype_phone))
                                                <i class="mdi mdi-skype"></i>
                                                <a href="callto:{{$guest->skype_phone}}">{{$guest->skype_phone}}</a> 
                                            @endif
                                            @if(!empty($guest->email))
                                                <i class="mdi mdi-email"></i>
                                                <a href="mailto:{{$guest->email}}">{{$guest->email}}</a>             
                                            @endif
                                        </div> 
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- End Row-->
                    </div>          
                    <!-- <div class="form-group row">

                    </div> -->
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> <!-- content -->

@endsection
