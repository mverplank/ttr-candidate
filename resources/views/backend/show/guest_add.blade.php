@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Guest</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li class="active">
                            <a href="{{url('admin/show/guests')}}">All Guests</a>
                        </li>
                        <li class="active">
                            Add Guest
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div style="float:right;">
                <a href="#custom-modal1" class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" data-animation="blur" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a">Import Data</a>
            </div>
        </div>

        <!-- Start Import Data Modal -->
        <div id="custom-modal1" class="modal-demo">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Import Host Details</h4>
            <div class="custom-modal-text" style="width:500px;">
                <div class="row form-group">
                    {{Form::select('data[Host][f][id]', (!empty($all_hosts) ? $all_hosts : []), !empty($selected_host) ? $selected_host : '', $attributes=array('id'=>'ImportHostFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                </div>
                <div class="row form-group">
                    <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="" id="importHost">
                        Import
                    </button>  
                </div>             
            </div>
        </div>
        <!-- End Import Data Modal -->

        <!-- Modal -->
        <div id="custom-modal" class="modal-demo" style="width:1000px;">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Edit Image</h4>
            <div class="custom-modal-text">
                <!-- Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="img-container">
                                <img id="image" src="" alt="Picture" style="width:100%">
                            </div>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 docs-buttons">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">
                                        <span class="fa fa-search-plus"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">
                                        <span class="fa fa-search-minus"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, -10, 0)">
                                        <span class="fa fa-arrow-left"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 10, 0)">
                                        <span class="fa fa-arrow-right"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, -10)">
                                        <span class="fa fa-arrow-up"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;move&quot;, 0, 10)">
                                        <span class="fa fa-arrow-down"></span>
                                    </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
                                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">
                                        <span class="fa fa-rotate-left"></span>
                                    </span>
                            </button>
                          <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">
                              <span class="fa fa-rotate-right"></span>
                            </span>
                          </button>
                        </div>

                        <div class="btn-group">
                          <button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleX&quot;, -1)">
                              <span class="fa fa-arrows-h"></span>
                            </span>
                          </button>
                          <button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleY&quot;, -1)">
                              <span class="fa fa-arrows-v"></span>
                            </span>
                          </button>
                        </div>

                        <div class="btn-group">
                          <button type="button" class="btn btn-primary" data-method="crop" title="Crop">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;crop&quot;)">
                              <span class="fa fa-check"></span>
                            </span>
                          </button>
                          <button type="button" class="btn btn-primary" data-method="clear" title="Clear">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;clear&quot;)">
                              <span class="fa fa-remove"></span>
                            </span>
                          </button>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
                                <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;reset&quot;)">
                                    <span class="fa fa-refresh"></span>
                                </span>
                            </button>
                            <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Import image with Blob URLs">
                                    <span class="fa fa-upload"></span>
                                </span>
                            </label>                         
                        </div>

                        <div class="btn-group btn-group-crop">
                            <button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" data-image_for="photo" data-module="Host" data-form_type="add" data-crop="1" id="getCropped" data-old_image="" data-outer_dropzone="HostPhoto0" data-dropzone_obj="">
                                <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { maxWidth: 4096, maxHeight: 4096 })">
                                    Apply
                                </span>
                            </button>                            
                        </div>

                        <!-- Show the cropped image in modal -->
                        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body"></div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>
                              </div>
                            </div>
                          </div>
                        </div><!-- /.modal -->
                      </div><!-- /.docs-buttons -->

                      <div class="col-md-3 docs-toggles">
                        <!-- <h3>Toggles:</h3> -->
                        <div class="btn-group d-flex flex-nowrap" data-toggle="buttons">
                          <label class="btn btn-primary active">
                            <input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">
                              16:9
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">
                              4:3
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">
                              1:1
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">
                              2:3
                            </span>
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">
                              Free
                            </span>
                          </label>
                        </div>

                      </div><!-- /.docs-toggles -->
                    </div>
                  </div>
            </div>
        </div>
        <!-- End Modal -->
        <a href="#custom-modal" class="" data-animation="blur" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" id="open_modal"></a>

        <div class="row">
            <!-- Form Starts-->  
            @if(Config::get('constants.ADMIN') == getCurrentUserType())
            {{ Form::open(array('url' => '/admin/show/guests/add', 'id' => 'GuestAdminAddForm','data-forma'=>'1', 'data-action'=>'add', 'novalidate'=>'novalidate','data-model'=>'Guest','data-ajax'=>'1')) }}
            @endif
            @if(Config::get('constants.HOST') == getCurrentUserType())
            {{ Form::open(array('url' => '/host/show/guests/add', 'id' => 'GuestAdminAddForm','data-forma'=>'1', 'data-action'=>'add', 'novalidate'=>'novalidate','data-model'=>'Guest','data-ajax'=>'1')) }}
            @endif
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Personal Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('HostD', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{ Form::hidden('data[Host][id]', 0, $attributes=array( 'class'=>'form-control','id'=>'host_user_id')) }}
                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][title]', $prefix_suffix['prefix'], '', $attributes=array('id'=>'GuestTitle', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1'))}}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][firstname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'First name','required'=>true,'id'=>'GuestFirstname','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','data-required'=>'1', 'maxlength'=>'255'))}}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][lastname]', $value = null, $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name','required'=>true, 'id'=>'GuestLastname','data-forma'=>'1','data-forma-def'=>'1', 'data-type'=>'text','data-required'=>'1', 'maxlength'=>'255'))}}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][sufix]', $prefix_suffix['suffix'], '', $attributes=array('id'=>'GuestSufix', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1', 'data-type'=>'select'))}}
                                        </div>
                                    </div>
                                </div>
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('GuestBio', 'Bio', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                               <!--  <textarea id="elm1" name="data[Profile][bio]"></textarea> -->
                                                {{ Form::textarea('data[Guest][bio]', null, ['id' => 'GuestBio','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'tinymce', 'format'=>'input error', 'data-force-validation'=>'1']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- End editor -->
                                <div class="form-group row">
                                    {{Form::label('GuestPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'photo')
                                                    <div class="col-sm-4 adding-medias media_Guest_photo" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'Guest', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_player" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="Guest" data-submodule="photo"></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_Guest_photo_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Guest" data-submodule="photo" data-limit="1" data-dimension="" data-media_type="image" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('HostHasManyUrls', 'Urls and Social Media', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table url_table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Type</div>
                                                        </th>
                                                        <th>
                                                            <div>Url</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                                        <td>
                                                            {{Form::select('data[Url][1][type]', $social_media, '', $attributes=array('id'=>'Url1Type', 'class'=>'form-control','data-forma-def'=>'1'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::url('data[Url][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'Url1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 host_extra_urls" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="GuestHasManyUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End row -->
                                
                                <div class="form-group row">
                                    {{Form::label('HostHasManyVideos', 'Video Clips', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table video_table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Title</div>
                                                        </th>
                                                        <th>
                                                            <div>Video HTML code</div>
                                                        </th>
                                                        <th>
                                                            <div>Enabled</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr data-id="" data-off="1" style="display: table-row;">
                                                        <td>
                                                            {{Form::text('data[Video][1][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'Video1Title'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::textarea('data[Video][1][code]', '',  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video1Code' ))}}
                                                        </td>
                                                         <td>
                                                            <div class="checkbox checkbox-pink checkbox-inline">
                                                                {{ Form::checkbox('data[Video][1][is_enabled]', '1' , false, array('id'=>'Video1IsEnabled1')) }}
                                                                {{Form::label('Video1IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                            </div>
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 host_videos" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyVideosAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('GuestEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::email('data[User][email]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'GuestEmail', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'email'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][phone]', $value = null, $attributes = array('class'=>'form-control', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPhone','maxlength'=>'45'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestCellphone', 'Cell phone', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][cellphone]', $value = null, $attributes = array('class'=>'form-control required','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text','maxlength'=>'45', 'id'=>'GuestCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'45', 'id'=>'GuestSkype','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype_phone]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'45', 'id'=>'GuestSkypePhone','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestContact', 'To be contacted', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="checkbox checkbox-purple checkbox-inline">
                                            {{ Form::checkbox('data[Guest][to_be_contacted]', '1' , true , array('id'=>'GuestToBeContacted')) }}
                                            {{Form::label('GuestToBeContacted', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Other Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('GuestPrType', 'Type', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'pr', true, array('id'=>'GuestPrTypePr'))}}
                                            {{Form::label('GuestPrTypePr', 'Pr', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-warning radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'publisher', false, array('id'=>'GuestPrTypePublisher'))}}
                                            {{Form::label('GuestPrTypePublisher', 'Publisher', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-purple radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'assistant', false, array('id'=>'GuestPrTypeAssistant'))}}
                                            {{Form::label('GuestPrTypeAssistant', 'Assistant', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-custom radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'other', false, array('id'=>'GuestPrTypeOther'))}}
                                            {{Form::label('GuestPrTypeOther', 'Other', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('GuestPrName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_name]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'255','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text','id'=>'GuestPrName'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('GuestPrCompanyName', 'Company Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_company_name]', '',$attributes = array('class'=>'form-control', 'id'=>'GuestPrCompanyName','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'maxlength'=>'255'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::email('data[Guest][pr_email]', $value = null, $attributes = array('class'=>'form-control','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'email', 'id'=>'GuestPrEmail'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrPhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_phone]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'45', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text', 'id'=>'GuestPrPhone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrCellphone', 'Cell phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_cellphone]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'45','data-forma'=>'1','data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPrCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_skype]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'255','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPrSkype'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_skype_phone]', $value = null, $attributes = array('class'=>'form-control', 'maxlength'=>'45','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPrSkypePhone'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Settings</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('GuestIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            {{ Form::radio('data[Guest][is_online]', '1', true, array('id'=>'GuestIsOnline1'))}}
                                            {{Form::label('GuestIsOnline1', 'Online', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{ Form::radio('data[Guest][is_online]', '0', false, array('id'=>'GuestIsOnline0'))}}
                                            {{Form::label('GuestIsOnline0', 'Offline', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                 <div class="form-group row">
                                    {{Form::label('GuestHostsId', 'Is Host / Co-host ?', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::select('data[Guest][hosts_id]',[''=>'selected'],'', $attributes = array('class'=>'form-control HostHost','id'=>'GuestHostsId','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'tokenInput'))}}
                                    </div>
                                     <span style="display: none;" class="loading">
                                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                                    </span>                                    
                                </div><!-- end row -->

                                <div class="form-group row">
                                    {{Form::label('guestHasManyCategories', 'Category / Sub-Category', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <?php //echo "<pre>";print_R(json_decode($itunes_categories));?>
                                    <div class="col-sm-7">                                     
                                        <div class="outside_itunes_cat" data-off="1">
                                            <div class="col-sm-5">
                                                {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories, '', $attributes=array('id'=>'Category1Category0','class'=>'form-control guests_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','required'))}}
                                            </div>
                                            <div class="col-sm-7">
                                                {{Form::select('data[Category][Category][]',[''=>'none'], '', $attributes=array('id'=>'Category1Category1','class'=>'form-control guest_sub_cate', 'multiple'=> 'multiple','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                            </div>
                                          <!--   <div class="col-sm-2 itunes_cat" style="display: block;">
                                                <i class="typcn typcn-delete"></i> 
                                            </div> -->
                                        </div>
                                        <!-- <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="GuestHasManyCategoriesAdd"><i class="glyphicon glyphicon-plus"></i>
                                                <span>add</span>
                                            </button>
                                        </div> -->
                                    </div>
                                </div><!-- End row -->
                                {{Form::hidden('data[User][groups_id]', 5, ['id'=>'UserGroupsId'])}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="form-group">
                    <div class="fixed_btn">
                        <button  type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Guest][btnAdd]" id="GuestBtnAdd" value ="add" data-form-id="GuestAdminAddForm">
                            Add
                        </button>         
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
         @include('backend.cms.upload_media_popup',['module_id' =>0,'main_module' => "Guest"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
