@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Guest</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li class="active">
                            <a href="{{url('admin/show/guests')}}">All Guests</a>
                        </li>
                        <li class="active">
                            Edit Guest
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div id="yes_crop"></div>
        <div class="row">
            <!-- Form Starts-->  
            @if(Config::get('constants.ADMIN') == getCurrentUserType())
            {{ Form::open(array('url' => '/admin/show/guests/edit/'.$guest->id, 'id'=>'GuestAdminEditForm')) }}
            @endif
            @if(Config::get('constants.HOST') == getCurrentUserType())
            {{ Form::open(array('url' => '/host/show/guests/edit/'.$guest->id, 'id'=>'GuestAdminEditForm')) }}
            @endif
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Personal Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('HostD', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::hidden('form_type', 'edit', array('id'=>'form_type'))}}
                                        {{Form::hidden('edit_id', $guest->id, array('id'=>'edit_id'))}}
                                       
                                        {{Form::hidden('data[Profile][id]', ($guest->user->count() > 0) ? ($guest->user->profiles->count() > 0 ? $guest->user->profiles[0]->id :"") : "")}}

                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][title]', $prefix_suffix['prefix'], ($guest->user->count() > 0) ? ($guest->user->profiles->count() > 0 ? $guest->user->profiles[0]->title :"") : "", $attributes=array('id'=>'GuestTitle', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1'))}}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][firstname]', ($guest->user->count() > 0) ? ($guest->user->profiles->count() > 0 ? $guest->user->profiles[0]->firstname :"") : "", $attributes = array('class'=>'form-control required', 'placeholder'=>'First name','required'=>true,'id'=>'GuestFirstname','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','data-required'=>'1', 'maxlength'=>'255'))}}
                                        </div>
                                        <div class="col-sm-3">
                                            {{Form::text('data[Profile][lastname]', ($guest->user->count() > 0) ? ($guest->user->profiles->count() > 0 ? $guest->user->profiles[0]->lastname :"") : "", $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name','required'=>true, 'id'=>'GuestLastname','data-forma'=>'1','data-forma-def'=>'1', 'data-type'=>'text','data-required'=>'1', 'maxlength'=>'255'))}}
                                        </div>
                                        <div class="form-group col-sm-3">
                                            {{Form::select('data[Profile][sufix]',$prefix_suffix['suffix'], ($guest->user->count() > 0) ? ($guest->user->profiles->count() > 0 ? $guest->user->profiles[0]->sufix :"") : "", $attributes=array('id'=>'GuestSufix','class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1', 'data-type'=>'select'))}}
                                        </div>
                                    </div>
                                </div>
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('GuestBio', 'Bio', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            <form method="post">
                                               <!--  <textarea id="elm1" name="data[Profile][bio]"></textarea> -->
                                                {{ Form::textarea('data[Guest][bio]',$guest->bio, ['id' => 'GuestBio','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'tinymce', 'format'=>'input error', 'data-force-validation'=>'1']) }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- End editor -->
                                <div class="form-group row">
                                    {{Form::label('GuestPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        <div class="row">
                                            @if($media->count() > 0)
                                                @php ($url_count = 1)
                                                @foreach($media as $med)
                                                    @if($med->sub_module == 'photo')
                                                        <div class="col-sm-4 adding-medias media_Guest_photo" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'Guest', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Channel_player" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="Guest" data-submodule="photo"></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                        @php ($url_count++)
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="media_Guest_photo_outer hidden" data-off="0" style="display: none;">
                                                </div>
                                        </div>
                                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                           Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                                        </span>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Guest" data-submodule="photo"  data-limit="1" data-dimension="" data-media_type="image" data-file_crop="1">Upload files</button>
                                        </div> 
                                    </div>
                                </div>
                                <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('GuestHasManyUrls', 'Urls and Social Media', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table url_table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Type</div>
                                                        </th>
                                                        <th>
                                                            <div>Url</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($guest->urls->count() > 0)
                                                    @php ($url_count = 1)
                                                    @foreach($guest->urls as $url)
                                                     <tr data-off="{{$url_count}}" style="display: table-row;">
                                                        <td>
                                                            {{Form::hidden('data[Url]['.$url_count.'][id]', $url->id)}}
                                                            {{Form::select('data[Url]['.$url_count.'][type]', $social_media, $url->type, $attributes=array('id'=>'Url'.$url_count.'Type', 'class'=>'form-control'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::url('data[Url]['.$url_count.'][url]', $url->url,  array('step' => '1', 'min' => '1', 'id' => 'Url'.$url_count.'Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 {{($url_count > 1) ? 'delete_guest_extra_urls' : 'guset_extra_urls'}}" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php ($url_count++)
                                                    @endforeach
                                                    @else
                                                    <tr data-off="1" style="display: table-row;" data-rnd="">
                                                        <td>
                                                            {{Form::select('data[Url][1][type]', $social_media, '', $attributes=array('id'=>'Url1Type', 'class'=>'form-control'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::url('data[Url][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'Url1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 guset_extra_urls" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </tbody>                                    
                                            </table>                                          
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="GuestHasManyUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End row -->
                                
                                <div class="form-group row">
                                    {{Form::label('GuestHasManyVideos', 'Video Clips', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table video_table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Title</div>
                                                        </th>
                                                        <th>
                                                            <div>Video HTML code</div>
                                                        </th>
                                                        <th>
                                                            <div>Enabled</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($guest->videos->count() > 0)
                                                    @php ($video_count = 1)
                                                    @foreach($guest->videos as $video)
                                                    <tr data-off="{{$video_count}}" style="display: table-row;">
                                                        <td>
                                                            {{Form::hidden('data[Video]['.$video_count.'][id]', $video->id)}}
                                                            {{Form::text('data[Video]['.$video_count.'][title]', $video->title, $attributes = array('class'=>'form-control', 'id'=>'Video'.$video_count.'Title'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::textarea('data[Video]['.$video_count.'][code]', $video->code,  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video'.$video_count.'Code' ))}}
                                                        </td>
                                                         <td>
                                                            <div class="checkbox checkbox-pink checkbox-inline">
                                                                {{ Form::checkbox('data[Video]['.$video_count.'][is_enabled]', '1' , ($video->is_enabled == 1 ? true : false), array('id'=>'Video'.$video_count.'IsEnabled1')) }}
                                                                {{Form::label('Video'.$video_count.'IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                            </div>
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 {{($video_count > 1) ? 'delete_guest_videos' : 'guest_videos'}}" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php ($video_count++)
                                                    @endforeach
                                                    @else
                                                    <tr data-id="" data-off="1" style="display: table-row;">
                                                        <td>
                                                            {{Form::text('data[Video][1][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'Video1Title'))}}
                                                        </td>
                                                        <td>
                                                            {{Form::textarea('data[Video][1][code]', '',  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video1Code' ))}}
                                                        </td>
                                                         <td>
                                                            <div class="checkbox checkbox-pink checkbox-inline">
                                                                {{ Form::checkbox('data[Video][1][is_enabled]', '1' , false, array('id'=>'Video1IsEnabled1')) }}
                                                                {{Form::label('Video1IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                            </div>
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 host_videos" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </tbody>                                                
                                            </table>                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="GuestHasManyVideosAdd"><i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('GuestEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::hidden('data[User][id]', $guest->user->id)}}
                                        {{Form::email('data[User][email]', $guest->user->email, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'GuestEmail', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'email'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][phone]', ($guest->user->profiles->count() > 0) ? $guest->user->profiles[0]->phone : null, $attributes = array('class'=>'form-control', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPhone','maxlength'=>'45'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestCellphone', 'Cell phone', array('class' => 'col-sm-4 form-control-label req'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][cellphone]', ($guest->user->profiles->count() > 0) ? $guest->user->profiles[0]->cellphone : null, $attributes = array('class'=>'form-control required','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text','maxlength'=>'45', 'id'=>'GuestCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype]', ($guest->user->profiles->count() > 0) ? $guest->user->profiles[0]->skype : null, $attributes = array('class'=>'form-control', 'maxlength'=>'45', 'id'=>'GuestSkype','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Profile][skype_phone]', ($guest->user->profiles->count() > 0) ? $guest->user->profiles[0]->skype_phone : null, $attributes = array('class'=>'form-control', 'maxlength'=>'45', 'id'=>'GuestSkypePhone','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestContact', 'To be contacted', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="checkbox checkbox-purple checkbox-inline">
                                            {{ Form::checkbox('data[Guest][to_be_contacted]', '1' , ($guest->to_be_contacted == 1) ? true : false , array('id'=>'GuestToBeContacted')) }}
                                            {{Form::label('GuestToBeContacted', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Other Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('GuestPrType', 'Type', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'pr',(($guest->pr_type == "pr") ? true : false), array('id'=>'GuestPrTypePr'))}}
                                            {{Form::label('GuestPrTypePr', 'Pr', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-warning radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'publisher',(($guest->pr_type == "publisher") ? true : false), array('id'=>'GuestPrTypePublisher'))}}
                                            {{Form::label('GuestPrTypePublisher', 'Publisher', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-purple radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'assistant',(($guest->pr_type == "assistant") ? true : false), array('id'=>'GuestPrTypeAssistant'))}}
                                            {{Form::label('GuestPrTypeAssistant', 'Assistant', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-custom radio-inline">
                                            {{Form::radio('data[Guest][pr_type]', 'other',(($guest->pr_type == "other") ? true : false), array('id'=>'GuestPrTypeOther'))}}
                                            {{Form::label('GuestPrTypeOther', 'Other', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('GuestPrName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_name]', $guest->pr_name, $attributes = array('class'=>'form-control', 'maxlength'=>'255','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text','id'=>'GuestPrName'))}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('GuestPrCompanyName', 'Company Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_company_name]',$guest->pr_company_name,$attributes = array('class'=>'form-control', 'id'=>'GuestPrCompanyName','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'maxlength'=>'255'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::email('data[Guest][pr_email]',$guest->pr_email, $attributes = array('class'=>'form-control','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'email', 'id'=>'GuestPrEmail'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrPhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_phone]',$guest->pr_phone, $attributes = array('class'=>'form-control', 'maxlength'=>'45', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text', 'id'=>'GuestPrPhone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrCellphone', 'Cell phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_cellphone]',$guest->pr_cellphone, $attributes = array('class'=>'form-control', 'maxlength'=>'45','data-forma'=>'1','data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPrCellphone'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_skype]',$guest->pr_skype, $attributes = array('class'=>'form-control', 'maxlength'=>'255','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPrSkype'))}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {{Form::label('GuestPrSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Guest][pr_skype_phone]',$guest->pr_skype_phone, $attributes = array('class'=>'form-control', 'maxlength'=>'45','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'text', 'id'=>'GuestPrSkypePhone'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Settings</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('GuestIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="radio radio-info radio-inline">
                                            {{ Form::radio('data[Guest][is_online]', '1',(($guest->is_online == "1") ? true : false), array('id'=>'GuestIsOnline1'))}}
                                            {{Form::label('GuestIsOnline1', 'Online', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                        <div class="radio radio-inline">
                                            {{ Form::radio('data[Guest][is_online]', '0',(($guest->is_online == "0") ? true : false), array('id'=>'GuestIsOnline0'))}}
                                            {{Form::label('GuestIsOnline0', 'Offline', array('class' => 'col-sm-4 form-control-label'))}}
                                        </div>
                                    </div>
                                </div><!-- end row -->
                                 <div class="form-group row">
                                    {{Form::label('GuestHostsId', 'Is Host / Co-host ?', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::select('data[Guest][hosts_id]',[''=>'selected'],$guest->hosts_id, $attributes = array('class'=>'form-control HostHost','id'=>'GuestHostsId','data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'tokenInput'))}}
                                    </div>
                                     <span style="display: none;" class="loading">
                                        <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                                    </span>                                    
                                </div><!-- end row -->
 
                                 <div class="form-group row">
                                    {{Form::label('guestHasManyCategories', 'Category / Sub-Category', array('class' => 'col-sm-4 form-control-label req'))}}
                                   
                                    <div class="col-sm-7">   
                                    @if(!empty($assigned_categories))

                                        @php ($guests_cat_count = 1)
                                        @foreach($assigned_categories as $guests_cat) 
                                        @if(array_key_exists($guests_cat,$all_categories))
                                            <div class="outside_guests_cat" data-off="{{$guests_cat_count}}">
                                                <div class="col-sm-5">
                                                    {{Form::hidden('data[Category][select_main_id]', $guests_cat, ['id'=>'select_main_id'])}}

                                                    {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories,$guests_cat, $attributes=array('id'=>'Category'.$guests_cat_count.'Category0','class'=>'form-control guests_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','required'))}}
                                                </div>                                           
                                                <div class="col-sm-7">
                                                    <?php $fetch_subcategories = subCategories($guests_cat,$assigned_categories); ?>
                                                    {{Form::hidden('data[Category][select_sub_id]', !empty($fetch_subcategories['selected_cat']) ? json_encode($fetch_subcategories['selected_cat']) : '', ['id'=>'select_sub_id'])}}

                                                    {{Form::select('data[Category][Category][]', $fetch_subcategories['subcategories'], (isset($fetch_subcategories['selected_cat'])) ? $fetch_subcategories['selected_cat'] : '', $attributes=array('id'=>'Category'.$guests_cat_count.'Category1',  'multiple'=> 'multiple', 'class'=>'form-control guest_sub_cate','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                                </div>
                                                <!-- <div class="col-sm-2 {{($guests_cat_count > 1) ? 'delete_guests_cat' : 'guests_cat'}}" style="display: block;">
                                                    <i class="typcn typcn-delete"></i> 
                                                </div> -->
                                            </div>
                                        @endif
                                        @php ($guests_cat_count++)
                                        @endforeach
                                        @else
                                        <div class="outside_guests_cat" data-off="1">
                                            <div class="col-sm-5">
                                                {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories, '', $attributes=array('id'=>'Category1Category0','class'=>'form-control guests_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','required'))}}
                                            </div>
                                            <div class="col-sm-5">
                                                {{Form::select('data[Category][Category][]',[''=>'none'], '', $attributes=array('id'=>'Category1Category1','class'=>'form-control guest_sub_cate', 'multiple'=> 'multiple', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                            </div>
                                           <!--  <div class="col-sm-2 {($guests_cat_count > 1) ? 'delete_guests_cat' : 'guests_cat'}}" style="display: block;">
                                                <i class="typcn typcn-delete"></i> 
                                            </div> -->
                                        </div>
                                        @endif   
                                                                         
                                        <!-- <div class="mtf-buttons" style="clear:both;float:right;">
                                            <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="GuestHasManyCategoriesAdd"><i class="glyphicon glyphicon-plus"></i>
                                                <span>add</span>
                                            </button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="form-group">
                    <div class="fixed_btn">
                        <button  type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Guest][btnAdd]" id="GuestBtnAdd" value ="add" data-form-id="GuestAdminEditForm">
                            Edit
                        </button>         
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
         @include('backend.cms.upload_media_popup',['module_id' =>$guest->id,'main_module' => "Guest"])
    </div> <!-- container -->
</div> <!-- content -->

@endsection
