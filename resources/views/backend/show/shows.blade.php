@extends('backend.layouts.main')
@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Shows</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>                        
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->    
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <div class="form-group row page_top_btns">
                    <div class="col-md-4">
                        {{Form::select('data[Channel][f][id]', (!empty($all_channels) ? $all_channels : ''), !empty($selected_channel) ? $selected_channel: '', $attributes=array('id'=>'ChannelFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>
                    <div class="col-md-6">   
                    </div>
                    <div class="col-md-2"> 
                        <div style="float:right;">
                            <a href="{{url('/admin/show/shows/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Show</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- end row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="shows_datatable" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Host(s) / Co-Host(s)</th>
                                <th>Channel(s)</th>
                                <th></th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div><!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
