@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Shows Archive</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            @if(Config::get('constants.HOST') == getCurrentUserType())
                                <a href="{{url('host')}}">Dashboard</a>
                            @endif
                            @if(Config::get('constants.ADMIN') == getCurrentUserType())
                                <a href="{{url('admin')}}">Dashboard</a>
                            @endif
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li class="active">
                            Archived
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row --> 
        
        <!-- Channel Filter-->
        <div class="row">
            <div class="card-box">                 
                @if(Config::get('constants.ADMIN') == getCurrentUserType())  
                <div class="form-group row"> 
                    <div class="col-md-4">
                        {{Form::select('data[Archive][f][channels_id]', (!empty($all_channels) ? $all_channels : ''), !empty($selected_channel) ? $selected_channel : '', $attributes=array('id'=>'ArchiveFChannelsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>                    
                    <div class="col-md-4">
                        {{Form::select('data[Archive][f][shows_id]', (!empty($all_shows) ? $all_shows : ''), !empty($selected_show) ? $selected_show : '', $attributes=array('id'=>'ArchiveFShowsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-custom'))}}
                    </div>
                    <div class="col-md-4">
                        {{Form::select('data[Archive][f][host_ids]', (!empty($all_hosts) ? $all_hosts : ''), !empty($selected_host) ? $selected_host : '', $attributes=array('id'=>'ArchiveFHostIds', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-teal'))}}
                    </div>
                </div>
                @endif  
                @if(Config::get('constants.HOST') == getCurrentUserType())
                <div class="form-group row page_top_btns"> 
                    <div class="col-md-12">
                        {{Form::select('data[Archive][f][channels_id]', (!empty($all_channels) ? $all_channels : ''), !empty($selected_channel) ? $selected_channel : '', $attributes=array('id'=>'ArchiveFChannelsId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
                    </div>
                </div>
                @endif
                
                @if(Config::get('constants.ADMIN') == getCurrentUserType())
                <div class="form-group row page_top_btns">   
                    <div class="col-md-4">
                        {{Form::select('data[Archive][f][year]', (!empty($years) ? $years : ''), !empty($selected_year) ? $selected_year : '', $attributes=array('id'=>'ArchiveFYear', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-pink'))}}
                    </div>
                    <div class="col-md-4">
                        {{Form::select('data[Archive][f][month]', (!empty($months) ? $months : ''), !empty($selected_month) ? $selected_month : '', $attributes=array('id'=>'ArchiveFMonth', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-info'))}}
                    </div>
                    <div class="col-md-4">                        
                        {!!$all_categories!!}
                    </div>            
                </div>       
                @endif             
            </div>
        </div> 
        <!-- end row -->
        <div class="row">
            
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->
                    <table id="archived_episodes_datatable" class="table table-striped table-bordered">
                        <thead>                       
                            <tr>
                                <th>Date</th>
                                <th>Title</th>
                                @if($type == 'host')
                                <th>Guests</th>
                                @endif
                                @if($type == 'admin')
                                <th>Encores</th>
                                <th>Media file</th>
                                @endif
                                <th></th>
                            </tr>
                        </thead>                       
                    </table>
                </div>
            </div>
        </div>
        <!-- end row --> 

    </div> <!-- container -->
</div> <!-- content -->
<!-- Encore Detail Modal -->
<div id="encore_detail" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Encores Details</h4>
    <div class="custom-modal-text">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-orange">Show</div>
                        <div class="col-md-9 show_title"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-primary">Episode</div>
                        <div class="col-md-9 episode_title"></div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-pink">Orginal Air Date</div>
                        <div class="col-md-9 episode_date"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-3 text-teal">Encore Dates</div>
                        <div class="col-md-9 episode_encores"></div>
                    </div>
                </div>
            </div>  
        </div>      
    </div>
</div>
<a href="#encore_detail" style="display:none;" id="open_encore_detail" class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" data-animation="blur" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a">
@endsection
