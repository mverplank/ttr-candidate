@extends('backend.layouts.main')
@section('content')
<style>
   #mceu_21-body{display:none;}
   .cropper-container.cropper-bg {
   width: 100%;
   }
</style>
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add Show</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="active">
                     Users
                  </li>
                  <li class="active">
                     <a href="{{url('admin/show/shows')}}">All Shows</a>
                  </li>
                  <li class="active">
                     Add Show
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <!-- end row -->      
      <div class="row">
         <!-- Form Starts-->  
         {{ Form::open(array('url' => 'admin/cms/pages/add', 'id' => 'ShowAdminAddForm')) }}
         <div class="col-xs-12">
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                     <h4 class="header-title m-t-0">Details</h4>
                     <div class="p-20">
                        <div class="form-group row">
                           {{Form::hidden('form_type', 'add', array('id'=>'form_type'))}}
                           {{Form::label('ShowName', 'Name', array('class' => 'col-sm-4 form-control-label req'))}}
                           <div class="col-sm-7">
                              {{Form::text('data[Show][name]', $value = null, $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1', 'data-parsley-maxlength'=>'255', 'id'=>'ShowName', 'required'=>true,'maxlength' =>'255','data-form'=>'show','data-moduleid'=>'0'))}}
                              <div class="error-block" style="display:none;"></div>
                           </div>
                        </div>
                        <!-- Editor for Bio-->
                        <div class="form-group row">
                           {{Form::label('ShowDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              <div class="card-box">
                                 <form method="post">
                                    {{ Form::textarea('data[Show][description]', null, ['id' => 'ShowDescription']) }}
                                 </form>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('HostHost', 'Hosts', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              {{Form::select('data[Host][Host][]',[], '', $attributes = array('class'=>'form-control HostHost select2-multiple', 'multiple'=>'multiple', 'id'=> 'HostHost', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Host...'))}}
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>                                     
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('ShowCoHost', 'CoHosts', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              {{Form::select('data[Cohost][Cohost][]',[], '', $attributes = array('class'=>'form-control ShowCoHost select2-multiple', 'multiple'=>'multiple', 'id'=> 'ShowCoHost', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Cohost...'))}}
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>                                     
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('ShowPodcastHost', 'Podcast Host', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              {{Form::select('data[Podcasthost][Podcasthost][]',[], '', $attributes = array('class'=>'form-control ShowPodcastHost select2-multiple', 'multiple'=>'multiple', 'id'=> 'ShowPodcastHost', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Podcast Host...'))}}
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>                                     
                        </div>
                        <!-- End row -->
                        <!-- End editor -->
                        <div class="form-group row">
                           {{Form::label('ShowSchedule0', 'Banner 120x300 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0)
                                 @php ($url_count = 1)
                                 @foreach($media as $med)
                                 @if($med->sub_module == 'schedule')
                                 <div class="col-sm-4 adding-medias media_Show_schedule" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Show_schedule" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Show" data-submodule="schedule"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++)
                                 @endif
                                 @endforeach
                                 @endif
                                 <div class="media_Show_schedule_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                              To change order drag and drop thumbanils.
                              <span style="color:#f9c851;"> You can upload only 3 files.</span>
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Show" data-submodule="schedule" data-limit="3" data-dimension="120x300" data-media_type="image" data-file_crop="0">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('ShowPlayer0', 'Banner 625x258 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0)
                                 @php ($url_count = 1)
                                 @foreach($media as $med)
                                 @if($med->sub_module == 'player')
                                 <div class="col-sm-4 adding-medias media_Show_player" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Show_player" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="Show" data-submodule="player"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++)
                                 @endif
                                 @endforeach
                                 @endif
                                 <div class="media_Show_player_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                              To change order drag and drop thumbanils.
                              <span style="color:#f9c851;"> You can upload only 3 files.</span>
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Show" data-submodule="player" data-limit="3" data-dimension="625x258" data-media_type="image" data-file_crop="0">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('ShowFeatured0', 'Banner 120x150 [px]', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0)
                                 @php ($url_count = 1)
                                 @foreach($media as $med)
                                 @if($med->sub_module == 'featured')
                                 <div class="col-sm-4 adding-medias media_Show_featured" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Show_featured" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="Show" data-submodule="featured"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++)
                                 @endif
                                 @endforeach
                                 @endif
                                 <div class="media_Show_featured_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                              To change order drag and drop thumbanils.
                              <span style="color:#f9c851;"> You can upload only 3 files.</span>
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Show" data-submodule="featured" data-limit="3" data-dimension="120x150" data-media_type="image" data-file_crop="0">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('Audio', 'Audio', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0)
                                 @php ($url_count = 1)
                                 @foreach($media as $med)
                                 @if($med->sub_module == 'audio')
                                 <div class="col-sm-4 adding-medias media_Show_audio" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Show_audio" onclick="mediaLinkDelete({{$med->id}}, this);"data-mainmodule="Show" data-submodule="audio"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++)
                                 @endif
                                 @endforeach
                                 @endif
                                 <div class="media_Show_audio_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                              <span style="color:#f9c851;"> You can upload only 1 file.</span>
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Show" data-submodule="audio" data-limit="1" data-dimension="" data-media_type="audio" data-file_crop="0">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                     <h4 class="header-title m-t-0">Media</h4>
                     <div class="p-20">
                        <div class="form-group row">
                           {{Form::label('ShowStream', 'Remote file or stream url', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              {{Form::text('data[Show][stream]', $value = null, $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'ShowStream'))}}
                              <span class="help-block"> 
                              <i class="fa fa-info-circle" aria-hidden="true"></i> 
                              Full URL of remote static media file or stream.
                              If empty, file don't exists or can't connect to stream - below defined local file is used.
                              Examples:
                              static file: "custom-name.mp3" or "http://channel-domain.com/files/Shows/custom-name.mp3"
                              live stream: "http://205.188.234.38:8002"
                              </span>   
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                     <h4 class="header-title m-t-0">Settings</h4>
                     <div class="p-20">
                        <div class="form-group row">
                           {{Form::label('ShowIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              <div class="radio radio-info radio-inline">
                                 {{Form::radio('data[Show][is_online]', '1', true, array('id'=>'ShowIsOnline1','value'=>'1'))}}
                                 {{Form::label('ShowIsOnline1', 'Online', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio radio-inline">
                                 {{Form::radio('data[Show][is_online]', '0', false, array('id'=>'ShowIsOnline0','value'=>'0'))}}
                                 {{Form::label('ShowIsOnline0', 'Offline', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ShowIsFeatured', 'Featured', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              <div class="radio radio-warning radio-inline">
                                 {{Form::radio('data[Show][is_featured]', '1', false, array('id'=>'ShowIsFeatured1','value'=>'1'))}}
                                 {{Form::label('ShowIsFeatured1', 'Yes', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio radio-inline">
                                 {{Form::radio('data[Show][is_featured]', '0', true, array('id'=>'ShowIsFeatured0','value'=>'0'))}}
                                 {{Form::label('ShowIsFeatured0', 'No', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ShowIsEncore', 'Encore', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              <div class="radio radio-purple radio-inline">
                                 {{Form::radio('data[Show][is_encore]', '1', false, array('id'=>'ShowIsEncore1','value'=>'1'))}}
                                 {{Form::label('ShowIsEncore1', 'Yes', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio radio-inline">
                                 {{Form::radio('data[Show][is_encore]', '0', true, array('id'=>'ShowIsEncore0','value'=>'0'))}}
                                 {{Form::label('ShowIsEncore0', 'No', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('ChannelChannelSet', 'Channels', array('class' => 'col-sm-4 form-control-label'))}}
                           <div class="col-sm-7">
                              @if(!empty($all_channels))
                              @foreach ( $all_channels as $i =>$item )
                              <div class="checkbox checkbox-pink">
                                 {!! Form::checkbox( 'data[Channel][Channel][]', $i, '', ['class' => 'md-check', 'id' => 'ChannelChannel'.$i] ) !!}
                                 {!! Form::label('ChannelChannel'.$i,  $item) !!}
                              </div>
                              @endforeach
                              @else
                              Not any Channel existing yet.
                              @endif
                           </div>
                        </div>
                        <!-- end row -->
                        <div class="form-group row">
                           {{Form::label('showHasManyCategories', 'Category / Sub-Category', array('class' => 'col-sm-4 form-control-label req'))}}
                           <?php //echo "<pre>";print_R(json_decode($itunes_categories));?>
                           <div class="col-sm-7">
                              <div class="outside_itunes_cat" data-off="1">
                                 <div class="col-sm-5">
                                    {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories, '', $attributes=array('id'=>'Category1Category0','class'=>'form-control shows_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','required'))}}
                                 </div>
                                 <div class="col-sm-7">
                                    {{Form::select('data[Category][Category][]',[''=>'none'], '', $attributes=array('id'=>'Category1Category1','class'=>'form-control show_sub_cate', 'multiple'=> 'multiple','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- End row -->                                
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="form-group">
               <div class="fixed_btn">
                  <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Show][btnAdd]" id="ShowBtnAdd" data-form-id="ShowAdminAddForm">
                  Add
                  </a>
               </div>
            </div>
         </div>
         <!-- end col-->
         {{ Form::close() }}
         <!-- Form Close -->
      </div>
      <!-- end row -->
      @include('backend.cms.upload_media_popup',['module_id' =>0,'main_module' => "Show"])
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection