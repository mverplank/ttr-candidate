@extends('backend.layouts.main') @section('content')
<!--  <style>
   #mceu_21-body{display:none;}
   .cropper-container.cropper-bg {
   width: 100%;
   }
   </style> -->
@if (Session::has('success'))
<a href="#custom-modal1" class="trigger_modal btn btn-primary waves-effect waves-light m-r-5 m-b-10" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a" style="display: none;"></a>
<script src="{{ asset('public/assets/js/jquery.min.js') }}"></script>
<script type="text/javascript">
   $(document).ready(function() {
   $('#custom-modal1').modal({
   backdrop: 'static',
   keyboard: false
   });
   $('.trigger_modal').trigger('click');
   });
</script>
<!-- Modal -->
<div id="custom-modal1" class="modal-demo">
   <button type="button" class="close" onclick="window.location.href = &quot;{{url('admin/show/schedules/agenda')}}?channel_id={{$schedule->channels_id}}&quot;">
   <span>&times;</span>
   <span class="sr-only">Close</span>
   </button>
   <h4 class="custom-modal-title">Do you want to share this episode?</h4>
   <div class="modal-dialog dialog-width">
      <div class="custom-modal-text">
         <div class="form-group row">
            <label class="col-sm-3 control-label">Embed Code</label>
            <div class="col-sm-9">
               <input type="text" class="form-control" readonly="" onClick="this.select();" value="{{url('/')}}/episode/{{$response}}.html">
            </div>
         </div>
         <div class="form-group row">
            <div class="col-sm-12 set_button_margins">
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Facebook" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-facebook waves-effect waves-light">
               <i class="fa fa-facebook m-r-5"></i> Facebook
               </button>
               </a>
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Twitter" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-twitter waves-effect waves-light">
               <i class="fa fa-twitter m-r-5"></i> Twitter
               </button>
               </a>
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Google" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-googleplus waves-effect waves-light">
               <i class="fa fa-google-plus m-r-5"></i> Google+
               </button>
               </a>
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Linkedin" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-linkedin waves-effect waves-light">
               <i class="fa fa-linkedin m-r-5"></i> Linkedin
               </button>
               </a>
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Pinterest" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-pinterest waves-effect waves-light">
               <i class="fa fa-pinterest m-r-5"></i> Pinterest
               </button>
               </a>
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Email" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-dropbox waves-effect waves-light">
               <i class="fa fa-envelope m-r-5"></i> Email
               </button>
               </a>
               <a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Tumblr" target="_blank" style="float:left;margin:0 5px 5px;">
               <button type="button" class="btn btn-tumblr waves-effect waves-light">
               <i class="fa fa-tumblr m-r-5"></i> Tumblr
               </button>
               </a>
            </div>
         </div>
      </div>
      {{ Form::open(array('route' => 'add_episode_schedule', 'method' => 'POST', 'id'=>'EpisodeScheduleForm')) }}
      <div class="form-group row">
         <div class="col-sm-3">
            {{Form::hidden('data[EpisodeScheduledShare][type]','episode',['id'=>''])}}
            {{Form::hidden('data[EpisodeScheduledShare][episodes_id]',$response,['id'=>''])}} {{Form::hidden('data[EpisodeScheduledShare][channel_id]',$schedule->channels_id,['id'=>''])}} 
            {{ Form::checkbox('data[EpisodeScheduledShare][is_twitter]',1,null, array('id'=>'EpisodeScheduledShareIsTwitter1','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'checkbox','class'=>'required')) }}
             {!! Form::label('EpisodeScheduledShareIsTwitter1','Twitter') !!}
         </div>
         <div class="col-sm-3">
            {{Form::text('data[EpisodeScheduledShare][twitter_date]', '', array('class'=>'form-control datepicker','id'=>'twitter_date','data-forma'=>1,'data-forma-def'=>1))}}
         </div>
         <div class="col-sm-3">
            {{Form::text('data[EpisodeScheduledShare][twitter_time]', '', array('class'=>'form-control timepicker','id'=>'twitter_time','data-forma'=>1,'data-forma-def'=>1))}}
         </div>
         <div class="col-sm-3">
            {{Form::textarea('data[EpisodeScheduledShare][twitter_desc]', '', array('class'=>'form-control','id'=>'EpisodeScheduledShareTwitterDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
         </div>
      </div>
      <div class="form-group row">
         <div class="col-sm-3">
            {{ Form::checkbox('data[EpisodeScheduledShare][is_facebook]',1,null, array('id'=>'EpisodeScheduledShareIsfacebook1','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'checkbox','class'=>'required')) }} {!! Form::label('EpisodeScheduledShareIsfacebook1','Facebook') !!}
         </div>
         <div class="col-sm-3">
            {{Form::text('data[EpisodeScheduledShare][facebook_date]', '', array('class'=>'form-control datepicker','id'=>'facebook_date','data-forma'=>1,'data-forma-def'=>1))}}
         </div>
         <div class="col-sm-3">
            {{Form::text('data[EpisodeScheduledShare][facebook_time]', '', array('class'=>'form-control timepicker','id'=>'facebook_time','data-forma'=>1,'data-forma-def'=>1))}}
         </div>
         <div class="col-sm-3">
            {{Form::textarea('data[EpisodeScheduledShare][facebook_desc]', '', array('class'=>'form-control','id'=>'EpisodeScheduledShareFacebookDesc','data-forma'=>1,'data-forma-def'=>1, 'data-type'=>'textarea', 'rows'=>2, 'cols'=>30 ))}}
         </div>
      </div>
      <div class="form-group row">
         <div class="col-sm-8 col-sm-offset-4">
            <button type="button" id="btnSharedSheduleEpisode" name="data[EpisodeScheduledShare][btnSave]" value="edit" class="btn btn-primary waves-effect waves-light submit_form" data-form-id="EpisodeScheduleForm" disabled>Save
            </button>
         </div>
      </div>
      {{ Form::close() }}
   </div>
</div>
<!-- Modal -->
<!-- <div class="modal fade in" id="myModal" role="dialog" style="display: block;"><div class="modal-dialog">
   // Modal content
   <div class="modal-content"><div class="modal-header"><a type="button" class="close" data-dismiss="modal" href="{{url('admin/show/schedules/agenda')}}">&times;</a><h4 class="modal-title">Are you want share this episode?</h4></div><div class="modal-body"><div class="form-group row"><div class="col-sm-12"><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Facebook" target="_blank"><button type="button" class="btn btn-facebook waves-effect waves-light"><i class="fa fa-facebook m-r-5"></i> Facebook
   </button></a><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Twitter" target="_blank"><button type="button" class="btn btn-twitter waves-effect waves-light"><i class="fa fa-twitter m-r-5"></i> Twitter
   </button></a><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Google" target="_blank"><button type="button" class="btn btn-googleplus waves-effect waves-light"><i class="fa fa-google-plus m-r-5"></i> Google+
   </button></a><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Linkedin" target="_blank"><button type="button" class="btn btn-linkedin waves-effect waves-light"><i class="fa fa-linkedin m-r-5"></i> Linkedin
   </button></a><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Pinterest" target="_blank"><button type="button" class="btn btn-pinterest waves-effect waves-light"><i class="fa fa-pinterest m-r-5"></i> Pinterest
   </button></a><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Email" target="_blank"><button type="button" class="btn btn-dropbox waves-effect waves-light"><i class="fa fa-envelope m-r-5"></i> Email
   </button></a><a href="{{url('/')}}/admin/show/episodes/share/{{$response}}/Tumblr" target="_blank"><button type="button" class="btn btn-tumblr waves-effect waves-light"><i class="fa fa-tumblr m-r-5"></i> Tumblr
   </button></a></div></div>
   // End Row 
   </div><div class="modal-footer"><a type="button" class="btn btn-default" data-dismiss="modal" href="{{url('admin/show/schedules/agenda')}}">Close</a></div></div></div></div> -->
@endif @if (Session::has('error'))
<div class="alert alert-danger">
   {{ Session::get('error') }}
</div>
@endif
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add Episode</h4>
               <ol class="breadcrumb p-0 m-0">
                  @if(Config::get('constants.ADMIN') == getCurrentUserType())
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="active">
                     <a href="{{url('admin/show/shows')}}">Shows</a>
                  </li>
                  <li class="active">
                     <a href="{{url('admin/show/schedules/agenda')}}">All Shows</a>
                  </li>
                  @endif @if(Config::get('constants.HOST') == getCurrentUserType())
                  <li>
                     <a href="{{url('host')}}">Dashboard</a>
                  </li>
                  <li class="active">
                     Shows
                  </li>
                  @endif
                  <li class="active">
                     Add Episode
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="card-box">
            <div class="form-group row page_top_btns">
               <div class="col-md-12">
                  <div style="float:right;">
                     @if(Config::get('constants.ADMIN') == getCurrentUserType())
                     <a href="{{url('/admin/show/episodes/select_encore/'.$schedule->id.'/'.$date)}}?show_id={{$schedule->shows_id}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" id="btnAddEncore">Add Encore</a> @endif @if(Config::get('constants.HOST') == getCurrentUserType())
                     <a href="{{url('/host/show/episodes/select_encore/'.$schedule->id.'/'.$date)}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" id="btnAddEncore">Add Encore</a> @endif
                     <a href="javascript:void(0)" onclick="toggleShows()" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5" id="btnAddEncore">Override Show</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end row -->
      <!--  add hosts export form by parmod -->
      <div class="row" style="display: none;" id="override_with_shows">
         <div class="col-sm-12">
            <div class="card-box">
               <h4 class="header-title m-t-0">Override Episode Show</h4>
               <div class="p-20">
                  <div class="form-group row">
                     {{Form::label('OverrideShowsId', 'Select Show', array('class' => 'form-control-label col-sm-2'))}}
                     <div class="col-sm-10">
                        {{Form::select('data[Override][shows_id]', $all_shows,'', $attributes=array('id'=>'OverrideShowsId', 'class'=>'selectpicker m-b-0', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'select'))}}
                        <input type="hidden" name="lebel" value="undefined" id="eport_lebel">
                     </div>
                  </div>
                  <!-- <div class="form-group row"><div class="col-sm-7 col-sm-offset-5"><button type="submit" class="btn btn-primary waves-effect waves-light submit_form" value="Add" id="HostsExportSubmit">Export</button></div></div>  -->
               </div>
            </div>
         </div>
      </div>
      <!-- form end  -->
      <div class="row">
         @if($schedule)
         <!-- Form Starts-->
         {{ Form::open(array('url' => 'admin/show/episodes/add_agenda/'.$schedule->id.'/'.$date.'/'.$day, 'id'=>'EpisodeAdminAddAgendaForm')) }}
         <div class="col-xs-12">
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                     <h4 class="header-title m-t-0">
                        <span id="schedule_channel"></span>
                     </h4>
                     <div class="p-20">
                        <div class="form-group row">
                           {{Form::hidden('data[Episode][channels_id]', $schedule->channels_id, ['id'=>'channel_schedule_id'])}} {{Form::hidden('data[Episode][shows_id]', $schedule->shows_id, ['id'=>'episode_shows_id'])}} {{Form::hidden('data[Episode][schedules_id]', $schedule->id, ['id'=>'schedules_id'])}} {{Form::hidden('', 'add', ['id'=>'form_type'])}} {{Form::hidden('data[Schedule][Schedules][]', $schedule->id, ['id'=>'SchedulesSchedules'])}} {{Form::label('ScheduleDow', 'Date', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-md-3">
                              @php ($day_name='') @if($day == 'Sun') @php ($day_name = 'Sunday') @elseif($day == 'Mon') @php ($day_name = 'Monday') @elseif($day == 'Tue') @php ($day_name = 'Tuesday') @elseif($day == 'Wed') @php ($day_name = 'Wednesday') @elseif($day == 'Thu') @php ($day_name = 'Thursday') @elseif($day == 'Fri') @php ($day_name = 'Friday') @elseif($day == 'Sat') @php ($day_name = 'Saturday') @endif {{Form::text('data[Schedule][day]', $day_name, $attributes = array('class'=>'form-control', 'id'=>'EpisodeDayView', 'readonly'=>''))}}
                           </div>
                           <div class="col-md-3">
                              {{Form::text('data[Episode][date]', Carbon::parse($date)->format('m/d/Y'), $attributes = array('class'=>'form-control', 'id'=>'EpisodeDateView', 'readonly'=>''))}}
                           </div>
                           <div class="col-md-2">
                              {{Form::text('data[Episode][time]', Carbon::parse($schedule->time)->format('g:i a'), $attributes = array('class'=>'form-control', 'id'=>'EpisodeTimeView', 'readonly'=>''))}}
                           </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('ShowNameView', 'Show', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              {{Form::text('data[Schedule][show_name]', ($schedule->show) ? $schedule->show->name: '', $attributes = array('class'=>'form-control', 'id'=>'ShowNameView','readonly'=>''))}}
                           </div>
                        </div>
                        <!-- End Row -->
                        <!--  <div class="form-group row">
                           {{Form::label('ShowNameView', 'Show', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                           {{Form::text('data[Schedule][show_name]', ($schedule->show) ? $schedule->show->name: '', $attributes = array('class'=>'form-control', 'id'=>'ShowNameView','readonly'=>''))}}
                           </div></div> -->
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('EpisodeAssignedType', 'Type', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-9">
                              <div class="radio radio-info radio-inline">
                                 {{Form::radio('data[Episode][assigned_type]', 'live', true, array('id'=>'EpisodeAssignedTypeLive','value'=>'live'))}} {{Form::label('EpisodeAssignedTypeLive', 'Live', array('class' => 'col-sm-4 form-control-label'))}}
                              </div>
                              <div class="radio radio radio-inline">
                                 {{Form::radio('data[Episode][assigned_type]', 'pre-recorded', false, array('id'=>'EpisodeAssignedTypePre-recorded','value'=>'pre-recorded'))}} {{Form::label('EpisodeAssignedTypePre-recorded', 'Pre-recorded', array('class' => 'col-sm-12 form-control-label'))}}
                              </div>
                           </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('EpisodeTitle', 'Episode Title', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="form-group">
                                 {{ Form::textarea('data[Episode][title]', null, ['id' => 'EpisodeTitle', 'class'=>'form-control', 'rows'=>'2']) }}
                                 <span class="help-block">
                                 <i class="fa fa-info-circle" aria-hidden="true"></i>
                                 <span class="chars-c">Characters left: 
                                 <b>0</b>
                                 </span>
                                 <br/>The Episode Title should not contain Show name (It will be added automatically)
                                 </span>
                              </div>
                           </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('EpisodeDescription', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="form-group">
                                 {{ Form::textarea('data[Episode][description]', null, ['id' => 'EpisodeDescription', 'class'=>'form-control']) }}
                              </div>
                           </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('TagTag', 'Tags', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-7 tag-input-type">
                              <div class="form-group">
                                 {{Form::text('data[Tag][Tag]','', ['id'=>'TagTag', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                 <span class="help-block">
                                 <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values
                                 </span>
                              </div>
                           </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('EpisodeCover', 'Episode Image', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="row">
                                 @if($media->count() > 0) @php ($url_count = 1) @foreach($media as $med) @if($med->sub_module == 'cover')
                                 <div class="col-sm-4 adding-medias media_Episode_cover" data-off="{{$url_count}}">
                                    <div class="jFiler-items jFiler-row">
                                       <ul class="jFiler-items-list jFiler-items-grid">
                                          <li class="jFiler-item" data-jfiler-index="1">
                                             <div class="jFiler-item-container">
                                                <div class="jFiler-item-inner">
                                                   <div class="jFiler-item-thumb">
                                                      <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'cover', 'image', 0, '{{$med->media->filename}}',{{$med->id}})" target="_blank">
                                                         <div class="jFiler-item-thumb-image">
                                                            <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <div class="jFiler-item-assets jFiler-row">
                                                      <ul class="list-inline pull-right">
                                                         <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_cover" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="cover"></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 @php ($url_count++) @endif @endforeach @endif
                                 <div class="media_Episode_cover_outer hidden" data-off="0" style="display: none;"></div>
                              </div>
                              <span class="help-block">
                              <i class="fa fa-info-circle" aria-hidden="true"></i>
                              Only jpg/jpeg/png/gif files. Maximum 1 file. Maximum file size: 120MB.
                              </span>
                           </div>
                           <div class="col-sm-2">
                              <div class="mtf-buttons" style="clear:both;float:right;">
                                 <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="cover" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                              </div>
                           </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group row">
                           {{Form::label('HostHost', 'Hosts', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              {{Form::select('data[Host][Host][]',[], '', $attributes = array('class'=>'form-control HostHost select2-multiple', 'multiple'=>'multiple', 'id'=> 'HostHost', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Host...'))}}
                              <input type="hidden" name="data[Host][Name]" id="HostHostName" value="">
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('CohostCohost', 'CoHosts', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              {{Form::select('data[Cohost][Cohost][]',[], '', $attributes = array('class'=>'form-control ShowCoHost select2-multiple', 'multiple'=>'multiple', 'id'=> 'CohostCohost', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Cohost...'))}}
                              <input type="hidden" name="data[Cohost][Name]" id="ShowCoHostName" value="">
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('PodcasthostPodcasthost', 'Podcast Host', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              {{Form::select('data[Podcasthost][Podcasthost][]',[], '', $attributes = array('class'=>'form-control ShowPodcastHost select2-multiple', 'multiple'=>'multiple', 'id'=> 'PodcasthostPodcasthost', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Podcast Host...'))}}
                              <input type="hidden" name="data[Podcasthost][Name]" id="ShowPodcastHostName" value="">
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>
                        </div>
                        <!-- End row -->
                        <div class="form-group row">
                           {{Form::label('GuestGuest', 'Guests', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              {{Form::select('data[Guest][Guest][]', [], '', $attributes = array('class'=>'form-control ShowGuestHost select2-multiple', 'id'=> 'GuestGuest', 'multiple'=>'multiple','data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Guest...'))}}
                              <input type="hidden" name="data[Guest][Name]" id="ShowGuestHostName" value="">
                           </div>
                           <span style="display: none;" class="loading">
                           <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                           </span>
                        </div>
                        <div class="form-group row">
                           {{Form::label('EpisodeHasManyGiveaways', 'Giveaways', array('class' => 'col-sm-3 form-control-label'))}}
                           <div class="col-sm-8">
                              <div class="form-group episode_has_giveways" data-off="1">
                                 {{Form::textarea('data[Giveaway][1][description]', '', ['id'=>'Giveaway1Description', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"textarea", 'rows'=>"1", 'autocomplete'=>"off"])}}
                              </div>
                           </div>
                           <div class="mtf-buttons" style="clear:both;float:right;">
                              <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="EpisodeHasManyGiveawaysAdd">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span> add</span>
                              </button>
                           </div>
                        </div>
                        <!-- End row -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- End row -->
            <div class="row">
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                     <h4 class="header-title m-t-0">Internal details (not visible on site)</h4>
                     <div class="p-20">
                        <div class="tabs-vertical-env">
                           <ul class="nav tabs-vertical">
                              <li class="active">
                                 <a href="#internal_host_notes" data-toggle="tab" aria-expanded="false">
                                 <span class="visible-xs">
                                 <i class="fa fa-home"></i>
                                 </span>
                                 <span class="hidden-xs">Notes</span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="#events_promote" data-toggle="tab" aria-expanded="true">
                                 <span class="visible-xs">
                                 <i class="fa fa-user"></i>
                                 </span>
                                 <span class="hidden-xs">Events Promote</span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="#special_offers" data-toggle="tab" aria-expanded="false">
                                 <span class="visible-xs">
                                 <i class="fa fa-envelope-o"></i>
                                 </span>
                                 <span class="hidden-xs">Special Offers</span>
                                 </a>
                              </li>
                              <li class="">
                                 <a href="#listener_interaction" data-toggle="tab" aria-expanded="false">
                                 <span class="visible-xs">
                                 <i class="fa fa-cog"></i>
                                 </span>
                                 <span class="hidden-xs">Listener Interaction</span>
                                 </a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div class="tab-pane active" id="internal_host_notes">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][internal_host_notes]',$value=null, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeInternalHostNotes', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                              <div class="tab-pane" id="events_promote">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][events_promote]',$value=null, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeEventsPromote', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                              <div class="tab-pane" id="special_offers">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][special_offers]',$value=null, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeSpecialOffers', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                              <div class="tab-pane" id="listener_interaction">
                                 <div class="form-group">
                                    {{Form::textarea('data[Episode][listener_interaction]',$value=null, ['rows' => 8, 'cols' => 100, 'id'=>'EpisodeListenerInteraction', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- end col -->
                     </div>
                     <!-- end p-20 -->
                  </div>
                  <!-- card box -->
               </div>
               <div class="col-sm-12 col-xs-12 col-md-12">
                  <div class="card-box">
                     <h4 class="header-title m-t-0">Social Sharing Feature</h4>
                     <div class="p-20">
                        <div class="form-group row">
                           <div class="tabs-vertical-env">
                              <ul class="nav tabs-vertical">
                                 <li class="active">
                                    <a href="#segment1" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs">
                                    <i class="fa fa-home"></i>
                                    </span>
                                    <span class="hidden-xs">post 1</span>
                                    </a>
                                 </li>
                                 <li class="">
                                    <a href="#segment2" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs">
                                    <i class="fa fa-user"></i>
                                    </span>
                                    <span class="hidden-xs">post 2</span>
                                    </a>
                                 </li>
                                 <li class="">
                                    <a href="#segment3" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs">
                                    <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <span class="hidden-xs">post 3</span>
                                    </a>
                                 </li>
                                 <li class="">
                                    <a href="#segment4" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs">
                                    <i class="fa fa-cog"></i>
                                    </span>
                                    <span class="hidden-xs">post 4</span>
                                    </a>
                                 </li>
                              </ul>
                              <div class="tab-content">
                                 <!-- <div class="card-box"> -->
                                 <div class="tab-pane active" id="segment1">
                                    <div class="card-box">
                                       <div class="form-group col-sm-12">
                                          {{Form::label('EpisodeSegment1Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                          <div class="col-sm-9">
                                             {{Form::text('data[Episode][segment1_title]', $value=null, ['id'=>'EpisodeSegment1Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                          </div>
                                       </div>
                                       <div class="form-group col-sm-12">
                                          {{Form::label('EpisodeSegment1', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                          <div class="col-sm-9">
                                             {{Form::textarea('data[Episode][segment1]', $value=null, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment1', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                          </div>
                                       </div>
                                       <div class="form-group col-sm-12">
                                          {{Form::label('EpisodeSegment1VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                          <div class="col-sm-9">
                                             <div class="form-group">
                                                {{Form::url('data[Episode][segment1_video_url]',$value=null, ['id'=>'EpisodeSegment1VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-12 col-xs-12 col-md-12">
                                             <div class="p-20">
                                                <div class="form-group row">
                                                   {{Form::label('EpisodeCover0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                   <div class="col-sm-8">
                                                      <div class="row">
                                                         @if($media->count() > 0) @php ($url_count = 1) @foreach($media as $med) @if($med->sub_module == 'segment1')
                                                         <div class="col-sm-4 adding-medias media_Episode_segment1" data-off="{{$url_count}}">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                               <li class="jFiler-item" data-jfiler-index="1">
                                                                  <div class="jFiler-item-container">
                                                                     <div class="jFiler-item-inner">
                                                                        <div class="jFiler-item-thumb">
                                                                           <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment1', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                              <div class="jFiler-item-thumb-image">
                                                                                 <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <div class="jFiler-item-assets jFiler-row">
                                                                           <ul class="list-inline pull-right">
                                                                              <li>
                                                                                 <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment1" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment1"></a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      @php ($url_count++) @endif @endforeach @endif
                                                      <div class="media_Episode_segment1_outer hidden" data-off="0" style="display: none;"></div>
                                                   </div>
                                                   <span class="help-block">
                                                   <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                   Click on image to edit.
                                                   </span>
                                                </div>
                                                <div class="col-sm-2">
                                                   <div class="mtf-buttons" style="clear:both;float:right;">
                                                      <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment1" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane" id="segment2">
                                 <div class="card-box">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment2Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment2_title]', $value=null, ['id'=>'EpisodeSegment2Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment2', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment2]', $value=null, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment2', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment2VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::url('data[Episode][segment2_video_url]', $value=null, ['id'=>'EpisodeSegment2VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 col-md-12">
                                          <div class="p-20">
                                             <div class="form-group row">
                                                {{Form::label('EpisodeCover0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                <div class="col-sm-8">
                                                   <div class="row">
                                                      @if($media->count() > 0) @php ($url_count = 1) @foreach($media as $med) @if($med->sub_module == 'segment2')
                                                      <div class="col-sm-4 adding-medias media_Episode_segment2" data-off="{{$url_count}}">
                                                         <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                               <li class="jFiler-item" data-jfiler-index="1">
                                                                  <div class="jFiler-item-container">
                                                                     <div class="jFiler-item-inner">
                                                                        <div class="jFiler-item-thumb">
                                                                           <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment2', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                              <div class="jFiler-item-thumb-image">
                                                                                 <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <div class="jFiler-item-assets jFiler-row">
                                                                           <ul class="list-inline pull-right">
                                                                              <li>
                                                                                 <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment1" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment2"></a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      @php ($url_count++) @endif @endforeach @endif
                                                      <div class="media_Episode_segment2_outer hidden" data-off="0" style="display: none;"></div>
                                                   </div>
                                                   <span class="help-block">
                                                   <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                   Click on image to edit.
                                                   </span>
                                                </div>
                                                <div class="col-sm-2">
                                                   <div class="mtf-buttons" style="clear:both;float:right;">
                                                      <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment2" data-limit="1" data-dimension="" data-media_type="image" data-file_crop="0">Upload files</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane" id="segment3">
                                 <div class="card-box">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment3Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment3_title]', $value=null, ['id'=>'EpisodeSegment3Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment3', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment3]', $value=null, ['rows' => 4, 'cols' => 54, 'id' => 'EpisodeSegment3', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment3VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::url('data[Episode][segment3_video_url]', $value=null, ['id'=>'EpisodeSegment3VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 col-md-12">
                                          <div class="p-20">
                                             <div class="form-group row">
                                                {{Form::label('EpisodeCover0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                <div class="col-sm-8">
                                                   <div class="row">
                                                      @if($media->count() > 0) @php ($url_count = 1) @foreach($media as $med) @if($med->sub_module == 'segment3')
                                                      <div class="col-sm-4 adding-medias media_Episode_segment3" data-off="{{$url_count}}">
                                                         <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                               <li class="jFiler-item" data-jfiler-index="1">
                                                                  <div class="jFiler-item-container">
                                                                     <div class="jFiler-item-inner">
                                                                        <div class="jFiler-item-thumb">
                                                                           <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment3', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                              <div class="jFiler-item-thumb-image">
                                                                                 <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <div class="jFiler-item-assets jFiler-row">
                                                                           <ul class="list-inline pull-right">
                                                                              <li>
                                                                                 <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment3" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment3"></a>
                                                                              </li>
                                                                           </ul>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      @php ($url_count++) @endif @endforeach @endif
                                                      <div class="media_Episode_segment3_outer hidden" data-off="0" style="display: none;"></div>
                                                   </div>
                                                   <span class="help-block">
                                                   <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                   Click on image to edit.
                                                   </span>
                                                </div>
                                                <div class="col-sm-2">
                                                   <div class="mtf-buttons" style="clear:both;float:right;">
                                                      <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment3" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane" id="segment4">
                                 <div class="card-box">
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment4Title', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::text('data[Episode][segment4_title]', $value=null, ['id'=>'EpisodeSegment4Title', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment4', 'Description', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          {{Form::textarea('data[Episode][segment4]', $value=null, ['rows' => 4, 'cols' => 54, 'id'=>'EpisodeSegment4', 'class'=>'form-control form_ui_textarea','data-forma'=>"1", 'data-forma-def'=>"1"])}}
                                       </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                       {{Form::label('EpisodeSegment4VideoUrl', 'Video URL', array('class' => 'col-sm-3 form-control-label'))}}
                                       <div class="col-sm-9">
                                          <div class="form-group">
                                             {{Form::url('data[Episode][segment4_video_url]', $value=null, ['id'=>'EpisodeSegment4VideoUrl', 'class'=>'form-control form_ui_input', 'data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"text",'autocomplete'=>"off",'maxlength'=>"255"])}}
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-12 col-md-12">
                                          <div class="p-20">
                                             <div class="form-group row">
                                                {{Form::label('EpisodeCover0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                                                <div class="col-sm-8">
                                                   <div class="row">
                                                      @if($media->count() > 0) @php ($url_count = 1) @foreach($media as $med) @if($med->sub_module == 'segment4')
                                                      <div class="col-sm-4 adding-medias media_Episode_segment4" data-off="{{$url_count}}">
                                                         <ul class="jFiler-items-list jFiler-items-grid">
                                                            <li class="jFiler-item" data-jfiler-index="1">
                                                               <div class="jFiler-item-container">
                                                                  <div class="jFiler-item-inner">
                                                                     <div class="jFiler-item-thumb">
                                                                        <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'segment4', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">
                                                                           <div class="jFiler-item-thumb-image">
                                                                              <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                           </div>
                                                                        </a>
                                                                     </div>
                                                                     <div class="jFiler-item-assets jFiler-row">
                                                                        <ul class="list-inline pull-right">
                                                                           <li>
                                                                              <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_segment4" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="segment4"></a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   @php ($url_count++) @endif @endforeach @endif
                                                   <div class="media_Episode_segment4_outer hidden" data-off="0" style="display: none;"></div>
                                                </div>
                                                <span class="help-block">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                Click on image to edit.
                                                </span>
                                             </div>
                                             <div class="col-sm-2">
                                                <div class="mtf-buttons" style="clear:both;float:right;">
                                                   <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="segment4" data-media_type="image" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- </div> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End row -->
      <div class="row">
         <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="card-box">
               <h4 class="header-title m-t-0">Media</h4>
               <div class="p-20">
                  <div class="form-group row">
                     {{Form::label('EpisodeMedia0', 'Episode', array('class' => 'col-sm-2 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="row">
                           @if($media->count() > 0) @php ($url_count = 1) @foreach($media as $med) @if($med->sub_module == 'media')
                           <div class="col-sm-4 adding-medias media_Episode_media" data-off="{{$url_count}}">
                              <div class="jFiler-items jFiler-row">
                                 <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="1">
                                       <div class="jFiler-item-container">
                                          <div class="jFiler-item-inner">
                                             <div class="jFiler-item-thumb">
                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'Episode', 'media', 'image', 0, '{{$med->media->filename}}',{{$med->id}})" target="_blank">
                                                   <div class="jFiler-item-thumb-image">
                                                      <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-right">
                                                   <li>
                                                      <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Episode_media" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Episode" data-submodule="media"></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           @php ($url_count++) @endif @endforeach @endif
                           <div class="media_Episode_media_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        Maximum 1 file. Maximum file size: 120MB.
                        </span>
                     </div>
                     <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                           <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Episode" data-submodule="media" data-media_type="audio" data-limit="1" data-dimension="" data-file_crop="0">Upload files</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End p-20 -->
            </div>
            <!-- End card box -->
         </div>
         <!-- End col-sm-12 -->
      </div>
      <!-- End row -->
      <div class="row">
         <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="card-box">
               <h4 class="header-title m-t-0">Settings</h4>
               <div class="p-20">
                  <div class="form-group row">
                     {{Form::label('EpisodeIsOnline', 'Status', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-9">
                        <div class="radio radio-info radio-inline">
                           {{Form::radio('data[Episode][is_online]', '1', true, array('id'=>'EpisodeIsOnline1'))}} {{Form::label('EpisodeIsOnline1', 'Online', array('class' => 'col-sm-4 form-control-label'))}}
                        </div>
                        <div class="radio radio radio-inline">
                           {{Form::radio('data[Episode][is_online]', '0', false, array('id'=>'EpisodeIsOnline0-recorded'))}} {{Form::label('EpisodeIsOnline0', 'Offline', array('class' => 'col-sm-12 form-control-label'))}}
                        </div>
                     </div>
                  </div>
                  <!-- End Row -->
                  <!-- <div class="form-group row">
                     {{Form::label('EpisodeIsCohostOverrideBanner', 'Use show banner of', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-9"><div class="radio radio-info radio-inline">
                     {{Form::radio('data[Episode][is_cohost_override_banner]', '0', true, array('id'=>'EpisodeIsCohostOverrideBanner0'))}}
                     {{Form::label('EpisodeIsCohostOverrideBanner0', 'Host', array('class' => 'col-sm-4 form-control-label'))}}
                     </div><div class="radio radio radio-inline">
                     {{Form::radio('data[Episode][is_cohost_override_banner]', '2', false, array('id'=>'EpisodeIsCohostOverrideBanner2'))}}
                     {{Form::label('EpisodeIsCohostOverrideBanner2', '2nd Host', array('class' => 'col-sm-12 form-control-label'))}}
                     </div><div class="radio radio radio-inline">
                     {{Form::radio('data[Episode][is_cohost_override_banner]', '1', false, array('id'=>'EpisodeIsCohostOverrideBanner1'))}}
                     {{Form::label('EpisodeIsCohostOverrideBanner1', '1st Co-Host', array('class' => 'col-sm-12 form-control-label'))}}
                     </div></div></div> -->
                  <!-- End Row -->
                  <!-- <div class="form-group row">
                     {{Form::label('EpisodeIsCohostOverrideBanner', 'Use show title of', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-9"><div class="radio radio-info radio-inline">
                     {{Form::radio('data[Episode][is_cohost_override_show_title]', '0', true, array('id'=>'EpisodeIsCohostOverrideShowTitle0'))}}
                     {{Form::label('EpisodeIsCohostOverrideBanner0', 'Host', array('class' => 'col-sm-4 form-control-label'))}}
                     </div><div class="radio radio radio-inline">
                     {{Form::radio('data[Episode][is_cohost_override_show_title]', '2', false, array('id'=>'EpisodeIsCohostOverrideShowTitle2'))}}
                     {{Form::label('EpisodeIsCohostOverrideShowTitle2', '2nd Host', array('class' => 'col-sm-12 form-control-label'))}}
                     </div><div class="radio radio radio-inline">
                     {{Form::radio('data[Episode][is_cohost_override_show_title]', '1', false, array('id'=>'EpisodeIsCohostOverrideShowTitle1'))}}
                     {{Form::label('EpisodeIsCohostOverrideShowTitle1', '1st Co-Host', array('class' => 'col-sm-12 form-control-label'))}}
                     </div></div></div> -->
                  <!-- End Row -->
                  <div class="form-group row">
                     {{Form::label('SponsorSponsor', 'Sponsor', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-7">
                        {{Form::select('data[Sponsor][Sponsor]', [],'', $attributes = array('class'=>'form-control SponsorSponsor select2-multiple', 'id'=> 'SponsorSponsor','list'=>'host-list', 'data-parsley-maxlength'=>'255', 'data-placeholder'=>'Choose Sponsor...'))}}
                     </div>
                     <span style="display: none;" class="loading">
                     <img src="{{ asset('public/loader.gif') }}" alt="Search" height="42" width="42">
                     </span>
                  </div>
                  <!-- End Row -->
                  <div class="form-group row">
                     {{Form::label('EpisodeHasManyCategories', 'Categories', array('class' => 'col-sm-3 form-control-label'))}}
                     <div class="col-sm-7">
                        @if(!empty($assigned_categories)) @php ($guests_cat_count = 1) @foreach($assigned_categories as $guests_cat) @if(array_key_exists($guests_cat,$all_categories))
                        <div class="outside_guests_cat" data-off="{{$guests_cat_count}}">
                           <div class="col-sm-5">
                              {{Form::hidden('data[Category][select_main_id]', $guests_cat, ['id'=>'select_main_id'])}} {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories,$guests_cat, $attributes=array('id'=>'Category'.$guests_cat_count.'Category0','class'=>'form-control guests_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                           </div>
                           <div class="col-sm-7">
                              <?php $fetch_subcategories = subCategories($guests_cat,$assigned_categories); ?>
                              {{Form::hidden('data[Category][select_sub_id]', !empty($fetch_subcategories['selected_cat']) ? json_encode($fetch_subcategories['selected_cat']) : '', ['id'=>'select_sub_id'])}} {{Form::select('data[Category][Category][]', $fetch_subcategories['subcategories'], (isset($fetch_subcategories['selected_cat'])) ? $fetch_subcategories['selected_cat'] : '', $attributes=array('id'=>'Category'.$guests_cat_count.'Category1', 'multiple'=> 'multiple', 'class'=>'form-control guest_sub_cate','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                           </div>
                           <!-- <div class="col-sm-2 {{($guests_cat_count > 1) ? 'delete_guests_cat' : 'guests_cat'}}" style="display: block;"><i class="typcn typcn-delete"></i></div> -->
                        </div>
                        @endif @php ($guests_cat_count++) @endforeach @else
                        <div class="outside_guests_cat" data-off="1">
                           <div class="col-sm-5">
                              {{Form::select('data[Category][Category][]',[' '=>'Please Select'] + $all_categories, '', $attributes=array('id'=>'Category1Category0','class'=>'form-control guests_main_cat','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                           </div>
                           <div class="col-sm-5">
                              {{Form::select('data[Category][Category][]',[''=>'none'], '', $attributes=array('id'=>'Category1Category1','class'=>'form-control guest_sub_cate', 'multiple'=> 'multiple', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                           </div>
                        </div>
                        @endif
                     </div>
                  </div>
                  <!-- End row -->
               </div>
               <!-- End p-20 -->
            </div>
            <!-- End card box -->
         </div>
         <!-- End col-sm-12 -->
      </div>
      <!-- End row -->
      <div class="form-group">
         <div class="fixed_btn">
            <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Episode][btnAdd]" id="ShowBtnAdd" data-form-id="EpisodeAdminAddAgendaForm">
            Add
            </a>
         </div>
      </div>
      <!-- End col-sm-12 -->
   </div>
   <!-- end col-->
   {{ Form::close() }}
   <!-- Form Close -->
   @else
   <h4 style="text-align:center;">There does not exist any schedule.</h4>
   @endif
</div>
<!-- end row -->
@include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "Episode"])
</div>
<!-- container -->
</div>
<!-- content -->
@endsection