@extends('backend.layouts.main')

@section('content')
<style type="text/css">
    .dataTables_filter {
        width: 340px;
        float: right;
    }
    input[type="search"]{
        margin-left: 5px;
    }
</style>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Guests</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Shows
                        </li>
                        <li class="active">
                            Guests
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->     
        <div class="row">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box"> 
                <div class="form-group row page_top_btns">
                    <div class="col-sm-10">
                        <div class="button-list guest_aplha_filter">
                            <div class="btn-switch btn-switch-custom">
                                {{Form::radio('data[Guest][f][firstname]', '', true, array('id'=>'GuestFFirsttname1'))}}
                                <label for="GuestFFirsttname1" class="btn btn-rounded btn-custom waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> All</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-primary">
                                {{Form::radio('data[Guest][f][firstname]', 'b', false, array('id'=>'GuestFFirsttname2'))}}
                                <label for="GuestFFirsttname2" class="btn btn-rounded btn-primary waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> A</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-success">
                                {{Form::radio('data[Guest][f][firstname]', 'b', false, array('id'=>'GuestFFirsttname3'))}}
                                <label for="GuestFFirsttname3"
                                       class="btn btn-rounded btn-success waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> B</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-warning">
                                {{Form::radio('data[Guest][f][firstname]', 'c', false, array('id'=>'GuestFFirsttname4'))}}
                                <label for="GuestFFirsttname4"
                                       class="btn btn-rounded btn-warning waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> C</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-info">
                                {{Form::radio('data[Guest][f][firstname]', 'd', false, array('id'=>'GuestFFirsttname5'))}}
                                <label for="GuestFFirsttname5"
                                       class="btn btn-rounded btn-info waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> D</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-pink">
                                {{Form::radio('data[Guest][f][firstname]', 'e', false, array('id'=>'GuestFFirsttname6'))}}
                                <label for="GuestFFirsttname6"
                                       class="btn btn-rounded btn-pink waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> E</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-inverse">
                                {{Form::radio('data[Guest][f][firstname]', 'f', false, array('id'=>'GuestFFirsttname7'))}}
                                <label for="GuestFFirsttname7"
                                       class="btn btn-rounded btn-inverse waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> F</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-custom">
                                {{Form::radio('data[Guest][f][firstname]', 'g', false, array('id'=>'GuestFFirsttname8'))}}
                                <label for="GuestFFirsttname8"
                                       class="btn btn-rounded btn-custom waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> G</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-brown">
                                {{Form::radio('data[Guest][f][firstname]', 'h', false, array('id'=>'GuestFFirsttname9'))}}
                                <label for="GuestFFirsttname9"
                                       class="btn btn-rounded btn-brown waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> H</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-orange">
                                {{Form::radio('data[Guest][f][firstname]', 'i', false, array('id'=>'GuestFFirsttname10'))}}
                                <label for="GuestFFirsttname10"
                                       class="btn btn-rounded btn-orange waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> I</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-purple">
                                {{Form::radio('data[Guest][f][firstname]', 'j', false, array('id'=>'GuestFFirsttname11'))}}
                                <label for="GuestFFirsttname11"
                                       class="btn btn-rounded btn-purple waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> J</strong>
                                </label>
                            </div>
                           
                            <div class="btn-switch btn-switch-teal">
                                {{Form::radio('data[Guest][f][firstname]', 'k', false, array('id'=>'GuestFFirsttname12'))}}
                                <label for="GuestFFirsttname12"
                                       class="btn btn-rounded btn-teal waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> K</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-danger">
                                {{Form::radio('data[Guest][f][firstname]', 'l', false, array('id'=>'GuestFFirsttname13'))}}
                                <label for="GuestFFirsttname13"
                                       class="btn btn-rounded btn-danger waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> L</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-inverse">
                                {{Form::radio('data[Guest][f][firstname]', 'm', false, array('id'=>'GuestFFirsttname14'))}}
                                <label for="GuestFFirsttname14"
                                       class="btn btn-rounded btn-inverse waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> M</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-pink">
                                {{Form::radio('data[Guest][f][firstname]', 'n', false, array('id'=>'GuestFFirsttname15'))}}
                                <label for="GuestFFirsttname15"
                                       class="btn btn-rounded btn-pink waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> N</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-warning">
                                {{Form::radio('data[Guest][f][firstname]', 'o', false, array('id'=>'GuestFFirsttname16'))}}
                                <label for="GuestFFirsttname16"
                                       class="btn btn-rounded btn-warning waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> O</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-danger">
                                {{Form::radio('data[Guest][f][firstname]', 'p', false, array('id'=>'GuestFFirsttname17'))}}
                                <label for="GuestFFirsttname17"
                                       class="btn btn-rounded btn-danger waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> P</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-success">
                                {{Form::radio('data[Guest][f][firstname]', 'q', false, array('id'=>'GuestFFirsttname18'))}}
                                <label for="GuestFFirsttname18"
                                       class="btn btn-rounded btn-success waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> Q</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-warning">
                                {{Form::radio('data[Guest][f][firstname]', 'r', false, array('id'=>'GuestFFirsttname19'))}}
                                <label for="GuestFFirsttname19"
                                       class="btn btn-rounded btn-warning waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> R</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-primary">
                                {{Form::radio('data[Guest][f][firstname]', 's', false, array('id'=>'GuestFFirsttname20'))}}
                                <label for="GuestFFirsttname20"
                                       class="btn btn-rounded btn-primary waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> S</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-orange">
                                {{Form::radio('data[Guest][f][firstname]', 't', false, array('id'=>'GuestFFirsttname21'))}}
                                <label for="GuestFFirsttname21"
                                       class="btn btn-rounded btn-orange waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> T</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-teal">
                                {{Form::radio('data[Guest][f][firstname]', 'u', false, array('id'=>'GuestFFirsttname22'))}}
                                <label for="GuestFFirsttname22"
                                       class="btn btn-rounded btn-teal waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> U</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-pink">
                                {{Form::radio('data[Guest][f][firstname]', 'v', false, array('id'=>'GuestFFirsttname23'))}}
                                <label for="GuestFFirsttname23"
                                       class="btn btn-rounded btn-pink waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> V</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-info">
                                {{Form::radio('data[Guest][f][firstname]', 'w', false, array('id'=>'GuestFFirsttname24'))}}
                                <label for="GuestFFirsttname24"
                                       class="btn btn-rounded btn-info waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> W</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-brown">
                                {{Form::radio('data[Guest][f][firstname]', 'x', false, array('id'=>'GuestFFirsttname25'))}}
                                <label for="GuestFFirsttname25"
                                       class="btn btn-rounded btn-brown waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> X</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-orange">
                                {{Form::radio('data[Guest][f][firstname]', 'y', false, array('id'=>'GuestFFirsttname26'))}}
                                <label for="GuestFFirsttname26"
                                       class="btn btn-rounded btn-orange waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> Y</strong>
                                </label>
                            </div>

                            <div class="btn-switch btn-switch-purple">
                                {{Form::radio('data[Guest][f][firstname]', 'z', false, array('id'=>'GuestFFirsttname27'))}}
                                <label for="GuestFFirsttname27"
                                       class="btn btn-rounded btn-purple waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> Z</strong>
                                </label>
                            </div>

                        </div>
                    </div>   
                    <div class="col-sm-2"> 
                        <div style="float:right;">
                            @if(Config::get('constants.HOST') == getCurrentUserType())
                                <a href="{{url('/host/show/guests/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Guest</a>
                            @endif
                            @if(Config::get('constants.ADMIN') == getCurrentUserType())
                                <a href="{{url('/admin/show/guests/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Guest</a>
                            @endif                            
                            <a href="javascript:void(0);" id="guests_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Export</a>
                           <!--  <a href="javascript:void(0)" onclick="deleteGuest(5499, this)"><i class="glyphicon glyphicon-trash"></i></a> -->
                        </div>
                    </div>
                </div>
             </div>
        </div> 
        <!-- end row -->    
         <!--  add hosts export form by parmod -->
        <div class="row" style="display: none;" id="guests_export_option">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="p-20">
                        {{ Form::open(array('url' => 'admin/show/guests/export_guests','method' => 'get', 'id'=>'HostsExportForm')) }}
                            <div class="form-group row"> 
                            {{Form::label('Banner', 'Name', array('class' => 'form-control-label col-sm-1'))}}             
                               <div class="col-sm-10">
                               
                                {{Form::select('data[Guest][export_filter]', array('All'=>'All','Online'=>'Online','Offline'=>'Offline','Featured'=>'Featured','Channels'=>$all_export_channels),'All', $attributes=array('id'=>'GuestExportFilter', 'class'=>'selectpicker m-b-0', 'data-forma'=>'1', 'data-forma-def'=>'1', 'data-type'=>'select'))}} 
                                <input type="hidden" name="lebel" value="undefined" id="guest_eport_lebel"> 
                                <!-- <input type="hidden" name="selectopn" value="" id="eport_selected_option"> -->                         
                               </div>
                            </div>
                            
                            <div class="form-group row"> 
                                 <div class="col-sm-7 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light submit_form" value="Add" id="GuestsExportSubmit">Export</button>
                                 </div>                           
                            </div>
            
                         {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- form end  -->             
        <!-- end row -->       
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">                    
                    <table id="guests_datatable" class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div> <!-- container -->
</div> <!-- content -->

@endsection
