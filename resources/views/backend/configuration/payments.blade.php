@extends('backend.layouts.main')

@section('content')
<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Payments Configuration</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Configuration
                        </li>
                        <li class="active">
                           Payments
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div id="yes_crop"></div>
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/cms/configurations/payments', 'id' => 'ConfigurationAdminPaymentsForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">PayPal</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('ConfigurationDefaultCurrency', 'Default currency', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::select('data[Configuration][default_currency]',$data['currency'],$currency->value, $attributes=array('id'=>'ConfigurationDefaultCurrency', 'class'=>'selectpicker', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                    </div>
                                </div><!-- end row -->
								<div class="form-group row">
									{{Form::label('ConfigurationPaymentsPaypalEnabled', 'Paypal payments enabled', array('class' => 'col-sm-4 form-control-label'))}}
									<div class="col-sm-7">
										<div class="form-group">
										{{ Form::checkbox('data[Configuration][payments_paypal_enabled]','1',$payments_paypal_enabled->value == '1' ? 'checked' : '', ['id' => 'ConfigurationPaymentsPaypalEnabled', 'data-switch' => 'bool']) }}
											<label for="ConfigurationPaymentsPaypalEnabled" data-on-label="On" data-off-label="Off"  data-size="large"></label class="checkbox_button">
										</div>
									</div>
								</div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('ConfigurationPaymentsPaypalEmail', 'Paypal Seller email', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7 tag-input-type">                                     
                                        <div class="form-group">
                                            {{Form::text('data[Configuration][payments_paypal_email]',$payments_paypal_email->value, ['id'=>'ConfigurationPaymentsPaypalEmail', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','type'=>'text'])}}
											<span class="help-block">
												<i class="fa fa-info-circle" aria-hidden="true"></i>
												Your PayPal seller email used to receive payments
											</span>
                                        </div>                                       
                                    </div>
                                </div> <!-- End row --> 
								<div class="form-group row ">
									{{Form::label('ConfigurationPaymentsMode', 'Payments mode', array('class' => 'col-sm-3 form-control-label'))}}
									<div class="col-sm-7"> 
										<div class="radio radio-info radio-inline">
											{{Form::radio('data[Configuration][payments_mode]',$data['payments_mode']['test'],$payments_mode->value == 'test' ? 'checked' : '', array('id'=>'ConfigurationPaymentsModeTest'))}}
											{{Form::label('ConfigurationPaymentsModeTest', 'test', array('class' => 'col-sm-4 form-control-label'))}}
										</div>
										<div class="radio radio-inline">
											{{Form::radio('data[Configuration][payments_mode]',$data['payments_mode']['sandbox'],$payments_mode->value == 'sandbox' ? 'checked' : '', array('id'=>'ConfigurationPaymentsModeSandbox'))}}
											{{Form::label('ConfigurationPaymentsModeSandbox', 'sandbox', array('class' => 'col-sm-4 form-control-label'))}}
										</div>
										<div class="radio radio-inline">
											{{Form::radio('data[Configuration][payments_mode]',$data['payments_mode']['prod'],$payments_mode->value == 'production' ? 'checked' : '', array('id'=>'ConfigurationPaymentsModeProd'))}}
											{{Form::label('ConfigurationPaymentsModeProd', 'production', array('class' => 'col-sm-4 form-control-label'))}}
										</div>
									</div>
									<span class="help-block">
										<i class="fa fa-info-circle" aria-hidden="true"></i>
										Use this setting to switch between test, sandbox and production modes
									</span>
                                </div><!-- end row -->								
                            </div>
                        </div>
                    </div>
                </div>               
                <div class="form-group">
					{{Form::hidden('data[Configuration][id]','',['id'=>'ConfigurationId','data-forma'=>'1'])}}
					{{Form::hidden('data[Configuration][submitted_by]','')}}
					<div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Configuration][btnSave]" id="ConfigurationBtnSave" data-form-id="ConfigurationAdminPaymentsForm" value="edit">
                            Save
                        </a>
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
