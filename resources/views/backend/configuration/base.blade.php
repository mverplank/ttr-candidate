@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Configuration
                        </li>
                        <li class="active">
                           Base
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">

            <div class="col-xs-12">
                {{ Form::open(array('url' => 'admin/cms/configurations/base', 'id'=>'ConfigurationAdminBaseForm')) }}
                <div class="card-box">
                    <!-- Form Starts-->
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Base</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationEmail', 'Base email', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::email('data[Configuration][email]',$data['base_url'], $attributes = array('class'=>'form-control required','data-type'=>'emailNamed', 'id'=>'ConfigurationEmail','data-forma'=>'1', 'data-forma-def'=>'1','data-required'=>'1'))}}
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>Default application email address.<br>It will be used as email address "from" for all automated emails sent by application.<br>Format: "some@hostname.com" OR "Some Name &lt;some@hostname.com&gt;"</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationDefaultDomain', 'Defaul domain', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][default_domain]', $data['default_domain'], $attributes = array('class'=>'form-control','data-forma'=>'45','data-forma-def' =>'1', 'id'=>'ConfigurationDefaultDomain'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Default domain used (only domain name, no http://www. before or / at the end; ex. 'transformationradio.fm')</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        {{Form::label('ConfigurationMetaTitle', 'Title', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][meta_title]',$data['meta_title'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationMetaTitle'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Title shown on browser</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationMetaDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::textarea('data[Configuration][meta_description]',$data['meta_description'], $attributes = array('class'=>'form-control', 'id'=>'ConfigurationMetaDescription','rows'=>"2", 'cols'=>"30" ))}}
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        {{Form::label('ConfigurationTimeFormat', 'Default time format', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::select('data[Configuration][time_format]', ['12'=>'12h','24'=>'24h'], $data['time_format'], $attributes=array('id'=>'ConfigurationTimeFormat', 'class'=>'form-control'))}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationDateFormat', 'Default date format', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::select('data[Configuration][date_format]', ['DMY'=>'DMY','MDY'=>'MDY','YMD'=>'YMD'],$data['date_format'], $attributes=array('id'=>'ConfigurationDateFormat', 'class'=>'form-control'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>D - day, M - month, Y - year</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationDateDelimiter', 'Date delimiter', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][date_delimiter]', $data['date_delimiter'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationDateDelimiter'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Delimiter between year, month and day values (ex. / or -)</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationRecaptchaSitekey', 'reCaptcha Site Key', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][recaptcha_sitekey]',$data['recaptcha_sitekey'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationRecaptchaSitekey'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Get at: <a href="https://www.google.com/recaptcha/admin#list" target="_blank">reCaptcha</a></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationRecaptchaSecretkey', 'reCaptcha Site Key', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][recaptcha_secretkey]',$data['recaptcha_secretkey'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationRecaptchaSecretkey'))}}
                                        </div>
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">RSS</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationRssTtl', 'RSS cache timeout minutes', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::number('data[Configuration][rss_ttl]',$data['rss_ttl'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'number','id'=>'ConfigurationRssTtl','data-type'=>'int', 'autocomplete'=>'off','min'=>'0'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>RSS feed cache life time in minutes; If cached content older than defiend value - new refreshed file will be created.</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationRssItemDescLimit', 'RSS description characters limit', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::number('data[Configuration][rss_item_desc_limit]',$data['rss_item_desc_limit'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'number','id'=>'ConfigurationRssItemDescLimit','data-type'=>'int', 'autocomplete'=>'off','min'=>'0'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>RSS feed each item description will be limited to above number of character</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationRssItemsLimit', 'RSS items count limit', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::number('data[Configuration][rss_items_limit]',$data['rss_items_limit'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'number','id'=>'ConfigurationRssItemsLimit','data-type'=>'int', 'autocomplete'=>'off','min'=>'0'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> RSS feed maximum number of items to be read by aggregator. Set zero for unlimited (all)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Facebook App</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationFacebookAppId', 'Facebook App ID', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][facebook_app_id]',$data['facebook_app_id'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationFacebookAppId'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>App ID; to obtain ID and Secret go to: <a href="https://developers.facebook.com/apps/" target="_blank">https://developers.facebook.com/apps/</a></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationFacebookAppSecret', 'Facebook App Secret', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][facebook_app_secret]',$data['facebook_app_secret'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationFacebookAppSecret'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>App Secret</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationFacebookAuthAppId', 'Facebook Login App ID', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][facebook_auth_app_id]',$data['facebook_auth_app_id'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationFacebookAuthAppId'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>App ID; to obtain ID and Secret go to: <a href="https://developers.facebook.com/apps/" target="_blank">https://developers.facebook.com/apps/</a></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationFacebookAuthAppSecret', 'Facebook Login App Secret', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][facebook_auth_app_secret]',$data['facebook_auth_app_secret'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationFacebookAuthAppSecret'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>App Secret</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Twitter App</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationTwitterAppKey', 'Facebook App ID', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][twitter_app_key]',$data['twitter_app_key'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationTwitterAppKey','data-type'=>'text'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Consumer Key  (API Key); to obtain key and secret visit: <a href="https://apps.twitter.com/app/new" target="_blank">https://apps.twitter.com/app/new</a> and create new application with callback URL: https://transformationtalkradio.com/social_media/twitter/callback , Permissions &gt; Access set to "Read and Write"<br>To view list of exisitng applications visit: <a href="https://apps.twitter.com/">https://apps.twitter.com/</a></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationTwitterAppSecret', 'Twitter App Secret', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][twitter_app_secret]',$data['twitter_app_secret'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','id'=>'ConfigurationTwitterAppSecret'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> Consumer Secret (API Secret)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">Other</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationCreateEpisodesForEmptyShows', 'Auto create Episodes', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            <div class="radio radio-inline">
                                                {{ Form::radio('data[Configuration][create_episodes_for_empty_shows]', '0',$data['create_episodes_for_empty_shows'] == 0 ? 'checked' : '', array('id'=>'ConfigurationCreateEpisodesForEmptyShows0'))}}
                                                {{Form::label('ConfigurationCreateEpisodesForEmptyShows0', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                {{ Form::radio('data[Configuration][create_episodes_for_empty_shows]', '1',$data['create_episodes_for_empty_shows'] == 1 ? 'checked' : '', array('id'=>'ConfigurationCreateEpisodesForEmptyShows1'))}}
                                                {{Form::label('ConfigurationCreateEpisodesForEmptyShows1', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                        </div>
                                        <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                         If enabled episodes are automatically created if has been not added manualy (before each day)</span>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationMailingHostNoEpisodesDetails', 'Missing episodes reminder', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            <div class="radio radio-inline">
                                                {{ Form::radio('data[Configuration][mailing__host_no_episodes_details]', '0',$data['mailing__host_no_episodes_details'] == 0 ? 'checked' : '', array('id'=>'ConfigurationMailingHostNoEpisodesDetails0'))}}
                                                {{Form::label('ConfigurationMailingHostNoEpisodesDetails0', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                {{ Form::radio('data[Configuration][mailing__host_no_episodes_details]', '1',$data['mailing__host_no_episodes_details'] == 1 ? 'checked' : '', array('id'=>'ConfigurationMailingHostNoEpisodesDetails1'))}}
                                                {{Form::label('ConfigurationMailingHostNoEpisodesDetails1', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                            </div>
                                        </div>
                                        <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                         If enabled each Sunday evenig send email remander to all hosts which has not entered episodes for current week
                                        </span>
                                    </div><!-- end row -->
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationCustomPodcastsChannelId', 'Custom podcasts channel ID', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::number('data[Configuration][custom_podcasts_channel_id]',$data['custom_podcasts_channel_id'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','id'=>'ConfigurationCustomPodcastsChannelId','data-type'=>'int', 'autocomplete'=>'off','min'=>'0'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Identifier of channel where uploaded by hosts/podcast-hosts custom podcasts should be presented</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h4 class="header-title m-t-0">StudioApp</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationStudioappClientPath', 'Path to studioApp client', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][studioapp_client_path]',$data['studioapp_client_path'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','id'=>'ConfigurationStudioappClientPath'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Full path to studioApp client application (with slash at the end)</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationStudioappServerPath', 'Path to studioApp server', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][studioapp_server_path]',$data['studioapp_server_path'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','id'=>'ConfigurationStudioappServerPath'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i>Full path to studioApp server applications (with slash at the end)</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationStudioappUrl', 'Url to studioApp', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][studioapp_url]',$data['studioapp_url'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','id'=>'ConfigurationStudioappUrl'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> Full URL to studioApp (with https:// at the begin)</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{Form::label('ConfigurationStudioappServerUrl', 'Url to studioAppServer', array('class' => 'col-sm-4 form-control-label'))}}
                                        <div class="col-sm-7">
                                            {{Form::text('data[Configuration][studioapp_server_url]',$data['studioapp_server_url'], $attributes = array('class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','id'=>'ConfigurationStudioappServerUrl'))}}
                                            <span class="help-block"><i class="fa fa-info-circle" aria-hidden="true"></i> Full URL to studioApp Server (with https:// at the begin)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="fixed_btn">
                                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Configuration][btnSave]" id="ConfigurationBtnSave" data-form-id="ConfigurationAdminBaseForm" >
                                    Update
                                </button>
                               <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                             
                            </div>
                        </div>
                    <!-- end row -->
                </div> <!-- end card-box -->
                {{ Form::close() }}
            </div><!-- end col-->
        </div>
        <!-- end row -->



    </div> <!-- container -->
</div> <!-- content -->

@endsection
