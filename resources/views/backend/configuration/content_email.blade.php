@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Host</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/user/hosts')}}">All Hosts</a>
                        </li>
                        <li class="active">
                            Add Host
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div id="yes_crop"></div>
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/cms/contents/emails', 'id' => 'ContentAdminEmailsForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Personal Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('HostDefaultChannelId', 'Template', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::select('data[Content][email_template]',$file_arr, '', $attributes=array('id'=>'ContentEmailTemplate', 'class'=>'selectpicker', 'data-selected-text-format'=>'count', 'data-style'=>'btn-default'))}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('ContentEmailSubject', 'Subject', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7 tag-input-type">                                     
                                        <div class="form-group">
                                            {{Form::text('data[Content][email_subject]','', ['id'=>'ContentEmailSubject', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text', 'disabled', '_disabled'=>'1', 'type'=>'text'])}}
											{{Form::hidden('data[Content][action]','',['id'=>'action_type'])}}
                                        </div>                                       
                                    </div>
                                </div> <!-- End row -->
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    {{Form::label('ContentEmailBody', 'HTML / text', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="card-box">
                                            {{ Form::textarea('data[Content][email_body]', '', ['id' => 'ContentEmailBody', 'class'=>'']) }}
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div>               
                <div class="form-group row">
					<div class="fixed_btn">
						<a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Content][btnPreview]" id="ContentBtnPreview" data-form-id="ContentAdminEmailsForm" value="preview">
							Preview
						</a>
						<a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Content][btnSend]" id="ContentBtnSend" data-form-id="ContentAdminEmailsForm" value="send">
							Send
						</a>
						<a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Content][btnSave]" id="ContentBtnSave" data-form-id="ContentAdminEmailsForm" value="edit">
							Save
						</a>
					</div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
