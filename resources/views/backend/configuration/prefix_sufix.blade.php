@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Content Configuration</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Configuration
                        </li>                      
                        <li class="active">
                            Content Configuration
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->        
        <div class="row">
            <div id="yes_crop"></div>
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/cms/configurations/content', 'id' => 'ConfigurationAdminContentForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('ConfigurationUserNamePrefixes', 'User name prefixes', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7 tag-input-type">                 
                                        <div class="form-group">
                                            {{Form::text('data[Configuration][user_name_prefixes]', $prefix, ['id'=>'ConfigurationUserNamePrefixes', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                                        </div>                                       
                                    </div>
                                </div> <!-- End row -->
                                <div class="form-group row">
                                    {{Form::label('ConfigurationUserNameSufixes', 'User name sufixes', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7 tag-input-type">                 
                                        <div class="form-group">
                                            {{Form::text('data[Configuration][user_name_sufixes]', $suffix, ['id'=>'ConfigurationUserNameSufixes', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                            <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                                        </div>                                       
                                    </div>
                                </div> <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row -->
                <div class="form-group">
                    <div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Configuration][btnSave]" id="ConfigurationBtnSave" data-form-id="ConfigurationAdminContentForm">
                            Save
                        </a>
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
