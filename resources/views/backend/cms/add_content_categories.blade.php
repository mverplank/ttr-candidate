@extends('backend.layouts.main')
@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Content Type</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="">
                            Content
                        </li>
                        <li class="">
                            <a href="{{route('content_data_index')}}">Manage Entries</a>
                        </li>
                        <li class="active">
                             Add
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/content_categories/add', 'id'=>'ContentCategoryAddForm')) }}
        <div class="row">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Base</h4>
                <div class="form-group row">
                    {{Form::label('ContentCategoryParentId', 'Parent Category', array('class' => 'col-sm-2 form-control-label'))}}  
                    <div class="col-md-5">
                        {!! $content_cate !!} 
                    </div>
                </div>
                <div class="form-group row">
                    {{Form::label('ContentCategoryName', 'Name', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10 form-group">
                        {{Form::text('data[ContentCategory][name]', $value = null, $attributes =array('id'=>'ContentCategoryName', 'class'=>'form-control required form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-required' => '1'))}}
                        <span class="error_text" style="display:none;"></span>
                    </div>
                </div><!-- End row -->
                <div class="form-group row">
                    {{Form::label('ContentCategoryDescription', 'Description', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10">
                        {{ Form::textarea('data[ContentCategory][description]', null, ['id' => 'ContentCategoryDescription', 'class'=>'form-control','rows'=>3, 'cols'=>50]) }}
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                            HTML HEAD meta description for search engines.
                            Recommended length: aprox. 160 characters.
                        </span>
                    </div>
                </div><!-- End row -->
                 <div class="form-group row">
                    {{Form::label('ContentCategoryKeywords_addTag', 'Keywords', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10 form-group">
                        {{Form::text('data[ContentCategory][Keywords]', $value = null, $attributes =array('id'=>'ContentCategoryKeywords_tag', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-role'=>'tagsinput'))}}
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                Enter comma separated values
                        </span>
                    </div>
                </div><!-- End row -->
                <div class="form-group row">
                     {{Form::label('ContentCategoryD', 'Order by field / direction', array('class' => 'col-sm-2 form-control-label'))}}  
                    <div class="col-md-5">
                       {{Form::select('data[ContentCategory][order_field]',(!empty($drderbyfield) ? $drderbyfield : []), '', $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryOrderField','data-style'=>'btn-purple'))}} 
                    </div>
                    <div class="col-md-5">
                       {{Form::select('data[ContentCategory][order_direction]',(!empty($drderby) ? $drderby : []), '', $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryOrderDirection','data-style'=>'btn-purple'))}} 
                    </div>
                    <span class="help-block col-sm-10 col-sm-offset-2"> 
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                           his cateogry items will be ordered by above field and in selected direction.
                            (Custom - allows to manually set order).
                    </span>
                </div>
            </div>
        </div>   
        <div class="row">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Back-end Layout</h4>
                <div class="form-group row">
                    {{Form::label('ContentCategoryLabelTitle', 'Title / Name', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10 form-group">
                        {{Form::text('data[ContentCategory][label_title]', $value = null, $attributes =array('id'=>'ContentCategoryLabelTitle', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text'))}}
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                              Content entry main title or name (max. length 255 characters)
                        </span>
                    </div>
                </div><!-- End row -->
            </div>
        </div>  
        <div class="row">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Front-end Layout</h4>
                <div class="form-group row">
                    {{Form::label('ContentCategoryIsFrontendGenerator', 'Generate index/view pages', array('class' => 'col-sm-3 form-control-label'))}}
                    <div class="col-sm-6">
                        <div class="form-group">
                        {{ Form::checkbox('data[ContentCategory][is_frontend_generator]', '1', true, ['id' => 'switch33', 'data-switch' => 'bool']) }}
                            <label for="switch33" data-on-label="Online"data-off-label="Offline"  data-size="large"></label class="checkbox_button">
                        </div>
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                             When On this Content Category index page and all entries view pages will be accessible via right URLs.When Off all Content Category related pages will not be accessibe using URLs (Content Category data can be used only as data source for widgets).
                        </span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <!-- hidden field -->
                        {{Form::hidden('data[ContentCategory][is_custom_index]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_index_params_json]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_custom_index_row]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_index_row_params_json]', '', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_custom_view]', '1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_view_params_json]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_custom_box]', '1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_box_params_json]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_title]','1', ['id'=>''])}}

                        {{Form::hidden('data[ContentCategory][is_title]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_subtitle]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_content]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_short_description]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_url]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_external_url]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_image]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_date]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_time]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_publish]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_sortable]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_featured]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_tags]','1', ['id'=>''])}}

                        
                        {{Form::hidden('data[ContentCategory][label_subtitle]','Sub-title', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_content]','Content', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_short_description]','Short description', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_url]','url', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_external_url]','External Url', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_images]','Images', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_date]','Date', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_time]','Time', ['id'=>''])}}
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="fixed_btn">
                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[ContentCategory][btnAdd]" id="ContentCategoryBtnAdd" data-form-id="ContentCategoryAddForm">
                    Add
                </button>          
            </div>
        </div>
     {{ Form::close() }}       
    </div>
</div>
@endsection     