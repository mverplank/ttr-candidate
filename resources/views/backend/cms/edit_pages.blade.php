@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add Pages</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="">
                     Content
                  </li>
                  <li class="">
                     <a href="{{route('get_cms_pages')}}">Pages</a>
                  </li>
                  <li class="active">
                     Add
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/pages/edit/'.$page->id, 'id'=>'CmsPageAdminEditForm')) }}
            <div class="form_start">
                <div class="row">
                    <div class="card-box">
                        <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Edit Page: {{$page->title}}</h4>
                        {{Form::hidden('data[Page][id]',$page->id, ['id'=>''])}}
                        <div class="form-group row">
                           {{Form::label('PageTitle', 'Ttitle', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-10">
                              {{Form::text('data[Page][title]',$page->title, $attributes = array('class'=>'form-control required','id'=>'PageTitle','required'=>true))}}
                           </div>
                        </div> 
                        <div class="form-group row">
                           {{Form::label('PageName', 'Name', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              {{Form::text('data[Page][name]', $page->name, $attributes = array('class'=>'form-control','id'=>'PageName'))}}
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                Expression to create URL (ex. "about us"). Leave empty if equal to title. It will be not possible to change it later.
                              </span>
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('PageName', 'URL', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              <input type="text" class="form-control form_ui_input" name="full_url" readonly="true" value="{{$page->full_url}}">
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                  Full of page
                              </span>
                           </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('PageDescription','Description', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{ Form::textarea('data[Page][description]',$page->description, ['id' => 'PageDescription', 'class'=>'form-control','rows'=>4, 'cols'=>50]) }}
                                <span class="help-block"> 
                                  <i class="fa fa-info-circle" aria-hidden="true"></i>
                                   HTML HEAD meta description for search engines. Recommended length: aprox. 160 characters.
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('PageKeywords','Keywords', array('class' => 'col-sm-2 form-control-label'))}}  
                            <div class="col-md-10">
                                {{Form::text('data[Page][keywords]',$page->keywords, $attributes =array('id'=>'PageKeywords', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-role'=>'tagsinput'))}}
                                <span class="help-block"> 
                                  <i class="fa fa-info-circle" aria-hidden="true"></i>
                                   Enter comma separated values
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Page][btnUpdate]" id="CmspageBtnAdd" data-form-id="CmsPageAdminEditForm">
                            Edit
                        </button>          
                    </div>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection