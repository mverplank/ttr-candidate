@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add Pages</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="">
                     Content
                  </li>
                  <li class="">
                     <a href="{{route('get_cms_pages')}}">Pages</a>
                  </li>
                  <li class="active">
                     Add
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/pages/add', 'id'=>'CmsPageAdminAddForm')) }}
            <div class="form_start">
                <div class="row">
                    <div class="card-box">
                        <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Create new page</h4>
                        <div class="form-group row">
                           {{Form::label('PagePagesId', 'Clone page', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              {{Form::select('data[Page][pages_id]',[' '=>'None'] + $pages, '', $attributes=array('id'=>'PagePagesId','class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                           </div>
                        </div>
                        <div class="form-group row">
                           {{Form::label('PageTitle', 'Ttitle', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-10">
                              {{Form::text('data[Page][title]', $value = null, $attributes = array('class'=>'form-control required','id'=>'PageTitle','required'=>true))}}
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                HTML TITLE meta value
                              </span>
                           </div>
                        </div> 
                        <div class="form-group row">
                           {{Form::label('PageName', 'Name', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              {{Form::text('data[Page][name]', $value = null, $attributes = array('class'=>'form-control','id'=>'PageName'))}}
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                Expression to create Name (ex. "about us"). Leave empty if equal to title. It will be not possible to change it later.
                              </span>
                           </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('PageDescription','Description', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{ Form::textarea('data[Page][description]', null, ['id' => 'PageDescription', 'class'=>'form-control','rows'=>4, 'cols'=>50,'maxlength'=>155]) }}
                                <span class="help-block"> 
                                  <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    HTML HEAD meta description for search engines. Recommended length: aprox. 155 characters.
                              </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('PageKeywords','Keywords', array('class' => 'col-sm-2 form-control-label'))}}  
                            <div class="col-md-10">
                                {{Form::text('data[Page][keywords]', $value = null, $attributes =array('id'=>'PageKeywords', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-role'=>'tagsinput'))}}
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                  Enter comma separated values
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Page][btnAdd]" id="CmspageBtnAdd" data-form-id="CmsPageAdminAddForm">
                            Add
                        </button>          
                    </div>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection