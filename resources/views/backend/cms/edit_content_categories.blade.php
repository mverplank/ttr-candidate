@extends('backend.layouts.main')
@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Content Type</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="">
                            Content
                        </li>
                        <li class="">
                            <a href="{{route('content_data_index')}}">Manage Entries</a>
                        </li>
                        <li class="active">
                            Edit
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/content_categories/edit/'.$thiscate->id, 'id'=>'ContentCategoryEditForm')) }}
        <div class="row">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Base</h4>
                <div class="form-group row">
                     {{Form::label('ContentCategoryParentId', 'Parent Category', array('class' => 'col-sm-2 form-control-label'))}}  
                    <div class="col-md-5">
                        {!! $content_cate !!} 
                    </div>
                </div>
                <div class="form-group row">
                    {{Form::label('ContentCategoryName', 'Name', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10 form-group">
                        {{Form::text('data[ContentCategory][name]',$thiscate->name, $attributes =array('id'=>'ContentCategoryName', 'class'=>'form-control required form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-required' => '1'))}}
                    </div>
                </div><!-- End row -->
                <div class="form-group row">
                    {{Form::label('ContentCategoryDescription', 'Description', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10">
                        {{ Form::textarea('data[ContentCategory][description]',$thiscate->description, ['id' => 'ContentCategoryDescription', 'class'=>'form-control','rows'=>3, 'cols'=>50]) }}
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                            HTML HEAD meta description for search engines.
                            Recommended length: aprox. 160 characters.
                        </span>
                    </div>
                </div><!-- End row -->
                 <div class="form-group row">
                    {{Form::label('ContentCategoryKeywords_addTag', 'Keywords', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10 form-group">
                        {{Form::text('data[ContentCategory][keywords]',$thiscate->keywords, $attributes =array('id'=>'ContentCategoryKeywords_tag', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-role'=>'tagsinput'))}}
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                                Enter comma separated values
                        </span>
                    </div>
                </div><!-- End row -->
                <div class="form-group row">
                     {{Form::label('ContentCategoryD', 'Order by field / direction', array('class' => 'col-sm-2 form-control-label'))}}  
                    <div class="col-md-5">
                       {{Form::select('data[ContentCategory][order_field]',(!empty($drderbyfield) ? $drderbyfield : []),$thiscate->order_field, $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryOrderField','data-style'=>'btn-purple'))}} 
                    </div>
                    <div class="col-md-5">
                       {{Form::select('data[ContentCategory][order_direction]',(!empty($drderby) ? $drderby : []),$thiscate->order_direction, $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryOrderDirection','data-style'=>'btn-purple'))}} 
                    </div>
                    <div class="row">
                        <span class="help-block col-sm-10 col-sm-offset-2"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                               his cateogry items will be ordered by above field and in selected direction.
                                (Custom - allows to manually set order).
                        </span>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Back-end Layout</h4>
                <div class="form-group row">
                    {{Form::label('ContentCategoryLabelTitle', 'Title / Name', array('class' => 'col-sm-2 form-control-label'))}}
                    <div class="col-sm-10 form-group">
                        {{Form::text('data[ContentCategory][label_title]',$thiscate->label_title, $attributes =array('id'=>'ContentCategoryLabelTitle', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text'))}}
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                              Content entry main title or name (max. length 255 characters)
                        </span>
                    </div>
                </div><!-- End row -->
            </div>
        </div>  
        <div class="row">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Front-end Layout</h4>
                <div class="form-group row">
                    {{Form::label('ContentCategoryIsFrontendGenerator', 'Generate index/view pages', array('class' => 'col-sm-3 form-control-label'))}}
                    <div class="col-sm-6">
                        <div class="form-group">
                        {{ Form::checkbox('data[ContentCategory][is_frontend_generator]', '1', true, ['id' => 'switch33', 'data-switch' => 'bool']) }}
                            <label for="switch33" data-on-label="Online"data-off-label="Offline"  data-size="large"></label class="checkbox_button">
                        </div>
                        <span class="help-block"> 
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                             When On this Content Category index page and all entries view pages will be accessible via right URLs.When Off all Content Category related pages will not be accessibe using URLs (Content Category data can be used only as data source for widgets).
                        </span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <!-- hidden field -->
                       <!--  {{Form::hidden('data[ContentCategory][is_custom_index]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_index_params_json]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_custom_index_row]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_index_row_params_json]', '', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_custom_view]', '1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_view_params_json]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_custom_box]', '1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][custom_box_params_json]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_title]','1', ['id'=>''])}}

                        {{Form::hidden('data[ContentCategory][is_title]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_subtitle]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_content]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_short_description]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_url]','', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_external_url]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_image]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_date]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_time]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_publish]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_sortable]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_featured]','1', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][is_tags]','1', ['id'=>''])}}

                        {{Form::hidden('data[ContentCategory][label_title]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_subtitle]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_content]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_short_description]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_url]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_external_url]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_images]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_date]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][label_time]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][order_field]',' ', ['id'=>''])}}
                        {{Form::hidden('data[ContentCategory][order_direction]',' ', ['id'=>''])}} -->
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    {{Form::label('ContentCategoryItemImage','Default category item image', array('class' => 'col-sm-2 form-control-label'))}}
                     <div class="col-sm-8">
                        <div class="row">
                        @if($media->count() > 0)
                            @php ($url_count = 1)
                                @foreach($media as $med)
                                    @if($med->sub_module == 'images')
                                    <div class="col-sm-4 adding-medias media_ContentCategory_images" data-off="{{$url_count}}">
                                        <div class="jFiler-items jFiler-row">
                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                    <div class="jFiler-item-container">                       
                                                        <div class="jFiler-item-inner">                           
                                                            <div class="jFiler-item-thumb">    
                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'ContentCategory', 'images', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                    <div class="jFiler-item-thumb-image">
                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                    </div> 
                                                                </a>  
                                                            </div>                                    
                                                            <div class="jFiler-item-assets jFiler-row">
                                                            <ul class="list-inline pull-right">
                                                                <li>
                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_ContentData_images" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="ContentCategory" data-submodule="images" ></a>
                                                                </li>                           
                                                            </ul>                                    
                                                            </div>                                
                                                        </div>                            
                                                    </div>                        
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                        @php ($url_count++)
                                    @endif
                                @endforeach
                               <!--  </ul>
                            </div> -->
                        @endif
                        <div class="media_ContentCategory_images_outer hidden" data-off="0" style="display: none;"></div>
                        </div>
                        <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                            <span style="color:#f9c851;">Only jpg/jpeg/png/gif files. Maximum 3 files. Maximum file size: 120MB. Clik on image to edit.</span>
                        </span>
                    </div>  
                    <div class="col-sm-2">
                        <div class="mtf-buttons" style="clear:both;float:right;">
                            <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="ContentCategory" data-submodule="images" data-media_type="image" data-limit="3" data-dimension="" data-file_crop="0">Upload files</button>
                        </div> 
                    </div>
                </div>
                <div class="form-group row">
                    <div class="">
                         {{Form::label('ContentCategoryIsCustomIndex', 'Index/list template', array('class' => 'col-sm-3 form-control-label'))}}
                         <div class="col-sm-9">
                            <div class="radio radio-info radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index]', '1', ($thiscate->is_custom_index == '1')? true:false, array('id'=>'ContentCategoryIsCustomIndex1'))}}
                                {{Form::label('ContentCategoryIsCustomIndex1', 'Custom', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index]', '0',($thiscate->is_custom_index == '0')? true:false, array('id'=>'ContentCategoryIsCustomIndex0'))}}
                               {{Form::label('ContentCategoryIsCustomIndex0', 'Default', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="">
                         {{Form::label('ContentCategoryIsCustomIndexRow', 'Index/list rows template', array('class' => 'col-sm-3 form-control-label'))}}
                         <div class="col-sm-9">
                            <div class="radio radio-info radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index_row]', '1', ($thiscate->is_custom_index_row == '1')? true:false, array('id'=>'ContentCategoryIsCustomIndexRow1'))}}
                                {{Form::label('ContentCategoryIsCustomIndexRow1', 'Custom', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index_row]', 'list',($thiscate->is_custom_index_row == 'list')? true:false, array('id'=>'ContentCategoryIsCustomIndexRowList'))}}
                               {{Form::label('ContentCategoryIsCustomIndexRowList', 'List', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index_row]', 'box',($thiscate->is_custom_index_row == 'box')? true:false, array('id'=>'ContentCategoryIsCustomIndexRowBox'))}}
                               {{Form::label('ContentCategoryIsCustomIndexRowBox', 'Box', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index_row]', 'image',($thiscate->is_custom_index_row == 'image')? true:false, array('id'=>'ContentCategoryIsCustomIndexRowImage'))}}
                               {{Form::label('ContentCategoryIsCustomIndexRowImage', 'Image & title', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_index_row]', '0',($thiscate->is_custom_index_row == '0')? true:false, array('id'=>'ContentCategoryIsCustomIndexRow0'))}}
                               {{Form::label('ContentCategoryIsCustomIndexRow0', 'Default', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                         </div>
                    </div>
                </div>
                <?php
                $custom_index='';
                if(!empty($thiscate->custom_index_row_params_json)){
                     $custom_index = $thiscate->custom_index_row_params_json;
                }
                if($thiscate->is_custom_index_row == 'image'){
                    $styled ="style=display:block;";
                }else{
                    $styled ="style=display:none;";
                }
                
                 ?>
                <div class="form-group row custom_index_row_params_json" {{$styled}}>
                    <div class="col-sm-3">
                        {{Form::label('ContentCategoryCustomIndexRowParamsJsonImageImgOrientation', 'image orientation', array('class' => 'form-control-label'))}}  
                        <div class="">

                           {{Form::select('data[ContentCategory][custom_index_row_params_json][image][img_orientation]',(!empty($imageorientation) ? $imageorientation : []),(!empty($custom_index->image->img_orientation) ? $custom_index->image->img_orientation :''), $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryCustomIndexRowParamsJsonImageImgOrientation','data-style'=>'btn-purple'))}} 
                        </div>                      
                    </div>
                    <div class="col-sm-3">
                        {{Form::label('ContentCategoryCustomIndexRowParamsJsonImageMediumColumns', 'medium size columns', array('class' => 'form-control-label'))}}  
                        <div class="">
                           {{Form::select('data[ContentCategory][custom_index_row_params_json][image][medium_columns]',(!empty($mediumsizecolumns) ? $mediumsizecolumns : []),(!empty($custom_index->image->medium_columns) ? $custom_index->image->medium_columns :''), $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryCustomIndexRowParamsJsonImageMediumColumns','data-style'=>'btn-purple'))}} 
                        </div>                      
                    </div>
                    <div class="col-sm-3">
                        {{Form::label('ContentCategoryCustomIndexRowParamsJsonImageSmallColumns', 'small size columns', array('class' => 'form-control-label'))}}  
                        <div class="">
                           {{Form::select('data[ContentCategory][custom_index_row_params_json][image][small_columns]',(!empty($smallsizecolumns) ? $smallsizecolumns : []),(!empty($custom_index->image->small_columns) ? $custom_index->image->small_columns : ''), $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryCustomIndexRowParamsJsonImageSmallColumns','data-style'=>'btn-purple'))}} 
                        </div>                      
                    </div>
                    <div class="col-sm-3">
                        {{Form::label('ContentCategoryCustomIndexRowParamsJsonImageIsTitle', 'show title', array('class' => 'form-control-label'))}}  
                        <div class="">
                           {{Form::select('data[ContentCategory][custom_index_row_params_json][image][is_title]',(!empty($showtitle) ? $showtitle : []),(!empty($custom_index->image->is_title)) ? $custom_index->image->is_title : '', $attributes=array('class'=>'selectpicker', 'data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select','id'=>'ContentCategoryCustomIndexRowParamsJsonImageIsTitle','data-style'=>'btn-purple'))}} 
                        </div>                      
                    </div>
                </div>
                <div class="form-group row">
                    <div class="">
                         {{Form::label('ContentCategoryIsCustomView', 'View content template', array('class' => 'col-sm-3 form-control-label'))}}
                         <div class="col-sm-9">
                            <div class="radio radio-info radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_view]', '1', ($thiscate->is_custom_view == '1')? true:false, array('id'=>'ContentCategoryIsCustomView1'))}}
                                {{Form::label('ContentCategoryIsCustomView1', 'Custom', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_view]', '0',($thiscate->is_custom_view == '0')? true:false, array('id'=>'ContentCategoryIsCustomView0'))}}
                               {{Form::label('ContentCategoryIsCustomView0', 'Default', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="">
                         {{Form::label('ContentCategoryIsCustomIndex', 'Content box template', array('class' => 'col-sm-3 form-control-label'))}}
                         <div class="col-sm-9">
                            <div class="radio radio-info radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_box]', '1', ($thiscate->is_custom_box == '1')? true:false, array('id'=>'ContentCategoryIsCustomBox1'))}}
                                {{Form::label('ContentCategoryIsCustomBox1', 'Custom', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                                {{Form::radio('data[ContentCategory][is_custom_box]', '0',($thiscate->is_custom_box == '0')? true:false, array('id'=>'ContentCategoryIsCustomBox0'))}}
                               {{Form::label('ContentCategoryIsCustomBox0', 'Default', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="fixed_btn">
                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[ContentCategory][btnEdit]" id="ContentCategoryBtnEdit" data-form-id="ContentCategoryEditForm">
                    Update
                </button>
                <a class="btn btn-primary waves-effect waves-light" href="{{url('admin/cms/content_categories/delete/'.$thiscate->id)}}"> Delete</a>      
            </div>
        </div>
      
     {{ Form::close() }}  
     @include('backend.cms.upload_media_popup',['module_id' =>$thiscate->id,'main_module' =>"ContentCategory"])     
    </div>
</div>
@push('builder_scripts')
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#ContentCategoryParentId').val('{{$thiscate->parent_id}}');
        jQuery('#ContentCategoryParentId').trigger('change');
    })
</script>
@endpush
@endsection     