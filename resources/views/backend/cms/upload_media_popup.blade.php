<!-- Media Library Modal  -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog dialog-width">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <div class="">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Media</h4>
            </div>
        </div>
        <div class="modal-body">
            <ul class="nav nav-tabs">
                <li class=" ">
                    <a href="#MediaFiles" data-toggle="tab" aria-expanded="true" id="MediaFilesUrl">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Media Library</span>
                    </a>
                </li>
                <li class="active">
                    <a href="#UploadFiles" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Upload Files</span>
                    </a>
                </li>
            </ul>   
            <div class="tab-content">
                <div class="tab-pane active" id="UploadFiles">
                    <!-- Hidden field for catching up the image src-->
                    <input type="hidden" name="imgTitle" id="" class="form-control selectedMediaTitle" value="" readonly="">
                    <input type="hidden" name="imgsrc" id="" class="form-control selectedMediaSrc" value="" readonly="">
                    <!-- Hidden field for catching up the newly added media -->
                    <input type="hidden" name="imgid" id="" class="form-control selectedMediaId" value="">

                   <div class="form-group row">
                        <div class="col-sm-9">
                            <div class="padding-left-0 padding-right-0">
                                <div id="banner_image" class="dropzone channel_image" data-limit="3" data-media_type="image" data-image-for="player" data-module="Channel" data-preview-ele="#banner_prev">
                                    <div class="col-sm-9 previewDiv" id="banner_prev"></div>
                                    <div class="col-sm-3 dz-message d-flex flex-column">
                                       <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                        Drag &amp; Drop here or click
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="MediaFiles">
                    <div class="row">
                        <div class="col-lg-8" style="border-right:1px solid #e4e3e3;">
                            <div class="media_popup">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" data-type="image" id="images" value="0" name="filterMedia" onclick="getMedia('image','', true)" checked>
                                            <label for="images"> Images </label>
                                        </div>
                                        <div class="radio radio-pink radio-inline">
                                            <input type="radio" data-type="audio" id="audios" value="1" name="filterMedia" onclick="getMedia('audio','',true)" >
                                            <label for="audios"> Audios </label>
                                        </div> 
                                        <div class="radio radio-purple radio-inline">
                                            <input type="radio" data-type="video" id="videos" value="2" name="filterMedia" onclick="getMedia('video','',true)" >
                                            <label for="videos"> Videos </label>
                                        </div> 
                                    </div>

                                    <div class="col-md-2">
                                        <div class="radio radio-purple radio-inline">
                                            <select id ='mediaDimension' name='mediaDimension' class="form-control">
                                                <option value="">Size</option>
                                                <option value="728x90">728x90</option>
                                                <option value="625x258">625x258</option>
                                                <option value="500x200">500x200</option>
                                                <option value="300x250">300x250</option>
                                                <option value="170x300">170x300</option>
                                                <option value="170x137">170x137</option>
                                                <option value="120x300">120x300</option>
                                                <option value="120x300">120x150</option>
                                            </select>
                                        </div> 
                                    </div>
                                    <!-- Filter by title -->
                                    <div class="col-md-3">
                                        <div class="form-group">                            
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-times" data-page="popup" id="remove_search"></i></button>
                                                </span>
                                                <input type="text" id="valueMediaByTitle" name="data[Media][f][title]" class="form-control" placeholder="Search  by Title">                                
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn waves-effect waves-light btn-primary" data-page="popup" id="filterMediaByTitle"><i class="fa fa-search"></i></button>
                                                </span>
                                                
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                               <!--  <ul class="grid row" id="mediagrid" style="overflow-y: scroll; height:650px;">
                                    
                                </ul> -->
                                <div class="row">
                                    <div id="media_append_section">
                                        <div class="jFiler-items jFiler-row">
                                            <ul class="jFiler-items-list jFiler-items-grid">
                                            </ul>
                                        </div>
                                        <div id="media_loading_image_gif" style="background-color: rgb(255, 255, 255); display: none;">
                                            <center><img id="loading-image" src="{{url('/')}}/storage/app/public/media/default/loader1.gif" alt="Loading..."></center>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div id="AttachmentDetail">
                            </div>
                        </div>
                    </div>     
                </div>
            </div>    
        </div>                      
        <div class="modal-footer">
            <input type="hidden" class="module_id" id="" name="module_id" value="{{$module_id}}">
            <input type="hidden" class="main_module" id="" name="main_module" value="{{$main_module}}">
            <input type="hidden" class="media_limit" name="media_limit" value="">
			<input type="hidden" class="sub_module" id="" name="sub_module" value="">
            <input type="hidden" class="file_name" id="" name="file_name" value="">
			<input type="hidden" class="file_crop" id="" name="file_crop" value="">
			<input type="hidden" class="remaining_file" id="" name="remaining_file" value="">
            <button type="button" class="btn btn-info btn-lg select_choose_media" id="" disabled>Add media</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
      
    </div>
</div> 

<!-- edit caption  -->
<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModal2Label">Edit Media &nbsp;&nbsp;:<span class="Media_Title_"></span></h4>
            </div>     
            {{ Form::open(array('url' => 'admin/cms/media/edit', 'id' => 'MediaAdminEditForm')) }}
            <div class="modal-body">               
                <h4></h4>
                <div class="center-media-image">
                    <div class="row">
                        <div class="col-sm-12 form-group row">
                            {{Form::label('MediaTitle_', 'Title', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('data[Media][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaTitle_'))}}                            
                            </div>
                        </div>
                        <div class="col-sm-12 form-group row">
                            {{Form::label('MediaAlt_', 'Alt', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('data[Media][alt]', $value = null, $attributes = array('class'=>'form-control',  'id'=>'MediaAlt_'))}}                            
                            </div>
                        </div>
                        <div class="col-sm-12 form-group row">
                            {{Form::label('MediaCaption_', 'Caption', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('data[Media][caption]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaCaption_'))}}                            
                            </div>
                        </div>
                        <div class="col-sm-12 form-group row">
                            {{Form::label('MediaDescription_', 'Descritpion', array('class' => 'col-sm-2 form-control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('data[Media][description]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaDescription_'))}}                            
                            </div>
                            {{Form::hidden('data[Media][id]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaId_'))}}
                        </div>
                    </div> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="button" id="edit_media_btn" class="btn btn-primary waves-effect waves-light" data-form-id="">Save changes</button>
            </div>
            {{ Form::close() }}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->