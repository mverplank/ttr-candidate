@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Media Library</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Content
                        </li>
                        <li class="active">
                            Media Library
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->  
        <div class="row">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="images" value="0" name="filterMedia" onclick="filterMedia('images')" checked>
                    <label for="images"> Images </label>
                </div>
                <div class="radio radio-pink radio-inline">
                    <input type="radio" id="audios" value="1" name="filterMedia" onclick="filterMedia('audios')" >
                    <label for="audios"> Audios </label>
                </div> 
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="videos" value="2" name="filterMedia" onclick="filterMedia('videos')" >
                    <label for="videos"> Videos </label>
                </div>
                <div style="float:right;">
                    <a href="{{url('/admin/cms/media/add/images')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Images</a>
                    <a href="{{url('/admin/cms/media/add/audios')}}" id="hosts_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Audios</a>
                    <a href="{{url('/admin/cms/media/add/videos')}}" id="hosts_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Videos</a>
                </div>    
            </div>  
        </div>     
        <!-- end row -->   
        <?php //echo "<pre>";print_R($images);die('success');?>       
        <ul class="grid effect-2" id="grid">
        @foreach($images as $image)
            <li>
                <a href="{{$image['url']}}" target="_blank">
                    <img src="{{$image['url']}}" alt="{{$image['name']}}">
                    <p>{{$image['name']}}</p>
                </a>
            </li>        
        @endforeach        
    </div> <!-- container -->
</div> <!-- content -->

@endsection
