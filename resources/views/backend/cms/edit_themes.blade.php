@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Edit Theme: {{$theme->name}}</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="">
                     Content
                  </li>
                  <li class="">
                     <a href="{{route('get_cms_themes')}}">Pages</a>
                  </li>
                  <li class="active">
                     edit
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/themes/edit/'.$theme->id, 'id'=>'CmsThemeAdminEditForm')) }}
          <div class="form_start">
             <div class="row">
              <div class="col-md-12">
                <div class="card-box">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#commonSettings" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Common Settings</span>
                      </a>
                    </li>
                    <li class="">
                      <a href="#components" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Components</span>
                      </a>
                    </li>
                  </ul>
                  <?php $data_json =(array)json_decode($theme->data_json, true);
                     // echo"<pre>";print_r($data_json);die;
                   ?>
                  <div class="tab-content">
                    <div class="tab-pane active" id="commonSettings">
                      <div class="row">
                        <div class="card-box">
                          <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">General</h4>
                          <div class="form-group row">
                             {{Form::label('ContentCategoryIsCustomIndex', 'For widget box use color scheme', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-9">
                            <div class="radio radio-info radio-inline">
                              {{Form::radio('data[CustomTheme][data_json][defaults][box_color_scheme]', '1',($data_json != NULL && $data_json['defaults']['box_color_scheme'] == '1')?true : false, array('id'=>'CustomThemeDataJsonDefaultsBoxColorSchemeDark'))}}
                              {{Form::label('CustomThemeDataJsonDefaultsBoxColorSchemeDark', 'dark', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            <div class="radio radio-warning radio-inline">
                              {{Form::radio('data[CustomTheme][data_json][defaults][box_color_scheme]', 'light',($data_json != NULL && $data_json['defaults']['box_color_scheme'] == '0')?true : false, array('id'=>'CustomThemeDataJsonDefaultsBoxColorSchemeLight'))}}
                               {{Form::label('CustomThemeDataJsonDefaultsBoxColorSchemeLight', 'primary', array('class' => 'col-sm-12 form-control-label'))}}
                            </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            {{Form::label('CustomThemeDataJsonDefaultsRadius', 'Default global radius [px]', array('class' => 'col-sm-3 form-control-label'))}} 
                            <div class="col-sm-9">
                            <div class="input-group">                                   
                              {{Form::number('data[CustomTheme][data_json][defaults][radius]',($data_json != NULL && $data_json['defaults']['radius']) ? $data_json['defaults']['radius'] : '', $attributes = array('class'=>'vertical-spin form-control','id'=>'CustomThemeDataJsonDefaultsRadius','data-forma'=>"1", 'data-forma-def'=>"1", 'data-type'=>"int", 'autocomplete'=>"off",'type'=>"number", 'min'=>"0",'step'=>"1"))}}
                            </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            {{Form::label('CustomThemeDataJsonDefaultsBox-shadow','Default box shadow', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-9">
                              {{ Form::textarea('data[CustomTheme][data_json][defaults][box-shadow]',($data_json != NULL && $data_json['defaults']['box-shadow']) ? $data_json['defaults']['box-shadow'] : '', ['id' => 'CustomThemeDataJsonDefaultsBox-shadow', 'class'=>'form-control','rows'=>4, 'cols'=>50]) }}
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                HTML HEAD meta description for search engines. Recommended length: aprox. 155 characters.
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="card-box">
                              <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Primary Colors</h4>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonLightStylesAnyColor', 'Default color', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][light][styles][any][color]',
                                    ($data_json != NULL && $data_json['light']['styles']['any']['color']) ? $data_json['light']['styles']['any']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAnyColor','data-color-format'=>'rgb'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesAnyBackground-color', 'background color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][any][background-color]', ($data_json != NULL && $data_json['light']['styles']['any']['background-color']) ? $data_json['light']['styles']['any']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAnyBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <div class="form-group row">
                                {{Form::label('CustomThemeDataJsonLightStylesAnyBackground','Background style', array('class' => 'form-control-label'))}}
                                  {{ Form::textarea('data[CustomTheme][data_json][light][styles][any][background]',($data_json != NULL && $data_json['light']['styles']['any']['background']) ? $data_json['light']['styles']['any']['color'] : '', ['id' => 'CCustomThemeDataJsonLightStylesAnyBackground', 'class'=>'form-control','rows'=>4, 'cols'=>50]) }}
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesH1Color', 'Header Main (H1) Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][h1][color]', ($data_json != NULL && $data_json['light']['styles']['h1']['color']) ? $data_json['light']['styles']['h1']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesH1Color'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesH2Color', 'Header Section/Box (H2) Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][h2][color]',($data_json != NULL && $data_json['light']['styles']['h2']['color']) ? $data_json['light']['styles']['h2']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesH2Color'))}}
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesH3Color', 'Header Section/Box sub-header (H3) Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][h3][color]',($data_json != NULL && $data_json['light']['styles']['h3']['color']) ? $data_json['light']['styles']['h3']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesH3Color'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesPColor', 'Text Paragraph Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][p][color]',($data_json != NULL && $data_json['light']['styles']['p']['color']) ? $data_json['light']['styles']['p']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesPColor'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesAColor', 'Link color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][a][color]',($data_json != NULL && $data_json['light']['styles']['a']['color']) ? $data_json['light']['styles']['a']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesABackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][a][background-color]', ($data_json != NULL && $data_json['light']['styles']['a']['background-color']) ? $data_json['light']['styles']['a']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesABackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesAhoverColor', 'Link hovered color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][ahover][color]',($data_json != NULL && $data_json['light']['styles']['ahover']['color']) ? $data_json['light']['styles']['ahover']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAhoverColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesAhoverBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][ahover][background-color]', ($data_json != NULL && $data_json['light']['styles']['ahover']['background-color']) ? $data_json['light']['styles']['ahover']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAhoverBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                               <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesAvisitedColor', 'Link visited color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][avisited][color]',($data_json != NULL && $data_json['light']['styles']['avisited']['color']) ? $data_json['light']['styles']['avisited']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAvisitedColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesAvisitedBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][avisited][background-color]',($data_json != NULL && $data_json['light']['styles']['avisited']['background-color']) ? $data_json['light']['styles']['avisited']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesAvisitedBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesButtonColor', 'Button color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][button][color]',($data_json != NULL && $data_json['light']['styles']['button']['color']) ? $data_json['light']['styles']['button']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesButtonColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesButtonBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][button][background-color]',($data_json != NULL && $data_json['light']['styles']['button']['background-color']) ? $data_json['light']['styles']['button']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesButtonBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesButtonhoverColor', 'Button hovered color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][buttonhover][color]',($data_json != NULL && $data_json['light']['styles']['buttonhover']['color']) ? $data_json['light']['styles']['buttonhover']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesButtonhoverColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesButtonhoverBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][buttonhover][background-color]',($data_json != NULL && $data_json['light']['styles']['buttonhover']['background-color']) ? $data_json['light']['styles']['buttonhover']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesButtonhoverBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesInputColor', 'Input color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][input][color]',($data_json != NULL && $data_json['light']['styles']['input']['color']) ? $data_json['light']['styles']['input']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesInputColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesInputBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][input][background-color]', ($data_json != NULL && $data_json['light']['styles']['input']['background-color']) ? $data_json['light']['styles']['input']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesInputBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesInputfocusColor', 'Input focused color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][inputfocus][color]',($data_json != NULL && $data_json['light']['styles']['inputfocus']['color']) ? $data_json['light']['styles']['inputfocus']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesInputfocusColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesInputfocusBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][inputfocus][background-color]',($data_json != NULL && $data_json['light']['styles']['inputfocus']['background-color']) ? $data_json['light']['styles']['inputfocus']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesInputfocusBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesLabelColor', 'Label color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][label][color]',($data_json != NULL && $data_json['light']['styles']['label']['color']) ? $data_json['light']['styles']['label']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesLabelColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesLabelBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][label][background-color]', ($data_json != NULL && $data_json['light']['styles']['label']['color']) ? $data_json['light']['styles']['label']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesLabelBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesClass-activeColor', 'Active element color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][class-active][color]', ($data_json != NULL && $data_json['light']['styles']['class-active']['color']) ? $data_json['light']['styles']['class-active']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesClass-activeColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonLightStylesClass-activeBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][light][styles][class-active][background-color]',($data_json != NULL && $data_json['light']['styles']['class-active']['background-color']) ? $data_json['light']['styles']['class-active']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonLightStylesClass-activeBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="card-box">
                              <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Secondary Colors</h4>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAnyColor', 'Default color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][any][color]',($data_json != NULL && $data_json['dark']['styles']['any']['color']) ? $data_json['dark']['styles']['any']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAnyColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAnyBackground-color', 'background color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][any][background-color]', ($data_json != NULL && $data_json['dark']['styles']['any']['background-color']) ? $data_json['dark']['styles']['any']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAnyBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="col-sm-12">
                                <div class="form-group row">
                                  {{Form::label('CustomThemeDataJsonDarkStylesAnyBackground','Background style', array('class' => 'form-control-label'))}}
                                    {{ Form::textarea('data[CustomTheme][data_json][dark][styles][any][background]',($data_json != NULL && $data_json['dark']['styles']['any']['background']) ? $data_json['dark']['styles']['any']['background'] : '', ['id' => 'CustomThemeDataJsonDarkStylesAnyBackground', 'class'=>'form-control','rows'=>4, 'cols'=>50]) }}
                                </div>
                              </div>
                              <!-- col-sm-12 end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesH1Color', 'Header Main (H1) Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][h1][color]',($data_json != NULL && $data_json['dark']['styles']['h1']['color']) ? $data_json['dark']['styles']['h1']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesH1Color'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesH2Color', 'Header Section/Box (H2) Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][h2][color]',($data_json != NULL && $data_json['dark']['styles']['h2']['color']) ? $data_json['dark']['styles']['h2']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesH2Color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesH3Color', 'Header Section/Box sub-header (H3) Color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][h3][color]',($data_json != NULL && $data_json['dark']['styles']['h3']['color']) ? $data_json['dark']['styles']['h3']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesH3Color'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesPColor', 'Text Paragraph Color', array('class' => 'form-control-label'))}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][p][color]',($data_json != NULL && $data_json['dark']['styles']['p']['color']) ? $data_json['dark']['styles']['p']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesPColor'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAColor', 'Link color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][a][color]',($data_json != NULL && $data_json['dark']['styles']['a']['color']) ? $data_json['dark']['styles']['a']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesABackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][a][background-color]',($data_json != NULL && $data_json['dark']['styles']['a']['background-color']) ? $data_json['dark']['styles']['a']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesABackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAhoverColor', 'Link hovered color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][ahover][color]',($data_json != NULL && $data_json['dark']['styles']['ahover']['color']) ? $data_json['dark']['styles']['ahover']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAhoverColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAhoverBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][ahover][background-color]', ($data_json != NULL && $data_json['dark']['styles']['ahover']['background-color']) ? $data_json['dark']['styles']['ahover']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAhoverBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAvisitedColor', 'Link visited color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][avisited][color]',($data_json != NULL && $data_json['dark']['styles']['avisited']['color']) ? $data_json['dark']['styles']['avisited']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAvisitedColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesAvisitedBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][avisited][background-color]', ($data_json != NULL && $data_json['dark']['styles']['avisited']['background-color']) ? $data_json['dark']['styles']['avisited']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesAvisitedBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesButtonColor', 'Button color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][button][color]',($data_json != NULL && $data_json['dark']['styles']['button']['color']) ? $data_json['dark']['styles']['button']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesButtonColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesButtonBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][button][background-color]', ($data_json != NULL && $data_json['dark']['styles']['button']['background-color']) ? $data_json['dark']['styles']['button']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesButtonBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesButtonhoverColor', 'Button hovered color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][buttonhover][color]',($data_json != NULL && $data_json['dark']['styles']['buttonhover']['color']) ? $data_json['dark']['styles']['buttonhover']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesButtonhoverColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesButtonhoverBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][buttonhover][background-color]',($data_json != NULL && $data_json['dark']['styles']['buttonhover']['background-color']) ? $data_json['dark']['styles']['buttonhover']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesButtonhoverBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesInputColor', 'Input color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][input][color]',($data_json != NULL && $data_json['dark']['styles']['buttonhover']['background-color']) ? $data_json['dark']['styles']['buttonhover']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesInputColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesInputBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][input][background-color]', ($data_json != NULL && $data_json['dark']['styles']['input']['background-color']) ? $data_json['dark']['styles']['input']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesInputBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesLabelColor', 'Label color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][label][color]',($data_json != NULL && $data_json['dark']['styles']['label']['color']) ? $data_json['dark']['styles']['label']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesLabelColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesLabelBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][label][background-color]', ($data_json != NULL && $data_json['dark']['styles']['label']['background-color']) ? $data_json['dark']['styles']['label']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesLabelBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesClass-activeColor', 'Active element color', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][class-active][color]',($data_json != NULL && $data_json['dark']['styles']['class-active']['color']) ? $data_json['dark']['styles']['class-active']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesClass-activeColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonDarkStylesClass-activeBackground-color', 'background', array('class' => 'form-control-label'))}}
                                      {{Form::text('data[CustomTheme][data_json][dark][styles][class-active][background-color]',($data_json != NULL && $data_json['dark']['styles']['class-active']['background-color']) ? $data_json['dark']['styles']['class-active']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonDarkStylesClass-activeBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <!-- card-box col-sm-6 end -->
                          <div class="col-sm-6">
                            <div class="card-box">
                              <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Typography</h4>
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH1Font-family', 'Main Header (H1) font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h1][font-family]',[], ($data_json != NULL && $data_json['styles']['h1']['font-family']) ? $data_json['styles']['h1']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH1Font-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesH1Font-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][h1][font-size]',($data_json != NULL && $data_json['styles']['h1']['font-size']) ? $data_json['styles']['h1']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesH1Font-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesH1Line-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][h1][line-height]',($data_json != NULL && $data_json['styles']['h1']['line-height']) ? $data_json['styles']['h1']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesH1Line-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH1Font-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h1][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['h1']['font-weight']) ? $data_json['styles']['h1']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH1Font-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH1Text-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h1][text-decoration]',$fontstyle, ($data_json != NULL && $data_json['styles']['h1']['text-decoration']) ? $data_json['styles']['h1']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH1Text-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH1Text-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h1][text-align]',$fontalign, ($data_json != NULL && $data_json['styles']['h1']['text-align']) ? $data_json['styles']['h1']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH1Text-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- eeeeeeeeeee -->
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH2Font-family', 'Section/Box Header (H2) font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h2][font-family]',[], ($data_json != NULL && $data_json['styles']['h2']['font-family']) ? $data_json['styles']['h2']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH2Font-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesH2Font-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][h2][font-size]',($data_json != NULL && $data_json['styles']['h2']['font-size']) ? $data_json['styles']['h2']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesH2Font-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesH2Line-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][h2][line-height]',($data_json != NULL && $data_json['styles']['h2']['line-height']) ? $data_json['styles']['h2']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesH2Line-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH2Font-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h2][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['h2']['font-weight']) ? $data_json['styles']['h2']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH2Font-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH2Text-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h2][text-decoration]',$fontstyle, ($data_json != NULL && $data_json['styles']['h2']['text-decoration']) ? $data_json['styles']['h2']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH2Text-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH2Text-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h2][text-align]',$fontalign,($data_json != NULL && $data_json['styles']['h2']['text-align']) ? $data_json['styles']['h2']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH2Text-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- qqqqqq -->
                                <!-- eeeeeeeeeee -->
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH3Font-family', 'Section/Box Sub-Header (H3) font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h3][font-family]',[], ($data_json != NULL && $data_json['styles']['h3']['font-family']) ? $data_json['styles']['h3']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH3Font-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesH3Font-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][h3][font-size]',($data_json != NULL && $data_json['styles']['h3']['font-size']) ? $data_json['styles']['h3']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesH3Font-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesH3Line-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][h3][line-height]',($data_json != NULL && $data_json['styles']['h3']['line-height']) ? $data_json['styles']['h3']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesH3Line-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH3Font-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h3][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['h3']['font-weight']) ? $data_json['styles']['h3']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH3Font-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH3Text-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h3][text-decoration]',$fontstyle, ($data_json != NULL && $data_json['styles']['h3']['text-decoration']) ? $data_json['styles']['h3']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH3Text-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesH3Text-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][h3][text-align]',$fontalign,($data_json != NULL && $data_json['styles']['h3']['text-align']) ? $data_json['styles']['h3']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesH3Text-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- qqqqqq -->
                                <!-- eeeeeeeeeee -->
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesPFont-family', 'Paragraph text font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][p][font-family]',[],($data_json != NULL && $data_json['styles']['p']['text-align']) ? $data_json['styles']['p']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesPFont-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesPFont-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][p][font-size]',($data_json != NULL && $data_json['styles']['p']['font-size']) ? $data_json['styles']['p']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesPFont-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesPLine-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][p][line-height]',($data_json != NULL && $data_json['styles']['p']['line-height']) ? $data_json['styles']['p']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesPLine-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesPFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][p][font-weight]',$fontweight,($data_json != NULL && $data_json['styles']['p']['font-weight']) ? $data_json['styles']['p']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesPFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesPText-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][p][text-decoration]',$fontstyle, ($data_json != NULL && $data_json['styles']['p']['text-decoration']) ? $data_json['styles']['p']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesPText-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesPText-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][p][text-align]',$fontalign,($data_json != NULL && $data_json['styles']['p']['text-align']) ? $data_json['styles']['p']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesPText-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- qqqqqq -->
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="card-box">
                              <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Typography elements</h4>
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesAFont-family', 'Links font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][a][font-family]',[], ($data_json != NULL && $data_json['styles']['a']['font-family']) ? $data_json['styles']['a']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesAFont-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesAFont-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][a][font-size]',($data_json != NULL && $data_json['styles']['a']['font-size']) ? $data_json['styles']['a']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesAFont-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesALine-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][a][line-height]',($data_json != NULL && $data_json['styles']['a']['line-height']) ? $data_json['styles']['a']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesALine-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesAFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][a][font-weight]',$fontweight,($data_json != NULL && $data_json['styles']['a']['line-height']) ? $data_json['styles']['a']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesAFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesAText-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][a][text-decoration]',$fontstyle, ($data_json != NULL && $data_json['styles']['a']['text-decoration']) ? $data_json['styles']['a']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesAText-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesAText-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][a][text-align]',$fontalign,($data_json != NULL && $data_json['styles']['a']['text-align']) ? $data_json['styles']['a']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesAText-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesAhoverFont-size', 'Hovered size', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][ahover][font-size]',($data_json != NULL && $data_json['styles']['ahover']['font-size']) ? $data_json['styles']['ahover']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesAhoverFont-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesAhoverFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][ahover][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['ahover']['font-weight']) ? $data_json['styles']['ahover']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesAhoverFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                     {{Form::label('CustomThemeDataJsonStylesAhoverText-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][ahover][text-decoration]',$fontstyle,($data_json != NULL && $data_json['styles']['ahover']['text-decoration']) ? $data_json['styles']['ahover']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesAhoverText-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                      
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- eeeeeeeeeee -->
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesButtonFont-family', 'Buttons font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][button][font-family]',[],($data_json != NULL && $data_json['styles']['button']['font-family']) ? $data_json['styles']['button']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesButtonFont-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesButtonFont-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][button][font-size]',($data_json != NULL && $data_json['styles']['button']['font-size']) ? $data_json['styles']['button']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesButtonFont-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesButtonLine-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][button][line-height]', ($data_json != NULL && $data_json['styles']['button']['line-height']) ? $data_json['styles']['button']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesButtonLine-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesButtonFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][button][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['button']['font-weight']) ? $data_json['styles']['button']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesButtonFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesButtonText-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][button][text-decoration]',$fontstyle,($data_json != NULL && $data_json['styles']['button']['text-decoration']) ? $data_json['styles']['button']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesButtonText-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesButtonText-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][button][text-align]',$fontalign, ($data_json != NULL && $data_json['styles']['button']['text-align']) ? $data_json['styles']['button']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesButtonText-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- qqqqqq -->
                                <!-- eeeeeeeeeee -->
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesInputFont-family', 'Inputs font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][input][font-family]',[], ($data_json != NULL && $data_json['styles']['input']['font-family']) ? $data_json['styles']['input']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesInputFont-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesInputFont-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][input][font-size]',($data_json != NULL && $data_json['styles']['input']['font-size']) ? $data_json['styles']['input']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesInputFont-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesInputLine-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][input][line-height]',($data_json != NULL && $data_json['styles']['input']['line-height']) ? $data_json['styles']['input']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesInputLine-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesInputFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][input][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['input']['font-weight']) ? $data_json['styles']['input']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesInputFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesInputText-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][input][text-decoration]',$fontstyle,($data_json != NULL && $data_json['styles']['input']['text-decoration']) ? $data_json['styles']['input']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesInputText-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesInputText-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][input][text-align]',$fontalign, ($data_json != NULL && $data_json['styles']['input']['text-align']) ? $data_json['styles']['input']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesInputText-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- qqqqqq -->
                                <!-- eeeeeeeeeee -->
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesLabelFont-family', 'Input labels font and size', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][label][font-family]',[], ($data_json != NULL && $data_json['styles']['label']['font-family']) ? $data_json['styles']['label']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesLabelFont-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesLabelFont-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][label][font-size]',($data_json != NULL && $data_json['styles']['label']['font-size']) ? $data_json['styles']['label']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesLabelFont-size','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        {{Form::label('CustomThemeDataJsonStylesLabelLine-height', 'line height', array('class' => 'form-control-label'))}}
                                        {{Form::text('data[CustomTheme][data_json][styles][label][line-height]',($data_json != NULL && $data_json['styles']['label']['line-height']) ? $data_json['styles']['label']['line-height'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesLabelLine-height','placeholder'=>'rem'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesLabelFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][label][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['label']['font-weight']) ? $data_json['styles']['label']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesLabelFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesLabelText-decoration', 'style', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][label][text-decoration]',$fontstyle,($data_json != NULL && $data_json['styles']['label']['text-decoration']) ? $data_json['styles']['label']['text-decoration'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesLabelText-decoration', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeDataJsonStylesLabelText-align', 'align', array('class' => 'form-control-label'))}}
                                      {{Form::select('data[CustomTheme][data_json][styles][label][text-align]',$fontalign, ($data_json != NULL && $data_json['styles']['label']['text-align']) ? $data_json['styles']['label']['text-align'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesLabelText-align', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                    </div>
                                  </div>
                                </div>
                                <!-- row end -->
                                <!-- qqqqqq -->
                            </div>
                          </div>
                        </div>
                        <!-- row cord box end -->
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="card-box">
                              <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Site Icon</h4>
                                <div class="form-group">
                                  {{Form::label('CustomThemeDataJsonStylesBodyBackground-color', 'Icon Image', array('class' => 'form-control-label'))}}
                                  
                                </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="card-box">
                              <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Page Background</h4>
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesBodyBackground-color', 'Color', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][styles][body][background-color]',($data_json != NULL && $data_json['styles']['body']['background-color']) ? $data_json['styles']['body']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonStylesBodyBackground-color'))}}
                                  </div>
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesBodyBackground-color', 'Image', array('class' => 'form-control-label'))}}
                                    
                                  </div>
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesBodyBackground','Background style', array('class' => 'form-control-label'))}}
                                      {{ Form::textarea('data[CustomTheme][data_json][styles][body][background]',($data_json != NULL && $data_json['styles']['body']['background']) ? $data_json['styles']['body']['background'] : '', ['id' => 'CustomThemeDataJsonStylesBodyBackground', 'class'=>'form-control','rows'=>4, 'cols'=>50]) }}
                                  </div>
                                  <div class="row">
                                    <div class="form-group">
                                      {{Form::label('CustomThemeD', 'Position / Repeat / Attachment / Size', array('class' => 'form-control-label'))}}
                                    <div class="col-sm-3">

                                        {{Form::select('data[CustomTheme][data_json][styles][body][background-position]',$position, ($data_json != NULL && $data_json['styles']['body']['background-position']) ? $data_json['styles']['body']['background-position'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesBodyBackground-position', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}

                                    </div>
                                    <div class="col-sm-3">
                                     
                                        {{Form::select('data[CustomTheme][data_json][styles][body][background-repeat]',$repeat, ($data_json != NULL && $data_json['styles']['body']['background-repeat']) ? $data_json['styles']['body']['background-repeat'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesBodyBackground-repeat', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                      
                                    </div>
                                    <div class="col-sm-3">
                                      
                                        {{Form::select('data[CustomTheme][data_json][styles][body][background-attachment]',$attachment,($data_json != NULL && $data_json['styles']['body']['background-attachment']) ? $data_json['styles']['body']['background-attachment'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesBodyBackground-attachment', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                      
                                    </div>
                                    <div class="col-sm-3">
                                      
                                        {{Form::select('data[CustomTheme][data_json][styles][body][background-size]',$size, ($data_json != NULL && $data_json['styles']['body']['background-size']) ? $data_json['styles']['body']['background-size'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesBodyBackground-size', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                      
                                    </div>
                                    </div>
                                  </div>
                            </div>
                          </div>
                        </div>
                        <!-- row cord box end -->
                      </div>
                    </div>
                    <div class="tab-pane" id="components">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="card-box">
                            <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Top Bar</h4>
                              <div class="row">
                                <div class="col-md-8">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesClass-top-barAFont-family', 'Font family and size', array('class' => 'form-control-label'))}}
                                    {{Form::select('data[CustomTheme][data_json][styles][class-top-bar_a][font-family]',[], ($data_json != NULL && $data_json['styles']['class-top-bar_a']['font-family']) ? $data_json['styles']['class-top-bar_a']['font-family'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesClass-top-barAFont-family', 'class'=>'form-control bfh-googlefonts','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesLabelFont-size', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][styles][class-top-bar_a][font-size]',($data_json != NULL && $data_json['styles']['class-top-bar_a']['font-size']) ? $data_json['styles']['class-top-bar_a']['font-size'] : '', $attributes = array('class'=>'form-control','id'=>'CustomThemeDataJsonStylesClass-top-barAFont-size','placeholder'=>'rem'))}}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesClass-top-barAFont-weight', 'weight', array('class' => 'form-control-label'))}}
                                    {{Form::select('data[CustomTheme][data_json][styles][class-top-bar_a][font-weight]',$fontweight, ($data_json != NULL && $data_json['styles']['class-top-bar_a']['font-weight']) ? $data_json['styles']['class-top-bar_a']['font-weight'] : '', $attributes=array('id'=>'CustomThemeDataJsonStylesClass-top-barAFont-weight', 'class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-font'=>'Lato'))}}
                                  </div>
                                </div>
                              </div>                             
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesClass-top-barAColor', 'Menu item color', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][styles][class-top-bar_a][color]',($data_json != NULL && $data_json['styles']['class-top-bar_a']['color']) ? $data_json['styles']['class-top-bar_a']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonStylesClass-top-barAColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesClass-top-barABackground-color', 'background', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][styles][class-top-bar_a][background-color]', ($data_json != NULL && $data_json['styles']['class-top-bar_a']['background-color']) ? $data_json['styles']['class-top-bar_a']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonStylesClass-top-barABackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesClass-top-barAhoverColor', 'Menu item hovered color', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][styles][class-top-bar_ahover][color]',($data_json != NULL && $data_json['styles']['class-top-bar_ahover']['color']) ? $data_json['styles']['class-top-bar_ahover']['color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonStylesClass-top-barAhoverColor'))}}
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    {{Form::label('CustomThemeDataJsonStylesClass-top-barAhoverBackground-color', 'background', array('class' => 'form-control-label'))}}
                                    {{Form::text('data[CustomTheme][data_json][styles][class-top-bar_ahover][background-color]',($data_json != NULL && $data_json['styles']['class-top-bar_ahover']['background-color']) ? $data_json['styles']['class-top-bar_ahover']['background-color'] : '', $attributes = array('class'=>'form-control colorpicker-element','id'=>'CustomThemeDataJsonStylesClass-top-barAhoverBackground-color'))}}
                                  </div>
                                </div>
                              </div>
                              <!-- row end -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!-- end col -->
            </div> 
            <div class="form-group">
              <div class="fixed_btn">
                <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Page][btnUpdate]" id="CmsthemeBtnAdd" data-form-id="CmsThemeAdminEditForm">
                  Edit
                </button>          
              </div>
            </div>  
          </div>
        {{ Form::close() }}
    </div>
</div>
@endsection