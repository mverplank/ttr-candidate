<span style="display:none;" class="media_module_type" data-type="{{$type}}" data-total_media="{{$total_pages}}"></span>
@if(!empty($media))
    @foreach($media as $image)
    <li class="jFiler-item" data-jfiler-index="1" data-id="{{$image['id']}}" style="">
        <div class="jFiler-item-container">                       
            <div class="jFiler-item-inner">                           
                <div class="jFiler-item-thumb">    
                    <a href="javascript:void(0)" onclick="openMediaInfo(this)" data-name="{{$image['name']}}" id="{{$image['id']}}" src="{{$image['src']}}" data-url="{{$image['url']}}" alt="{{$image['alt']}}" data-created_at="{{$image['created_at']}}" data-updated_at="{{$image['updated_at']}}" data-title="{{$image['title']}}" data-caption="{{$image['caption']}}" data-description="{{$image['description']}}" data-type="{{$image['type']}}" data-uploaded_by="{{$image['uploaded_by']}}" data-filename="{{$image['filename']}}" data-uploaded_area="{{$image['uploaded_area']}}">    <div class="jFiler-item-status"></div>
                        <div class="jFiler-item-info">            
                            <span class="jFiler-item-title">
                                <b title="{{$image['name']}}">{{$image['name']}}</b>
                            </span>
                            @if(isset($image['size']))
                            <span class="jFiler-item-others">{{$image['size']}} KB</span> 
                            @endif
                        </div>                                        
                        <div class="jFiler-item-thumb-image {{$image['class']}}">
                            <img src="{{$image['src']}}" draggable="false">
                        </div> 
                    </a>  
                </div>                                        
            </div>                            
        </div>                        
    </li>
    @endforeach
<!-- end row -->
@endif