@extends('backend.layouts.main')
@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Content Type</h4>
                    <ol class="breadcrumb p-0 m-0">                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="">
                            Content
                        </li>
                        <li class="active">
                            Manage Entries
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
         <!-- end row -->
        <div class="row">
		@if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card-box">
               
                <div class="form-group row">
                     {{Form::label('ContentCategoryId', 'Content Type', array('class' => 'col-sm-2 form-control-label'))}}  
                    <div class="col-md-6">
                     
                       {!! $content_cate !!}
                    </div>
                    <div class="col-md-4"> 
                        <a href="#" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5" id='content_cate_btn' disabled> Edit
                        </a> 
                        <a href="{{url('admin/cms/content_categories/add')}}" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5"> Add new
                        </a>                            
                        <a href="{{route('get_tree')}}" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5"> View tree
                        </a>   
                    </div>
                </div>
            </div>
        </div>     
        <!-- end row --> 
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    
                    <span id="drag_drop" data-drag="{{$drag}}"></span>
                   
                    <div class="row">
                    <div class="col-md-4">
                        <h3 class="popover-title" id="content_type_title"></h3>
                    </div>
                    <div class="col-md-6"> </div>
                    <div class="col-md-2" style="float:right;">
                        <a href="javascript:void(0)" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5"> Preview
                        </a> 
                        @if(!empty($content_type_id))
                            <a href="{{url('admin/cms/content_data/add')}}/{{$content_type_id}}" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5" target="_blank" id="content_data_url"> Add
                            </a>  
                        @else
                            <a href="#" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5" id="content_data_url"> Add
                            </a> 
                        @endif
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-- end row --> 
        <span id="content_type_id" data-id="{{$content_type_id}}"></span>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->

                    <table id="content_data_datatable" class="table compact hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Title/Name</th>
                            <th>Created</th>
                            <th>Modified</th>                            
                            <th></th>                            
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- end row --> 
    </div>
</div>
@endsection     