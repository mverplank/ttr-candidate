@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Media</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Content
                        </li>                      
                        <li class="active">
                            Media Library
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->  
        <div class="row">           
            <div class="alert alert-success" role="alert" id="msg" style="display: none;">
            </div>
            <div class="card-box">
                <div class="form-group row page_top_btns"> 
                    <div class="col-md-3">
                        <div class="input-group">        
                            {{Form::text('data[Date][f][date]', $value = null, $attributes = array('class'=>'form-control required filterMediaByDate', 'placeholder'=>'mm/dd/yyyy', 'id'=>'filterMediaByDate'))}}
                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>  
                        </div>
                    </div>  
                    @if($type == "image")
                    <div class="col-md-3">
                        <div class="radio radio-purple radio-inline">
                            <select id ='allMediaDimension' name='allMediaDimension' class="form-control">
                                <option value="">Size</option>
                                <option value="728x90">728x90</option>
                                <option value="625x258">625x258</option>
                                <option value="500x200">500x200</option>
                                <option value="300x250">300x250</option>
                                <option value="170x300">170x300</option>
                                <option value="170x137">170x137</option>
                                <option value="120x300">120x300</option>
                                <option value="120x300">120x150</option>
                            </select>
                        </div> 
                    </div>
                    @endif
                    
                    <div style="float:right;">
                        @if($type != "image")
                            <a href="{{url('/admin/cms/media/add/image')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Images</a>
                        @endif
                        @if($type != "audio")
                            <a href="{{url('/admin/cms/media/add/audio')}}" id="hosts_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Audios</a>
                        @endif
                        @if($type != "video")
                            <a href="{{url('/admin/cms/media/add/video')}}" id="hosts_export" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">Add Videos</a>
                        @endif
                    </div> 
                </div>   
            </div>  
        </div>     
        @if($type == 'image')
        <div id="yes_crop"></div>
        @endif
        <!-- end row -->   
        <div class="row">
            <div class="col-xs-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <span style="display:none;" id="media_type" data-type="{{$type}}" data-total_media="{{$total_pages}}"></span>
                            <!-- <h4 class="header-title m-t-0">Example 1</h4>
                            <p class="text-muted font-13 m-b-30">
                                In this example we designed our own file input and used our own theme -
                                'dragdropbox'. We also added the file preview in our browser before
                                uploading the file.
                            </p> -->

                            <div class="p-20">
                                <div class="form-group clearfix">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="padding-left-0 padding-right-0">
                                                <div id="media_image" class="dropzone channel_image" data-limit="3" data-image-for="media" data-module="Media" data-preview-ele="#zapbox_prev" data-media_type="{{$type}}">
                                                    <div class="col-sm-9 previewDiv" id="zapbox_prev"></div>
                                                    <div class="col-sm-3 dz-message d-flex flex-column">
                                                       <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                                        Drag &amp; Drop here or click
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-12 padding-left-0 padding-right-0" id="media_section">
                                        <input type="file" name="files[]" id="filer_input1" multiple="multiple">
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->    
                    <div class="jFiler-items jFiler-row">
                        <ul class="jFiler-items-list jFiler-items-grid">
                            @foreach($all_media as $image)
                            <li class="jFiler-item" data-jfiler-index="1" data-id="{{$image['id']}}" style="">          
                                <div class="jFiler-item-container">                       
                                    <div class="jFiler-item-inner">                           
                                        <div class="jFiler-item-thumb">    
                                            <a href="{{$image['url']}}" target="_blank">
                                                <div class="jFiler-item-status"></div>
                                                <div class="jFiler-item-info">            
                                                    <span class="jFiler-item-title">
                                                        <b title="2.jpg">{{$image['name']}}</b>
                                                    </span>
                                                    @if(isset($image['size']))
                                                    <span class="jFiler-item-others">{{$image['size']}} KB</span> 
                                                    @endif
                                                </div>                                        
                                                <div class="jFiler-item-thumb-image {{$image['class']}}">
                                                    <img src="{{$image['src']}}" draggable="false">
                                                </div> 
                                            </a>  
                                        </div>                                    
                                        <div class="jFiler-item-assets jFiler-row">      
                                            <ul class="list-inline pull-left">         
                                                <li>
                                                    <span class="jFiler-item-others">
                                                        <i class="icon-jfi-file-image jfi-file-ext-jpg"></i>
                                                    </span>
                                                </li>             
                                            </ul>                                        
                                            <ul class="list-inline pull-right">           
                                                <li>
                                                    <a class="icon-jfi-edit color_icon" onclick="editMedia(&quot;{{$image['id']}}&quot;)"><i class="ion-edit"></i></a>
                                                </li>
                                                <li>
                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon" onclick="deleteMedia({{$image['id']}}, this)"></a>
                                                </li>                           
                                            </ul>                                    
                                        </div>                                
                                    </div>                            
                                </div>                        
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- end row -->

                    <div id="media_append_section" style="max-height: none !important;">
                        <div class="jFiler-items jFiler-row">
                            <ul class="jFiler-items-list jFiler-items-grid">
                            </ul>
                        </div>
                        <div id="media_loading_image_gif" style="background-color: rgb(255, 255, 255); display: none;">
                            <center><img id="loading-image" src="{{url('/')}}/storage/app/public/media/default/loader1.gif" alt="Loading..."></center>
                        </div>
                    </div>
                    <!-- end row -->
                    
                </div>
            </div><!-- end col-->
        </div>
        <!-- end row -->        
    </div> <!-- container -->
</div> <!-- content -->
<!-- sample modal content -->
<button class="btn btn-primary waves-effect waves-light" id="edit_media_modal" data-toggle="modal" data-target="#myModal"></button>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Edit Media</h4>
            </div>     
            {{ Form::open(array('url' => 'admin/cms/media/edit', 'id' => 'MediaAdminEditForm')) }}
            <div class="modal-body">               
                <h4></h4>
                <div class="p-20">
                    <div class="form-group row">
                        {{Form::label('MediaTitle', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                            {{Form::text('data[Media][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaTitle'))}}                            
                        </div>
                    </div>
                    <div class="form-group row">
                        {{Form::label('MediaAlt', 'Alt', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                            {{Form::text('data[Media][alt]', $value = null, $attributes = array('class'=>'form-control',  'id'=>'MediaAlt'))}}                            
                        </div>
                    </div>
                    <div class="form-group row">
                        {{Form::label('MediaCaption', 'Caption', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                            {{Form::text('data[Media][caption]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaCaption'))}}                            
                        </div>
                    </div>
                    <div class="form-group row">
                        {{Form::label('MediaDescription', 'Descritpion', array('class' => 'col-sm-3 form-control-label'))}}
                        <div class="col-sm-8">
                            {{Form::text('data[Media][description]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaDescription'))}}                            
                        </div>
                    </div>
                </div>               
                {{Form::hidden('data[Media][id]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaId'))}}     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="button" id="edit_media_btn" class="btn btn-primary waves-effect waves-light">Save changes</button>
            </div>
            {{ Form::close() }}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
