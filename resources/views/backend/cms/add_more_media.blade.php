@if(!empty($media))
        @foreach($media as $image)
        <li class="jFiler-item" data-jfiler-index="1" data-id="{{$image['id']}}" style="">
            <div class="jFiler-item-container">                       
                <div class="jFiler-item-inner">                           
                    <div class="jFiler-item-thumb">    
                        <a onclick="editMedia(&quot;{{$image['id']}}&quot;)">
                            <div class="jFiler-item-status"></div>
                            <div class="jFiler-item-info">            
                                <span class="jFiler-item-title">
                                    <b title="2.jpg">{{$image['name']}}</b>
                                </span>
                                @if(isset($image['size']))
                                <span class="jFiler-item-others">{{$image['size']}} KB</span> 
                                @endif
                            </div>                                        
                            <div class="jFiler-item-thumb-image {{$image['class']}}">
                                <img src="{{$image['src']}}" draggable="false">
                            </div> 
                        </a>  
                    </div>                                    
                    <div class="jFiler-item-assets jFiler-row">      
                        <ul class="list-inline pull-left">         
                            <li>
                                <span class="jFiler-item-others">
                                    <i class="icon-jfi-file-image jfi-file-ext-jpg"></i>
                                </span>
                            </li>             
                        </ul>                                        
                        <ul class="list-inline pull-right">           
                           <!--  <li>
                                <a class="icon-jfi-edit color_icon" onclick="editMedia(&quot;{{$image['id']}}&quot;)"><i class="ion-edit"></i></a>
                            </li> -->
                            <li>
                                <a class="icon-jfi-trash jFiler-item-trash-action color_icon" onclick="deleteMedia({{$image['id']}}, this)"></a>
                            </li>                           
                        </ul> 
                        <p>
                            <b>{{$image['title']}}</b>
                            {{$image['description']}}
                        </p>                                     
                    </div>                                
                </div>                            
            </div>                        
        </li>
        @endforeach
<!-- end row -->
@endif