@extends('backend.layouts.main')
@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Themes</h4>
                        <ol class="breadcrumb p-0 m-0">                        
                            <li>
                                <a href="{{url('admin')}}">Dashboard</a>
                            </li>
                            <li class="">
                                Content
                            </li>
                            <li class="active">
                                Themes
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif
            </div>
            <!-- end row --> 
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-6">Themes</div>
                            <div class="col-md-2" style="float:right;">
                                <a href="{{url('/admin/cms/themes/add')}}" class="btn btn-primary btn-rounded waves-effect waves-light m-b-5"> Add
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row --> 
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->
                        <table id="cms_themes_datatable" class="table compact hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Theme name</th>
                                    <th>Assigned to</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
@endsection