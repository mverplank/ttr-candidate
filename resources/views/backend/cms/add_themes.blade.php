@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add Themes</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="">
                     Content
                  </li>
                  <li class="">
                     <a href="{{route('get_cms_pages')}}">Pages</a>
                  </li>
                  <li class="active">
                     Add
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/themes/add', 'id'=>'CmsThemeAdminAddForm')) }}
            <div class="form_start">
                <div class="row">
                    <div class="card-box">
                        <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Create new theme</h4>
                        <div class="form-group row">
                           {{Form::label('CustomThemeThemesId', 'Template', array('class' => 'col-sm-2 form-control-label'))}}
                           <div class="col-sm-10">
                              {{Form::select('data[CustomTheme][themes_id]',$themes, '', $attributes=array('id'=>'CustomThemeThemesId','class'=>'form-control','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'select'))}}
                              <span class="help-block"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                  Only letters, numbers and _ are allowed.
                              </span>
                           </div>
                        </div> 
                        <div class="form-group row">
                           {{Form::label('CustomThemeName', 'Name', array('class' => 'col-sm-2 form-control-label req'))}}
                           <div class="col-sm-10">
                              {{Form::text('data[CustomTheme][name]', $value = null, $attributes = array('class'=>'form-control required','id'=>'CustomThemeName','required'=>true))}}
                           </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[CustomTheme][btnAdd]" id="CustomThemeBtnSave" data-form-id="CmsThemeAdminAddForm">
                            Add
                        </button>          
                    </div>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection