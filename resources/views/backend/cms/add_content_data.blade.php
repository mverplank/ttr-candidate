@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">Add {{$contentdata->name}}</h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     <a href="{{url('admin')}}">Dashboard</a>
                  </li>
                  <li class="">
                     Content
                  </li>
                  <li class="">
                     <a href="{{route('content_data_index')}}">Manage Entries</a>
                  </li>
                  <li class="">
                     <a href=" ">Content Data</a>
                  </li>
                  <li class="active">
                     Add
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
         <!-- end row -->
        {{ Form::open(array('url' => 'admin/cms/content_data/add/'.$contentdata->id, 'id'=>'ContentDataAdminAddForm')) }}
            <div class="form_start">
                {{Form::hidden('data[ContentData][content_categories_id]',$contentdata->id, ['id'=>''])}}
                <div class="row">
                    <div class="card-box">
                        <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Content</h4>
                        @if($contentdata->label_title != '')
                            <div class="form-group row">
                                {{Form::label('ContentDataTitle', $contentdata->label_title, array('class' => 'col-sm-2 form-control-label'))}}  
                                <div class="col-md-10">
                                    {{Form::text('data[ContentData][title]', $value = null, $attributes =array('id'=>'ContentDataTitle', 'class'=>'form-control required form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-required' => '1'))}}
                                </div>
                            </div>
                        @endif

                           <div class="form-group row">
                                {{Form::label('ContentDataSubtitle','Sub-title', array('class' => 'col-sm-2 form-control-label'))}}
                                <div class="col-sm-10">
                                    {{ Form::textarea('data[ContentData][subtitle]', null, ['id' => 'ContentDataSubtitle', 'class'=>'form-control','rows'=>'2', 'cols'=>50]) }}
                                </div>
                            </div>
                            <div class="form-group row">
                               {{Form::label('showHasManyCategories', 'Category', array('class' => 'col-sm-2 form-control-label req'))}}
                               <?php //echo "<pre>";print_R(json_decode($itunes_categories));?>
                               <div class="col-sm-10">
                                    <div class="outside_itunes_cat" data-off="1">
                                        <div class="col-sm-6 row">
                                            {!! $all_categories !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                {{Form::label('ContentDataShortDescription','Short description', array('class' => 'col-sm-2 form-control-label'))}}
                                <div class="col-sm-10">
                                    {{ Form::textarea('data[ContentData][short_description]', null, ['id' => 'ContentDataShortDescription', 'class'=>'form-control','rows'=>4, 'cols'=>50]) }}
                                </div>
                            </div>

                            <!-- Editor -->
                            <div class="form-group row">
                                {{Form::label('ContentDataContent','Content', array('class' => 'col-sm-2 form-control-label'))}}
                                <div class="col-sm-10">
                                    <div class="card-box">
                                        <form method="post">
                                            {{ Form::textarea('data[ContentData][content]', null, ['id' => 'ContentDataContent']) }}
                                        </form>
                                    </div>
                                </div>
                            </div>

                        <div class="form-group row">
                            {{Form::label('ContentDataKeywords','Keywords', array('class' => 'col-sm-2 form-control-label'))}}  
                            <div class="col-md-10">
                                {{Form::text('data[ContentData][keywords]', $value = null, $attributes =array('id'=>'ContentDataKeywords', 'class'=>'form-control form_ui_input','data-maxlength' => '255','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text','data-role'=>'tagsinput'))}}
                            </div>
                        </div>
                            <div class="form-group row">
                                {{Form::label('ContentDataUrl','Url', array('class' => 'col-sm-2 form-control-label'))}}  
                                <div class="col-md-10">
                                    {{Form::text('data[ContentData][url]', $value = null, $attributes =array('id'=>'ContentDataUrl', 'class'=>'form-control'))}}
                                </div>
                            </div>

                            <div class="form-group row">
                                {{Form::label('ContentDataExternalUrl','External Url', array('class' => 'col-sm-2 form-control-label'))}}  
                                <div class="col-md-10">
                                    {{Form::url('data[ContentData][external_url]', $value = null, $attributes =array('id'=>'ContentDataExternalUrl', 'class'=>'form-control'))}}
                                </div>
                            </div>

                        <div class="form-group row">
                            {{Form::label('ContentDataDate','Date', array('class' => 'col-sm-2 form-control-label'))}}  
                            <div class="col-md-10">
                                {{Form::text('data[ContentData][date]',$value = null, $attributes =array('id'=>'ContentDataDate', 'class'=>'form-control form_ui_input ContentDataDate','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text'))}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('ContentDataTime','Time', array('class' => 'col-sm-2 form-control-label'))}}  
                            <div class="col-md-10">
                                {{Form::text('data[ContentData][time]','', $attributes =array('id'=>'ContentDataTime', 'class'=>'form-control form_ui_input timepicker','data-forma' => '1','data-forma-def'=> '1','data-type' => 'text'))}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('ContentDataIsFeatured', 'Featured', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-9">
                                {{ Form::checkbox('data[ContentData][is_featured]', '1',true, ['id' => 'switch35', 'data-switch' => 'bool']) }}
                                <label for="switch35" data-on-label="Yes"data-off-label="No"  data-size="large"></label class="checkbox_button">
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="card-box">
                        <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Images</h4>
                            <div class="form-group row">
                                {{Form::label('ContentCategoryParentId', $contentdata->label_images, array('class' => 'col-sm-2 form-control-label'))}}
                                <div class="col-sm-8">
                                    <div class="row">
                                        @if($media->count() > 0)
                                            @php ($url_count = 1)
                                            @foreach($media as $med)
                                                @if($med->sub_module == 'images')
                                                    <div class="col-sm-4 adding-medias media_ContentData_images" data-off="{{$url_count}}">
                                                        <div class="jFiler-items jFiler-row">
                                                            <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                    <div class="jFiler-item-container">                       
                                                                        <div class="jFiler-item-inner">                           
                                                                            <div class="jFiler-item-thumb">    
                                                                                <a href="javascript:void(0)" onclick="editMediaImage(this, 'ContentData', 'images', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                    <div class="jFiler-item-thumb-image">
                                                                                        <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                    </div> 
                                                                                </a>  
                                                                            </div>                                    
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                            <ul class="list-inline pull-right">
                                                                                <li>
                                                                                    <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_ContentData_images" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="ContentData" data-submodule="images" ></a>
                                                                                </li>                           
                                                                            </ul>                                    
                                                                            </div>                                
                                                                        </div>                            
                                                                    </div>                        
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    @php ($url_count++)
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="media_ContentData_images_outer hidden" data-off="0" style="display: none;">
                                            </div>
                                    </div>
                                    <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        <span style="color:#f9c851;">Only jpg/jpeg/png/gif files. Maximum 100 files. Maximum file size: 120MB. Clik on image to edit.</span>
                                    </span>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="mtf-buttons" style="clear:both;float:right;">
                                        <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="ContentData" data-submodule="images" data-media_type="image" data-limit="100" data-dimension="">Upload files</button>
                                    </div> 
                                </div>
                            </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="card-box">
                        <h4 class="header-title m-t-0" style="background-color: grey; width:100%; padding:10px 10px 10px 10px; color:#FFFFFF;">Publish</h4>
                        <div class="form-group row">
                            {{Form::label('ContentDataIsOnline', 'Status', array('class' => 'col-sm-3 form-control-label'))}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                {{ Form::checkbox('data[ContentData][is_online]', '1', true, ['id' => 'switch34', 'data-switch' => 'bool']) }}
                                    <label for="switch34" data-on-label="Online"data-off-label="Offline"  data-size="large"></label class="checkbox_button">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                {!! Form::label('ContentDataPublish','Publish date') !!}
                            </div>                
                            <div class="col-sm-5">
                                {{Form::text('data[ContentData][publish_date]', '',  array('class'=>'form-control ContentDataDate','id'=>'ContentDatapublishdate','data-forma'=>1,'data-forma-def'=>1))}}
                            </div>
                            <div class="col-sm-5">
                                {{Form::text('data[ContentData][publish_time]', '',  array('class'=>'form-control timepicker','id'=>'ContentDatapublishtime','data-forma'=>1,'data-forma-def'=>1))}}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="fixed_btn">
                        <button type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[ContentCategory][btnAdd]" id="ContentDataBtnAdd" data-form-id="ContentDataAdminAddForm">
                            Add
                        </button>          
                    </div>
                </div>
            </div>
        {{ Form::close() }}      
         @include('backend.cms.upload_media_popup',['module_id' => 0,'main_module' => "ContentData"]) 
    </div>
</div>
@endsection