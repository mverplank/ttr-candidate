@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Content Categories</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Content
                        </li>
                        <li class="active">
                            <a href="{{route('content_data_index')}}">Manage Entries</a>
                        </li>                      
                        <li class="active">
                            Content Categories
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->        
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="panel panel-color panel-warning">
                    <div class="panel-heading">
                        <h2 class="panel-title">Content Categories</h2>
                        <!-- <p class="panel-sub-title font-13">To create, rename or delete element use right mouse button. Use drag and drop to re-order tree structure.</p> -->
                    </div>
                    <div class="panel-body">
                        <div class="content_category">
                            <input type="hidden" id="CategoryShows" name="data[Category][shows]" data-forma="1" data-forma-def="1" data-type="jstree">
                            {!! $generate_tree !!}               
                        </div>
                    </div>
                    <div class="panel-footer">
                        <i class="ion-android-information"></i>  To create, rename or delete element use right mouse button. Use drag and drop to re-order tree structure.
                    </div>
                </div>
            </div>
           
        </div> <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
