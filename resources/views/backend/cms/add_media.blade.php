@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Media</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Content
                        </li>                      
                        <li class="active">
                            Media Library
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->  
        <div class="row">           
            <div class="alert alert-success" role="alert" id="msg" style="display: none;">
            </div>
            <div class="card-box">
                <div class="form-group row page_top_btns"> 
                    <!-- Filter by date -->
                    <div class="col-md-3">
                        <div class="input-group">        
                            {{Form::text('data[Date][f][date]', $value = null, $attributes = array('class'=>'form-control required filterMediaByDate', 'placeholder'=>'mm/dd/yyyy', 'id'=>'filterMediaByDate'))}}
                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>  
                        </div>
                    </div>  
                    <!-- Filter by title -->
                    <div class="col-md-3">
                        <div class="form-group">                            
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-times" id="remove_search"></i></button>
                                </span>
                                <input type="text" id="valueMediaByTitle" name="data[Media][f][title]" class="form-control" placeholder="Search  by Title">                                
                                <span class="input-group-btn">
                                    <button type="button" class="btn waves-effect waves-light btn-primary" id="filterMediaByTitle"><i class="fa fa-search"></i></button>
                                </span>
                                
                            </div>
                        </div>
                    </div>  
                    <!-- Filter by size -->
                    @if($type == "image")
                    <div class="col-md-2">
                        <div class="radio radio-purple radio-inline">
                            <select id ='allMediaDimension' name='allMediaDimension' class="form-control">
                                <option value="">Size</option>
                                <option value="728x90">728x90</option>
                                <option value="625x258">625x258</option>
                                <option value="500x200">500x200</option>
                                <option value="300x250">300x250</option>
                                <option value="170x300">170x300</option>
                                <option value="170x137">170x137</option>
                                <option value="120x300">120x300</option>
                                <option value="120x300">120x150</option>
                            </select>
                        </div> 
                    </div>
                    @endif
                  
                </div>   
            </div>  
        </div>     
        <!-- Active div -->
        @if($type == "image")
            @php
                $image_active = "active";
                $audio_active = "";
                $video_active = "";
            @endphp
        @elseif($type == "audio")
            @php
                $image_active = "";
                $audio_active = "active";
                $video_active = "";
            @endphp
        @elseif($type == "video")
            @php
                $image_active = "";
                $audio_active = "";
                $video_active = "active";
            @endphp
        @endif

        @if($type == 'image')
            <div id="yes_crop"></div>
        @endif
        <span style="display:none;" id="media_type" data-type="{{$type}}" data-total_media="{{$total_pages}}"></span>

        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <ul class="nav nav-tabs">
                        <li class="{{$image_active}}">
                            <a href="#image" data-toggle="tab" aria-expanded="false" onclick="fetchMedia('image')">
                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                <span class="hidden-xs">Images</span>
                            </a>
                        </li> 
                        <li class="{{$audio_active}}">
                            <a href="#audio" data-toggle="tab" aria-expanded="true" onclick="fetchMedia('audio')">
                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                <span class="hidden-xs">Audios</span>
                            </a>
                        </li>
                        <li class="{{$video_active}}">
                            <a href="#video" data-toggle="tab" aria-expanded="false" onclick="fetchMedia('video')">
                                <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                <span class="hidden-xs">Videos</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <!-- Images -->
                        <div class="tab-pane {{$image_active}}" id="image">
                            <div class="p-20">
                                <div class="form-group clearfix">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="padding-left-0 padding-right-0">
                                                <div id="media_image_image" class="channel_image {{$type == 'image' ? 'dropzone': '' }}" data-limit="3" data-image-for="media" data-module="Media" data-preview-ele="#zapbox_prev" data-media_type="image">
                                                    <div class="col-sm-9 previewDiv" id="zapbox_prev"></div>
                                                    <div class="col-sm-3 dz-message d-flex flex-column">
                                                       <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                                        Drag &amp; Drop here or click
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="jFiler-items jFiler-row">
                                <div class="set-thumb">
                                    <span title="Grid View" class="thumbs list-active"><i class="glyphicon glyphicon-th-large"></i></span>
                                    <span title="List View" class="list-thumb"><i class="glyphicon glyphicon-th-list"></i></span>
                                </div>
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($all_media as $image)
                                    <li class="jFiler-item" data-jfiler-index="1" data-id="{{$image['id']}}" style="">          
                                        <div class="jFiler-item-container">                       
                                            <div class="jFiler-item-inner">                           
                                                <div class="jFiler-item-thumb">    
                                                    <a  onclick="editMedia({{$image['id']}})">
                                                        <div class="jFiler-item-status"></div>
                                                        <div class="jFiler-item-info">            
                                                            <span class="jFiler-item-title">
                                                                <b title="2.jpg">{{$image['name']}}</b>
                                                            </span>
                                                            @if(isset($image['size']))
                                                            <span class="jFiler-item-others">{{$image['size']}} KB</span> 
                                                            @endif
                                                        </div>                                        
                                                        <div class="jFiler-item-thumb-image {{$image['class']}}">
                                                            <img src="{{$image['src']}}" draggable="false">
                                                        </div> 
                                                    </a>  
                                                </div>                                    
                                                <div class="jFiler-item-assets jFiler-row">      
                                                    <ul class="list-inline pull-left">         
                                                        <li>
                                                            <span class="jFiler-item-others">
                                                                <i class="icon-jfi-file-image jfi-file-ext-jpg"></i>
                                                            </span>
                                                        </li>             
                                                    </ul>                                        
                                                    <ul class="list-inline pull-right">           
                                                        <!-- <li>
                                                            <a class="icon-jfi-edit color_icon" onclick="editMedia(&quot;{{$image['id']}}&quot;)"><i class="ion-edit"></i></a>
                                                        </li> -->
                                                        <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon" onclick="deleteMedia({{$image['id']}}, this)"></a>
                                                        </li>                           
                                                    </ul>    
                                                    <p>
                                                        <b>{{$image['title']}}</b>
                                                        {{$image['description']}}
                                                    </p>                                
                                                </div>                                
                                            </div>                            
                                        </div>                        
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- end row -->

                            <div id="media_append_section" style="max-height: none !important;">
                              
                                <div id="media_loading_image_gif" style="background-color: rgb(255, 255, 255); display: none;">
                                    <center><img id="loading-image" src="{{url('/')}}/storage/app/public/media/default/loader1.gif" alt="Loading..."></center>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- Audios -->
                        <div class="tab-pane {{$audio_active}}" id="audio">
                            <div class="p-20">
                                <div class="form-group clearfix">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="padding-left-0 padding-right-0">
                                                <div id="media_image_audio" class="channel_image {{$type == 'audio' ? 'dropzone': '' }}" data-limit="3" data-image-for="media" data-module="Media" data-preview-ele="#zapbox_prev1" data-media_type="audio">
                                                    <div class="col-sm-9 previewDiv" id="zapbox_prev1"></div>
                                                    <div class="col-sm-3 dz-message d-flex flex-column">
                                                       <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                                        Drag &amp; Drop here or click
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="jFiler-items jFiler-row">
                                <div class="set-thumb">
                                    <span title="Grid View" class="thumbs list-active"><i class="glyphicon glyphicon-th-large"></i></span>
                                    <span title="List View" class="list-thumb"><i class="glyphicon glyphicon-th-list"></i></span>
                                </div>
                                <ul class="jFiler-items-list jFiler-items-grid">
                                   
                                    @foreach($all_media as $image)
                                    <li class="jFiler-item" data-jfiler-index="1" data-id="{{$image['id']}}" style="">          
                                        <div class="jFiler-item-container">                       
                                            <div class="jFiler-item-inner">                           
                                                <div class="jFiler-item-thumb">    
                                                    <a  onclick="editMedia(&quot;{{$image['id']}}&quot;)">
                                                        <div class="jFiler-item-status"></div>
                                                        <div class="jFiler-item-info">            
                                                            <span class="jFiler-item-title">
                                                                <b title="2.jpg">{{$image['name']}}</b>
                                                            </span>
                                                            @if(isset($image['size']))
                                                            <span class="jFiler-item-others">{{$image['size']}} KB</span> 
                                                            @endif
                                                        </div>                                        
                                                        <div class="jFiler-item-thumb-image {{$image['class']}}">
                                                            <img src="{{$image['src']}}" draggable="false">
                                                        </div> 
                                                    </a>  
                                                </div>                                    
                                                <div class="jFiler-item-assets jFiler-row">      
                                                    <ul class="list-inline pull-left">         
                                                        <li>
                                                            <span class="jFiler-item-others">
                                                                <i class="icon-jfi-file-image jfi-file-ext-jpg"></i>
                                                            </span>
                                                        </li>             
                                                    </ul>                                        
                                                    <ul class="list-inline pull-right">           
                                                        <!-- <li>
                                                            <a class="icon-jfi-edit color_icon" onclick="editMedia(&quot;{{$image['id']}}&quot;)"><i class="ion-edit"></i></a>
                                                        </li> -->
                                                        <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon" onclick="deleteMedia({{$image['id']}}, this)"></a>
                                                        </li>                           
                                                    </ul> 
                                                    <p>
                                                        <b>{{$image['title']}}</b>
                                                        {{$image['description']}}
                                                    </p>                                    
                                                </div>                                
                                            </div>                            
                                        </div>                        
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- end row -->

                            <div id="media_append_section" style="max-height: none !important;">
                                
                                <div id="media_loading_image_gif" style="background-color: rgb(255, 255, 255); display: none;">
                                    <center><img id="loading-image" src="{{url('/')}}/storage/app/public/media/default/loader1.gif" alt="Loading..."></center>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>

                        <!-- Videos -->
                        <div class="tab-pane {{$video_active}}" id="video">
                         
                            <div class="p-20">
                                <div class="form-group clearfix">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="padding-left-0 padding-right-0">
                                                <div id="media_image_video" class="channel_image {{$type == 'video' ? 'dropzone': '' }}" data-limit="3" data-image-for="media" data-module="Media" data-preview-ele="#zapbox_prev2" data-media_type="video">
                                                    <div class="col-sm-9 previewDiv" id="zapbox_prev2"></div>
                                                    <div class="col-sm-3 dz-message d-flex flex-column">
                                                       <i class="fa fa-cloud-upload upload_icon" aria-hidden="true"></i>
                                                        Drag &amp; Drop here or click
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="jFiler-items jFiler-row">
                                <div class="set-thumb">
                                    <span title="Grid View" class="thumbs list-active"><i class="glyphicon glyphicon-th-large"></i></span>
                                    <span title="List View" class="list-thumb"><i class="glyphicon glyphicon-th-list"></i></span>
                                </div>
                                <ul class="jFiler-items-list jFiler-items-grid">
                                  
                                    @foreach($all_media as $image)
                                    <li class="jFiler-item" data-jfiler-index="1" data-id="{{$image['id']}}" style="">          
                                        <div class="jFiler-item-container">                       
                                            <div class="jFiler-item-inner">                           
                                                <div class="jFiler-item-thumb">    
                                                    <a  onclick="editMedia(&quot;{{$image['id']}}&quot;)">
                                                        <div class="jFiler-item-status"></div>
                                                        <div class="jFiler-item-info">            
                                                            <span class="jFiler-item-title">
                                                                <b title="2.jpg">{{$image['name']}}</b>
                                                            </span>
                                                            @if(isset($image['size']))
                                                            <span class="jFiler-item-others">{{$image['size']}} KB</span> 
                                                            @endif
                                                        </div>                                        
                                                        <div class="jFiler-item-thumb-image {{$image['class']}}">
                                                            <img src="{{$image['src']}}" draggable="false">
                                                        </div> 
                                                    </a>  
                                                </div>                                    
                                                <div class="jFiler-item-assets jFiler-row">      
                                                    <ul class="list-inline pull-left">         
                                                        <li>
                                                            <span class="jFiler-item-others">
                                                                <i class="icon-jfi-file-image jfi-file-ext-jpg"></i>
                                                            </span>
                                                        </li>             
                                                    </ul>                                        
                                                    <ul class="list-inline pull-right">           
                                                        <!-- <li>
                                                            <a class="icon-jfi-edit color_icon" onclick="editMedia(&quot;{{$image['id']}}&quot;)"><i class="ion-edit"></i></a>
                                                        </li> -->
                                                        <li>
                                                            <a class="icon-jfi-trash jFiler-item-trash-action color_icon" onclick="deleteMedia({{$image['id']}}, this)"></a>
                                                        </li>                           
                                                    </ul> 
                                                    <p>
                                                        <b>{{$image['title']}}</b>
                                                        {{$image['description']}}
                                                    </p>                                    
                                                </div>                                
                                            </div>                            
                                        </div>                        
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- end row -->   
                            <div id="media_append_section" style="max-height: none !important;">
                              
                                <div id="media_loading_image_gif" style="background-color: rgb(255, 255, 255); display: none;">
                                    <center><img id="loading-image" src="{{url('/')}}/storage/app/public/media/default/loader1.gif" alt="Loading..."></center>
                                </div>
                            </div>
                            <!-- end row -->                        
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->             
    </div> <!-- container -->
</div> <!-- content -->
<!-- sample modal content -->
<button class="btn btn-primary waves-effect waves-light" id="edit_media_modal" data-toggle="modal" data-target="#myModal"></button>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog dialog-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Edit Media &nbsp;&nbsp;:<span class="Media_Title_"></span></h4>
            </div>     
            {{ Form::open(array('url' => 'admin/cms/media/edit', 'id' => 'MediaAdminEditForm')) }}
            <div class="modal-body">               
                <h4></h4>
                <div class="row center-media-image">
                    <div class="col-lg-8">
                        <div class="media-size Media_Perview_Div" >
                            
                        </div>
                        <div class="Edit_Media_Image">
                             
                        </div>
                       
                    </div>
                    <div class="col-lg-4" style="border-left:1px solid #e4e3e3;">
                        <div class="p-20 form-group">

                          <!--  <label for"fileUrl">File URL:</label>
                           <input class="form-control" type="text" name="" id="fileUrl" value=""> -->
                           {{Form::hidden('MediaFileName', '', ['id'=>'mediaFile'])}}

                           <div class="mediaInfo">
                           </div>
                        </div>
                        <div class="p-20">
                            <div class="form-group row">
                                {{Form::label('MediaTitle', 'Title', array('class' => 'col-sm-3 form-control-label'))}}
                                <div class="col-sm-8">
                                    {{Form::text('data[Media][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaTitle'))}}                            
                                </div>
                            </div>
                            <div class="form-group row">
                                {{Form::label('MediaAlt', 'Alt', array('class' => 'col-sm-3 form-control-label'))}}
                                <div class="col-sm-8">
                                    {{Form::text('data[Media][alt]', $value = null, $attributes = array('class'=>'form-control',  'id'=>'MediaAlt'))}}                            
                                </div>
                            </div>
                            <div class="form-group row">
                                {{Form::label('MediaCaption', 'Caption', array('class' => 'col-sm-3 form-control-label'))}}
                                <div class="col-sm-8">
                                    {{Form::text('data[Media][caption]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaCaption'))}}                            
                                </div>
                            </div>
                            <div class="form-group row">
                                {{Form::label('MediaDescription', 'Descritpion', array('class' => 'col-sm-3 form-control-label'))}}
                                <div class="col-sm-8">
                                    {{Form::text('data[Media][description]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaDescription'))}}                            
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                              
                {{Form::hidden('data[Media][id]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'MediaId'))}}     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="button" id="edit_media_btn" class="btn btn-primary waves-effect waves-light">Save changes</button>
            </div>
            {{ Form::close() }}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
