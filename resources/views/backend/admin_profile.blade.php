@extends('backend.layouts.main')
@section('content')
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <div class="page-title-box">
               <h4 class="page-title">
                  @if(Config::get('constants.HOST') == getCurrentUserType())
                  My Profile
                  @else    
                  Edit User
                  @endif
               </h4>
               <ol class="breadcrumb p-0 m-0">
                  <li>
                     @if(Config::get('constants.HOST') == getCurrentUserType())
                     <a href="{{url('host')}}">Dashboard</a>
                     @elseif(Config::get('constants.PARTNER') == getCurrentUserType())
                     <a href="{{url('partner')}}">Dashboard</a>
                     @else
                     <a href="{{url('admin')}}">Dashboard</a>
                     @endif
                  </li>
                  @if(Config::get('constants.ADMIN') == getCurrentUserType())
                  <li class="active">
                     Users
                  </li>
                  <li class="active">
                     <a href="{{url('admin/user/users')}}">All Users</a>
                  </li>
                  @endif
                  <li class="active">
                     @if(Config::get('constants.HOST') == getCurrentUserType())
                     My Profile
                     @else
                     Edit User
                     @endif
                  </li>
               </ol>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <!-- end row -->
      <div id="yes_crop"></div>
      <div class="row">
         <!-- Form Starts-->
         {{ Form::open(array('url' => 'admin/user/users/profile/', 'method' => 'PUT', 'id'=>'UserAdminProfileForm')) }}
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-12">
                  <div class="card-box">
                     <div class="row">
                        <div class="col-sm-12 col-xs-12 col-md-12">
                           {{Form::hidden('form_type', 'edit', array('id'=>'form_type'))}}
                            @if(Config::get('constants.HOST') == getCurrentUserType())
                                <div class="info_title">
                                    <div class="col-md-10">
                                       <h4 class="header-title m-t-0">Personal Details</h4>
                                    </div>
                                    <div class="col-md-2"> 
                                        
                                        <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'Personal Details', this)" title="Section Info" id="sa-title">What is this?</a>

                                    </div>                       
                                </div>
                            @else
                                <h4 class="header-title m-t-0">Personal Details</h4>
                            @endif    
                            <div class="p-20">
                                <div class="form-group row">
                                     {{Form::label('name', 'Name', array('class' => 'col-sm-2 form-control-label req'))}}
                                 <div class="col-sm-9">
                                    <div class="form-group col-sm-3">
                                       <?php 
                                          $prefixes = array('' => 'Choose prefix..', 'Dr.' => 'Dr.', 'Prof' => 'Prof', 'Pir' => 'Pir', 'Colonel' => 'Colonel');
                                          ?>
                                       {{Form::select('data[Profile][title]', $prefixes, (isset($user->profiles[0]->title)) ? $user->profiles[0]->title : '', $attributes=array('id'=>'ProfileTitle', 'class'=>'form-control'))}}
                                    </div>
                                    {{Form::hidden('data[Profile][id]', (isset($user->profiles[0]->id))  ? $user->profiles[0]->id : '')}}
                                    {{Form::hidden('data[UserSetting][id]',($user->user_settings->count() > 0) ? $user->user_settings[0]->id : '')}}
                                    <div class="col-sm-3">
                                       {{Form::text('data[Profile][firstname]', (isset($user->profiles[0]->firstname))  ? $user->profiles[0]->firstname : '', $attributes = array('class'=>'form-control required', 'placeholder'=>'First name', 'data-parsley-maxlength'=>'255','id'=>'ProfileFirstname', 'required'=>true))}}
                                    </div>
                                    <div class="col-sm-3">
                                       {{Form::text('data[Profile][lastname]', (isset($user->profiles[0]->lastname))  ? $user->profiles[0]->lastname : '', $attributes = array('class'=>'form-control required', 'placeholder'=>'Last name', 'data-parsley-maxlength'=>'255','id'=>'ProfileLastname', 'required'=>true))}}
                                    </div>
                                    <div class="form-group col-sm-3">
                                       {{Form::select('data[Profile][sufix]', array('' => 'Choose suffix..', 'Np' => 'Np', 'Jd' => 'Jd', 'M.s.' => 'M.s.'), (isset($user->profiles[0]->sufix))  ? $user->profiles[0]->sufix : '', $attributes=array('id'=>'ProfileSufix', 'class'=>'form-control'))}}
                                    </div>
                                 </div>
                              </div>
                              @if(Config::get('constants.HOST') == getCurrentUserType())
                              <!-- Editor for Bio-->
                              <div class="form-group row">
                                 {{Form::label('HostBio', 'Bio', array('class' => 'col-sm-4 form-control-label'))}}
                                 <div class="col-sm-7">
                                    <div class="card-box">
                                       {{Form::hidden('data[Host][id]', $user->hosts->count() > 0 ? $user->hosts[0]->id : '', ['id' => 'HostId'])}}
                                       <form method="post">
                                          {{ Form::textarea('data[Host][bio]',  $user->hosts->count() > 0 ? $user->hosts[0]->bio : '', ['id' => 'HostBio']) }}
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <!-- End row -->
                              <!-- End editor -->
                              <div class="form-group row">
                                 {{Form::label('Host 0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                 <div class="col-sm-8">
                                    {{Form::hidden('edit_id', $user->hosts->count() > 0 ? $user->hosts[0]->id : 0, array('id'=>'edit_id'))}}
                                    <div class="row">
                                       @if($media->count() > 0)
                                       @php ($url_count = 1)
                                       @foreach($media as $med)
                                       @if($med->sub_module == 'photo')
                                       <div class="col-sm-4 adding-medias media_Host_photo" data-off="{{$url_count}}">
                                          <div class="jFiler-items jFiler-row">
                                             <ul class="jFiler-items-list jFiler-items-grid">
                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                   <div class="jFiler-item-container">                       
                                                      <div class="jFiler-item-inner">                           
                                                         <div class="jFiler-item-thumb">    
                                                            <a href="javascript:void(0)" onclick="editMediaImage(this, 'Host', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                               <div class="jFiler-item-thumb-image">
                                                                  <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                               </div> 
                                                            </a>  
                                                         </div>                                    
                                                         <div class="jFiler-item-assets jFiler-row">
                                                         <ul class="list-inline pull-right">
                                                            <li>
                                                               <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_photo" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="photo"></a>
                                                            </li>                           
                                                         </ul>                                    
                                                         </div>                                
                                                      </div>                            
                                                   </div>                        
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       @php ($url_count++)
                                       @endif
                                       @endforeach
                                       @endif
                                       <div class="media_Host_photo_outer hidden" data-off="0" style="display: none;">
                                       </div>
                                    </div>
                                    <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    Only jpg/jpeg/png/gif files.<span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span>Clik on image to edit.
                                 </div>
                                 <div class="col-sm-2">
                                    <div class="mtf-buttons" style="clear:both;float:right;">
                                       <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="photo" data-media_type="image" data-limit="1" data-dimension="" data-module_id="{{$id}}" data-file_crop="1">Upload files</button>
                                    </div>
                                 </div>
                              </div>
                              <?php //echo "<pre>";print_R($user->hosts);die;?>
                              <div class="form-group row">
                                 {{Form::label('HostHasManyUrls', 'Urls and Social Media', array('class' => 'col-sm-4 form-control-label'))}}
                                 <div class="col-sm-7">
                                    <div class="mtf-dg">
                                       <table class="mtf-dg-table url_table default">
                                          <thead>
                                             <tr>
                                                <th>
                                                   <div>Type</div>
                                                </th>
                                                <th>
                                                   <div>Url</div>
                                                </th>
                                                <th></th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @if($user->hosts->count() > 0) 
                                             @if($user->hosts[0]->urls->count() > 0)
                                             @php ($url_count = 1)
                                             @foreach($user->hosts[0]->urls as $url)
                                             <tr data-id="" data-off="{{$url_count}}" style="display: table-row;" data-rnd="">
                                                <td>
                                                   {{Form::hidden('data[Url]['.$url_count.'][id]', $url->id)}}
                                                   {{Form::select('data[Url]['.$url_count.'][type]', $social_media, $url->type, $attributes=array('id'=>'Url'.$url_count.'Type', 'class'=>'form-control SelectSocialMediaType'))}}
                                                </td>
                                                <td>
                                                   {{Form::url('data[Url]['.$url_count.'][url]', $url->url,  array('step' => '1', 'min' => '1', 'id' => 'Url'.$url_count.'Url','class'=>'form-control SelectSocialMediaURL', 'placeholder'=>'http://www.google.com' ))}}
                                                </td>
                                                <td class="actions">
                                                   <div class="col-sm-6 col-md-4 col-lg-3 col-sm-6 col-md-4 col-lg-3 delete_host_extra_urls" style="display: block;">
                                                      <i class="typcn typcn-delete"></i> 
                                                   </div>
                                                </td>
                                             </tr>
                                             @php ($url_count++)
                                             @endforeach
                                             @else
                                             <tr data-off="1" style="display: table-row;" data-rnd="">
                                                <td>
                                                   {{Form::select('data[Url][1][type]', $social_media, '', $attributes=array('id'=>'Url1Type', 'class'=>'form-control'))}}
                                                </td>
                                                <td>
                                                   {{Form::url('data[Url][1][url]', '',  array('step' => '1', 'min' => '1', 'id' => 'Url1Url','class'=>'form-control', 'placeholder'=>'http://www.google.com' ))}}
                                                </td>
                                                <td class="actions">
                                                   <div class="col-sm-6 col-md-4 col-lg-3 host_extra_urls" style="display: block;">
                                                      <i class="typcn typcn-delete"></i> 
                                                   </div>
                                                </td>
                                             </tr>
                                             @endif
                                             @endif
                                          </tbody>
                                       </table>
                                       <div class="mtf-buttons" style="clear:both;float:right;">
                                          <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyUrlsAdd"><i class="glyphicon glyphicon-plus"></i>
                                          <span>add</span>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- End row -->
                              <div class="form-group row">
                                 {{Form::label('HostHasManyVideos', 'Video Clips', array('class' => 'col-sm-4 form-control-label'))}}
                                 <div class="col-sm-7">
                                    <div class="mtf-dg">
                                       <table class="mtf-dg-table video_table default">
                                          <thead>
                                             <tr>
                                                <th>
                                                   <div>Title</div>
                                                </th>
                                                <th>
                                                   <div>Video HTML code</div>
                                                </th>
                                                <th>
                                                   <div>Enabled</div>
                                                </th>
                                                <th></th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @if($user->hosts->count() > 0)
                                             @if($user->hosts[0]->videos->count() > 0)
                                             @php ($video_count = 1)
                                             @foreach($user->hosts[0]->videos as $video)
                                             <tr data-off="{{$video_count}}" style="display: table-row;">
                                                <td>
                                                   {{Form::hidden('data[Video]['.$video_count.'][id]', $video->id)}}
                                                   {{Form::text('data[Video]['.$video_count.'][title]', $value = $video->title, $attributes = array('class'=>'form-control', 'id'=>'Video'.$video_count.'Title'))}}
                                                </td>
                                                <td>
                                                   {{Form::textarea('data[Video]['.$video_count.'][code]', $video->code,  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video'.$video_count.'Code' ))}}
                                                </td>
                                                <td>
                                                   <div class="checkbox checkbox-pink checkbox-inline">
                                                      {{ Form::checkbox('data[Video]['.$video_count.'][is_enabled]', '1' , ($video->is_enabled == 1 ? true : false), array('id'=>'Video'.$video_count.'IsEnabled1')) }}
                                                      {{Form::label('Video'.$video_count.'IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                   </div>
                                                </td>
                                                <td class="actions">
                                                   <div class="col-sm-6 col-md-4 col-lg-3 {{($video_count > 1) ? 'delete_host_videos' : 'host_videos'}}" style="display: block;">
                                                      <i class="typcn typcn-delete"></i> 
                                                   </div>
                                                </td>
                                             </tr>
                                             @php ($video_count++)
                                             @endforeach
                                             @else
                                             <tr data-id="" data-off="1" style="display: table-row;">
                                                <td>
                                                   {{Form::text('data[Video][1][title]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'Video1Title'))}}
                                                </td>
                                                <td>
                                                   {{Form::textarea('data[Video][1][code]', '',  array('step' => '1', 'min' => '1', 'class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'Video1Code' ))}}
                                                </td>
                                                <td>
                                                   <div class="checkbox checkbox-pink checkbox-inline">
                                                      {{ Form::checkbox('data[Video][1][is_enabled]', '1' , false, array('id'=>'Video1IsEnabled1')) }}
                                                      {{Form::label('Video1IsEnabled1', ' ', array('class' => 'col-sm-12 form-control-label'))}}
                                                   </div>
                                                </td>
                                                <td class="actions">
                                                   <div class="col-sm-6 col-md-4 col-lg-3 host_videos" style="display: block;">
                                                      <i class="typcn typcn-delete"></i> 
                                                   </div>
                                                </td>
                                             </tr>
                                             @endif
                                             @endif
                                          </tbody>
                                       </table>
                                       <div class="mtf-buttons" style="clear:both;float:right;">
                                          <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyVideosAdd"><i class="glyphicon glyphicon-plus"></i>
                                          <span>add</span>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- End row -->
                              <div class="form-group row">
                                 {{Form::label('HostTags', 'Tags', array('class' => 'col-sm-4 form-control-label'))}}
                                 <div class="col-sm-7 tag-input-type">
                                    <div class="form-group">
                                       {{Form::text('data[Host][tags]',$user->hosts->count() > 0 ? $user->hosts[0]->tags : '', ['id'=>'HostTags_tagsinput', 'class'=>'form-control form_ui_input', 'data-role' => 'tagsinput'])}}
                                       <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i> Enter comma separated values</span>
                                    </div>
                                 </div>
                              </div>
                              <!-- End row -->                    
                              @endif
                           </div>
                        </div>
                     </div>
                     <!-- end row -->
                     @if(Config::get('constants.HOST') != getCurrentUserType())
                     <div class="row">
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <h4 class="header-title m-t-0">Account & Contact</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('UserUsername', 'Username', array('class' => 'col-sm-2 form-control-label req'))}}
                                    <div class="col-sm-9">
                                        {{Form::text('data[User][username]', (isset($user->username))  ? $user->username : '', $attributes = array('class'=>'form-control required','data-unique'=> '1','data-form'=> 'profile',  'data-validation' => '1','data-parsley-maxlength'=>'45', 'id'=>'UserUsername'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-2 form-control-label req'))}}
                                    <div class="col-sm-9">
                                        {{Form::email('data[User][email]', (isset($user->email))  ? $user->email : '', array('class'=>'form-control required', 'data-unique'=> '1','data-form'=> 'profile',  'data-validation' => '1', 'data-parsley-maxlength'=>'45', 'id'=>'UserEmail', 'required'=>true,'autocomplete'=>'off'))}}
                                        {{Form::hidden('data[User][id]', $user->id, array('id'=>'id'))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('UserPhoto0', 'Photo', array('class' => 'col-sm-2 form-control-label'))}}
                                    <div class="col-sm-8">
                                        {{Form::hidden('edit_id', $user->id, array('id'=>'edit_id'))}}
                                        <div class="row">
                                        @if($media->count() > 0)
                                        @php ($url_count = 1)
                                            @foreach($media as $med)
                                            @if($med->sub_module == 'photo')
                                            <div class="col-sm-4 adding-medias media_User_photo" data-off="{{$url_count}}">
                                                <div class="jFiler-items jFiler-row">
                                                    <ul class="jFiler-items-list jFiler-items-grid">
                                                        <li class="jFiler-item" data-jfiler-index="1">          
                                                            <div class="jFiler-item-container">     <div class="jFiler-item-inner">                           
                                                                    <div class="jFiler-item-thumb">    
                                                                        <a href="javascript:void(0)" onclick="editMediaImage(this, 'User', 'photo', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                            <div class="jFiler-item-thumb-image">
                                                                                <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                            </div> 
                                                                        </a>  
                                                                    </div>                                    
                                                                    <div class="jFiler-item-assets jFiler-row">
                                                                        <ul class="list-inline pull-right">
                                                                            <li>
                                                                                <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_photo" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="User" data-submodule="photo"></a>
                                                                            </li>                    
                                                                        </ul>    
                                                                    </div>     
                                                                </div>                     
                                                            </div>                        
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            @php ($url_count++)
                                            @endif
                                            @endforeach
                                        @endif
                                        <div class="media_User_photo_outer hidden" data-off="0" style="display: none;">
                                        </div>
                                    </div>
                                    <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                     Only jpg/jpeg/png/gif files.<span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span>Clik on image to edit.</span>
                                </div>
                                <div class="col-sm-2">
                                    <div class="mtf-buttons" style="clear:both;float:right;">
                                        <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="User" data-submodule="photo" data-media_type="image" data-limit="1" data-dimension="" data-module_id="{{$id}}" data-file_crop="1">Upload files</button>
                                     </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                     </div>
                     <!-- end row -->
                     @endif
                  </div>
                  <!-- end card-box -->
               </div>
               <!-- end col-->
            </div>
            <!-- end row -->
            @if(Config::get('constants.HOST') == getCurrentUserType())
            <div class="row">
                <!-- Info Modal -->
                <div id="info-modal" class="modal-demo">
                    <button type="button" class="close" onclick="Custombox.close();">
                        <span>&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="custom-modal-title">Title</h4>
                    <div class="custom-modal-text">
                        <div class="form-group row">                      
                            {{Form::hidden('module', '', array('id'=>'info_module'))}}
                            {{Form::hidden('section', '', array('id'=>'info_section'))}}                        
                            <div class="col-md-12">
                                <textarea class="form-control module_info_desc" rows="5" placeholder="Add info here..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 col-md-6">
                    <div class="card-box">
                        <div class="info_title">
                            <div class="col-md-9">
                               <h4 class="header-title m-t-0">Account & Contact</h4>
                            </div>
                            <div class="col-md-2"> 
                                
                                <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'Account & Contact', this)" title="Section Info" id="sa-title">What is this?</a>

                            </div>                       
                        </div>
                        <div class="p-20">
                            <div class="form-group row">
                               {{Form::label('UserEmail', 'Email', array('class' => 'col-sm-4 form-control-label req'))}}
                               <div class="col-sm-7">
                                  {{Form::email('data[User][email]', $user->email, $attributes = array('class'=>'form-control required', 'id'=> 'UserEmail', 'data-parsley-maxlength'=>'255'))}}
                                  {{Form::hidden('data[User][id]', $user->id, array('id'=>'UserId'))}}
                                  {{Form::hidden('data[User][username]', $user->username, array('id'=>'UserUsername'))}}
                               </div>
                               <!-- end col-->
                            </div>
                           <div class="form-group row">
                              {{Form::label('UserAlternateEmail', 'Alternate Email', array('class' => 'col-sm-4 form-control-label'))}}
                              <div class="col-sm-7">
                                 {{Form::email('data[Host][alternate_email]',$user->hosts[0]->alternate_email, $attributes = array('class'=>'form-control','id'=>'UserAlternateEmail',))}}
                                 <span class="error-block" style="display:none;"></span>
                              </div>
                           </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('ProfilePhone', 'Land line phone', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::tel('data[Profile][phone]', ($user->profiles->count() > 0) ? $user->profiles[0]->phone : null, $attributes = array('class'=>'form-control', 'id'=> 'ProfilePhone', 'data-parsley-maxlength'=>'45'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('ProfileCellphone', 'Cell phone', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::tel('data[Profile][cellphone]', ($user->profiles->count() > 0) ? $user->profiles[0]->cellphone : null, $attributes = array('class'=>'form-control', 'id'=> 'ProfileCellphone', 'data-parsley-maxlength'=>'tel'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('ProfileSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Profile][skype]', ($user->profiles->count() > 0) ? $user->profiles[0]->skype : null, $attributes = array('class'=>'form-control', 'id'=> 'ProfileSkype', 'data-parsley-maxlength'=>'45'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('ProfileSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::tel('data[Profile][skype_phone]', ($user->profiles->count() > 0) ? $user->profiles[0]->skype_phone : null, $attributes = array('class'=>'form-control', 'id'=> 'ProfileSkypePhone', 'data-parsley-maxlength'=>'255'))}}
                               </div>
                            </div>
                            <!-- end row -->
                         </div>
                         <!-- end p-20-->
                        </div>
                      <!-- end card-box -->
                    </div>
                   <div class="col-sm-6 col-xs-6 col-md-6">
                      <div class="card-box">
                        <div class="info_title">
                            <div class="col-md-9">
                               <h4 class="header-title m-t-0">Other Contact</h4>
                            </div>
                            <div class="col-md-2"> 
                                <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'Other Contact', this)" title="Section Info" id="sa-title">What is this?</a>
                            </div>                       
                        </div>
                         <div class="p-20">
                            <div class="form-group row">
                               {{Form::label('HostPrType', 'Type', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-12">
                                  <div class="radio radio-info radio-inline">
                                     {{Form::radio('data[Host][pr_type]', 'pr', ($user->hosts->count() > 0) ? (($user->hosts[0]->pr_type) ? (($user->hosts[0]->pr_type == "pr") ? true : false) : false) : true, array('id'=>'HostPrTypePr'))}}
                                     {{Form::label('HostPrTypePr', 'Pr', array('class' => 'col-sm-4 form-control-label'))}}
                                  </div>
                                  <div class="radio radio-warning radio-inline">
                                     {{Form::radio('data[Host][pr_type]', 'publisher', ($user->hosts->count() > 0) ? (($user->hosts[0]->pr_type) ? (($user->hosts[0]->pr_type == "publisher") ? true : false) : false) : false, array('id'=>'HostPrTypePublisher'))}}
                                     {{Form::label('HostPrTypePublisher', 'Publisher', array('class' => 'col-sm-4 form-control-label'))}}
                                  </div>
                                  <div class="radio radio-purple radio-inline">
                                     {{Form::radio('data[Host][pr_type]', 'assistant', ($user->hosts->count() > 0) ? (($user->hosts[0]->pr_type) ? (($user->hosts[0]->pr_type == "assistant") ? true : false) : false) : false, array('id'=>'HostPrTypeAssistant'))}}
                                     {{Form::label('HostPrTypeAssistant', 'Assistant', array('class' => 'col-sm-4 form-control-label'))}}
                                  </div>
                                  <div class="radio radio-custom radio-inline">
                                     {{Form::radio('data[Host][pr_type]', 'other', ($user->hosts->count() > 0) ? (($user->hosts[0]->pr_type) ? (($user->hosts[0]->pr_type == "other") ? true : false) : false) : false, array('id'=>'HostPrTypeOther'))}}
                                     {{Form::label('HostPrTypeOther', 'Other', array('class' => 'col-sm-4 form-control-label'))}}
                                  </div>
                               </div>
                            </div>
                            <div class="form-group row">
                               {{Form::label('HostPrName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][pr_name]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_name : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrName'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostPrCompanyName', 'Company Name', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][pr_company_name]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_company_name : '', $attributes = array('class'=>'form-control', 'id'=>'HostPrCompanyName'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostPrEmail', 'Email', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::email('data[Host][pr_email]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_email : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrEmail'))}}
                                  <div class="error-block" style="display:none;"></div>
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostPrPhone', 'Landline Phone', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][pr_phone]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_phone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrPhone'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostPrCellphone', 'Cell phone', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][pr_cellphone]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_cellphone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'45', 'id'=>'HostPrCellphone'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostPrSkype', 'Skype', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][pr_skype]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_skype : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'HostPrSkype'))}}
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostPrSkypePhone', 'Skype phone', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][pr_skype_phone]', $user->hosts->count() > 0 ? $user->hosts[0]->pr_skype_phone : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'255', 'id'=>'HostPrSkypePhone'))}}
                               </div>
                            </div>
                            <div class="form-group row">
                                {{Form::label('ProZoomLink', 'Zoom Link', array('class' => 'col-sm-4 form-control-label'))}}
                                <div class="col-sm-7 tag-input-type">
                                   <div class="form-group">
                                      {{Form::url('data[Host][zoom_link]',$user->hosts[0]->zoom_link, $attributes = array('class'=>'form-control', 'id'=>'ProZoomLink'))}}
                                      <span class="error-block" style="display:none;"></span>
                                   </div>
                                </div>
                             </div><!-- End row --> 
                            <!-- end row -->
                         </div>
                         <!-- end p-20-->
                      </div>
                      <!-- end card-box -->
                   </div>
                </div>
                <!-- end row -->
                <div class="row">
                   <div class="col-sm-12 col-xs-12 col-md-12">
                      <div class="card-box">
                        <div class="info_title">
                            <div class="col-md-10">
                               <h4 class="header-title m-t-0">iTunes Profile Setup</h4>
                            </div>
                            <div class="col-md-2"> 
                                <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'iTunes Profile Setup', this)" title="Section Info" id="sa-title">What is this?</a>
                            </div>                       
                        </div>
                        <div class="p-20">
                            <div class="form-group row">
                               {{Form::label('HostArchivedRssFeedUrl', 'iTunes feed url', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  <a href="{{url('feed/episodes/archived.rss?host=')}}" target="_blank">{{url('feed/episodes/archived.rss?host=')}}</a>
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostItunesTitle', 'Title', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][itunes_title]', $user->hosts->count() > 0 ? $user->hosts[0]->itunes_title : '', $attributes = array('class'=>'form-control', 'data-parsley-maxlength'=>'225', 'id'=>'HostItunesTitle'))}}
                               </div>
                            </div>
                            <div class="form-group row">
                               {{Form::label('HostItunesSubtitle', 'Subtitle', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{Form::text('data[Host][itunes_subtitle]', $user->hosts->count() > 0 ? $user->hosts[0]->itunes_subtitle : '',  $attributes = array('class'=>'form-control', 'id'=>'HostItunesSubtitle'))}}
                               </div>
                            </div>
                            <div class="form-group row">
                               {{Form::label('HostItunesDesc', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  {{ Form::textarea('data[Host][itunes_desc]', $user->hosts->count() > 0 ? $user->hosts[0]->itunes_desc : '', ['id' => 'HostItunesDesc']) }}
                               </div>
                            </div>
                            <div class="form-group row">
                               {{Form::label('HostHasManyItunesCategories', 'Category / Sub-Category', array('class' => 'col-sm-4 form-control-label'))}}
                               <?php //echo "<pre>";print_R(json_decode($itunes_categories));?>
                               <div class="col-sm-7">
                                  {{Form::hidden('itunes_cat_combo', $itunes_categories, ['id'=>'itunes_cat_combo'] )}}
                                  @if($user->hosts->count() > 0)
                                  @if(!empty($user->hosts[0]->itunes_categories))
                                  @php ($itunes_cat_count = 1)
                                  @foreach($user->hosts[0]->itunes_categories as $itunes_cat)
                                  <div class="outside_itunes_cat" data-off="{{$itunes_cat_count}}">
                                     <div class="col-sm-5">
                                        {{Form::select('data[ItunesCategory]['.$itunes_cat_count.'][category]', (!empty($itunes_categories) ? json_decode($itunes_categories)[0] : ''), $itunes_cat->category, $attributes=array('id'=>'ItunesCategory'.$itunes_cat_count.'Category', 'class'=>'form-control itunes_main_cat', 'data-style'=>'btn-default'))}}
                                     </div>
                                     <div class="col-sm-5">
                                        {{Form::select('data[ItunesCategory]['.$itunes_cat_count.'][subcategory]', (!empty($itunes_categories) ? json_decode($itunes_categories)[1]->{$itunes_cat->category} : ''), $itunes_cat->subcategory, $attributes=array('id'=>'ItunesCategory'.$itunes_cat_count.'Subcategory', 'class'=>'form-control itunes_sub_cat', 'data-style'=>'btn-default'))}}
                                     </div>
                                  </div>
                                  @php ($itunes_cat_count++)
                                  @endforeach
                                  @else
                                  <div class="outside_itunes_cat" data-off="1">
                                     <div class="col-sm-5">
                                        {{Form::select('data[ItunesCategory][1][category]', (!empty($itunes_categories) ? json_decode($itunes_categories)[0] : ''), (count( (array)$user->hosts[0]->itunes_categories) > 0) ? 'Arts' : '', $attributes=array('id'=>'ItunesCategory1Category', 'class'=>'form-control itunes_main_cat', 'data-style'=>'btn-default'))}}
                                     </div>
                                     <div class="col-sm-5">
                                        {{Form::select('data[ItunesCategory][1][subcategory]', (!empty($itunes_categories) ? json_decode($itunes_categories)[1]->Arts : ''), $user->hosts[0]->itunes_categories->{1}->subcategory, $attributes=array('id'=>'ItunesCategory1Subcategory', 'class'=>'form-control itunes_sub_cat', 'data-style'=>'btn-default'))}}
                                     </div>
                                  </div>
                                  @endif
                                  @endif
                               </div>
                            </div>
                            <div class="form-group row">
                               {{Form::label('HostItunes0', 'Image', array('class' => 'col-sm-2 form-control-label'))}}
                               <div class="col-sm-8">
                                  {{Form::hidden('edit_id', $user->hosts->count() > 0 ? $user->hosts[0]->id : 0, array('id'=>'edit_id'))}}
                                  <div class="row">
                                     @if($media->count() > 0)
                                     @php ($url_count = 1)
                                     @foreach($media as $med)
                                     @if($med->sub_module == 'itunes')
                                     <div class="col-sm-4 adding-medias media_Host_itunes" data-off="{{$url_count}}">
                                        <div class="jFiler-items jFiler-row">
                                           <ul class="jFiler-items-list jFiler-items-grid">
                                              <li class="jFiler-item" data-jfiler-index="1">          
                                                 <div class="jFiler-item-container">                       
                                                    <div class="jFiler-item-inner">                           
                                                       <div class="jFiler-item-thumb">    
                                                          <a href="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" target="_blank">         
                                                                <div class="jFiler-item-thumb-image">
                                                                <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                             </div> 
                                                          </a>  
                                                       </div>                                    
                                                       <div class="jFiler-item-assets jFiler-row">
                                                       <ul class="list-inline pull-right">
                                                          <li>
                                                             <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_Host_itunes" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="itunes"></a>
                                                          </li>                           
                                                       </ul>                                    
                                                       </div>                                
                                                    </div>                            
                                                 </div>                        
                                              </li>
                                           </ul>
                                        </div>
                                     </div>
                                     @php ($url_count++)
                                     @endif
                                     @endforeach
                                     @endif
                                     <div class="media_Host_itunes_outer hidden" data-off="0" style="display: none;">
                                     </div>
                                  </div>
                                  <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                  Only jpg/jpeg/png/gif files.<span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span>Clik on image to edit.<br>
                                  iTunes requires an exact 1400x1400 pixel image.
                                 </span>

                               </div>
                               <div class="col-sm-2">
                                  <div class="mtf-buttons" style="clear:both;float:left;">
                                     <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="Host" data-submodule="itunes" data-media_type="image" data-limit="1" data-dimension="" data-module_id="{{$id}}" data-file_crop="1">Upload files</button>
                                  </div>
                               </div>
                            </div>
                            <!-- End row -->
                         </div>
                      </div>
                   </div>
                </div>
                <!-- End row -->
                <div class="row">
                   <div class="col-sm-12 col-xs-12 col-md-12">
                      <div class="card-box">
                        <div class="info_title">
                            <div class="col-md-10">
                               <h4 class="header-title m-t-0">Settings</h4>
                            </div>
                            <div class="col-md-2"> 
                                <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'Settings', this)" title="Section Info" id="sa-title">What is this?</a>
                            </div>                       
                        </div>
                         <div class="p-20">
                            <div class="form-group row">
                               {{Form::label('HostArchivedRssFeedUrl', 'Archived Shows RSS feed url', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  @if($user->hosts->count() > 0)
                                  <a href="{{url('feed/episodes/archived.rss?host='.$user->hosts[0]->id)}}" target="_blank">{{url('feed/episodes/archived.rss?host='.$user->hosts[0]->id)}}</a>
                                  @endif
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostUpcomingRssFeedUrl', 'Upcoming Shows RSS feed url', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  @if($user->hosts->count() > 0)
                                  <a href="{{url('feed/episodes/upcoming.rss?host='.$user->hosts[0]->id)}}" target="_blank">{{url('feed/episodes/upcoming.rss?host='.$user->hosts[0]->id)}}</a>
                                  @endif
                               </div>
                            </div>
                            <!-- end row -->
                            <div class="form-group row">
                               {{Form::label('HostD', 'Listener feedback email notification', array('class' => 'col-sm-4 form-control-label'))}}
                               <div class="col-sm-7">
                                  <div class="col-sm-4">
                                     <div class="radio radio-inline">
                                        {{ Form::radio('data[Host][is_feedback_email_notification]', '0', ($user->hosts->count() > 0) ? ($user->hosts[0]->is_feedback_email_notification == 0) ? true : false : false, array('id'=>'HostIsFeedbackEmailNotification0'))}}
                                        {{Form::label('HostIsFeedbackEmailNotification0', 'Off', array('class' => 'col-sm-4 form-control-label'))}}
                                     </div>
                                     <div class="radio radio-info radio-inline">
                                        {{ Form::radio('data[Host][is_feedback_email_notification]', '1', ($user->hosts->count() > 0) ? ($user->hosts[0]->is_feedback_email_notification == 1) ? true : false : false, array('id'=>'HostIsFeedbackEmailNotification1'))}}
                                        {{Form::label('HostIsFeedbackEmailNotification1', 'On', array('class' => 'col-sm-4 form-control-label'))}}
                                     </div>
                                  </div>
                                  <div class="col-sm-6">
                                     <?php 
                                        $style = "";
                                        if($user->hosts->count() > 0){
                                            if($user->hosts[0]->is_feedback_email_notification == 1){
                                                $style="display:block";
                                            }else{
                                                $style = "display:none";
                                            }
                                        }
                                        ?>
                                     {{Form::email('data[Host][feedback_notification_email]',$user->hosts->count() > 0 ? $user->hosts[0]->feedback_notification_email : '', ['id'=>'HostFeedbackNotificationEmail', 'placeholder'=>'email address', 'class'=>'form-control', 'style'=>$style] )}}
                                  </div>
                               </div>
                            </div>
                            <!-- end row -->                            
                            {{Form::hidden('data[User][groups_id]', 2, ['id'=>'UserGroupsId'])}}
                         </div>
                      </div>
                      <!-- costom Content -->
                      <div class="row">
                         <div class="col-xs-12">
                            <div class="card-box">
                               <div class="row">
                                  <div class="col-sm-12 col-xs-12 col-md-12">
                                    <div class="info_title">
                                        <div class="col-md-10">
                                           <h4 class="header-title m-t-0">Testimonials</h4>
                                        </div>
                                        <div class="col-md-2"> 
                                            <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'Testimonials', this)" title="Section Info" id="sa-title">What is this?</a>
                                        </div>                       
                                    </div>
                                    <div class="p-20">
                                        <div calss= "row">
                                           <div class="col-md-5">
                                              Testimonial
                                           </div>
                                           <div class="col-md-3">
                                              Author
                                           </div>
                                           <div class="col-md-2">
                                              Enabled
                                           </div>
                                           <div class="col-md-2">
                                              &nbsp;
                                           </div>
                                        </div>
                                        {{Form::hidden('edit_id', $user->id, array('id'=>'edit_id'))}}
                                        {{Form::hidden('form_type', 'edit', array('id'=>'form_type'))}}
                                        @if($user->hosts->count() > 0)
                                        @if($user->hosts[0]->host_testimonials->count() > 0)
                                        @php ($testimonial_count = 1)
                                        @foreach($user->hosts[0]->host_testimonials as $testimonials) 
                                        <div class="form-group row host_testimonials" data-off="{{$testimonial_count}}">
                                           {{Form::hidden('data[HostTestimonial]['.$testimonial_count.'][id]', $testimonials['id'], array('id'=>'host_content_id'.$testimonial_count))}}                                          
                                           <div class="col-md-5">
                                              {{Form::textarea('data[HostTestimonial]['.$testimonial_count.'][testimonial]',$testimonials["testimonial"],  array('class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'HostTestimonial'.$testimonial_count.'Testimonial' ))}}
                                           </div>
                                           <div class="col-md-3">
                                              {{Form::text('data[HostTestimonial]['.$testimonial_count.'][author]',$testimonials["author"], $attributes = array('class'=>'form-control', 'id'=>'HostTestimonial'.$testimonial_count.'Author','maxlength'=>'255'))}}
                                           </div>
                                           <div class="col-md-2">
                                              {{ Form::checkbox('data[HostTestimonial]['.$testimonial_count.'][is_enabled]', '1' ,($testimonials["is_enabled"] == "1") ? 'checked' : '', array('id'=>'HostTestimonial'.$testimonial_count.'IsEnabled1')) }}
                                           </div>
                                           <div class="col-md-2 {{($testimonial_count == 1) ? 'testimonial' : 'delete_Testimonial'}}" style="display: block;">
                                              <i class="typcn typcn-delete"></i> 
                                           </div>
                                        </div>
                                        @php ($testimonial_count++)
                                        @endforeach
                                        @else
                                        <div class="form-group row host_testimonials" data-off="1">
                                           <div class="col-md-5">
                                              {{Form::textarea('data[HostTestimonial][0][testimonial]', '',  array('class'=>'form-control', 'rows'=>2, 'cols'=>30, 'id'=>'HostTestimonial0Testimonial' ))}}
                                           </div>
                                           <div class="col-md-3">
                                              {{Form::text('data[HostTestimonial][0][author]', $value = null, $attributes = array('class'=>'form-control', 'id'=>'HostTestimonial0Author','maxlength'=>'255'))}}
                                           </div>
                                           <div class="col-md-2">
                                              {{ Form::checkbox('data[HostTestimonial][0][is_enabled]', '1' , false, array('id'=>'HostTestimonial0IsEnabled1')) }}
                                           </div>
                                           <div class="col-md-2 testimonial" style="display: block;"><i class="typcn typcn-delete"></i> 
                                           </div>
                                           @endif
                                           @endif 
                                           <div class="mtf-buttons" style="clear:both;float:right;">
                                              <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyHostTestimonialsAdd"><i class="glyphicon glyphicon-plus"></i>
                                              <span>add</span>
                                              </button>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                                  <!-- end p-20 -->
                               </div>
                               <!-- end row -->
                            </div>
                            <!-- end card-box -->
                         </div>
                         <!-- end col-->
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="info_title">
                                            <div class="col-md-10">
                                                <h4 class="header-title m-t-0">Custom Content</h4>
                                            </div>
                                            <div class="col-md-2"> 
                                                <a href="javascript:void(0);" class="btn btn-pink waves-effect waves-light m-r-5 m-b-10" onclick="infoModule('Host', 'Custom Content', this)" title="Section Info" id="sa-title">What is this?</a>
                                            </div>                       
                                        </div>
                                        @if($user->hosts->count() > 0)
                                            @if($user->hosts[0]->host_contents->count() > 0)
                                            @php ($contents_count = 1)
                                            @foreach($user->hosts[0]->host_contents as $contents)
                                            <div class="col-md-6 card-box host_contents" data-off="{{$contents_count}}">
                                                <div class="form-group row">
                                                    <div class="col-md-3 {{($contents_count ==1) ? 'custom_content' : 'delete_Contents'}}" style="display: block;"><i class="typcn typcn-delete"></i> 
                                                    </div>
                                                    <div class="p-20">
                                                       {{Form::hidden('data[HostContent]['.$contents_count.'][id]', $contents['id'], array('id'=>'host_content_id'.$contents_count))}}    
                                                       {{Form::hidden('data[HostContent]['.$contents_count.'][offset]', 'image_'.$contents_count)}}    
                                                       <div class="col-md-12">
                                                          {{Form::label('HostContent'.$contents_count.'Title', 'Title', array('class' => 'col-sm-4 form-control-label'))}} 
                                                          {{Form::text('data[HostContent]['.$contents_count.'][title]',$contents['title'],  array('class'=>'form-control','maxlength'=>'255', 'id'=>'HostContent'.$contents_count.'Title' ))}}
                                                       </div>
                                                       <div class="col-md-12">
                                                          {{Form::label('HostContent'.$contents_count.'Url', 'Url', array('class' => 'col-sm-4 form-control-label'))}} 
                                                          {{Form::url('data[HostContent]['.$contents_count.'][url]',$contents['url'],  array('class'=>'form-control','id'=>'HostContent'.$contents_count.'Url' ))}}
                                                       </div>
                                                       <div class="col-md-12">
                                                          {{Form::label('HostContent'.$contents_count.'Description', 'Description', array('class' => 'col-sm-4 form-control-label'))}} 
                                                          {{Form::textarea('data[HostContent]['.$contents_count.'][description]',$contents['description'],  array('class'=>'form-control','maxlength'=>'180','row'=>'4','col'=>'30' , 'id'=>'HostContent'.$contents_count.'Description' ))}}
                                                       </div>
                                                       <div class="col-md-12">
                                                          <div class="form-group row">
                                                             <div class="col-sm-12">
                                                                {{Form::hidden('edit_id', $user->hosts->count() > 0 ? $user->hosts[0]->id : 0, array('id'=>'edit_id'))}}
                                                                <div class="row">
                                                                   @if($host_content_media->count() > 0)
                                                                   @php ($url_count = 1)
                                                                   @foreach($host_content_media as $med)
                                                                   @if($med->sub_module == 'image_'.$contents_count && $med->module_id == $contents['id'])
                                                                   <div class="col-sm-4 adding-medias media_HostContent_image_{{$contents_count}}" data-off="{{$url_count}}">
                                                                   <div class="jFiler-items jFiler-row">
                                                                      <ul class="jFiler-items-list jFiler-items-grid">
                                                                         <li class="jFiler-item" data-jfiler-index="1">          
                                                                            <div class="jFiler-item-container">                       
                                                                               <div class="jFiler-item-inner">                           
                                                                                  <div class="jFiler-item-thumb">    
                                                                                     <a href="javascript:void(0)" onclick="editMediaImage(this, 'HostContent', 'image_'{{$contents_count}}, 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                                        <div class="jFiler-item-thumb-image">
                                                                                           <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                                        </div> 
                                                                                     </a>  
                                                                                  </div>                                    
                                                                                  <div class="jFiler-item-assets jFiler-row">
                                                                                  <ul class="list-inline pull-right">
                                                                                     <li>
                                                                                        <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_HostContent_image_{{$contents_count}}" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="Host" data-submodule="photo"></a>
                                                                                     </li>                           
                                                                                  </ul>                                    
                                                                                  </div>                                
                                                                               </div>                            
                                                                            </div>                        
                                                                         </li>
                                                                      </ul>
                                                                   </div>
                                                                   </div>
                                                                   @php ($url_count++)
                                                                   @endif
                                                                   @endforeach
                                                                   @endif
                                                                   <div class="media_HostContent_image_{{$contents_count}}_outer hidden" data-off="0" style="display: none;">
                                                                   </div>
                                                                </div>
                                                                <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                                Only jpg/jpeg/png/gif files.</span><span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span><span>Clik on image to edit.</span>
                                                             </div>
                                                             <div class="mtf-buttons" style="clear:both;float:left;">
                                                                <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="HostContent" data-submodule="image_{{$contents_count}}" data-media_type="image" data-limit="1" data-dimension=""data-module_id="{{$contents['id']}}" data-file_crop="1">Upload files</button>
                                                             </div>
                                                          </div>
                                                       </div>
                                                       <div class="col-sm-12">
                                                          <div class="radio radio-info radio-inline">
                                                             {{Form::radio('data[HostContent]['.$contents_count.'][is_enabled]', '1',($contents["is_enabled"] == "1") ? true : false, array('id'=>'HostContent'.$contents_count.'IsEnabled1'))}}
                                                             {{Form::label('HostContent'.$contents_count.'IsEnabled1', 'Enabled', array('class' => 'col-sm-4 form-control-label'))}}
                                                          </div>
                                                          <div class="radio radio-warning radio-inline">
                                                             {{Form::radio('data[HostContent]['.$contents_count.'][is_enabled]', '0',($contents["is_enabled"] == "0") ? true : false, array('id'=>'HostContent'.$contents_count.'IsEnabled0'))}}
                                                             {{Form::label('HostContent'.$contents_count.'IsEnabled0', 'Disabled', array('class' => 'col-sm-4 form-control-label'))}}
                                                          </div>
                                                       </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @php ($contents_count++)
                                            @endforeach
                                            @else
                                            <div class="col-md-6 card-box host_contents" data-off="1">
                                     <div class="form-group row">
                                        <div class="p-20">
                                           {{Form::hidden('data[HostContent][1][offset]', 'image_1')}}    
                                           <div class="col-md-12">
                                              {{Form::label('HostContent0Title', 'Title', array('class' => 'col-sm-4 form-control-label'))}} 
                                              {{Form::text('data[HostContent][0][title]', '',  array('class'=>'form-control','maxlength'=>'255', 'id'=>'HostContent0Title' ))}}
                                           </div>
                                           <div class="col-md-12">
                                              {{Form::label('HostContent0Url', 'Url', array('class' => 'col-sm-4 form-control-label'))}} 
                                              {{Form::url('data[HostContent][0][url]', '',  array('class'=>'form-control','id'=>'HostContent0Url' ))}}
                                           </div>
                                           <div class="col-md-12">
                                              {{Form::label('HostContent0Description', 'Description', array('class' => 'col-sm-4 form-control-label'))}} 
                                              {{Form::textarea('data[HostContent][0][description]', '',  array('class'=>'form-control','maxlength'=>'180','row'=>'4','col'=>'30' , 'id'=>'HostContent0Description' ))}}
                                           </div>
                                           <div class="col-md-12">
                                              <div class="form-group row">
                                                 <div class="col-sm-12">
                                                    {{Form::hidden('edit_id', $user->hosts->count() > 0 ? $user->hosts[0]->id : 0, array('id'=>'edit_id'))}}
                                                     <div class="row">
                                                       @if($host_content_media->count() > 0)
                                                       @php ($url_count = 1)
                                                       @foreach($host_content_media as $med)
                                                       @if($med->sub_module == 'image_1' && $med->module_id == '1')
                                                       <div class="col-sm-4 adding-medias media_HostContent_image_1" data-off="1">
                                                          <div class="jFiler-items jFiler-row">
                                                             <ul class="jFiler-items-list jFiler-items-grid">
                                                                <li class="jFiler-item" data-jfiler-index="1">          
                                                                   <div class="jFiler-item-container">                       
                                                                      <div class="jFiler-item-inner">                           
                                                                         <div class="jFiler-item-thumb">    
                                                                            <a href="javascript:void(0)" onclick="editMediaImage(this, 'HostContent', 'image_1', 'image', 0, '{{$med->media->filename}}', {{$med->id}})" target="_blank">                              
                                                                               <div class="jFiler-item-thumb-image">
                                                                                  <img src="{{asset('storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename)}}" draggable="false">
                                                                               </div> 
                                                                            </a>  
                                                                         </div>                                    
                                                                         <div class="jFiler-item-assets jFiler-row">
                                                                         <ul class="list-inline pull-right">
                                                                            <li>
                                                                               <a class="icon-jfi-trash jFiler-item-trash-action color_icon delete_HostContent_image_1" onclick="mediaLinkDelete({{$med->id}}, this);" data-mainmodule="HostContent" data-submodule="image_1"></a>
                                                                            </li>                           
                                                                         </ul>                                    
                                                                         </div>                                
                                                                      </div>                            
                                                                   </div>                        
                                                                </li>
                                                             </ul>
                                                          </div>
                                                       </div>
                                                       @php ($url_count++)
                                                       @endif
                                                       @endforeach
                                                       @endif
                                                       <div class="media_HostContent_image_1_outer hidden" data-off="0" style="display: none;">
                                                       </div>
                                                    </div>
                                                    <span class="help-block"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                    Only jpg/jpeg/png/gif files.</span><span style="color:#f9c851;"> Maximum 1 file. Maximum file size: 120MB.</span><span>Clik on image to edit.</span>
                                                 </div>
                                                 <div class="mtf-buttons" style="clear:both;float:left;">
                                                    <button type="button" class="btn btn-info btn-lg open_choose_media" data-toggle="modal" id="" data-target="" data-mainmodule="HostContent" data-submodule="image_1" data-media_type="image" data-limit="1" data-dimension="" data-module_id="0">Upload files</button>
                                                 </div>
                                                 </div>
                                              </div>
                                           </div>
                                           <div class="col-sm-12">
                                              <div class="radio radio-info radio-inline">
                                                 {{Form::radio('data[HostContent][0][is_enabled]', '1',true, array('id'=>'HostContent0IsEnabled1'))}}
                                                 {{Form::label('HostContent0IsEnabled1', 'Enabled', array('class' => 'col-sm-4 form-control-label'))}}
                                              </div>
                                              <div class="radio radio-warning radio-inline">
                                                 {{Form::radio('data[HostContent][0][is_enabled]', '0',false, array('id'=>'HostContent0IsEnabled0'))}}
                                                 {{Form::label('HostContent0IsEnabled0', 'Disabled', array('class' => 'col-sm-4 form-control-label'))}}
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                            </div>
                                            @endif
                                        @endif
                                    </div>
                                    <!-- end row -->    
                                </div>
                                <!-- end card-box -->
                            </div>
                            <!-- end col-->
                            <div class="mtf-buttons" style="clear:both;float:right;">
                                <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5" id="HostHasManyHostContentsAdd"><i class="glyphicon glyphicon-plus"></i>
                                <span>add</span>
                                </button>
                            </div>
                        </div>
                        <!-- end row -->                              
                    </div>
                </div>
                <!-- End row -->
            @endif
            <div class="form-group">
               <div class="fixed_btn">
                  <button  type="button" id="UserBtnSave" name="data[User][btnSave]" value="edit"class="btn btn-primary waves-effect waves-light submit_form"  data-form-id="UserAdminProfileForm">
                  Save
                  </button>
               </div>
            </div>
            <!-- end row -->
         </div>
         {{ Form::close() }}
      </div>
      <!-- end row -->
      @include('backend.cms.upload_media_popup',['module_id' =>'','main_module' =>''])
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection