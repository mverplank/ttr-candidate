@extends('backend.layouts.main')
@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Help</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row --> 
        <div class="row">
            <div style="height: calc(100% - 82px);" class="small-12 columns">                
                <iframe src="{{ url('/admin/help/pages/view/index') }}" frameborder="0" style="position: relative; height: 100%; width: 100%;min-height: 400px;" height="100%" width="100%"></iframe>
              </div>
        </div>
    </div> <!-- container -->
</div> <!-- content -->
@endsection