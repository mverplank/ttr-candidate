<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">« back to index</a></p>
<h1 id="scheduled-social-media-share-feature">Scheduled Social Media Share feature</h1>
<hr>
<p>** Social Media Applications configuration ( administrator )  **</p>
<ul>
<li>
<p>Facebook</p>
<ol>
<li>
<p>Go to: https://developers.facebook.com/apps/ and click on <code>+ Add a New App</code></p>
</li>
<li>
<p>Follow instructions on screen and enter required data until new app created sucesfully</p>
</li>
<li>
<p>Once new app created select it and change settings / enter below data:</p>
</li>
</ol>
<ul>
<li><code>Settings &gt; Basic</code> on <code>App Domains</code> enter domain(s) name: www.transformationtalkradio.com<br>
NOTE: All used channels domians must be included here</li>
<li><code>Settings &gt; Basic</code> on <code>Privacy Policy URL</code> enter right URL to privacy policy page</li>
<li><code>Settings &gt; Basic</code> on <code>Category</code> select right category</li>
<li><code>Settings &gt; Basic</code> click on <code>+ Add Platform</code> and choose <code>Website</code>; as <code>Site URL</code> enter full url: https://www.transformationtalkradio.com</li>
<li><code>Facebook Login &gt; Settings</code> on <code>Valid OAuth Redirect URIs</code> enter URL: https://www.transformationtalkradio.com/social_media/facebook/callback<br>
NOTE: All used channels domians must be included here</li>
<li><code>App Review</code> &gt; On &quot;Make (...) public?&quot;&quot; check <code>yes</code></li>
</ul>
<ol start="4">
<li>Go to <code>Settings &gt; Basic</code> and copy <code>App ID</code> and <code>App Secret</code></li>
</ol>
<ul>
<li>
<p>Logged-in as admin paste both values on transformnet app on: Configuration &gt; Base 'Facebook App' section (https://www.transformationtalkradio.com/admin/cms/configurations/base) and Click <code>Save</code></p>
</li>
<li>
<p>Twitter</p>
<ol>
<li>Go to: https://apps.twitter.com/app/new and follow on screen instructions</li>
</ol>
<ul>
<li>as <code>Callback URL</code> enter value: https://transformationtalkradio.com/social_media/twitter/callback</li>
</ul>
<ol start="2">
<li>
<p>Once new application created select it and go to: Permissions &gt; Access and change (if not already set) value of &quot;What type of access does your application need?&quot; to <code>Read and Write</code></p>
</li>
<li>
<p>Go to twitter application <code>Keys and Access Tokens</code> where copy values of <code>Consumer Key (API Key)</code> and <code>Consumer Secret (API Secret)</code></p>
</li>
</ol>
<ul>
<li>Paste both values on transforment app on: Configuration &gt; Base 'Twitter App' section (https://www.transformationtalkradio.com/admin/cms/configurations/base) and Click <code>Save</code></li>
</ul>
</li>
</ul>
</li>
</ul>
<p>** Social Media scheduled share activation ( host or administrator )  **</p>
<p>In order to enable scheduled share feature each host/admin must connect ( activate ) his account with their social media accounts:</p>
<ol>
<li>
<p>Logged in as admin/host choose <code>Social Media Accounts</code> from top right drop-down menu</p>
</li>
<li>
<p>Activate all required social media accounts clicking on <code>Connect</code> button; once ready <code>Connected</code> label will appear on Social Media service name.</p>
</li>
</ol>
<p>** Social Media scheduled share usage (host or admin) **</p>
<p>Episode scheduled share can be set by Host or Administrator user. At least one social media account must be connected (see section above).<br>
In order to schedule social media post in the future:</p>
<ol>
<li>Go to episode view page</li>
<li>Click on <code>Scheduled Share</code>, then <code>+ Add</code> button and schedule form will appear. Selecting target social services, post date and time and clicking <code>Save</code> creates new entry. When scheduled date and time will occur whole Epsiode or Episode segment will be posted on all selected social services.</li>
</ol>
<hr>
<p>Notices:</p>
<ul>
<li>Episode and their four segments can be scheduled to share on different social services and at different times each</li>
<li>Each admin or host / co-host assigned to Episode can schedule share on his different social services and at different times each (each admin/host/co-host can do this separately for Episode)</li>
</ul>
<p><br><center><a href="index">« back to index</a></center></p>