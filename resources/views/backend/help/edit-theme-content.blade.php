<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="channel-theme-custom-content-management">Channel theme custom content management</h1>
<hr>
<p><strong> Editing custom pages  </strong> 
<br>
Custom pages can be edited at: <b>Content &gt; Custom Pages</b>. 
There can be added any number of custom pages for each channel separatelly.
Each custom page is just html page with optinally php code.
<br>
<br></p>
<p><strong> Editing theme elements  </strong>
<br>
Custom elements can be edited at: <b>Content &gt; Elements</b>. 
Elements number can vary for different channel themes. For each channel there are: header, footer, home
Each custom page is just html page with optinally php code.
<br>
<br></p>
<p><strong> HTML elements available to place on above custom pages or layout elements  </strong>
<br></p>
<ol>
<li><p>Url to internal pages</p>
<pre><code> &lt;?php echo $this-&gt;Html-&gt;url(&#39;/url/path/to/some/pages&#39;); ?&gt;
</code></pre><p> and full link html code:        </p>
<pre><code> &lt;a href=&quot;&lt;?php echo $this-&gt;Html-&gt;url(&#39;/url/path/to/some/pages&#39;); ?&gt;&quot;&gt;link lable&lt;/a&gt;
</code></pre><p> <u>Examples:</u>
 <br>
 <br>
 <i>Url to guest page</i></p>
<pre><code> &lt;?php echo $this-&gt;Html-&gt;url(&#39;/guest/kathleen-adams,1868.html&#39;); ?&gt;
</code></pre><p> <i>Url to host lists</i></p>
<pre><code> &lt;?php echo $this-&gt;Html-&gt;url(&#39;/hosts&#39;); ?&gt;
</code></pre><p> <i>Url to custom page</i></p>
<pre><code> &lt;?php echo $this-&gt;Html-&gt;url(&#39;/about-us.html&#39;); ?&gt;
</code></pre></li>
<li><p>Url to extenal pages (just plain url)</p>
<pre><code> http://some-server.com
</code></pre></li>
<li><p>Internal images - all images to be used on custom pages or elements have to be uploaded by ftp first.<br>
All images should be placed at <b>/app/webroot/files/custom/</b> directly or on some subdirectory.
To place image on page there is need to use url:</p>
<pre><code> &lt;?php echo $this-&gt;Html-&gt;url(&#39;/files/custom/your-image-name.jpg&#39;); ?&gt;
</code></pre><p> and full image html code:        </p>
<pre><code> &lt;img src=&quot;&lt;?php echo $this-&gt;Html-&gt;url(&#39;/files/custom/your-image-name.jpg&#39;); ?&gt;&quot; /&gt;
</code></pre></li>
<li><p>External images (just plain url)</p>
<pre><code> http://some-server.com/images/other-image.jpg 
</code></pre><p> and full image html code:        </p>
<pre><code> &lt;img src=&quot;http://some-server.com/images/other-image.jpg&quot; /&gt;
</code></pre><p><br>
<br></p>
</li>
</ol>
<p><strong> SPECIAL ELEMENTS  </strong>
<br></p>
<p><br>
<br>
<br>
<strong> Banners </strong> - view one or more banners:</p>
<pre><code>    &lt;?php echo $this-&gt;Banners-&gt;view(size, count, parameters); ?&gt;
</code></pre><table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Example</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>size</em></td>
<td>banner size <em>(string)</em></td>
<td><code>120x300</code></td>
<td>Banner size defined as string. Available values: &#39;120x300&#39;, &#39;300x250&#39;, &#39;625x258&#39;, &#39;728x90&#39;</td>
</tr>
<tr>
<td><em>count</em></td>
<td>banners count <em>(int)</em></td>
<td><code>7</code></td>
<td>Number of banners to show</td>
</tr>
<tr>
<td><em>parameters</em></td>
<td>banners additional parameters <em>(array)</em></td>
<td><code>array(&#39;showtime&#39; =&gt; 10)</code></td>
<td>Additional parameters. Available values:<br><code>showtime</code> - banners rotator refresh time [s] ex. 60; set zero to no rotate<br><code>category</code> - category of banners to view; ex. 7<br><code>target</code> - target window to open page assigned to banner ex. &#39;_blank&#39; (open on new tab) or &#39;_self&#39; (open on this same tab)</td>
</tr>
</tbody>
</table>
<p><u>Examples:</u>
<br>
<br>
<i>Show single banner of size 728x90 with default refresh time 10[s]</i></p>
<pre><code>&lt;?php echo $this-&gt;Banners-&gt;view(&#39;728x90&#39;); ?&gt;
</code></pre><p><i>Show seven banners of size 120x300 form category id=95 and refresh interval 60 seconds</i></p>
<pre><code>&lt;?php echo $this-&gt;Banners-&gt;view(&#39;120x300&#39;, 7, array(&#39;showtime&#39; =&gt; 60, &#39;category&#39; =&gt; 95)); ?&gt;
</code></pre><p><i>Show single banner of size 300x250, never refresh and open banner target url on this same (current) window/tab</i></p>
<pre><code>&lt;?php echo $this-&gt;Banners-&gt;view(&#39;300x250&#39;, 1, array(&#39;showtime&#39; =&gt; 0, &#39;target&#39; =&gt; &#39;_self&#39;); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Zapbox </strong>:</p>
<pre><code>    &lt;?php echo $this-&gt;Zapbox-&gt;view(); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Player </strong>:</p>
<p><i>Live Stream Player with current show banner</i></p>
<pre><code>&lt;?php echo $this-&gt;Channel-&gt;playerLive(); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Featured Shows </strong>:</p>
<p><i>Featured shows horizontal scrollable box</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;featuredShows(); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Featured Guests </strong>:</p>
<p><i>Featured guests horizontal scrollable box</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;featuredGuests(); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Live On Air box </strong>:</p>
<p><i>Live On Air</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;liveOnAir(); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Coming up next box </strong>:</p>
<p><i>Coming up next</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;comingUpNext(); ?&gt;
</code></pre><p><br>
<br>
<br>
<br>
<strong> Giveaways </strong>:</p>
<p><i>Giveaways</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;giveaways(); ?&gt;
</code></pre><p><br><center><a href="index">&laquo; back to index</a></center></p>