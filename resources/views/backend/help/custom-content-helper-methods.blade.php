<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="custom-content-helper-methods">Custom Content Helper methods</h1>
<hr>
<p>All described below methods can be placed on any custom page or element using below syntax:</p>
<pre><code>    &lt;?php echo $this-&gt;Content-&gt;methodName(parameters); ?&gt;
</code></pre><p>where:<br></p>
<ul>
<li><i><b>methodName</b></i> is target method name<br></li>
<li><i><b>parameters</b></i> are comma separated method parameters. Parameters count and available values vary depending on used method.<br>
<br>
<br></li>
</ul>
<p><u>Examples:</u>
<br>
<br>
<i>View box with twenty elements of custom content ctegory of ID=1 ordered randomly</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;box(1, 20, array(&#39;order&#39; =&gt; &#39;random&#39;)); ?&gt;
</code></pre><p><br>
<br></p>
<h2 id="liveonair">liveOnAir</h2>
<p>View box with currently live show information (time, title, description, host photo)</p>
<pre><code>    &lt;?php echo $this-&gt;Content-&gt;liveOnAir($descLimit, $titleLimit, $imgHeight); ?&gt;
</code></pre><p><br>
<strong> Available parameters </strong></p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Example</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>$descLimit</em></td>
<td>show description characters count <em>(int)</em></td>
<td>218</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><em>$titleLimit</em></td>
<td>show title characters count <em>(int)</em></td>
<td>255</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><em>$imgHeight</em></td>
<td>host image height pixels <em>(int)</em></td>
<td>200</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p><br>
<strong> CSS class </strong></p>
<pre><code>    .live-on-air
</code></pre><p><br>
<br></p>
<h2 id="comingupnext">comingUpNext</h2>
<p>View box with coming up next show information (time, title, description, host photo)</p>
<pre><code>    &lt;?php echo $this-&gt;Content-&gt;comingUpNext($descLimit, $titleLimit, $imgHeight); ?&gt;
</code></pre><p><br>
<strong> Available parameters </strong></p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Example</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>$descLimit</em></td>
<td>show description characters count <em>(int)</em></td>
<td>218</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><em>$titleLimit</em></td>
<td>show title characters count <em>(int)</em></td>
<td>255</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><em>$imgHeight</em></td>
<td>host image height pixels <em>(int)</em></td>
<td>200</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p><br>
<strong> CSS class </strong></p>
<pre><code>    .coming-up-next
</code></pre><p><br>
<br>
<br><center><a href="index">&laquo; back to index</a></center></p>