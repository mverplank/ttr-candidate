<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="custom-files-placement">Custom files placement</h1>
<hr>
<p>Any custom file must be placed on one of below directories:</p>
<ol>
<li><p>Files used for any channel:   </p>
<pre><code> /app/webroot/files/custom
</code></pre><blockquote>
<p>Directory &#39;custom&#39; can posses any number of sub-directories or files.<br>
Example: /app/webroot/files/custom/<u>backgrounds/dotted.jpg</u></p>
</blockquote>
</li>
<li><p>Files used for single channel only:</p>
<pre><code> /app/View/Themed/{Short_channel_name}/webroot/files/custom
</code></pre><blockquote>
<p>{Short_channel_name} is short name of channel generated automatically once channel is created.<br>
It can be found on channel edit page ( Channels &gt; List &gt; Edit )<br>
Directory &#39;custom&#39; can posses any number of sub-directories or files.</p>
</blockquote>
</li>
</ol>
<p><br>
<br>
Files from above directories can be accessed using url: </p>
<pre><code>    http://channel-domain.com/files/custom/{custom-file-sub-directory}/{file-name.extension}
</code></pre><blockquote>
<p>Example: <a href="http://transformationtalkradio.com/files/custom/backgrounds/dotted.jpg">http://transformationtalkradio.com/files/custom/backgrounds/dotted.jpg</a><br><br>
Above url first checks for file existance on directory:<br>
<code>/app/View/Themed/{Short_channel_name}/webroot/files/custom</code><br><br>
if file exists - it is send to browser, if not found - below directory is checked:<br>
<code>/app/webroot/files/custom</code></p>
</blockquote>
<p><br><center><a href="index">&laquo; back to index</a></center></p>