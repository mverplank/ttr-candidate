<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="archived-episodes-rss-feed">Archived episodes RSS feed</h1>
<hr>
<p><strong> Base RSS (ver 2.0) feed url: </strong></p>
<pre><code>    http://channel-domain.com/feed/episodes/archived.rss
</code></pre><blockquote>
<p>Above url will return all channels/hosts/guests all archived episodes. If &quot;channel-domain.com&quot; is assinged to any channel - only episodes of that channel will be returned.</p>
</blockquote>
<p><br>
<strong> Available parameters </strong></p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Example</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>channel</em></td>
<td>channel identifier <em>(int)</em></td>
<td><code>?channel=3</code></td>
<td>Show only given channel episodes</td>
</tr>
<tr>
<td><em>host</em></td>
<td>host identifier <em>(int)</em></td>
<td><code>?host=12</code></td>
<td>Show only given host episodes</td>
</tr>
<tr>
<td><em>guest</em></td>
<td>guest identifier <em>(int)</em></td>
<td><code>?guest=73</code></td>
<td>Show only given guest episodes</td>
</tr>
<tr>
<td><em>sponsor</em></td>
<td>sponsor identifier <em>(int)</em></td>
<td><code>?sponsor=16</code></td>
<td>Show only episodes sponsored by this sponsor</td>
</tr>
<tr>
<td><em>limit</em></td>
<td>feed items limit value <em>(int)</em></td>
<td><code>?limit=50</code></td>
<td>Limit number of recent epsiodes to given count</td>
</tr>
</tbody>
</table>
<blockquote>
<p>To get channel or host identifier go to Channel or Host Edit page and check for shown ID value. </p>
</blockquote>
<p>Usage examples:</p>
<pre><code>    http://channel-domain.com/feed/episodes/archived.rss?channel=2
    http://channel-domain.com/feed/episodes/archived.rss?host=12
</code></pre><p>Parameters can be mixed - example:</p>
<pre><code>    http://channel-domain.com/feed/episodes/archived.rss?channel=2&amp;host=12&amp;limit=20
</code></pre><blockquote>
<p>Above url will return recent twenty archived episodes of host with id 12 published on channel with id 2 only</p>
</blockquote>
<p><br></p>
<h1 id="upcoming-episodes-rss-feed">Upcoming episodes RSS feed</h1>
<hr>
<p><strong> Base RSS (ver 2.0) feed url: </strong></p>
<pre><code>    http://channel-domain.com/feed/episodes/upcoming.rss
</code></pre><blockquote>
<p>Above url will return all channels/hosts/guests all upcoming episodes in next seven days. If &quot;channel-domain.com&quot; is assinged to any channel - only episodes of that channel will be returned.
<br>For available parameters - see above: &#39;Archived episodes RSS feed&#39;</p>
</blockquote>
<p><br> 
<br>
<br>
<br>
<strong> Uploading archived episodes </strong></p>
<p>Archived episodes can be uploaded via web-interface at: Shows &gt; Archive &gt; Edit Episode &gt; Media section,
<br>or direclty via FTP to directory:</p>
<pre><code>        /app/webroot/files/Episode/{EpisodeID}/media/
</code></pre><p>File name can be any. {EpisodeID} is viewed on Edit Episode page. </p>
<p><br><center><a href="index">&laquo; back to index</a></center></p>