<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="custom-content-categories-management">Custom Content Categories Management</h1>
<hr>
<p><strong> Editing custom content categories (Admin) </strong> 
<br>
Custom content categories can be edited at: <b>Content &gt; Content Categories</b>. 
There can be added any number of custom content categories.<br>
To add new content category use &quot;Add New Category&quot; button.<br>
To edit content category use &quot;Edit Category&quot; button on category row.<br>
On &quot;Edit Content Category&quot; page category can be assigned to partiular channels. If category is assigned to channel
 there can be added entires ONLY for this particular channel (shown only on this channel website, not on others).
Such items can be entered at: <b>Channel &gt; List </b> channel&#39;s row &quot;Edit website contents&quot; button.</p>
<p><br>
<br></p>
<hr>
<p><strong> Placing custom categories on front-end </strong> 
<br>
1.) link to custom content category index:</p>
<pre><code>&lt;?php echo $this-&gt;Html-&gt;url(&#39;/{any-custom-text-label},{contentCategoryID}.html&#39;); ?&gt;
</code></pre><p>where:<br></p>
<ul>
<li><i>any-custom-text-label</i> is any text label; ususally it should be category plural 
name plus some additional SEO related words; ex. &quot;books-about-happy-life&quot;<br></li>
<li><i>contentCategoryID</i> is content category ID - it can be read on Content Category Edit page
<br>
<br></li>
</ul>
<p><u>Examples:</u>
<br>
<br>
<i>Url to books category index</i></p>
<pre><code>&lt;?php echo $this-&gt;Html-&gt;url(&#39;/books,7.html&#39;); ?&gt;
</code></pre><p><i>Url to category index + SEO related keywords in url</i></p>
<pre><code>&lt;?php echo $this-&gt;Html-&gt;url(&#39;/some-seo-related-expression,123.html&#39;); ?&gt;    
</code></pre><p><br>
<br>
2.) link to custom content particular item view page:</p>
<pre><code>&lt;?php echo $this-&gt;Html-&gt;url(&#39;/{any-custom-text-label-1}, {any-custom-text-label-2},{contentDataID}.html&#39;); ?&gt;
</code></pre><p>where:<br></p>
<ul>
<li><i>any-custom-text-label-1</i> is any text label; ususally it should be category singular 
name plus some additional SEO related words; ex. &quot;book&quot;<br></li>
<li><i>any-custom-text-label-2</i> is any text label; ususally related to particular item; ex. &quot;book-about-happy-life&quot;<br></li>
<li><i>contentDataID</i> is content data ID - it can be read on Content Data Edit page
<br>
<br></li>
</ul>
<p><u>Examples:</u>
<br>
<br>
<i>Url to particualr book view page</i></p>
<pre><code>&lt;?php echo $this-&gt;Html-&gt;url(&#39;/book,about-happy-life,1234.html&#39;); ?&gt;
</code></pre><p><br>
<br>
3.) Nested horizontal scrolled box :</p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;box(ContentCategoryID, ItemsNumber); ?&gt; 
</code></pre><p>where:<br></p>
<ul>
<li><i>ContentCategoryID</i> is content category ID - it can be read on Content Category Edit page</li>
<li><i>ItemsNumber</i> is number of items to be shown; 
<br>
<br></li>
</ul>
<p><u>Examples:</u>
<br>
<br>
<i>Show horizontal scrollable box of ten items form category seven</i></p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;box(7, 10); ?&gt;
</code></pre><p><i>Full page width content box with h1 header</i></p>
<pre><code>&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;small-12 columns&quot;&gt;
        &lt;h1&gt;&lt;?php echo $ContentCategory[7][&#39;name&#39;]; ?&gt;&lt;/h1&gt;
        &lt;div class=&quot;box bg no-padding&quot;&gt;
            &lt;div class=&quot;row&quot;&gt;
                &lt;div class=&quot;small-12 columns&quot;&gt;
                    &lt;?php echo $this-&gt;Content-&gt;box(7, 10); ?&gt;                                
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;
</code></pre><p><br>
<br>
4.) Images slider :</p>
<pre><code>&lt;?php echo $this-&gt;Content-&gt;slider(contentCategroyId, ItemsNumber [, Parameters]); ?&gt;  
</code></pre><p>where:<br> </p>
<ul>
<li><i>ContentCategoryID</i> is content category ID; it can be read on particular content category edit page</li>
<li><i>ItemsNumber</i> is number of category items to be shown on slider; </li>
<li><i>Parameters</i> is array of custom parameters:
<br></li>
</ul>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Example</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>title</em></td>
<td>true/false <em>(bool)</em></td>
<td><code>true</code></td>
<td>If true show category items titles on image bottom</td>
</tr>
<tr>
<td><em>width</em></td>
<td>image width in pixels <em>(int)</em></td>
<td><code>800</code></td>
<td>Target image width</td>
</tr>
<tr>
<td><em>height</em></td>
<td>image height in pixels <em>(int)</em></td>
<td><code>600</code></td>
<td>Target image height</td>
</tr>
<tr>
<td><em>class</em></td>
<td>css class name <em>(string)</em></td>
<td><code>news</code></td>
<td>Additional css class name for slider</td>
</tr>
<tr>
<td><em>flexslider</em></td>
<td>flexslider widget configuration <em>(array)</em></td>
<td><code>array(&#39;animation&#39; =&gt; &#39;fade&#39;)</code></td>
<td>Flexslider widget configuration array; all options available: <a href="https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties" target="_blank">here</a></td>
</tr>
</tbody>
</table>
<p>Flexslider base options:<br> </p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Example</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>animation</em></td>
<td>[ slide or fade ] <em>(string)</em></td>
<td><code>slide</code></td>
<td>Animation type</td>
</tr>
<tr>
<td><em>direction</em></td>
<td>[ horizontal or vertical ] <em>(string)</em></td>
<td><code>horizontal</code></td>
<td>Animation direction</td>
</tr>
<tr>
<td><em>reverse</em></td>
<td>[ true or false ] <em>(bool)</em></td>
<td><code>true</code></td>
<td>Reverse animation direction</td>
</tr>
<tr>
<td><em>animationSpeed</em></td>
<td>speed [ms] <em>(int)</em></td>
<td><code>1000</code></td>
<td>Animation speed (1000[ms] = 1[s])</td>
</tr>
<tr>
<td><em>slideshowSpeed</em></td>
<td>speed [ms] <em>(string)</em></td>
<td><code>5000</code></td>
<td>Slideshow speed (1000[ms] = 1[s])</td>
</tr>
<tr>
<td><em>slideshow</em></td>
<td>[ true or false ] <em>(bool)</em></td>
<td><code>false</code></td>
<td>Start slideshow on slider view</td>
</tr>
</tbody>
</table>
<p>NOTE:<br>
Slider width is 100% of parent element width. To set right slider width just set right parent element width or grid size.
The shown title and sub-title are title and sub-title defined for particular content category item. </p>
<p><br>
<br></p>
<p><u>Examples:</u> 
<br>
<br> 
<i>Show slider of five images with size forced to 640x280 px from category id=7 ordered randomly and not showing title </i> </p>
<pre><code>&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;small-6 columns&quot;&gt;
        &lt;?php echo $this-&gt;Content-&gt;slider(7, 5, array(&#39;order&#39; =&gt; &#39;random&#39;, &#39;title&#39; =&gt; false, &#39;width&#39; =&gt; 160, &#39;height&#39; =&gt; 90)); ?&gt;
    &lt;/div&gt;
&lt;/div&gt;
</code></pre><p><i>Show slider of ten images from category id=6 ordered randomly and wifth fade animation </i> </p>
<pre><code>&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;small-6 columns&quot;&gt;
        &lt;?php echo $this-&gt;Content-&gt;slider(6, 10, array(&#39;order&#39; =&gt; &#39;random&#39;, &#39;flexslider&#39; =&gt; array(&#39;animation&#39; =&gt; &#39;fade&#39;))); ?&gt;
    &lt;/div&gt;
&lt;/div&gt;
</code></pre><p><br>
<br>
<br>
<br></p>
<hr>
<p><strong> Using custom front-end templates </strong>
<br>
By default custom content items are presented using default tempaltes, however this can be customized.<br>
Each category custom list, view pages and row, box elements can be defined on Category Edit page section &#39;Front-end Layout&#39;.
To activate custom tempalte of any type switch must be set ot ON. Then html editor appears 
where default html code can be modified to meet requirements.<br>
Customized html templates will be used automatically. The default templates can be restored switching On/Off switch to OFF.</p>
<p><br>
<br></p>
<p><br><center><a href="index">&laquo; back to index</a></center></p>