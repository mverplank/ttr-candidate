<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">« back to index</a></p>
<h1 id="architecture">Architecture</h1>
<ul>
<li>
<p><strong>Server</strong></p>
<p>OS: Linux (prefered Ubuntu)<br>
HTTP: Apache with PHP 5.3+<br>
DB: MySQL<br>
PHP modules: pdo, curl, date, gd</p>
</li>
<li>
<p><strong>Back end</strong></p>
<p><a href="http://cakephp.org/">CakePHP</a> ver 2.4.1</p>
</li>
<li>
<p><strong>Front end</strong></p>
<p>Pre-processors: [PUG] (jade) (<a href="https://pugjs.org">https://pugjs.org</a>), <a href="http://sass-lang.com/">Sass</a><br>
Style libraries: <a href="http://compass-style.org/">Compass</a>, <a href="http://foundation.zurb.com/">Zurb foundation</a></p>
</li>
<li>
<p><strong>Development</strong></p>
<p><a href="https://git-scm.com/">GIT</a></p>
<p><a href="https://nodejs.org/en/">nodejs</a></p>
<p><a href="http://gulpjs.com/">Gulp</a>  / <a href="http://gruntjs.com/">Grunt</a></p>
<p><a href="https://www.openssl.org/">OpenSSL</a></p>
<p><a href="https://www.mysql.com/products/workbench/">MySQL workbench</a></p>
</li>
</ul>
<h1 id="introduction">Introduction</h1>
<ul>
<li>
<p>All code changes must be maintained using git</p>
</li>
<li>
<p>All database changes must be made using MySQL workbench (on EER diagram)</p>
</li>
<li>
<p>Directories structure:</p>
<p>Base code structure is equal to CakePHP directories structure and follows MVC convention. Development source code is placed at: <code>/app_dev</code> directory.
All modules are placed at <code>/app_dev/Plugin</code> directory.
<s>Transformnet application handles multiple domains and each domain has it's own theme;
all themes files are located at <code>app_dev/View/Themed/{THEME_NAME}</code> where theme name is related to domain name
(ex. files specific for <a href="http://transformationtalkradio.com">transformationtalkradio.com</a> are located on &quot;Transformationtalkradio&quot; sub-directory)</s></p>
</li>
<li>
<p>Themes/domains styles</p>
<p>Layout and some styles used are based on Zurb foundation framework and defined with sass.
For admin theme main sass file is located at:</p>
<pre><code>app_dev/View/Themed/Admin/webroot/scss/theme.scss
</code></pre>
<p>Each channel domain has it's own theme assigned.<br>
Custom Theme can be assinged to channel at <a href="https://www.transformationtalkradio.com/admin/channel/channels"><code>Channel</code> &gt; <code>List</code> &gt; <code>Edit</code></a> page.<br>
All custom themes can be edited on admin back-end at <a href="https://www.transformationtalkradio.com/admin/cms/themes"><code>Content</code> &gt; <code>Themes</code></a>.<br>
All custom themes are generated using base, default template. To edit this default template follow instructions described on <em>Default Template Development</em> chapter.</p>
</li>
<li>
<p>Database structure (model diagram) can be viewed using MySql Workbench application:</p>
<pre><code>  doc/db/database_transformnet-2.mwb
</code></pre>
</li>
<li>
<p>Other:</p>
<ul>
<li>For most ctp (html) files jade template files are used (equal named as .ctp file). Those jade files should be edited instead of ctp files. (ctp files will be grenerated by gulp/grunt)</li>
</ul>
</li>
</ul>
<h1 id="development">Development</h1>
<p><strong>first time only</strong></p>
<ol>
<li>
<p>clone git repository</p>
<pre><code> git clone ssh://thedrpatshow@107.180.78.124/home/thedrpatshow/public_html/transformationradio.git ./
</code></pre>
</li>
<li>
<p>install required grunt nodejs modules</p>
<pre><code> npm install
</code></pre>
</li>
<li>
<p>Use openSSL to generate self-signed ssl certificate files, name them as: &quot;ssl-cert.crt&quot; and &quot;ssl-cert.key&quot;
and place it directory above directory where source code is placed (one level up)</p>
<pre><code> openssl genrsa -out server.key 1024
 openssl req -nodes -new -key server.key -out server.csr
 openssl x509 -sha256 -req -days 365 -in server.csr -signkey server.key -out server.crt
</code></pre>
</li>
<li>
<p>Using MySQL Workbench create database structure (model file location below) or copy from existing database:</p>
<pre><code> doc/db/database_transformnet-2.mwb  
</code></pre>
</li>
</ol>
<p>Once db structure created database should be filled with data exported from existing db tables from server.<br>
If prefered MySQL php admin can be used:  <a href="https://www.transformationtalkradio.com/app_dev/deploy/adminer.php">https://www.transformationtalkradio.com/app_dev/deploy/adminer.php</a></p>
<ol start="5">
<li>Application uses domain name to detect which channel theme should be used and therefore app on development enviroment should be accessed via domain name, not IP. All domains must point out to this same IP and <code>/app_dev/webroot</code> directory.<br>
Below is sample configuration:</li>
</ol>
<ul>
<li>
<p>on <code>etc/hosts</code> file (for each domain):<br>
192.168.1.2 transformationtalkradio.dev
...</p>
</li>
<li>
<p>on Apache configuration files (for each domain):</p>
<pre><code>  &lt;VirtualHost 192.168.1.2:80&gt;

     ServerName transformationtalkradio.dev
     ServerAlias transformationtalkradio.dev
     DocumentRoot /var/www/html/transformnet/app_dev/webroot
     &lt;Directory /&gt;
      AllowOverride All
     &lt;/Directory&gt;
     &lt;Directory /var/www/html/transformnet/app_dev/webroot&gt;
      Options Indexes FollowSymLinks MultiViews
      AllowOverride all
      Require all granted
     &lt;/Directory&gt;

   &lt;/VirtualHost&gt;
   ...
</code></pre>
</li>
</ul>
<ol start="6">
<li>
<p>Custom directories and files (not included on git) have to be created/copied from live website:</p>
<pre><code> /app_dev/View/Themed/ &lt;ThemeNameGoesHere&gt; /Cms   (Note: all themes directories)
 app/Plugin/Cms/View/Emails
 app/Plugin/Cms/View/Layouts
 app/webroot/files/custom
 app/webroot/files  (Optional; only some sub-directories and files should be copied (if required) as directory contains mp3 files and can be very large size)
</code></pre>
</li>
</ol>
<p><strong>on each development turn</strong></p>
<ol>
<li>
<p>get recent changes from git repository</p>
<pre><code> git pull origin master
</code></pre>
</li>
<li>
<p>run grunt &quot;watch&quot; task</p>
<pre><code> grunt esteWatch
</code></pre>
</li>
<li>
<p>All required to watch code files will be now processed by grunt (scss files wil be compiled to css, jade to ctp, js files will be concentrated ...)</p>
</li>
<li>
<p>Database changes must be made made using MySQL Workbench - database model file location:</p>
<pre><code> doc/db/database_transformnet-2.mwb  
</code></pre>
</li>
<li>
<p>Once finished commit changes or commit and push into server git repository:</p>
<pre><code> git add .
 git commit -m &quot;Description for recent set of changes&quot;

 git push origin
</code></pre>
</li>
</ol>
<h1 id="default-template-development">Default Template development</h1>
<p>Default Template is set of base html elements, css styles and java scripts used as base when generating each theme.<br>
All <em>Default</em> template related files are located at:</p>
<pre><code>/app_dev/Plugin/Cms/View/Templates/Default  
</code></pre>
<p>To edit template follow below instructions:</p>
<ol>
<li>
<p>Go to template directory: <code>/app_dev/Plugin/Cms/View/Templates/Default</code></p>
</li>
<li>
<p>Run:<br>
<code>gulp</code> (pug/js/scss files will be processed once changed and right html/js/css files will be generated)<br>
OR<br>
<code>gulp serve</code> (as above, but browser is auto-refreshed once any file changed)</p>
</li>
<li>
<p>Edited template with selected theme styles and layout (<em>transformationtalkradio</em> as example) can be accessed via below urls:<br>
<a href="http://transformationtalkradio.dev?dev">http://transformationtalkradio.dev?dev</a> if template based theme <em>transformationtalkradio</em> is enabled by default on <em>transformationtalkradio.dev</em><br>
<a href="http://transformationtalkradio.dev?dev&amp;theme=transformationtalkradio">http://transformationtalkradio.dev?dev&amp;theme=transformationtalkradio</a> to force use of <em>transformationtalkradio</em> theme based on template<br>
<a href="http://localhost:3000/?dev">http://localhost:3000/?dev</a> when using <code>gulp serve</code><br>
Note: Any page can be viewed using above urls; only proper url parameters have to be applied (example: <a href="http://localhost:3000/hosts?dev">http://localhost:3000/hosts?dev</a>)</p>
</li>
<li>
<p>All template styles definitions are located at <code>styles</code> sub-directory. New scss (sass) files should be placed on one of sub-directories: <code>styles/components</code>, <code>styles/partials</code> or <code>styles/pages</code>. Then should be imported on <code>styles/theme-stylesheet.scss</code> (to be included on css file at body end) or <code>styles/theme-inline.scss</code> (to be included on header styles)</p>
</li>
<li>
<p>All template scripts are located at <code>scripts</code> sub-directory. To add new javascript file it should be first copied to proper sub-directory (<code>scripts/libs</code>, <code>scripts/plugins</code>) and then added to <code>scripts/_include.body.json</code> (to be included on body end) or <code>scripts/_include.head.json</code> (to be included on header end) definitions files.<br>
Note: on above definition files order of js files is important.</p>
</li>
<li>
<p>Commonly used pug/html elements are located at: <code>elements</code> sub-directory. Any new elements should be placed here.</p>
</li>
<li>
<p>Each theme must be re-generated to apply changes made on template. This can be performed on  admin account main menu: Content &gt; Themes &gt; Edit &gt; 'Save &amp; generate theme' or on theme editor using 'Generate' button.<br>
To re-generate all themes can be used controller action: <code>/admin/cms/themes/generate_all_themes</code> (must be logged-in as admin)</p>
</li>
</ol>
<h1 id="deployment">Deployment</h1>
<p>Notices:</p>
<ul>
<li>Path to application on server: <code>/home/thedrpatshow/public_html/transformationradio.fm</code></li>
<li>On server there are two application instances: development (located at: <code>/app_dev</code>)  and production (located: <code>/app</code>)</li>
<li>Development version is tied with git repository; All files changes pushed by git are propagated to this directory files</li>
<li>To deploy recent file changes on production version deploy script must be used</li>
</ul>
<p>Procedure:</p>
<ol>
<li>
<p>Once recent files changes has been pushed via git to development version it can be used to preview / test modifications and new functionalities.</p>
</li>
<li>
<p>Once accepted changes can be deployed to production version using below script:</p>
<p><a href="https://www.transformationtalkradio.com/app_dev/deploy/do.php">https://www.transformationtalkradio.com/app_dev/deploy/do.php</a></p>
</li>
</ol>
<p>(Optional) To make application website online / offline (disabled access and shows maintenance message) below script can be used:</p>
<pre><code>  https://www.transformationtalkradio.com/app_dev/deploy/mainteance.php
</code></pre>
<p>Deploymnet issues &amp; solutions:</p>
<ul>
<li>
<p>after deploy script run website still shows maintenace message:</p>
<ul>
<li>run mainteance script (<a href="https://www.transformationtalkradio.com/app_dev/deploy/mainteance.php">https://www.transformationtalkradio.com/app_dev/deploy/mainteance.php</a>); &quot;set production server status&quot; value to online and hit update</li>
</ul>
</li>
<li>
<p>after deploy website shows often internal errors (5xx) or some pages are not working properly:</p>
<ul>
<li>clear cache manually using url: <a href="https://www.transformationtalkradio.com/tools/clear_cache">https://www.transformationtalkradio.com/tools/clear_cache</a></li>
</ul>
</li>
<li>
<p>git push don't update development version source code (at: /app_dev):</p>
<ul>
<li>run post-receive hook script in git directory:
<ol>
<li>ssh login to server</li>
<li>go to: cd ~/www/transformationradio.fm.git/hooks</li>
<li>run: ./post-receive</li>
</ol>
</li>
</ul>
</li>
</ul>
<p><br><center><a href="index">&laquo; back to index</a></center></p>