<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="sam-broadcaster-integration">SAM Broadcaster integration</h1>
<hr>
<p>Connection between this application and SAM Broadcaster is made via PAL script 
available to download at Channels &gt; Edit channel page. 
PAL script only has one function: on right day of week and time it adds at the top
 of queue right show media file, fade out current playback and starts playback of target show.</p>
<p><br></p>
<p><strong> Channel and SAM Broadcaster configuration  </strong></p>
<ol>
<li>Go to channel &quot;Add&quot; or &quot;Edit&quot; page to section &quot;SAM Broadcaster&quot;</li>
<li><p>Set value for &quot;Show files directory&quot;<br></p>
<blockquote>
<p>This value should be full path (disk drive letter, directories path with backslash at the end)
 to directory where all media files to be broadcasted are placed.<br>
Example: &quot;D:\ChannelName\broadcasts\&quot;<br></p>
</blockquote>
</li>
<li><p>Set value for &quot;Minimum show duration [min]&quot;</p>
<blockquote>
<p>This value defines the minimum duration of any show for this channel. 
Any show duration can be multiplicity of this value, but not less. 
This value is also used by PAL script to set how often check schedule (connect to this application)</p>
</blockquote>
</li>
<li><p>Hit Update (or Add) button and download PAL script</p>
</li>
<li><p>Downloaded PAL script file add to SAM Broadcaster PAL scripts section (+ &quot;Add new PAL script&quot;)
and &quot;Start selected PAL script&quot;</p>
</li>
</ol>
<p><br></p>
<p><strong>  Sources of shows </strong></p>
<p>Available media sources for shows are as follow:<br></p>
<ul>
<li><p><b>Local media file</b> - mp3 files placed on machine where SAM broadcaster is installed.<br>
Files can be placed on defult directory defined for channel (&quot;Show files directory&quot;) or on any other directory.
Examples:</p>
<pre><code>      D:\ChannelName\broadcasts\stream.2014-07-17.143000.mp3

      E:\BestOfTheBest\HostName\Some-custom-title.mp3
</code></pre></li>
<li><p><b>Remote static media file</b> - mp3 file located on any server. Example:</p>
<pre><code>      http://channel-name.com/files/Episode/132/Custom-title.mp3
</code></pre></li>
<li><p><b>Media stream</b> - media stream url. Example:</p>
<pre><code>       http://205.188.234.38:8002
</code></pre></li>
</ul>
<p><br>
All above media sources can be defiend for three levels:<br></p>
<ul>
<li><p><b>Episode</b> (single, unique show scheduled for given channel, date and time)</p>
</li>
<li><p><b>Scheduled show</b> (show scheduled for given channel, day of week and time; it can repeats weekly for single channel)</p>
</li>
<li><p><b>Show</b> (used for one or more channels, one or more days of week and times; it can repeats many times for many channels)</p>
</li>
</ul>
<p><br></p>
<p>Once PAL script attempts to broadcast show it search for media source in below order:<br></p>
<ol>
<li><p>Local file located on channel&#39;s &quot;Show files directory&quot;</p>
<blockquote>
<p>Each mp3 media file placed inside this directory must match pattern:<br> </p>
<pre><code>       stream.YYYY-MM-DD.HHMM00.mp3
</code></pre><p>where:<br>
<i>YYYY</i> is four digits year; ex. 2014<br>
<i>MM</i> is two digits month; ex. 07<br>
<i>DD</i> is two digits day; ex. 17<br>
<i>HH</i> is two digit hour in 24 hours format; ex 14<br>
<i>MM</i> is two digit minutes; ex 30<br>
<br>(example: <i>stream.2014-07-17.143000.mp3</i>    &nbsp;&nbsp;=&gt;&nbsp;&nbsp;    2:30 pm 17th July 2014 ) </p>
</blockquote>
</li>
<li><p>Episode remote static media file or media stream</p>
<blockquote>
<p>Can be set on Shows &gt; Add/Edit Eipsode page &gt; &quot;Remote file or stream&quot;
It must be url (ex. <a href="http://channel-name.com/files/Episode/132/Custom-title.mp3">http://channel-name.com/files/Episode/132/Custom-title.mp3</a>) </p>
</blockquote>
</li>
<li><p>Episode local media file</p>
<blockquote>
<p>Can be set on Shows &gt; Add/Edit Eipsode page &gt; &quot;Local filename&quot;
It can be custom local directory/file (ex. E:\BestOfTheBest\HostName\Some-custom-title.mp3)</p>
</blockquote>
</li>
<li><p>Scheduled show remote static media file or media stream</p>
<blockquote>
<p>Can be set on Channel Schedule &gt; Add/Edit Show page &gt; &quot;Remote file or stream&quot;
It must be url (ex. <a href="http://channel-name.com/files/Episode/132/Custom-title.mp3">http://channel-name.com/files/Episode/132/Custom-title.mp3</a>) </p>
</blockquote>
</li>
<li><p>Scheduled show local media file</p>
<blockquote>
<p>Can be set on Channel Schedule &gt; Add/Edit Show page &gt; &quot;Local filename&quot;
It can be custom local directory/file (ex. E:\BestOfTheBest\HostName\Some-custom-title.mp3)</p>
</blockquote>
</li>
<li><p>Show remote static media file or media stream</p>
<blockquote>
<p>Can be set on Shows &gt; Add/Edit Show page &gt; &quot;Remote file or stream&quot;
It must be url (ex. <a href="http://channel-name.com/files/Episode/132/Custom-title.mp3">http://channel-name.com/files/Episode/132/Custom-title.mp3</a>) </p>
</blockquote>
</li>
<li><p>Show local media file</p>
<blockquote>
<p>Can be set on Shows &gt; Add/Edit Show page &gt; &quot;Local filename&quot;
It can be custom local directory/file (ex. E:\BestOfTheBest\HostName\Some-custom-title.mp3)</p>
</blockquote>
</li>
</ol>
<p>NOTE: PAL script ALWAYS adds to queue and starts playback remote static file or stream. 
Local file is only queued if exisits. If file don&#39;t defiend or don&#39;t exstits script just do nothing.
<br>
<br>
The simplest use case is:<br></p>
<ul>
<li>define Channel Schedule (assign shows to days of weeks and start times)</li>
<li>for future shows other than &quot;live&quot; put mp3 files named: <i>stream.YYYY-MM-DD.HHMM00.mp3</i> in defined &quot;Show files directory&quot;.</li>
<li>for shows that are re-broadcasted set &quot;Remote file or stream&quot; stream urls on Channel &gt; Schedule &gt; Show Add/Edit page.</li>
</ul>
<p><br></p>
<p><strong>  Using soruces of shows - examples</strong></p>
<blockquote>
<p>Channel Show files directory is: &quot;<i>D:\ChannelName\broadcasts\</i>&quot;<br>
Date/time is: 2014-07-17 14:30</p>
</blockquote>
<ul>
<li><p>Show scheduled for 2014-07-17 14:30, Episode Local file is defined: <i>D:\ChannelName\140\episode.mp3</i> and exists<br>
Result:<br> File <i>episode.mp3</i> is broadcasted</p>
</li>
<li><p>Show scheduled for 2014-07-17 14:30, Episode Local file is defined: <i>D:\ChannelName\140\episode.mp3</i> and exists, 
Scheduled Show file is defined: <i>D:\ChannelName\Sheduled\Wed\each-wed.mp3</i> and exists
, Show file is defined: <i>D:\ChannelName\Show\Name\default-show.mp3</i> and exists
<br>
Result:<br> File <i>episode.mp3</i> is broadcasted</p>
</li>
<li><p>Show scheduled for 2014-07-17 14:30, Episode Local file is defined: <i>D:\ChannelName\140\episode.mp3</i> and DON&#39;T exists, 
Scheduled Show file is defined: <i>D:\ChannelName\Show\Wed\each-wed.mp3</i> and exists<br>
Result:<br> File <i>each-wed.mp3</i> is broadcasted</p>
</li>
<li><p>Show scheduled for 2014-07-17 14:30, show type is &quot;Live&quot; and file is defined: <i>D:\ChannelName\broadcasts\2014-07-17.143000.mp3</i> and exists <br>
Result:<br> File <i>2014-07-17.143000.mp3</i> is broadcasted</p>
</li>
</ul>
<p><br><center><a href="index">&laquo; back to index</a></center></p>