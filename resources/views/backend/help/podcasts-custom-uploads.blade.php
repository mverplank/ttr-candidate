<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">« back to index</a></p>
<h1 id="podcasts-custom-uploads">Podcasts Custom Uploads</h1>
<hr>
<h2 id="podcast-host-members">Podcast-host members</h2>
<ol>
<li>
<p>Assign channel for uploaded podcasts (existing or create new channel)
at: <strong>Configuration &gt; Base
[Other]</strong> <code>Custom podcasts channel ID</code>; once assigned it should be not changed as all uploaded podcasts are tied with this channel.</p>
<p>NOTE: If new channel created also new theme has to be created and assigned to this new created channel.</p>
</li>
<li>
<p>Create required memberships</p>
<p>at: <strong>Configuration &gt; Memberships</strong>;<br>
New memberships have to be <code>podcast host type</code>; there can be any number of memberships and
each membership can posses any number of paid or free payment periods and weekly uploads defined.</p>
</li>
<li>
<p>Customize podcast user
sign-up page<br>
at: <strong>Content &gt; Pages:</strong> <code>podcast-host-signup</code>; it an be customized as any other custom page - the only requirement
is to place &quot;Membership Signup Form&quot; widget which generates signup form. (NOTE: to signup as new podcast-host user you can't
be logged-in because membership will be assigned to currently logged-in user) URL: <a href="https://www.transformationtalkradio.com/podcast-host-signup.html">https://www.transformationtalkradio.com/podcast-host-signup.html</a></p>
</li>
<li>
<p>Podcast-host after signup (if paid membership - after paypal payment) is automatically logged-in to their account and is
requested to fill host profile details and show details. Once ready podcast-host is able to upload new podcasts.</p>
</li>
<li>
<p>Podcast-host
can extend/upgrade membership using &quot;My account&quot; on top right user dropdown-menu. When extending this same membership - selected
period days are added to current expire date; when upgrading/downgrading selected period days are applied from current day.
Free membership can be selected only once.</p>
</li>
</ol>
<p>Podcast-host member can upload defined in membership number of podcasts per week.
This number can be changed by admin on host edit page, but (this changed value) will be valid till end of the membership.
If user extend/change membership value set by admin will be overwritten. Podcast-hosts, podcast-host shows and all uploaded
podcasts will be available on &quot;podcasts-channel&quot; website and related rss feeds. To include particular podcast-host and their
podcasts on existing channels (other than &quot;podcasts channel&quot;) it is required to assign it to right channel(s) on host edit
page (by admin).</p>
<h2 id="custom-uploaded-podcasts-for-existing-hosts">Custom uploaded podcasts for (existing) hosts</h2>
<p>To allow podcasts upload for host/co-host on host edit page (as admin):</p>
<ol>
<li>switch on &quot;podcast-host&quot; in addition to &quot;host&quot; and/or &quot;co-host&quot;</li>
<li>on section <strong>Membership / Podcasts Host settings</strong> set value
for &quot;Max weekly uploads&quot;; (three by default)</li>
<li>on section <strong>Membership / Podcasts Host settings</strong> set value
for &quot;Membership End&quot;; (no expiration by default)</li>
<li>(optional) on section <strong>Settings / Channels</strong> assign host to podcasts channel (if required to view such host uploaded podcasts on uploaded podcasts channels).</li>
</ol>
<p>Once set host/co-host
will be available to upload custom podcasts (outside scheduled shows). Podcasts can be uploaded for any show host is assigned
to. Maximum weekly uploads apply to all shows together (ex. three different shows or three this same show episodes can be
uploaded).<br>
Such defined <code>host</code> and also <code>podcast-host</code> can't add/edit their custom show (unlike podcast-host ONLY member).</p>
<h2 id="front-end-for-custom-uploaded-podcasts">Front-end for custom uploaded podcasts</h2>
<p>Channel where uploaded podcasts will be shown (on archives, upcoming listings) must be assigned as podcasts channel (at <strong>Configuration &gt; Base
[Other]</strong> <code>Custom podcasts channel ID</code>). On this channel theme/pages editor all widgets can be used and two data source related to uploaded podcasts are present: <code>TTR Recent uploaded podcasts</code> and <code>TTR Upcoming uploaded podcasts</code>. Both can be used as data source for listings, sliders, carousels and other widgets where data source is available.</p>
<p><br><center><a href="index">« back to index</a></center></p>