<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="facebook-login-configuration">Facebook login configuration</h1>
<hr>
<p><strong> Facebook account </strong></p>
<p>1.) Login to your Facebook account at: <a href="https://www.facebook.com/" target="_blank">https://www.facebook.com/</a>                    </p>
<p>2.) Go to: <a href="https://developers.facebook.com/apps">https://developers.facebook.com/apps</a></p>
<p>3.) Click on &#39;<i>Add a New App</i>&#39; and choose &#39;<i>website</i>&#39;</p>
<p>4.) Enter website App name (any custom name) and click on &#39;<i>Create New Facebook App ID</i>&#39; and then on popup confirm button &#39;<i>Create App ID</i>&#39;</p>
<p>5.) Click on &#39;<i>Skip Quick Start</i>&#39; on page top and application &#39;<i>Dashboard</i>&#39; page will appear</p>
<p>6.) Copy &#39;<i>App ID</i>&#39; and &#39;<i>App Secret</i>&#39; values</p>
<p><br>
<br>
<br>
<strong> Your web application </strong></p>
<p>1.) Login as administrator user (admin) on: <a href="http://yoursite.com/login">http://yoursite.com/login</a></p>
<p>2.) Go to: &#39;<i>Configuration</i>&#39; &raquo; &#39;<i>Base</i>&#39;</p>
<p>3.) On section &#39;<i>Facebook login</i>&#39; enter your <i>Facebook App ID</i> and <i>Facebook App Secret</i> values.</p>
<p><br><center><a href="index">&laquo; back to index</a></center></p>