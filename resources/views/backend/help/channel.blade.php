<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>
<p><a href="index">&laquo; back to index</a> </p>
<h1 id="new-channel-creation-process">New channel creation process</h1>
<hr>
<ol>
<li><p>Add new channel (Menu: Channels &gt; List &gt; &quot;Add New Channel&quot;)</p>
</li>
<li><p>Add new hosts (Menu: Users &gt; Hosts &gt; &quot;Add Host&quot;) or edit exisitng. <br>To assign host to new channel use checkboxes on &quot;Settings&quot; section.</p>
</li>
<li><p>Add new shows (Menu: Shows &gt; Shows &gt; &quot;Add Show&quot;) or edit exisitng. <br>To assign show to new channel use checkboxes on &quot;Settings&quot; section.</p>
</li>
<li><p>Create channel schedule (Menu: Channels &gt; Schedule) <br> To add new show to schedule click on calendar time slot or use &quot;Add Show&quot; button.</p>
</li>
<li><p>Add Episodes for shows (Menu: Shows &gt; Schedule) <br> To add new episode click on show time slot and &quot;Add Episode&quot; page will appear. </p>
</li>
</ol>
<p><br><center><a href="index">&laquo; back to index</a></center></p>