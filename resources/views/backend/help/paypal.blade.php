<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>

<p><a href="index">&laquo; back to index</a></p>
<h1 id="paypal-payments-configuration">PayPal payments configuration</h1>
<hr>
<p><strong> PayPal account </strong></p>
<p>1.) Login to your Paypal account at: <a href="https://www.paypal.com/login" target="_blank">https://www.paypal.com/login</a>                    </p>
<p>2.) Check your account type (shown below &#39;Welcome&#39; message) - it must be <i>Business</i>.<br></p>
<blockquote>
<p><i>Personal</i> or <i>Premier</i> type account must be upgraded to <i>Business</i>
(for details how to do it check Paypal help section)     </p>
</blockquote>
<p>3.) Go to &#39;<i>My Account</i>&#39; &raquo; &#39;<i>Profile</i>&#39; page (link located on top menu)</p>
<p>4.) Select &#39;<i>Instant Payment Notification preferences</i>&#39; from &#39;<i>Selling preferences</i>&#39; section of &#39;<i>Profile</i>&#39; page</p>
<p>5.)  On &#39;<i>Instant Payment Notification (IPN)</i>&#39; page click &#39;<i>Choose IPN Settings</i>&#39; (or &#39;<i>Edit settings</i>&#39; if already defined) button </p>
<p>6.) On &#39;<i>Edit Instant Payment Notification (IPN) settings</i>&#39; page set below values (and hit &#39;<i>Save</i>&#39;):</p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>Notification URL</em></td>
<td>enter value: <code>http://yoursite.com/payments/paypal/ipn</code></td>
</tr>
<tr>
<td><em>IPN messages</em></td>
<td>check option <code>Receive IPN messages (Enabled)</code></td>
</tr>
</tbody>
</table>
<p>7.) Select &#39;<i>Website Payment Preferences&#39;</i>&#39; from &#39;<i>&#39;Hosted payment settings</i>&#39; section of &#39;<i>Profile</i>&#39; page</p>
<p>8.) Set below values (and hit &#39;<i>Save</i>&#39;):</p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>Auto Return</em></td>
<td>check option <code>On</code></td>
</tr>
<tr>
<td><em>Return URL</em></td>
<td>enter value: <code>http://yoursite.com</code></td>
</tr>
</tbody>
</table>
<p><br>
<br>
<br>
<strong> Your web application </strong></p>
<p>1.) Login as administrator user (admin) on: <a href="http://yoursite.com/login">http://yoursite.com/login</a></p>
<p>2.) Go to: &#39;<i>Configuration</i>&#39; &raquo; &#39;<i>Payments</i>&#39;</p>
<p>3.) On section &#39;<i>PayPal</i>&#39; of &#39;<i>Payments Configuration</i>&#39; page set below values  (and hit &#39;<i>Save</i>&#39;): </p>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Value</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><em>PayPal payments enabled</em></td>
<td><code>ON</code></td>
<td>Enable or Disable PayPal payments: <code>ON</code> - enable, <code>OFF</code> - disable</td>
</tr>
<tr>
<td><em>PayPal Seller email</em></td>
<td><code>seller@yoursite.com</code></td>
<td>Your email address assigned to your Paypal business account; All payments will be received to Paypal account assigned for this email.</td>
</tr>
<tr>
<td><em>Payments mode</em></td>
<td><code>production</code></td>
<td>Payments gateway work mode: <br><code>test</code> - all payments are simulated; <br><code>sandbox</code> - all payments are processed on PayPal sandbox test envirnoment; <br><code>production</code> - all payments are real payments</td>
</tr>
</tbody>
</table>
<p><br><center><a href="index">&laquo; back to index</a></center></p>