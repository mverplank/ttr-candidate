<link href="{{ asset('public/css/help.css')}}" rel="stylesheet" type="text/css"/>
<h1 id="contents">Contents</h1>
<hr>
<ol>
    <li><a href="rss">Episodes RSS feeds</a> </li>
    <li><a href="channel">New channel creation process</a></li>
    <li><a href="sam">SAM Broadcaster integration</a></li>
    <li><a href="edit-theme-content">Channel theme custom content management</a></li>
    <li><a href="custom-content-management">Custom content management</a></li>
    <li><a href="custom-content-helper-methods">Custom content helper methods</a></li>
    <li><a href="custom-files">Custom files placement</a></li>
    <li><a href="paypal">PayPal payments configuration</a></li>
    <li><a href="facebook">Facebook login configuration</a></li>
    <li><a href="scheduled-social-share">Scheduled social share feature configuration</a></li>
    <li><a href="dev-deploy">Development &amp; Deployment instructions</a></li>
  	<li><a href="podcasts-custom-uploads">Podcasts Custom Uploads</a></li>
</ol>