@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
    .cropper-container.cropper-bg {
        width: 100%;
    }
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Newsletter</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Tools
                        </li>
                        <li class="active">
                            Newsletter
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div id="yes_crop"></div>
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/mailing/composer', 'id' => 'MailingAdminIndexForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Composer</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('MailingFrom', 'From', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Mailing][from]','', ['id'=>'MailingFrom', 'class'=>'form-control required','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','type'=>'text'])}}
                                    </div>
                                </div><!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MailingSubject', 'Subject', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Mailing][subject]','', ['id'=>'MailingSubject', 'class'=>'form-control required','data-forma'=>'1','data-forma-def'=>'1','data-type'=>'text','type'=>'text'])}}
                                    </div>
                                </div><!-- end row -->
                                <!-- Editor for Bio-->
                                <div class="form-group row">
                                    <div class="col-sm-7 col-sm-offset-4">
                                        <div class="card-box">
                                            {{ Form::textarea('data[Content][email_body]', '', ['id' => 'ContentEmailBody', 'class'=>'']) }}
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-xs-12">               
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Audience</h4>
                                <div class="p-20">
                                    <div class="form-group row">
                                            @if(!empty($groups))
                                                @foreach ( $groups as $i =>$item )
                                                <div class="col-sm-2">
                                                    <div class="checkbox checkbox-pink">
                                                        {!! Form::checkbox( 'data[Mailing][audience][]', $i,true, ['class' => 'md-check', 'id' => 'MailingAudience'.$i] ) !!}
                                                        {!! Form::label('MailingAudience'.$i,  $item) !!}
                                                    </div>
                                                </div>
                                                @endforeach
                                            @else
                                                Not any Channel existing yet.
                                            @endif
                                    </div><!-- end row -->
                                    <!-- <div class="form-group row ">
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <div class="checkbox checkbox-pink">
                                                    {!! Form::checkbox( 'data[Mailing][audience][]','1', true, ['class' => 'md-check', 'id' => 'MailingAudience1'] ) !!}
                                                    {!! Form::label('MailingAudience1','Administrator') !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="checkbox checkbox-pink">
                                                    {!! Form::checkbox( 'data[Mailing][audience][]','2',true, ['class' => 'md-check', 'id' => 'MailingAudience2'] ) !!}
                                                    {!! Form::label('MailingAudience2','Host') !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">    
                                                <div class="checkbox checkbox-pink">
                                                    {!! Form::checkbox( 'data[Mailing][audience][]','3',true, ['class' => 'md-check', 'id' => 'MailingAudience3'] ) !!}
                                                    {!! Form::label('MailingAudience3','Member') !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">    
                                                <div class="checkbox checkbox-pink">
                                                    {!! Form::checkbox( 'data[Mailing][audience][]','4',true, ['class' => 'md-check', 'id' => 'MailingAudience4'] ) !!}
                                                    {!! Form::label('MailingAudience4','Partner') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>                           
                <div class="form-group">
					<div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Mailing][btnSave]" id="MailingBtnSave" data-form-id="MailingAdminIndexForm" value="send">
                            Send
                        </a>
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
