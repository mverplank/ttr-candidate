@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">My Dashboard</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        @if(Config::get('constants.ADMIN') == getCurrentUserType())
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">               
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="active">
                            <a href="#home-b1" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                <span class="hidden-xs">Live Feedback</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#profile-b1" data-toggle="tab" aria-expanded="true" id="podcastStatsChart">
                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                <span class="hidden-xs">Podcast Statistics</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#messages-b1" data-toggle="tab" aria-expanded="false" id="FavoriteStatsChart">
                                <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                <span class="hidden-xs">Favorites Statistics</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home-b1">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                            <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                        </div>
                        <div class="tab-pane" id="profile-b1">
                            <div class="row">
								<div class="col-md-6">
									<div id="podcastStatsChart7" class="ct-chart ct-golden-section">
										<h5 style="text-align:center;">Last 7 days most popular podcasts</h2> 
									</div>
									<div>
										<ul id="chart_view_7" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
								<div class="col-md-6">
									<div id="podcastStatsChart31" class="ct-chart ct-golden-section"><h5 style="text-align:center;">Last 31 days most popular podcasts</h2> </div>
									<div>
										<ul id="chart_view_31" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
								<div class="col-md-12">
									<div id="podcastStatsChartAll" class="ct-chart ct-golden-section"><h5 style="text-align:center;">All time most popular podcasts</h2> </div>
									<div>
										<ul id="chart_view_all" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages-b1">
                        	<!-- <div class="col-md-4">
		                        {{Form::select('data[Channel][f][id]', ['1'=>'yes', '2'=>'no'], '', $attributes=array('id'=>'ChannelSFId', 'class'=>'selectpicker m-b-0', 'data-selected-text-format'=>'count', 'data-style'=>'btn-purple'))}}
		                    </div> -->
		                    <div class="col-md-4" style="float:right;">
			                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
								    <i class="fa fa-calendar"></i>&nbsp;
								    <span></span> <i class="fa fa-caret-down"></i>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div id="favoritesStatsEpisode" class="ct-chart ct-golden-section">
										<h5 style="text-align:center;">Most favorited Episodes</h2> 
									</div>
									<div>
										<ul id="favoritesEpisodeList" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
								<div class="col-md-6">
									<div id="favoritesStatsShow" class="ct-chart ct-golden-section"><h5 style="text-align:center;">Most favorited Shows</h2> </div>
									<div>
										<ul id="favoritesShowList" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div id="favoritesStatsHost" class="ct-chart ct-golden-section"><h5 style="text-align:center;">Most favorited Hosts</h2> </div>
									<div>
										<ul id="favoritesHostList" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
								<div class="col-md-6">
									<div id="favoritesStatsGuest" class="ct-chart ct-golden-section"><h5 style="text-align:center;">Most favorited Guests</h2> </div>
									<div>
										<ul id="favoritesGuestList" style="list-style-type:none;">
									
										</ul>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        @endif
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

@endsection
