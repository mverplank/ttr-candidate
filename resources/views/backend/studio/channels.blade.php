@extends('backend.layouts.main')

@section('content')
 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Channel StudioApps</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Channel StudioApps
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
            
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->

                    <table id="studio_channels_datatable" class="table compact hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th></th>                            
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div> <!-- container -->
</div> <!-- content -->

@endsection
