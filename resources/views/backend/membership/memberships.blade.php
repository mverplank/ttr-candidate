@extends('backend.layouts.main')

@section('content')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Memberships</h4>
                    <ol class="breadcrumb p-0 m-0">
                        
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Configuration
                        </li>
                        <li class="active">
                            Memberships
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->     
        <div class="row">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
            @endif
           
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
           
            <div class="card-box">
                <div class="radio radio-info radio-inline">
                    <input type="radio" id="all" value="" name="filterMemberships" checked="" onclick="filterMemberships('')" {{ ($membership_type == '') ? "checked" : "" }}>
                    <label for="all"> All Types</label>
                </div>
                <div class="radio radio-primary radio-inline">
                    <input type="radio" id="website-user" value="website-user" name="filterMemberships" onclick="filterMemberships('website-user')" {{ ($membership_type == 'website-user') ? "checked" : "" }}>
                    <label for="admin"> Website/PlayerApp user </label>
                </div> 
                <div class="radio radio-purple radio-inline">
                    <input type="radio" id="podcast-host" value="podcast-host" name="filterMemberships" onclick="filterMemberships('podcast-host')" {{ ($membership_type == 'podcast-host') ? "checked" : "" }}>
                    <label for="partner"> Podcast Host </label>
                </div>
                
                <div style="float:right;">
                    <a href="{{url('/admin/membership/memberships/add')}}" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">  Add
                    </a>
                </div>
            </div>
        </div> 
        <!-- end row -->           
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4> -->
                    <table id="memberships_datatable" class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="last_column_width"></th>
                            </tr>
                        </thead>
                       
                    </table>
                </div>
            </div>
        </div> <!-- end row -->           
    </div> <!-- container -->
</div> <!-- content -->

@endsection
