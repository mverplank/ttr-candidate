@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Membership</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/membership/memberships')}}">All Memberships</a>
                        </li>
                        <li class="active">
                            Edit Membership
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
      
        <div class="row">
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/membership/memberships/edit/'.$membership->id, 'id' => 'MembershipAdminEditForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('MembershipIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="1" name="data[Membership][is_online]" {{(isset($membership->is_online))  ? ($membership->is_online == 1 ? 'checked' : '') : ''}}>
                                            <label for="inlineRadio1"> Online </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2" value="0" name="data[Membership][is_online]" {{(isset($membership->is_online))  ? ($membership->is_online == 0 ? 'checked' : '') : ''}}>
                                            <label for="inlineRadio2"> Offline </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MembershipType', 'Type', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <p class="form-control-static">{{(isset($membership->type))  ? $membership->type : ''}}</p>
                                        <!--  <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio3" value="website-user" name="data[Membership][type]" checked>
                                            <label for="inlineRadio3"> Website/PlayerApp User </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio4" value="podcast-host" name="data[Membership][type]">
                                            <label for="inlineRadio4"> Podcast Host </label>
                                        </div> -->
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MembershipName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Membership][name]', (isset($membership->name))  ? $membership->name : '', $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1', 'data-parsley-maxlength'=>'255', 'id'=>'MembershipName', 'required'=>true))}}
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('MembershipDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {!! Form::textarea('data[Membership][description]',(isset($membership->description))  ? $membership->description : '',['class'=>'form-control', 'rows' => 5]) !!}
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MembershipHasManyMembershipPeriods', 'Periods and Prices', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Name</div>
                                                        </th>
                                                        <th>
                                                            <div>Days</div>
                                                        </th>
                                                        <th>
                                                            <div>Price</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $i = 0
                                                    @endphp
                                                    @if($membership->membership_periods->count() > 0)
                                                        @foreach($membership->membership_periods as $period)
                                                            <tr data-id="" data-off="0" style="display: table-row;" data-rnd="">
                                                                <td>
                                                                    {{Form::hidden('data[MembershipPeriod]['.$i.'][id]', $period->id,  array('id' => 'MembershipPeriod'.$i.'Id' ))}}
                                                                    {{Form::text('data[MembershipPeriod]['.$i.'][name]', $period->name,  array('class' => 'required', 'id' => 'MembershipPeriod'.$i.'Name' ))}}
                                                                </td>
                                                                <td>
                                                                    {{Form::number('data[MembershipPeriod]['.$i.'][days]', $period->days,  array('step' => '1', 'min' => '1', 'id' => 'MembershipPeriod'.$i.'Days' ))}}
                                                                </td>
                                                                <td>
                                                                    {{Form::text('data[MembershipPeriod]['.$i.'][quote]', $period->quote,  array('id' => 'MembershipPeriod'.$i.'QuoteLabel', 'placeholder' => '$ 0.00', 'pattern'=>"/^([0-9]+|[0-9]+\.[0-9]{1,2})$/" ))}}
                                                                              
                                                                </td>
                                                                <td class="actions">
                                                                    <div class="col-sm-6 col-md-4 col-lg-3 {{ ($i == 0) ? 'membership_period' : 'delete_membership_period'}}" style="display: block;">
                                                                        <i class="typcn typcn-delete"></i> 
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @php
                                                            $i++;
                                                            @endphp
                                                        @endforeach
                                                    @else
                                                        <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                                            <td>
                                                                {{Form::hidden('data[MembershipPeriod][0][id]', 0,  array('id' => 'MembershipPeriod0Id' ))}}
                                                                {{Form::text('data[MembershipPeriod][0][name]', null,  array('id' => 'MembershipPeriod0Name' ))}}
                                                            </td>
                                                            <td>
                                                                {{Form::number('data[MembershipPeriod][0][days]', '30',  array('step' => '1', 'min' => '1', 'id' => 'MembershipPeriod0Days' ))}}
                                                            </td>
                                                            <td>
                                                                {{Form::text('data[MembershipPeriod][0][quote]', '0.00',  array('id' => 'MembershipPeriod0QuoteLabel', 'placeholder' => '$ 0.00', 'pattern'=>"/^([0-9]+|[0-9]+\.[0-9]{1,2})$/" ))}}
                                                            </td>
                                                            <td class="actions">
                                                                <div class="col-sm-6 col-md-4 col-lg-3 membership_period" style="display: block;">
                                                                    <i class="typcn typcn-delete"></i> 
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" id="MembershipHasManyMembershipPeriodsAdd">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div>
                        </div>
                    </div>
                </div>
              
                <!-- For Website/Playerapp user--> 
                @if($membership->type == "website-user")
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Bonus Items</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    <?php //echo "<pre>";print_R($items);?>
                                    {{Form::label('InventoryItemInventoryItemSet', 'Assigned Items', array('class' => 'col-sm-3 form-control-label'))}}
                                    <div class="col-sm-7 total_inventory_items">
                                        <?php
                                            $existed_items = array();
                                            if(isset($membership->inventory_items) && !empty($membership->inventory_items)){
                                                foreach($membership->inventory_items as $item){
                                                    $existed_items[$item->id] = $item->name;
                                                }
                                            }
                                        ?>
                                        @if(!empty($items))
                                            @foreach ( $items as $i => $item )
                                            <div class="checkbox checkbox-pink" {{(array_key_exists($i, $existed_items) ? '' : 'style=display:none;')}}>
                                                {!! Form::checkbox( 'data[InventoryItem][InventoryItem][]', $i, '', ['class' => 'md-check', 'id' => $item, (array_key_exists($i, $existed_items) ? 'checked' : '')] ) !!}
                                                {!! Form::label($item,  $item) !!}
                                            </div>
                                            @endforeach
                                        @else
                                            No Bonus Items assigned yet.
                                        @endif
                                        
                                    </div>
                                    <div class="col-sm-1 col-md-1">
                                        <i class="mdi mdi-arrow-expand-all toggle_inventory_items"></i> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <!-- For Podcast users-->
                @if($membership->type == "podcast-host")
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Podcast Host Parameters</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('MembershipDataJsonMaxUploadsPerWeek', 'Max weekly uploads', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::number('data[Membership][data_json][max_uploads_per_week]', $membership->data_json['max_uploads_per_week'], $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1', 'id'=>'MembershipDataJsonMaxUploadsPerWeek', 'min'=>0, 'step'=>1, 'required'=>true))}}
                                         {{Form::hidden('data[Membership][data_json][max_uploads_per_week_params][min]', $membership->data_json['max_uploads_per_week_params']['min'], $attributes = array('class'=>'form-control','id'=>'MembershipDataJsonMaxUploadsPerWeek'))}}
                                        {{Form::hidden('data[MembershipPeriod_deleted]', '0', $attributes = array('class'=>'form-control','id'=>'MembershipPeriodDeleted'))}}
                                        <i class="glyphicon glyphicon-info-sign"></i><span class="font-13 text-muted">Maximum number of allowed podcast uploads per week (for each show)</span>
                                        <div class="error-block" style="display:none;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Membership][btnAdd]" id="MembershipBtnAdd" data-form-id="MembershipAdminEditForm">
                            Edit
                        </a>
                       <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                     
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
      
    </div> <!-- container -->
</div> <!-- content -->

@endsection
