@extends('backend.layouts.main')

@section('content')

<style>
    #mceu_21-body{display:none;}
</style>

 <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Membership</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="{{url('admin')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Users
                        </li>
                        <li class="active">
                            <a href="{{url('admin/membership/memberships')}}">All Memberships</a>
                        </li>
                        <li class="active">
                            Add Membership
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
       
        <div class="row">
            <!-- Form Starts-->  
            {{ Form::open(array('url' => 'admin/membership/memberships/add', 'id' => 'MembershipAdminAddForm')) }}
            <div class="col-xs-12">               
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Details</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('MembershipIsOnline', 'Status', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="1" name="data[Membership][is_online]" checked>
                                            <label for="inlineRadio1"> Online </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2" value="0" name="data[Membership][is_online]">
                                            <label for="inlineRadio2"> Offline </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MembershipType', 'Type', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="MembershipTypeWebsite-user" value="website-user" name="data[Membership][type]" checked>
                                            <label for="inlineRadio3"> Website/PlayerApp User </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="MembershipTypePodcast-host" value="podcast-host" name="data[Membership][type]">
                                            <label for="inlineRadio4"> Podcast Host </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MembershipName', 'Name', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::text('data[Membership][name]', $value = null, $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1', 'data-parsley-maxlength'=>'255', 'id'=>'MembershipName', 'required'=>true))}}
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{Form::label('MembershipDescription', 'Description', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {!! Form::textarea('data[Membership][description]',null,['class'=>'form-control', 'rows' => 5]) !!}
                                    </div>
                                </div>
                                <!-- end row -->
                                <div class="form-group row">
                                    {{Form::label('MembershipHasManyMembershipPeriods', 'Periods and Prices', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        <div class="mtf-dg">
                                            <table class="mtf-dg-table default">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div>Name</div>
                                                        </th>
                                                        <th>
                                                            <div>Days</div>
                                                        </th>
                                                        <th>
                                                            <div>Price</div>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- <tr data-id="" data-off="SkelOffset" style="display:none;">
                                                        <td>
                                                            <input type="hidden" name="data[MembershipPeriodSkelModel][SkelOffset][id]" value="">
                                                            <input type="hidden" name="data[MembershipPeriodSkelModel][SkelOffset][rnd]" value="">
                                                            <div class="row input-out  compact">
                                                               
                                                                <div class="small-12 columns">
                                                                    <div class="floated full-width">
                                                                        <input name="data[MembershipPeriodSkelModel][SkelOffset][name]" data-forma-def="1" data-type="text" type="text" id="MembershipPeriodSkelModelSkelOffsetName">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row input-out  compact">
                                                                
                                                                <div class="small-12 columns">
                                                                    <div class="floated full-width">
                                                                        <div class="floated">
                                                                            <div class="el">
                                                                                <input name="data[MembershipPeriodSkelModel][SkelOffset][days]" data-forma-def="1" data-type="int" autocomplete="off" class="mtf-int " type="number" min="1" step="1" value="30" id="MembershipPeriodSkelModelSkelOffsetDays">
                                                                                <input type="hidden" name="data[MembershipPeriodSkelModel][SkelOffset][days_params][min]" value="1">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row input-out  compact">
                                                                
                                                                <div class="small-12 columns">
                                                                    <div class="floated full-width">
                                                                        <div style="position:relative;margin-bottom:0;">
                                                                            <div class="floated">
                                                                                <div class="el">
                                                                                    <input name="data[MembershipPeriodSkelModel][SkelOffset][quote_label]" data-type="text" autocomplete="off" class="mtf-quote " value="0.00" type="text" id="MembershipPeriodSkelModelSkelOffsetQuoteLabel">
                                                                                    <div class="in-prefix ">$</div>
                                                                                </div>
                                                                                <input type="hidden" id="MembershipPeriodSkelModelSkelOffsetQuote" name="data[MembershipPeriodSkelModel][SkelOffset][quote]" value="" data-forma-def="1" data-type="quote">
                                                                                <input type="hidden" name="data[MembershipPeriodSkelModel][SkelOffset][quote_params][regex]" value="/^([0-9]+|[0-9]+\.[0-9]{1,2})$/">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="actions">
                                                            <button type="button" class="delete hover-delete"><i class="ico">
                                                                <svg id="i-delete" viewBox="0 0 25 32">
                                                                    <path d="M23.179 23.607q0 0.714-0.5 1.214l-2.429 2.429q-0.5 0.5-1.214 0.5t-1.214-0.5l-5.25-5.25-5.25 5.25q-0.5 0.5-1.214 0.5t-1.214-0.5l-2.429-2.429q-0.5-0.5-0.5-1.214t0.5-1.214l5.25-5.25-5.25-5.25q-0.5-0.5-0.5-1.214t0.5-1.214l2.429-2.429q0.5-0.5 1.214-0.5t1.214 0.5l5.25 5.25 5.25-5.25q0.5-0.5 1.214-0.5t1.214 0.5l2.429 2.429q0.5 0.5 0.5 1.214t-0.5 1.214l-5.25 5.25 5.25 5.25q0.5 0.5 0.5 1.214z"></path>
                                                                </svg>&nbsp;</i>
                                                            </button>
                                                        </td>
                                                    </tr> -->
                                                    <tr data-id="" data-off="1" style="display: table-row;" data-rnd="">
                                                        <td>
                                                            {{Form::text('data[MembershipPeriod][1][name]', null,  array('id' => 'MembershipPeriod1Name', 'class'=>'required' ))}}
                                                        </td>
                                                        <td>
                                                           <!--  <input name="data[MembershipPeriod][1][days]" data-forma-def="1" data-type="int" autocomplete="off" class="mtf-int mtf-success" type="number" min="1" step="1" value="30" id="MembershipPeriod1Days">
                                                            <input type="hidden" name="data[MembershipPeriod][1][days_params][min]" value="1"> -->
                                                            {{Form::number('data[MembershipPeriod][1][days]', '30',  array('step' => '1', 'min' => '1', 'id' => 'MembershipPeriod1Days' ))}}
                                                        </td>
                                                        <td>
                                                            {{Form::text('data[MembershipPeriod][1][quote]', '0.00',  array('id' => 'MembershipPeriod1QuoteLabel', 'placeholder' => '$ 0.00', 'pattern'=>"/^([0-9]+|[0-9]+\.[0-9]{1,2})$/" ))}}
                                                                      
                                                        </td>
                                                        <td class="actions">
                                                            <div class="col-sm-6 col-md-4 col-lg-3 membership_period" style="display: block;">
                                                                <i class="typcn typcn-delete"></i> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                            <div class="mtf-buttons" style="clear:both;float:right;">
                                                <button type="button" id="MembershipHasManyMembershipPeriodsAdd" data-form-id="MembershipAdminAddForm" class="submit_form">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                    <span>add</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- For Website/Playerapp user--> 
                <div class="row MembershipTypeWebsite-user">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Bonus Items</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('InventoryItemInventoryItemSet', 'Assigned Items', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        
                                        <?php
                                            $items = array();
                                            if(isset($all_items) && !empty($all_items)){
                                                foreach($all_items as $item){
                                                    $items[$item->id] = '('.$item->category_name.') '.$item->name;
                                                }
                                            }
                                        ?>
                                        @if(!empty($items))
                                            @foreach ( $items as $i =>$item )
                                            <div class="checkbox checkbox-pink">
                                                {!! Form::checkbox( 'data[InventoryItem][InventoryItem][]', $i, '', ['class' => 'md-check', 'id' => $item] ) !!}
                                                {!! Form::label($item,  $item) !!}
                                            </div>
                                            @endforeach
                                        @else
                                            No Bonus Items assigned yet.
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- For Podcast users-->
                <div class="row MembershipTypePodcast-host">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Podcast Host Parameters</h4>
                            <div class="p-20">
                                <div class="form-group row">
                                    {{Form::label('MembershipDataJsonMaxUploadsPerWeek', 'Max weekly uploads', array('class' => 'col-sm-4 form-control-label'))}}
                                    <div class="col-sm-7">
                                        {{Form::number('data[Membership][data_json][max_uploads_per_week]', 3, $attributes = array('class'=>'form-control required', 'data-unique'=> '1', 'data-validation' => '1', 'id'=>'MembershipDataJsonMaxUploadsPerWeek', 'min'=>0, 'step'=>1, 'required'=>true))}}
                                         {{Form::hidden('data[Membership][data_json][max_uploads_per_week_params][min]', '0', $attributes = array('class'=>'form-control','id'=>'MembershipDataJsonMaxUploadsPerWeek'))}}
                                        {{Form::hidden('data[MembershipPeriod_deleted]', '0', $attributes = array('class'=>'form-control','id'=>'MembershipPeriodDeleted'))}}
                                        <i class="glyphicon glyphicon-info-sign"></i><span class="font-13 text-muted">Maximum number of allowed podcast uploads per week (for each show)</span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="fixed_btn">
                        <a type="button" class="btn btn-primary waves-effect waves-light submit_form" name="data[Membership][btnAdd]" id="MembershipBtnAdd" data-form-id="MembershipAdminAddForm">
                            Add
                        </a>
                       <!--  {{Form::submit('Add',$attributes=array('class'=>'btn btn-primary waves-effect waves-light', 'name'=>'data[User][btnAdd]', 'id'=>'UserBtnAdd'))}} -->
                     
                    </div>
                </div>
            </div><!-- end col-->
            {{ Form::close() }}
            <!-- Form Close -->
        </div> <!-- end row -->
      
    </div> <!-- container -->
</div> <!-- content -->

@endsection
