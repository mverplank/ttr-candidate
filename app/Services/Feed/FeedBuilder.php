<?php
namespace App\Services\Feed;

use Illuminate\Support\Facades\App;
use App\Models\Post;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Storage;
use DateTime;
use DB;
use Laravelium\Feed\Feed;
class FeedBuilder
{
    private $config;

    public function __construct()
    {
        $this->config = config()->get('feed');
    }

    public function render($host, $channel, $guest, $show, $hostId, $rsstype, $limit, $filename="", $type, $episodes = array(), $channelId = null, $guestId = null, $daysAhead = 0)
    {
       /* echo '<pre>';
        print_r($host);
        die;*/
        // create new feed

        $feed = App::make("feed");

        // multiple feeds are supported
        // if you are using caching you should set different cache keys for your feeds

        // cache the feed for 60 minutes (second parameter is optional)
        $datetime1 = new DateTime();
        //$start_date = new DateTime();
        $datetime1->setTimestamp(time());
        //print_r($datetime1);
        $datetime2 = new DateTime();
        $datetime2->setTimestamp(strtotime(date('Y-m-d').' 23:59:59'));
        //print_r($datetime2);
        $interval = $datetime1->diff($datetime2);
        $cacheTime = $interval->format('%h')*60+$interval->format('%i');
        $cacheTime = 0;
        //die;

        $feed->setCache($cacheTime, $filename);

        // check if there is cached feed and build new only if is not
        if($feed->isCached() && Storage::disk('local')->exists($filename))
        {
            //echo 'else';
            header('Content-Type: text/xml');
            //header('Content-Type: application/rss+xml; charset=utf-8');
            $rss = Storage::disk('local')->get($filename);
            //echo $feed->getCacheKey();
            print_r($rss);
            die;
            //echo $rss;
            //$feed->response($rss);
            //return $feed->render($type);
            //$feed = new Feed();
            //$feed = new \Laravelium\Feed\Feed;
            //$feed->setCache()
            //return $feed->render($type, $feed->getCacheDuration(), $feed->getCacheKey());
            //$feed = Feed::put($rss);
        }
        else
        {
            //echo 'if';
            // Get only archived episodes (ended shows)
            $timeConditions = '';
            switch ($type) {
                case 'upcoming':
                    // next seven days

                    $timeConditions = '( ( Episode.date > "' . date('Y-m-d') . '" OR ( Episode.date = "' . date('Y-m-d') . '" AND UNIX_TIMESTAMP(Episode.time) > "' . time() . '")) AND Episode.date<="' . date('Y-m-d', time() + $daysAhead * 24 * 60 * 60) . '") ';
                    $order = 'ORDER BY `Episode`.`date` ASC, `Episode`.`time` ASC';
                    //
                    // handled on 'upcoming' method!
                    //
                    break;
                case 'archived':
                default:
                    $timeConditions = '( Episode.date < "' . date('Y-m-d') . '" OR ( Episode.date = "' . date('Y-m-d') . '" AND UNIX_TIMESTAMP(Episode.time) < "' . time() . '")) '; // , 'Show.duration'
                    $order = 'ORDER BY `Episode`.`date` DESC, `Episode`.`time` DESC';
                    break;
            }
            $limit = $limit === null || $limit == 0?'':' LIMIT '.$limit;
            if ($type === 'archived') {
                //echo 'archived';
                // Get Episodes
                $episodes = DB::select('SELECT `Episode`.`id` AS `Episode_id`, `Episode`.`description`, `Episode`.`date`, `Episode`.`time`, `Episode`.`stream`, `EpisodesHasHost`.`hosts_id`, `EpisodesHasGuest`.`guests_id`, `Show`.`name`, `Host1`.`users_id` AS `Host_id`, `Host1`.`pr_name` AS `Host_fullname`, `Profile`.`title` AS `Profile_title`, `Profile`.`firstname` AS `Profile_firstname`, `Profile`.`lastname` AS `Profile_lastname`, `Profile`.`sufix` AS `Profile_postfix`, `Guest`.`title` AS `Guest_title`, `Guest`.`firstname` AS `Guest_firstname`, `Guest`.`lastname`  AS `Guest_lastname`, `Guest`.`sufix` AS `Guest_postfix`, `Guest`.`name` AS `Guest_name`,`Tag`.`name` As `Tag_name`, `Show`.`id` AS `Show_id`, `Show`.`name` AS `Show_name`, `Show`.`description` AS `Show_description`, (CASE WHEN TRIM(`Episode`.`title`)!="" THEN (CASE WHEN TRIM(`Episode`.`title`)!=TRIM(`Show`.`name`) THEN CONCAT(`Show`.`name`, ": ", `Episode`.`title`) ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS  `Episode__title` FROM `episodes` AS `Episode` LEFT JOIN `episodes_has_hosts` AS `EpisodesHasHost` ON (`EpisodesHasHost`.`episodes_id` = `Episode`.`id`) LEFT JOIN `hosts` AS `Host` ON (`Host`.`id` = `EpisodesHasHost`.`hosts_id`) LEFT JOIN `episodes_has_guests` AS `EpisodesHasGuest` ON (`EpisodesHasGuest`.`episodes_id` = `Episode`.`id`) LEFT JOIN `guests` AS `Guest` ON (`Guest`.`id` = `EpisodesHasGuest`.`guests_id`) LEFT JOIN `episodes_has_sponsors` AS `EpisodesHasSponsor` ON (`EpisodesHasSponsor`.`episodes_id` = `Episode`.`id`) LEFT JOIN `sponsors` AS `Sponsor` ON (`Sponsor`.`id` = `EpisodesHasSponsor`.`sponsors_id`) LEFT JOIN `episodes_has_tags` AS `EpisodesHasTag` ON (`EpisodesHasTag`.`episodes_id` = `Episode`.`id`) LEFT JOIN `tags` AS `Tag` ON (`Tag`.`id` = `EpisodesHasTag`.`tags_id`) LEFT JOIN `hosts` AS `Host1` ON (`Host1`.`id` = `EpisodesHasHost`.`hosts_id`) LEFT JOIN `profiles` AS `Profile` ON (`Profile`.`users_id` = `Host1`.`users_id`) LEFT JOIN `shows` AS `Show` ON (`Show`.`id` = `Episode`.`shows_id`)  WHERE '.$timeConditions.'  AND ( `Host`.`id`="'.$hostId.'" OR `Guest`.`hosts_id`="'.$hostId.'" ) GROUP BY `Episode`.`id` '.$order.' '.$limit);
                /*echo '<pre>';print_r($episodes);echo '</pre>';die;*/
            } else {
                $episodes = DB::select('SELECT `Episode`.`id` AS `Episode_id`, `Episode`.`description`, `Episode`.`date`, `Episode`.`time`, `Episode`.`stream`, `EpisodesHasHost`.`hosts_id`, `EpisodesHasGuest`.`guests_id`, `Show`.`name`, `Host1`.`users_id` AS `Host_id`, `Host1`.`pr_name` AS `Host_fullname`, `Profile`.`title` AS `Profile_title`, `Profile`.`firstname` AS `Profile_firstname`, `Profile`.`lastname` AS `Profile_lastname`, `Profile`.`sufix` AS `Profile_postfix`, `Guest`.`title` AS `Guest_title`, `Guest`.`firstname` AS `Guest_firstname`, `Guest`.`lastname` AS `Guest_lastname`, `Guest`.`sufix` AS `Guest_postfix`, `Show`.`id` AS `Show_id`, `Show`.`name` AS `Show_name`, `Show`.`description` AS `Show_description`, (CASE WHEN TRIM(`Episode`.`title`)!="" THEN (CASE WHEN TRIM(`Episode`.`title`)!=TRIM(`Show`.`name`) THEN CONCAT(`Show`.`name`, ": ", `Episode`.`title`) ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS  `Episode__title` FROM `episodes` AS `Episode` LEFT JOIN `episodes_has_hosts` AS `EpisodesHasHost` ON (`EpisodesHasHost`.`episodes_id` = `Episode`.`id`) LEFT JOIN `hosts` AS `Host` ON (`Host`.`id` = `EpisodesHasHost`.`hosts_id`) LEFT JOIN `episodes_has_guests` AS `EpisodesHasGuest` ON (`EpisodesHasGuest`.`episodes_id` = `Episode`.`id`) LEFT JOIN `guests` AS `Guest` ON (`Guest`.`id` = `EpisodesHasGuest`.`guests_id`) LEFT JOIN `episodes_has_sponsors` AS `EpisodesHasSponsor` ON (`EpisodesHasSponsor`.`episodes_id` = `Episode`.`id`) LEFT JOIN `sponsors` AS `Sponsor` ON (`Sponsor`.`id` = `EpisodesHasSponsor`.`sponsors_id`) LEFT JOIN `hosts` AS `Host1` ON (`Host1`.`id` = `EpisodesHasHost`.`hosts_id`) LEFT JOIN `profiles` AS `Profile` ON (`Profile`.`users_id` = `Host1`.`users_id`) LEFT JOIN `shows` AS `Show` ON (`Show`.`id` = `Episode`.`shows_id`)  WHERE '.$timeConditions.'  AND `Episode`.`channels_id` = 2  GROUP BY `Episode`.`id` '.$order.' '.$limit);
                
            }

            //echo 'in if';
            // creating rss feed with our most recent 20 posts
            /*$posts = \DB::table('posts')->orderBy('created_at', 'desc')->take(20)->get();

            // set your feed's title, description, link, pubdate and language
            $feed->title = 'Your title';
            $feed->description = 'Your description';
            $feed->logo = 'http://yoursite.tld/logo.jpg';
            $feed->link = url('feed');
            $feed->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
            $feed->pubdate = $posts[0]->created_at;
            $feed->lang = 'en';
            $feed->setShortening(true); // true or false
            $feed->setTextLimit(100); // maximum length of description text

            foreach ($posts as $post)
            {
                // set item's title, author, url, pubdate, description, content, enclosure (optional)*
                $feed->add($post->title, $post->author, ($post->slug), $post->created_at, $post->description, $post->content);
            }
            //$this->info("Generating RSS Feed");
            // It is important that you sort by the latest post
            $posts  = Post::latest()->get();
            $site   = [
                        'name' => 'YOUR SITE NAME', // Simplest Web
                        'url' => 'YOUR SITE URL', // Link to your rss.xml. eg. https://simplestweb.in/rss.xml
                        'description' => 'YOUR SITE DESCRIPTION',
                        'language' => 'YOUR SITE LANGUAGE', // eg. en, en-IN, jp
                        'lastBuildDate' => $posts[0]->created_at->format(DateTime::RSS), // This generates the latest posts date in RSS compatible format
                    ];*/
            // Passes posts and site data into the rss.blade.view, out comes text in rss format
            $host = empty($host)?[]:$host[0];
            header('Content-Type: text/xml; charset=utf-8');
            echo $rssFileContents = view('rss', compact('episodes', 'host', 'rsstype', 'type', 'channel'));
            // Saves the generated rss feed into a file called rss.xml in the public folder, this works only
            //Storage::disk('local')->put('rss.xml', $rssFileContents);
            Storage::disk('local')->put($filename, $rssFileContents);
            $feed->render($rsstype);
            die();
            //$this->info("Completed");
            //return $feed->render($rsstype);
        }
        
        // first param is the feed format
        // optional: second param is cache duration (value of 0 turns off caching)
        // optional: you can set custom cache key with 3rd param as string
        //return $feed->render($type);

        // to return your feed as a string set second param to -1
        // $xml = $feed->render('atom', -1);

    }

    /**
     * Creating rss feed with our most recent posts. 
     * The size of the feed is defined in feed.php config.
     *
     * @return mixed
     */
    private function getFeedData()
    {
        $maxSize = $this->config['max_size'];
        $posts = Post::paginate($maxSize)->with['user'];
        return $posts;
    }
}