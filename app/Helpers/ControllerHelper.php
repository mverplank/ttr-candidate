<?php

/**
 * @author : Contriverz
 * @since  : 25-01-2019
**/
namespace App\Helpers;



class ControllerHelper{
    

    /**
     * Generate iTunes Categories/Sub-Categories 
     * The itunes categories using in the Hosts.
     * @see: https://www.apple.com/itunes/podcasts/specs.html 
     * @return array
     */

    public static function comboCategoriesSubcategories() {
            
        $comboData = array();
        
        // combo select boxes 
        $_categories = array();
        $_subcategories = array();
                
        $categories = array(
            __('Arts'), __('Business'), __('Comedy'), __('Education'), __('Games & Hobbies'), __('Government & Organizations')
            , __('Health'), __('Kids & Family'), __('Music'), __('News & Politics'), __('Religion & Spirituality')
            , __('Science & Medicine'), __('Society & Culture'), __('Sports & Recreation'), __('Technology'), __('TV & Film')
        );
        
        // ORDER IS IMPORTANT!!!
        $subcategories = array(
            0 => array(__('None'), __('Design'), __('Fashion & Beauty'), __('Food'), __('Literature'), __('Performing Arts'), __('Visual Arts'))
            ,1 => array(__('None'), __('Business News'), __('Careers'), __('Investing'), __('Management & Marketing'), __('Shopping'))
            ,2 => array()
            ,3 => array(__('None'), __('Educational Technology'), __('Higher Education'), __('K-12'), __('Language Courses'), __('Training'))
            ,4 => array(__('None'), __('Automotive'), __('Aviation'), __('Hobbies'), __('Other Games'), __('Video Games'))
            ,5 => array(__('None'), __('Local'), __('National'), __('Non-Profit'), __('Regional'))
            ,6 => array(__('None'), __('Alternative Health'), __('Fitness & Nutrition'), __('Self-Help'), __('Sexuality'))
            ,7 => array()
            ,8 => array()
            ,9 => array()
            ,10 => array(__('None'), __('Buddhism'), __('Christianity'), __('Hinduism'), __('Islam'), __('Judaism'), __('Other'), __('Spirituality'))
            ,11 => array(__('None'), __('Medicine'), __('Natural Sciences'), __('Social Sciences'))
            ,12 => array(__('None'), __('History'), __('Personal Journals'), __('Philosophy'), __('Places & Travel'))
            ,13 => array(__('None'), __('Amateur'), __('College & High School'), __('Outdoor'), __('Professional'))
            ,14 => array(__('None'), __('Gadgets'), __('Tech News'), __('Podcasting'), __('Software How-To'))
            ,15 => array()
        );

        foreach ($categories as $off => $v) {
            
            // category sub-categories
            $scs = $subcategories[$off];  
            if(!empty($scs)){
                foreach ($scs as $v1) {
                    //$_subcategories[$v][] = array('v' => $v1, 'l' => $v1);
                    $_subcategories[$v][$v1] = $v1;
                }
            }else{
                $_subcategories[$v] = array();
            }
            
            // categories 
            //$_categories[] = array('v' => $v, 'l' => $v);
            $_categories[$v] = $v;
        }
       
        // combo select boxes data 
        $comboData[] = $_categories;
        $comboData[] = $_subcategories;
        // echo "Category = <pre>";print_R($comboData);die('cat');
        return $comboData;
    }


    // Function to get the client IP address
    public static function getClientIp() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}