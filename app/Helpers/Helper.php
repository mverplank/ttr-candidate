<?php

/**
 * @author : Contriverz
 * @since  : 02-01-2019
**/
use App\Models\Backend\User;
use App\Models\Backend\Category;

//use App\Models\Backend\ContentCategory;
use App\Models\Frontend\ContentData;
use App\Models\Backend\Media;
use App\Models\Backend\MediaLinking;

    // Display image in the blade
    if (!function_exists('showImage')) {

        function showImage($id, $model, $field, $file){
            $imgUrl = asset('public/files/'.$model.'/'.$id.'/'.$field . ($file!==null ? '/'.$file : ''));
            $imgPath = public_path().'/files/'.$model."/".$id.'/'. $field.'/'.$file;
            if(!file_exists($imgPath)){
                $imgUrl = asset('public/assets/images/users/avatar.jpeg'); 
            }
            return '<img src="'.$imgUrl.'" width="120">';
        }
    }

    if (!function_exists('showHostProfileImage')) {

        function showHostProfileImage($id, $model, $field, $file){
           
            $imgUrl = asset('storage/app/public/'.$model."/".$id.'/'. $field.'/'.$file);
            $imgPath = storage_path().'/app/public/'.$model."/".$id.'/'. $field.'/'.$file;

            if(!file_exists($imgPath)){
                //$imgUrl = asset('storage/app/public/thumb/img/noimg.jpg'); 
                $imgUrl = asset('public/assets/images/users/avatar.jpeg'); 
            }
            return '<img src="'.$imgUrl.'" width="120">'; 
        }
    }
    
    if (!function_exists('showProfileImage')) {
        function showProfileImage($media_id='', $media_type='', $media_file=''){
            $imgUrl = asset('public/assets/images/users/avatar.jpeg'); 
            if(!empty($media_id)){
                $imgUrl = asset('storage/app/public/media/'.$media_id.'/'.$media_type.'/'.$media_file);
                $imgPath = storage_path().'/app/public/media/'.$media_id.'/'.$media_type.'/'.$media_file;

                if(!file_exists($imgPath)){
                    $imgUrl = asset('public/assets/images/users/avatar.jpeg'); 
                }
            }
            return '<img src="'.$imgUrl.'" width="120">'; 
        }
    }

    if (!function_exists('srcHostProfileImage')) {
        function srcHostProfileImage($id, $model, $field, $file){
            $user    = new User();
            $host_id = $user->getHostId($id);

            $imgUrl  = asset('storage/app/public/'.$model."/".$host_id.'/'. $field.'/'.$file);
            $imgPath = storage_path().'/app/public/'.$model."/".$host_id.'/'. $field.'/'.$file;
            if(!file_exists($imgPath)){
                $imgUrl = asset('public/assets/images/users/avatar.jpeg');
            }
            //return '<img src="'.$imgUrl.'" width="120">'; 
            return $imgUrl; 
        }
    }

    // Save profile image
    if (!function_exists('fileUpload')) {
        function fileUpload(Request $request, $file) {
            $this->validate($request, [
                'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            /*if ($request->hasFile('input_img')) {
                $image = $request->file('input_img');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $name);
                $this->save();

                return back()->with('success','Image Upload successfully');
            }*/
        }
    }

    // Show membership end dates at frontend list page
    if (!function_exists('showMembershipEnd')) {
        function showMembershipEnd($date=null) {
            if(is_null($date)){
                $show_date = "<span style='color:red'>n/a</span>";

            }else{
                $curdate = strtotime(date('Y-m-d'));
                $mydate  = strtotime($date);
                if($curdate > $mydate){
                    $show_date = '<span style="color:red">'.date('m/d/Y', strtotime($date)).'</span>';
                }else{
                    $show_date = '<span style="color:green">'.date('m/d/Y', strtotime($date)).'</span>';
                }
                
            }
            return $show_date;
        }
    }

    // Change membership end date to only date mainly used in the edit member page
    if (!function_exists('dateMembershipEnd')) {
        function dateMembershipEnd($date=null) {
            $show_date = "";
            if(!is_null($date)){
                $show_date = date("m/d/Y", strtotime($date));
            }
            return $show_date;
        }
    }

    // Fetch the information of the current user
    if (!function_exists('getCurrentUserInfo')) {
        function getCurrentUserInfo($date=null) {
           
            $current_user_id = 0;
            $user_info = array();
            $user_info['prefix'] = "";
            $user_info['suffix'] = "";
            $user_info['full_name'] = "";
            if (Auth::check())
            {
                $current_user_id = Auth::user()->id; 
                
                if($current_user_id != 0 ){
                    $full_info = User::find($current_user_id)->profiles;
                    if($full_info->count() > 0){
                        $user_info['prefix'] = $full_info[0]->title;
                        $user_info['suffix'] = $full_info[0]->sufix;
                        $user_info['full_name'] = $full_info[0]->name;
                    }
                    $user_info['username']  = Auth::user()->username;                   
                }  
            }
            return  $user_info;
        }
    }

    // Fetch the information of the current user
    if (!function_exists('getCurrentUserType')) {

        function getCurrentUserType() {
           
            $current_user_id = 0;
            $user_type = "";
           
            if (Auth::check())
            {
                $current_user_id = Auth::user()->id;
                if($current_user_id != 0 ){
                    $type = User::select('groups_id')->find($current_user_id);
                    if($type->count() > 0){
                       $user_type = $type->groups_id;
                    }      
                }  
            }
            return  $user_type;
        }
    }
    // sub categories filter
    if (!function_exists('subCategories')) {

        function subCategories($parent_id,$assigned_categories) {
            //echo $parent_id."<br>";
            //echo "<pre>";print_R($assigned_categories);
            $return_sub_cat =  array();
            $subcategories = Category::where('parent_id', '=', $parent_id)
                ->pluck('name','id')->toArray(); 

                //echo "<pre>";print_R($subcategories);die('success');

                $return_sub_cat['subcategories'] = $subcategories;
                foreach($assigned_categories as $catss){
                    if (array_key_exists($catss,$subcategories))
                    {
                        $return_sub_cat['selected_cat'][] = $catss;
                        //break;
                    }
                }
                //echo "<pre>";print_R($return_sub_cat);die('success');
                return $return_sub_cat;

        }
    }

    // Helper function to delete media_linking entries 
    if (!function_exists('deleteMediaLink')) {
        function deleteMediaLink($main_module, $module_id)
        {
            $delete = MediaLinking::where(array('module_id' => $module_id, 'main_module' => $main_module))->delete();
        }
    }
    // Helper function to get current week entries 
    if (!function_exists('get_days'))
    {
        function get_days($type)
        {
            $start = 0; $end = 0;
            if($type == 'current'){
                $start              = date("Y-m-d",strtotime('sunday last week'));
                //$today              = date("Y-m-d",strtotime('today'));//die;
                $end                = date("Y-m-d",strtotime('saturday this week'));
            }
            else{ // Case of upcoming
                $today              = date("Y-m-d",strtotime('today'));//die;
                $start              = date("Y-m-d",strtotime('sunday this week'));
                $end                = date("Y-m-d",strtotime('saturday next week'));
            }
            /*$data['start']              = date("Y-m-d",strtotime('sunday last week'));
            $data['today']              = date("Y-m-d",strtotime('today'));//die;
            $data['end']                = date("Y-m-d",strtotime('saturday this week'));*/

            $days = array();
            $days['Sun'] = $start;
            $dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
            for($i = 1; $i<6;$i++)
            {
                $days[$dayNames[$i]] = date("Y-m-d",strtotime($start. " + $i days"));
            }
            $days['Sat'] = $end;
            return $days;
        }
    }
    // Helper function to get current week entries 
    if (!function_exists('get_slider'))
    {
        function get_slider($category, $limit, $channel_id, $order = 'ASC', $is_publish = true, $column = 'off'){
            $data = [];
            $where_publish = "AND (`ContentData`.`publish` IS NULL OR `ContentData`.`publish` <='".date('Y-m-d H:i:s')."')";
            if(!$is_publish)
            {
                $where_publish = '';
            }
            if(is_int($category))
            {
                $data = DB::select(
                            DB::raw(
                                "SELECT `ContentData`.`id`, `ContentData`.`content_categories_id`, `ContentData`.`off`, `ContentData`.`channels_id`, `ContentData`.`is_online`, `ContentData`.`is_featured`, `ContentData`.`title`, `ContentData`.`subtitle`, `ContentData`.`short_description`, `ContentData`.`keywords`, `ContentData`.`content`, `ContentData`.`url`, `ContentData`.`external_url`, `ContentData`.`date`, `ContentData`.`time`, `ContentData`.`created_at`, `ContentData`.`updated_at`, `ContentData`.`publish`, (`ContentData`.`short_description`) AS `ContentData__desc`, `media`.`meta_json`  AS meta_json, `media`.`id` AS mid FROM `content_data` AS `ContentData` LEFT JOIN media_linking ON `media_linking`.`module_id` =  `ContentData`.`id` LEFT JOIN `media` ON `media`.`id` = `media_linking`.`media_id`  WHERE (`ContentData`.`channels_id` IS NULL " . ($channel_id>0 ? ' OR ContentData.channels_id="' . $channel_id . '"' : '').") AND `ContentData`.`content_categories_id` = {$category} AND `ContentData`.`is_online` = '1'".$where_publish." GROUP BY `ContentData`.`id` ORDER BY `ContentData`.`".$column."` {$order} LIMIT {$limit}"));
            }
            else
            {
                switch($category):
                    case 'featured_guests':
                        $query = "SELECT `Guest`.`id`, `Guest`.`bio`, `Guest`.`title`, `ChannelsHasFeaturedGuest`.*, CONCAT(`Guest`.`title`, ' ', `Guest`.`firstname`, ' ', `Guest`.`lastname`, ' ', `Guest`.`sufix`) AS fullname, LEFT(Guest.bio, 10000) AS short_description, `media`.`meta_json`  AS meta_json, `media`.`id` AS mid FROM `channels_has_featured_guests` AS `ChannelsHasFeaturedGuest` LEFT JOIN `guests` AS `Guest` ON (`Guest`.`id` = `ChannelsHasFeaturedGuest`.`guests_id`) LEFT JOIN media_linking ON `media_linking`.`module_id` =  `ChannelsHasFeaturedGuest`.`guests_id` LEFT JOIN `media` ON `media`.`id` = `media_linking`.`media_id` WHERE `Guest`.`deleted` = 0 AND `Guest`.`is_online` = 1 AND `ChannelsHasFeaturedGuest`.`channels_id` = {$channel_id} GROUP BY `Guest`.`id` ORDER BY `ChannelsHasFeaturedGuest`.`off` ASC LIMIT {$limit}";
                    break;
                    case 'featured_hosts':
                        $query = "SELECT `Host`.`id`, `Host`.`bio`, `Profile`.`name`, `ChannelsHasFeaturedHost`.*, LEFT(Host.bio, 10000) AS short_description FROM `channels_has_featured_hosts` AS `ChannelsHasFeaturedHost` LEFT JOIN `hosts` AS `Host` ON (`Host`.`id` = `ChannelsHasFeaturedHost`.`hosts_id`) LEFT JOIN `users` AS `User` ON (`User`.`id` = `Host`.`users_id`) LEFT JOIN `profiles` AS `Profile` ON (`Profile`.`users_id` = `User`.`id`) WHERE `Host`.`deleted` = 0 AND `Host`.`is_online` = 1 AND `ChannelsHasFeaturedHost`.`channels_id` = {$channel_id} GROUP BY `Host`.`id` ORDER BY `ChannelsHasFeaturedHost`.`off` ASC LIMIT {$limit}";
                        break;

                    case 'featured_episodes':
                        $query = "SELECT `Episode`.`id`, `Episode`.`title`, `Episode`.`description`, `Episode`.`date`, `Episode`.`time`, `Show`.`id`, `ChannelsHasFeaturedEpisode`.*, LEFT(Episode.description, 10000) AS short_description FROM `channels_has_featured_episodes` AS `ChannelsHasFeaturedEpisode` LEFT JOIN `episodes` AS `Episode` ON (`Episode`.`id` = `ChannelsHasFeaturedEpisode`.`episodes_id`) LEFT JOIN `shows` AS `Show` ON (`Show`.`id` = `Episode`.`shows_id`) WHERE `Episode`.`is_online` = 1 AND `ChannelsHasFeaturedEpisode`.`channels_id` = {$channel_id} AND `Episode`.`date` >= '2019-05-14' GROUP BY `Episode`.`id` ORDER BY `Episode`.`date` ASC, `Episode`.`time` ASC LIMIT {$limit}";
                        break;

                endswitch;
                $data = DB::select(DB::raw($query));
            }
            return $data;
        }
    }

    // Helper function to get banner
    if (!function_exists('get_banner'))
    {
        function get_banner($type, $limit, $channel_id){
            switch($type):
                case '728x90':
                    $p = 'is_728x90';
                    break;
                case '625x258':
                    $p = 'is_625x258';
                    break;
                case '300x250':
                    $p = 'is_300x250';
                    break;
                case '120x300':
                    $p = 'is_120x300';
                break;
                default:
                    $p = false;
            endswitch;
            if(!$p)
                return [];
            //$limit = 50;
            $query = 'SELECT `Banner`.`id`, `Banner`.`title`, `Banner`.`url`, `media`.`meta_json`  AS meta_json, `media`.`id` AS mid FROM `banners` AS `Banner` LEFT JOIN `banners_has_channels` AS `BannersHasChannel` ON (`BannersHasChannel`.`banners_id` = `Banner`.`id`) LEFT JOIN `channels` AS `Channel` ON (`Channel`.`id` = `BannersHasChannel`.`channels_id`) LEFT JOIN `sponsors` AS `Sponsor` ON (`Banner`.`sponsors_id` = `Sponsor`.`id`) LEFT JOIN media_linking ON (`media_linking`.`module_id` =  `Banner`.`id` AND `media_linking`.`main_module` = "Banner" AND `media_linking`.`sub_module` = "'.$type.'") LEFT JOIN `media` ON `media`.`id` = `media_linking`.`media_id` WHERE `Banner`.`is_online` = "1" AND (view_period_from IS NULL OR view_period_from="" OR view_period_from="0000-00-00 00:00:00" OR view_period_from <="'.date('Y-m-d').'") AND (view_period_to IS NULL OR view_period_to="" OR view_period_to="0000-00-00 00:00:00" OR view_period_to >="'.date('Y-m-d').'") AND (max_number_of_views IS NULL OR max_number_of_views="" OR max_number_of_views="0" OR max_number_of_views > views) AND (max_number_of_clicks IS NULL OR max_number_of_clicks="" OR max_number_of_clicks="0" OR max_number_of_clicks > clicks) AND `'.$p.'` = 1 AND `Channel`.`id` = "'.$channel_id.'" GROUP BY `Banner`.`id` ORDER BY RAND() ASC LIMIT '.$limit;
            $data = DB::select(DB::raw($query));
            return $data;
        }
    }
    // Helper function to get banner
    if (!function_exists('banner_template'))
    {
        function banner_template($banner, $resolution, $target='', $class="", $sub_class = 'sponsor'){
            $html = [];
            if(count($banner))
            {
                foreach($banner as $b){
                    $url = url('rdr/'.$b->id.'?trg='.$b->url);
                    $imgURL = asset('public/images/'.$resolution.'.png');
                    if(!empty($banner->meta_json))
                    {
                        $t = json_decode($banner->meta_json);
                        if(file_exists(storage_path().'/app/public/media/'.$banner->mid.'/image/'.$t->filename)){
                            $imgURL = asset("storage/app/public/media/".$banner->mid.'/image/'.$t->filename);
                        } 
                    }
                    $html[] = '<div class="'.$class.'"><div class="'.$sub_class.'" data-id="'.$b->id.'" data-rfs="5" data-size="'.$resolution.'" class=""><a href="'.$url.'" '.(!empty($target)?'target="'.$target.'"':'').'><img src="'.$imgURL.'" data-original="'.$imgURL.'" alt="'.$b->title.'" class="lldef lld" style=""></a></div></div>';
                }
            }
            return $html;
        }
    }

    // Helper function to get banner
    if (!function_exists('get_media'))
    {
        function get_media($module_id, $main_module='', $sub_module='', $width = null, $height = null){
            //if(!empty($sub_module))
            //{
                //printf($show_id, $main_module, $sub_module, $width, $height);
                $media      = new Media();
                $media_info = $media->select('media.id as mid', 'media.filename as media_file');
                

                $media_info = $media_info->join('media_linking', 'media_linking.media_id', '=', 'media.id');
                $media_info = $media_info->where('media_linking.module_id', '=', $module_id);
                $media_info = $media_info->where('media_linking.main_module', '=', $main_module);
                //$media_info = $media_info->where('media.deleted', 0);
                if(!empty($sub_module) && $sub_module <> 0)
                    $media_info = $media_info->where('media_linking.sub_module', '=', $sub_module);
                if($width && $height){
                    $media_info = $media_info->where('media.width', '=', $width);
                    $media_info = $media_info->where('media.height', '=', $height);
                }
                return $media_info->get()->toArray();
                //$shows = $shows->leftJoin('media_linking', 'media_linking.module_id', '=', 'shows.id');
                //$shows = $shows->leftJoin('media', 'media.id', '=', 'media_linking.media_id');
            //}
        }
    }

    // Helper function to get banner
    if (!function_exists('get_guest_social_media_urls'))
    {
        function get_guest_social_media_urls($id, $column = 'guests_id'){
            $return = '';
            $query  = "select url, label, type from urls where ".$column." = {$id}";
            $urls   = DB::Select(DB::raw($query));
            $icons  = [
                        'website'   => '<img src="'.asset('/public/images/globe.png').'" alt="website" width=25 />',
                        'blog'      => '<img src="'.asset('/public/images/blog.svg.png').'" alt="blog" width=25 />',
                        'rss'       => '<i class="fa fa-rss-square"></i>',
                        'twitter'   => '<i class="fa fa-twitter-square"></i>',
                        'facebook'  => '<i class="fa fa-fa-facebook-square"></i>',
                        'google'    => '<i class="fa fa-google-plus-square"></i>',
                        'itunes'    => '<i class="fa fa-music"></i>',
                        'youtube'   => '<i class="fa fa-youtube-square"></i>',
                        'instagram' => '<i class="fa fa-instagram"></i>',
                        'pinterest' => '<i class="fa fa-pinterest-square"></i>',
                        'other'     => '',
                      ];
            if(!empty($urls))
            {
                //print_r($urls);die;
                $_c = '';
                foreach($urls as $url)
                {
                    //print_r($url);die;
                    if ($url->url!=='') {
                
                        if (isset($url->label)) {
                            
                            if (!trim($url->label)!=='') {
                                $label = str_ireplace(array(
                                        'http://', 'https://', 
                                        'www.',
                                        'twitter.com', 'facebook.com', 'plus.google.com', 'youtube.com/user', 'instagram.com', 'pinterest.com', 'itunes.com'
                                    ), '', $url->url);
                                /*if ($options['onlyAccountName']) {

                                    
                                }*/
                            } else {

                                $label = $url->label;
                            }
                        } else {
                            
                            
                            $label = str_ireplace(array('http://', 'https://'), '', $url->url);
                        }
                        
                        //$_c .= '<li>'.($options['icons']?$this->Icon->view($icons[$url['type']]):'').($options['labels']?$url['type'].': ':'').'<a href="'.$url['url'].'" target="_blank">'.$label.'</a></li>';
                        $_c .= '<li>'.$icons[$url->type].'<a href="'.$url->url.'" target="_blank">'.$label.'</a></li>';
                    }
                }
                $return = ($_c!==''?'<ul class="social-media-urls">':'').$_c.($_c!==''?'</ul>':'');
            }
            return $return;   
        }
    }

    // Helper function to get banner
    if (!function_exists('episode_guests'))
    {
        function episode_guests($episode_id){
            $return = '';
            $query  = "SELECT `g`.`name` FROM `guests` g join `episodes_has_guests` eg on `eg`.`guests_id` = `g`.`id` where `eg`.`episodes_id` = {$episode_id}";
            $guests   = DB::Select(DB::raw($query));
            if(!empty($guests))
            {
                foreach ($guests as $guest) {
                    $return .= '<div class="tags">'.$guest->name.'</div>';
                }
            }
            /*echo '<pre>';
            print_r($urls);
            die;*/
            return $return;   
        }
    }
    // Helper function to create Rss Output
    if (!function_exists('createRss'))
    {
        function createRss($k, $v)
        {
            return '<'.str_replace('__', ':', $k).'>'.$v.'</'.str_replace('__', ':', $k).'>';
        }
    }