<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BannersHasChannel
 * 
 * @property int $banners_id
 * @property int $channels_id
 * 
 * @property \App\Models\Backend\Banner $banner
 * @property \App\Models\Backend\Channel $channel
 *
 * @package App\Models\Backend
 */
class BannersHasChannel extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'banners_id' => 'int',
		'channels_id' => 'int'
	];

	public function banner()
	{
		return $this->belongsTo(\App\Models\Backend\Banner::class, 'banners_id');
	}

	public function channel()
	{
		return $this->belongsTo(\App\Models\Backend\Channel::class, 'channels_id');
	}

	public function fetchBannersAccToChannel($channel_id=0, $is_online=2){
		if($channel_id != 0){
			
			$get_banners = array();
			$get_banners = BannersHasChannel::where('channels_id', $channel_id)->with('banner')->with('banner.sponsor')->where('is_online', $is_online)->get();
			if($get_banners->count() > 0){
				return $get_banners;
			}
			return $get_banners;
		}
	}
}
