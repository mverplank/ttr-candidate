<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HostsHasHostType
 * 
 * @property int $hosts_id
 * @property int $host_types_id
 * 
 * @property \App\Models\HostType $host_type
 * @property \App\Models\Host $host
 *
 * @package App\Models\Frontend
 */
class HostsHasHostType extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'host_types_id' => 'int'
	];

	protected $fillable = [
		'hosts_id',
		'host_types_id'
	];

	public function host_type()
	{
		return $this->belongsTo(\App\Models\Backend\HostType::class, 'host_types_id');
	}

	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}
}
