<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HostItune
 * 
 * @property int $id
 * @property int $hosts_id
 * @property bool $is_enabled
 * @property \Carbon\Carbon $publish_date
 * @property string $keywords
 * @property string $title
 * @property string $subtitle
 * @property string $description
 * @property string $podcast
 * @property string $podcast_title
 * @property string $podcast_category
 * @property string $podcast_subcategory
 * 
 * @property \App\Models\Frontend\Host $host
 *
 * @package App\Models\Frontend
 */
class HostItune extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'is_enabled' => 'bool'
	];

	protected $dates = [
		'publish_date'
	];

	protected $fillable = [
		'hosts_id',
		'is_enabled',
		'publish_date',
		'keywords',
		'title',
		'subtitle',
		'description',
		'podcast',
		'podcast_title',
		'podcast_category',
		'podcast_subcategory'
	];

	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}
}
