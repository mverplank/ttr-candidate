<?php

/**
 * @author: Contriverz
 * @since : Thu, 02 May 2019 03:40:23.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Models\Backend\MediaLinking;
use DB;
/**
 * Class Show
 * 
 * @property int $id
 * @property string $channels_ids
 * @property int $deleted
 * @property string $name
 * @property string $description
 * @property int $duration
 * @property bool $is_encore
 * @property bool $is_online
 * @property bool $is_featured
 * @property string $file
 * @property string $stream
 * @property int $favorites
 * @property bool $is_created_by_host
 * 
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 * @property \Illuminate\Database\Eloquent\Collection $shows_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 *
 * @package App\Models\Backend
 */
class Show extends Eloquent
{
	public $timestamps = true;

	protected $casts  = [
		'deleted'     => 'int',
		'duration'    => 'int',
		'is_encore'   => 'bool',
		'is_online'   => 'bool',
		'is_featured' => 'bool',
		'favorites'   => 'int',
		'is_created_by_host' => 'bool'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'channels_ids',
		'deleted',
		'name',
		'description',
		'duration',
		'is_encore',
		'is_online',
		'is_featured',
		'file',
		'stream',
		'favorites',
		'is_created_by_host'
	];

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_shows', 'shows_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->hasMany(\App\Models\Backend\Episode::class, 'shows_id');
	}

	public function schedules()
	{
		return $this->hasMany(\App\Models\Backend\Schedule::class, 'shows_id');
	}

	public function shows_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\ShowsHasCategory::class, 'shows_id');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'shows_has_hosts', 'shows_id', 'hosts_id')
					->withPivot('id', 'off', 'type');
	}

	// Fetch all shows with id for the filters
	public function getShowsForFilter(){
		$show_obj = $this->select('id', 'name')->where('deleted', 0)->orderBy('name', 'asc')->pluck('name', 'id');
		return $show_obj;
	}


    /* Count all the featured shows */
    public function countAllFeaturedShows($channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$shows = new Show();
		if(!empty($channel_id)){
			$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id')->where('channels_has_shows.channels_id', '=', $channel_id);
		}

		//Fetch who are not deleted
		$shows = $shows->where('shows.deleted', 0);
		
		// Overall Search 
        if(!empty($search_value)){
        	//if(empty($channel_id)){
            	$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id');
        	//}
       		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id')
           			->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id') 
           			->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id')
           			->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');
            $shows = $shows->where(function($q) use ($search_value){
							$q->orWhere('shows.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('channels.name' ,'like', '%'.$search_value.'%');
						});
    								
        }
		$shows = $shows->count();
		return $shows;
	}
	
    /**
	* Fetches all the featured shows
	* @return result in the form of object
	*/
	public function getAllFeaturedShows($channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$shows = Show::select('shows.id', 'shows.name', 'shows.description', 'media.meta_json', 'media.id as mid');
		//Get featured shows
		$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id')->groupBy('shows.id');
		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
		$shows = $shows->leftJoin('media_linking', 'media_linking.module_id', '=', 'shows.id');
		$shows = $shows->leftJoin('media', 'media.id', '=', 'media_linking.media_id');

		$shows = $shows->where(['shows.is_featured' => 1, 'shows.deleted' => 0, 'shows.is_online' => 1, 'channels.id' => $channel_id]);

		// Overall Search 
        if(!empty($search_value)){
        	//if(empty($channel_id)){
            	$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id');
        	//}
        	$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
        	$shows = $shows->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id');
        	$shows = $shows->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id');
        	$shows = $shows->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');

            $shows = $shows->where(function($q) use ($search_value){
							$q->orWhere('shows.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('channels.name' ,'like', '%'.$search_value.'%');
						});
        }

        $shows = $shows->orderBy('shows.name', 'asc');
       	$shows  = $shows->paginate($length);
        //$shows = $shows->offset($start)->limit($length)->get();
		return $shows;
	}
	public function show_hosts($show_id, $type = 'host')
	{
		//$host_obj = Host::select(DB::raw('`hosts`.`id`, `profiles`.`name`, TRIM(CONCAT( `profiles`.`lastname`, " ", `profiles`.`firstname` )) as `fullname`'))
		//				->join('profiles', 'profiles.users_id','=', 'hosts.users_id');

		/*$hosts = \App\Models\Frontend\Host::select('Host.id', 'Host.bio', 'Host.cohost_option_override_host_banners',  DB::raw('(SELECT TRIM(CONCAT(`Profile`.`title`, " ", `Profile`.`firstname`, " ", `Profile`.`lastname`, " ", `Profile`.`sufix`)) FROM profiles Profile WHERE `Profile`.`users_id`=`Host`.`users_id`) AS `Host__fullname`'));
		$hosts = $hosts->join('shows_has_hosts shh', 'shh.hosts_id', '=', 'Host.id');
		$hosts = $hosts->where('Host.shows_id', $show_id);
		$hosts = $hosts->orderBy('shh.off1');
		return $hosts;*/
		$hosts = DB::Select(DB::raw('SELECT `Host`.`id`, `Host`.`bio`, `Host`.`cohost_option_override_host_banners`, `media`.`filename`, `media`.`id` AS mid, (SELECT TRIM(CONCAT(`Profile`.`title`, " ", `Profile`.`firstname`, " ", `Profile`.`lastname`, " ", `Profile`.`sufix`)) FROM profiles Profile WHERE `Profile`.`users_id`=`Host`.`users_id`) AS `fullname` FROM `hosts` AS `Host` join shows_has_hosts shh on shh.hosts_id = `Host`.`id` LEFT JOIN media_linking ON `media_linking`.`module_id` =  `Host`.`id` AND `media_linking`.`main_module` = "Host" AND `media_linking`.`sub_module` = "photo" LEFT JOIN `media` ON `media`.`id` = `media_linking`.`media_id` WHERE shows_id="'.$show_id.'" AND `Host`.`type` = "'.$type.'" group by `Host`.`id` ORDER BY shh.off'));
		return $hosts;
	}
	/* Model methods for frontend ends here*/
}
