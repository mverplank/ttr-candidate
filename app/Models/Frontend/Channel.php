<?php

/**
 * @author : Contriverz
 * @since  : Fri, 03 May 2019 01:24:16.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Channel
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $deleted
 * @property bool $is_online
 * @property string $domain
 * @property string $theme
 * @property string $stream
 * @property string $url_archived_episodes
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $sam_dir_shows
 * @property string $sam_file_connecting
 * @property int $sam_min_show_duration
 * @property bool $studioapp_enabled
 * @property array $studioapp_settings_json
 * @property bool $is_missing_episodes_reminder
 * @property string $theme_name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banners
 * @property \Illuminate\Database\Eloquent\Collection $channels_has_content_categories
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $guests
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 * @property \Illuminate\Database\Eloquent\Collection $shows
 * @property \Illuminate\Database\Eloquent\Collection $sponsors
 * @property \Illuminate\Database\Eloquent\Collection $chnnel_itunes
 * @property \Illuminate\Database\Eloquent\Collection $content_data
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 *
 * @package App\Models\Frontend
 */
class Channel extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'deleted' => 'int',
		'is_online' => 'bool',
		'sam_min_show_duration' => 'int',
		'studioapp_enabled' => 'bool',
		'studioapp_settings_json' => 'object',
		'is_missing_episodes_reminder' => 'bool'
	];

	protected $fillable = [
		'name',
		'description',
		'deleted',
		'is_online',
		'domain',
		'theme',
		'stream',
		'url_archived_episodes',
		'seo_title',
		'seo_keywords',
		'seo_description',
		'sam_dir_shows',
		'sam_file_connecting',
		'sam_min_show_duration',
		'studioapp_enabled',
		'studioapp_settings_json',
		'is_missing_episodes_reminder',
		'theme_name'
	];

	public function banners()
	{
		return $this->belongsToMany(\App\Models\Frontend\Banner::class, 'banners_has_channels', 'channels_id', 'banners_id');
	}

	public function channels_has_content_categories()
	{
		return $this->hasMany(\App\Models\Frontend\ChannelsHasContentCategory::class, 'channels_id');
	}

	public function episodes()
	{
		return $this->hasMany(\App\Models\Frontend\Episode::class, 'channels_id');
	}

	public function guests()
	{
		return $this->belongsToMany(\App\Models\Frontend\Guest::class, 'channels_has_guests', 'channels_id', 'guests_id');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Frontend\Host::class, 'channels_has_hosts', 'channels_id', 'hosts_id');
	}

	public function shows()
	{
		return $this->belongsToMany(\App\Models\Frontend\Show::class, 'channels_has_shows', 'channels_id', 'shows_id');
	}

	public function sponsors()
	{
		return $this->belongsToMany(\App\Models\Frontend\Sponsor::class, 'channels_has_sponsors', 'channels_id', 'sponsors_id');
	}

	public function chnnel_itunes()
	{
		return $this->hasMany(\App\Models\Frontend\ChnnelItune::class, 'channels_id');
	}

	public function content_data()
	{
		return $this->hasMany(\App\Models\Frontend\ContentDatum::class, 'channels_id');
	}

	public function schedules()
	{
		return $this->hasMany(\App\Models\Frontend\Schedule::class, 'channels_id');
	}

	// Fetch all channels with id for the filters
	public function getAllChannels(){
		$channel_obj = Channel::select('id', 'name')->where('deleted', 0)->where('is_online', 1)->orderBy('name', 'asc')->pluck('name', 'id');
		return $channel_obj;
	}
	/**
     * Fetch the shows of the channel
     * 
     * @param channel id
     * @return all shows of that channel
     */
    public function fetchChannelShows($channel_id =0){
		if($channel_id !=0 ){
			$channel = $this->find($channel_id);	
			
			//Only fetch the shows which are not deleted and having online status
			/*$get_shows = $channel->shows()->where(['shows.deleted'=>0, 'shows.is_online'=>1])->orderBy('shows.name', 'asc');
			return $get_shows;*/
			$get_shows = $channel->shows()->where(['shows.deleted'=>0, 'shows.is_online'=>1])->orderBy('shows.name', 'asc')->pluck('name', 'id');

			//echo "<pre>";print_R($get_shows);die;
			$make_arr = [];
			$i=0;
			if($get_shows){
				//
				foreach($get_shows as $key=>$show){
					//echo "<pre>";print_R($show);
					$make_arr[$i]['id'] = $key;
					$make_arr[$i]['name'] = $show;
					$i++;
				}
			}
			//echo "<pre>";print_R($make_arr);die;
			return $make_arr;
		}
	}
}
