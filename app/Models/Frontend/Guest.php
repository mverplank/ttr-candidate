<?php

/**
 * @author: Contriverz.
 * @since : Mon, 06 May 2019 01:08:11.
 */

namespace App\Models\Frontend;


use Reliese\Database\Eloquent\Model as Eloquent;
//use App\Models\Frontend\Activity;
use Auth;
use DB;
/**
 * Class Guest
 * 
 * @property int $id
 * @property int $users_id
 * @property int $guests_id
 * @property bool $deleted
 * @property string $title
 * @property string $sufix
 * @property string $firstname
 * @property string $lastname
 * @property string $name
 * @property string $bio
 * @property string $email
 * @property string $phone
 * @property string $cellphone
 * @property string $skype
 * @property string $skype_phone
 * @property string $pr_type
 * @property string $pr_name
 * @property string $pr_company_name
 * @property string $pr_phone
 * @property string $pr_cellphone
 * @property string $pr_skype
 * @property string $pr_skype_phone
 * @property string $pr_email
 * @property bool $is_online
 * @property bool $is_featured
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $off
 * @property int $favorites
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $guest_contents
 * @property \Illuminate\Database\Eloquent\Collection $guests_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $urls
 * @property \Illuminate\Database\Eloquent\Collection $videos
 *
 * @package App\Models\Frontend
 */
class Guest extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'users_id' => 'int',
		'hosts_id' => 'int',
		'deleted' => 'bool',
		'is_online' => 'bool',
		'is_featured' => 'bool',
		'off' => 'int',
		'favorites' => 'int'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'users_id',
		'created_by',
		'hosts_id',
		'deleted',
		'title',
		'sufix',
		'firstname',
		'lastname',
		'name',
		'bio',
		'email',
		'phone',
		'cellphone',
		'skype',
		'skype_phone',
		'pr_type',
		'pr_name',
		'pr_company_name',
		'pr_phone',
		'pr_cellphone',
		'pr_skype',
		'pr_skype_phone',
		'pr_email',
		'is_online',
		'is_featured',
		'created_at',
		'updated_at',
		'off',
		'favorites',
		'to_be_contacted'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\Frontend\User::class, 'users_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Frontend\Channel::class, 'channels_has_guests', 'guests_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->belongsToMany(\App\Models\Frontend\Episode::class, 'episodes_has_guests', 'guests_id', 'episodes_id')
					->withPivot('id', 'off');
	}

	public function guest_contents()
	{
		return $this->hasMany(\App\Models\Frontend\GuestContent::class, 'guests_id');
	}

	public function guests_has_categories()
	{
		return $this->hasMany(\App\Models\Frontend\GuestsHasCategory::class, 'guests_id');
	}

	public function urls()
	{
		return $this->hasMany(\App\Models\Frontend\Url::class, 'guests_id');
	}

	public function videos()
	{
		return $this->hasMany(\App\Models\Frontend\Video::class, 'guests_id');
	}

	/**
	* Count all the guests
	* @return result in the form of object
	* @param 
	*/
	public function countAllGuestss($alphabet='', $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{

		$guests = User::select('users.*', 'guests.id AS guest_id', 'guests.bio AS guest_bio', 'hosts.id AS host_id', 'hosts.bio AS host_bio', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix', 'profiles.name');/*->where('users.groups_id', 5)
						->orWhere(function($query) {
			                $query->where('users.groups_id', 2)
			                    ->where('users.is_guest','=', 1);
			            })->where('users.deleted', 0)*/

		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
			$guests = $guests->where('profiles.lastname' ,'like', $alphabet.'%');
		}

		// Overall Search 
        if(!empty($search_value)){
            $guests = $guests->where(function($q) use ($search_value){
    							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
    								->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
    								->orWhere('hosts.bio' ,'like', '%'.$search_value.'%');
    						});
        }
		
		//Fetch who are not deleted
		$guests = $guests->leftJoin('guests', 'users.id', '=', 'guests.users_id')->leftJoin('hosts', 'users.id', '=', 'hosts.users_id');

		//Fetch name of the host
		$guests = $guests->leftJoin('profiles', 'profiles.users_id', '=', 'users.id');

		// Overall Search 
        if(!empty($search_value)){
          
            // Join for searching the host name from profiles
			$guests = $guests->where(function($q) use ($search_value){
				        $q->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('hosts.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%');
				   });
        }

     	$guests = $guests->where('users.groups_id', 5)
					->orWhere(function($query) {
			            $query->where('users.groups_id', 2)
			                ->where('users.is_guest', 1);
			        })->where('users.deleted', 0)
					->where('guests.deleted', 0);


        // Sorting by column
        if($order != null){
            $guests = $guests->orderBy($order, $dir);
        }else{
            $guests = $guests->orderBy('profiles.name', 'asc')->orderBy('profiles.firstname', 'asc');
        } 
        $guests = $guests->count();
		return $guests;	
	}

	/**
	 * Fetches all the guests
	 * @return result in the form of object
	 * @param 
	 */
	public function getAllGuests($alphabet='', $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		
		$guests = User::select('users.*', 'guests.id AS guest_id', 'guests.bio AS guest_bio', 'hosts.id AS host_id', 'hosts.bio AS host_bio', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix', 'profiles.name', 'media.id AS media_id', 'media.filename AS media_file', 'media.type AS media_type');

		//Fetch media of guests
		$guests = $guests->leftJoin('guests', 'users.id', '=', 'guests.users_id')
						->leftJoin('media_linking', function ($join) {
				            $join->on('media_linking.module_id', '=', 'guests.id')
				            ->where('media_linking.main_module', 'Guest')
				            ->where('media_linking.sub_module', 'photo');
				        })->leftJoin('media', 'media_linking.media_id', '=', 'media.id')
						->leftJoin('hosts', 'users.id', '=', 'hosts.users_id');
		
		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
				$guests = $guests->where('profiles.lastname' ,'like', $alphabet.'%');
								
		}
		
		

		// Overall Search 
        if(!empty($search_value)){
            $guests = $guests->where(function($q) use ($search_value){
    							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
    								->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
    								->orWhere('hosts.bio' ,'like', '%'.$search_value.'%');
    						});
        }


		$guests =  $guests->leftJoin('channels_has_guests', 'guests.id', '=', 'channels_has_guests.guests_id');

		//Fetch name of the guest
		$guests = $guests->leftJoin('profiles', 'profiles.users_id', '=', 'users.id');

		// Overall Search 
        if(!empty($search_value)){
          
            // Join for searching the host name from profiles
			$guests = $guests->where(function($q) use ($search_value){
				        $q->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('hosts.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%');
				   });
        }

        $guests = $guests->where('users.groups_id', 5)
						->orWhere(function($query) {
				            $query->where('users.groups_id', 2)
				                ->where('users.is_guest', 1);
				        })->where('users.deleted', 0)
						->where('guests.deleted', 0);

        // Sorting by column
        if($order != null){
            $guests = $guests->orderBy($order, $dir);
        }else{
            $guests = $guests->orderBy('profiles.name', 'asc')->orderBy('profiles.firstname', 'asc');
        } 
       
        $guests = $guests->offset($start)->limit($length)->get();
		return $guests;
		/*******************************************************/
	}

	/**
	 * Fetches all the guests
	 * @return result in the form of object
	 * @param 
	 */
	public function countAllGuests($alphabet='',$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		
		$guests = new User();
		
		//Fetch who are not deleted
		$guests = $guests->leftJoin('guests', 'users.id', '=', 'guests.users_id')
						->leftJoin('media_linking', function ($join) {
				            $join->on('media_linking.module_id', '=', 'guests.id')
				            	->where('media_linking.main_module', 'Guest')
				            	->where('media_linking.sub_module', 'photo');
				        	})->leftJoin('media', 'media_linking.media_id', '=', 'media.id')
						->leftJoin('hosts', 'users.id', '=', 'hosts.users_id');
		
		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
			$guests = $guests->where('profiles.lastname' ,'like', $alphabet.'%');			
		}
		
		// Overall Search 
        if(!empty($search_value)){
            $guests = $guests->where(function($q) use ($search_value){
    							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
    								->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
    								->orWhere('hosts.bio' ,'like', '%'.$search_value.'%');
    						});
        }


		$guests =  $guests->leftJoin('channels_has_guests', 'guests.id', '=', 'channels_has_guests.guests_id');

		//Fetch name of the guest
		$guests = $guests->leftJoin('profiles', 'profiles.users_id', '=', 'users.id');

		// Overall Search 
        if(!empty($search_value)){
          
            // Join for searching the host name from profiles
			$guests = $guests->where(function($q) use ($search_value){
				        $q->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('hosts.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%');
				   });
        }

        $guests = $guests->where('users.groups_id', 5)
						->orWhere(function($query) {
				            $query->where('users.groups_id', 2)
				                ->where('users.is_guest', 1);
				        })->where('users.deleted', 0)
						->where('guests.deleted', 0);

        // Sorting by column
        if($order != null){
            $guests = $guests->orderBy($order, $dir);
        }else{
            $guests = $guests->orderBy('profiles.name', 'asc')->orderBy('profiles.firstname', 'asc');
        } 
       
        $guests = $guests->count();
		return $guests;
		
	}

	/**
	 * Export  guests
	 * @return result in the form of object
	 */
	public function eportGuests($channel_id='',$all='', $online='', $offline='', $featured=''){
		
			$guests = Guest::select('guests.lastname', 'guests.firstname', 'guests.email',
                'guests.phone', 'guests.cellphone', 'guests.skype', 'guests.skype_phone',
                'pr_name', 'pr_company_name', 'pr_phone', 'pr_cellphone',
                'pr_skype', 'pr_skype_phone', 'pr_email');
			if(!empty($channel_id)){
				$guests = $guests->Join('channels_has_guests', 'channels_has_guests.guests_id', '=', 'guests.id')
								->where('channels_has_guests.channels_id', '=', $channel_id)
								->where('guests.id', '!=', Auth::id())
			    				->where('guests.deleted',0);
			}
			if(!empty($online)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
	                   			  ->where('guests.deleted',0)
	                   			  ->where('guests.is_online','=',1);
			}
			if(!empty($offline)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
                   				  ->where('guests.deleted',0)
                   				  ->where('guests.is_online','=',0);
			}
			if(!empty($featured)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
                   				  ->where('guests.deleted',0)
                   				  ->where('guests.is_featured','=',1);
			}									

			if(!empty($all)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
                   				  ->where('guests.deleted',0);
			}

			// Exclude the guests who don't want to be contacted
			$guests = $guests->where('guests.to_be_contacted',1);
			$guests = $guests->orderBy('guests.lastname', 'asc')->get();
			return $guests;
	}

	/**
	 * Fetch the guests for the filters 
	 * @return result is the object of the name and id of the guests
	 */
	public function getGuestsForFilter(){
		$guests_list = $this->select(DB::raw('`guests`.`id`, TRIM(CONCAT( `lastname`, " ", `firstname` )) as `fullname`'))->orderBy('lastname', 'asc')->pluck('fullname', 'id');
		return $guests_list;
	}
}
