<?php

/**
 * @author: Contriverz
 * @since : Wed, 01 May 2019 02:51:56.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Medium
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property string $title
 * @property string $alt
 * @property string $caption
 * @property string $description
 * @property string $filename
 * @property string $type
 * @property array $meta_json
 *
 * @package App\Models\Frontend
 */
class MediaLinking extends Eloquent
{
	public $timestamps = true;
	protected $table = 'media_linking';
	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'media_id',
		'main_module',
		'sub_module',
		'module_id',
		'created_at',
		'created_by',
		'updated_at'
	];
	
	public function media()
	{
		return $this->belongsTo(\App\Models\Frontend\Media::class, 'media_id');
	}

	/**
    * @description get all media by main module
    * @param module_id is 0 
    * @param $main_module here is main module
    * @param $sub_module here is the sub module
    */
	public function get_media($module_id, $main_module, $sub_module){
		
		$media = MediaLinking::where('module_id',$module_id)
								->where('main_module',$main_module)
								->where('sub_module',$sub_module)
								->get();
		return $media;
	}
}
