<?php

/**
 * @author: Contriverz
 * @since : Wed, 01 May 2018 12:26:26.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Archive
 * 
 * @property int $id
 * @property \Carbon\Carbon $datetime
 * @property int $episode_id
 * @property string $type
 * @property string $title
 * @property string $title_soundex
 * @property int $shows_id
 * @property int $channels_id
 * @property string $show_name
 * @property string $show_name_soundex
 * @property string $description
 * @property string $host_ids
 * @property string $hosts
 * @property string $hosts_soundex
 * @property string $cohost_ids
 * @property string $cohosts
 * @property string $cohosts_soundex
 * @property string $podcasthost_ids
 * @property string $podcasthosts
 * @property string $podcasthosts_soundex
 * @property string $guest_ids
 * @property string $guests
 * @property string $guests_soundex
 * @property string $tag_ids
 * @property string $tags
 * @property string $tags_soundex
 * @property string $categories_ids
 * @property string $categories
 * @property string $categories_soundex
 * @property \Carbon\Carbon $year
 * @property int $month
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $time
 * @property int $duration
 * @property int $is_online
 * @property bool $is_featured
 * @property int $encores_number
 * @property string $stream
 * @property bool $is_uploaded_by_host
 * @property string $video_url
 *
 * @package App\Models\Frontend
 */
class Archive extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episode_id' => 'int',
		'shows_id' => 'int',
		'channels_id' => 'int',
		'month' => 'int',
		'duration' => 'int',
		'is_online' => 'int',
		'is_featured' => 'bool',
		'encores_number' => 'int',
		'is_uploaded_by_host' => 'bool'
	];

	protected $dates = [
		'datetime',
		'year',
		'date',
		'time'
	];

	protected $fillable = [
		'datetime',
		'episode_id',
		'type',
		'title',
		'title_soundex',
		'shows_id',
		'channels_id',
		'show_name',
		'show_name_soundex',
		'description',
		'host_ids',
		'hosts',
		'hosts_soundex',
		'cohost_ids',
		'cohosts',
		'cohosts_soundex',
		'podcasthost_ids',
		'podcasthosts',
		'podcasthosts_soundex',
		'guest_ids',
		'guests',
		'guests_soundex',
		'tag_ids',
		'tags',
		'tags_soundex',
		'categories_ids',
		'categories',
		'categories_soundex',
		'year',
		'month',
		'date',
		'time',
		'duration',
		'is_online',
		'is_featured',
		'encores_number',
		'stream',
		'is_uploaded_by_host',
		'video_url'
	];

	public function getArchivedEpisodes($channel_id=0, $start=0, $length=10, $show_id=0, $host=0, $year=0, $month=0, $category_id=0, $search_value=null, $search_regex=null, $current_user_id='', $type=1, $order=null, $dir=null,  $column_search=null){		

		$archives = Archive::select('archives.id', 'archives.datetime', 'archives.title', 'archives.description', 'archives.encores_number','archives.hosts', 'archives.cohosts', 'archives.guests', 'archives.podcasthosts', 'archives.episode_id', 'media.id AS media_id', 'media.filename AS media_file', 'media.type AS media_type')
						->leftJoin('media_linking', function ($join) {
				             $join->on('media_linking.module_id', '=', 'archives.shows_id')
				            ->where('media_linking.main_module', '=', 'Show')
				            ->where('media_linking.sub_module', '=', 'schedule');
				        })->leftJoin('media', 'media_linking.media_id', '=', 'media.id');

		/*
		if(!empty($type)){
            $hosts = $hosts->join('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('hosts_has_host_types.host_types_id', '=', $type);
		}*/
		
		if(!empty($channel_id)){
			//$archives = $archives->where('channels_id', '=', $channel_id);
			$archives = $archives->leftJoin('shows', 'archives.shows_id', '=', 'shows.id')->whereRaw('FIND_IN_SET('.$channel_id.', shows.channels_ids)');
		}
		if(!empty($show_id)){
			$archives = $archives->where('shows_id', '=', $show_id);
		}
		if(!empty($year)){
			$archives = $archives->where('year', '=', $year);
		}
		if(!empty($month)){
			$archives = $archives->where('month', '=', $month);
		}
		if(!empty($category_id)){
			$archives = $archives->whereRaw('FIND_IN_SET('.$category_id.', categories_ids)');
		}
		if(!empty($host)){
			$archives = $archives->whereRaw('FIND_IN_SET('.$host.', host_ids)');
		}

		$archives = $archives->whereRaw('UNIX_TIMESTAMP( CONCAT(`archives`.`date`, " ", `archives`.`time`)) + `archives`.`duration` * 60 < UNIX_TIMESTAMP() AND archives.is_online = 1');
		/*if($type == 2){
			$archives = $archives->whereRaw('UNIX_TIMESTAMP( CONCAT(`archives`.`date`, " ", `archives`.`time`)) + `archives`.`duration` * 60 < UNIX_TIMESTAMP() AND (    FIND_IN_SET("'.$current_user_id.'", `archives`.`host_ids`) > 0 OR FIND_IN_SET("'.$current_user_id.'", `archives`.`cohost_ids`) > 0)');
			
		}*/
		
		
		// Overall Search 
        if(!empty($search_value)){
        	
          	$archives = $archives->where(function($q)  use ($search_value){
          					$q->orWhere('archives.title' ,'like', '%'.$search_value.'%')
          					  ->orWhere('archives.show_name' ,'like', '%'.$search_value.'%');
          				});
        }

        // Sorting by column
        if($order != null){
            $archives = $archives->orderBy($order, $dir);
        }else{
        	$archives = $archives->orderBy('archives.datetime', 'DESC');
        }
  
        //$archives = $archives->offset($start)->limit($length)->get();
        $archives = $archives->paginate($length);
        //echo "<pre>";print_R($archives);die;
		return $archives;
	}

	/**
	 * Count all the archived episodes
     * @param $channel_id - episodes of particular channel
     * @param  
	 */
	public function countArchivedEpisodes($channel_id=0, $current_user_id='', $type=1, $category_id=0, $host=0, $month=0, $year=0, $show_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		

		$archives = Archive::leftJoin('media_linking', function ($join) {
				             $join->on('media_linking.module_id', '=', 'archives.shows_id')
				            ->where('media_linking.main_module', '=', 'Show')
				            ->where('media_linking.sub_module', '=', 'schedule');
				        })->leftJoin('media', 'media_linking.media_id', '=', 'media.id');

		/*
		if(!empty($type)){
            $hosts = $hosts->join('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('hosts_has_host_types.host_types_id', '=', $type);
		}*/
		
		if(!empty($channel_id)){
			//$archives = $archives->where('channels_id', '=', $channel_id);
			$archives = $archives->leftJoin('shows', 'archives.shows_id', '=', 'shows.id')->whereRaw('FIND_IN_SET('.$channel_id.', shows.channels_ids)');
		}
		
		$archives = $archives->whereRaw('UNIX_TIMESTAMP( CONCAT(`archives`.`date`, " ", `archives`.`time`)) + `archives`.`duration` * 60 < UNIX_TIMESTAMP() AND archives.is_online = 1');
		/*if($type == 2){
			$archives = $archives->whereRaw('UNIX_TIMESTAMP( CONCAT(`archives`.`date`, " ", `archives`.`time`)) + `archives`.`duration` * 60 < UNIX_TIMESTAMP() AND (    FIND_IN_SET("'.$current_user_id.'", `archives`.`host_ids`) > 0 OR FIND_IN_SET("'.$current_user_id.'", `archives`.`cohost_ids`) > 0)');
			
		}else*/
		{
			if($show_id){
				$archives = $archives->where('shows_id', '=', $show_id);
			}
			if($year){
				$archives = $archives->where('year', '=', $year);
			}
			if($month){
				$archives = $archives->where('month', '=', $month);
			}
			if($category_id){
				$archives = $archives->whereRaw('FIND_IN_SET('.$category_id.', categories_ids)');
			}
			if($host){
				$archives = $archives->whereRaw('FIND_IN_SET('.$host.', host_ids)');
			}
		}
		
		
		// Overall Search 
        if(!empty($search_value)){
          	$archives = $archives->where('archives.title' ,'like', '%'.$search_value.'%');
        }

        // Sorting by column
        if($order != null){
            $archives = $archives->orderBy($order, $dir);
        }else{
        	$archives = $archives->orderBy('archives.datetime', 'DESC');
        }
        $archives = $archives->count();
		//echo "<pre>";print_R($archives);die('success');
		return $archives;
	}
}