<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContentDatum
 * 
 * @property int $id
 * @property int $content_categories_id
 * @property int $off
 * @property int $channels_id
 * @property bool $is_online
 * @property bool $is_featured
 * @property string $title
 * @property string $subtitle
 * @property string $short_description
 * @property string $keywords
 * @property string $content
 * @property string $url
 * @property string $external_url
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $publish
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\ContentCategory $content_category
 * @property \Illuminate\Database\Eloquent\Collection $content_data_has_tags
 *
 * @package App\Models\Backend
 */
class ContentData extends Eloquent
{
	public $timestamps = true;
	protected $table = "content_data";

	protected $casts = [
		'content_categories_id' => 'int',
		'off' => 'int',
		'channels_id' => 'int',
		'is_online' => 'bool',
		'is_featured' => 'bool'
	];

	protected $dates = [
		'date',
		//'time',
		'created_at',
		'updated_at',
		'publish'
	];

	protected $time = ['time'];

	protected $fillable = [
		'off',
		'channels_id',
		'content_categories_id',
		'is_online',
		'is_featured',
		'title',
		'subtitle',
		'short_description',
		'keywords',
		'content',
		'url',
		'external_url',
		'date',
		'time',
		'created_at',
		'updated_at',
		'publish'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Backend\Channel::class, 'channels_id');
	}

	public function content_category()
	{
		return $this->belongsTo(\App\Models\Backend\ContentCategory::class, 'content_categories_id');
	}

	public function content_data_has_tags()
	{
		return $this->hasMany(\App\Models\Backend\ContentDataHasTag::class, 'content_data_id');
	}

	public function tags()
	{
	return $this->belongsToMany(\App\Models\Backend\Tag::class, 'content_data_has_tags', 'content_data_id', 'tags_id');
	}

	/**
	 * Fetch the content data acc. to the content category id
	 * @param $content_category_id is the category id of the content
	 * @param $order_field is the column for the sorting
	 * @param $order_direction is the sorting order
	 */
	public function fetchDataOfCategory($content_category_id='', $order_field, $order_direction, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$get = $this;

		// Overall Search 
        if(!empty($search_value)){
            $get = $get->where(function($q) use ($search_value){
    							$q->orWhere('title' ,'like', '%'.$search_value.'%');
    						});
        }
        
        // Sorting by column
        if($order != null){
            $get = $get->orderBy($order, $dir);
        }else{
            $get = $get->orderBy('created_at', 'desc');
        } 
		

		if($content_category_id > 0){
			$get = $get->where('content_categories_id', $content_category_id);
		}
		
		if($order_field == "modified"){
    		$order_field = "updated_at";
    	}else if($order_field == "created"){
    		$order_field = "created_at";
    	}

		$get = $get->with('content_category')
					->whereNull('channels_id')
					->orderBy($order_field, $order_direction);

		$get = $get->offset($start)->limit($length)->get();
		return $get;
	}

	/**
	 * Fetch the content data acc. to the content category id
	 * @param $content_category_id is the category id of the content
	 * @param $order_field is the column for the sorting
	 * @param $order_direction is the sorting order
	 */
	public function countDataOfCategory($content_category_id='', $order_field, $order_direction, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$get = $this;

		// Overall Search 
        if(!empty($search_value)){
            $get = $get->where(function($q) use ($search_value){
    							$q->orWhere('title' ,'like', '%'.$search_value.'%');
    						});
        }
        
        // Sorting by column
        if($order != null){
        	if($order == "modified"){
        		$order = "updated_at";
        	}
            $get = $get->orderBy($order, $dir);
        }else{
            $get = $get->orderBy('created_at', 'desc');
        } 
		

		if($content_category_id > 0){
			$get = $get->where('content_categories_id', $content_category_id);
		}

		if($order_field == "modified"){
    		$order_field = "updated_at";
    	}else if($order_field == "created"){
    		$order_field = "created_at";
    	}

		$get = $get->with('content_category')
					->whereNull('channels_id')
					->orderBy($order_field, $order_direction);
					
		$get = $get->count();
		return $get;
	}

	/**
	* @description -  Add the content data
	* @param $data - content data
	*/
	public function add_content_data($data){
		if(!empty($data)){
			try{
				if(!empty($data['ContentData'])){
					$data['ContentData']['time'] =date('h:i:s',strtotime($data['ContentData']['time']));
					$data['ContentData']['date'] =date('Y-m-d',strtotime($data['ContentData']['date']));
					$selected_date =$data['ContentData']['publish_date'].' '.date('h:i:s',strtotime($data['ContentData']['publish_time']));
					$date_string = strtotime($selected_date);
					$data['ContentData']['publish'] = date('Y-m-d h:i:s',$date_string);
					unset($data['ContentData']['publish_date']);
					unset($data['ContentData']['publish_time']);
					
					$contentData = new ContentData($data['ContentData']);
					$contentData->save();
					$media = new MediaLinking();
					$media->add($contentData->id, 'ContentData');
				}
				return true;
			}catch(\Exception $e){
				return false;
	   		}
		}	
	}

	/**
	* @description -  Edit the content data
	* @param $data - content data
	* @param $id - content data id
	*/
	public function edit_content_data($id,$data){
		if(!empty($data)){
			try{
				$content = ContentData::find($id);
				if(!empty($data['ContentData'])){
					$data['ContentData']['date'] =date('Y-m-d',strtotime($data['ContentData']['date']));
					$data['ContentData']['time'] =date('Y-m-d',strtotime($data['ContentData']['time']));
					$selected_date =$data['ContentData']['publish_date'].' '.date('h:i:s',strtotime($data['ContentData']['publish_time']));
					$date_string = strtotime($selected_date);
					$data['ContentData']['publish'] = date('Y-m-d h:i:s',$date_string);

					$content->update($data['ContentData']);
				}

				$old_tags = array();
				$old_content_tags = $content->tags()->get();
				if($old_content_tags->count() > 0){
		            foreach($old_content_tags as $tag){
		                $old_tags[$tag->name] = $tag->id;
		            }
		        }

				if(!empty($data['Tag']['Tag'])){
					$new_tags = explode(",",$data['Tag']['Tag']);
				        if(!empty($new_tags)){
				        	foreach($new_tags as $item){
				        		$existtag = Tag::where('name', '=', $item)->where('type', '=', $data['ContentData']['content_categories_id'])->first();
				        		if($existtag){
				        			try{
				        				if (!in_array($existtag->id, $old_tags)){
					        				$tagid = $existtag->id;
					        				$content->tags()->attach($tagid);
				        				}
				        			}
				        			catch(\Exception $e){
										return false;
							   		}				        			
				        		}else{
				        			try{
					        			$tag = new Tag;
					        			$tag->name =$item;
					        			$tag->type =$data['ContentData']['content_categories_id'];
					        			$tag->save();
					        			$content->tags()->attach($tag->id);
				        			}catch(\Exception $e){
										return false;
							   		}
				        		}
							}

							foreach($old_tags as $key=>$olt){
				                if (!in_array($key, $new_tags)){
			        				$content->tags()->detach($olt);
			        			}	
					        }
						}
				}
				return true;
			}catch(\Exception $e){
				return false;
	   		}
		}	
	}


	/**
	* @description - Delete the content data
	* @param $id - it is content data id
	*/
	public function delete($id=''){
		
		//Delete the linking of the content data and the tags
		if ($this->has('content_data_has_tags')) {
			$this->content_data_has_tags()->delete();
		}
		
		//Delete all the tags from the tag table
		$tagObj = new Tag();
		$tagObj->where('type', $id)->delete();

		//Delete the content data itself
		$this->where('id', $id)->delete();

		return true;
	}
}
