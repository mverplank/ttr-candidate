<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HostContent
 * 
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property int $hosts_id
 * @property int $off
 * @property bool $is_enabled
 * 
 * @property \App\Models\Host $host
 *
 * @package App\Models
 */
class HostContent extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'off' => 'int',
		'is_enabled' => 'bool'
	];

	protected $fillable = [
		'title',
		'description',
		'url',
		'hosts_id',
		'off',
		'is_enabled'
	];

	public function host()
	{
		return $this->belongsTo(\App\Models\Host::class, 'hosts_id');
	}
}
