<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HostType
 * 
 * @property int $id
 * @property string $name
 * @property bool $deleted
 * 
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 *
 * @package App\Models\Backend
 */
class HostType extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'deleted' => 'bool'
	];

	protected $fillable = [
		'name',
		'deleted'
	];

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'hosts_has_host_types', 'host_types_id', 'hosts_id');
	}
}
