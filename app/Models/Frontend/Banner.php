<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Frontend;
use App\Models\Backend\MediaLinking;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
/**
 * Class Banner
 * 
 * @property int $id
 * @property int $sponsors_id
 * @property bool $is_online
 * @property int $off
 * @property string $title
 * @property string $url
 * @property string $notices
 * @property \Carbon\Carbon $view_period_from
 * @property \Carbon\Carbon $view_period_to
 * @property int $max_number_of_views
 * @property int $max_number_of_clicks
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property int $views
 * @property int $clicks
 * @property bool $is_120x300
 * @property bool $is_300x250
 * @property bool $is_625x258
 * @property bool $is_728x90
 * 
 * @property \App\Models\Backend\Sponsor $sponsor
 * @property \Illuminate\Database\Eloquent\Collection $banners_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $channels
 *
 * @package App\Models\Backend
 */
class Banner extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'sponsors_id' => 'int',
		'is_online' => 'bool',
		'off' => 'int',
		'max_number_of_views' => 'int',
		'max_number_of_clicks' => 'int',
		'views' => 'int',
		'clicks' => 'int',
		'is_120x300' => 'bool',
		'is_300x250' => 'bool',
		'is_625x258' => 'bool',
		'is_728x90' => 'bool'
	];

	protected $dates = [
		'view_period_from',
		'view_period_to',
		'created_at',
		'modified_at'
	];

	protected $fillable = [
		'sponsors_id',
		'is_online',
		'off',
		'title',
		'url',
		'notices',
		'view_period_from',
		'view_period_to',
		'max_number_of_views',
		'max_number_of_clicks',
		'created',
		'modified',
		'views',
		'clicks',
		'is_120x300',
		'link_120x300',
		'is_300x250',
		'link_300x250',
		'is_625x258',
		'link_625x258',
		'is_728x90',
		'link_728x90'
	];

	public function sponsor()
	{
		return $this->belongsTo(\App\Models\Backend\Sponsor::class, 'sponsors_id');
	}

	public function banners_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\BannersHasCategory::class, 'banners_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'banners_has_channels', 'banners_id', 'channels_id');
	}

	/**
	* @description Edit the banner
	* @param where $id is banner_id and $data is the $request object for updating the object
	*/	
	public function edit($id=0, $data)
	{
		if($id != 0){
			if(!empty($data)){
				try{
					// Update data to banner
					$banner = Banner::find($id);
					if(!empty($data['Banner']['view_period_from'])){
						$data['Banner']['view_period_from'] = date_format(date_create($data['Banner']['view_period_from']),"Y-m-d H:i:s");
					}
					if(!empty($data['Banner']['view_period_to'])){
						$data['Banner']['view_period_to'] = date_format(date_create($data['Banner']['view_period_to']),"Y-m-d H:i:s");
					}
					$banner->update($data['Banner']);

					//Fetch if exists the banners  categories
					try{
					   	$old_banner = array();
						$old_banners = $banner->banners_has_categories()->get();
						if($old_banners->count() > 0){

				            foreach($old_banners as $baner){
				               $old_banner[] = $baner->categories_id;
				            }
   						}	
   					
   						$banner_categories_arr = array();
					   	if(isset($data['Category'])){
							if(!empty($data['Category'])){
								if(!empty($data['Category']['Category'])){	
       								if(!empty($old_banner)){
       									foreach($old_banner as $baner){
											if(!in_array($baner, $data['Category']['Category'])){
												$del_banner_types_object = BannersHasCategory::where('categories_id' , '=', $baner)->where('banners_id', '=', $banner->id)->delete();
											}
										}
										
										foreach($data['Category']['Category'] as $item){
											if(!in_array($item, $old_banner)){
												$obj = array('banners_id'=>$banner->id , 'categories_id'=>$item);
												
												$banner_categories_arr[] = new BannersHasCategory($obj);
											}
										}
       								}
   									else{
								       	//When there does not exist some banners  categories
								       	foreach($data['Category']['Category'] as $item){
											$obj = array('banners_id'=>$banner->id , 'categories_id'=>$item);
											$banner_categories_arr[] = new BannersHasCategory($obj);
										}  
   									}	
   									
									// Add banners  categories
									if(!empty($banner_categories_arr)){
										$banner->banners_has_categories()->saveMany($banner_categories_arr);
									}
								}
							}
						}else{
							//If no banner categories then delete all the old ones.
							if(!empty($old_banner)){
						       	foreach($old_banner as $baner){	
									$del_banner_types_object = BannersHasCategory::where('categories_id' , '=', $baner)->where('banners_id', '=', $banner->id)->delete();	
								}
						    }
						}
					}
					catch(\Exception $e){
						
						return false;
					}

   					// Update data to channels_has_banners
   					try{
						$old_channels = array();
						$old_banner_channels = $banner->channels()->get();
						if($old_banner_channels->count() > 0){
				            foreach($old_banner_channels as $channel){
				               $old_channels[] = $channel->id;
				            }
				        }	
   						if(isset($data['Channel'])){
							if(!empty($data['Channel'])){
								if(!empty($data['Channel']['Channel'])){	
							       	if(!empty($old_channels)){
							       		foreach($old_channels as $chn){
											if(!in_array($chn, $data['Channel']['Channel'])){
												$banner->channels()->detach($chn);
											}
										}
       								}
      
									foreach($data['Channel']['Channel'] as $item){
										if(!in_array($item, $old_channels)){
											$banner->channels()->attach($item);
										}
									}
								}
							}
						}else{
							if(!empty($old_channels)){
						       	foreach($old_channels as $chn){
									$banner->channels()->detach($chn);	
								}
   							}
						}
					}
					catch(\Exception $e){
						//$e->getMessage();die;
						return false;
					}
					return true;
				}catch(\Exception $e){
					//$e->getMessage();die;
					return false;
				}
			}
		}else{
			return false;
		}
	}
}
