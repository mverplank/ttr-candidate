<?php

/**
 * @author: Contriverz
 * @since : Wed, 01 May 2019 02:51:14.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class Medium
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property string $title
 * @property string $alt
 * @property string $caption
 * @property string $description
 * @property string $filename
 * @property string $type
 * @property array $meta_json
 *
 * @package App\Frontend
 */
class Media extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'meta_json' => 'object'
	];

	protected $dates = [
		'created_at'
	];

	protected $fillable = [
		'created_at',
		'title',
		'alt',
		'caption',
		'description',
		'name',
		'filename',
		'type',
		'height',
		'width',
		'meta_json',
		'uploaded_by',
		'uploaded_area'
	];
	
	public function medialinking()
	{
		return $this->hasMany(\App\Models\Frontend\MediaLinking::class, 'media_id');
	}

	// Return all media meta_json 
	public function get_media($module_id = false, $main_module, $sub_module)
	{
		$host_obj = Media::select(DB::raw('`media`.`meta_json` AS meta_json, `media_linking`.`media_id` AS mid, `media_linking`.`module_id` AS showID'))
						->leftJoin('media_linking', 'media_linking.media_id','=', 'media.id')
						->where('media_linking.main_module',$main_module)
						->where('media_linking.sub_module',$sub_module)
						->whereIN('module_id',$module_id)
						->get();
		/*if(!empty($type)){
			$host_obj = $host_obj->join('hosts_has_host_types','hosts_has_host_types.hosts_id','=','hosts.id')->where('hosts_has_host_types.host_types_id',$type);
		}
		if(!empty($operation) && $operation == "import"){

			$host_obj = $host_obj->whereHas('user', function($query)
							{
							    $query->where('is_guest', 0);   	  
							});
		}*/
		
		//$host_obj = $host_obj->pluck('meta_json', 'mid', 'showID');
		$media = $host_obj->toArray();
		$return = array();
		foreach ($media as $key => $value) {
		 	$return[$value['showID']]['mid'] = $value['mid'];
		 	$return[$value['showID']]['meta_json'] = $value['meta_json'];
		} 
		return $return;
		//return $host_obj->toArray();
	}
}
