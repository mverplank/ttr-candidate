<?php

/**
 * @author: Contriverz  
 * @since : Thu, 02 May 2019 11:13:19.
 */

namespace App\Models\Frontend;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use DB;
/**
 * Class Sponsor
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property bool $is_online
 * @property bool $is_featured
 * @property string $type
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $url
 * @property array $social_urls_json
 * @property string $company
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $email
 * @property string $skype
 * @property string $phone
 * @property string $cellphone
 * @property string $contact_name
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_skype
 * @property string $contact_cellphone
 * @property string $billing_name
 * @property string $billing_email
 * @property string $billing_phone
 * @property string $billing_skype
 * @property string $billing_cellphone
 * @property string $notes
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banners
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 *
 * @package App\Models\Frontend
 */
class Sponsor extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'is_online' => 'bool',
		'is_featured' => 'bool',
		'social_urls_json' => 'object'
	];

	protected $dates = [
		'start_date',
		'end_date',
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'is_online',
		'is_featured',
		'type',
		'title',
		'firstname',
		'lastname',
		'name',
		'description',
		'url',
		'social_urls_json',
		'company',
		'address',
		'city',
		'state',
		'zip',
		'email',
		'skype',
		'phone',
		'cellphone',
		'start_date',
		'end_date',
		'contact_name',
		'contact_email',
		'contact_phone',
		'contact_skype',
		'contact_cellphone',
		'billing_name',
		'billing_email',
		'billing_phone',
		'billing_skype',
		'billing_cellphone',
		'notes'
	];

	public function banners()
	{
		return $this->hasMany(\App\Models\Backend\Banner::class, 'sponsors_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_sponsors', 'sponsors_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->belongsToMany(\App\Models\Backend\Episode::class, 'episodes_has_sponsors', 'sponsors_id', 'episodes_id')
					->withPivot('id', 'off');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'hosts_has_sponsors', 'sponsors_id', 'hosts_id');
	}

	/**
	* Fetches all the sponsors
	* @return result in the form of object
	* @param status 1 for online, 0 for offline
	*/
	public function getSponsors($channel_id=0, $start=0, $length=10, $status=2, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{

		$sponsors = $this->select('sponsors.id', 'sponsors.created_at', 'firstname', 'sponsors.updated_at', 'sponsors.type', 'is_online', 'is_featured', 'sponsors.title', 'name', 'sponsors.description', 'url', 'social_urls_json', 'company', 'address', 'city', 'state', 'zip', 'email', 'skype', 'phone', 'cellphone', 'contact_name', 'contact_email', 'contact_phone', 'contact_skype', 'contact_cellphone', 'billing_name', 'billing_email', 'billing_phone', 'billing_skype', 'billing_cellphone', 'notes', DB::raw('(TRIM(CONCAT( name, " (", TRIM(company), ")" ))) AS  Sponsor__fullname'), DB::raw('CONCAT(SUBSTRING_INDEX(sponsors.description, " ", 26), " ...") AS Sponsor__short_description'), 'media.id AS media_id', 'media.filename AS media_file', 'media.type AS media_type')		  ->whereHas('channels', function ($query) use ($channel_id){
				        $query->where('channels_id', '=', $channel_id);
				    })
				    ->leftJoin('media_linking', function ($join) {
			            $join->on('media_linking.module_id', '=', 'sponsors.id')
			            	->where('media_linking.main_module', 'Sponsor');
			        })
			        ->leftJoin('media', 'media_linking.media_id', '=', 'media.id')      
				    ->where('sponsors.is_online', 1)
				    ->groupBy('sponsors.id')
				    ->inRandomOrder()
				    ->paginate($length);
		
		//echo "<pre>";print_R($sponsors);die('success');
		return $sponsors;
	}

	/**
	* Fetches all the sponsors
	* @return result in the form of object
	* @param status 1 for online, 0 for offline
	*/
	public function countSponsors($channel_id=0, $status=2, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{

		$sponsors = Sponsor::select('sponsors.id', 'sponsors.created_at', 'firstname', 'sponsors.updated_at', 'sponsors.type', 'is_online', 'is_featured', 'sponsors.title', 'name', 'sponsors.description', 'url', 'social_urls_json', 'company', 'address', 'city', 'state', 'zip', 'email', 'skype', 'phone', 'cellphone', 'contact_name', 'contact_email', 'contact_phone', 'contact_skype', 'contact_cellphone', 'billing_name', 'billing_email', 'billing_phone', 'billing_skype', 'billing_cellphone', 'notes', DB::raw('(TRIM(CONCAT( name, " (", TRIM(company), ")" ))) AS  Sponsor__fullname'), DB::raw('CONCAT(SUBSTRING_INDEX(sponsors.description, " ", 26), " ...") AS Sponsor__short_description'), 'media.id AS media_id', 'media.filename AS media_file', 'media.type AS media_type')		  ->whereHas('channels', function ($query) use ($channel_id){
				        $query->where('channels_id', '=', $channel_id);
				    })
				    ->leftJoin('media_linking', function ($join) {
			            $join->on('media_linking.module_id', '=', 'sponsors.id')
			            	->where('media_linking.main_module', 'Sponsor');
			        })
			        ->leftJoin('media', 'media_linking.media_id', '=', 'media.id')      
				    ->where('sponsors.is_online', 1)
				    ->groupBy('sponsors.id')
				    ->count();
		
		return $sponsors;
	}


	/**
	* Fetch the sponsors for the filters 
	* @return result is the object of the name and id of the sponsors
	*/
	public function getSponsorsForFilter(){
		$sponsors_list = $this->select('id', 'name')->orderBy('lastname', 'asc')->pluck('name', 'id');
		return $sponsors_list;
	}

}
