<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BannersHasCategory
 * 
 * @property int $banners_id
 * @property int $categories_id
 * 
 * @property \App\Models\Backend\Banner $banner
 * @property \App\Models\Backend\Category $category
 *
 * @package App\Models\Backend
 */
class BannersHasCategory extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'banners_id' => 'int',
		'categories_id' => 'int'
	];

	protected $fillable = [
		'banners_id',
		'categories_id'
	];

	public function banner()
	{
		return $this->belongsTo(\App\Models\Backend\Banner::class, 'banners_id');
	}

	public function category()
	{
		return $this->belongsTo(\App\Models\Backend\Category::class, 'categories_id');
	}
}
