<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Token
 * 
 * @property int $id
 * @property int $users_id
 * @property string $service
 * @property string $key
 * @property string $secret
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property bool $is_active
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Token extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int',
		'is_active' => 'bool'
	];

	protected $dates = [
		'created',
		'modified'
	];

	protected $hidden = [
		'secret'
	];

	protected $fillable = [
		'users_id',
		'service',
		'key',
		'secret',
		'created',
		'modified',
		'is_active'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
