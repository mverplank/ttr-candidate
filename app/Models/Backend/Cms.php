<?php

/**
 * @author : Contriverz
 * @since  : Wed, 29 Mar 2019.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Cms extends Eloquent implements HasMedia
{	
	use HasMediaTrait;
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(130)
            ->height(130);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('my_multi_collection');
    }

    public function addMedia($file){
    	$this->addMedia($file)->toMediaCollection();
    	return true;
    }
}
