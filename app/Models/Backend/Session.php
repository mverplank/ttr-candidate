<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Session
 * 
 * @property string $id
 * @property string $data
 * @property int $expires
 *
 * @package App\Models
 */
class Session extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'expires' => 'int'
	];

	protected $fillable = [
		'data',
		'expires'
	];
}
