<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Giveaway
 * 
 * @property int $id
 * @property int $episodes_id
 * @property string $name
 * @property string $description
 * 
 * @property \App\Models\Backend\Episode $episode
 *
 * @package App\Models\Backend
 */
class Giveaway extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int'
	];

	protected $fillable = [
		'episodes_id',
		'name',
		'description'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}
}
