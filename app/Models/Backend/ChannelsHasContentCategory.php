<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChannelsHasContentCategory
 * 
 * @property int $channels_id
 * @property int $content_categories_id
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\ContentCategory $content_category
 *
 * @package App\Models
 */
class ChannelsHasContentCategory extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int',
		'content_categories_id' => 'int'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class, 'channels_id');
	}

	public function content_category()
	{
		return $this->belongsTo(\App\Models\ContentCategory::class, 'content_categories_id');
	}
}
