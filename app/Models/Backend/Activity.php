<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Activity
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property int $users_id
 * @property string $activity
 *
 * @package App\Models\Backend
 */
class Activity extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'users_id' => 'int'
	];

	protected $dates = [
		'created',
		'updated_at',
		'created_at'
	];

	protected $fillable = [
		'created',
		'users_id',
		'activity',
		'updated_at',
		'created_at'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	/*================   Activity List Count ===============*/

	public function countActivities($order=null, $dir=null, $column_search=array(), $search_value=null, $search_regex=null){

		$activity = Activity::select('users.username', 'activities.created', 'activities.activity')
		                       ->join('users', 'users.id', '=', 'activities.users_id'); 
		// Overall Search 
		if($search_value){
			$activity = $activity->where(function($q) use ($search_value){
            							$q->orWhere('activities.activity' ,'like', '%'.$search_value.'%')
            								->orWhere('activities.created' ,'like', '%'.$search_value.'%')
            								->orWhere('users.username' ,'like', '%'.$search_value.'%');
            						});
		}

		// Sorting by column
		if($order != null){
            $activity = $activity->orderBy($order, $dir);
        }else{
            $activity = $activity->orderBy('id', 'DESC');
        } 
        $activity = $activity->count(); 
		
        // $activity = Activity::count(); 
		return $activity;
	}


	public function getAllActivities($start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$activity = Activity::select('users.username', 'activities.created', 'activities.activity')
		                       ->join('users', 'users.id', '=', 'activities.users_id'); 
		
		// Overall Search 
		if(!empty($search_value)){
			//$activity = $activity->where('name', 'like', 'T%')
			$activity = $activity->where(function($q) use ($search_value){
            							$q->orWhere('activities.activity' ,'like', '%'.$search_value.'%')
            								->orWhere('activities.created' ,'like', '%'.$search_value.'%')
            								->orWhere('users.username' ,'like', '%'.$search_value.'%');
            						});
		}                       

		if(!empty($column_search)){
            foreach($column_search as $column){
                /*if($column['name'] == "total_segments" || $column['name'] == "pending_segments"){

	             	$this->db->having($column['name']." LIKE '%".$column['value']."%'");
	             	//$this->db->having($column['name']." LIKE '%".$search_value."%'");
	             	
	            }else if($column['name'] == "status"){
	            	
            		$status = trim(strtolower($column['value']));
            		//$OR = 1;
            		if(strpos("pending", $status) !== false){
	            		$this->db->having("pending_segments > 0");			            		
	            	}if(strpos("completed", $status) !== false){
            			$this->db->having("pending_segments = 0");				            	
            		}	
            	}else{*/
            		$activity = $activity->where($column['name'].", 'like', '%".$column['value']."%'");
            	//}
            }
        }

      

        if($order != null){
           
           // if($order == "username"){
            	//$activity = $activity->orderBy($order, $dir);
            	/*$activity = $activity->with(
            						['user' => function ($q) use ($order, $dir) {
								  					$q->orderBy($order, $dir);
								  					//$q->where('groups_id', '=', '2');
												}
									]);*/
            	//dd($activity);
			/*	$activity = $activity->with('user')->offset($start)->limit($length)->get(); 
            	$activity = $activity->sortBy(function($q) use ($order){
										        return $q->{$order};
										    });*/
            
          
            $activity = $activity->orderBy($order, $dir);
        }else{
            $activity = $activity->orderBy('id', 'DESC');
        } 
		$activity = $activity->offset($start)->limit($length)->get(); 
		return $activity;	
	}
	
	// add activities
	public function add($new_arr){
		if(!empty($new_arr)){
			$obj = new Activity($new_arr);
			$obj->save();
		}
	}	
	
}
