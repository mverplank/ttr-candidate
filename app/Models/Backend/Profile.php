<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Profile
 * 
 * @property int $id
 * @property int $users_id
 * @property string $title
 * @property string $bio
 * @property string $sufix
 * @property string $firstname
 * @property string $lastname
 * @property string $name
 * @property string $phone
 * @property string $cellphone
 * @property string $skype
 * @property string $skype_phone
 * @property string $notes
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Profile extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int'
	];

	protected $fillable = [
		'users_id',
		'title',
		'bio',
		'sufix',
		'firstname',
		'lastname',
		'name',
		'phone',
		'cellphone',
		'skype',
		'skype_phone',
		'notes'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	/*public function add($data)
	{
		if(!empty($data)){
			$this->users_id    = $data['user_id'];
			$this->title       = $data['title'];
			$this->bio         = $data['bio'];
			$this->sufix       = $data['sufix'];
			$this->firstname   = $data['firstname'];
			$this->lastname    = $data['lastname'];
			$this->name        = $data['name'];
			$this->phone       = $data['phone'];
			$this->cellphone   = $data['cellphone'];
			$this->skype       = $data['skype'];
			//$this->skype_phone = $data['skype_phone'];
			$this->notes       = $data['notes'];
		}
	}*/
}
