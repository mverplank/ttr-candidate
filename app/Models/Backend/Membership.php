<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use Auth;
/**
 * Class Membership
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $discount
 * @property float $quote
 * @property bool $is_online
 * @property string $type
 * @property array $data_json
 * 
 * @property \Illuminate\Database\Eloquent\Collection $inventory_month_items
 * @property \Illuminate\Database\Eloquent\Collection $membership_periods
 * @property \Illuminate\Database\Eloquent\Collection $inventory_items
 * @property \Illuminate\Database\Eloquent\Collection $user_settings
 *
 * @package App\Models\Backend
 */
class Membership extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'discount' => 'int',
		'quote' => 'float',
		'is_online' => 'bool',
		//'data_json' => 'array'
	];

	protected $fillable = [
		'name',
		'description',
		'discount',
		'quote',
		'is_online',
		'type',
		'data_json'
	];

	public function inventory_month_items()
	{
		return $this->hasMany(\App\Models\Backend\InventoryMonthItem::class, 'memberships_id');
	}

	public function membership_periods()
	{
		return $this->hasMany(\App\Models\Backend\MembershipPeriod::class, 'memberships_id');
	}

	public function inventory_items()
	{
		return $this->belongsToMany(\App\Models\Backend\InventoryItem::class, 'memberships_has_inventory_items', 'memberships_id', 'inventory_items_id');
	}

	public function user_settings()
	{
		return $this->hasMany(\App\Models\Backend\UserSetting::class, 'memberships_id');
	}
	// Accessor
	public function setDataJsonAttribute($value)
	{
	     $this->attributes['data_json'] = json_encode($value);
	}
	//Mutator
	public function getDataJsonAttribute($details)
	{
	    return json_decode($details, true);
	}

	public function add($data){

		try{
			if($data['Membership']['type'] == "website-user"){
				unset($data['Membership']['data_json']);
			}
			$membership = new Membership($data['Membership']);
			$membership->save();

			// Save data to MembershipPeriod
			try{
				foreach($data['MembershipPeriod'] as $periods){
					$periods['memberships_id'] = $membership->id;
					$membership_period = new MembershipPeriod($periods);
					$membership_period->save($periods);
				}
				
				// Save data to Inventory Items
				try{
					if(isset($data['InventoryItem']) && !empty($data['InventoryItem'])){
						if(!empty($data['InventoryItem']['InventoryItem'])){
							foreach($data['InventoryItem']['InventoryItem'] as $item){
								$new_arr = array('memberships_id'=>$membership->id, 'inventory_items_id' =>$item );
								$inventory_item = new MembershipsHasInventoryItem;
								$inventory_item->add($new_arr);
							}
						}
					}
				}
				catch(\Exception $e){
			        //echo $e->getMessage();   
			        //  die();
					return false;
			    }
			}
			catch(\Exception $e){
		        //echo $e->getMessage();   
		        //  die();
				return false;
		    }
		    //return true;
		    return $membership->id;
		    
		}
		catch(\Exception $e){
	        //echo $e->getMessage();   
	        //die();
			return false;
	    }
	}
	/**
	* Count all the memberships
	* @return result in the form of object
	* @param type is 
	*/
	public function countAllMemberships($type='',$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$memberships =  $this->select('id', 'name');

		if($type != ''){
			$memberships = $memberships->where('type', $type);
		}

		// Overall Search 
        if(!empty($search_value)){
            $memberships = $memberships->where('name' ,'like', '%'.$search_value.'%');
        }

        // Sorting by column
        if($order != null){
            $memberships = $memberships->orderBy($order, $dir);
        }else{
            $memberships = $memberships->orderBy('id', 'desc');
        } 
		$memberships = $memberships->count();
		return $memberships;
	}

	/**
	* Fetches all the memberships
	* @return result in the form of object
	* @param type is 
	*/
	public function getAllMemberships($type='',$start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$memberships =  $this->select('id', 'name', 'type');

		if($type != ''){
			$memberships = $memberships->where('type', $type);
		}

		// Overall Search 
        if(!empty($search_value)){
            $memberships = $memberships->where('name' ,'like', '%'.$search_value.'%');
        }

        // Sorting by column
        if($order != null){
            $memberships = $memberships->orderBy($order, $dir);
        }else{
            $memberships = $memberships->orderBy('id', 'desc');
        } 
		$memberships = $memberships->offset($start)->limit($length)->get();
		return $memberships;
	}

	public function edit($id, $data){

		try{
			
			$membership = Membership::find($id);
			if(isset($data['Membership']['is_online'])){
				$membership->is_online = $data['Membership']['is_online'];
			}if(isset($data['Membership']['name'])){
				$membership->name = $data['Membership']['name'];
			}if(isset($data['Membership']['description'])){
				$membership->description = $data['Membership']['description'];
			}if(isset($data['Membership']['data_json'])){
				$membership->data_json = $data['Membership']['data_json'];
			}
			$membership->save();

			// Update Membership Period
			$new_mem_arr = array();
			$old_mem_arr = $membership->getIdsOfMembershipPeriodsByMembershipId($id);
			
			if(!empty($data['MembershipPeriod'])){
				foreach($data['MembershipPeriod'] as $mem_period){
					
					if($mem_period['id'] == 0){
						$mem_period['memberships_id'] = $membership->id;
						$membership_period = new MembershipPeriod($mem_period);
						$membership_period->save();
						
					}else{
						
						$new_mem_arr[] = $mem_period['id'];
						$membership_period = MembershipPeriod::find($mem_period['id']);
						if($membership_period){
							if(isset($mem_period['name'])){
								$membership_period->name = $mem_period['name'];
							}if(isset($mem_period['days'])){
								$membership_period->days = $mem_period['days'];
							}if(isset($mem_period['quote'])){
								$membership_period->quote = $mem_period['quote'];
							}
							$membership_period->save();		
						}
					}	
				}

				// Delete the periods which does not come in the edit operation
				foreach($old_mem_arr as $old_period_id){
					if(!in_array($old_period_id, $new_mem_arr)){
						$membership->deleteMembershipPeriodById($old_period_id);
					}
				}
			}	
			
			// Update Inventory Items
			$old_inventory_items = $membership->getInventoryItemsByMembershipId($id);
			if(isset($data['InventoryItem']) && !empty($data['InventoryItem'])){
				if(!empty($data['InventoryItem']['InventoryItem'])){
					if(!empty($old_inventory_items)){
						foreach($old_inventory_items as $item){
							if(!in_array($item, $data['InventoryItem']['InventoryItem'])){
								$membership->deleteAssociationOfMembershipAndIventoryItems($id, $item);
							}
						}
					}
					// Add new inventroy items association
					foreach($data['InventoryItem']['InventoryItem'] as $item){
						if(!in_array($item, $old_inventory_items)){
							$new_arr = array('memberships_id'=>$id, 'inventory_items_id' =>$item );
							$inventory_item = new MembershipsHasInventoryItem;
							$inventory_item->add($new_arr);
						}
					}
				}
			}else{
				if(!empty($old_inventory_items)){
					foreach($old_inventory_items as $item){
						$membership->deleteAssociationOfMembershipAndIventoryItems($id, $item);
					}
				}
			}
			
			return true;
		}
		catch(\Exception $e){
			// echo "herew";
			// echo $e->getMessage();   
			// die('success');
			return false;
	    }
	}
	// Get all membership epriod Ids from the membership id
	public function getIdsOfMembershipPeriodsByMembershipId($id){
		if($id !='' && $id != 0){
			$arr = $new_arr = array();
			$membership_periods = MembershipPeriod::select('id')->where('memberships_id', $id)->get();
			if(!$membership_periods->isEmpty()){
				$arr = $membership_periods->toArray();
				foreach($arr as $val){
					$new_arr[] = $val['id'];
				}
			}
			//echo "<pre>";print_R($new_arr);die('success');
			return $new_arr;
		}
	}
	// Delete membership periods by the membership id
	public function deleteMembershipPeriodById($id){
		if($id !='' && $id != 0){
			$membership_periods = MembershipPeriod::where('id', $id)->delete();
		}
	}

	// Get all inventory items assigned to the membership using membership id
	public function getInventoryItemsByMembershipId($id){
		if($id !='' && $id != 0){
			$arr = $new_arr = array();
			$membership_inventory_item = MembershipsHasInventoryItem::select('inventory_items_id')->where('memberships_id', $id)->get();
			if(!$membership_inventory_item->isEmpty()){
				$arr = $membership_inventory_item->toArray();
				foreach($arr as $val){
					$new_arr[] = $val['inventory_items_id'];
				}
			}
			return $new_arr;
		}
	}

	//Remove Association between Membership and Inventory Items
	public function deleteAssociationOfMembershipAndIventoryItems($membership_id, $inventory_item_id){
		if($membership_id !='' && $membership_id != 0){
			
			MembershipsHasInventoryItem::where('memberships_id', $membership_id)->where('inventory_items_id', $inventory_item_id)->delete();
		}
	}

	// Delete Membership, Association with Inventory Items and Membership Periods
	public function deleteMembership($id){
		
		if($id != '' || $id !=0){
			$membership = Membership::find($id);
			if ($membership->has('membership_periods')) {
			    $membership->membership_periods()->delete();
			}
			/*if ($membership->has('user_settings')) {
			    $membership->user_settings()->delete();
			}*/
			MembershipsHasInventoryItem::where('memberships_id', $id)->delete();
			return $membership->delete();
		}
	}	
}
