<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InventoryCategory
 * 
 * @property int $id
 * @property string $name
 * @property int $off
 * 
 * @property \Illuminate\Database\Eloquent\Collection $inventory_items
 *
 * @package App\Models
 */
class InventoryCategory extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'off' => 'int'
	];

	protected $fillable = [
		'name',
		'off'
	];

	public function inventory_items()
	{
		return $this->hasMany(\App\Models\Backend\InventoryItem::class, 'inventory_categories_id');
	}

	/*Count all inventory Items
     *
     */
    public function countInventoryCategories($order=null, $dir=null, $column_search=array(), $search_value=null, $search_regex=null){
        $inventory_categories = new InventoryCategory();

        // Overall Search 
        if(!empty($search_value)){
            $inventory_categories = $inventory_categories->where('name' ,'like', '%'.$search_value.'%');
        }

        // Sorting by column
        if($order != null){
            $inventory_categories = $inventory_categories->orderBy($order, $dir);
        }else{
            $inventory_categories = $inventory_categories->orderBy('name', 'asc');
        } 
        $inventory_categories = $inventory_categories->count();
        return $inventory_categories;
    }


	/**
	 *Fetch all inventory Items
	 *
	 */
	public function allInventoryCategories($start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		$inventory_categories = new InventoryCategory();
        // die('success');
        // Overall Search 
        if(!empty($search_value)){
            $inventory_categories = $inventory_categories->where('name' ,'like', '%'.$search_value.'%');
        }

        // Sorting by column
        if($order != null){
            $inventory_categories = $inventory_categories->orderBy($order, $dir);
        }else{
            $inventory_categories = $inventory_categories->orderBy('name', 'asc');
        }       
        $inventory_categories = $inventory_categories->offset($start)->limit($length)->get();
        return $inventory_categories;
	}

	/**
	 *Fetch all inventory Items for select box
	 *
	 */
	public function allInventoryCategoriesForSelect($sort_column='name', $sort='asc'){
		$inventory_categories = InventoryCategory::orderBy($sort_column, $sort)->pluck('name', 'id');
		return $inventory_categories;
	}

	/**
	 *Delete Item categories
	 *
	 */
	public function deleteCategory($id){

		if($id != '' || $id !=0){
			$category = InventoryCategory::find($id);
			if ($category->has('inventory_items')) {
			   $category->inventory_items()->delete();
			}
			return $category->delete();
		}
	}
}
