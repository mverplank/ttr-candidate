<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PrefilledForm
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property string $type
 * @property string $name
 * @property string $bio
 * @property array $url_json
 * @property string $video_embed
 * @property string $tags
 * @property string $email
 * @property string $phone
 * @property string $cellphone
 * @property string $skype
 * @property string $skype_phone
 * @property string $contact_name
 * @property string $contact_company_name
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_skype
 * @property int $episode_id
 * @property int $episode_schedules_id
 * @property \Carbon\Carbon $episode_date
 * @property \Carbon\Carbon $episode_time
 * @property string $episode_show_name
 * @property string $episode_channel_name
 * @property string $episode_title
 * @property string $episode_description
 * @property array $episode_giveaways_json
 * @property string $episode_internal_host_notes
 * @property string $episode_events_promote
 * @property string $episode_special_offers
 * @property string $episode_listener_interaction
 * @property string $episode_segment1
 * @property string $episode_segment1_title
 * @property string $episode_segment1_video_url
 * @property string $episode_segment2
 * @property string $episode_segment2_title
 * @property string $episode_segment2_video_url
 * @property string $episode_segment3
 * @property string $episode_segment3_title
 * @property string $episode_segment3_video_url
 * @property string $episode_segment4
 * @property string $episode_segment4_title
 * @property string $episode_segment4_video_url
 * @property string $show_name
 * @property string $show_description
 * @property string $sponsor_name
 * @property string $sponsor_company_name
 * @property string $sponsor_company_description
 * @property array $social_urls_json
 * @property string $sponsor_email
 * @property string $sponsor_phone
 * @property string $sponsor_cellphone
 * @property string $sponsor_skype
 * @property string $sponsor_url
 * @property string $title
 * @property string $firstname
 * @property string $lastname
 * @property string $sufix
 * @property array $video_json
 *
 * @package App\Models
 */
class PrefilledForm extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'url_json' => 'json',
		'episode_id' => 'int',
		'episode_schedules_id' => 'int',
		'episode_giveaways_json' => 'json',
		'social_urls_json' => 'json',
		'video_json' => 'json'
	];

	protected $dates = [
		'created',
		'modified',
		'episode_date',
		'episode_time'
	];

	protected $fillable = [
		'created',
		'modified',
		'type',
		'name',
		'bio',
		'url_json',
		'video_embed',
		'tags',
		'email',
		'phone',
		'cellphone',
		'skype',
		'skype_phone',
		'contact_name',
		'contact_company_name',
		'contact_email',
		'contact_phone',
		'contact_skype',
		'episode_id',
		'episode_schedules_id',
		'episode_date',
		'episode_time',
		'episode_show_name',
		'episode_channel_name',
		'episode_title',
		'episode_description',
		'episode_giveaways_json',
		'episode_internal_host_notes',
		'episode_events_promote',
		'episode_special_offers',
		'episode_listener_interaction',
		'episode_segment1',
		'episode_segment1_title',
		'episode_segment1_video_url',
		'episode_segment2',
		'episode_segment2_title',
		'episode_segment2_video_url',
		'episode_segment3',
		'episode_segment3_title',
		'episode_segment3_video_url',
		'episode_segment4',
		'episode_segment4_title',
		'episode_segment4_video_url',
		'show_name',
		'show_description',
		'sponsor_name',
		'sponsor_company_name',
		'sponsor_company_description',
		'social_urls_json',
		'sponsor_email',
		'sponsor_phone',
		'sponsor_cellphone',
		'sponsor_skype',
		'sponsor_url',
		'title',
		'firstname',
		'lastname',
		'sufix',
		'video_json'
	];
}
