<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContentDataHasTag
 * 
 * @property int $content_data_id
 * @property int $tags_id
 * 
 * @property \App\Models\ContentData $content_datum
 * @property \App\Models\Tag $tag
 *
 * @package App\Models\Backend
 */
class ContentDataHasTag extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'content_data_id' => 'int',
		'tags_id' => 'int'
	];

	public function content_data()
	{
		return $this->belongsTo(\App\Models\Backend\ContentData::class, 'content_data_id');
	}

	public function tag()
	{
		return $this->belongsTo(\App\Models\Backend\Tag::class, 'tags_id');
	}
}
