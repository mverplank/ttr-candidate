<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tag
 * 
 * @property int $id
 * @property string $name
 * @property string $type
 * 
 * @property \Illuminate\Database\Eloquent\Collection $content_data_has_tags
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 *
 * @package App\Models\Backend
 */
class HostHasDefaultTag extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'is_default'
	];
}
