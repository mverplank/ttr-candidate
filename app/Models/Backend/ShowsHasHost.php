<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ShowsHasHost
 * 
 * @property int $id
 * @property int $shows_id
 * @property int $hosts_id
 * @property int $off
 * @property string $type
 * 
 * @property \App\Models\Host $host
 * @property \App\Models\Show $show
 *
 * @package App\Models\Backend
 */
class ShowsHasHost extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'shows_id' => 'int',
		'hosts_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'off',
		'type'
	];

	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}

	public function show()
	{
		return $this->belongsTo(\App\Models\Backend\Show::class, 'shows_id');
	}

	/**
    * @description Get the hosts of the show
    * @param $show_id here is the id of the show and type is the host type, default is host(1)
    * @default Host Types - Host(1), Cohost(2) and Podcast(3)
    * @return the options of the select box
    */
    public function getHostsByType($show_id, $type='host'){
    	$make_hosts = array();
    	if($show_id != 0 ){
    		$hosts = ShowsHasHost::with('host.user.profiles')->where('type', $type)->where('shows_id', $show_id)->get();
    		if($hosts->count() > 0 ){
    			foreach($hosts as $key=>$host){
    				if($host->host->user->profiles->count() > 0){
    					$host_id = $host->host->id;
    					foreach($host->host->user->profiles as $profile){
    						$make_hosts[$key]['id'] = $host_id;
				    		$make_hosts[$key]['text']  = $profile->title.' '.$profile->firstname.' '.$profile->lastname;
    					}
    				}
    			}
    			return $make_hosts;
    		}else{
    			return false;
    		}
    	} 
    	return false;
    }
}
