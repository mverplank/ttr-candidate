<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HostsHasSponsor
 * 
 * @property int $hosts_id
 * @property int $sponsors_id
 * 
 * @property \App\Models\Host $host
 * @property \App\Models\Sponsor $sponsor
 *
 * @package App\Models\Backend
 */
class HostsHasSponsor extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'sponsors_id' => 'int'
	];

	protected $fillable = [
		'hosts_id',
		'sponsors_id'
	];


	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}

	public function sponsor()
	{
		return $this->belongsTo(\App\Models\Backend\Sponsor::class, 'sponsors_id');
	}
	
	public function add($new_arr){
		if(!empty($new_arr)){
			$obj = new HostsHasSponsor($new_arr);
			$obj->save();
		}
	}

	/**
    * @description Get the sponsor's hosts
    * @param $sponsor_id here is the id of the sponsor
    * @return the options of the select box
    */
    public function getHostsByType($sponsor_id){
    	$make_hosts = array();
    	if($sponsor_id != 0 ){
    		
    		$hosts = $this->with('host.user.profiles')->where('hosts_has_sponsors.sponsors_id', $sponsor_id)->where('sponsors_id', $sponsor_id)->get();
    		//echo "<pre>";print_R($hosts);die('success');
    		if($hosts->count() > 0 ){
    			foreach($hosts as $key=>$host){
    				if($host->host->user->profiles->count() > 0){
    					$host_id = $host->host->id;
    					foreach($host->host->user->profiles as $profile){
    						$make_hosts[$key]['id'] = $host_id;
				    		$make_hosts[$key]['text']  = $profile->title.' '.$profile->firstname.' '.$profile->lastname;
    					}
    				}
    			}
    			//echo "<pre>";print_R($make_hosts);die('success');
    			return $make_hosts;
    		}else{
    			return false;
    		}
    	} 
    	return false;
    }
}
