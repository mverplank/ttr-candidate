<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContentCategory
 * 
 * @property int $id
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property string $name
 * @property string $description
 * @property string $keywords
 * @property bool $deleted
 * @property string $is_custom_index
 * @property array $custom_index_params_json
 * @property string $is_custom_index_row
 * @property array $custom_index_row_params_json
 * @property string $is_custom_view
 * @property array $custom_view_params_json
 * @property string $is_custom_box
 * @property array $custom_box_params_json
 * @property bool $is_title
 * @property bool $is_subtitle
 * @property bool $is_content
 * @property bool $is_short_description
 * @property bool $is_url
 * @property bool $is_external_url
 * @property int $is_image
 * @property bool $is_date
 * @property bool $is_time
 * @property bool $is_publish
 * @property bool $is_sortable
 * @property bool $is_featured
 * @property bool $is_tags
 * @property string $label_title
 * @property string $label_subtitle
 * @property string $label_content
 * @property string $label_short_description
 * @property string $label_url
 * @property string $label_external_url
 * @property string $label_images
 * @property string $label_date
 * @property string $label_time
 * @property string $order_field
 * @property string $order_direction
 * 
 * @property \Illuminate\Database\Eloquent\Collection $channels_has_content_categories
 * @property \Illuminate\Database\Eloquent\Collection $content_data
 *
 * @package App\Models
 */
class ContentCategory extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent_id' => 'int',
		'lft' => 'int',
		'rght' => 'int',
		'deleted' => 'bool',
		'custom_index_params_json' => 'json',
		'custom_index_row_params_json' => 'object',
		'custom_view_params_json' => 'json',
		'custom_box_params_json' => 'json',
		'is_title' => 'bool',
		'is_subtitle' => 'bool',
		'is_content' => 'bool',
		'is_short_description' => 'bool',
		'is_url' => 'bool',
		'is_external_url' => 'bool',
		'is_image' => 'int',
		'is_date' => 'bool',
		'is_time' => 'bool',
		'is_publish' => 'bool',
		'is_sortable' => 'bool',
		'is_featured' => 'bool',
		'is_tags' => 'bool'
	];

	protected $fillable = [
		'parent_id',
		'lft',
		'rght',
		'name',
		'description',
		'keywords',
		'deleted',
		'is_custom_index',
		'custom_index_params_json',
		'is_custom_index_row',
		'custom_index_row_params_json',
		'is_custom_view',
		'custom_view_params_json',
		'is_custom_box',
		'custom_box_params_json',
		'is_title',
		'is_subtitle',
		'is_content',
		'is_short_description',
		'is_url',
		'is_external_url',
		'is_image',
		'is_date',
		'is_time',
		'is_publish',
		'is_sortable',
		'is_featured',
		'is_tags',
		'label_title',
		'label_subtitle',
		'label_content',
		'label_short_description',
		'label_url',
		'label_external_url',
		'label_images',
		'label_date',
		'label_time',
		'order_field',
		'order_direction'
	];

	public function channels_has_content_categories()
	{
		return $this->hasMany(\App\Models\Backend\ChannelsHasContentCategory::class, 'content_categories_id');
	}

	public function content_data()
	{
		return $this->hasMany(\App\Models\Backend\ContentData::class, 'content_categories_id');
	}
	
	/**
	* @description -  Add the content category
	* @param $data - category detail
	*/
	public function add_content_categories($data){
		if(!empty($data)){
			try{
				//echo "<pre>";print_R($data);die('success');
				if(!empty($data['ContentCategory'])){

					//Set the main catgeory which this category doesn't belong to any category
					if(empty($data['ContentCategory']['parent_id'])){
						$data['ContentCategory']['parent_id'] = 53;
					}
					if(isset($data['ContentCategory']['is_frontend_generator'])){
						unset($data['ContentCategory']['is_frontend_generator']);
					}
					ContentCategory::insert($data['ContentCategory']);
				}
				return true;
			}catch(\Exception $e){
				return false;
	   		}
		}	
	}

	/**
	* @description -  Edit the content category
	* @param $data - category detail
	* @param $id - category id
	*/
	public function edit_content_categories($id=0, $data){
		if($id != 0){
			if(!empty($data)){
				try{
					if(!empty($data['ContentCategory'])){
						$custom_index_row_params_json = $data['ContentCategory']['custom_index_row_params_json'];
						$data['ContentCategory']['custom_index_row_params_json'] =$custom_index_row_params_json;
						$cate = ContentCategory::where('id', '=' ,$id)->first();
						//echo "<pre>";print_r($cate);die;
						$cate->update($data['ContentCategory']);
					}
					return true;
				}catch(\Exception $e){
					echo $e->getmessage();die();
					return false;
		   		}
			}
		}				
	}

	/**
	* @description - Get the Category  list for the filters
	* @param $type - category type
	* @param $select_options - output in the form of select box
	* @param $selected_cat - show already selected the option used only when the second paramter is true
	* @param $child - if also want to fetch child catgeories then set true otherwise it will only fetch the main categories
	*/
	public function getCategoriesForFilter($main_cat_id=null, $select_options=false, $selected_cat=0, $child=false){
		//echo $selected_cat;die;
		if($select_options){
			$selectCategoryTree = "";
		}
		
		$category_obj = $this->select('id', 'name');
		if(!empty($main_cat_id)){
			$category_obj = $category_obj->where('parent_id', $main_cat_id);
		}

		$arr = array();
		$category_obj = $category_obj->orderBy('name', 'asc')->pluck('name', 'id');
		$get_subcategories = $this->getSubcategoryFromMainCatgeoryId($main_cat_id, $select_options, $child);
				
		if(!empty($get_subcategories)){
			if($select_options){

				$selectCategoryTree .= "<select name='data[ContentCategory][parent_id]' class='selectpicker m-b-0', data-selected-text-format='count', data-style='btn-purple' id='ContentCategoryParentId'>
						<option value=''>All Categories</option>";
				foreach($get_subcategories as $ck=>$cat){	
					
					$level = 1;
					if($selected_cat == $ck){
						$selected = "selected";
					}else{
						$selected = '';
					}			
					if(!empty($cat['children'])){
						$level = 2;
						$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat['name'].'</option>';						
						$selectCategoryTree .= $this->makeCategoryTree($cat, $ck, $level, $selected_cat);
					}else{
						if(!$child){
							$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat.'</option>';
						}else{
							$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat['name'].'</option>';
						}								
					}
				}	
				$selectCategoryTree .= "</select>";
				return $selectCategoryTree;
			}else{
				return $get_subcategories;
			}
		}
		return $category_obj;
	}

	// Fetch Subcategory from the parent catgeory id
	public function getSubcategoryFromMainCatgeoryId($id, $make_tree=false, $child=false){
	
		$subcategory_obj = $this->select('id', 'name')->where('parent_id', $id)->orderBy('name', 'asc')->pluck('name', 'id');
		
		$children = array();
		
		if($subcategory_obj->count() > 0){			
			foreach($subcategory_obj as $subk=>$sub_cat){				
	            # Add the child to the list of children, and get its subchildren	           	 
	            if(!$child){
	            	$children[$subk] = $sub_cat;
	            	$this->getSubcategoryFromMainCatgeoryId($subk, $make_tree, $child);
	            }else{
	            	$children[$subk]['name'] = $sub_cat;
	            	$children[$subk]['children'] = $this->getSubcategoryFromMainCatgeoryId($subk, $make_tree, $child);
	            }
	        }
	    }	
	    return $children;
	}

	// Make category tree with the select options
	public function makeCategoryTree($data, $category_id, $level, $selected_cat=0){
		if(!empty($data)){
			$selectCategoryTree = '';
			$padding = 0;
			$level++;
			
			if(!empty($data['children'])){
				foreach ($data['children'] as $key => $value) {					
					if($selected_cat == $key){
						$selected = "selected";
					}else{
						$selected = '';
					}
					$padding = $level*15;
					$selectCategoryTree .= '<option style="padding-left:'.$padding.'px;" value='.$key.' '.$selected.'>'.$value['name'].'</option>';		
					$selectCategoryTree .= $this->makeCategoryTree($value, $key, $level, $selected_cat);				
				}				
			}else{
				
				if($selected_cat == $category_id){
					$selected = "selected";
				}else{
					$selected = '';
				}
				if($level > 2){
					$level--;
				}				
			}			
		}
		return $selectCategoryTree;
	}

	/** 
	* Get the categories according to the type of the category
    * @return all the categories
	*/
	public function getFlatArrayOfCategoriesByType(){

		$flat_array = array();
		$fetch_categories = $this->select('id', 'parent_id', 'lft', 'rght', 'name')
								 //->where('id', 53)
        						 //->orWhereNotNull('parent_id')
								 ->orderBy('name', 'asc')
								 ->get();

		if($fetch_categories->count() > 0){
			$flat_array = $fetch_categories->toArray();
		}
		
		return $flat_array;
	}
}
