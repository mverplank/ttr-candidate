<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;
use App\Models\Backend\MediaLinking;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
/**
 * Class Banner
 * 
 * @property int $id
 * @property int $sponsors_id
 * @property bool $is_online
 * @property int $off
 * @property string $title
 * @property string $url
 * @property string $notices
 * @property \Carbon\Carbon $view_period_from
 * @property \Carbon\Carbon $view_period_to
 * @property int $max_number_of_views
 * @property int $max_number_of_clicks
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property int $views
 * @property int $clicks
 * @property bool $is_120x300
 * @property bool $is_300x250
 * @property bool $is_625x258
 * @property bool $is_728x90
 * 
 * @property \App\Models\Backend\Sponsor $sponsor
 * @property \Illuminate\Database\Eloquent\Collection $banners_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $channels
 *
 * @package App\Models\Backend
 */
class Banner extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'sponsors_id' => 'int',
		'is_online' => 'bool',
		'off' => 'int',
		'max_number_of_views' => 'int',
		'max_number_of_clicks' => 'int',
		'views' => 'int',
		'clicks' => 'int',
		'is_120x300' => 'bool',
		'is_300x250' => 'bool',
		'is_625x258' => 'bool',
		'is_728x90' => 'bool'
	];

	protected $dates = [
		'view_period_from',
		'view_period_to',
		'created_at',
		'modified_at'
	];

	protected $fillable = [
		'sponsors_id',
		'is_online',
		'off',
		'title',
		'url',
		'notices',
		'view_period_from',
		'view_period_to',
		'max_number_of_views',
		'max_number_of_clicks',
		'created',
		'modified',
		'views',
		'clicks',
		'is_120x300',
		'link_120x300',
		'is_300x250',
		'link_300x250',
		'is_625x258',
		'link_625x258',
		'is_728x90',
		'link_728x90'
	];

	public function sponsor()
	{
		return $this->belongsTo(\App\Models\Backend\Sponsor::class, 'sponsors_id');
	}

	public function banners_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\BannersHasCategory::class, 'banners_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'banners_has_channels', 'banners_id', 'channels_id');
	}

	/**
	* Fetch the list of all banners and their sponsor's name
	*
	*/
	public function countAllBanners($category_id=0, $status=2, $channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$banners = Banner::select('banners.id', 'banners.url', 'banners.title', 'banners.clicks', 'banners.is_online');
						//->leftJoin('sponsors', 'sponsors.id', '=', 'banners.sponsors_id');
		
		if($category_id > 0){
			$banners = $banners->leftJoin('banners_has_categories', 'banners_has_categories.banners_id', '=', 'banners.id')->where('banners_has_categories.categories_id', $category_id);
		}
		if($channel_id > 0){
			$banners = $banners->leftJoin('banners_has_channels', 'banners_has_channels.banners_id', '=', 'banners.id')->where('banners_has_channels.channels_id', $channel_id);
		}
		if($status != 2 ){
			$banners = $banners->where('banners.is_online',$status);
		}
		
		// Overall Search 
        if(!empty($search_value)){
            $banners = $banners->where(function($q) use ($search_value){
    							$q->orWhere('banners.url' ,'like', '%'.$search_value.'%')
    								//->orWhere('sponsors.name' ,'like', '%'.$search_value.'%')
    								->orWhere('banners.title' ,'like', '%'.$search_value.'%')
    								->orWhere('banners.clicks' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $banners = $banners->orderBy($order, $dir);
        }else{
            $banners = $banners->orderBy('name', 'asc');
        } 
        $banners = $banners->count();
		return $banners;
	}

	/**
	* Fetch the list of all banners and their sponsor's name
	*
	*/
	public function getAllBanners($category_id=0, $status=2, $channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
	
		$banners = Banner::select('banners.id', 'banners.url', 'banners.title', 'banners.clicks', 'banners.is_online', 'banners.view_period_from', 'banners.view_period_to');
						//->leftJoin('sponsors', 'sponsors.id', '=', 'banners.sponsors_id');
		
		if($category_id > 0){
			$banners = $banners->leftJoin('banners_has_categories', 'banners_has_categories.banners_id', '=', 'banners.id')->where('banners_has_categories.categories_id', $category_id);
		}				
		if($channel_id > 0){
			$banners = $banners->leftJoin('banners_has_channels', 'banners_has_channels.banners_id', '=', 'banners.id')->where('banners_has_channels.channels_id', $channel_id);
		}
		if($status != 2 ){
			$banners = $banners->where('banners.is_online',$status);
		}
		// Overall Search 
        if(!empty($search_value)){
            $banners = $banners->where(function($q) use ($search_value){
    							$q->orWhere('banners.url' ,'like', '%'.$search_value.'%')
    								//->orWhere('sponsors.name' ,'like', '%'.$search_value.'%')    								
    								->orWhere('banners.title' ,'like', '%'.$search_value.'%')
    								->orWhere('banners.clicks' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $banners = $banners->orderBy($order, $dir);
        }else{
            $banners = $banners->orderBy('title', 'asc');
        } 
		$banners = $banners->offset($start)->limit($length)->get();
		return $banners;
	}

	/**
     * Move All User Images From Tmp Location to permananet folder
     *
     */
    public function procesImage($object, $module){
    	
        $folders = ['player', 'attachments', 'photo', '120x300', '300x250', '625x258', '728x90'];
        $userid = \Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;

            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){

                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);

                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }   
        return 'success';
    }


	/**
	* @description Adding the Banner
	* @param $data is the $request object
	*/	
	public function add($data){
		try{
			// Save data to Banner 
			if(!empty($data['Banner']['view_period_from'])){
				$data['Banner']['view_period_from'] = date_format(date_create($data['Banner']['view_period_from']),"Y-m-d H:i:s");
			}
			if(!empty($data['Banner']['view_period_to'])){
				$data['Banner']['view_period_to'] = date_format(date_create($data['Banner']['view_period_to']),"Y-m-d H:i:s");
			}
			$banner = new Banner($data['Banner']);
			if($banner->save()){
				$media = new MediaLinking();
				$media->add($banner->id, 'Banner');
	        }
			// Save data to Category 
			try{
				$banner_has_cat_arr = array();
				if(isset($data['Category'])){
					if(!empty($data['Category'])){
						if(!empty($data['Category']['Category'])){
							foreach($data['Category']['Category'] as $item){
								$obj = array('banners_id'=>$banner->id , 'categories_id'=>$item);
								$banner_has_cat_arr[] = new BannersHasCategory($obj);
							}
							if(!empty($banner_has_cat_arr)){
								$banner->banners_has_categories()->saveMany($banner_has_cat_arr);
							}
						}
					}
				}
			}
			catch(\Exception $e){
				echo $e->getmessage();die();
				return false;
	   		}
	   		// Save data to Channel 
			try{
				if(isset($data['Channel'])){
					if(!empty($data['Channel'])){
						if(!empty($data['Channel']['Channel'])){
							foreach($data['Channel']['Channel'] as $item){
								$banner->channels()->attach($item);
							}
						}
					}
				}
			}
			catch(\Exception $e){
				echo $e->getmessage();die;
				return false;
	   		}
	   		return true;	   
		}
		catch(\Exception $e){
			echo $e->getmessage();die();
			return false;
	   	}
	}
	
	/**
	* @description Edit the banner
	* @param where $id is banner_id and $data is the $request object for updating the object
	*/	
	public function edit($id=0, $data)
	{
		if($id != 0){
			if(!empty($data)){
				try{
					// Update data to banner
					$banner = Banner::find($id);
					if(!empty($data['Banner']['view_period_from'])){
						$data['Banner']['view_period_from'] = date_format(date_create($data['Banner']['view_period_from']),"Y-m-d H:i:s");
					}
					if(!empty($data['Banner']['view_period_to'])){
						$data['Banner']['view_period_to'] = date_format(date_create($data['Banner']['view_period_to']),"Y-m-d H:i:s");
					}
					$banner->update($data['Banner']);

					//Fetch if exists the banners  categories
					try{
					   	$old_banner = array();
						$old_banners = $banner->banners_has_categories()->get();
						if($old_banners->count() > 0){

				            foreach($old_banners as $baner){
				               $old_banner[] = $baner->categories_id;
				            }
   						}	
   					
   						$banner_categories_arr = array();
					   	if(isset($data['Category'])){
							if(!empty($data['Category'])){
								if(!empty($data['Category']['Category'])){	
       								if(!empty($old_banner)){
       									foreach($old_banner as $baner){
											if(!in_array($baner, $data['Category']['Category'])){
												$del_banner_types_object = BannersHasCategory::where('categories_id' , '=', $baner)->where('banners_id', '=', $banner->id)->delete();
											}
										}
										
										foreach($data['Category']['Category'] as $item){
											if(!in_array($item, $old_banner)){
												$obj = array('banners_id'=>$banner->id , 'categories_id'=>$item);
												
												$banner_categories_arr[] = new BannersHasCategory($obj);
											}
										}
       								}
   									else{
								       	//When there does not exist some banners  categories
								       	foreach($data['Category']['Category'] as $item){
											$obj = array('banners_id'=>$banner->id , 'categories_id'=>$item);
											$banner_categories_arr[] = new BannersHasCategory($obj);
										}  
   									}	
   									
									// Add banners  categories
									if(!empty($banner_categories_arr)){
										$banner->banners_has_categories()->saveMany($banner_categories_arr);
									}
								}
							}
						}else{
							//If no banner categories then delete all the old ones.
							if(!empty($old_banner)){
						       	foreach($old_banner as $baner){	
									$del_banner_types_object = BannersHasCategory::where('categories_id' , '=', $baner)->where('banners_id', '=', $banner->id)->delete();	
								}
						    }
						}
					}
					catch(\Exception $e){
						
						return false;
					}

   					// Update data to channels_has_banners
   					try{
						$old_channels = array();
						$old_banner_channels = $banner->channels()->get();
						if($old_banner_channels->count() > 0){
				            foreach($old_banner_channels as $channel){
				               $old_channels[] = $channel->id;
				            }
				        }	
   						if(isset($data['Channel'])){
							if(!empty($data['Channel'])){
								if(!empty($data['Channel']['Channel'])){	
							       	if(!empty($old_channels)){
							       		foreach($old_channels as $chn){
											if(!in_array($chn, $data['Channel']['Channel'])){
												$banner->channels()->detach($chn);
											}
										}
       								}
      
									foreach($data['Channel']['Channel'] as $item){
										if(!in_array($item, $old_channels)){
											$banner->channels()->attach($item);
										}
									}
								}
							}
						}else{
							if(!empty($old_channels)){
						       	foreach($old_channels as $chn){
									$banner->channels()->detach($chn);	
								}
   							}
						}
					}
					catch(\Exception $e){
						//$e->getMessage();die;
						return false;
					}
					return true;
				}catch(\Exception $e){
					//$e->getMessage();die;
					return false;
				}
			}
		}else{
			return false;
		}
	}

	public function deleteBanner($id){

		if($id != '' || $id !=0){
			$banner = Banner::find($id);
			if ($banner->has('banners_has_categories')) {
				$banner->banners_has_categories()->delete();	
			}
			if ($banner->has('channels')) {
			  	$banner->channels()->detach();
			}
			$banner->delete();
			//Now delete media_linking table entry
			deleteMediaLink('Banner', $id);
			return true;
		}
	}
}
