<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Medium
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property string $title
 * @property string $alt
 * @property string $caption
 * @property string $description
 * @property string $filename
 * @property string $type
 * @property array $meta_json
 *
 * @package App\Models
 */
class MediaLinking extends Eloquent
{
	public $timestamps = true;
	protected $table = 'media_linking';
	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'media_id',
		'main_module',
		'sub_module',
		'module_id',
		'created_at',
		'created_by',
		'updated_at'
	];
	
	public function media()
	{
		return $this->belongsTo(\App\Models\Backend\Media::class, 'media_id');
	}

	/**
     * @description add media by main module
     * @param $module_id here is module id 
     * @param $main_module here is main module
     */
	public function add($module_id ,$main_module){
		
		MediaLinking::where('main_module',$main_module)
                          ->where('module_id',0)
                          ->where('created_by',\Auth::user()->id)
                          ->update(['module_id'=>$module_id]);
	}

	/**
    * @description get all media by main module
    * @param module_id is 0 
    * @param $main_module here is main module
    */
	public function get_media($module_id,$main_module){
		$media = MediaLinking::where('module_id',$module_id)
								->where('main_module',$main_module)
								->where('created_by',\Auth::user()->id)->get();
		return $media;
	}
}
