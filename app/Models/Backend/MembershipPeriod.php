<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MembershipPeriod
 * 
 * @property int $id
 * @property int $memberships_id
 * @property string $name
 * @property int $days
 * @property float $quote
 * @property int $discount
 * @property bool $recurency_payments_enabled
 * 
 * @property \App\Models\Membership $membership
 *
 * @package App\Models
 */
class MembershipPeriod extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'memberships_id' => 'int',
		'days' => 'int',
		'quote' => 'float',
		'discount' => 'int',
		'recurency_payments_enabled' => 'bool'
	];

	protected $fillable = [
		'memberships_id',
		'name',
		'days',
		'quote',
		'discount',
		'recurency_payments_enabled'
	];

	public function membership()
	{
		return $this->belongsTo(\App\Models\Membership::class, 'memberships_id');
	}
}
