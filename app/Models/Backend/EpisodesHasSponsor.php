<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodesHasSponsor
 * 
 * @property int $id
 * @property int $episodes_id
 * @property int $sponsors_id
 * @property int $off
 * 
 * @property \App\Models\Episode $episode
 * @property \App\Models\Sponsor $sponsor
 *
 * @package App\Models
 */
class EpisodesHasSponsor extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'sponsors_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'off'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Episode::class, 'episodes_id');
	}

	public function sponsor()
	{
		return $this->belongsTo(\App\Models\Sponsor::class, 'sponsors_id');
	}
}
