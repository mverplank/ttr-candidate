<?php

/**
 * @author: Contriverz
 * @since : Fri, 17 May 2019 12:00:17.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class ModuleInfo
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property string $module
 * @property string $section
 * @property string $description
 *
 * @package App\Models\Backend
 */
class ModuleInfo extends Eloquent
{
	protected $table   = "module_info";
	public $timestamps = true;

	protected $fillable = [
		'module',
		'section',
		'description'
	];
	
	public function getInfo($data)
	{	
		if(!empty($data)){
			$check = $this->where('module', $data['module'])->where('section', $data['section'])->first();
			if($check){
				return $check;
			}else{
				//No info.
				return false;			
			}			
		}	
		return false;
	}

	public function saveInfo($data)
	{	
		if(!empty($data)){
			$check = $this->where('module', $data['module'])->where('section', $data['section'])->first();
			if($check){
				//Update the description
				$this->where('module', $data['module'])->where('section', $data['section'])->update(['description'=>$data['description']]);
				//echo "<pre>";print_R($check);die('success');
			}else{
				//Add the description
				$this->module      = $data['module'];
				$this->section     = $data['section'];
				$this->description = $data['description'];
				$this->save();
				//echo "<pre>";print_R($check);die('no');
			}
			return true;			
		}	
	}
}
