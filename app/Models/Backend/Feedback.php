<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Feedback
 * 
 * @property int $id
 * @property int $schedules_id
 * @property \Carbon\Carbon $created
 * @property string $name
 * @property string $location
 * @property string $message
 * @property string $ip
 * 
 * @property \App\Models\Schedule $schedule
 *
 * @package App\Models
 */
class Feedback extends Eloquent
{
	protected $table = 'feedbacks';
	public $timestamps = false;

	protected $casts = [
		'schedules_id' => 'int'
	];

	protected $dates = [
		'created'
	];

	protected $fillable = [
		'schedules_id',
		'created',
		'name',
		'location',
		'message',
		'ip'
	];

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Schedule::class, 'schedules_id');
	}
}
