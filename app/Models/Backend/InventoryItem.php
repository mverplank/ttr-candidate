<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class InventoryItem
 * 
 * @property int $id
 * @property int $users_id
 * @property int $inventory_categories_id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property string $id_custom
 * @property bool $is_online
 * @property bool $is_accepted
 * @property bool $is_on_initial_email
 * @property string $name
 * @property string $description
 * @property float $value
 * @property string $type
 * @property string $code
 * 
 * @property \App\Models\InventoryCategory $inventory_category
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $inventory_item_urls
 * @property \Illuminate\Database\Eloquent\Collection $inventory_month_items
 * @property \Illuminate\Database\Eloquent\Collection $memberships
 *
 * @package App\Models\Backend
 */
class InventoryItem extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'users_id' => 'int',
		'inventory_categories_id' => 'int',
		'is_online' => 'bool',
		'is_accepted' => 'bool',
		'is_on_initial_email' => 'bool',
		'value' => 'float'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'users_id',
		'inventory_categories_id',
		'id_custom',
		'is_online',
		'is_accepted',
		'is_on_initial_email',
		'name',
		'description',
		'value',
		'type',
		'code'
	];

	public function inventory_category()
	{
		return $this->belongsTo(\App\Models\Backend\InventoryCategory::class, 'inventory_categories_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	public function inventory_item_urls()
	{
		return $this->hasMany(\App\Models\Backend\InventoryItemUrl::class, 'inventory_items_id');
	}

	public function inventory_month_items()
	{
		return $this->hasMany(\App\Models\Backend\InventoryMonthItem::class, 'inventory_items_id');
	}

	public function memberships()
	{
		return $this->belongsToMany(\App\Models\Backend\Membership::class, 'memberships_has_inventory_items', 'inventory_items_id', 'memberships_id');
	}

	/**
	 * Fetch all Inventory Items of the current user
	 * @return All the inventory items object
	 */
	public function countItemsOfCurrentUser($current_user_id, $is_online=null, $sorting_column="category_name", $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		if($current_user_id != 0){
			
			$results = InventoryItem::select('inventory_items.*', 'inventory_categories.name as category_name', 'profiles.name as submitted_by')
									->leftJoin('inventory_categories', 'inventory_categories.id', '=', 'inventory_items.inventory_categories_id')
									->leftJoin('users', 'inventory_items.users_id', '=', 'users.id')
									->leftJoin('profiles', 'users.id', '=', 'profiles.users_id');
									//->where('inventory_items.users_id',1);
									//->where('inventory_items.users_id',$current_user_id);
			// Overall Search 
			if(!empty($search_value)){
				$results = $results->where(function($q) use ($search_value){
	            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
	            								->orWhere('inventory_items.name' ,'like', '%'.$search_value.'%');
	            							});
			}

			// Sorting by column
			if($order != null){
	            $results = $results->orderBy($order, $dir);
	        }else{
	            $results = $results->orderBy($sorting_column, 'asc');
	        } 
	     
			if($sorting_column == "category_name"){
				$results = $results->where('is_online', 1)->where('is_accepted', 1);
			}else{
				if($is_online == 1 || $is_online == 0){
					$results = $results->where('is_online',$is_online);
				}
			}			
			$results = $results->count();
			return $results;
		}
	}

	/**
	 * Fetch all Inventory Items of the current user
	 * @return All the inventory items object
	 */
	public function findItemsOfCurrentUser($current_user_id, $is_online=2, $sorting_column="category_name",$start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null, $pagination=true)
	{

		if($current_user_id != 0){

			$results = InventoryItem::select('inventory_items.*', 'inventory_categories.name as category_name', 'profiles.name as submitted_by', 'inventory_items.created_at')->with('inventory_month_items')
									->leftJoin('inventory_categories', 'inventory_categories.id', '=', 'inventory_items.inventory_categories_id')
									->leftJoin('users', 'inventory_items.users_id', '=', 'users.id')
									->leftJoin('profiles', 'users.id', '=', 'profiles.users_id');//->where('inventory_items.users_id', $current_user_id);
		
			// Overall Search 
			if(!empty($search_value)){
				$results = $results->where(function($q) use ($search_value){
	            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
	            								->orWhere('inventory_items.name' ,'like', '%'.$search_value.'%');
	            							});
			}

			// Sorting by column
			if($order != null){
	            $results = $results->orderBy($order, $dir);
	        }else{
	            $results = $results->orderBy($sorting_column, 'asc');
	        } 
	        //Pagination
	        if($pagination){
	        	$results = $results->offset($start)->limit($length);
	        }			
					
			if($sorting_column == "category_name"){
				$results = $results->where('is_online', 1)->where('is_accepted', 1);
			}else{
				if($is_online == 1 || $is_online == 0){
					$results = $results->where('is_online',$is_online);
				}
			}					
			//$results = $results->toSql();
			$results = $results->get();
			return $results;
		}
	}

	/**
	 * Count all Inventory Items of the partners
	 * @return count of the inventory item's
	 */
	public function countItemsOfPartners($current_user_id=0, $is_accepted=0, $is_online=2, $sorting_column="category_name", $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{		
		$results = InventoryItem::select('inventory_items.*', 'inventory_categories.name as category_name', 'profiles.name as submitted_by')
								->leftJoin('inventory_categories', 'inventory_categories.id', '=', 'inventory_items.inventory_categories_id')
								->leftJoin('users', 'inventory_items.users_id', '=', 'users.id')
								->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
								->where('users.groups_id',4);
		
		if($current_user_id !=0 ){
			$results = $results->where('inventory_items.users_id',$current_user_id);			
		}else{
			$results = $results->where('inventory_items.is_accepted',$is_accepted);
		}
		// Overall Search 
		if(!empty($search_value)){
			$results = $results->where(function($q) use ($search_value){
            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
            								->orWhere('inventory_items.name' ,'like', '%'.$search_value.'%');
            							});
		}

		// Sorting by column
		if($order != null){
            $results = $results->orderBy($order, $dir);
        }else{
            $results = $results->orderBy($sorting_column, 'asc');
        } 
		
		if($is_online == 1 || $is_online == 0){
			$results = $results->where('is_online',$is_online);
		}				
		
		$results = $results->count();									
		return $results;		
	}

	/**
	 * Fetch all Inventory Items of the partners
	 * @return All the inventory items object
	 */
	public function findItemsOfPartners($current_user_id=0, $is_accepted=0, $is_online=2, $sorting_column="category_name", $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{		

		//echo $is_online;die;
		$results = InventoryItem::select('inventory_items.*', 'inventory_categories.name as category_name', 'profiles.name as submitted_by', 'inventory_items.created_at')
								->leftJoin('inventory_categories', 'inventory_categories.id', '=', 'inventory_items.inventory_categories_id')
								->leftJoin('users', 'inventory_items.users_id', '=', 'users.id')
								->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
								->where('users.groups_id',4);
								
		if($current_user_id !=0 ){
			$results = $results->where('inventory_items.users_id',$current_user_id);			
		}else{
			$results = $results->where('inventory_items.is_accepted',$is_accepted);
		}
								
		// Overall Search 
		if(!empty($search_value)){
			$results = $results->where(function($q) use ($search_value){
            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
            								->orWhere('inventory_items.name' ,'like', '%'.$search_value.'%');
            							});
		}

		// Sorting by column
		if($order != null){
            $results = $results->orderBy($order, $dir);
        }else{
            $results = $results->orderBy($sorting_column, 'asc');
        } 
        // Online Status

        if($is_online == 1 || $is_online == 0){
			$results = $results->where('is_online',$is_online);
		}
        //Pagination
		$results = $results->offset($start)->limit($length);
									
		$results = $results->get();
		//$results = $results->toSql();
		//echo "<pre>"; print_R($results);die;
		return $results;		
	}

	/**
	 * Fetch all Inventory Items
	 * @return All the inventory items object
	 */
	public function getAllInventoryItems(){
		$items =  InventoryItem::orderBy('name', 'asc')->get();
		return $items;
	}

	/**
	 * @description Add Inventory Item
	 * @param $data having $request object
	 */
	public function add($data){
		//echo "<pre>";print_R($data);die('success');
		if(!empty($data)){
			try{
				$inventory_item = new InventoryItem($data['InventoryItem']);
				if($inventory_item->save()){
					$media = new MediaLinking();
					$media->add($inventory_item->id, 'InventoryItem');
		        }
				try{					
					if(!empty($data['InventoryItemUrl'])){
						foreach($data['InventoryItemUrl'] as $url){
							$urls_objects_arr[] = new InventoryItemUrl($url);
						}
						$inventory_item->inventory_item_urls()->saveMany($urls_objects_arr);
					}
				}
				catch(\Exception $e){
					return false;
				}
				return $inventory_item->id;
			}
			catch(\Exception $e){				
				return false;
		    }
		}		
	}


	/**
	 * @description Edit the Inventory Item
	 * @param $id = id of the item, $data = $request data
	*/
	public function edit($id, $data){
		try{
			
			$inventory_obj = InventoryItem::find($id);
			$inventory_obj->update($data['InventoryItem']);

		    // Now save the inventory urls data
		    try{					
				if(!empty($data['InventoryItemUrl'])){
					$relational_data = $inventory_obj->with('inventory_item_urls')->get();
					
					foreach($data['InventoryItemUrl'] as $url){
						
						if(!empty($url['id'])){
							
							$urlObj = InventoryItemUrl::find($url['id']);
							$urlObj->url = $url['url'];
							$urlObj->description = $url['description'];
							$urlObj->save();
						}else{
							$urlObj = new InventoryItemUrl($url);
							$inventory_obj->inventory_item_urls()->save($urlObj);
						}
					}
				}
			}
			catch(\Exception $e){	
				echo $e->getMessage();		
				die('dscsdnckjsdc');
				return false;
		    }
		    
			$inventory_obj->save();
			return true;
		}
		catch(\Exception $e){
			/*echo $e->getMessage();   
			die('success');*/
			return false;
	    }
	}

	/******  Delete the Inventory item and its related data with the Host and Channels   ******/
	public function deleteItem($id){
		if($id != '' || $id !=0){
			$inventory_item = InventoryItem::find($id);
			if($inventory_item->inventory_item_urls()->exists()){
				$inventory_item->inventory_item_urls()->delete();
			}	
			deleteMediaLink('InventoryItem', $id);		
			return $inventory_item->delete();
		}
	}	

	/*
    * Move All Host related images From Tmp Location to permananet folder
    *
    */
    public function procesImage($object, $module){
    
        $folders = ['attachments', 'image', 'files'];
        $userid = \Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }    
       
        return 'success';
    }
}
