<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Medium
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property string $title
 * @property string $alt
 * @property string $caption
 * @property string $description
 * @property string $filename
 * @property string $type
 * @property array $meta_json
 *
 * @package App\Models
 */
class Media extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'meta_json' => 'object'
	];

	protected $dates = [
		'created_at'
	];

	protected $fillable = [
		'created_at',
		'title',
		'alt',
		'caption',
		'description',
		'filename',
		'type',
		'height',
		'width',
		'meta_json',
		'uploaded_by',
		'uploaded_area'
	];
	
	public function medialinking()
	{
		return $this->hasMany(\App\Models\Backend\MediaLinking::class, 'media_id');
	}
}
