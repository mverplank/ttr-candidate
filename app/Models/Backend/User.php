<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;
use Illuminate\Support\Facades\Storage;
use Reliese\Database\Eloquent\Model as Eloquent;
use Auth;
/**
 * Class User
 * 
 * @property int $id
 * @property int $groups_id
 * @property bool $deleted
 * @property string $status
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $access_token
 * @property string $oauth_uid
 * @property string $oauth_provider
 * @property \Carbon\Carbon $created_at
 * @property string $created_ip
 * @property \Carbon\Carbon $modified
 * @property \Carbon\Carbon $last_login
 * @property string $origin
 * 
 * @property \App\Models\Group $group
 * @property \Illuminate\Database\Eloquent\Collection $episode_scheduled_shares
 * @property \Illuminate\Database\Eloquent\Collection $guests
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 * @property \Illuminate\Database\Eloquent\Collection $inventory_items
 * @property \Illuminate\Database\Eloquent\Collection $profiles
 * @property \Illuminate\Database\Eloquent\Collection $tokens
 * @property \Illuminate\Database\Eloquent\Collection $user_settings
 *
 * @package App\Models\Backend
 */
class User extends Eloquent
{

	public $timestamps = true;

	protected $casts = [
		'groups_id' => 'int',
		'deleted' => 'bool'
	];

	protected $dates = [
		'created',
		'modified',
		'updated_at',
		'last_login'
	];

	protected $hidden = [
		'password',
		'access_token'
	];

	protected $fillable = [
		'groups_id',
		'deleted',
		'status',
		'username',
		'email',
		'password',
		'access_token',
		'oauth_uid',
		'oauth_provider',
		'created_at',
		'created_ip',
		'modified',
		'updated_at',
		'last_login',
		'origin',
		'last_login_at',
        'last_login_ip',
	];

	public function group()
	{
		return $this->belongsTo(\App\Models\Backend\Group::class, 'groups_id');
	}

	public function episode_scheduled_shares()
	{
		return $this->hasMany(\App\Models\Backend\EpisodeScheduledShare::class, 'users_id');
	}

	public function guests()
	{
		return $this->hasMany(\App\Models\Backend\Guest::class, 'users_id');
	}

	public function hosts()
	{
		return $this->hasMany(\App\Models\Backend\Host::class, 'users_id');
	}

	public function inventory_items()
	{
		return $this->hasMany(\App\Models\Backend\InventoryItem::class, 'users_id');
	}

	public function profiles()
	{
		return $this->hasMany(\App\Models\Backend\Profile::class, 'users_id');
	}

	public function tokens()
	{
		return $this->hasMany(\App\Models\Backend\Token::class, 'users_id');
	}

	public function user_settings()
	{
		return $this->hasMany(\App\Models\Backend\UserSetting::class, 'users_id');
	}

	
	// Count Users
	public function countUsers($type=0, $order=null, $dir=null, $column_search=array(), $search_value=null, $search_regex=null){

		$users = User::select('users.id','users.username','users.email','users.last_login', 'profiles.bio', 'profiles.name')
						->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
						->where(function($q) {
					        $q->where('users.id', '!=', Auth::id())
					          ->where('users.deleted',0);
					           //->where('users.groups_id','!=',$type);
					    });

		if($type !=0 ){
			
			if( $type == 2 ){
				$users = $users->leftJoin('hosts', 'hosts.users_id', '=', 'users.id')
								->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
								->where('host_types_id', '=', 1);
			}else if( $type == 5  ){
				$users = $users->leftJoin('hosts', 'hosts.users_id', '=', 'users.id')
								->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
								->where('host_types_id', '=', 2);
			}else if( $type == 6 ){
				$users = $users->leftJoin('hosts', 'hosts.users_id', '=', 'users.id')
								->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
								->where('host_types_id', '=', 3);
			}else{
				$users = $users->where('users.groups_id', $type);
			}
		}

		// Overall Search 
		if(!empty($search_value)){
			$users = $users->where(function($q) use ($search_value){
    						$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
								->orWhere('email' ,'like', '%'.$search_value.'%')
								->orWhere('last_login' ,'like', '%'.$search_value.'%');
    						});
		}

		// Sorting by column
		if($order != null){
            $users = $users->orderBy($order, $dir);
        }else{
            $users = $users->orderBy('users.created_at', 'desc');
        } 

		$users = $users->count(); 
		return $users;
	}

	// Get all users 
	public function getAllUsers($type=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$users = $this->select('users.id', 'users.groups_id', 'users.username','users.email','users.last_login', 'users.status', 'profiles.bio', 'profiles.name', 'profiles.title', 'profiles.lastname', 'profiles.firstname', 'profiles.sufix')
						->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
						->where(function($q) {
					        $q->where('users.id', '!=', Auth::id())
					          ->where('users.deleted',0);
					          //->where('users.groups_id','!=',2);
					    });
		if($type != 0 ){
			if( $type == 2 ){
				$users = $users->leftJoin('hosts', 'hosts.users_id', '=', 'users.id')
								->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
								->where('host_types_id', '=', 1);
			}else if( $type == 5  ){
				$users = $users->leftJoin('hosts', 'hosts.users_id', '=', 'users.id')
								->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
								->where('host_types_id', '=', 2);
			}else if( $type == 6 ){
				$users = $users->leftJoin('hosts', 'hosts.users_id', '=', 'users.id')
								->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
								->where('host_types_id', '=', 3);
			}else{
				$users = $users->where('users.groups_id', $type);
			}
		}

		// Overall Search 
		if(!empty($search_value)){
			$users = $users->where(function($q) use ($search_value){
            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
            								->orWhere('email' ,'like', '%'.$search_value.'%')
            								->orWhere('last_login' ,'like', '%'.$search_value.'%');
            						});
			if($type == 4){
				$users = $users->orWhere('bio' ,'like', '%'.$search_value.'%');
			}
		}

		// Sorting by column
		if($order != null){
            $users = $users->orderBy($order, $dir);
        }else{
            $users = $users->orderBy('users.created_at', 'desc');
        } 
		
		$users = $users->offset($start)->limit($length)->get();
		return $users;
	}

	// Get Members count for members list Page
	public function countMembers($order=null, $dir=null, $column_search=array(), $search_value=null, $search_regex=null){
		$users = User::select('users.id','users.username', 'profiles.name', 'user_settings.membership_end', 'memberships.name as membership')
						->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
						->leftJoin('user_settings', 'users.id', '=', 'user_settings.users_id')
						->leftJoin('memberships', 'memberships.id', '=', 'user_settings.memberships_id')
						->where(function($q) {
					        $q->where('users.id', '!=', Auth::id())
					          ->where('users.groups_id', 3)
					          ->where('users.deleted',0);
					    });

		// Overall Search 
		if(!empty($search_value)){
			$users = $users->where(function($q) use ($search_value){
            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
            								->orWhere('email' ,'like', '%'.$search_value.'%')
            								->orWhere('last_login' ,'like', '%'.$search_value.'%');
            						});
		}

		// Sorting by column
		if($order != null){
            $users = $users->orderBy($order, $dir);
        }else{
            $users = $users->orderBy('users.created_at', 'desc');
        } 
		$users = $users->count();
		return $users;
	}

	// Get all Members for members list Page
	public function getAllMembers( $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		$users = User::select('users.id','users.username', 'profiles.name', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix', 'user_settings.membership_end', 'memberships.name as membership')
						->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
						->leftJoin('user_settings', 'users.id', '=', 'user_settings.users_id')
						->leftJoin('memberships', 'memberships.id', '=', 'user_settings.memberships_id')
						->where(function($q) {
					        $q->where('users.id', '!=', Auth::id())
					          ->where('users.groups_id', 3)
					          ->where('users.deleted',0);
					    });

		// Overall Search 
		if(!empty($search_value)){
			$users = $users->where(function($q) use ($search_value){
            							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
            								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
            								->orWhere('memberships.name' ,'like', '%'.$search_value.'%')
            								->orWhere('user_settings.membership_end' ,'like', '%'.date('Y-m-d', strtotime($search_value)).'%');
            						});
		}

		// Sorting by column
		if($order != null){
            $users = $users->orderBy($order, $dir);
        }else{
            $users = $users->orderBy('users.created_at', 'desc');
        } 
        //Pagination
		$users = $users->offset($start)->limit($length)->get();		
		return $users;
	}

	 /*
    * Move All User Images From Tmp Location to permananet folder
    *
    */

    public function procesImage($object, $module){
    	
        $folders = ['player', 'zapbox', 'zapboxsmall', 'widget', 'cover', 'attachments', 'photo'];
        $userid = Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }           
        return 'success';
    }


	// Add new user
	public function add($data, $model="User"){
		try{
			if(isset($data[$model])){
				$data['User'] = $data[$model];
			}
			$data['User']['password'] = bcrypt($data['User']['password']);
			if(isset($data['User']['groups_id_radio'])){
				if($data['User']['groups_id_radio'] == 2 || $data['User']['groups_id_radio'] > 4){
					$data['User']['groups_id'] = 2;
					$model = "Host";
				}else{
					$data['User']['groups_id'] = $data['User']['groups_id_radio'];
				}				
			}

			$user = new User($data['User']);

			// Initialize the media linking	
			$media = new MediaLinking();
			if($user->save()){	

				// Link the media	
				if($model =='User'){
					$media->add($user->id, 'User');
				}
				if($model =='Partner'){
					$media->add($user->id, 'Partner');
				}			
	        }
		
			// Save data to profiles
			try{
				$data['Profile']['users_id'] = $user->id;
				$data['Profile']['name'] = $data['Profile']['firstname']." ".$data['Profile']['lastname'];
				$profile = new Profile($data['Profile']);
				$profile->save();

				// Also save into the hosts table if this is a host
				try{
					if($data['User']['groups_id_radio'] == 2 || $data['User']['groups_id_radio'] > 4){
						$currentdate = date('Y-m-d H:i:s');						
						$data['Host']['users_id'] = $user->id;
						$data['Host']['bio']      = $user->bio;
						$host  = new Host($data['Host']);

						if($host->save()){
							$host_type = '';
							if($data['User']['groups_id_radio'] == 2){
								$host_type = 1;
							}else if($data['User']['groups_id_radio'] == 5){
								$host_type = 2;
							}else if($data['User']['groups_id_radio'] == 6){
								$host_type = 3;
							}
							$obj = array('hosts_id'=>$host->id , 'host_types_id'=>$host_type);
								$host_types_object = new HostsHasHostType($obj);
							if(!empty($obj)){
								$host->hosts_has_host_types()->save($host_types_object);
							}
							if($model =='Host'){
								$media->add($host->id, 'Host');
							}

							// Add Transaction Report
							try{
								$fullname = $profile->title . ' ' . $profile->firstname . ' ' . $profile->lastname . ' ' . $profile->sufix;
								$newactivity = array(
									'created'  => $currentdate,
									'users_id' => Auth::id(),
									'activity' =>'ADDED HOST "'. $fullname .'" (ID:' . $host->id .')'
								);
								$activity = new Activity();								
								$activity->add($newactivity);
							}
							catch(\Exception $e){
								return false;
							}
						}
					}
				}
				catch(\Exception $e){
					return false;
			    }
				
				// Save data to usersettings
				if(isset($data['UserSetting'])){
					try{
						$data['UserSetting']['users_id'] = $user->id;
						$data['UserSetting']['mailing_newsletter'] = $data['UserSetting']['mailing_newsletter_radio'];
						if(isset($data['UserSetting']['membership_end'])){
							$data['UserSetting']['membership_end'] = date_format(date_create($data['UserSetting']['membership_end']), "Y-m-d");
						}
						$user_setting = new UserSetting($data['UserSetting']);
						$user_setting->save();
					}
					catch(\Exception $e){
						return false;
				    }
				}
			}
			catch(\Exception $e){
				return false;
		    }
		    return $user->id;
		    
		}
		catch(\Exception $e){
			return false;
	    }
	}
	/**
	 * @description: Edit the user
	 * @param: $id = id of the user, $data = $request data
	*/
	public function edit($id, $data){
		try{
			
			$user_obj = User::find($id);
			if(isset($data['Partner'])){
				$data['User'] = $data['Partner'];
			}
			if(isset($data['User']['username'])){
				$user_obj->username = $data['User']['username'];
			}if(isset($data['User']['status'])){
				$user_obj->status = $data['User']['status'];
			}if(isset($data['User']['email'])){
				$user_obj->email = $data['User']['email'];
			}if(isset($data['User']['password'])){
				$user_obj->password = bcrypt($data['User']['password']);
			}if(isset($data['User']['groups_id_radio'])){
				$user_obj->groups_id = $data['User']['groups_id_radio'];
			}if(isset($data['User']['bio'])){
				$user_obj->bio = $data['User']['bio'];
			}
			$user_obj->save();
			
			// Save data to Profile
			$profile = Profile::find($data['Profile']['id']);
			$profile->update($data['Profile']);
					
			// Save data into User setting
			if(in_array("UserSetting", $data)){
				$user_setting = UserSetting::find($data['UserSetting']['id']);
				
				if(!empty($user_setting)){
					if(isset($data['UserSetting']['mailing_newsletter_radio'])){
						$user_setting->mailing_newsletter = $data['UserSetting']['mailing_newsletter_radio'];
					}
					if(isset($data['UserSetting']['membership_end'])){
						$user_setting->membership_end = date_format(date_create($data['UserSetting']['membership_end']), "Y-m-d");
					}
					if(isset($data['UserSetting']['memberships_id'])){
						$user_setting->memberships_id = $data['UserSetting']['memberships_id'];
					}
					$user_setting->save();
				}
			}
			return true;
		}
		catch(\Exception $e){
			return false;
	    }
	}

	public function deleteUser($id){
		
		if($id != '' || $id !=0){
			$user = User::find($id);
			/*	if ($user->has('profiles')) {
			    $user->profiles()->delete();
			}
			if ($user->has('user_settings')) {
			    $user->user_settings()->delete();
			}

			return $user->delete();*/
			return $user->update(['deleted'=>1]);
		}
	}	
	/**
	 * @description - Get the host if from the user's id
	 * @param  - user id
	 * @return - host id
	 */
	public function getHostId($id=0){
		$host_id = 0;
		if($id != 0){
			$user = $this->find($id);
			$host = $user->hosts()->first();
			if($host){
				$host_id = $host->id; 
			}
			return $host_id;
		}
	}
}
