<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Permalink
 * 
 * @property int $id
 * @property string $permalink
 * @property string $model
 * @property int $model_id
 * @property string $url
 *
 * @package App\Models
 */
class Permalink extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'model_id' => 'int'
	];

	protected $fillable = [
		'permalink',
		'model',
		'model_id',
		'url'
	];
}
