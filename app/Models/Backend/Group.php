<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Group
 * 
 * @property int $id
 * @property string $name
 * @property string $desc
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Group extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'desc'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'groups_id');
	}
}
