<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Models\Backend\MediaLinking;
use DB;
/**
 * Class Show
 * 
 * @property int $id
 * @property string $channels_ids
 * @property int $deleted
 * @property string $name
 * @property string $description
 * @property int $duration
 * @property bool $is_encore
 * @property bool $is_online
 * @property bool $is_featured
 * @property string $file
 * @property string $stream
 * @property int $favorites
 * @property bool $is_created_by_host
 * 
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 * @property \Illuminate\Database\Eloquent\Collection $shows_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 *
 * @package App\Models\Backend
 */
class Show extends Eloquent
{
	public $timestamps = true;

	protected $casts  = [
		'deleted'     => 'int',
		'duration'    => 'int',
		'is_encore'   => 'bool',
		'is_online'   => 'bool',
		'is_featured' => 'bool',
		'favorites'   => 'int',
		'is_created_by_host' => 'bool'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'channels_ids',
		'deleted',
		'name',
		'description',
		'duration',
		'is_encore',
		'is_online',
		'is_featured',
		'file',
		'stream',
		'favorites',
		'is_created_by_host'
	];

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_shows', 'shows_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->hasMany(\App\Models\Backend\Episode::class, 'shows_id');
	}

	public function schedules()
	{
		return $this->hasMany(\App\Models\Backend\Schedule::class, 'shows_id');
	}

	public function shows_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\ShowsHasCategory::class, 'shows_id');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'shows_has_hosts', 'shows_id', 'hosts_id')
					->withPivot('id', 'off', 'type');
	}

	// Fetch all shows with id for the filters
	public function getShowsForFilter(){
		$show_obj = $this->select('id', 'name')->where('deleted', 0)->orderBy('name', 'asc')->pluck('name', 'id');
		return $show_obj;
	}

	/**
	* Count all the shows
	* @return result as a total
	*/
	public function countAllShows($channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$shows = new Show();
		if(!empty($channel_id)){
			$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id')->where('channels_has_shows.channels_id', '=', $channel_id);
		}

		//Fetch who are not deleted
		$shows = $shows->where('shows.deleted', 0);
		
		// Overall Search 
        if(!empty($search_value)){
        	//if(empty($channel_id)){
            	$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id');
        	//}
       		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id')
           			->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id') 
           			->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id')
           			->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');
            $shows = $shows->where(function($q) use ($search_value){
							$q->orWhere('shows.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('channels.name' ,'like', '%'.$search_value.'%');
						});
    								
        }

        // Sorting by column
        if($order != null){
        	if($order == "profiles.name"){
        		$shows = $shows->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id');
	        	$shows = $shows->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id');
	        	$shows = $shows->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');
	        	
        	}else if($order == "channels.name"){
        		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
        	}
    		$shows = $shows->orderBy($order, $dir);
        }else{
        	$shows = $shows->orderBy('shows.name', 'asc');
        }

		$shows = $shows->count();
		return $shows;
	}

	/**
	* Fetches all the shows
	* @return result in the form of object
	*/
	public function getAllShows($channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$shows = Show::select('shows.id', 'shows.name', DB::raw('COUNT(channels_has_featured_shows.id) as featured'));
		
		if(!empty($channel_id)){
			$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id')->where('channels_has_shows.channels_id', '=', $channel_id);
		}
		//Get featured shows
		$shows = $shows->leftJoin('channels_has_featured_shows', 'channels_has_featured_shows.shows_id', '=', 'shows.id')->groupBy('shows.id');

		//Fetch who are not deleted
		$shows = $shows->where('shows.deleted', 0);

		// Overall Search 
        if(!empty($search_value)){
        	//if(empty($channel_id)){
            	$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id');
        	//}
        	$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
        	$shows = $shows->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id');
        	$shows = $shows->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id');
        	$shows = $shows->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');

            $shows = $shows->where(function($q) use ($search_value){
							$q->orWhere('shows.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('channels.name' ,'like', '%'.$search_value.'%');
						});
        }

        // Sorting by column
        if($order != null){
        	
    		if($order == "profiles.name"){
        		$shows = $shows->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id');
	        	$shows = $shows->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id');
	        	$shows = $shows->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');
	        	
        	}else if($order == "channels.name"){
        		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
        	}
    		$shows = $shows->orderBy($order, $dir);
        	          
        }else{
        	$shows = $shows->orderBy('shows.name', 'asc');
        }
       
        $shows = $shows->offset($start)->limit($length)->get();
		return $shows;
	}	

	/**
	* Fetches all the hosts/co-hosts of the shows
	* @param $show_id is the id of the show
	* @return result in the form of object
	*/
	public function getShowHostsCohosts($show_id, $html=false){
		$hosts = array();
		if(!empty($show_id)){
			$shows = Show::where('id', $show_id)->with('hosts.user.profiles')->get();
			if($shows->count() > 0){
				foreach($shows as $show){
					if($show->hosts->count() > 0){
						foreach($show->hosts as $host){
							if($host->user->profiles->count() > 0){
								foreach($host->user->profiles as $profile){
									$hosts[] = $profile->name;
								}
							}
						}
					}
				}
			}
			if($html){
				if(!empty($hosts)){
					return implode($hosts,',');
				}else{
					return '';
				}			
			}
			return $hosts;
		}
	}

	/**
	* Fetches all the channels of the shows
	* @param $show_id is the id of the show
	* @return result in the form of object
	*/
	public function getShowChannels($show_id, $html=false){
		$channels = array();
		if(!empty($show_id)){

			$shows = Show::where('id', $show_id)->with('channels')->get();
			
			if($shows->count() > 0){
				foreach($shows as $show){
					if($show->channels->count() > 0){
						foreach($show->channels as $channel){
							$channels[] = $channel->name;
						}
					}
				}
			}			
			if($html){
				if(!empty($channels)){
					return implode($channels,',');
				}else{
					return '';
				}			
			}
			return $channels;
		}
	}

	/**
	* Add Show 
	* @param $data is the request object
	* @return boolean
	*/
	public function add($data){		
		try{			
			if(isset($data['Channel'])){
				if(!empty($data['Channel']['Channel'])){
					if(count($data['Channel']['Channel']) > 1){
						$data['Show']['channels_ids'] = implode($data['Channel']['Channel'], ',');
					}else{
						$data['Show']['channels_ids'] = $data['Channel']['Channel'][0];
					}
				}
			}
			$show = new Show($data['Show']);
			if($show->save()){			
        		$media = new MediaLinking();
				$media->add($show->id, 'Show');
	        }
	        try{
	        	if(isset($data['Channel'])){
					if(!empty($data['Channel']['Channel'])){
						foreach($data['Channel']['Channel'] as $channel){
							$show->channels()->attach($channel);
						}
					}
				}
	        }catch(\Exception $e){
	        	echo $e->getMessage('Channel');
				die;
				return false;
	        }
	        try{
	        	// Add hosts
	        	if(isset($data['Host'])){
					if(!empty($data['Host'])){
						if(!empty($data['Host']['Host'])){
							foreach($data['Host']['Host'] as $host){
								$show->hosts()->where('shows_has_hosts.type', 'host')->attach($host, ['off'=>0, 'type'=>'host']);
							}							
						}
					}
				}
				// Add cohosts
				if(isset($data['Cohost'])){
					if(!empty($data['Cohost'])){
						if(!empty($data['Cohost']['Cohost'])){
							foreach($data['Cohost']['Cohost'] as $cohost){
								$show->hosts()->attach($cohost, ['off'=>0, 'type'=>'cohost']);
							}
						}
					}
				}
				// Add podcast hosts
				if(isset($data['Podcasthost'])){
					if(!empty($data['Podcasthost'])){
						if(!empty($data['Podcasthost']['Podcasthost'])){
							foreach($data['Podcasthost']['Podcasthost'] as $podcathost){
								$show->hosts()->attach($podcathost, ['off'=>0, 'type'=>'podcasthost']);
							}
						}
					}
				}
	        }catch(\Exception $e){
	        	echo $e->getMessage();die('host');
	        	return false;
	        }
			// Save data to guests_has_categories
			try{
				if(isset($data['Category']) && !empty($data['Category'])){
					if(!empty($data['Category']['Category'])){		
						foreach($data['Category']['Category'] as $cate){
							if(!empty($cate)){
								$newcate = array('shows_id'=>$show->id, 'categories_id' => $cate );
								$guestcate = new ShowsHasCategory();								
								$guestcate->add($newcate);
							}
						}													
					}
				}
			}
			catch(\Exception $e){
				return false;
		    }	        
	        return true;
		}
		catch(\Exception $e){
			 //echo $e->getMessage();die('main');
			return false;
	    }
	}

	/**
	* Edit Show 
	* @param $id is the show id and $data is the request object
	* @return boolean
	*/
	public function edit($id, $data){		
		try{			
			if(isset($data['Channel'])){
				if(!empty($data['Channel']['Channel'])){
					if(count($data['Channel']['Channel']) > 1){
						$data['Show']['channels_ids'] = implode($data['Channel']['Channel'], ',');
					}else{
						$data['Show']['channels_ids'] = $data['Channel']['Channel'][0];
					}
				}
			}
			$show = Show::find($id);
			$show->update($data['Show']);			
			
	        // Update data to channels_has_shows
		    try{
		    	$old_channels = array();
				$old_show_channels = $show->channels()->get();
				if($old_show_channels->count() > 0){
		            foreach($old_show_channels as $channel){
		                $old_channels[] = $channel->id;
		            }
		        }	    	
		    
		    	if(isset($data['Channel'])){
					if(!empty($data['Channel'])){
						if(!empty($data['Channel']['Channel'])){	
					        if(!empty($old_channels)){
					        	foreach($old_channels as $chn){
									if(!in_array($chn, $data['Channel']['Channel'])){
										$show->channels()->detach($chn);
									}
								}
					        }
					        
							foreach($data['Channel']['Channel'] as $item){
								if(!in_array($item, $old_channels)){
									$show->channels()->attach($item);
								}
							}
						}
					}
				}else{
					if(!empty($old_channels)){
			        	foreach($old_channels as $chn){
							$show->channels()->detach($chn);									
						}
			        }
				}
			}
			catch(\Exception $e){
				return false;
		    }

		    // Update data to shows_has_host
		    try{
		    	$old_hosts_ids = array();
		    	$old_cohosts_ids = array();
		    	$old_podcasthosts_ids = array();

		    	//Fetch old hosts
				$old_hosts = $show->hosts()->where('shows_has_hosts.type', 'host')->get();
				if($old_hosts->count() > 0){
		            foreach($old_hosts as $host){
		                $old_hosts_ids[] = $host->id;
		            }
		        }	

		        //Fetch old cohosts
		        $old_cohosts = $show->hosts()->where('shows_has_hosts.type', 'cohost')->get();
				if($old_hosts->count() > 0){
		            foreach($old_cohosts as $cohost){
		                $old_cohosts_ids[] = $cohost->id;
		            }
		        }	

		        //Fetch old podcast hosts
		        $old_podcasthosts = $show->hosts()->where('shows_has_hosts.type', 'podcasthost')->get();
				if($old_podcasthosts->count() > 0){
		            foreach($old_podcasthosts as $podcasthost){
		                $old_podcasthosts_ids[] = $podcasthost->id;
		            }
		        }	
		      
		    	// Don't know what is "Off" here. For now, by default send 0 to it.
		    	// Update the hosts
		    	if(isset($data['Host'])){
					if(!empty($data['Host'])){
						if(!empty($data['Host']['Host'])){
							if(!empty($old_hosts_ids)){
					        	foreach($old_hosts_ids as $hst){
									if(!in_array($hst, $data['Host']['Host'])){
										$show->hosts()->wherePivot('shows_has_hosts.type', 'like', 'host')->detach($hst);
									}
								}
					        }
							foreach($data['Host']['Host'] as $host){
								if(!in_array($host, $old_hosts_ids)){
									$show->hosts()->where('shows_has_hosts.type', 'host')->attach($host, ['off'=>0, 'type'=>'host']);
								}
							}							
						}
					}
				}else{
					if(!empty($old_hosts_ids)){
			        	foreach($old_hosts_ids as $hst){
							$show->hosts()->wherePivot('shows_has_hosts.type', 'like', 'host')->detach($hst);		
						}
			        }
				}

				// Update the cohosts
				if(isset($data['Cohost'])){
					if(!empty($data['Cohost'])){
						if(!empty($data['Cohost']['Cohost'])){
							if(!empty($old_cohosts_ids)){
					        	foreach($old_cohosts_ids as $chst){
									if(!in_array($chst, $data['Cohost']['Cohost'])){
										$show->hosts()->wherePivot('shows_has_hosts.type', 'like', 'cohost')->detach($chst);
									}
								}
					        }
							foreach($data['Cohost']['Cohost'] as $cohost){
								if(!in_array($cohost, $old_cohosts_ids)){
									$show->hosts()->attach($cohost, ['off'=>0, 'type'=>'cohost']);
								}
							}
						}
					}
				}else{
					if(!empty($old_cohosts_ids)){
			        	foreach($old_cohosts_ids as $chst){
							$show->hosts()->wherePivot('shows_has_hosts.type', 'like', 'cohost')->detach($chst);									
						}
			        }
				}

				// Update the podcast hosts
				if(isset($data['Podcasthost'])){
					if(!empty($data['Podcasthost'])){
						if(!empty($data['Podcasthost']['Podcasthost'])){
							if(!empty($old_podcasthosts_ids)){
					        	foreach($old_podcasthosts_ids as $phst){
									if(!in_array($phst, $data['Podcasthost']['Podcasthost'])){
										$show->hosts()->wherePivot('shows_has_hosts.type', 'like', 'podcasthost')->detach($phst, ['shows_has_hosts.type' => 'podcasthost']);
									}
								}
					        }
							foreach($data['Podcasthost']['Podcasthost'] as $podcathost){
								if(!in_array($podcathost, $old_podcasthosts_ids)){
									$show->hosts()->attach($podcathost, ['off'=>0, 'type'=>'podcasthost']);
								}
							}
						}
					}
				}else{
					if(!empty($old_podcasthosts_ids)){
			        	foreach($old_podcasthosts_ids as $phst){
							$show->hosts()->wherePivot('shows_has_hosts.type', 'like', 'podcasthost')->detach($phst);									
						}
			        }
				}
		    }
		    catch(\Exception $e){
				return false;
		    }

			try{  	
		    	$same = 0;
		    	if(isset($data['Category'])){
					if(!empty($data['Category'])){
						if(!empty($data['Category']['Category'])){
					        if(!empty($data['Category']['select_main_id']))	{
					        	//Change main category the delete all the categories
					        	if(!in_array($data['Category']['select_main_id'], $data['Category']['Category'])){
					        		$del_guest_cat_object = ShowsHasCategory::where('shows_id' , '=', $show->id)->delete();
								}else{
									if(!empty($data['Category']['select_sub_id']))	{
										$sub_cats = json_decode($data['Category']['select_sub_id']);
										
										foreach($sub_cats as $sub_cat){
											if(!in_array($sub_cat, $data['Category']['Category'])){
												$del_guest_cat_object = ShowsHasCategory::where(['shows_id' => $show->id, 'categories_id' => $sub_cat])->delete();
											}
										}
									}
								}
					        }
					        
				        	foreach($data['Category']['Category'] as $item){
				        		if(!empty($item))	{
				        			$newcat = array('shows_id'=>$show->id, 'categories_id' => $item );
									$guest_cat = ShowsHasCategory::updateOrCreate($newcat);
									$guest_cat->save();
				        		}
							}
						}
					}
				}
			}catch(\Exception $e){
				echo $e->getMessage();die();
				return false;
		    }
	        return true;
		}
		catch(\Exception $e){
			return false;
	    }
	}

	/**
	 * @description Delete the Show by setting the deleted column value to 1.
	 * @param where $id is host_id
	 */	
	public function deleteShow($id=0){
		
		if($id != '' || $id !=0){
			$show = Show::find($id);
			$show->update(['deleted'=>1]);
			return true;
		}
	}

	/**
     * Move All Host related images From Tmp Location to permananet folder
     *
     */
    public function procesImage($object, $module){
    
        $folders = ['player', 'featured', 'schedule', 'audio'];
        $userid = \Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }  
        return 'success';
    }

    /* Model methods for frontend starts here*/
    public function countAllFeaturedShows($channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$shows = new Show();
		if(!empty($channel_id)){
			$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id')->where('channels_has_shows.channels_id', '=', $channel_id);
		}

		//Fetch who are not deleted
		$shows = $shows->where('shows.deleted', 0);
		
		// Overall Search 
        if(!empty($search_value)){
        	//if(empty($channel_id)){
            	$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id');
        	//}
       		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id')
           			->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id') 
           			->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id')
           			->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');
            $shows = $shows->where(function($q) use ($search_value){
							$q->orWhere('shows.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('channels.name' ,'like', '%'.$search_value.'%');
						});
    								
        }
		$shows = $shows->count();
		return $shows;
	}
    /**
	* Fetches all the featured shows
	* @return result in the form of object
	*/
	public function getAllFeaturedShows($channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$shows = Show::select('shows.id', 'shows.name', 'shows.description', 'media.meta_json', 'media.id as mid');
		//Get featured shows
		$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id')->groupBy('shows.id');
		$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
		$shows = $shows->leftJoin('media_linking', 'media_linking.module_id', '=', 'shows.id');
		$shows = $shows->leftJoin('media', 'media.id', '=', 'media_linking.media_id');

		$shows = $shows->where(['shows.is_featured' => 1, 'shows.deleted' => 0, 'shows.is_online' => 1, 'channels.id' => $channel_id]);

		// Overall Search 
        if(!empty($search_value)){
        	//if(empty($channel_id)){
            	$shows = $shows->leftJoin('channels_has_shows', 'channels_has_shows.shows_id', '=', 'shows.id');
        	//}
        	$shows = $shows->leftJoin('channels', 'channels.id', '=', 'channels_has_shows.channels_id');
        	$shows = $shows->leftJoin('shows_has_hosts', 'shows_has_hosts.hosts_id', '=', 'shows.id');
        	$shows = $shows->leftJoin('hosts', 'hosts.id', '=', 'shows_has_hosts.hosts_id');
        	$shows = $shows->leftJoin('profiles', 'hosts.users_id', '=', 'profiles.users_id');

            $shows = $shows->where(function($q) use ($search_value){
							$q->orWhere('shows.name' ,'like', '%'.$search_value.'%')
								->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
								->orWhere('channels.name' ,'like', '%'.$search_value.'%');
						});
        }

        $shows = $shows->orderBy('shows.name', 'asc');
       	$shows  = $shows->paginate($length);
        //$shows = $shows->offset($start)->limit($length)->get();
		return $shows;
	}
	/* Model methods for frontend ends here*/
}
