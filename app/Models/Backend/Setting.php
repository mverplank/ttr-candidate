<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class Setting
 * 
 * @property string $name
 * @property string $label
 * @property string $value
 * @property string $description
 * @property string $type
 * @property bool $is_editable
 * @property string $options
 * @property string $validation
 * @property int $set
 * @property int $offset
 *
 * @package App\Models
 */
class Setting extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'is_editable' => 'bool',
		'set' => 'int',
		'offset' => 'int'
	];

	protected $fillable = [
		'name',
		'label',
		'value',
		'description',
		'type',
		'is_editable',
		'options',
		'validation',
		'set',
		'offset'
	];

	// Fetch prefixes and suffixes
	public function allPrefixSuffix(){
		$get = Setting::select('name', 'value')->where('name', 'user_name_prefixes')->orWhere('name', 'user_name_sufixes')->get();
		$main = array();
		foreach($get as $key=>$val){
            if($val['name'] == 'user_name_prefixes'){
                $prefix = explode(",",$val['value']);
                $main['prefix'] = array_combine($prefix, $prefix);
                $main['prefix'] = array_merge(['' => 'Choose Prefix...'],$main['prefix']);
            }
            else if($val['name'] == 'user_name_sufixes'){
                $suffix = explode(",",$val['value']);
                $main['suffix'] = array_combine($suffix, $suffix);
                $main['suffix'] = array_merge(['' => 'Choose Suffix...'],$main['suffix']);
            }
        }
		return $main;
	}

	/** Fetch prefixes and suffixes
	 * @return In the comma seprated values
	 */
	public function fetchPrefixSufixToShow(){
		$get = Setting::select('name', 'value')->where('name', 'user_name_prefixes')->orWhere('name', 'user_name_sufixes')->get();
		$content = array();
		if($get->count() >0){
			foreach($get as $val){
				if($val->name == "user_name_prefixes"){
					$content['prefix'] = $val->value; 
				}else if($val->name == "user_name_sufixes"){
					$content['sufix']  = $val->value; 
				}else{
					$content['prefix'] = array(); 
					$content['sufix']  = array(); 
				}
			}
		}
		return $content;
	}

	/** Fetch prefixes and suffixes
	 * @param $data contains the sufix and prefix in the comma separated values	
	 * @return boolean
	 */
	public function updatePrefixSufix($data){
		
		$new_data = array();
		$i=0;
		//DB::enableQuerylog();
		try{
			foreach($data['Configuration'] as $key=>$values){
				$obj = new Setting();
				$obj->where('name',$key)->update(['value'=>$values]);
				//dd(DB::getQuerylog());
			}
			return true;
		}catch(\Exception $e){
			return false;
		}
		
	}
}
