<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;
use App\Models\Backend\Profile;
use App\Models\Backend\Tag;
use App\Models\Backend\Guest;
use App\Models\Backend\Show;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Archive
 * 
 * @property int $id
 * @property \Carbon\Carbon $datetime
 * @property int $episode_id
 * @property string $type
 * @property string $title
 * @property string $title_soundex
 * @property int $shows_id
 * @property int $channels_id
 * @property string $show_name
 * @property string $show_name_soundex
 * @property string $description
 * @property string $host_ids
 * @property string $hosts
 * @property string $hosts_soundex
 * @property string $cohost_ids
 * @property string $cohosts
 * @property string $cohosts_soundex
 * @property string $podcasthost_ids
 * @property string $podcasthosts
 * @property string $podcasthosts_soundex
 * @property string $guest_ids
 * @property string $guests
 * @property string $guests_soundex
 * @property string $tag_ids
 * @property string $tags
 * @property string $tags_soundex
 * @property string $categories_ids
 * @property string $categories
 * @property string $categories_soundex
 * @property \Carbon\Carbon $year
 * @property int $month
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $time
 * @property int $duration
 * @property int $is_online
 * @property bool $is_featured
 * @property int $encores_number
 * @property string $stream
 * @property bool $is_uploaded_by_host
 * @property string $video_url
 *
 * @package App\Models
 */
class Archive extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episode_id' => 'int',
		'shows_id' => 'int',
		'channels_id' => 'int',
		'month' => 'int',
		'duration' => 'int',
		'is_online' => 'int',
		'is_featured' => 'bool',
		'encores_number' => 'int',
		'is_uploaded_by_host' => 'bool'
	];

	protected $dates = [
		'datetime',
		'year',
		'date',
		'time'
	];

	protected $fillable = [
		'datetime',
		'episode_id',
		'type',
		'title',
		'title_soundex',
		'shows_id',
		'channels_id',
		'show_name',
		'show_name_soundex',
		'description',
		'host_ids',
		'hosts',
		'hosts_soundex',
		'cohost_ids',
		'cohosts',
		'cohosts_soundex',
		'podcasthost_ids',
		'podcasthosts',
		'podcasthosts_soundex',
		'guest_ids',
		'guests',
		'guests_soundex',
		'tag_ids',
		'tags',
		'tags_soundex',
		'categories_ids',
		'categories',
		'categories_soundex',
		'year',
		'month',
		'date',
		'time',
		'duration',
		'is_online',
		'is_featured',
		'encores_number',
		'stream',
		'is_uploaded_by_host',
		'video_url'
	];

	public function countAllArchivedEpisodes($current_user_id, $type=1, $category_id=0, $host=0, $month=0, $year=0, $show_id=0, $channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$archives = new Archive();

		if(!empty($channel_id)){
			$archives = $archives->where('channels_id', '=', $channel_id);
		}


		if($type == 2){
			/*$archives = $archives->where(function($query) use ($current_user_id) {
					        $query->whereRaw('FIND_IN_SET("'.$current_user_id.'", host_ids) > 0')
					            ->orWhereRaw('FIND_IN_SET("'.$current_user_id.'", cohost_ids) > 0')
					            ->whereRaw('UNIX_TIMESTAMP(CONCAT(date, " ", time)) + duration*60 < UNIX_TIMESTAMP()');
					    });*/
			$archives = $archives->whereRaw('  UNIX_TIMESTAMP(CONCAT(`archives`.`date`, " ", `archives`.`time`)) + `archives`.`duration` * 60 < UNIX_TIMESTAMP() AND (    FIND_IN_SET("'.$current_user_id.'", `archives`.`host_ids`) > 0 OR FIND_IN_SET("'.$current_user_id.'", `archives`.`cohost_ids`) > 0)');
		}else{
			if(!empty($show_id)){
				$archives = $archives->where('shows_id', '=', $show_id);
			}
			if(!empty($year)){
				$archives = $archives->where('year', '=', $year);
			}
			if(!empty($month)){
				$archives = $archives->where('month', '=', $month);
			}
			if(!empty($category_id)){
				$archives = $archives->whereRaw('FIND_IN_SET('.$category_id.', categories_ids)');
			}
			if(!empty($host)){
				$archives = $archives->whereRaw('FIND_IN_SET('.$host.', host_ids)');
			}
		}
	
		// Overall Search 
        if(!empty($search_value)){
        	$archives = $archives->where('archives.title' ,'like', '%'.$search_value.'%');	
        }

        // Sorting by column
        if($order != null){
            $archives = $archives->orderBy($order, $dir);
        }
		$archives = $archives->count();
		return $archives;
	}

	public function getAllArchivedEpisodes($current_user_id, $type=1, $category_id=0, $host=0, $month=0, $year=0, $show_id=0, $channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$archives = Archive::select('archives.id', 'archives.datetime', 'archives.title', 'archives.encores_number','archives.hosts', 'archives.cohosts', 'archives.guests', 'archives.podcasthosts', 'archives.episode_id');
		/*
		if(!empty($type)){
            $hosts = $hosts->join('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('hosts_has_host_types.host_types_id', '=', $type);
		}*/
		//echo $current_user_id;die('success');
		if(!empty($channel_id)){
			$archives = $archives->where('channels_id', '=', $channel_id);
		}
		

		//Host - 2
		/*if($type == 2){
			$archives = $archives->where(function($query) use ($current_user_id) {
					        $query->whereRaw('FIND_IN_SET("'.$current_user_id.'", host_ids) > 0')
					            ->orWhereRaw('FIND_IN_SET("'.$current_user_id.'", cohost_ids) > 0')
					            ->whereRaw('UNIX_TIMESTAMP(CONCAT(date, " ", time)) + duration*60 < UNIX_TIMESTAMP()');
					    });
		}*/
		if($type == 2){
			$archives = $archives->whereRaw('UNIX_TIMESTAMP( CONCAT(`archives`.`date`, " ", `archives`.`time`)) + `archives`.`duration` * 60 < UNIX_TIMESTAMP() AND (    FIND_IN_SET("'.$current_user_id.'", `archives`.`host_ids`) > 0 OR FIND_IN_SET("'.$current_user_id.'", `archives`.`cohost_ids`) > 0)');
			
		}else{
			if(!empty($show_id)){
				$archives = $archives->where('shows_id', '=', $show_id);
			}
			if(!empty($year)){
				$archives = $archives->where('year', '=', $year);
			}
			if(!empty($month)){
				$archives = $archives->where('month', '=', $month);
			}
			if(!empty($category_id)){
				$archives = $archives->whereRaw('FIND_IN_SET('.$category_id.', categories_ids)');
			}
			if(!empty($host)){
				$archives = $archives->whereRaw('FIND_IN_SET('.$host.', host_ids)');
			}
		}
		
		
		
		// Overall Search 
        if(!empty($search_value)){
          	$archives = $archives->where('archives.title' ,'like', '%'.$search_value.'%');
        }

        // Sorting by column
        if($order != null){
            $archives = $archives->orderBy($order, $dir);
        }
        $archives = $archives->offset($start)->limit($length)->get();
		return $archives;
	}

	/**
	 *@description Add the archive
	 *@param the $archive,$episode contains the data to be added
	 *@return boolean
	 */
	public function add($data,$archive,$episode){
		//echo"<pre>";print_r($archive);die;
		if($episode->date >= \Carbon::now()){
					
			$show = Show::where('id',$episode->shows_id)->first();
			$date_ =explode(" ", $episode->date);
	        $date = date('Y-m-d H:i:s', strtotime($date_[0].' '.$episode->time));
	        $archiv = array();	
			$archiv['episode_id'] =$episode->id;
			$archiv['type'] =$episode->assigned_type;
			$archiv['datetime'] =$date;
			$archiv['shows_id'] =$episode->shows_id;
			$archiv['channels_id'] =$episode->channels_id;
			$archiv['description'] =$episode->description;
			$archiv['year'] =$episode->year;
			$archiv['month'] =$episode->month;
			$archiv['date'] =$episode->date;
			$archiv['time'] =$episode->time;		
			$archiv['is_online'] =$episode->is_online;
			$archiv['is_featured'] =$episode->is_featured;
			$archiv['encores_number'] =$episode->encores_number;
			$archiv['stream'] =$episode->stream;
			$archiv['is_uploaded_by_host'] =$episode->is_uploaded_by_host;
			$archiv['video_url'] =$episode->video_url;
			$archiv['duration'] =$show->duration;

			$archiv['title'] = $archive['title'];
			$archiv['title_soundex'] = $archive['title_soundex'];
			
			$archiv['show_name'] = $archive['show_name'];
			$archiv['show_name_soundex'] = $archive['show_name_soundex'];

			$archiv['tag_ids'] =$archive['Tagid'];
			$archiv['tags_soundex'] = $archive['TagSX'];
			$archiv['tags'] =$archive['Tag'];

			$archiv['categories_ids'] =$archive['Categoryid'];
			$archiv['categories'] =$archive['Category']; 
			$archiv['categories_soundex'] = $archive['CategorySX'];

			$archiv['guest_ids'] =$archive['Guestid'];
			$archiv['guests'] =$archive['Guest'];
			$archiv['guests_soundex'] =$archive['GuestSX'];
			
			$archiv['host_ids'] =$archive['Hostid'];
			$archiv['hosts'] =$archive['Host'];
			$archiv['hosts_soundex'] =$archive['HostSX'];

			$archiv['cohost_ids'] =$archive['Cohostid'];
			$archiv['cohosts'] = $archive['Cohost'];
			$archiv['cohosts_soundex'] = $archive['CohostSX'];

			$archiv['podcasthost_ids'] = $archive['Podcasthostid'];
			$archiv['podcasthosts'] = $archive['Podcasthost'];
			$archiv['podcasthosts_soundex'] = $archive['PodcasthostSX'];
			//$archiv['schedules_id'] =$archive['Schedule'];
			Archive::insert($archiv);
		}
	}

	public function edit($id=0, $data){
			
		if($id != 0){
			if(!empty($data)){	
			
			$host_ids ='';
			$hosts  = '';
			$cohost_ids ='';

			$cohosts='';
			$podcasthost_ids ='';
			$podcasthosts ='';
			$tag_ids ='';
			$tags ='';
			$guest_ids ='';
			$guests ='';

				try{
					if(isset($data['Host'])){
						if(!empty($data['Host'])){
							if(!empty($data['Host']['Host'])){
								$host_ids = implode (", ", $data['Host']['Host']);
								$hosts_arr  =array();
								foreach($data['Host']['Host'] as $host_id){
									$host = Host::select('users_id')->where('id',$host_id)->first();
									$profile = Profile::select('name')->where('users_id',$host->users_id)->first();
									$hosts_arr[] = $profile->name;
								}
								$hosts = implode (", ",$hosts_arr);
							}
						}
					}					
					if(isset($data['Cohost'])){
						if(!empty($data['Cohost'])){
							if(!empty($data['Cohost']['Cohost'])){
								$cohost_ids = implode (", ", $data['Cohost']['Cohost']);
								$cohosts_arr =array();
								foreach($data['Cohost']['Cohost'] as $cohost_id){
									$host = Host::select('users_id')->where('id',$cohost_id)->first();
									$profile_co = Profile::select('name')->where('users_id',$host->users_id)->first();
									$cohosts_arr[] = $profile_co->name;
								}
								$cohosts = implode (", ",$cohosts_arr);
							}
						}
					}
					if(isset($data['Podcasthost'])){
						if(!empty($data['Podcasthost'])){
							if(!empty($data['Podcasthost']['Podcasthost'])){
								$cohost_ids = implode (", ", $data['Podcasthost']['Podcasthost']);
								$procohosts_arr =array();
								foreach($data['Podcasthost']['Podcasthost'] as $prohost_id){
									$host = Host::select('users_id')->where('id',$prohost_id)->first();
									$profile_pro = Profile::select('name')->where('users_id',$host->users_id)->first();
									$procohosts_arr[] = $profile_pro->name;
								}
								$cohosts = implode (", ",$procohosts_arr);
							}
						}
					}
					
					if(isset($data['Tag'])){
						if(!empty($data['Tag'])){
							if(!empty($data['Tag']['Tag'])){
								$tag_ids = implode (", ", $data['Tag']['Tag']);
								$tags_arr =array();
								foreach($data['Tag']['Tag'] as $tag_id){
									$tags = Tag::select('name')->where('id',$tag_id)->first();
									$tags_arr[] = $tags->name;
								}
								$tags = implode (", ",$tags_arr);
							}
						}
					}	
					if(isset($data['Guest'])){
						if(!empty($data['Guest'])){
							if(!empty($data['Guest']['Guest'])){
								$guest_ids = implode (", ", $data['Guest']['Guest']);
								$guests_arr =array();
								foreach($data['Guest']['Guest'] as $guest_id){
									$profile_guest = Guest::select('name')->where('id',$guest_id)->first();
									$guests_arr[] = $profile_guest->name;
								}
								$guests = implode (", ",$guests_arr);
							}
						}
					}	
					
					// if(isset($data['Category'])){
						// if(!empty($data['Category'])){
							// if(!empty($data['Category']['Category'])){
								// $categories_ids = implode (", ", $data['Category']['Category']);
								// $cats_arr =array();
								// foreach($data['Category']['Category'] as $cat_id){
									// $category = Category::select('name')->where('id',$cat_id)->first();
									// $cats_arr[] = $profile->name;
								// }
								// $categories = implode (", ",$cats_arr);
							// }
						// }
					// }
					
					// if(isset($data['Sponsor'])){
						// if(!empty($data['Sponsor'])){
							// if(!empty($data['Sponsor']['Sponsor'])){
								// $guest_ids = implode (", ", $data['Sponsor']['Sponsor']);
							// }
						// }
					// }
					$archive = Archive::where('episode_id',$id)->first();
					$archive->host_ids = $host_ids;
					$archive->hosts = $hosts;
					$archive->cohost_ids = $cohost_ids;
					$archive->cohosts = $cohosts;
					$archive->podcasthost_ids = $podcasthost_ids;
					$archive->podcasthosts = $podcasthosts;
					$archive->tag_ids = $tag_ids;
					$archive->tags = $tags;
					$archive->guest_ids = $guest_ids;
					$archive->guests = $guests;
					// $archive->categories_ids = $categories_ids;
					// $archive->categories = $categories;
					$archive->update($data['Episode']);

					return true;
				}catch(\Exception $e){
					/* echo $e->getMessage();die(); */
					return false;
				}
			
			}
		}
	}
}