<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodesHasTag
 * 
 * @property int $episodes_id
 * @property int $tags_id
 * 
 * @property \App\Models\Backend\Episode $episode
 * @property \App\Models\Backend\Tag $tag
 *
 * @package App\Models\Backend
 */
class EpisodesHasTag extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'tags_id' => 'int'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}

	public function tag()
	{
		return $this->belongsTo(\App\Models\Backend\Tag::class, 'tags_id');
	}
}
