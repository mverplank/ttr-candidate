<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChannelsHasFeaturedEpisode
 * 
 * @property int $id
 * @property int $channels_id
 * @property int $episodes_id
 * @property int $off
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Episode $episode
 *
 * @package App\Models
 */
class ChannelsHasFeaturedEpisode extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int',
		'episodes_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'off'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class, 'channels_id');
	}

	public function episode()
	{
		return $this->belongsTo(\App\Models\Episode::class, 'episodes_id');
	}
}
