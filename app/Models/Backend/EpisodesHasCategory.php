<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodesHasCategory
 * 
 * @property int $episodes_id
 * @property int $categories_id
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\Episode $episode
 *
 * @package App\Models\Backend
 */
class EpisodesHasCategory extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'categories_id' => 'int'
	];
	protected $fillable = [
		'episodes_id',
		'categories_id'
	];
	public function category()
	{
		return $this->belongsTo(\App\Models\Backend\Category::class, 'categories_id');
	}

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}
	
}
