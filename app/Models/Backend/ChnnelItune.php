<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChnnelItune
 * 
 * @property int $id
 * @property int $channels_id
 * @property string $podcast
 * @property string $podcast_title
 * @property string $podcast_category
 * @property string $podcast_subcategory
 * @property string $keywords
 * @property string $subtitle
 * @property string $description
 * 
 * @property \App\Models\Channel $channel
 *
 * @package App\Models
 */
class ChnnelItune extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int'
	];

	protected $fillable = [
		'podcast',
		'podcast_title',
		'podcast_category',
		'podcast_subcategory',
		'keywords',
		'subtitle',
		'description'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class, 'channels_id');
	}
}
