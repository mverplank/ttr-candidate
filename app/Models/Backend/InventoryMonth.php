<?php

/**
 * @author: Contriverz.
 * @since : Mon, 29 Apr 2019 011:54:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class InventoryMonth
 * 
 * @property int $id
 * @property \Carbon\Carbon $date
 * 
 * @property \Illuminate\Database\Eloquent\Collection $inventory_month_items
 *
 * @package App\Models\Backend
 */
class InventoryMonth extends Eloquent
{
	public $timestamps = false;

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'date'
	];

	public function inventory_month_items()
	{
		return $this->hasMany(\App\Models\Backend\InventoryMonthItem::class, 'inventory_months_id');
	}

	/**
	 * Get monthly inventory items
     * @param $date is the inventory month date greater than
     * @param $id is the membership id
	 */
	public function getMonthlyItems($date, $id, $start=0, $length=10, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$get_monthly_items = $this;

		 $get_monthly_items = $get_monthly_items->select('id', 'date', DB::raw("(SELECT COUNT(*) FROM inventory_month_items WHERE `inventory_month_items`.`inventory_months_id`=`inventory_months`.`id` AND `inventory_month_items`.`memberships_id`=".$id.") AS `InventoryMonth__items`"))
							->whereDate('date', '>=',  \Carbon::parse($date)->toDateString());
							

        // Sorting by column
        if($order != null){
            $get_monthly_items = $get_monthly_items->orderBy($order, $dir);
        }else{
            $get_monthly_items = $get_monthly_items->orderBy('date', 'ASC');
        } 

		$get_monthly_items = $get_monthly_items->offset($start)->limit($length)->get();
		return $get_monthly_items;				 
	}

	/**
	 * Get count of monthly inventory items
     * @param $date is the inventory month date greater than
     * @param $id is the membership id
	 */
	public function countMonthlyItems($date, $id, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$get_monthly_items = $this;

		// Overall Search 
        /*if(!empty($search_value)){
            $get_monthly_items = $get_monthly_items->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }*/

        $get_monthly_items = $get_monthly_items->select('id', 'date', DB::raw("(SELECT COUNT(*) FROM inventory_month_items WHERE `inventory_month_items`.`inventory_months_id`=`inventory_months`.`id` AND `inventory_month_items`.`memberships_id`=".$id.") AS `InventoryMonth__items`"))
							->whereDate('date', '>=',  \Carbon::parse($date)->toDateString());
							

        // Sorting by column
        if($order != null){
            $get_monthly_items = $get_monthly_items->orderBy($order, $dir);
        }else{
            $get_monthly_items = $get_monthly_items->orderBy('date', 'ASC');
        } 

		$get_monthly_items = $get_monthly_items->count();
		return $get_monthly_items;		 
	}
}
