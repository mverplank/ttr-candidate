<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PodcastStat
 * 
 * @property int $id
 * @property int $episodes_id
 * @property int $episodes_channels_id
 * @property int $episodes_shows_id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property int $total
 * @property int $last_week
 * @property int $last_month
 * @property int $downloaded_total
 * @property int $downloaded_last_week
 * @property int $downloaded_last_month
 * @property int $downloaded_last_year
 * @property int $played_total
 * @property int $played_last_week
 * @property int $played_last_month
 * @property int $played_last_year
 * @property string $hosts_ids
 * @property string $guests_ids
 * @property string $categories_ids
 * @property string $tags
 * 
 * @property \App\Models\Backend\Episode $episode
 * @property \Illuminate\Database\Eloquent\Collection $podcast_hits
 *
 * @package App\Models\Backend
 */
class PodcastStat extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'episodes_channels_id' => 'int',
		'episodes_shows_id' => 'int',
		'total' => 'int',
		'last_week' => 'int',
		'last_month' => 'int',
		'downloaded_total' => 'int',
		'downloaded_last_week' => 'int',
		'downloaded_last_month' => 'int',
		'downloaded_last_year' => 'int',
		'played_total' => 'int',
		'played_last_week' => 'int',
		'played_last_month' => 'int',
		'played_last_year' => 'int'
	];

	protected $dates = [
		'created',
		'modified'
	];

	protected $fillable = [
		'episodes_id',
		'episodes_channels_id',
		'episodes_shows_id',
		'created',
		'modified',
		'total',
		'last_week',
		'last_month',
		'downloaded_total',
		'downloaded_last_week',
		'downloaded_last_month',
		'downloaded_last_year',
		'played_total',
		'played_last_week',
		'played_last_month',
		'played_last_year',
		'hosts_ids',
		'guests_ids',
		'categories_ids',
		'tags'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id')
					->where('episodes.id', '=', 'podcast_stats.episodes_id')
					->where('episodes.channels_id', '=', 'podcast_stats.episodes_channels_id')
					->where('episodes.shows_id', '=', 'podcast_stats.episodes_shows_id');
	}

	public function podcast_hits()
	{
		return $this->hasMany(\App\Models\Backend\PodcastHit::class, 'podcast_stats_id');
	}

	
}
