<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MembershipsHasInventoryItem
 * 
 * @property int $memberships_id
 * @property int $inventory_items_id
 * 
 * @property \App\Models\InventoryItem $inventory_item
 * @property \App\Models\Membership $membership
 *
 * @package App\Models
 */
class MembershipsHasInventoryItem extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'memberships_id' => 'int',
		'inventory_items_id' => 'int'
	];

	protected $fillable = [
		'memberships_id',
		'inventory_items_id'
	];

	public function inventory_item()
	{
		return $this->belongsTo(\App\Models\Backend\InventoryItem::class, 'inventory_items_id');
	}

	public function membership()
	{
		return $this->belongsTo(\App\Models\Backend\Membership::class, 'memberships_id');
	}

	public function add($new_arr){
		if(!empty($new_arr)){
			$inventory_item = new MembershipsHasInventoryItem($new_arr);
			$inventory_item->save();
		}
	}
}
