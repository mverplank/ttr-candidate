<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;
use App\Traits\Excludable;
use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\Backend\Archive;
use App\Models\Backend\EpisodeScheduledShare;
use Auth;
use Storage;
use Carbon;
use DB;
/**
 * Class Episode
 * 
 * @property int $id
 * @property int $channels_id
 * @property int $shows_id
 * @property int $schedules_id
 * @property int $episodes_id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property int $year
 * @property int $month
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $time
 * @property string $title
 * @property string $title_soundex
 * @property string $description
 * @property bool $is_encore
 * @property bool $is_online
 * @property bool $is_featured
 * @property bool $is_itunes
 * @property string $notes
 * @property string $talking_points
 * @property string $segment1
 * @property string $segment1_title
 * @property string $segment1_video_url
 * @property string $segment2
 * @property string $segment2_title
 * @property string $segment2_video_url
 * @property string $segment3
 * @property string $segment3_title
 * @property string $segment3_video_url
 * @property string $segment4
 * @property string $segment4_title
 * @property string $segment4_video_url
 * @property string $events_promote
 * @property string $special_offers
 * @property string $listener_interaction
 * @property string $giveaways
 * @property string $internal_host_notes
 * @property string $type
 * @property string $assigned_type
 * @property string $file
 * @property string $stream
 * @property int $encores_number
 * @property int $downloads
 * @property string $episodescol
 * @property int $is_cohost_override_banner
 * @property int $is_cohost_override_show_title
 * @property int $cohost_show_id
 * @property string $cohost_show_name
 * @property int $favorites
 * @property bool $is_uploaded_by_host
 * @property string $video_url
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Schedule $schedule
 * @property \App\Models\Show $show
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episode_itunes
 * @property \Illuminate\Database\Eloquent\Collection $episode_scheduled_shares
 * @property \Illuminate\Database\Eloquent\Collection $episodes_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $guests
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 * @property \Illuminate\Database\Eloquent\Collection $sponsors
 * @property \Illuminate\Database\Eloquent\Collection $tags
 * @property \Illuminate\Database\Eloquent\Collection $podcast_stats
 *
 * @package App\Models\Backend
 */
class Episode extends Eloquent
{
	use Excludable;
	public $timestamps = true;
	protected $casts = [
		'channels_id' => 'int',
		'shows_id' => 'int',
		'schedules_id' => 'int',
		'episodes_id' => 'int',
		'year' => 'int',
		'month' => 'int',
		'is_encore' => 'bool',
		'is_online' => 'bool',
		'is_featured' => 'bool',
		'is_itunes' => 'bool',
		'encores_number' => 'int',
		'downloads' => 'int',
		'is_cohost_override_banner' => 'int',
		'is_cohost_override_show_title' => 'int',
		'cohost_show_id' => 'int',
		'favorites' => 'int',
		'is_uploaded_by_host' => 'bool'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'date',
		//'time'
	];

	protected $hidden = ['giveaways'];

	protected $fillable = [
		'channels_id',
		'schedules_id',
		'episodes_id',
		'created_at',
		'shows_id',
		'updated_at',
		'year',
		'month',
		'date',
		'time',
		'title',
		'title_soundex',
		'description',
		'is_encore',
		'is_online',
		'is_featured',
		'is_itunes',
		'notes',
		'talking_points',
		'segment1',
		'segment1_title',
		'segment1_video_url',
		'segment2',
		'segment2_title',
		'segment2_video_url',
		'segment3',
		'segment3_title',
		'segment3_video_url',
		'segment4',
		'segment4_title',
		'segment4_video_url',
		'events_promote',
		'special_offers',
		'listener_interaction',
		'giveaways',
		'internal_host_notes',
		'type',
		'assigned_type',
		'file',
		'stream',
		'encores_number',
		'downloads',
		'episodescol',
		'is_cohost_override_banner',
		'is_cohost_override_show_title',
		'cohost_show_id',
		'cohost_show_name',
		'favorites',
		'is_uploaded_by_host',
		'video_url'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Backend\Channel::class, 'channels_id');
	}

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Backend\Schedule::class, 'schedules_id');
	}

	public function show()
	{
		return $this->belongsTo(\App\Models\Backend\Show::class, 'shows_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_featured_episodes', 'episodes_id', 'channels_id')
					->withPivot('id', 'off');
	}

	public function episode_itunes()
	{
		return $this->hasMany(\App\Models\Backend\EpisodeItune::class, 'episodes_id');
	}

	public function episode_scheduled_shares()
	{
		return $this->hasMany(\App\Models\Backend\EpisodeScheduledShare::class, 'episodes_id');
	}

	public function episodes_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\EpisodesHasCategory::class, 'episodes_id');
	}

	public function guests()
	{
		return $this->belongsToMany(\App\Models\Backend\Guest::class, 'episodes_has_guests', 'episodes_id', 'guests_id')
					->withPivot('id', 'off');
		//return $this->hasOne(\App\Models\Backend\Guest::class, 'episodes_id', 'guests_id');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'episodes_has_hosts', 'episodes_id', 'hosts_id')
					->withPivot('id', 'off', 'type');
	}

	public function schedules()
	{
		return $this->belongsToMany(\App\Models\Backend\Schedule::class, 'episodes_has_schedules', 'episodes_id', 'schedules_id')
					->withPivot('id', 'date', 'time');
	}

	public function sponsors()
	{
		return $this->belongsToMany(\App\Models\Backend\Sponsor::class, 'episodes_has_sponsors', 'episodes_id', 'sponsors_id')
					->withPivot('id', 'off');
	}

	public function tags()
	{
		return $this->belongsToMany(\App\Models\Backend\Tag::class, 'episodes_has_tags', 'episodes_id', 'tags_id');
	}

	public function giveaways()
	{
		return $this->hasMany(\App\Models\Backend\Giveaway::class, 'episodes_id');
	}

	public function podcast_stats()
	{
		return $this->hasMany(\App\Models\Backend\PodcastStat::class, 'episodes_id');
	}

	/**
     * @description Get the episode detail by the id
     * @param $id here is episode id 
     * @return Episode object
     */
	public function getEpisodeById($id){
		if($id != 0){			
			$episode = Episode::exclude('giveaways')->with('episodes_has_categories')->with('episodes_has_categories.category')->with('hosts')->with('tags')->with('guests')->with('hosts.urls')->with('channels')->with('show')->with('giveaways')->with('hosts.user.profiles')->with('sponsors')->where('id', $id)->first();
			return $episode;
		}
	}

	/**
     * @description Fetch the episode's podcast stats 
     * @param  $episode_id here is episode id 
     * @param  $year here is particular stat year 
     * @param  $month here is particular stat month 
     * @return Episode object
     */
	public function fetchPodcastHits($episode_id, $year, $month){
		
		$get = $this->with('podcast_stats')->where('id', $episode_id)->first();
		$podcast_hit_obj   = new PodcastHit();
		if($get->podcast_stats->count() > 0){
			foreach($get->podcast_stats as $val){
				$fetch_podcast_hit = $podcast_hit_obj->fetchPodcastHits($val->id, $year, $month);
				
				if($fetch_podcast_hit->count() >0){
					// fillfull empty days
			        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			        $day = 1;
			        while($day<=$daysInMonth) {
			            $stats[$day] = 0;
			            $day++;
			        }
			       
			        foreach ($fetch_podcast_hit as $row) {			           
			            $stats[$row->day] = (int)$row->hits;
			        }
			        //echo "<pre>";print_R($stats);die;
			        return $stats;
				}
			}
		}
	}


	/**
    * Convert each word of expression to it's soundex values; ex: 
    *  "the day after tommorow" -> "T000 D000 A136 T560"
    * explode with comma ","
    * @param String $in 
    * @return String 
    */
    public function soundex($in = '') {
       $words = explode(",", $in);
       $soundexWords = array();
       foreach ($words as $word) {
           $soundexWords[] = soundex($word);
       }

       return implode(' ', $soundexWords);
    }
	/**
    * Convert each word of expression to it's soundex values; ex: 
    *  "the day after tommorow" -> "T000 D000 A136 T560"
    * explode with white space " "
    * @param String $in 
    * @return String 
    */
    public function soundex_($in = '') {
       $words = explode(" ", $in);
       $soundexWords = array();
       foreach ($words as $word) {
           $soundexWords[] = soundex($word);
       }

       return implode(' ', $soundexWords);
    }

	/**
	 *@description Add the episode
	 *@param the $data contains the data to be added
	 *@return boolean
	 */
	public function add($data){
		if(!empty($data)){
			//echo"<pre>";print_r($data);die;
			try{
				$archive = array();
				$archive['title']         ='';
				$archive['title_soundex']  = '';
				$data['Episode']['date']  = Carbon::parse($data['Episode']['date'])->format('Y-m-d');
				$data['Episode']['year']  = Carbon::parse($data['Episode']['date'])->format('Y');
				$data['Episode']['month'] = Carbon::parse($data['Episode']['date'])->format('m');
				$data['Episode']['time']  = Carbon::parse($data['Episode']['time'])->format('H:i:s');
				// title and title soundex for archive
				$archive['title']         = $data['Episode']['title'];
				$archive['title_soundex'] =$this->soundex_($data['Episode']['title']);

				$episode = new Episode($data['Episode']);
				if($episode->save()){
					$media = new MediaLinking();
					$media->add($episode->id, 'Episode');
				}
				//Add data to episodes_has_schedules
				try{
					$archive['show_name'] = " ";
					$archive['show_name_soundex'] = " ";

					if(isset($data['Schedule'])){
						if(!empty($data['Schedule'])){
							if(!empty($data['Schedule']['Schedules'])){
								foreach($data['Schedule']['Schedules'] as $item){
									$episode->schedules()->attach($item,['date'=>$episode->date, 'time'=>$episode->time]);
								}
								// show name and show name soundex for archive
								$archive['show_name'] = $data['Schedule']['show_name'];
								$archive['show_name_soundex'] =$this->soundex_($data['Schedule']['show_name']);

								$archive['Schedule'] = implode(',',$data['Schedule']['Schedules']);
								
							}
						}
					}
				}catch(\Exception $e){
			    	return false;			    	
			    }
			    
				//Add data to tags table
				try{
					$archive['Tag'] ='';
					$archive['Tagid'] ='';
					$archive['TagSX'] ='';
			    	if(isset($data['Tag'])){
						if(!empty($data['Tag'])){
							if(!empty($data['Tag']['Tag'])){
								$tagid = array();
						        $new_tags = explode(",",$data['Tag']['Tag']);
						        if(!empty($new_tags)){
						        	foreach($new_tags as $item){
						        		$tag = new Tag(['name'=>$item, 'type'=>'episode']);
						        		$addtag = $tag->save();
										$episode->tags()->attach($tag->id);
										array_push($tagid, $tag->id);
									}

									$archive['Tag'] =$data['Tag']['Tag'];
									$archive['TagSX'] =$this->soundex($data['Tag']['Tag']);
									$archive['Tagid'] = implode(',',$tagid);
									//echo"<pre>";print_r($archive['Tag']);die;
						        }
							}
						}
					}
			    }catch(\Exception $e){
			    	//echo $e->getMessage();die('tags ');
			    	return false;			    	
			    }
			    // Add data to episode's giveaways
			    try{
			    	$archive['description'] = "";
					if(isset($data['Giveaway'])){
						$dec = array();
						foreach($data['Giveaway'] as $giveaway){
							if(!empty($giveaway['description'])){
								array_push($dec, $giveaway['description']);
								$newgiveaway = array('episodes_id'=>$episode->id, 'description' => $giveaway['description']);
								$episode_way = new Giveaway($newgiveaway);
								$episode_way->save();
							}
						}
						
						$archive['description'] =implode(',',$dec);
					}
				}catch(\Exception $e){
					return false;
			    }
			    // Add data to episode's hosts
			    try{
			    	$archive['Host']          = "";
			    	$archive['HostSX']        = "";
			    	$archive['Hostid']        = ""; 
			    	$archive['Cohost']        = "";
			    	$archive['Cohostid']      = "";
			    	$archive['CohostSX']      = "";
			    	$archive['Podcasthost']   = "";
			    	$archive['Podcasthostid'] = "";
			    	$archive['PodcasthostSX'] = "";
			    	// Add the hosts
			    	if(isset($data['Host'])){
						if(!empty($data['Host'])){
							if(!empty($data['Host']['Host'])){
								foreach($data['Host']['Host'] as $host){
									$episode->hosts()->attach($host, ['off'=>0, 'type'=>'host']);
								}	
								//echo"<pre>";print_r($data['Host']['Host']);die;
								$archive['Host'] = $data['Host']['Name'];
								$archive['HostSX'] = $this->soundex($data['Host']['Name']);
								$archive['Hostid'] =implode(',',$data['Host']['Host']);	
							}
						}
					}

					// Add the cohosts
					if(isset($data['Cohost'])){
						if(!empty($data['Cohost'])){
							if(!empty($data['Cohost']['Cohost'])){
								foreach($data['Cohost']['Cohost'] as $cohost){
									$episode->hosts()->attach($cohost, ['off'=>0, 'type'=>'cohost']);
								}
									$archive['Cohostid'] =implode(',',$data['Cohost']['Cohost']);
									$archive['Cohost'] =$data['Cohost']['Name'];
									$archive['CohostSX'] =$this->soundex($data['Cohost']['Name']);
						
							}
						}
					}

					// Add the podcast hosts
					if(isset($data['Podcasthost'])){
						if(!empty($data['Podcasthost'])){
							if(!empty($data['Podcasthost']['Podcasthost'])){
								foreach($data['Podcasthost']['Podcasthost'] as $podcathost){
									$episode->hosts()->attach($podcathost, ['off'=>0, 'type'=>'podcasthost']);
								}
								$archive['Podcasthostid'] =implode(',',$data['Podcasthost']['Podcasthost']);
								$archive['Podcasthost'] = $data['Cohost']['Name'];
								$archive['PodcasthostSX'] = $this->soundex($data['Cohost']['Name']);
							}
						}
					}
			    }
			    catch(\Exception $e){
					return false;
			    }

			    // Add data to episode's guests 
			    try{
			    	$archive['Guest']   = "";
			    	$archive['Guestid'] = "";
			    	$archive['GuestSX'] = "";
			    	if(isset($data['Guest'])){
						if(!empty($data['Guest'])){
							if(!empty($data['Guest']['Guest'])){
								foreach($data['Guest']['Guest'] as $guest){
									$episode->guests()->attach($guest, ['off'=>0]);
								}	
								$archive['Guestid'] = implode(',',$data['Guest']['Guest']);
								$archive['Guest'] = $data['Guest']['Name'];
								$archive['GuestSX'] = $this->soundex($data['Guest']['Name']);
							}
						}
					}
			    }
			    catch(\Exception $e){
					return false;
			    }

			    // Add data to episode's sponsors 
			    try{
			    	$archive['Sponsor'] = "";
			    	if(isset($data['Sponsor'])){
						if(!empty($data['Sponsor'])){
							if(!empty($data['Sponsor']['Sponsor'])){
								$episode->sponsors()->attach($data['Sponsor']['Sponsor'], ['off'=>0]);	
									$archive['Sponsor'] = $data['Sponsor']['Sponsor'];
							}
						}
					}
			    }
			    catch(\Exception $e){
					return false;
			    }

			    // Save data to episodes_has_categories
				try{
					$archive['Category']   = "";
					$archive['Categoryid'] = "";
					$archive['CategorySX'] = "";
					if(isset($data['Category']) && !empty($data['Category'])){
						if(!empty($data['Category']['Category'])){		
							foreach($data['Category']['Category'] as $cate){
								if(!empty($cate)){
									$newcate = array('episodes_id'=>$episode->id, 'categories_id' => $cate );
									$episodecate = new EpisodesHasCategory($newcate);	
									$episodecate->save();
								}
							}	
							//$archive['Category']   =$data['Category']['Name'];
							//$archive['CategorySX']   =$this->soundex($data['Category']['Name']);
							$archive['Categoryid'] =implode(',',$data['Category']['Category']);												
						}
					}
				}
				catch(\Exception $e){
					return false;
			    }
			    //save data in archive table
			    try{
			    	//$archive['Category']   =$data['Category']['Name'];
					//$archive['CategorySX']   =$this->soundex($data['Category']['Name']);
					$archive['Categoryid'] =implode(',',$data['Category']['Category']);
			    	if($episode->date >= Carbon::now()){
						$archived = new Archive();
	            		$archived->add($data,$archive,$episode);
	            	}
				}
				catch(\Exception $e){
					return false;
			    }

				return $episode->id;
			}catch(\Exception $e){
				return false;
		    }
		}
	}

	/**
	*@description ADD episode schedule
	*@param $data contains the data to be updated
	*@return boolean
	*/
	public function episode_schedule($data){
		if(!empty($data)){
            try{
                if(isset($data['EpisodeScheduledShare'])){
                	$data['EpisodeScheduledShare']['users_id'] = \Auth::user()->id;
                    try{
                        if(!empty($data['EpisodeScheduledShare']['is_twitter'])){

                        	$selected_date=$data['EpisodeScheduledShare']['twitter_date'].' '.$data['EpisodeScheduledShare']['twitter_time'];
                    		$newdate = date_create_from_format('m-d-Y h:i A', $selected_date);
                    		$date_string = date_format($newdate, 'Y-m-d H:i:s');

                        	$data['EpisodeScheduledShare']['twitter_time'] =$date_string;
                        }
                   
                        if(!empty($data['EpisodeScheduledShare']['is_facebook'])){

                        	$selected_date=$data['EpisodeScheduledShare']['facebook_date'].' '.$data['EpisodeScheduledShare']['facebook_time'];
                    		$newdate = date_create_from_format('m-d-Y h:i A', $selected_date);
                    		$date_string = date_format($newdate, 'Y-m-d H:i:s');
                    		$data['EpisodeScheduledShare']['facebook_time'] =$date_string;
                        }
                        $episode_obj = new EpisodeScheduledShare($data['EpisodeScheduledShare']);
                                    $episode_obj->save();

                    }catch(\Exception $e){
                        return false;
                    }
                }
                return true;
            }catch(\Exception $e){
                return false;
            }
                
        }
	}

	/**
	*@description ADD episode  twitter schedule
	*@param $data contains the data to be updated
	*@return boolean
	*/
	public function episode_schedule_twitter($data){
		if(!empty($data)){
            try{
                if(isset($data['EpisodeScheduledShare'])){
                    if(!empty($data['EpisodeScheduledShare'])){
                    	$user_id = \Auth::user()->id;
                    	 $data['EpisodeScheduledShare']['users_id'] = $user_id;
                    	//$selected_date = '';
                    	if($data['EpisodeScheduledShare']['for'] == 'twitter'){
                    		$selected_date=$data['EpisodeScheduledShare']['twitter_date'].' '.$data['EpisodeScheduledShare']['twitter_time'];
                    		$newdate = date_create_from_format('m-d-Y h:i A', $selected_date);
                    		$date_string = date_format($newdate, 'Y-m-d H:i:s');
                        	$data['EpisodeScheduledShare']['twitter_time'] =$date_string;
                    	}
                    	if($data['EpisodeScheduledShare']['for'] == 'facebook'){
                    		$selected_date =$data['EpisodeScheduledShare']['facebook_date'].' '.$data['EpisodeScheduledShare']['facebook_time'];
                    		$newdate = date_create_from_format('m-d-Y h:i A', $selected_date);
                    		$date_string = date_format($newdate, 'Y-m-d H:i:s');
                        	$data['EpisodeScheduledShare']['facebook_time'] =$date_string;
                    	}
                        	$check_schedule =  EpisodeScheduledShare::where('episodes_id', '=' ,$data['EpisodeScheduledShare']['episodes_id'])->where('users_id', '=', $user_id)->where('type', '=',$data['EpisodeScheduledShare']['type'])->first();
                        	
                        	if($check_schedule){
                        		$check_schedule->update($data['EpisodeScheduledShare']);
                        	}else{
                        		$episode_obj = new EpisodeScheduledShare($data['EpisodeScheduledShare']);
                                $episode_obj->save();
                        	}
                    }
                }
            	return true;
            }catch(\Exception $e){
            	echo $e->getMessage();die();
                return false;
            }
                
        }
	}

	/**
	 *@description Edit the episode
	 *@param $id is the episode id and the $data contains the data to be updated
	 *@return boolean
	 */
	public function edit($id=0, $data){
		if($id != 0){
			if(!empty($data)){
				
				try{
					$episode = Episode::find($id);
					$episode->update($data['Episode']);
					//channels
				    try{
				    	$old_channels = array();
						$old_episode_channels = $episode->channels()->get();
						if($old_episode_channels->count() > 0){
				            foreach($old_episode_channels as $channel){
				                $old_channels[] = $channel->id;
				            }
				        }	    	
				    
				    	if(isset($data['FeaturedOnChannel'])){
							if(!empty($data['FeaturedOnChannel'])){
								if(!empty($data['FeaturedOnChannel']['FeaturedOnChannel'])){	
							        if(!empty($old_channels)){
							        	foreach($old_channels as $chn){
											if(!in_array($chn, $data['FeaturedOnChannel']['FeaturedOnChannel'])){
												$episode->channels()->detach($chn);
											}
										}
							        }
							        
									foreach($data['FeaturedOnChannel']['FeaturedOnChannel'] as $item){
										if(!in_array($item, $old_channels)){
											$episode->channels()->attach($item);
										}
									}
								}
							}
						}else{
							if(!empty($old_channels)){
					        	foreach($old_channels as $chn){
									$episode->channels()->detach($chn);									
								}
					        }
						}
					}
					catch(\Exception $e){
						
						return false;
				    }				
				    // Update data to shows_has_host
				    try{
				    	$old_hosts_ids = array();
				    	$old_cohosts_ids = array();
				    	$old_podcasthosts_ids = array();

				    	//Fetch old hosts
						$old_hosts = $episode->hosts()->where('episodes_has_hosts.type', 'host')->get();
						if($old_hosts->count() > 0){
				            foreach($old_hosts as $host){
				                $old_hosts_ids[] = $host->id;
				            }
				        }	

				        //Fetch old cohosts
				        $old_cohosts = $episode->hosts()->where('episodes_has_hosts.type', 'cohost')->get();
						if($old_hosts->count() > 0){
				            foreach($old_cohosts as $cohost){
				                $old_cohosts_ids[] = $cohost->id;
				            }
				        }	

				        //Fetch old podcast hosts
				        $old_podcasthosts = $episode->hosts()->where('episodes_has_hosts.type', 'podcasthost')->get();
						if($old_podcasthosts->count() > 0){
				            foreach($old_podcasthosts as $podcasthost){
				                $old_podcasthosts_ids[] = $podcasthost->id;
				            }
				        }	
				      
				    	// Don't know what is "Off" here. For now, by default send 0 to it.
				    	// Update the hosts
				    	if(isset($data['Host'])){
							if(!empty($data['Host'])){
								if(!empty($data['Host']['Host'])){
									if(!empty($old_hosts_ids)){
							        	foreach($old_hosts_ids as $hst){
											if(!in_array($hst, $data['Host']['Host'])){
												$episode->hosts()->wherePivot('episodes_has_hosts.type', 'like', 'host')->detach($hst);
											}
										}
							        }
									foreach($data['Host']['Host'] as $host){
										if(!in_array($host, $old_hosts_ids)){
											$episode->hosts()->where('episodes_has_hosts.type', 'host')->attach($host, ['off'=>0, 'type'=>'host']);
										}
									}							
								}
							}
						}else{
							if(!empty($old_hosts_ids)){
					        	foreach($old_hosts_ids as $hst){
									$episode->hosts()->wherePivot('episodes_has_hosts.type', 'like', 'host')->detach($hst);		
								}
					        }
						}

						// Update the cohosts
						if(isset($data['Cohost'])){
							if(!empty($data['Cohost'])){
								if(!empty($data['Cohost']['Cohost'])){
									if(!empty($old_cohosts_ids)){
							        	foreach($old_cohosts_ids as $chst){
											if(!in_array($chst, $data['Cohost']['Cohost'])){
												$episode->hosts()->wherePivot('episodes_has_hosts.type', 'like', 'cohost')->detach($chst);
											}
										}
							        }
									foreach($data['Cohost']['Cohost'] as $cohost){
										if(!in_array($cohost, $old_cohosts_ids)){
											$episode->hosts()->attach($cohost, ['off'=>0, 'type'=>'cohost']);
										}
									}
								}
							}
						}else{
							if(!empty($old_cohosts_ids)){
					        	foreach($old_cohosts_ids as $chst){
									$episode->hosts()->wherePivot('episodes_has_hosts.type', 'like', 'cohost')->detach($chst);									
								}
					        }
						}

						// Update the podcast hosts
						if(isset($data['Podcasthost'])){
							if(!empty($data['Podcasthost'])){
								if(!empty($data['Podcasthost']['Podcasthost'])){
									if(!empty($old_podcasthosts_ids)){
							        	foreach($old_podcasthosts_ids as $phst){
											if(!in_array($phst, $data['Podcasthost']['Podcasthost'])){
												$episode->hosts()->wherePivot('episodes_has_hosts.type', 'like', 'podcasthost')->detach($phst, ['episodes_has_hosts.type' => 'podcasthost']);
											}
										}
							        }
									foreach($data['Podcasthost']['Podcasthost'] as $podcathost){
										if(!in_array($podcathost, $old_podcasthosts_ids)){
											$episode->hosts()->attach($podcathost, ['off'=>0, 'type'=>'podcasthost']);
										}
									}
								}
							}
						}else{
							if(!empty($old_podcasthosts_ids)){
					        	foreach($old_podcasthosts_ids as $phst){
									$episode->hosts()->wherePivot('episodes_has_hosts.type', 'like', 'podcasthost')->detach($phst);									
								}
					        }
						}						
				    }
				    catch(\Exception $e){
				    	//echo $e->getMessage();die('host');
						return false;
				    }  
				    // Update the guests
				    try{
				    	$old_guests_ids =array();
				    	//Fetch old guest
				        $old_guests = $episode->guests()->get();
						if($old_guests->count() > 0){
				            foreach($old_guests as $gust){
				                $old_guests_ids[] = $gust->id;
				            }
				        }
				    	// Update the guest
						if(isset($data['Guest'])){
							if(!empty($data['Guest'])){
								if(!empty($data['Guest']['Guest'])){
									if(!empty($old_guests_ids)){
							        	foreach($old_guests_ids as $guest){
											if(!in_array($guest, $data['Guest']['Guest'])){
												$episode->guests()->detach($guest);
											}
										}
							        }
									foreach($data['Guest']['Guest'] as $guests){
										if(!in_array($guests, $old_guests_ids)){
											$episode->guests()->attach($guests, ['off'=>0]);
										}
									}
								}
							}
						}else{
							if(!empty($old_guests_ids)){
					        	foreach($old_guests_ids as $guest){
									$episode->guests()->detach($guest);									
								}
					        }
						}
				    }catch(\Exception $e){
						//echo $e->getMessage();die('gust');
						return false;
				    }
					// Update data to categories episodes
				    try{
				    	$old_categories = array();
						$old_episode_categories = $episode->episodes_has_categories()->get();
						if($old_episode_categories->count() > 0){
				            foreach($old_episode_categories as $category){
				                $old_categories[] = $category->id;
				            }
				        }	    					    	
				    	if(isset($data['Category'])){
							if(!empty($data['Category'])){
								if(!empty($data['Category']['Category'])){
							        if(!empty($old_categories)){
							        	foreach($old_categories as $cat){
											if(!in_array($cat, $data['Category']['Category'])){
											$del_episode_cat_object = EpisodesHasCategory::where('episodes_id' , '=', $episode->id)->where('categories_id', '=', $cat)->delete();
											}
										}
							        }
									foreach($data['Category']['Category'] as $item){
										if(!empty($item)){
											if(!in_array($item, $old_categories)){
											$newcat = array('episodes_id'=>$episode->id, 'categories_id' => $item );
											$episode_cat = EpisodesHasCategory::updateOrCreate($newcat);
											$episode_cat->save();
										}
										}
									}
								}
							}
						}else{
							if(!empty($old_categories)){
					        	foreach($old_categories as $cat){
								$del_episode_cat_object = EpisodesHasCategory::where('episodes_id' , '=', $episode->id)->where('categories_id', '=', $cat)->delete();									
								}
					        }
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('cattt');
						return false;
				    }
					// Update data giveways episodes
				    try{
				    	$old_giveways = array();
						$old_episode_giveways = $episode->giveaways()->get();
						if($old_episode_giveways->count() > 0){
				            foreach($old_episode_giveways as $giveway){
				                $old_giveways[] = $giveway->id;
				            }
				        }	    					    	
				    	if(isset($data['Giveaway'])){
							if(!empty($data['Giveaway'])){
						        if(!empty($old_giveways)){
						        	foreach($old_giveways as $way){
										if(!in_array($way, $data['Giveaway'])){
										$del_episode_away_object = Giveaway::where('episodes_id' , '=', $episode->id)->where('id', '=', $way)->delete();
										}
									}
						        }
								foreach($data['Giveaway'] as $item){
									if(!in_array($item, $old_giveways)){
										$newway = array('episodes_id'=>$episode->id, 'description' => $item['description'] );
										$episode_way = Giveaway::updateOrCreate($newway);
										$episode_way->save();
									}
								}
							}
						}else{
							if(!empty($old_giveways)){
					        	foreach($old_giveways as $way){
								$del_episode_away_object = Giveaway::where('episodes_id' , '=', $episode->id)->where('id', '=', $way)->delete();									
								}
					        }
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('give');
						return false;
				    }	
				    // Update into table episodes_has_tags.			    
				    try{
				    	$old_tags = array();
						$old_episode_tags = $episode->tags()->get();
						if($old_episode_tags->count() > 0){
				            foreach($old_episode_tags as $tag){
				                $old_tags[] = $tag->id;
				            }
				        }	    	
				    	//echo "<pre>";print_R($data['Tag']);die;
				    	if(isset($data['Tag'])){
							if(!empty($data['Tag'])){
								if(!empty($data['Tag']['Tag'])){
									$new_tags = explode(",",$data['Tag']['Tag']);
									$new_tag_ids = json_decode($data['Tag']['TagIds']);
									if(!empty($new_tag_ids)){
										foreach($new_tag_ids as $k=>$nid){
											if(!in_array($k, $new_tags)){
												$episode->tags()->detach($nid);
												//unset(array_search ($k,$new_tags));
											}else{
												$check_key = array_search($k,$new_tags);
												unset($new_tags[$check_key]);
											}
										}
									}
						        	if(!empty($new_tags)){
							        	foreach($new_tags as $item){
											$tag = new Tag(['name'=>$item, 'type'=>'episode']);
							        		$addtag = $tag->save();
											$episode->tags()->attach($tag->id);
										}
									}
								}else{
									if(!empty($data['Tag']['TagIds'])){
										$old_ids = json_decode($data['Tag']['TagIds']);
										foreach($old_ids as $id){
											$episode->tags()->detach($id);		
										}
									}
								}
							}
						}
					}
					catch(\Exception $e){
						return false;
				    }
				    // Update Sponsors
				    try{
						$old_sponsor = $episode->sponsors()->get();					    
						if(isset($data['Sponsor'])){
							if(!empty($data['Sponsor'])){
								if(!empty($data['Sponsor']['Sponsor'])){	
									if($old_sponsor->count() > 0){
										$sponsors = EpisodesHasSponsor::where('episodes_id', $episode->id )->update(['sponsors_id'=>$data['Sponsor']['Sponsor'] ,'off'=>0]);
									}else{
										$sponsors = EpisodesHasSponsor::insert(['episodes_id' =>$episode->id ,'sponsors_id'=>$data['Sponsor']['Sponsor'],'off'=>0]);
									}
								}
							}
						}
					}
					catch(\Exception $e){
						return false;
				    }
					return true;
				}catch(\Exception $e){
					return false;
			    }
			}
		}else{
			return false;
		}
	}

	public function addEncore($id=0, $data){
		if($id != 0){
			if(!empty($data)){
				try{
					
					$old_episode = Episode::find($id);					
					$episode = new Episode();
					$episode->channels_id                   = $old_episode->channels_id;
					$episode->shows_id                      = $data['Schedule']['shows_id'];
					$episode->schedules_id                  = $data['Schedule']['schedules_id'];
					$episode->episodes_id                   = $id;
					$episode->year                          = $old_episode->year;
					$episode->month                         = $old_episode->month;
					$episode->date                          = date("Y-m-d", strtotime($data['Schedule']['date']));
					$episode->time                          = date("G:i", strtotime($data['Schedule']['time']));
					if($data['Episode']['title'] != ''){
						$episode->title                     = "Encore: ".$old_episode->title;  
					}else{
						$episode->title                     = $data['Episode']['title'];  
					}					 
					$episode->title_soundex                 = $old_episode->title_soundex;  
					if($data['Episode']['description'] != ''){
						$episode->description               = $old_episode->description;  
					}else{
						$episode->description               = $data['Episode']['description'];  
					}  
					$episode->is_encore                     = 1;
					$episode->is_online                     = $old_episode->is_online;   
					$episode->is_featured                   = $old_episode->is_featured;   
					$episode->is_itunes                     = $old_episode->is_itunes;  
					$episode->notes                         = $old_episode->notes;
					$episode->talking_points                = $old_episode->talking_points;   
					$episode->segment1                      = $old_episode->segment1;   
					$episode->segment1_title                = $old_episode->segment1_title;   
					$episode->segment1_video_url            = $old_episode->segment1_video_url; 
					$episode->segment2                      = $old_episode->segment2;   
					$episode->segment2_title                = $old_episode->segment2_title; 
					$episode->segment2_video_url            = $old_episode->segment2_video_url;
					$episode->segment3                      = $old_episode->segment3;   
					$episode->segment3_title                = $old_episode->segment3_title;   
					$episode->segment3_video_url            = $old_episode->segment3_video_url;   
					$episode->segment4                      = $old_episode->segment4;   
					$episode->segment4_title                = $old_episode->segment4_title;   
					$episode->segment4_video_url            = $old_episode->segment4_video_url;   
					$episode->events_promote                = $old_episode->events_promote;   
					$episode->special_offers                = $old_episode->special_offers;   
					$episode->listener_interaction          = $old_episode->listener_interaction;   
					$episode->giveaways                     = $old_episode->giveaways;   
					$episode->internal_host_notes           = $old_episode->internal_host_notes;   
					$episode->type                          = $old_episode->type;   
					$episode->assigned_type                 = $old_episode->assigned_type;   
					$episode->file                          = $old_episode->file;   
					$episode->stream                        = $old_episode->stream;   
					$episode->encores_number                = $old_episode->encores_number;  
					$episode->downloads                     = $old_episode->downloads;   
					$episode->episodescol                   = $old_episode->episodescol;  
					$episode->is_cohost_override_banner     = $old_episode->is_cohost_override_banner;   
					$episode->is_cohost_override_show_title = $old_episode->is_cohost_override_show_title;   
					$episode->cohost_show_id                = $old_episode->cohost_show_id;   
					$episode->cohost_show_name              = $old_episode->cohost_show_name;   
					$episode->favorites                     = $old_episode->favorites;   
					$episode->is_uploaded_by_host           = $old_episode->is_uploaded_by_host; 
					$episode->video_url                     = $old_episode->video_url;   

					if($episode->save()){
						$result = $this->procesEncoreImage($episode, 'Episode');
					}
					//Add data to tags table
					try{
						$old_tags = array();
						$old_episode_tags = $old_episode->tags()->get();
						if($old_episode_tags->count() > 0){
				            foreach($old_episode_tags as $tag){
				                $old_tags[] = $tag->id;
				            }
				        }
				        if(!empty($old_tags)){
				        	foreach($old_tags as $item){
								$episode->tags()->attach($item);
							}
				        }								
				    }catch(\Exception $e){
				    	return false;			    	
				    }
				    
				    // Add data to episode's giveaways
				    try{

				    	$old_giveaways = array();
						$old_episode_giveaways = $old_episode->giveaways()->get();
						if($old_episode_giveaways->count() > 0){
				            foreach($old_episode_giveaways as $giveaway){
				            	$newway = array('episodes_id'=>$episode->id, 'description' => $giveaway->description );
				            	$episode_way = Giveaway::updateOrCreate($newway);
								$episode_way->save();
							}
				        }
					}catch(\Exception $e){
						return false;
				    }

				    // Add data to episode's hosts
				    try{				    	
				    	// Don't know what is "Off" here. For now, by default send 0 to it.
				    	// Fetch old hosts
						$old_hosts = $old_episode->hosts()->where('episodes_has_hosts.type', 'host')->get();
						if($old_hosts->count() > 0){
							// Add hosts with the encore
				            foreach($old_hosts as $host){					                
				                $episode->hosts()->where('episodes_has_hosts.type', 'host')->attach($host->id, ['off'=>0, 'type'=>'host']);
				            }
				        }	

				        //Fetch old cohosts
				        $old_cohosts = $old_episode->hosts()->where('episodes_has_hosts.type', 'cohost')->get();
						if($old_cohosts->count() > 0){
				            foreach($old_cohosts as $cohost){
				                $episode->hosts()->attach($cohost->id, ['off'=>0, 'type'=>'cohost']);
				            }
				        }	

				        //Fetch old podcast hosts
				        $old_podcasthosts = $old_episode->hosts()->where('episodes_has_hosts.type', 'podcasthost')->get();
						if($old_podcasthosts->count() > 0){
				            foreach($old_podcasthosts as $podcasthost){			               
				                $episode->hosts()->attach($podcasthost->id, ['off'=>0, 'type'=>'podcasthost']);
				            }
				        }  
				    }
				    catch(\Exception $e){
						return false;
				    }
				   
				    // Add data to episode's guests 
				    try{
				    	
				    	//Fetch old guest
				        $old_guests = $old_episode->guests()->get();
						if($old_guests->count() > 0){
				            foreach($old_guests as $guest){
				               $episode->guests()->attach($guest->id, ['off'=>0]);
				            }
				        }
				    }
				    catch(\Exception $e){
						return false;
				    }
				    
				    // Add data to episode's sponsors 
				    try{
				    	$old_sponsor = array();
						$old_episode_sponsor = $old_episode->sponsors()->get();
						if($old_episode_sponsor->count() > 0){
				            foreach($old_episode_sponsor as $spon){
				                $newspon  = array('episodes_id' =>$episode->id ,'sponsors_id'=>$spon->id);
				                $sponsors = EpisodesHasSponsor::insert($newspon);
				            }
				        }
				    }
				    catch(\Exception $e){
						return false;
				    }
				   
				    // Save data to episodes_has_categories
					try{

				    	$old_categories = array();
						$old_episode_categories = $old_episode->episodes_has_categories()->get();
						if($old_episode_categories->count() > 0){
				            foreach($old_episode_categories as $category){
				                $newcat = array('episodes_id'=>$episode->id, 'categories_id' => $category->categories_id );
								$episode_cat = EpisodesHasCategory::updateOrCreate($newcat);
								$episode_cat->save();
				            }
				        }
					}
					catch(\Exception $e){
						return false;
				    }			    

				    //Add data to episodes_has_schedules
					try{
						$old_episode_schedules = $old_episode->schedules()->get();
						if($old_episode_schedules->count() > 0){
				            foreach($old_episode_schedules as $schedule){
				            	$episode->schedules()->attach($schedule,['date'=>$episode->date, 'time'=>$episode->time]);
				            }
				        }
					}catch(\Exception $e){
				    	return false;			    	
				    }
					
					//die('success');

					/*
					$date = '';
					$desc = '';
					$is_twitter  = '';
					$is_facebook = '';
					if(isset($data['EpisodeScheduledShare'])){
						if(!empty($data['EpisodeScheduledShare'])){
							if(!empty($data['EpisodeScheduledShare']['time'])){
								$selected_date =$data['EpisodeScheduledShare']['time']['year'].'-'.$data['EpisodeScheduledShare']['time']['month'].'-'.$data['EpisodeScheduledShare']['time']['day'].' '. $data['EpisodeScheduledShare']['time']['hour'].':'.$data['EpisodeScheduledShare']['time']['min'].':00 '.$data['EpisodeScheduledShare']['time']['meridian'];
            						$date_string = strtotime($selected_date);
            						$date =date('Y-m-d H:i:s',$date_string);
            						//echo"$date";die();
							}
							if(!empty($data['EpisodeScheduledShare']['desc'])){
								$desc = $data['EpisodeScheduledShare']['desc'];
							}
							if(!empty($data['EpisodeScheduledShare']['is_twitter'])){
								$is_twitter = $data['EpisodeScheduledShare']['is_twitter'];
							}
							$data_array = array('users_id'=>\Auth::user()->id,'time' =>$date,'episodes_id'=>$id,'is_twitter'=>$is_twitter,'is_facebook'=>$is_facebook ,'desc'=>$desc);
							$episode = EpisodeScheduledShare::where('episodes_id',$id)->first();
							if(is_null($episode)){
								$episode_obj = new EpisodeScheduledShare($data_array);
								$episode_obj->save();
							}else{

								$episode->update($data_array);
							}
							// echo"<pre>";print_r($episode); die();
							// $episode->update($data_array);

						}
					}
					*/
					try{
						$oldepisode = Episode::where('episodes_id', '=',$id)->count();
						$old_episode->update(['encores_number' =>$oldepisode+1]);
					}catch(\Exception $e){
						//echo $e->getMessage();die();
						return false;
					}

				}catch(\Exception $e){
					
					return false;
			    }
				return true;
			}
		}else{
			return false;
		}
	}

	/**
     * Fetch the encore details of the episode
     * @param $episodeId is the id of the episode
     */
    public function getEncores($episodeId) {
        if(!empty($episodeId)){
        	
            $episode = $this->select('episodes.date', 'episodes.time')
            				->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
            				->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
            				->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id')
            				->where('episodes_id', $episodeId)
            				->get();
            return $episode;
        }
    }

	public function deleteEpisode($id=0){
		
		if($id != '' || $id !=0){
			$episode = Episode::find($id);
			
			if ($episode->has('channels')) {
				$episode->channels()->detach();
			}
			
			if ($episode->has('guests')) {
				$episode->guests()->detach();	
			  	
			}
			if ($episode->has('sponsors')) {
					$episode->sponsors()->detach();
			}

			if ($episode->has('tags')) {
				$episode->tags()->delete();
			}
			if ($episode->has('giveaways')) {
				$episode->giveaways()->delete();	
			}
			if ($episode->has('episodes_has_categories')) {
				$episode->episodes_has_categories()->delete();
			}
			if ($episode->has('hosts')) {
					$episode->hosts()->detach();
			}
			if ($episode->has('episode_scheduled_shares')) {
					$episode->episode_scheduled_shares()->delete();
			}
			deleteMediaLink('Episode', $id);
			$archive = Archive::where('episode_id',$id);
			$archive->delete();
			$episode->delete();
			return true;
		}

	}	

	/**
     * Get count of the upcoming episodes
     *
     */
	public function countAllUpcomingEpisodes($current_user_id, $type=1, $host=0, $sponsor_id=0, $channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$episodes = $this;
		if(!empty($channel_id)){
			$episodes = $episodes->where('episodes.channels_id', '=', $channel_id);
		}if(!empty($host_id)){
			$episodes = $episodes->where('EpisodesHasHost.hosts_id', '=', $host_id);
		}if(!empty($sponsor_id)){
			$episodes = $episodes->where('EpisodesHasSponsor.sponsors_id', '=', $sponsor_id);
		}

		$episodes = $episodes->select( DB::raw('(episodes.id) AS o_episode_id, episodes.channels_id, episodes.shows_id, episodes.schedules_id, episodes.episodes_id, episodes.created_at, episodes.updated_at, episodes.year,episodes.month, episodes.date, episodes.time, episodes.title_soundex, episodes.description, episodes.is_encore, episodes.is_online, episodes.is_featured, episodes.is_itunes, episodes.notes, episodes.talking_points, episodes.segment1, episodes.segment1_title, episodes.segment1_video_url, episodes.segment2, episodes.segment2_title, episodes.segment2_video_url, episodes.segment3, episodes.segment3_title, episodes.segment3_video_url, episodes.segment4, episodes.segment4_title, episodes.segment4_video_url, episodes.events_promote, episodes.special_offers, episodes.listener_interaction, episodes.giveaways, episodes.internal_host_notes, episodes.type, episodes.assigned_type, episodes.file, episodes.stream, episodes.encores_number, episodes.downloads, episodes.episodescol, episodes.is_cohost_override_banner, episodes.is_cohost_override_show_title, episodes.cohost_show_id, episodes.cohost_show_name, episodes.favorites, episodes.is_uploaded_by_host, episodes.video_url, ( CONCAT( DATE_FORMAT(episodes.date, "%b %e"), "  ", DATE_FORMAT(episodes.time, "%l:%i %p"))) AS Episode__fulldate, (episodes.title) AS Episode__title_only, (CASE WHEN TRIM(episodes.title)!= "" THEN (CASE WHEN TRIM(episodes.title)!= TRIM(Show.name) THEN CASE WHEN episodes.is_cohost_override_show_title > 0 THEN CONCAT(     episodes.cohost_show_name, ": ", episodes.title) ELSE CONCAT(Show.name, ": ", episodes.title) END ELSE Show.name END) ELSE Show.name END) AS Episode__title,
			( SELECT  COUNT(*) FROM channels_has_featured_episodes CHFE WHERE CHFE.episodes_id = episodes.id GROUP BY CHFE.episodes_id) AS Episode__featured,`Schedule`.`id`, 
  			`Schedule`.`channels_id`, `Schedule`.`shows_id`, `Schedule`.`schedules_id`, `Schedule`.`deleted`, `Schedule`.`dow`, `Schedule`.`week`, `Schedule`.`month`,   `Schedule`.`year`,   `Schedule`.`time`,   `Schedule`.`duration`,   `Schedule`.`type`, `Schedule`.`file`, `Schedule`.`stream`,  `Schedule`.`type_label`, `Schedule`.`start_date`, `Schedule`.`end_date`,  `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`,  `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`,   `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`,   `Show`.`favorites`, `Show`.`is_created_by_host`, `Channel`.`id`,   `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`,   `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`,   `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`,   `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`,   `Channel`.`sam_file_connecting`,   `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`,   `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`,   `Channel`.`theme_name`'))
					->leftJoin('episodes_has_hosts AS EpisodesHasHost', 'EpisodesHasHost.episodes_id', '=', 'episodes.id')
					->leftJoin('hosts AS Host', 'Host.id', '=', 'EpisodesHasHost.hosts_id')
					->leftJoin('episodes_has_sponsors AS EpisodesHasSponsor', 'EpisodesHasSponsor.episodes_id', '=', 'episodes.id')
					->leftJoin('sponsors AS Sponsor', 'Sponsor.id', '=', 'EpisodesHasSponsor.sponsors_id')
					->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
					->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
					->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id')
					->whereRaw('( UNIX_TIMESTAMP( CONCAT( episodes.date, " ", episodes.time )) + Schedule.duration * 60 > UNIX_TIMESTAMP())  OR ( episodes.is_uploaded_by_host = "1"  AND UNIX_TIMESTAMP( CONCAT( episodes.date, " ", episodes.time ) ) > UNIX_TIMESTAMP())')
					->groupBy('episodes.id');
					
		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }
		// Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'DESC')
								->orderBy('episodes.time', 'DESC');
        } 
		$episodes = $episodes->count();
		return $episodes;
	}

	/**
     * Get all the upcoming episodes 
     *
     */
	public function getAllUpcomingEpisodes($current_user_id, $type=1, $host_id=0, $sponsor_id=0, $channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$episodes = $this;
		if(!empty($channel_id)){
			$episodes = $episodes->where('episodes.channels_id', '=', $channel_id);
		}if(!empty($host_id)){
			$episodes = $episodes->where('EpisodesHasHost.hosts_id', '=', $host_id);
		}if(!empty($sponsor_id)){
			$episodes = $episodes->where('EpisodesHasSponsor.sponsors_id', '=', $sponsor_id);
		}
		//echo $current_user_id;die;
		$episodes = $episodes->select( DB::raw('Sponsor.name as sponsor_name, (episodes.id) AS o_episode_id, episodes.channels_id, episodes.shows_id, episodes.schedules_id,  episodes.episodes_id, episodes.created_at, episodes.updated_at, episodes.year, episodes.month, episodes.date, episodes.time, episodes.title_soundex, episodes.description, episodes.is_encore, episodes.is_online, episodes.is_featured, episodes.is_itunes, episodes.notes, episodes.talking_points, episodes.segment1, episodes.segment1_title, episodes.segment1_video_url, episodes.segment2, episodes.segment2_title, episodes.segment2_video_url, episodes.segment3, episodes.segment3_title, episodes.segment3_video_url, episodes.segment4, episodes.segment4_title, episodes.segment4_video_url, episodes.events_promote, episodes.special_offers, episodes.listener_interaction, episodes.giveaways, episodes.internal_host_notes, episodes.type, episodes.assigned_type, episodes.file, episodes.stream, episodes.encores_number, episodes.downloads, episodes.episodescol, episodes.is_cohost_override_banner, episodes.is_cohost_override_show_title, episodes.cohost_show_id, episodes.cohost_show_name, episodes.favorites, episodes.is_uploaded_by_host, episodes.video_url, ( CONCAT( DATE_FORMAT(episodes.date, "%b %e"),  "  ", DATE_FORMAT(episodes.time, "%l:%i %p") )) AS Episode__fulldate, (episodes.title) AS episode__title_only, (CASE WHEN TRIM(episodes.title)!= "" THEN ( CASE WHEN TRIM(episodes.title)!= TRIM(Show.name) THEN CASE WHEN episodes.is_cohost_override_show_title > 0 THEN CONCAT( episodes.cohost_show_name, ": ", episodes.title ) ELSE CONCAT(Show.name, ": ", episodes.title ) END ELSE Show.name END) ELSE Show.name END ) AS Episode__title, (SELECT COUNT(*) FROM channels_has_featured_episodes CHFE WHERE CHFE.episodes_id = episodes.id GROUP BY CHFE.episodes_id) AS Episode__featured, `Schedule`.`id`, `Schedule`.`channels_id`, `Schedule`.`shows_id`, `Schedule`.`schedules_id`, `Schedule`.`deleted`, `Schedule`.`dow`, `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`time`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`file`, `Schedule`.`stream`, `Schedule`.`type_label`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`, `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`, `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`, `Show`.`favorites`, `Show`.`is_created_by_host`, `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name`'))
			->leftJoin('episodes_has_hosts AS EpisodesHasHost', 'EpisodesHasHost.episodes_id', '=', 'episodes.id')
			->leftJoin('hosts AS Host', 'Host.id', '=', 'EpisodesHasHost.hosts_id')
			->leftJoin('episodes_has_sponsors AS EpisodesHasSponsor', 'EpisodesHasSponsor.episodes_id', '=', 'episodes.id')
			->leftJoin('sponsors AS Sponsor', 'Sponsor.id', '=', 'EpisodesHasSponsor.sponsors_id')
			->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
			->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
			->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id')
			
			->groupBy('episodes.id1');
			 

		if($type == 2){
			$episodes = $episodes->where('Host.id', $current_user_id)
								 ->whereRaw('(UNIX_TIMESTAMP( CONCAT(episodes.date, " ", episodes.time) ) + Schedule.duration * 60 > UNIX_TIMESTAMP()) 
			    OR ( episodes.is_uploaded_by_host = "0"  AND UNIX_TIMESTAMP( CONCAT(
			          episodes.date, " ", episodes.time)) > UNIX_TIMESTAMP())');
		}else{
			$episodes = $episodes->whereRaw('(UNIX_TIMESTAMP( CONCAT(episodes.date, " ", episodes.time) ) + Schedule.duration * 60 > UNIX_TIMESTAMP()) 
			    OR ( episodes.is_uploaded_by_host = "1"  AND UNIX_TIMESTAMP( CONCAT(
			          episodes.date, " ", episodes.time)) > UNIX_TIMESTAMP())');
		}
		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'ASC')
								->orderBy('episodes.time', 'ASC');
        } 
		$episodes = $episodes->offset($start)->limit($length)->get();
		return $episodes;
	}

	/**
     * Get all the upcoming episodes 
     *
     */
	public function getAllUpcomingEpisodesAjax($current_user_id, $type=1, $host_id=0, $sponsor_id=0, $channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		/*$episodes = $this;
		if(!empty($channel_id)){
			$episodes = $episodes->where('episodes.channels_id', '=', $channel_id);
		}if(!empty($host_id)){
			$episodes = $episodes->where('EpisodesHasHost.hosts_id', '=', $host_id);
		}if(!empty($sponsor_id)){
			$episodes = $episodes->where('EpisodesHasSponsor.sponsors_id', '=', $sponsor_id);
		}
		//echo $current_user_id;die;
		$episodes = $episodes->select( DB::raw('Sponsor.name as sponsor_name, (episodes.id) AS o_episode_id, episodes.channels_id, episodes.shows_id, episodes.schedules_id,  episodes.episodes_id, episodes.created_at, episodes.updated_at, episodes.year, episodes.month, episodes.date, episodes.time, episodes.title_soundex, episodes.description, episodes.is_encore, episodes.is_online, episodes.is_featured, episodes.is_itunes, episodes.notes, episodes.talking_points, episodes.segment1, episodes.segment1_title, episodes.segment1_video_url, episodes.segment2, episodes.segment2_title, episodes.segment2_video_url, episodes.segment3, episodes.segment3_title, episodes.segment3_video_url, episodes.segment4, episodes.segment4_title, episodes.segment4_video_url, episodes.events_promote, episodes.special_offers, episodes.listener_interaction, episodes.giveaways, episodes.internal_host_notes, episodes.type, episodes.assigned_type, episodes.file, episodes.stream, episodes.encores_number, episodes.downloads, episodes.episodescol, episodes.is_cohost_override_banner, episodes.is_cohost_override_show_title, episodes.cohost_show_id, episodes.cohost_show_name, episodes.favorites, episodes.is_uploaded_by_host, episodes.video_url, ( CONCAT( DATE_FORMAT(episodes.date, "%b %e"),  "  ", DATE_FORMAT(episodes.time, "%l:%i %p") )) AS Episode__fulldate, (episodes.title) AS episode__title_only, (CASE WHEN TRIM(episodes.title)!= "" THEN ( CASE WHEN TRIM(episodes.title)!= TRIM(Show.name) THEN CASE WHEN episodes.is_cohost_override_show_title > 0 THEN CONCAT( episodes.cohost_show_name, ": ", episodes.title ) ELSE CONCAT(Show.name, ": ", episodes.title ) END ELSE Show.name END) ELSE Show.name END ) AS Episode__title, (SELECT COUNT(*) FROM channels_has_featured_episodes CHFE WHERE CHFE.episodes_id = episodes.id GROUP BY CHFE.episodes_id) AS Episode__featured, `Schedule`.`id`, `Schedule`.`channels_id`, `Schedule`.`shows_id`, `Schedule`.`schedules_id`, `Schedule`.`deleted`, `Schedule`.`dow`, `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`time`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`file`, `Schedule`.`stream`, `Schedule`.`type_label`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`, `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`, `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`, `Show`.`favorites`, `Show`.`is_created_by_host`, `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name`'))
			->leftJoin('episodes_has_hosts AS EpisodesHasHost', 'EpisodesHasHost.episodes_id', '=', 'episodes.id')
			->leftJoin('hosts AS Host', 'Host.id', '=', 'EpisodesHasHost.hosts_id')
			->leftJoin('episodes_has_sponsors AS EpisodesHasSponsor', 'EpisodesHasSponsor.episodes_id', '=', 'episodes.id')
			->leftJoin('sponsors AS Sponsor', 'Sponsor.id', '=', 'EpisodesHasSponsor.sponsors_id')
			->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
			->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
			->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id')
			
			->groupBy('episodes.id1');
			 

		if($type == 2){
			$episodes = $episodes->where('Host.id', $current_user_id)
								 ->whereRaw('(UNIX_TIMESTAMP( CONCAT(episodes.date, " ", episodes.time) ) + Schedule.duration * 60 > UNIX_TIMESTAMP()) 
			    OR ( episodes.is_uploaded_by_host = "0"  AND UNIX_TIMESTAMP( CONCAT(
			          episodes.date, " ", episodes.time)) > UNIX_TIMESTAMP())');
		}else{
			$episodes = $episodes->whereRaw('(UNIX_TIMESTAMP( CONCAT(episodes.date, " ", episodes.time) ) + Schedule.duration * 60 > UNIX_TIMESTAMP()) 
			    OR ( episodes.is_uploaded_by_host = "1"  AND UNIX_TIMESTAMP( CONCAT(
			          episodes.date, " ", episodes.time)) > UNIX_TIMESTAMP())');
		}
		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'ASC')
								->orderBy('episodes.time', 'ASC');
        } 
		$episodes = $episodes->offset($start)->limit($length)->get();*/
		$episodes = DB::Select(DB::raw('SELECT `Episode`.`id` AS Episode_id, `Episode`.`channels_id`, `Episode`.`shows_id`, `Episode`.`schedules_id`, `Episode`.`episodes_id`, `Episode`.`created_at`, `Episode`.`updated_at`, `Episode`.`year`, `Episode`.`month`, `Episode`.`date`, `Episode`.`time`, `Episode`.`title_soundex`, `Episode`.`description`, `Episode`.`is_encore`, `Episode`.`is_online`, `Episode`.`is_featured`, `Episode`.`is_itunes`, `Episode`.`notes`, `Episode`.`talking_points`, `Episode`.`segment1`, `Episode`.`segment1_title`, `Episode`.`segment1_video_url`, `Episode`.`segment2`, `Episode`.`segment2_title`, `Episode`.`segment2_video_url`, `Episode`.`segment3`, `Episode`.`segment3_title`, `Episode`.`segment3_video_url`, `Episode`.`segment4`, `Episode`.`segment4_title`, `Episode`.`segment4_video_url`, `Episode`.`events_promote`, `Episode`.`special_offers`, `Episode`.`listener_interaction`, `Episode`.`giveaways`, `Episode`.`internal_host_notes`, `Episode`.`type`, `Episode`.`assigned_type`, `Episode`.`file`, `Episode`.`stream`, `Episode`.`encores_number`, `Episode`.`downloads`, `Episode`.`episodescol`, `Episode`.`is_cohost_override_banner`, `Episode`.`is_cohost_override_show_title`, `Episode`.`cohost_show_id`, `Episode`.`cohost_show_name`, `Episode`.`favorites`, `Episode`.`is_uploaded_by_host`, `Episode`.`video_url`, (CONCAT( DATE_FORMAT(`Episode`.`date`, "%b %e"), "&nbsp; ", DATE_FORMAT(`Episode`.`time`, "%l:%i %p") )) AS `Episode__fulldate`, (`Episode`.`title`) AS `Episode__title_only`, (CASE WHEN TRIM(`Episode`.`title`)!="" THEN (CASE WHEN TRIM(`Episode`.`title`)!=TRIM(`Show`.`name`) THEN CASE WHEN `Episode`.`is_cohost_override_show_title`>0 THEN CONCAT( `Episode`.`cohost_show_name`, ": ", `Episode`.`title`) ELSE CONCAT( `Show`.`name`, ": ", `Episode`.`title`) END ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS `Episode__title`, (SELECT COUNT(*) FROM channels_has_featured_episodes CHFE WHERE `CHFE`.`episodes_id`=`Episode`.`id` GROUP BY `CHFE`.`episodes_id`) AS `Episode__featured`, `Schedule`.`id`, `Schedule`.`channels_id`, `Schedule`.`shows_id`, `Schedule`.`schedules_id`, `Schedule`.`deleted`, `Schedule`.`dow`, `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`time`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`file`, `Schedule`.`stream`, `Schedule`.`type_label`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`, `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`, `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`, `Show`.`favorites`, `Show`.`is_created_by_host`, `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name`, `Sponsor`.`name` as sponsor_name FROM `episodes` AS `Episode` LEFT JOIN `episodes_has_hosts` AS `EpisodesHasHost` ON (`EpisodesHasHost`.`episodes_id` = `Episode`.`id`) LEFT JOIN `hosts` AS `Host` ON (`Host`.`id` = `EpisodesHasHost`.`hosts_id`) LEFT JOIN `episodes_has_sponsors` AS `EpisodesHasSponsor` ON (`EpisodesHasSponsor`.`episodes_id` = `Episode`.`id`) LEFT JOIN `sponsors` AS `Sponsor` ON (`Sponsor`.`id` = `EpisodesHasSponsor`.`sponsors_id`) LEFT JOIN `schedules` AS `Schedule` ON (`Episode`.`schedules_id` = `Schedule`.`id`) LEFT JOIN `shows` AS `Show` ON (`Episode`.`shows_id` = `Show`.`id`) LEFT JOIN `channels` AS `Channel` ON (`Episode`.`channels_id` = `Channel`.`id`) WHERE ( (UNIX_TIMESTAMP(CONCAT(`Episode`.`date`, " ", `Episode`.`time`)) + `Schedule`.`duration`*60 > UNIX_TIMESTAMP()) OR (`Episode`.`is_uploaded_by_host`="'.$host_id.'" AND UNIX_TIMESTAMP(CONCAT(`Episode`.`date`, " ", `Episode`.`time`)) > UNIX_TIMESTAMP())) GROUP BY `Episode`.`id` ORDER BY `Episode`.`date` ASC, `Episode`.`time` ASC LIMIT 20'));
		return $episodes;
	}

	/**
     * Get count of the episodes to add encores to them
     *
     */
	public function countEpisodesForEncores($episode_date='', $schedule_id=0, $selected_date='', $current_user_id, $type=1, $host_id=0, $guest_id=0, $show_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$episodes = $this;
		if(!empty($host_id)){
			$episodes = $episodes->where('EpisodesHasHost.hosts_id', '=', $host_id);
		}if(!empty($guest_id)){
			$episodes = $episodes->where('EpisodesHasGuest.guests_id', '=', $guest_id);
		}if(!empty($show_id)){
			$episodes = $episodes->where('episodes.shows_id', '=', $show_id);
		}if(!empty($episode_date)){
			$episodes = $episodes->where('episodes.date', 'like', Carbon::parse($episode_date)->format('Y-m-d'));
		}
		
		$episodes = $episodes->select( DB::raw('`episodes`.`date`, `episodes`.`time`, `episodes`.`year`, `Profile`.`lastname`, `Host`.`id`, `Guest`.`id`, ( CONCAT(    DATE_FORMAT(`episodes`.`date`, "%b %e"), "  ", DATE_FORMAT(`episodes`.`time`, "%l:%i %p"))) AS `Episode__fulldate`, (CASE WHEN TRIM(`episodes`.`title`)!= "" THEN (      CASE WHEN TRIM(`episodes`.`title`)!= TRIM(`Show`.`name`) THEN CASE WHEN `episodes`.`is_cohost_override_show_title` > 0 THEN CONCAT(`episodes`.`cohost_show_name`, ": ",`episodes`.`title`) ELSE CONCAT(`Show`.`name`, ": ", `episodes`.`title`) END ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS `Episode__title`, `episodes`.`id` as s_episode_id, archives.encores_number'))
			->leftJoin('archives', 'archives.episode_id', '=', 'episodes.id')
			->leftJoin('episodes_has_hosts AS EpisodesHasHost', 'EpisodesHasHost.episodes_id', '=', 'episodes.id')
			->leftJoin('hosts AS Host', 'Host.id', '=', 'EpisodesHasHost.hosts_id')
			->leftJoin('episodes_has_guests AS EpisodesHasGuest', 'EpisodesHasGuest.episodes_id', '=', 'episodes.id')
			->leftJoin('guests AS Guest', 'Guest.id', '=', 'EpisodesHasGuest.guests_id')
			->leftJoin('profiles AS Profile', 'Host.users_id', '=', 'Profile.users_id')
			->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
			->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
			->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id')
			->where('episodes.is_encore', '0')
			->groupBy('episodes.id');
			
		$episodes = $episodes->where(function($q){
    							$q->where('episodes.date', '<', date('Y-m-d'));
    							$q->orWhere(function($q1){
    								$q1->where('episodes.date', '=', date('Y-m-d'))
    								   ->where('episodes.time','<=', date('H:i:00'));
    								});
    						   });

		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.cohost_show_name' ,'like', '%'.$search_value.'%')
    								->orWhere('Show.name' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'DESC')
								->orderBy('episodes.time', 'DESC');
        }  
		$episodes = $episodes->get();
		//$episodes = $episodes->toSql();
		//echo "<pre>";print_R($episodes);die;
		return $episodes;
	}

	/**
     * Get all the episodes to add encores to them
     *
     */
	public function getEpisodesForEncores($episode_date='', $schedule_id=0, $selected_date='', $current_user_id, $type=1, $host_id=0, $guest_id=0, $show_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$episodes = $this;
		/*if(!empty($channel_id)){
			$episodes = $episodes->where('episodes.channels_id', '=', $channel_id);
		}*/
		if(!empty($host_id)){
			$episodes = $episodes->where('EpisodesHasHost.hosts_id', '=', $host_id);
		}if(!empty($guest_id)){
			$episodes = $episodes->where('EpisodesHasGuest.guests_id', '=', $guest_id);
		}if(!empty($show_id)){
			$episodes = $episodes->where('episodes.shows_id', '=', $show_id);
		}if(!empty($episode_date)){
			$episodes = $episodes->where('episodes.date', 'like', Carbon::parse($episode_date)->format('Y-m-d'));
		}
		
		$episodes = $episodes->select( DB::raw('`episodes`.`date`, `episodes`.`time`, `episodes`.`year`, `Profile`.`lastname`, `Host`.`id`, `Guest`.`id`, ( CONCAT(    DATE_FORMAT(`episodes`.`date`, "%b %e"), "  ", DATE_FORMAT(`episodes`.`time`, "%l:%i %p"))) AS `Episode__fulldate`, (CASE WHEN TRIM(`episodes`.`title`)!= "" THEN (      CASE WHEN TRIM(`episodes`.`title`)!= TRIM(`Show`.`name`) THEN CASE WHEN `episodes`.`is_cohost_override_show_title` > 0 THEN CONCAT(`episodes`.`cohost_show_name`, ": ",`episodes`.`title`) ELSE CONCAT(`Show`.`name`, ": ", `episodes`.`title`) END ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS `Episode__title`, `episodes`.`id` as s_episode_id, archives.encores_number'))
			->leftJoin('archives', 'archives.episode_id', '=', 'episodes.id')
			->leftJoin('episodes_has_hosts AS EpisodesHasHost', 'EpisodesHasHost.episodes_id', '=', 'episodes.id')
			->leftJoin('hosts AS Host', 'Host.id', '=', 'EpisodesHasHost.hosts_id')
			->leftJoin('episodes_has_guests AS EpisodesHasGuest', 'EpisodesHasGuest.episodes_id', '=', 'episodes.id')
			->leftJoin('guests AS Guest', 'Guest.id', '=', 'EpisodesHasGuest.guests_id')
			->leftJoin('profiles AS Profile', 'Host.users_id', '=', 'Profile.users_id')
			->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
			->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
			->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id');
		
		// Get the encores
		//;	
		$episodes = $episodes->where(function($q){
    							$q->where('episodes.date', '<', date('Y-m-d'));
    							$q->orWhere(function($q1){
    								$q1->where('episodes.date', '=', date('Y-m-d'))
    								   ->where('episodes.time','<=', date('H:i:00'));
    								});
    						   });

		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
									->orWhere('episodes.cohost_show_name' ,'like', '%'.$search_value.'%')
    								->orWhere('Show.name' ,'like', '%'.$search_value.'%')
    								->orWhere('Profile.firstname' ,'like', '%'.$search_value.'%')
									->orWhere('Profile.lastname' ,'like', '%'.$search_value.'%')
									->orWhere('Profile.title' ,'like', '%'.$search_value.'%')
									->orWhere('Profile.sufix' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }

        $episodes = $episodes->where('episodes.is_encore', '0')->groupBy('episodes.id');
        
        // Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'DESC')
								->orderBy('episodes.time', 'DESC');
        } 
		$episodes = $episodes->offset($start)->limit($length)->get();
		//$episodes = $episodes->toSql();
		//echo "<pre>";print_R($episodes);die;
		//echo $episodes;die;
		return $episodes;
	}

	/**
     * Move All User Images From Tmp Location to permananet folder
     *
     */
    public function procesImage($object, $module){
    	
        $folders = ['cover', 'segment1', 'segment2', 'segment3', 'segment4', 'media'];
        $userid = Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }           
        return 'success';
    }

    /**
     * Move All User Images From Tmp Location to permananet folder
     *
     */
    public function procesEncoreImage($object, $module){
    	
        $folders = ['cover', 'segment1', 'segment2', 'segment3', 'segment4', 'media'];
        $userid = Auth::user()->id;
        foreach($folders as $folder){
            $old_path = $module.'/'.$object->episodes_id.'/'.$folder;
            //check if at tmp location folder and file exist
            $old_exists = Storage::disk('public')->has($old_path);
            if($old_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($old_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }           
        return 'success';
    }

    /**
     * Get count of all the episodes of the sponsor
     *
     */
    public function countAllSponsorEpisodes( $sponsor_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

    	$episodes = $this;
    	$episodes = $episodes->select( DB::raw('episodes.id as s_episode_id, `episodes`.`channels_id`, `episodes`.`shows_id`, `episodes`.`schedules_id`, `episodes`.`episodes_id`, `episodes`.`created_at`, `episodes`.`updated_at`, `episodes`.`year`, `episodes`.`month`, `episodes`.`date`, `episodes`.`time`, `episodes`.`title_soundex`, `episodes`.`description`, `episodes`.`is_encore`, `episodes`.`is_online`, `episodes`.`is_featured`, `episodes`.`is_itunes`, `episodes`.`notes`, `episodes`.`talking_points`, `episodes`.`segment1`, `episodes`.`segment1_title`, `episodes`.`segment1_video_url`, `episodes`.`segment2`, `episodes`.`segment2_title`, `episodes`.`segment2_video_url`, `episodes`.`segment3`, `episodes`.`segment3_title`, `episodes`.`segment3_video_url`, `episodes`.`segment4`, `episodes`.`segment4_title`, `episodes`.`segment4_video_url`, `episodes`.`events_promote`, `episodes`.`special_offers`, `episodes`.`listener_interaction`, `episodes`.`giveaways`, `episodes`.`internal_host_notes`, `episodes`.`type`, `episodes`.`assigned_type`, `episodes`.`file`, `episodes`.`stream`, `episodes`.`encores_number`, `episodes`.`downloads`, `episodes`.`episodescol`, `episodes`.`is_cohost_override_banner`, `episodes`.`is_cohost_override_show_title`, `episodes`.`cohost_show_id`, `episodes`.`cohost_show_name`, `episodes`.`favorites`, `episodes`.`is_uploaded_by_host`, `episodes`.`video_url`, (CONCAT( DATE_FORMAT(`episodes`.`date`, "%b %e"), "  ", DATE_FORMAT(`episodes`.`time`, "%l:%i %p") )) AS  `Episode__fulldate`, (`episodes`.`title`) AS  `Episode__title_only`, (CASE WHEN TRIM(`episodes`.`title`)!="" THEN (CASE WHEN TRIM(`episodes`.`title`)!=TRIM(`Show`.`name`) THEN CASE WHEN `episodes`.`is_cohost_override_show_title`>0 THEN CONCAT( `episodes`.`cohost_show_name`, ": ", `episodes`.`title`) ELSE CONCAT( `Show`.`name`, ": ", `episodes`.`title`) END  ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS  `Episode__title`, `Schedule`.`id`, `Schedule`.`channels_id`, `Schedule`.`shows_id`, `Schedule`.`schedules_id`, `Schedule`.`deleted`, `Schedule`.`dow`, `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`time`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`file`, `Schedule`.`stream`, `Schedule`.`type_label`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`, `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`, `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`, `Show`.`favorites`, `Show`.`is_created_by_host`, `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name`'))
    			->leftJoin('episodes_has_sponsors AS EpisodesHasSponsor', 'EpisodesHasSponsor.episodes_id', '=', 'episodes.id')
    			->leftJoin('sponsors AS Sponsor', 'Sponsor.id', '=', 'EpisodesHasSponsor.sponsors_id')
    			->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
    			->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
    			->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id');
    						
    	if($sponsor_id > 0){
			$episodes = $episodes->where('Sponsor.id', $sponsor_id);
		}
		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'DESC')
								->orderBy('episodes.time', 'DESC');
        } 
		$episodes = $episodes->count();
		return $episodes;
    }

    /**
     * Get all the episodes of the sponsor
     *
     */
    public function getAllSponsorEpisodes( $sponsor_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
    	$episodes = $this;
    	$episodes = $episodes->select( DB::raw('episodes.id as s_episode_id, `episodes`.`channels_id`, `episodes`.`shows_id`, `episodes`.`schedules_id`, `episodes`.`episodes_id`, `episodes`.`created_at`, `episodes`.`updated_at`, `episodes`.`year`, `episodes`.`month`, `episodes`.`date`, `episodes`.`time`, `episodes`.`title_soundex`, `episodes`.`description`, `episodes`.`is_encore`, `episodes`.`is_online`, `episodes`.`is_featured`, `episodes`.`is_itunes`, `episodes`.`notes`, `episodes`.`talking_points`, `episodes`.`segment1`, `episodes`.`segment1_title`, `episodes`.`segment1_video_url`, `episodes`.`segment2`, `episodes`.`segment2_title`, `episodes`.`segment2_video_url`, `episodes`.`segment3`, `episodes`.`segment3_title`, `episodes`.`segment3_video_url`, `episodes`.`segment4`, `episodes`.`segment4_title`, `episodes`.`segment4_video_url`, `episodes`.`events_promote`, `episodes`.`special_offers`, `episodes`.`listener_interaction`, `episodes`.`giveaways`, `episodes`.`internal_host_notes`, `episodes`.`type`, `episodes`.`assigned_type`, `episodes`.`file`, `episodes`.`stream`, `episodes`.`encores_number`, `episodes`.`downloads`, `episodes`.`episodescol`, `episodes`.`is_cohost_override_banner`, `episodes`.`is_cohost_override_show_title`, `episodes`.`cohost_show_id`, `episodes`.`cohost_show_name`, `episodes`.`favorites`, `episodes`.`is_uploaded_by_host`, `episodes`.`video_url`, (CONCAT( DATE_FORMAT(`episodes`.`date`, "%b %e"), "  ", DATE_FORMAT(`episodes`.`time`, "%l:%i %p") )) AS  `Episode__fulldate`, (`episodes`.`title`) AS  `Episode__title_only`, (CASE WHEN TRIM(`episodes`.`title`)!="" THEN (CASE WHEN TRIM(`episodes`.`title`)!=TRIM(`Show`.`name`) THEN CASE WHEN `episodes`.`is_cohost_override_show_title`>0 THEN CONCAT( `episodes`.`cohost_show_name`, ": ", `episodes`.`title`) ELSE CONCAT( `Show`.`name`, ": ", `episodes`.`title`) END  ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS  `Episode__title`, `Schedule`.`id`, `Schedule`.`channels_id`, `Schedule`.`shows_id`, `Schedule`.`schedules_id`, `Schedule`.`deleted`, `Schedule`.`dow`, `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`time`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`file`, `Schedule`.`stream`, `Schedule`.`type_label`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`, `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`, `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`, `Show`.`favorites`, `Show`.`is_created_by_host`, `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name`'))
    			->leftJoin('episodes_has_sponsors AS EpisodesHasSponsor', 'EpisodesHasSponsor.episodes_id', '=', 'episodes.id')
    			->leftJoin('sponsors AS Sponsor', 'Sponsor.id', '=', 'EpisodesHasSponsor.sponsors_id')
    			->leftJoin('schedules AS Schedule', 'episodes.schedules_id', '=', 'Schedule.id')
    			->leftJoin('shows AS Show', 'episodes.shows_id', '=', 'Show.id')
    			->leftJoin('channels AS Channel', 'episodes.channels_id', '=', 'Channel.id');
    	
    	$episodes = $episodes->with('guests')->with('hosts.user.profiles');

		if($sponsor_id > 0){
			$episodes = $episodes->where('Sponsor.id', $sponsor_id);
		}
		// Overall Search 
        if(!empty($search_value)){
            $episodes = $episodes->where(function($q) use ($search_value){
    							$q->orWhere('episodes.title' ,'like', '%'.$search_value.'%')
    								->orWhere('episodes.date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $episodes = $episodes->orderBy($order, $dir);
        }else{
            $episodes = $episodes->orderBy('episodes.date', 'DESC')
								->orderBy('episodes.time', 'DESC');
        } 
		$episodes = $episodes->offset($start)->limit($length)->get();
		//echo "<pre>";print_R($episodes);die;
		return $episodes;
    }

    public function editScheduledShare($id=0, $data){
		if($id != 0){
			if(!empty($data)){
				try{
					$date ='';
					$desc ='';
					$is_twitter ='';
					$is_facebook ='';
					if(isset($data['EpisodeScheduledShare'])){
						if(!empty($data['EpisodeScheduledShare'])){
							if(!empty($data['EpisodeScheduledShare']['time'])){
								$selected_date =$data['EpisodeScheduledShare']['time']['year'].'-'.$data['EpisodeScheduledShare']['time']['month'].'-'.$data['EpisodeScheduledShare']['time']['day'].' '. $data['EpisodeScheduledShare']['time']['hour'].':'.$data['EpisodeScheduledShare']['time']['min'].':00 '.$data['EpisodeScheduledShare']['time']['meridian'];
		           				$date_string = strtotime($selected_date);
		           				$date =date('Y-m-d H:i:s',$date_string);
	           					//echo"$date";die();
							}
							if(!empty($data['EpisodeScheduledShare']['desc'])){
								$desc = $data['EpisodeScheduledShare']['desc'];
							}
							if(!empty($data['EpisodeScheduledShare']['is_twitter'])){
								$is_twitter = $data['EpisodeScheduledShare']['is_twitter'];
							}
							$data_array = array('users_id'=>\Auth::user()->id,'time' =>$date,'episodes_id'=>$id,'is_twitter'=>$is_twitter,'is_facebook'=>$is_facebook ,'desc'=>$desc);
							$episode = EpisodeScheduledShare::where('episodes_id',$id)->first();
							if(is_null($episode)){
								$episode_obj = new EpisodeScheduledShare($data_array);
								$episode_obj->save();
							}else{
								$episode->update($data_array);
							}
							// echo"<pre>";print_r($episode); die();
							// $episode->update($data_array);
						}
					}
				}catch(\Exception $e){
					/*echo $e->getMessage();
					die();*/
					return false;
		   		}
				return true;
			}
		}else{
			return false;
		}
    }

    public function getEpisodeCountByDateTimeShow($date, $time, $shows_id){
    	
		$schedules = $this->leftJoin('schedules', 'episodes.schedules_id', '=', 'schedules.id')
			->leftJoin('shows', 'episodes.shows_id', '=', 'shows.id')
			->leftJoin('channels', 'episodes.channels_id', '=', 'channels.id')
                ->where('episodes.date', $date)
                ->where('episodes.time', $time)
                ->where('episodes.shows_id',  $shows_id)
                ->count();
       
        return $schedules;
    }

    /**
     * Get the facebook data to share on scheduled time 
     */
    public function getDataToShare($type, $social_media, $social_media_time, $check_column){
    	/*echo $type."<br>";
    	echo $social_media."<br>";
    	echo $check_column."<br>";
    	*/

    	$date = now();
    	$compare_date = date_format($date,"Y-m-d H:i").':00';
    	//echo $compare_date;
		$get_fb_data = $this->select('episodes.*', 'media.filename', 'media.type', 'media.id AS media_id')->with('episode_scheduled_shares')->whereHas('episode_scheduled_shares', function($query) use ($type, $compare_date, $social_media, $social_media_time, $check_column)
				{
				    $query->where($social_media, 1)
				    	  ->where('type', $type)
				    	  ->where($social_media_time ,'>=', $compare_date)
				    	  ->where($check_column, 0)
				    	  ->whereNotNull('episodes_id');
				})
				->leftJoin("media_linking",function($join){
		            $join->on("media_linking.module_id","=","episodes.id")
		                ->where("media_linking.main_module","=","Episode");
		        })->leftJoin('media', 'media.id','=', 'media_linking.media_id')
		        ->groupBy('episodes.id')
				->get();
				//->toSql();

    	//echo "<pre>";print_R($get_fb_data);die;
    	return $get_fb_data;
    }

    /**
     * Update the schedule which is successfully shared
     * @param $episode_scheduled_id is the episode's scheduled share id
     * @param $update_column is the social media to update that the post is shared on the   * social media
     */
    public function updateSuccessfullySharedSchedule($episode_scheduled_id, $update_column){

    	if($episode_scheduled_id != '' && $episode_scheduled_id !=0){
    		$episode_share = EpisodeScheduledShare::find($episode_scheduled_id);
    		$episode_share->{$update_column} = 1;
    		$update = $episode_share->update();  		    		
    		return true;
    	}
    }
}
