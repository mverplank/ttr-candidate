<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Theme
 * 
 * @property int $id
 * @property int $themes_id
 * @property string $name
 * @property string $type
 * @property bool $is_active
 * @property array $definition_json
 * @property array $data_json
 *
 * @package App\Models
 */
class Theme extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'themes_id' => 'int',
		'is_active' => 'bool',
		'definition_json' => 'json',
		//'data_json' => 'array'
	];

	protected $fillable = [
		'themes_id',
		'name',
		'type',
		'is_active',
		'definition_json',
		'data_json'
	];

	/**
	 * Fetch the content data acc. to the content category id
	 * @param $content_category_id is the category id of the content
	 * @param $order_field is the column for the sorting
	 * @param $order_direction is the sorting order
	 */
	public function fetchThemes($content_category_id='', $order_field, $order_direction, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$get = $this;

		$get = $get->leftJoin('channels','channels.theme_name', '=', 'themes.name')
		->select('themes.id AS t_id','themes.name AS theme_name', 'channels.name AS channel_name');
		//echo"<pre>";print_r($get);die;
		// Overall Search 
        if(!empty($search_value)){
            $get = 	$get->where(function($q) use ($search_value){
    					$q->orWhere('themes.name' ,'like', '%'.$search_value.'%');
    				});
        }
		$get = $get->orderBy($order_field, $order_direction);
		$get = $get->offset($start)->limit($length)->get();
		//dump($get);
		return $get;
	}

	/**
	 * Fetch the content data acc. to the content category id
	 * @param $content_category_id is the category id of the content
	 * @param $order_field is the column for the sorting
	 * @param $order_direction is the sorting order
	 */
	public function countThemes($content_category_id='', $order_field, $order_direction, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		$get = $this;
		// Overall Search 
        if(!empty($search_value)){
            $get = $get->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%');
    						});
        }

		$get = $get->orderBy($order_field, $order_direction);
		$get = $get->count();
		return $get;
	}


	/**
	* Add Theme 
	* @param $data is the request object
	* @return boolean
	*/
	public function add($data){		
		try{			
			if(isset($data['CustomTheme'])){
				//echo"<pre>";print_r($data['CustomTheme']);die;
				$page = new Theme($data['CustomTheme']);
				$page->save();
			}
			return true;
		}catch(\Exception $e){
			echo $e->getMessage();die();
			return false;
	    }
	}

	/**
	* update Theme 
	* @param $data is the request object
	* @return boolean
	*/
	public function edit($id, $data){		
		try{			
			if(isset($data['CustomTheme']['data_json'])){
				$data['CustomTheme']['data_json'] = json_encode($data['CustomTheme']['data_json'],true);
				$theme =Theme::where('id', '=', $id)->first();
				$theme->update($data['CustomTheme']);
			}
			return true;
		}catch(\Exception $e){
			echo $e->getMessage();die();
			return false;
	    }
	}



}
