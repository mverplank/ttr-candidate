<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Video
 * 
 * @property int $id
 * @property int $hosts_id
 * @property int $guests_id
 * @property string $off
 * @property string $title
 * @property string $code
 * @property bool $is_enabled
 * 
 * @property \App\Models\Host $host
 * @property \App\Models\Guest $guest
 *
 * @package App\Models\Backend
 */
class Video extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'guests_id' => 'int',
		'is_enabled' => 'bool'
	];

	protected $fillable = [
		'hosts_id',
		'guests_id',
		'off',
		'title',
		'code',
		'is_enabled'
	];

	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}

	public function guest()
	{
		return $this->belongsTo(\App\Models\Backend\Guest::class, 'guests_id');
	}
}
