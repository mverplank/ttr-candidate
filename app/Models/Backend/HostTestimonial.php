<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HostTestimonial
 * 
 * @property int $id
 * @property int $hosts_id
 * @property string $testimonial
 * @property string $author
 * @property int $off
 * @property bool $is_enabled
 * 
 * @property \App\Models\Host $host
 *
 * @package App\Models
 */
class HostTestimonial extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'off' => 'int',
		'is_enabled' => 'bool'
	];

	protected $fillable = [
		'hosts_id',
		'testimonial',
		'author',
		'off',
		'is_enabled'
	];

	public function host()
	{
		return $this->belongsTo(\App\Models\Host::class, 'hosts_id');
	}
}
