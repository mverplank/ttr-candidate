<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 March 2018 04:55:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodesInterval
 * 
 * @property int $episodes_id
 * @property int $tags_id
 * 
 * @property \App\Models\Backend\Episode $episode
 * @property \App\Models\Backend\Users   $user
 *
 * @package App\Models\Backend
 */
class EpisodesInterval extends Eloquent
{
	public $timestamps = true;
	protected $table = 'episodes_interval';
	protected $casts = [
		'episodes_id' => 'int',
		'users_id' => 'int'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}

	public function users()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	public function addToInterval($data){
		if(!empty($data)){

			$interval_obj = $this->where('episodes_id', $data['episodes_id'])->first();
			
			if($interval_obj){
				$interval_obj->entry = $data['entry'];
				$interval_obj->update();
				
			}else{
				$this->episodes_id = $data['episodes_id'];
				$this->users_id    = $data['users_id'];
				$this->entry       = $data['entry'];
				$this->exit        = '0000-00-00 00:00:00';
				$this->save();
			}		
		}		
		return true;
	}

	public function deleteInterval($id=''){
		if( $id !='' && $id !=0 ){
			$this->where('episodes_id', $id)->delete();
		}
	}
}
