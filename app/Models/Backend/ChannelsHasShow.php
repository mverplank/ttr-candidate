<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChannelsHasShow
 * 
 * @property int $channels_id
 * @property int $shows_id
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Show $show
 *
 * @package App\Models
 */
class ChannelsHasShow extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int',
		'shows_id' => 'int'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class, 'channels_id');
	}

	public function show()
	{
		return $this->belongsTo(\App\Models\Show::class, 'shows_id');
	}
}
