<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CohostType
 * 
 * @property int $id
 * @property string $name
 * @property bool $deleted
 * @property bool $option_override_host_banners
 * 
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 *
 * @package App\Models\Backend
 */
class CohostType extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'deleted' => 'bool',
		'option_override_host_banners' => 'bool'
	];

	protected $fillable = [
		'name',
		'deleted',
		'option_override_host_banners'
	];

	public function hosts()
	{
		return $this->hasMany(\App\Models\Backend\Host::class, 'cohost_types_id');
	}
}
