<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Url
 * 
 * @property int $id
 * @property int $hosts_id
 * @property int $guests_id
 * @property string $url
 * @property string $label
 * @property string $type
 * 
 * @property \App\Models\Guest $guest
 * @property \App\Models\Host $host
 *
 * @package App\Models\Backend
 */
class Url extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'hosts_id' => 'int',
		'guests_id' => 'int'
	];

	protected $fillable = [
		'hosts_id',
		'guests_id',
		'url',
		'label',
		'type'
	];

	public function guest()
	{
		return $this->belongsTo(\App\Models\Backend\Guest::class, 'guests_id');
	}

	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}
}
