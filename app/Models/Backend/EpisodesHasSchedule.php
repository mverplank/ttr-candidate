<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodesHasSchedule
 * 
 * @property int $id
 * @property int $episodes_id
 * @property int $schedules_id
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $time
 * 
 * @property \App\Models\Episode $episode
 * @property \App\Models\Schedule $schedule
 *
 * @package App\Models
 */
class EpisodesHasSchedule extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'schedules_id' => 'int'
	];

	protected $dates = [
		'date',
		'time'
	];

	protected $fillable = [
		'date',
		'time'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Episode::class, 'episodes_id');
	}

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Schedule::class, 'schedules_id');
	}
}
