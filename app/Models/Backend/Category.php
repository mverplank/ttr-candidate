<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $id
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property string $name
 * @property string $type
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banners_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $episodes_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $guests_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $shows_has_categories
 *
 * @package App\Models\Backend
 */
class Category extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent_id' => 'int',
		'lft' => 'int',
		'rght' => 'int'
	];

	protected $fillable = [
		'parent_id',
		'lft',
		'rght',
		'name',
		'type'
	];

	public function banners_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\BannersHasCategory::class, 'categories_id');
	}

	public function episodes_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\EpisodesHasCategory::class, 'categories_id');
	}

	public function guests_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\GuestsHasCategory::class, 'categories_id');
	}

	public function shows_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\ShowsHasCategory::class, 'categories_id');
	}

	/**
	* @description Add the Category
	* @param 
	*/
	/*public function add($data){
		echo "<pre>";print_R($data);die;

		//$banner_categories = new Category();
        $banner_categories->parent_id =null;
        $banner_categories->lft  = null;
        $banner_categories->rght = null;
        $banner_categories->name = $request->name;
        $banner_categories->type = 'banner';
        $addCate = $banner_categories->save();       
        if($addCate){
           return response()->json(['seccess'=>true,'message'=>"Add successfully"]);
        }          
	}*/


	/**
	* @description Count of all categories
	* @param category id
	*/
	public function countCategories($type='', $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$categories = new Category();

        // Overall Search 
        if(!empty($search_value)){
            $categories = $categories->where('name' ,'like', '%'.$search_value.'%');
        }

        // Category type
        if($type != null){
            $categories = $categories->where('type', $type);
        }

        // Sorting by column
        if($order != null){
            $categories = $categories->orderBy($order, $dir);
        }else{
            $categories = $categories->orderBy('name', 'asc');
        } 
        $categories = $categories->count();
        return $categories;
	}

	/**
	* @description Get all categories
	* @param category id
	*/
	public function getCategories($type='', $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$categories = new Category();

		// Overall Search 
        if(!empty($search_value)){
            $categories = $categories->where('name' ,'like', '%'.$search_value.'%');
        }

        // Category type
        if(!empty($type)){
            $categories = $categories->where('type', $type);
        }
        // Sorting by column
        if($order != null){
            $categories = $categories->orderBy($order, $dir);
        }else{
            $categories = $categories->orderBy('name', 'asc');
        } 
        
        $categories = $categories->offset($start)->limit($length)->get();
        return $categories;
	}

	/**
	* @description Delete category
	* @param category id
	*/
	public function deleteCategory($id){

		if($id != '' || $id !=0){
			$category = Category::find($id);
			if ($category->has('banners_has_categories')) {
			   $category->banners_has_categories()->delete();
			}
			if ($category->has('episodes_has_categories')) {
			   $category->episodes_has_categories()->delete();
			}
			if ($category->has('guests_has_categories')) {
			   $category->guests_has_categories()->delete();
			}
			if ($category->has('shows_has_categories')) {
			   $category->shows_has_categories()->delete();
			}	

			return $category->delete();
		}
	}

	// Fetch categories of the particular type
 	public function getAllCategory($type=''){
 		$category_obj = $this->select('id', 'name');
 		if(!empty($type)){
 			$category_obj = $category_obj->where('type', $type);
 		}
		$category_obj = $category_obj->orderBy('name', 'asc')->pluck('name', 'id');
		return $category_obj;
	}

	/**
	* @description - Get the Category  list for the filters
	* @param $type - category type
	* @param $select_options - output in the form of select box
	* @param $selected_cat - show already selected the option used only when the second paramter is true
	* @param $child - if also want to fetch child catgeories then set true otherwise it will only fetch the main categories
	*/
	public function getCategoriesForFilter($type="", $select_options=false, $selected_cat=0, $child=false){
		if($select_options){
			$selectCategoryTree = "";
		}
		
		$category_obj = Category::select('id', 'name')->whereNull('parent_id');
		if(!empty($type)){
			$category_obj = $category_obj->where('type', $type);
		}
		$arr = array();
		$category_obj = $category_obj->orderBy('name', 'asc')->pluck('name', 'id');
		if($category_obj->count() > 0){
			
			// Fetch parent category
			foreach ($category_obj as $key => $main_cat) {
				$arr[$key]['name'] = $main_cat;
				$get_subcategories = $this->getSubcategoryFromMainCatgeoryId($key, $select_options, $child);
				
				if(!empty($get_subcategories)){
					if($select_options){
						$selectCategoryTree .= "<select name='data[Archive][f][categories_ids]' class='selectpicker m-b-0', data-selected-text-format='count', data-style='btn-primary' id='ArchiveFCategoriesIds'>
								<option value=''>All Categories</option>";
						foreach($get_subcategories as $ck=>$cat){	
							$level = 1;
							if($selected_cat == $key){
								$selected = "selected";
							}else{
								$selected = '';
							}

							if(!empty($cat['children'])){
								$level = 2;
								$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat['name'].'</option>';						
								$selectCategoryTree .= $this->makeCategoryTree($cat, $ck, $level, $selected_cat);
							}else{
								if(!$child){
									$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat.'</option>';
								}else{
									$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat['name'].'</option>';
								}								
							}
						}	
						$selectCategoryTree .= "</select>";
						return $selectCategoryTree;
					}else{
						return $get_subcategories;
					}	
				}
			}
		}
		return $category_obj;
	}

	/**
	* @description - Get the Category  list for the filters
	* @param $type - category type
	* @param $select_options - output in the form of select box
	* @param $selected_cat - show already selected the option used only when the second paramter is true
	* @param $child - if also want to fetch child catgeories then set true otherwise it will only fetch the main categories
	*/
	public function getContentCategories($type="", $select_options=false, $selected_cat=0, $child=false){
		if($select_options){
			$selectCategoryTree = "";
		}
		
		$category_obj = Category::select('id', 'name')->whereNull('parent_id');
		if(!empty($type)){
			$category_obj = $category_obj->where('type', $type);
		}
		$arr = array();
		$category_obj = $category_obj->orderBy('name', 'asc')->pluck('name', 'id');
		if($category_obj->count() > 0){
			
			// Fetch parent category
			foreach ($category_obj as $key => $main_cat) {
				$arr[$key]['name'] = $main_cat;
				$get_subcategories = $this->getSubcategoryFromMainCatgeoryId($key, $select_options, $child);
				
				if(!empty($get_subcategories)){
					if($select_options){
						$selectCategoryTree .= "<select name='data[ContentData][has_category]' class='selectpicker m-b-0', data-selected-text-format='count', data-style='btn-primary' id=''>
								<option value=''>All Categories</option>";
						foreach($get_subcategories as $ck=>$cat){	
							$level = 1;
							if($selected_cat == $key){
								$selected = "selected";
							}else{
								$selected = '';
							}

							if(!empty($cat['children'])){
								$level = 2;
								$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat['name'].'</option>';						
								$selectCategoryTree .= $this->makeCategoryTree($cat, $ck, $level, $selected_cat);
							}else{
								if(!$child){
									$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat.'</option>';
								}else{
									$selectCategoryTree .= '<option class="optionGroup" value='.$ck.' '.$selected.'>'.$cat['name'].'</option>';
								}								
							}
						}	
						$selectCategoryTree .= "</select>";
						return $selectCategoryTree;
					}else{
						return $get_subcategories;
					}	
				}
			}
		}
		return $category_obj;
	}
	
	// Fetch Subcategory from the parent catgeory id
	public function getSubcategoryFromMainCatgeoryId($id, $make_tree=false, $child=false){
		$subcategory_obj = Category::select('id', 'name')->where('parent_id', $id)->orderBy('name', 'asc')->pluck('name', 'id');
		$children = array();
		
		if($subcategory_obj->count() > 0){			
			foreach($subcategory_obj as $subk=>$sub_cat){				
	            # Add the child to the list of children, and get its subchildren	            
	            if(!$child){
	            	
	            	$children[$subk] = $sub_cat;
	            	$this->getSubcategoryFromMainCatgeoryId($subk, $make_tree, $child);
	            }else{
	            	
	            	$children[$subk]['name'] = $sub_cat;
	            	$children[$subk]['children'] = $this->getSubcategoryFromMainCatgeoryId($subk, $make_tree, $child);
	            }
	        }
	    }		
	    return $children;
	}

	// Make category tree with the select options
	public function makeCategoryTree($data, $category_id, $level, $selected_cat=0){
		if(!empty($data)){
			$selectCategoryTree = '';
			$padding = 0;
			$level++;
			
			if(!empty($data['children'])){
				foreach ($data['children'] as $key => $value) {					
					if($selected_cat == $key){
						$selected = "selected";
					}else{
						$selected = '';
					}
					$padding = $level*15;

					$selectCategoryTree .= '<option style="padding-left:'.$padding.'px;" value='.$key.' '.$selected.'>'.$value['name'].'</option>';
					
					$selectCategoryTree .= $this->makeCategoryTree($value, $key, $level, $selected_cat);					
					
				}				
			}else{
				
				if($selected_cat == $category_id){
					$selected = "selected";
				}else{
					$selected = '';
				}
				if($level > 2){
					$level--;
				}				
			}			
		}
		return $selectCategoryTree;
	}

	public function searchCat($parent_id, $type){
	
		$category = Category::select('id','name');
		$category = $category->where(function ($query) use ($parent_id,$type) {
	                    $query->Where('parent_id', '=', $parent_id)
	                          ->Where('type', '=',$type);
	                })->get();
	                /*})->toSql();
		echo $category;die;*/
		return $category;
	}

	/** 
	* Get the categories according to the type of the category
    * @param - $type is the category type
    * @return all the categories
	*/
	public function getFlatArrayOfCategoriesByType($type=''){
		$flat_array = array();
		if(!empty($type)){
			$fetch_categories = $this->where('type', $type)->orderBy('name', 'asc')->get();
			if($fetch_categories->count() > 0){
				$flat_array = $fetch_categories->toArray();
			}
		}
		return $flat_array;
	}
}
