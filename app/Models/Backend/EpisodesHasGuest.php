<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\Backend\Episode;
/**
 * Class EpisodesHasGuest
 * 
 * @property int $id
 * @property int $episodes_id
 * @property int $guests_id
 * @property int $off
 * 
 * @property \App\Models\Episode $episode
 * @property \App\Models\Guest $guest
 *
 * @package App\Models
 */
class EpisodesHasGuest extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'guests_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'episodes_id' => 'int',
		'guests_id' => 'int',
		'off' => 'int'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}

	public function guest()
	{
		return $this->belongsTo(\App\Models\Backend\Guest::class, 'guests_id');
	}

	/**
    * @description Get the guest of the episode
    * @param $episode here is the id of the episode,)
    * @return the options of the select box
    */
    public function getGuestsByType($episode_id){
		
    	$make_guests = array();
		$episode = $this->with('guest')->where('episodes_id', $episode_id)->get();
        //echo"<pre>";print_r($episode);die;
		if($episode->count() > 0 ){
			foreach($episode as $key=>$guest){
                $make_guests[$key]['id'] = $guest->guest->id;
                $make_guests[$key]['text']=$guest->guest->name;
            }
        }
        return $make_guests;
    }


}
