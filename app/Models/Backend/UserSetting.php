<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserSetting
 * 
 * @property int $id
 * @property int $users_id
 * @property int $memberships_id
 * @property array $membership_data_json
 * @property \Carbon\Carbon $membership_end
 * @property string $theme
 * @property string $lang
 * @property bool $mailing_newsletter
 * @property int $mailing_bounced
 * @property string $payment_data
 * @property float $payment_balance
 * @property bool $studioapp_enabled
 * @property array $studioapp_settings_json
 * @property array $socialmedia_tokens_json
 * @property array $playerapp_data_json
 * @property \Carbon\Carbon $playerapp_last_sync
 * 
 * @property \App\Models\Membership $membership
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserSetting extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int',
		'memberships_id' => 'int',
		'membership_data_json' => 'object',
		'mailing_newsletter' => 'bool',
		'mailing_bounced' => 'int',
		'payment_balance' => 'float',
		'studioapp_enabled' => 'bool',
		'studioapp_settings_json' => 'object',
		'socialmedia_tokens_json' => 'object',
		'playerapp_data_json' => 'object'
	];

	protected $dates = [
		'membership_end',
		'playerapp_last_sync'
	];

	protected $fillable = [
		'users_id',
		'memberships_id',
		'membership_data_json',
		'membership_end',
		'theme',
		'lang',
		'mailing_newsletter',
		'mailing_bounced',
		'payment_data',
		'payment_balance',
		'studioapp_enabled',
		'studioapp_settings_json',
		'socialmedia_tokens_json',
		'playerapp_data_json',
		'playerapp_last_sync'
	];

	public function membership()
	{
		return $this->belongsTo(\App\Models\Backend\Membership::class, 'memberships_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	// public function add($data)
	// {
	// 		if(!empty($data)){
	// 			$this->users_id = $data['user_id'];
	// 		}
	// }
}
