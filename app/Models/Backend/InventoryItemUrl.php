<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InventoryItemUrl
 * 
 * @property int $id
 * @property int $inventory_items_id
 * @property string $url
 * @property string $description
 * 
 * @property \App\Models\InventoryItem $inventory_item
 *
 * @package App\Models\Backend
 */
class InventoryItemUrl extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'inventory_items_id' => 'int'
	];

	protected $fillable = [
		'url',
		'description'
	];

	public function inventory_item()
	{
		return $this->belongsTo(\App\Models\InventoryItem::class, 'inventory_items_id');
	}
}
