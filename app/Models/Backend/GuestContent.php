<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GuestContent
 * 
 * @property int $id
 * @property int $guests_id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property int $off
 * 
 * @property \App\Models\Guest $guest
 *
 * @package App\Models
 */
class GuestContent extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'guests_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'guests_id',
		'title',
		'description',
		'url',
		'off'
	];

	public function guest()
	{
		return $this->belongsTo(\App\Models\Guest::class, 'guests_id');
	}
}
