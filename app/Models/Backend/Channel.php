<?php

/**
 * @author : Contriverz
 * @since  : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Channel
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $deleted
 * @property bool $is_online
 * @property string $domain
 * @property string $theme
 * @property string $stream
 * @property string $url_archived_episodes
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $sam_dir_shows
 * @property string $sam_file_connecting
 * @property int $sam_min_show_duration
 * @property bool $studioapp_enabled
 * @property array $studioapp_settings_json
 * @property bool $is_missing_episodes_reminder
 * @property string $theme_name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banners
 * @property \Illuminate\Database\Eloquent\Collection $channels_has_content_categories
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $guests
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 * @property \Illuminate\Database\Eloquent\Collection $shows
 * @property \Illuminate\Database\Eloquent\Collection $sponsors
 * @property \Illuminate\Database\Eloquent\Collection $chnnel_itunes
 * @property \Illuminate\Database\Eloquent\Collection $content_data
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 *
 * @package App\Models\Backend
 */
class Channel extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'deleted' => 'int',
		'is_online' => 'bool',
		'sam_min_show_duration' => 'int',
		'studioapp_enabled' => 'bool',
		'studioapp_settings_json' => 'object',
		'is_missing_episodes_reminder' => 'bool'
	];

	protected $fillable = [
		'name',
		'description',
		'deleted',
		'is_online',
		'domain',
		'theme',
		'stream',
		'url_archived_episodes',
		'seo_title',
		'seo_keywords',
		'seo_description',
		'sam_dir_shows',
		'sam_file_connecting',
		'sam_min_show_duration',
		'studioapp_enabled',
		'studioapp_settings_json',
		'is_missing_episodes_reminder',
		'theme_name'
	];

	public function banners()
	{
		return $this->belongsToMany(\App\Models\Backend\Banner::class, 'banners_has_channels', 'channels_id', 'banners_id');
	}

	public function channels_has_content_categories()
	{
		return $this->hasMany(\App\Models\Backend\ChannelsHasContentCategory::class, 'channels_id');
	}

	public function episodes()
	{
		return $this->hasMany(\App\Models\Backend\Episode::class, 'channels_id');
	}

	public function guests()
	{
		return $this->belongsToMany(\App\Models\Backend\Guest::class, 'channels_has_guests', 'channels_id', 'guests_id');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'channels_has_hosts', 'channels_id', 'hosts_id');
	}

	public function shows()
	{
		return $this->belongsToMany(\App\Models\Backend\Show::class, 'channels_has_shows', 'channels_id', 'shows_id');
	}

	public function sponsors()
	{
		return $this->belongsToMany(\App\Models\Backend\Sponsor::class, 'channels_has_sponsors', 'channels_id', 'sponsors_id');
	}

	public function chnnel_itunes()
	{
		return $this->hasMany(\App\Models\Backend\ChnnelItune::class, 'channels_id');
	}

	public function content_data()
	{
		return $this->hasMany(\App\Models\Backend\ContentDatum::class, 'channels_id');
	}

	public function schedules()
	{
		return $this->hasMany(\App\Models\Backend\Schedule::class, 'channels_id');
	}

	// Fetch all channels with id for the filters
	public function getAllChannels(){
		$channel_obj = Channel::select('id', 'name')->where('deleted', 0)->orderBy('name', 'asc')->pluck('name', 'id');
		return $channel_obj;
	}

	/**
	* Count all the channels
	* @return result in the form of object
	* @param status 1 for online, 0 for offline
	*/
	public function countChannels($order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		$channels = Channel::select('id', 'name', 'description')
							->where('deleted',0);
	   
		// Overall Search 
        if(!empty($search_value)){
            $channels = $channels->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%')
    								->orWhere('description' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $channels = $channels->orderBy($order, $dir);
        }else{
            $channels = $channels->orderBy('id', 'asc');
        } 
		$channels = $channels->count();
		return $channels;
	}

	/**
	* Fetches all the channels
	* @return result in the form of object
	* @param status 1 for online, 0 for offline
	*/
	public function getChannels($start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		$channels = Channel::select('id', 'name', 'description')
							->where('deleted',0);
		
		// Overall Search 
        if(!empty($search_value)){
            $channels = $channels->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%')
    								->orWhere('description' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $channels = $channels->orderBy($order, $dir);
        }else{
            $channels = $channels->orderBy('name', 'asc');
        } 
		$channels = $channels->offset($start)->limit($length)->get();
		return $channels;
	}


	/**
	* Delete the channel
	* @return boolean
	* @param $id is the channel id
	*/
	public function deleteChannel($id){
		
		if($id != '' || $id !=0){
			$channel = Channel::find($id);
			deleteMediaLink('Channel', $id);
			return $channel->update(['deleted'=>1]);			
		}
	}

	/**
     * Fetch the shows of the channel
     * 
     * @param channel id
     * @return all shows of that channel
     */
    public function fetchChannelShows($channel_id =0){
		if($channel_id !=0 ){
			$channel = $this->find($channel_id);	
			
			//Only fetch the shows which are not deleted and having online status		
			$get_shows = $channel->shows()->where(['shows.deleted'=>0, 'shows.is_online'=>1])->orderBy('shows.name', 'asc')->pluck('name', 'id');	
			$make_arr = [];
			$i=0;
			if($get_shows){
				//
				foreach($get_shows as $key=>$show){
					$make_arr[$i]['id'] = $key;
					$make_arr[$i]['name'] = $show;
					$i++;
				}
			}
			return $make_arr;
		}
	}	

	/**
     * Get all the channels having studio app enabled
	 */
	public function getStudioChannels($start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		
		$channels = $this->select('id', 'name');
		// Overall Search 
        if(!empty($search_value)){
            $channels = $channels->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%');
    								//->orWhere('description' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $channels = $channels->orderBy($order, $dir);
        }else{
            $channels = $channels->orderBy('name', 'asc');
        } 

        //Conditions
		$channels = $channels->where('studioapp_enabled', 1)
							->where('deleted', 0);

		$channels = $channels->offset($start)
							->limit($length)
							->get();
		return $channels;
	}

	/**
     * Count all the channels having studio app enabled
	 */
	public function countStudioChannels($order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		$channels = $this;
		// Overall Search 
        if(!empty($search_value)){
            $channels = $channels->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%');
    								//->orWhere('description' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $channels = $channels->orderBy($order, $dir);
        }else{
            $channels = $channels->orderBy('name', 'asc');
        } 
     
		$channels = $channels->where('studioapp_enabled', 1)
							->where('deleted', 0)
							->count();

		return $channels;
	}
}
