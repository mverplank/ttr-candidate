<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChannelsHasSponsor
 * 
 * @property int $channels_id
 * @property int $sponsors_id
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Sponsor $sponsor
 *
 * @package App\Models\Backend
 */
class ChannelsHasSponsor extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int',
		'sponsors_id' => 'int'
	];

	protected $fillable = [
		'channels_id',
		'sponsors_id'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Backend\Channel::class, 'channels_id');
	}

	public function sponsor()
	{
		return $this->belongsTo(\App\Models\Backend\Sponsor::class, 'sponsors_id');
	}

	public function add($new_arr){
		if(!empty($new_arr)){
			$obj = new ChannelsHasSponsor($new_arr);
			$obj->save();
		}
	}
}
