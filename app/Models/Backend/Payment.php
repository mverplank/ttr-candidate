<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property string $model
 * @property int $model_id
 * @property string $method
 * @property string $transaction_id
 * @property \Carbon\Carbon $transaction_date
 * @property string $transaction_status
 * @property float $transaction_quote
 * @property string $transaction_currency
 * @property string $transaction_items
 * @property string $transaction_data
 * @property string $notes
 * @property string $created_by
 * @property string $payer_name
 * @property string $payer_address
 * @property string $payer_email
 * @property string $payer_phone
 *
 * @package App\Models
 */
class Payment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'model_id' => 'int',
		'transaction_quote' => 'float'
	];

	protected $dates = [
		'created',
		'modified',
		'transaction_date'
	];

	protected $fillable = [
		'created',
		'modified',
		'model',
		'model_id',
		'method',
		'transaction_id',
		'transaction_date',
		'transaction_status',
		'transaction_quote',
		'transaction_currency',
		'transaction_items',
		'transaction_data',
		'notes',
		'created_by',
		'payer_name',
		'payer_address',
		'payer_email',
		'payer_phone'
	];
}
