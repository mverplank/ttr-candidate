<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class PodcastHit
 * 
 * @property int $id
 * @property int $podcast_stats_id
 * @property \Carbon\Carbon $created
 * @property string $event
 * 
 * @property \App\Models\Backend\PodcastStat $podcast_stat
 *
 * @package App\Models\Backend
 */
class PodcastHit extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'podcast_stats_id' => 'int'
	];

	protected $dates = [
		'created'
	];

	protected $fillable = [
		'podcast_stats_id',
		'created',
		'event'
	];

	public function podcast_stat()
	{
		return $this->belongsTo(\App\Models\Backend\PodcastStat::class, 'podcast_stats_id');
	}

	/**
     * @description Fetch the episode's podcast hits 
     * @param  $stat_id here is podcast stat id 
     * @return podcast hit object
     */
	public function fetchPodcastHits($stat_id, $year, $month){
		if(!empty($stat_id)){
			
			$get = $this->select(DB::raw('DAY(created) AS day, COUNT(id) AS hits'))
						->where(function($q) use($stat_id, $year, $month) {
					        $q->where('podcast_stats_id', $stat_id)
					          ->whereRaw('YEAR(created) = "'.$year.'"')
					          ->whereRaw('MONTH(created) = "'.$month.'"');
					    })->groupBy('created')
					      ->orderBy('created', 'asc')
					      ->get();
					      // /->toSql();
			
			/*echo "<pre>";print_R($get);
			echo $stat_id;die;*/
			return $get;

			/* $rows = $this->PodcastHit->find('all', array(
            'fields' => array('DAY(PodcastHit.created) AS day', 'COUNT(PodcastHit.id) AS hits'),
            'conditions' => array('PodcastStat.episodes_id' => $episodeId, 'YEAR(PodcastHit.created)' => $year, 'MONTH(PodcastHit.created)' => $month), 
            'order' => array('PodcastHit.created' => 'asc')
            ,'group' => array('DAY(PodcastHit.created)')*/
        //));
		}
		return false;		
	}
}
