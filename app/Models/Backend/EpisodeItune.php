<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodeItune
 * 
 * @property int $id
 * @property int $episodes_id
 * @property string $title
 * @property string $subtitle
 * @property string $keywords
 * @property bool $is_enabled
 * @property \Carbon\Carbon $publish_date
 * 
 * @property \App\Models\Episode $episode
 *
 * @package App\Models
 */
class EpisodeItune extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'is_enabled' => 'bool'
	];

	protected $dates = [
		'publish_date'
	];

	protected $fillable = [
		'title',
		'subtitle',
		'keywords',
		'is_enabled',
		'publish_date'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Episode::class, 'episodes_id');
	}
}
