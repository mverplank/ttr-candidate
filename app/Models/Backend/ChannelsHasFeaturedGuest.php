<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChannelsHasFeaturedGuest
 * 
 * @property int $id
 * @property int $channels_id
 * @property int $guests_id
 * @property int $off
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Guest $guest
 *
 * @package App\Models
 */
class ChannelsHasFeaturedGuest extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int',
		'guests_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'off'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class, 'channels_id');
	}

	public function guest()
	{
		return $this->belongsTo(\App\Models\Guest::class, 'guests_id');
	}
}
