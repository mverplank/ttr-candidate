<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class I18n
 * 
 * @property int $id
 * @property string $locale
 * @property string $model
 * @property int $foreign_key
 * @property string $field
 * @property string $content
 *
 * @package App\Models
 */
class I18n extends Eloquent
{
	protected $table = 'i18n';
	public $timestamps = false;

	protected $casts = [
		'foreign_key' => 'int'
	];

	protected $fillable = [
		'locale',
		'model',
		'foreign_key',
		'field',
		'content'
	];
}
