<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GuestsHasCategory
 * 
 * @property int $guests_id
 * @property int $categories_id
 * 
 * @property \App\Models\Backend\Category $category
 * @property \App\Models\Backend\Guest $guest
 *
 * @package App\Models\Backend
 */
class GuestsHasCategory extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'guests_id' => 'int',
		'categories_id' => 'int'
	];
	protected $fillable = [
		'guests_id',
		'categories_id'
	];
	public function category()
	{
		return $this->belongsTo(\App\Models\Backend\Category::class, 'categories_id');
	}

	public function guest()
	{
		return $this->belongsTo(\App\Models\Backend\Guest::class, 'guests_id');
	}
	
	
	public function add($new_arr){
		if(!empty($new_arr)){
			$obj = new GuestsHasCategory($new_arr);
			$obj->save();
		}
	}
	
}
