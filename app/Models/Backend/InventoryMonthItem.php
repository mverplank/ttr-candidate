<?php

/**
 * @author: Contriverz
 * @since : Tue, 30 April 2019 11:49:23.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InventoryMonthItem
 * 
 * @property int $id
 * @property int $memberships_id
 * @property int $inventory_months_id
 * @property int $inventory_items_id
 * @property int $week
 * 
 * @property \App\Models\Backend\InventoryItem $inventory_item
 * @property \App\Models\Backend\InventoryMonth $inventory_month
 * @property \App\Models\Backend\Membership $membership
 *
 * @package App\Models\Backend
 */
class InventoryMonthItem extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'memberships_id' => 'int',
		'inventory_months_id' => 'int',
		'inventory_items_id' => 'int',
		'week' => 'int'
	];

	protected $fillable = [
		'week'
	];

	public function inventory_item()
	{
		return $this->belongsTo(\App\Models\Backend\InventoryItem::class, 'inventory_items_id');
	}

	public function inventory_month()
	{
		return $this->belongsTo(\App\Models\Backend\InventoryMonth::class, 'inventory_months_id');
	}

	public function membership()
	{
		return $this->belongsTo(\App\Models\Backend\Membership::class, 'memberships_id');
	}
}
