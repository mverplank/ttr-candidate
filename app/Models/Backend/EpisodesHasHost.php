<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodesHasHost
 * 
 * @property int $id
 * @property int $episodes_id
 * @property int $hosts_id
 * @property int $off
 * @property string $type
 * 
 * @property \App\Models\Episode $episode
 * @property \App\Models\Host $host
 *
 * @package App\Models
 */
class EpisodesHasHost extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'episodes_id' => 'int',
		'hosts_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'off',
		'type'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}

	public function host()
	{
		return $this->belongsTo(\App\Models\Backend\Host::class, 'hosts_id');
	}
	
	/**
    * @description Get the hosts of the episode
    * @param $show_id here is the id of the episode and type is the host type, default is host(1)
    * @default Host Types - Host(1), Cohost(2) and Podcast(3)
    * @return the options of the select box
    */
    public function getHostsByType($episode_id, $type='host'){
		
    	$make_hosts = array();
    	if($episode_id != 0 ){
    		$hosts = EpisodesHasHost::with('host.user.profiles')->where('type', $type)->where('episodes_id', $episode_id)->get();
    		if($hosts->count() > 0 ){
    			foreach($hosts as $key=>$host){
    				if($host->host->user->profiles->count() > 0){
    					$host_id = $host->host->id;
    					foreach($host->host->user->profiles as $profile){
    						$make_hosts[$key]['id'] = $host_id;
				    		$make_hosts[$key]['text']  = $profile->title.' '.$profile->firstname.' '.$profile->lastname;
    					}
    				}
    			}
    			return $make_hosts;
    		}else{
    			return false;
    		}
    	} 
    	return false;
    }	
	
	
}
