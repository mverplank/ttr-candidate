<?php

/**
 * @author: Contriverz  
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;
use App\Models\Backend\Profile;
use App\Models\Backend\Guest;
use App\Models\Backend\Sponsor;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use DB;
/**
 * Class Sponsor
 * 
 * @property int $id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $modified
 * @property bool $is_online
 * @property bool $is_featured
 * @property string $type
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $url
 * @property array $social_urls_json
 * @property string $company
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $email
 * @property string $skype
 * @property string $phone
 * @property string $cellphone
 * @property string $contact_name
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_skype
 * @property string $contact_cellphone
 * @property string $billing_name
 * @property string $billing_email
 * @property string $billing_phone
 * @property string $billing_skype
 * @property string $billing_cellphone
 * @property string $notes
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banners
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $hosts
 *
 * @package App\Models\Backend
 */
class Sponsor extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'is_online' => 'bool',
		'is_featured' => 'bool',
		'social_urls_json' => 'object'
	];

	protected $dates = [
		'start_date',
		'end_date',
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'is_online',
		'is_featured',
		'type',
		'title',
		'firstname',
		'lastname',
		'name',
		'description',
		'url',
		'social_urls_json',
		'company',
		'address',
		'city',
		'state',
		'zip',
		'email',
		'skype',
		'phone',
		'cellphone',
		'start_date',
		'end_date',
		'contact_name',
		'contact_email',
		'contact_phone',
		'contact_skype',
		'contact_cellphone',
		'billing_name',
		'billing_email',
		'billing_phone',
		'billing_skype',
		'billing_cellphone',
		'notes'
	];

	public function banners()
	{
		return $this->hasMany(\App\Models\Backend\Banner::class, 'sponsors_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_sponsors', 'sponsors_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->belongsToMany(\App\Models\Backend\Episode::class, 'episodes_has_sponsors', 'sponsors_id', 'episodes_id')
					->withPivot('id', 'off');
	}

	public function hosts()
	{
		return $this->belongsToMany(\App\Models\Backend\Host::class, 'hosts_has_sponsors', 'sponsors_id', 'hosts_id');
	}

	/**
	* Count all the sponsors
	* @return result in the form of object
	* @param status 1 for online, 0 for offline
	*/
	public function countAllSponsors($host_id=0, $channel_id=0, $status=2, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		$sponsors = Sponsor::select('id', 'name', 'company', 'is_online', 'is_featured');
		
		if($status == 1 || $status == 0){
			$sponsors = $sponsors->where('is_online',$status);
		}

		if($channel_id > 0){
			$sponsors = $sponsors->leftJoin('channels_has_sponsors', 'channels_has_sponsors.sponsors_id', '=', 'sponsors.id')->where('channels_has_sponsors.channels_id', $channel_id);
		}if($host_id > 0){
			$sponsors = $sponsors->leftJoin('hosts_has_sponsors', 'hosts_has_sponsors.sponsors_id', '=', 'sponsors.id')->where('hosts_has_sponsors.hosts_id', $host_id);
		}

		// Overall Search 
        if(!empty($search_value)){
            $sponsors = $sponsors->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%')
    								->orWhere('company' ,'like', '%'.$search_value.'%')
    								->orWhere('start_date' ,'like', '%'.$search_value.'%')
    								->orWhere('end_date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $sponsors = $sponsors->orderBy($order, $dir);
        }else{
            $sponsors = $sponsors->orderBy('name', 'asc');
        } 
		$sponsors = $sponsors->count();
		return $sponsors;
	}

	/**
	* Fetches all the sponsors
	* @return result in the form of object
	* @param status 1 for online, 0 for offline
	*/
	public function getAllSponsors($host_id=0, $channel_id=0, $status=2, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		$sponsors = Sponsor::select('id', 'name', 'firstname', 'lastname', 'company', 'is_online', 'is_featured', 'start_date', 'end_date');
		
		if($status == 1 || $status == 0){
			$sponsors = $sponsors->where('is_online',$status);
		}

		if($channel_id > 0){
			$sponsors = $sponsors->leftJoin('channels_has_sponsors', 'channels_has_sponsors.sponsors_id', '=', 'sponsors.id')->where('channels_has_sponsors.channels_id', $channel_id);
		}
		if($host_id > 0){
			$sponsors = $sponsors->leftJoin('hosts_has_sponsors', 'hosts_has_sponsors.sponsors_id', '=', 'sponsors.id')->where('hosts_has_sponsors.hosts_id', $host_id);
		}

		// Overall Search 
        if(!empty($search_value)){
            $sponsors = $sponsors->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%')
    								->orWhere('company' ,'like', '%'.$search_value.'%')
    								->orWhere('start_date' ,'like', '%'.$search_value.'%')
    								->orWhere('end_date' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $sponsors = $sponsors->orderBy($order, $dir);
        }else{
            $sponsors = $sponsors->orderBy('name', 'asc');
        } 
		$sponsors = $sponsors->offset($start)->limit($length)->get();
		//echo $start." == ".$length;
		//$sponsors = $sponsors->offset($start)->limit($length)->toSql();
		//echo $sponsors;die('success');
		return $sponsors;
	}

	/**
	 * @description: Add the sponsor
	 * @param: $data = $request data
	 */
	public function add($data){
		
		try{
			if(isset($data['Sponsor']['start_date'])){
				$data['Sponsor']['start_date'] = date_format(date_create($data['Sponsor']['start_date']), "Y-m-d");
			}if(isset($data['Sponsor']['end_date'])){
				$data['Sponsor']['end_date']   = date_format(date_create($data['Sponsor']['end_date']), "Y-m-d");
			}
			
			$data['Sponsor']['name'] = $data['Sponsor']['firstname']." ".$data['Sponsor']['lastname'];
			
			$sponsor = new Sponsor($data['Sponsor']);
			if($sponsor->save()){
              	$media = new MediaLinking();
			  	$media->add($sponsor->id, 'Sponsor');
	        }
			try{
				try{
					if(isset($data['Channel']) && !empty($data['Channel'])){
						if(!empty($data['Channel']['Channel'])){
							foreach($data['Channel']['Channel'] as $item){
								$new_arr = array('sponsors_id'=>$sponsor->id, 'channels_id' => $item );
								$sponsors_channel = new ChannelsHasSponsor;
								$sponsors_channel->add($new_arr);
							}
						}
					}
				}catch(\Exception $e){
					return false;
			    }

				try{
					if(isset($data['Host']) && !empty($data['Host'])){
						if(!empty($data['Host']['Host'])){		
							foreach($data['Host']['Host']as $host){
								$newhost = array('sponsors_id'=>$sponsor->id, 'hosts_id' => $host );
								$sponsors_host = new HostsHasSponsor();						
								$sponsors_host->add($newhost);
							}													
						}
					}
				}
				catch(\Exception $e){
					return false;
			    }
			}
			catch(\Exception $e){
				return false;
		    }

		    return $sponsor->id;
		}
		catch(\Exception $e){
			return false;
	    }
	}

	/**
	 * @description: Edit the sponsor
	 * @param: $id = id of the sponsor, $data = $request data
	 */
	public function edit($id, $data){
		try{
			//echo "<pre>";print_r($data);die;
			$sponsor_obj = Sponsor::find($id);
			
			$sponsor_obj->is_online         = $data['Sponsor']['is_online'];			
			$sponsor_obj->is_featured       = $data['Sponsor']['is_featured'];
			//$sponsor_obj->title             = $data['Sponsor']['title'];		
			$sponsor_obj->name              = $data['Sponsor']['firstname']." ".$data['Sponsor']['lastname'];			
			$sponsor_obj->firstname         = $data['Sponsor']['firstname']; 			
			$sponsor_obj->lastname          = $data['Sponsor']['lastname'];			
			$sponsor_obj->company           = $data['Sponsor']['company'];			
			$sponsor_obj->description       = $data['Sponsor']['description'];			
			$sponsor_obj->notes             = $data['Sponsor']['notes'];			
			$sponsor_obj->social_urls_json  = $data['Sponsor']['social_urls_json'];			
			$sponsor_obj->email             = $data['Sponsor']['email'];		
			$sponsor_obj->skype             = $data['Sponsor']['skype'];			
			$sponsor_obj->phone             = $data['Sponsor']['phone'];		
			$sponsor_obj->cellphone         = $data['Sponsor']['cellphone'];			
			$sponsor_obj->city              = $data['Sponsor']['city'];			
			$sponsor_obj->state             = $data['Sponsor']['state'];			
			$sponsor_obj->contact_name      = $data['Sponsor']['contact_name'];			
			$sponsor_obj->contact_email     = $data['Sponsor']['contact_email'];			
			$sponsor_obj->contact_skype     = $data['Sponsor']['contact_skype'];		
			$sponsor_obj->contact_phone     = $data['Sponsor']['contact_phone'];
			$sponsor_obj->contact_cellphone = $data['Sponsor']['contact_cellphone'];		
			$sponsor_obj->billing_name      = $data['Sponsor']['billing_name'];		
			$sponsor_obj->billing_email     = $data['Sponsor']['billing_email'];	
			$sponsor_obj->billing_skype     = $data['Sponsor']['billing_skype'];
			$sponsor_obj->billing_phone     = $data['Sponsor']['billing_phone'];			
			$sponsor_obj->billing_cellphone = $data['Sponsor']['billing_cellphone'];
			if(isset($data['Sponsor']['start_date'])){
				$sponsor_obj->start_date = date_format(date_create($data['Sponsor']['start_date']), "Y-m-d");
			}if(isset($data['Sponsor']['end_date'])){
				$sponsor_obj->end_date   = date_format(date_create($data['Sponsor']['end_date']), "Y-m-d");
			}
						
			//Detach the relations of Channels 
			if(!empty($data['old_assigned_channels'])){
				$get_channels = json_decode($data['old_assigned_channels']);
				
				if(isset($data['Channel']) && !empty($data['Channel'])){
					if(!empty($data['Channel']['Channel'])){
						foreach($get_channels as $chn){
							if(!in_array($chn, $data['Channel']['Channel'])){
								$sponsor_obj->channels()->detach($chn);
							}
						}
						foreach($data['Channel']['Channel'] as $item){
							if(!in_array($item, $get_channels)){
								$sponsor_obj->channels()->attach($item);
							}
						}
					}
				}else{
					if(!empty($get_channels)){
			        	foreach($get_channels as $chn){
							$sponsor_obj->channels()->detach($chn);		 							
						}
			        }					
				}
			}
			//Detach the relations of Hosts 
			if(!empty($data['Host'])){

				$get_hosts_arr = array();
				$get_hosts = $sponsor_obj->hosts()->get();
				if($get_hosts->count() > 0){
		            foreach($get_hosts as $hosts){
		                $get_hosts_arr[] = $hosts->id;
		            }
		        }				
				//echo "<pre>"; print_r($get_hosts_arr); die();
				if(isset($data['Host']) && !empty($data['Host'])){
					if(!empty($data['Host']['Host'])){
						foreach($get_hosts_arr as $host){
							if(!in_array($host, $data['Host']['Host'])){
								$sponsor_obj->hosts()->detach($host);
							}
						}
						foreach($data['Host']['Host'] as $item){
							if(!in_array($item, $get_hosts_arr)){
								$newhost = array('sponsors_id'=>$sponsor_obj->id, 'hosts_id' => $item );
								//$sponsor_obj->hosts()->attach($item);
								$host = HostsHasSponsor::updateOrCreate($newhost);				
								$host->save();

							}
						}
					}
				}else{
					if(!empty($get_hosts_arr)){
			        	foreach($get_hosts_arr as $hostt){
							$sponsor_obj->hosts()->detach($hostt);		 							
						}
			        }					
				}
			}			
			$sponsor_obj->save();
			return true;
		}
		catch(\Exception $e){
			//echo $e->getMessage();die();
			return false;
	    }
	}

	/******  Delete the Sponsor and its related data with the Host and Channels   ******/
	public function deleteSponsor($id){
		
		if($id != '' || $id !=0){
			
			$sponsor = Sponsor::find($id);
			$sponsor->channels()->detach();
			$sponsor->hosts()->detach();
			deleteMediaLink('Sponsor', $id);
			return $sponsor->delete();
		}
	}	

	/**
	* Fetch the sponsors for the filters 
	* @return result is the object of the name and id of the sponsors
	*/
	public function getSponsorsForFilter(){
		$sponsors_list = $this->select('id', 'name')->orderBy('lastname', 'asc')->pluck('name', 'id');
		return $sponsors_list;
	}

	/*public function getAllSponsor(){
		$sponsor_obj = Sponsor::select('id', 'name')->orderBy('name', 'asc')->pluck('name', 'id');
		return $sponsor_obj;
	}*/

	/**
	* Export  Sponsors
	* @return result in the form of object
	*/
	public function eportSponsors($type='',$channel_id='',$all='', $online='', $offline='', $featured=''){

		$sponsors = Sponsor::select('sponsors.title', 'sponsors.name','sponsors.company', 'sponsors.type', 'sponsors.url', 'sponsors.email', 'sponsors.skype','sponsors.phone', 'sponsors.cellphone','sponsors.contact_name', 'sponsors.contact_email', 'sponsors.contact_phone', 'sponsors.contact_skype', 'sponsors.contact_cellphone','sponsors.billing_name','sponsors.billing_email','sponsors.billing_skype','sponsors.billing_phone','sponsors.billing_cellphone');

		if(!empty($channel_id)){
			$sponsors =  $sponsors->leftJoin('channels_has_sponsors', 'sponsors.id', '=', 'channels_has_sponsors.sponsors_id')
							      ->where('channels_has_sponsors.channels_id','=',$channel_id);
		}
		if(!empty($online)){
			$sponsors =  $sponsors->where('sponsors.is_online','=',1);
		}
		if(!empty($offline)){
			$sponsors =  $sponsors->where('sponsors.is_online','=',0);
		}
		if(!empty($featured)){
			$sponsors =  $sponsors->where('sponsors.is_featured','=',1);
		}									

		/*if(!empty($all)){
			$sponsors =  $sponsors->where('sponsors.deleted',0);
		}*/
		$sponsors = $sponsors->orderBy('sponsors.title', 'asc')->get();
		return $sponsors;
	}
	/**
	 * @description search the host
	 * @param $data is the $request object
	 */	
	public function hostSearch($searchtext, $type='host'){
		$make_hosts = array();
		if(!empty($searchtext)){
			$profile = Profile::select('profiles.title','profiles.sufix','profiles.firstname','profiles.lastname','hosts.id as host_id')
					->leftJoin('hosts', 'profiles.users_id', '=', 'hosts.users_id')
					->leftJoin('users', 'users.id', '=', 'hosts.users_id')
					->leftJoin('user_settings', 'user_settings.users_id', '=', 'hosts.users_id')
					->leftJoin('host_itunes', 'host_itunes.hosts_id', '=', 'hosts.id');

			if($type == "host"){
				$profile = $profile->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('host_types_id', 1);
			}else if($type == "cohost"){
				$profile = $profile->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('host_types_id', 2);
			}else if($type == "podcast-host"){
				$profile = $profile->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('host_types_id', 3);
			}

			$profile = $profile->where('hosts.deleted', 0)->groupBy('hosts.id');
			$profile = $profile->where(function ($query) use ($searchtext) {
		                    $query->where('profiles.title', 'like', '%'.$searchtext .'%')
		                     	  ->orWhere('profiles.sufix', 'like', '%'.$searchtext .'%')
		                     	  ->orWhere('profiles.lastname', 'like', '%'.$searchtext .'%')
		                          ->orWhere('profiles.firstname', 'like', '%'.$searchtext .'%');
		                })->get(); 

		    if($profile->count() > 0){
		    	
		    	$i=0;
		    	foreach($profile as $host){
		    		$make_hosts[$i]['id']   = $host->host_id;
		    		$make_hosts[$i]['text'] = $host->title.' '.$host->firstname.' '.$host->lastname.' '.$host->sufix;
		    		$i++;
		    	}
		    	return json_encode($make_hosts);	    	
		    }            
		}else{
			$make_hosts[0]['id']   = 0;
			$make_hosts[0]['text'] = "No result found.";
			return json_encode($make_hosts);	    	
		}
	}
	/**
	 * @description search the guest
	 * @param $data is the $request object
	 */	
	public function GuestSearch($searchtext){
		$make_guests = array();
		if(!empty($searchtext)){
			$guests = Guest::select('guests.title','guests.sufix','guests.firstname','guests.lastname','guests.id as guest_id')
							->leftJoin('hosts', 'guests.hosts_id', '=', 'hosts.id')
							->where('guests.deleted', 0);
			$guests = $guests->where(function ($query) use ($searchtext) {
		                    $query->where('title', 'like', '%'.$searchtext .'%')
		                     	  ->orWhere('sufix', 'like', '%'.$searchtext .'%')
		                     	  ->orWhere('lastname', 'like', '%'.$searchtext .'%')
		                          ->orWhere('firstname', 'like', '%'.$searchtext .'%');
		                })->get();
		                
		    if($guests->count() > 0){
		    	$i=0;
		    	foreach($guests as $guest){
		    		$make_guests[$i]['id']   = $guest->guest_id;
		    		$make_guests[$i]['text'] = $guest->title.' '.$guest->firstname.' '.$guest->lastname.' '.$guest->sufix;
		    		$i++;
		    	}
		    	return json_encode($make_guests);	    	
		    }            
		}else{
			$make_guests[0]['id']   = 0;
			$make_guests[0]['text'] = "No result found.";
			return json_encode($make_guests);	    	
		}		
	}

	/**
	 * @description search the sponsor
	 * @param $data is the $request object
	 */	
	public function SponsorSearch($searchtext){
		$make_sponsors = array();
		if(!empty($searchtext)){
			$sponsors = Sponsor::select(DB::raw('`id`, (TRIM(CONCAT( `name`, " (", TRIM(`company`), ")" ))) as `Sponsor__fullname`'))
								->where('is_online', 1);
			$sponsors = $sponsors->where(function ($query) use ($searchtext) {
		                    $query->where('name', 'like', '%'.$searchtext .'%')
		                          //->orWhere('title', 'like',$searchtext .'%');
		                          ->orWhere('company', 'like', '%'.$searchtext .'%');
		                })->get();

		    if($sponsors->count() > 0){
		    	$i=0;
		    	foreach($sponsors as $sponsor){
		    		$make_sponsors[$i]['id']   = $sponsor->id;
		    		$make_sponsors[$i]['text'] = $sponsor->Sponsor__fullname;
		    		$i++;
		    	}
		    	return json_encode($make_sponsors);	    	
		    }            
		}else{
			$make_sponsors[0]['id']   = 0;
			$make_sponsors[0]['text'] = "No result found.";
			return json_encode($make_sponsors);	    	
		}
	}

	/*
     * Move All Sponsor Image From Tmp Location to permananet folder
     *
     */
    public function procesImage($object, $module){
    	
        $folders = ['player', 'zapbox', 'zapboxsmall', 'widget', 'cover', 'attachments', 'photo'];
        $userid = \Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }    
        return 'success';
    }

 

}
