<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Page
 * 
 * @property int $id
 * @property string $name
 * @property array $definition_json
 * @property array $data_json
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $html
 * @property bool $is_editable
 * @property bool $is_removable
 * @property bool $is_enabled
 * @property bool $is_listed
 *
 * @package App\Models
 */
class Page extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'definition_json' => 'json',
		'data_json' => 'json',
		'is_editable' => 'bool',
		'is_removable' => 'bool',
		'is_enabled' => 'bool',
		'is_listed' => 'bool'
	];

	protected $fillable = [
		'name',
		'definition_json',
		'data_json',
		'title',
		'description',
		'keywords',
		'html',
		'is_editable',
		'is_removable',
		'is_enabled',
		'is_listed'
	];

	/**
	 * Fetch the content data acc. to the content category id
	 * @param $content_category_id is the category id of the content
	 * @param $order_field is the column for the sorting
	 * @param $order_direction is the sorting order
	 */
	public function fetchPages($content_category_id='', $order_field, $order_direction, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$get = $this;

		// Overall Search 
        if(!empty($search_value)){
            $get = 	$get->where(function($q) use ($search_value){
    					$q->orWhere('title' ,'like', '%'.$search_value.'%');
    				});
        }

        // Sorting by column
        if($order_field != null){
        	if($order_field == "modified"){
	    		$order_field = "updated_at";
	    	}else if($order_field == "created"){
	    		$order_field = "created_at";
	    	}
            $get = $get->orderBy($order_field, $order_direction);
        }else{
            $get = $get->orderBy('created_at', 'desc');
        }
		$get = $get->orderBy($order_field, $order_direction);
		$get = $get->offset($start)->limit($length)->get();
		return $get;
	}

	/**
	 * Fetch the content data acc. to the content category id
	 * @param $content_category_id is the category id of the content
	 * @param $order_field is the column for the sorting
	 * @param $order_direction is the sorting order
	 */
	public function countPages($content_category_id='', $order_field, $order_direction, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){
		$get = $this;
		// Overall Search 
        if(!empty($search_value)){
            $get = $get->where(function($q) use ($search_value){
    							$q->orWhere('title' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
        	if($order == "modified"){
        		$order = "updated_at";
        	}
            $get = $get->orderBy($order_field, $order_direction);
        }else{
            $get = $get->orderBy('created_at', 'desc');
        }
		$get = $get->orderBy($order_field, $order_direction);
		$get = $get->count();
		return $get;
	}

	function check_slug($slug, $id=false){
		if($id){
			$checkslug = Page::where('name','=',$slug)->where('id', '!=', $id)->get()->count();
		}
		//$this->db->where('slug', $slug);
		else{
			$checkslug = Page::where('name','=',$slug)->get()->count();
		}
		return (bool) $checkslug;

	}

	/**
	* create slug name 
	* @param $name is the request data
	* @return slug
	*/
	function validate_slug($slug, $id=false, $count=false){
		if($this->check_slug($slug.$count, $id)){
			if(!$count){
				$count	= 1;
			}else{
				$count++;
			}
			return $this->validate_slug($slug, $id, $count);
		}else{
			return $slug.$count;
		}

	}

	/**
	* Add Page 
	* @param $data is the request object
	* @return boolean
	*/
	public function add($data){		
		try{			
			if(isset($data['Page'])){
				$slug_name ='';
				if($data['Page']['name'] ==''){
					$slug_name = $data['Page']['title'];
				}else{

					$slug_name = $data['Page']['name'];
				}
				$slug = str_slug($slug_name, '-');
				$data['Page']['name'] = $this->validate_slug($slug);
				//echo $data['Page']['name']; die;
				$page = new Page($data['Page']);
				$page->save();
			}
			return true;
		}catch(\Exception $e){
        	echo $e->getMessage('page');die;
			return false;
        }
	}

	/**
	* update Page 
	* @param $data is the request object
	* @return boolean
	*/
	public function edit($id, $data){		
		try{			
			if(isset($data['Page'])){
				$slug_name ='';
				if($data['Page']['name'] ==''){
					$slug_name = $data['Page']['title'];
				}else{

					$slug_name = $data['Page']['name'];
				}
				$slug = str_slug($slug_name, '-');
				$data['Page']['name'] = $this->validate_slug($slug,$id);
				$page = new Page($data['Page']);
				$page->save();
			}
			return true;
		}catch(\Exception $e){
			//echo $e->getMessage();die();
			return false;
	    }
	}

	/**
	 * @description delete page
	 * @param where $id is page id
	 */	
	public function deletePage($id=0){
		try{
			if($id != '' || $id !=0){
				$show = Page::find($id);
				$show->delete();	
			}
			return true;
		}catch(\Exception $e){
			return false;
	    }
	}
}
