<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChannelsHasFeaturedHost
 * 
 * @property int $id
 * @property int $channels_id
 * @property int $hosts_id
 * @property int $off
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Host $host
 *
 * @package App\Models
 */
class ChannelsHasFeaturedHost extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'channels_id' => 'int',
		'hosts_id' => 'int',
		'off' => 'int'
	];

	protected $fillable = [
		'off'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class, 'channels_id');
	}

	public function host()
	{
		return $this->belongsTo(\App\Models\Host::class, 'hosts_id');
	}
}
