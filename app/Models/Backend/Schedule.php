<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;
use Reliese\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;
use DB;
/**
 * Class Schedule
 * 
 * @property int $id
 * @property int $channels_id
 * @property int $shows_id
 * @property int $schedules_id
 * @property bool $deleted
 * @property string $dow
 * @property int $week
 * @property int $month
 * @property int $year
 * @property \Carbon\Carbon $time
 * @property int $duration
 * @property string $type
 * @property string $file
 * @property string $stream
 * @property string $type_label
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Schedule $schedule
 * @property \App\Models\Show $show
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $feedback
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 *
 * @package App\Models\Backend
 */
class Schedule extends Eloquent
{
	public $timestamps = false;

	private $_firstDayOfWeek = 'Sun';
 	private $_channelSchedule = array();

	protected $casts = [
		'channels_id' => 'int',
		'shows_id' => 'int',
		'schedules_id' => 'int',
		'deleted' => 'bool',
		'month' => 'int',
		'year' => 'int',
		'duration' => 'int'
	];

	protected $dates = [
		
		'start_date',
		'end_date'
	];

	protected $time = [
		'time'
	];

	protected $fillable = [
		'schedules_id',
		'channels_id',
		'shows_id',
		'deleted',
		'dow',
		'week',
		'month',
		'year',
		'time',
		'duration',
		'type',
		'file',
		'stream',
		'type_label',
		'start_date',
		'end_date'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Backend\Channel::class, 'channels_id');
	}

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Backend\Schedule::class, 'schedules_id');
	}

	public function show()
	{
		return $this->belongsTo(\App\Models\Backend\Show::class, 'shows_id');
	}

	public function episodes()
	{
		return $this->belongsToMany(\App\Models\Backend\Episode::class, 'episodes_has_schedules', 'schedules_id', 'episodes_id')
					->withPivot('id', 'date', 'time');
	}

	public function feedback()
	{
		return $this->hasMany(\App\Models\Backend\Feedback::class, 'schedules_id');
	}

	public function schedules()
	{
		return $this->hasMany(\App\Models\Backend\Schedule::class, 'schedules_id');
	}

	/**
	 * @description Adding the Schedule
	 * @param $data is the $request object
	 */	
	public function add($data){
		
		// Save data to schedule
		try{			
			if(isset($data['Schedule']['id'])){
				unset($data['Schedule']['id']);
			}
			
			$dow = $week = '';
			foreach($data['Schedule']['data'] as $schedule){
				$new_schedule = $schedule;
				if(!empty($schedule['dow'])){
					$dow = implode (",", $schedule['dow']);
				}
				if(!empty($schedule['week'])){
					$week = implode (",", $schedule['week']);
				}
				$new_schedule['start_date'] = $this->getUpdatedAtAttribute($schedule['start_date'], 'date'); 
				$new_schedule['end_date'] = $this->getUpdatedAtAttribute($schedule['end_date'], 'date'); 
				$new_schedule['time'] = $this->getUpdatedAtAttribute($schedule['time'], 'time');
				$new_schedule['dow']  = $dow;
				$new_schedule['week'] = $week;
				
				$save_schedule = new Schedule($new_schedule);
				$save_schedule->save();

				/*foreach($schedule['dow'] as $day){
					if(!empty($schedule['week'])){
						foreach($schedule['week'] as $week){
							$new_schedule['start_date'] = $this->getUpdatedAtAttribute($schedule['start_date'], 'date'); 
							$new_schedule['end_date'] = $this->getUpdatedAtAttribute($schedule['end_date'], 'date'); 
							$new_schedule['time'] = $this->getUpdatedAtAttribute($schedule['time'], 'time');
							$new_schedule['dow']  = $day;
							$new_schedule['week'] = $week;
							$save_schedule = new Schedule($new_schedule);
							$save_schedule->save();	
						}
					}
				}*/				
			}	   
			return true;
		}catch(\Exception $e){
			
			return false;
		}
	}

	/**
	 * @description Editing the Schedule
	 * @param $data is the $request object
	 */	
	public function edit($data){
		
		// Update data to schedule
		try{
			$schedule_obj = Schedule::find($data['Schedule']['id']);
			$dow = $week = '';
			foreach($data['Schedule']['data'] as $schedule){

				$new_schedule = $schedule;
				if(!empty($schedule['dow'])){
					$dow = implode (",", $schedule['dow']);
				}
				if(!empty($schedule['week'])){
					$week = implode (",", $schedule['week']);
				}
				$new_schedule['start_date'] = $this->getUpdatedAtAttribute($schedule['start_date'], 'date'); 
				$new_schedule['end_date'] = $this->getUpdatedAtAttribute($schedule['end_date'], 'date'); 
				$new_schedule['time'] = $this->getUpdatedAtAttribute($schedule['time'], 'time');
				$new_schedule['dow']  = $dow;
				$new_schedule['week'] = $week;
				$schedule_obj->update($new_schedule);

				/*$new_schedule = $schedule;
				if(!empty($schedule['dow'])){
					foreach($schedule['dow'] as $day){
						if(!empty($schedule['week'])){
							foreach($schedule['week'] as $week){
								$new_schedule['start_date'] = $this->getUpdatedAtAttribute($schedule['start_date'], 'date'); 
								$new_schedule['end_date'] = $this->getUpdatedAtAttribute($schedule['end_date'], 'date'); 
								$new_schedule['time'] = $this->getUpdatedAtAttribute($schedule['time'], 'time');
								$new_schedule['dow']  = $day;
								$new_schedule['week'] = $week;
								//echo "<pre>";print_R($new_schedule);die('fvfdv');
								$schedule_obj->update($new_schedule);	
							}
						}
					}
				}*/
			}
			return true;
		}catch(\Exception $e){
			return false;
		}   
	}	

	/**
	 *@description - Change format of the dates 
	 *@param - $date is the value of the date and $format to which format the date would be return
	 */
	public function getUpdatedAtAttribute($date, $format='')
	{		
		if($format == "time"){
			return Carbon::parse($date)->format('H:i:s');
		}else if($format == "date"){
			return Carbon::parse($date)->format('Y-m-d');
		}else{
			return Carbon::parse($date)->format('Y-m-d H:i:s');
		}
	    
	}

	/**
	 *@description - Fetch schedule of the channels 
	 *@param - $month for which month, $year for which year the schedule is being fetched and the channel_id is the id of the channel ()
	 */
	public function fetchSchedule($month, $year, $channel_id){

		$schedules = DB::select( DB::raw("SELECT `Show`.`name`, `Show`.`is_encore`, `Schedule`.`id`, `Schedule`.`dow` as schedule_dow, `Schedule`.`time` as schedule_time, `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Schedule`.`year`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`type_label`, `Replay`.`dow` as replay_dow, `Replay`.`time` as replay_time, `Replay`.`channels_id`, (SELECT COUNT(*) FROM schedules Schedules WHERE `Schedules`.`channels_id`=`Schedule`.`channels_id` AND `Schedules`.`dow`=`Replay`.`dow` AND `Schedules`.`time`=`Replay`.`time`) AS `replays_count` FROM `schedules` AS `Schedule` LEFT JOIN `schedules` AS `Replay` ON (`Replay`.`id` = `Schedule`.`schedules_id` AND `Replay`.`deleted` = 0) LEFT JOIN `shows` AS `Show` ON (`Schedule`.`shows_id` = `Show`.`id`) LEFT JOIN `channels` AS `Channel` ON (`Schedule`.`channels_id` = `Channel`.`id`)  WHERE `Schedule`.`deleted` = '0' AND `Show`.`deleted` = 0 AND `Schedule`.`channels_id` = ".$channel_id."  GROUP BY `Schedule`.`id`"));

		// $schedules = $this->select('schedules.id', 'schedules.time', 'schedules.dow', 'schedules.week', 'schedules.year', 'schedules.month', 'schedules.type', 'schedules.start_date', 'schedules.end_date', 'shows.name')
		// 				->join('shows', 'shows.id', '=', 'schedules.shows_id')
		// 				->where('schedules.month', $month)
		// 				->where('schedules.year', $year)
		// 				->where('schedules.channels_id', $channel_id)
		// 				->get();

		

		return $schedules;
	}

	/**
	 *@description - Fetch replays of the shows 
	 *@param - $month for which month, $year for which year the schedule is being fetched and the channel_id is the id of the channel ()
	 */
	public function fetchReplays($channel_id){
		
		$schedules = DB::select( DB::raw('SELECT `Schedule`.`id`, (CONCAT(`Schedule`.`dow`," ",DATE_FORMAT(`Schedule`.`time`,"%l:%i %p")," - ", `Show`.`name`, " (", `Schedule`.`duration`, " min)")) AS  `Schedule__name` 
			FROM `schedules` AS `Schedule` 
			LEFT JOIN `shows` AS `Show` ON (`Schedule`.`shows_id` = `Show`.`id`) 
			LEFT JOIN `channels` AS `Channel` ON (`Schedule`.`channels_id` = `Channel`.`id`)  
			WHERE `type` != "replay" AND `Schedule`.`deleted` = "0" AND `Channel`.`id` = '.$channel_id.'  
			ORDER BY (CASE dow WHEN "Sun" THEN 0 WHEN "Mon" THEN 1 WHEN "Tue" THEN 2 WHEN "Wed" THEN 3 WHEN "Thu" THEN 4 WHEN "Fri" THEN 5 WHEN "Sat" THEN 6 END) ASC, `Schedule`.`time` ASC'));
		
		return $schedules;
	}	

	/**
	 *@description - Fetch schedule 
	 *@param  - $id id the unique id of the schedule
	 *@return - object of the schedule 
	 */
	public function fetchScheduleByID($id){
		if($id != 0){
			$get = $this->where('id', $id)->first();
			return $get;
		}else{
			return false;
		}
	}

	/**
     * Get schedules as FullCalendar events 
     * 
     * @param String $fromDate      date period begin in format: yyyy-mm-dd
     * @param String $toDate        date period end in format: yyyy-mm-dd    
     * @param Array $conditions     query additional conditions 
     * 
     */
    public function getCalendarEvents($fromDate = null, $toDate = null, $channelId = null, $hostId = null, $onlyLive=false, $allDetails=true) {

        // if host id given - get host details
        if ($hostId !== null && $hostId != 0) {
        	$hostObj = new Host();
        	$hostDefaultChannelId = $hostObj->getDefaultChannelId($hostId);
        }
       
        // Shows and Episodes data
        $shows = $this->getShowsEpisodesForPeriod($channelId, $fromDate, $toDate, $hostId);
     	//echo "<pre>";print_R($shows);die('success');
        // prepare data for FullCalendar
        $day = $fromDate;
        $toDateDay = $toDate;

        $calendarEvents = array();
        $calendarEventsData = array();
        while ($day <= $toDateDay) {

            // is any show for this day
            if (isset($shows[$day])) {
            	//echo "CHECK DAY =".$day."<pre>";print_R($shows[$day]);die;
                foreach ($shows[$day] as $time => $d) { // all day shows (by hours) 
                    
                    // Begin HOST only 
                    if ($hostId) { // this is agenda calendar view for host user
                        if ($d->type === 'live') {
                            //
                            // LIVE type show
                            //                            
                            // If live show is scheduled on more than single channel at this same DOW and time 
                            // show it only for default host channel ( Host.default_channel_id ).
                            // If live show is defined only for other channel than default - show it there   

                            if ($hostDefaultChannelId !== $channelId) { // this channel is not default host channel 
                                // Check if this live show is scheduled on other channels at this same dow and time too.
                                // If NOT - show it on calendar 
                               
                                $isMulitpleSchedules = $this->select('channels_id')
					                                ->leftJoin('channels', 'channels.id', '=', 'schedules.channels_id')
					                                ->where('schedules.id', '!=', $d->schedule_id)
					                                ->where('schedules.shows_id', '=', $d->shows_id)
					                                ->where('schedules.time', '=', $d->schedule_time)
					                                ->whereRaw('FIND_IN_SET ("'.$d->schedule_dow.'", schedules.dow)')
					                                ->get();
                                /*$isMulitpleSchedules = $this->find('all', array('fields' => array('Schedule.channels_id'), 'conditions' => array(
                                        'Schedule.id !=' => $d['Schedule']['id'],
                                        'Schedule.shows_id' => $d['Show']['id'], // this smae show 
                                        //'Schedule.type' => $d['Schedule']['type'],
                                        //'Schedule.channels_id !=' => $channelId,   // is present on host default channel? 
                                        'Schedule.dow' => $d['Schedule']['dow'], // this same DOW
                                        'Schedule.time' => $d['Schedule']['time']   // this same TIME 
                                )));*/

                                if ($isMulitpleSchedules->count() > 0) { // show will be shown only for defaul channel 
                                    // check if show is scheduled for default host channel 
                                    $hide = false;
                                    foreach ($isMulitpleSchedules as $s) {

                                        if ($s->channels_id == $hostDefaultChannelId) {

                                            // this show is scheduled for default host channel too - don't show here 
                                            $hide = true;
                                            break;
                                        }
                                    }

                                    if ($hide) {
                                        // don't show - go to next scheduled show 
                                        continue;
                                    }
                                } else {
                                    // this live show is not present on other channels
                                    // Show it
                                }
                            }
                        }
                    }
                    // --- end of HOST only 
                   
                    $className = null;
                    if (isset($d->episode_id) && $d->type !== 'replay') { // episdoe details defined for this show 
                        //
                        // Episode premiere or replay 
                        //
                        $id = $d->episode_id;
                        $title = !empty($d->title) ? nl2br($d->title." \n <strong>Show: </strong>".$d->name ) : $d->name;
                        //$bg = '#65822F';
                        $className = 'cal-episode cal-'.$d->type;
                        //$episode = array('Episode' => array('id' => $id));
                        $episode = array('episode_id' => $id);
                    } else { // no Episode details 
                                   
                        // Show premiere or replay (taken from Channel base schedule) 
                        $id = $d->schedule_id;

                        //
                        // Premiere
                        //
                        if ($d->type !== 'replay') { // this is premiere or encore
                            if ($d->type !== 'live') {

                                if (!$d->is_encore) {

                                    // file or stream 
                                    $title = $d->name;
                                    $className = 'cal-show';
                                } else {

                                    // encore show 
                                    $title = ($allDetails ? '[E] ' : '') . $d->name;
                                    $className = 'cal-show cal-encore';
                                }

                                //$bg = '#756E51';                                
                            } else {

                                // live show                                                                 
                                $title = ($allDetails ? '[L] ' : '') . $d->name;
                                //$bg = '#756E51';
                                $className = 'cal-show cal-live';
                            }
                        } else {    // this is replay
                            //
                            // Replay 
                            //
                            //$className = 'no-edit'; // don't show pointer cursor (Replay is not editable) 
                            // Is replayed Episode for Show is defined ?                             
                            
                           /* $replayDate = $this->dow2date($d->replay_dow, $day);

                            if ($replayDate && !isset($shows[$replayDate][$d->replay_time]['Episode']['id'])) { // this is replay of show 
                                //
                                // Show Replay 
                                //
                                $title = '[R: ' . $d->replay_dow . ' ' . $helperBase->time($d->replay_time) . '] ' . $d->name;
                                $bg = '#B3B1A0';
                            } else { // this is replay of episode
                                //
                                // Episode Replay 
                                //
                                $title = '[R: ' . $d->dow . ' ' . $helperBase->time($d->time) . '] ' . $shows[$replayDate][$d->replay_time]['Episode']['title'];
                                $bg = '#A0C45C';
                            }*/
                            
                            
                            //$replayDate = isset($d['ReplayOf']['date']) ? $d['ReplayOf']['date'] : $this->_dow2date($d['ReplayOf']['dow'], $day);
                            $replayDate = $this->dow2date($d->replay_dow, $day);
							
                            if ($replayDate) {
                                                            
                                if ($replayDate && !isset($d->episode_id)) { // this is replay of show 
                                    //
                                    // Show Replay 
                                    //
                                    if ($allDetails) {
                                
                                        $title = '[R: ' . (isset($d->replay_channels_id) && $channelId !== $d->replay_channels_id ? 'Ch#' . $d->replay_channels_id . ' ' : '') . $d->replay_dow . (property_exists($d,'orginalShowDate') ? ' ' . $d->orginalShowDate : '') . ' ' . (($d->replay_time) ? date("g:i a", strtotime($d->replay_time)) : '') . '] ' . $d->name;
                                    } else {
                                        $title = 'Replay of: "' . $d->name . '" from: ' . $d->replay_dow . (property_exists($d,'orginalShowDate') ? ' ' . $d->orginalShowDate : '') . ' ' . (($d->replay_time) ? date("g:i a", strtotime($d->replay_time)) : '');
                                    }
                                    //$bg = '#B3B1A0';
                                    $className = ' no-edit cal-show cal-replay';
                                } else { // this is replay of episode
                                    
                                    //
                                    // Episode Replay 
                                    //
                                    if ($allDetails) {                                

                                        $title = '[R: ' . (property_exists($d, 'replay_channels_id') && $channelId !== $d->replay_channels_id ? 'Ch#' . $d->replay_channels_id . ' ' : '') . $d->replay_dow . (property_exists($d, 'orginalShowDate') ? ' ' . $d->orginalShowDate : '') . ' ' . (($d->replay_time) ? date("g:i a", strtotime($d->replay_time)) : '') . '] ' . (!empty($d->title) ? $d->title : $d->name);
                                    } else {
                                        
                                        $title = 'Replay of: "' . (!empty($d->title) ? $d->title : $d->name) . '" from: ' . $d->replay_dow . (property_exists($d,'orginalShowDate') ? ' ' . $d->orginalShowDate : '') . ' ' . (($d->replay_time) ? date("g:i a", strtotime($d->replay_time)) : '');                                    
                                    }
                                    
                                    //$bg = '#A0C45C';
                                    $className = 'no-edit cal-episode cal-replay';
                                }
                            } else {    // no show to be replayed present 

                                //$title = "[R: SHOW DELETED!]\nRe-assign to new show!";
                                //$bg = '#FFA112';
                                continue;   // show to be replayed don't exists - show nothing (empty slot) 
                            }
                        }
                        $episode = array(); // no episode data 
                    }

                    if ($d->is_online) { // only if is online
                        // show replays only for admin user; 
                        // for host user - ----- hide all replays of theirs shows
                        // hiade ALL except LIVE shows 
                        //if ($hostId && $d['Schedule']['type'] == 'replay') { 
                        if ( ($hostId && $d->type !== 'live') ||
                                ($onlyLive && $d->type !== 'live'     // only live shows forced 
                                && $d->type !== 'file_or_stream'      // include file_or_stream too 
                                ) ) {

                            continue;
                        }
                        //echo "checki<pre>";print_R($episode);
                        // add single calendar (FULLcalendar syntax) event
                        $calendarEvents[] = $this->addCalEvent($day . ' ' . $d->schedule_time, $d->duration, array(
                            'id' => $allDetails ? $id : $channelId, // for front end instedad Shcedule.id or Episode.id send CHannel.id 
                            'title' => $title,
                            //'backgroundColor' => $bg,
                            //'borderColor' => $bg,
                            'className' => $className), 
                        	array(
                        		'schedule_id' => $d->schedule_id,
                        		'type' => $d->type,
                        		'name' => $d->name,
	                            'is_encore' => $d->is_encore,
	                            $episode
                        	)
	                        /*array_replace_recursive(array(
	                            //channelId' => $channelId,
	                            'Schedule' => array(
	                                'id' => $d->schedule_id,
	                                'type' => $d->type
	                            ),
	                            'Show' => array(
	                                'name' => $d->name,
	                                'is_encore' => $d->is_encore
	                            )
                            ), $episode)*/
                        );
                    }
                }
            }

            // go to next day 
            $day = date("Y-m-d", strtotime('+1 day', strtotime($day)));
        }
        //echo "bfgb fgb g<pre>";print_r($calendarEvents);die('success');
        return array('calendarEvents' => $calendarEvents, 'calendarEventsData' => $calendarEventsData);
    }


     /**
     * Get full schedule (shows, episodes, replayes) for given period.
     * If no episode defined - no 'Episode' array present.
     * USE OLD ->get method 
     * 
     * @param Int $channelId
     * @param String $from          date period begin (ex. 2104-07-03) 
     * @param String $to            date period end (ex. 2014-07-20) 
     * @param Bool $fullDetails     if true read also show/episode description and host id/name and guest(s) id/name 
     * 
     * @returns Array         Schedule as array:
     *                          [date][time] = Show/Schedule(/Replay) data
     *                          ex.
     *                          ['2014-07-30']['13:00'] = array('Schedule' => array(), 'Show' => array(), 'Episode' => array(), 'ReplayOf' => array())
     */
    public function getShowsEpisodesForPeriod($channelId, $from, $to = null, $hostId = null, $guestId = null, $fullDetails = false, $scheduleId=null) {
    	
        // shows for given period
        
        $shows = $this->getShowsForPeriod($channelId, $from, $to, $hostId, $guestId, $fullDetails, $scheduleId);    
      
        $episodes = $this->getEpisodesForReplays($shows, $fullDetails);
        
        // Replace schedules with the episodes if exists
       	$new_show = array();
        foreach($shows as $key_date=>$val_date){
        	if(array_key_exists($key_date, $episodes)){
        		foreach($val_date as $key_time=>$val_time){
	        		if(array_key_exists($key_time, $episodes[$key_date])){
	        			$val_time->episode_id          = $episodes[$key_date][$key_time]->episode_id;
	        			$val_time->date                = $episodes[$key_date][$key_time]->date;
	        			$val_time->episode_time        = $episodes[$key_date][$key_time]->episode_time;
	        			$val_time->schedules_id        = $episodes[$key_date][$key_time]->schedules_id;
	        			$val_time->episode_description = $episodes[$key_date][$key_time]->episode_description;
	        			$val_time->Episode__title      = $episodes[$key_date][$key_time]->Episode__title;
	        		}
	        	}
        	}
        }
        return $shows;

        /*$r = array();
        foreach ($shows as $date => $timeData) {
            
            foreach ($timeData as $time => $data) {
                
                $r[$date][$time] = $data;
            
                if (isset($episodes[$date][$time]['Episode'])) {
                    $r[$date][$time]['Episode'] = $episodes[$date][$time]['Episode'];
                }
            }
        }*/
        
        //return $r;                
    }

     /**
     * Get channel schedule (shows assoicated to days dates and start times) 
     * 
     * @param Int $channelId
     * @param String $from          date period begin (ex. 2104-07-03) 
     * @param String $to            date period end (ex. 2014-07-20) 
     * @param Strign $hostId 
     * @param Bool $fullDetails     if true read also show/episode description and host id/name
     * 
     * @returns Array         Schedule as array:
     *                          [date][time] = Schow/Schedule(/Replay) data
     *                          ex.
     *                          ['2014-07-30']['13:00'] = array('Schedule' => array(), 'Show' => array(), 'ReplayOf' => array())
     */
    public function getShowsForPeriod($channelId, $from, $to = null, $hostId = null, $guestId = null, $fullDetails = false, $scheduleId=null) {

        //$shows = $this->get($channelId, null, $fullDetails, $hostId); // moved down - run for each day of week
    	
        $_shows = array();
        $day = $from;
        if ($to === null) {
            $to = $from;
        }
        while ($day <= $to) 
        {
            $timestamp = strtotime($day);
            $dow   = date("D", $timestamp); // day of week  ex. 'Mon'        
            $date  = $this->dow2date($dow, $day);   // full date ex. "2014-07-07"
            //echo $date."<br>";

            $shows = $this->get($channelId, $date, $fullDetails, $hostId, $guestId, $scheduleId, $to);
            //echo "HERE CHECK Shows<pre>";print_R($shows);
            if (isset($shows[$dow]))
            {
                foreach ($shows[$dow] as $k => $v) 
				{
					/*echo $day."<br>";
					echo $shows[$dow][$k]->start_date;
					die;*/
                	if(!(strtotime($day) >= strtotime($shows[$dow][$k]->start_date) && strtotime($day) <= strtotime($shows[$dow][$k]->end_date)))
                	{
                    	unset($shows[$dow][$k]);
                	}
				}
				$_shows[$date] = $shows[$dow]; // convert array key - day of week to day date
            }
            // go to next day 
            $day = date("Y-m-d", strtotime('+1 day', $timestamp));
        }
        //echo "<pre>";print_R($_shows);die;
        return $_shows;
    }

    /**
     * Convert week day name to MySQL date relative to given date / Today 
     * 
     * @param String $day
     * @param String $referenceDate     reference date value (Now by default) 
     */
    private function dow2date($day = 'Mon', $refrerenceDate = null, $format = "Y-m-d", $firstDayOfWeek = null) {
       
        if ($firstDayOfWeek === null) {
            $firstDayOfWeek = $this->_firstDayOfWeek;
        }
       
        if (!in_array($day, array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'))) {  // date given as "Y-m-d" 
            return $day;        
        }        
    
        if ($refrerenceDate === null) {
            $refrerenceDate = time();
        } else {
            $refrerenceDate = strtotime($refrerenceDate);
        }

        $days = array();
        if (date("D", $refrerenceDate) !== $firstDayOfWeek) {
            $d = strtotime('last ' . $firstDayOfWeek, $refrerenceDate);
        } else {
            $d = $refrerenceDate;
        }

        $days[date("D", $d)] = date($format, $d);
        for ($i = 0; $i < 6; $i++) {
            $d = strtotime('+1 day', $d);
            $days[date("D", $d)] = date($format, $d);
        }
        
        return $days[ucwords(strtolower($day))];
    }

    /**
     * Convert given date to day of week
     * 
     * @param String $date      date as any acceptable String date 
     * 
     * @return String           day of week name (ex. 'Sun')
     * 
     */
    private function date2dow($date) {

        $timestamp = strtotime($date);
        $dow = date("D", $timestamp); // day of week  ex. 'Mon' 
        return $dow;
    }


    /**
     * Get channel weekly schedule (shows assoicated to day of weeks and start times) 
     * 
     * @param Int $channelId        only for given channel
     * @param Int $hostId           only for given host 
     * @param String $day           only for particular day given as: day of week (three letters - ex 'Mon') OR date (ex. 2014-08-10); if false all week days (for current week) 
     * @param Bool $fullData        read full data including Episode/Show title, description, Hosts(s) and Guest(s) names 
     * 
     * @returns Array         Schedule as array:
     *                          [dow][time] = Schow/Schedule(/Replay) data
     *                          ex. 
     *                          ['Mon']['13:00'] = array('Schedule' => array(), 'Show' => array(), 'ReplayOf' => array())
     * 
     */
    public function get($channelId = null, $day = null, $fullData = false, $hostId = null, $guestId = null, $scheduleId=null) {
                
        //if (!count($this->_channelSchedule[$channelId])) { // not cached 
        // additional conditions 
        $fields = array();
        $conditions = '';       

        // for given channel only          
        /*if ($channelId !== null) {
            $conditions['Schedule.channels_id'] = $channelId;
        }
		*/
		// exactly schedule id
        if ($scheduleId) {
            $conditions .= 'AND Schedule.id="'.$scheduleId.'"';
        }
        // for given, single day only
        if ($day !== null) {
        	//echo $day."<br>";
        	// date given - convert to day of week
            if (!in_array($day, array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'))) { 
                $episodeDate = $day;
                $day = $this->date2dow($day);
            } else {
                $episodeDate = $this->dow2date($day);
            }
            /*// Add Schedule DOW rule
            $conditions['Schedule.dow'] = $day;*/
        }
		
		if ($hostId) {
            $conditions .= ' AND Show.id IN (SELECT ShowsHasHosts.shows_id FROM shows_has_hosts ShowsHasHosts WHERE ShowsHasHosts.hosts_id="'.$hostId.'")';
        }
        // convert to format: [dow][hour] = show data 
        // echo $channelId." == ".$day."<br>";
        // echo "date ==== ".$episodeDate."<br>";
       
        $data = DB::select( DB::raw("SELECT `Schedule`.`id` as `schedule_id`, `Schedule`.`dow` as `schedule_dow`, `Schedule`.`time` as `schedule_time` , `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`type_label`, `Schedule`.`stream` as `schedule_stream`, `Schedule`.`file` as `schedule_file`, `Schedule`.`channels_id` as `schedule_channels_id`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id` as `show_id`, `Show`.`name`, `Show`.`is_online`, `Show`.`is_encore`, `Show`.`stream` as `show_stream`, `Show`.`file` as `show_file`, `Show`.`description` as `show_description`, `Episode`.`id` as `episode_id`, `Episode`.`shows_id`, `Episode`.`title`, `Episode`.`stream` as `episode_stream`, `Episode`.`file` as `episode_file`, `Episode`.`description` as `episode_description`, `Episode`.`date`, `Episode`.`time` as `episode_time`, `Episode`.`channels_id` as `episode_channels_id`, `Episode`.`is_cohost_override_banner`, `Episode`.`is_cohost_override_show_title`, `Episode`.`cohost_show_id`, `Episode`.`cohost_show_name`, `ReplayOf`.`dow` as `replay_dow`, `ReplayOf`.`time` as `replay_time`, `ReplayOf`.`id` as `replay_id`, `ReplayOf`.`schedules_id`, `ReplayOf`.`channels_id` as `replay_channels_id`, `ReplayOf`.`stream` as `replay_stream`, `ReplayOf`.`file` as `replay_file`, (SELECT COUNT(*) FROM schedules Schedules WHERE `Schedules`.`channels_id`=`ReplayOf`.`channels_id` AND `Schedules`.`dow`=`ReplayOf`.`dow` AND `Schedules`.`time`=`ReplayOf`.`time`) AS `scheduled_shows_count` FROM `schedules` AS `Schedule` LEFT JOIN `schedules` AS `ReplayOf` ON (`ReplayOf`.`id` = `Schedule`.`schedules_id` AND `ReplayOf`.`deleted` = 0) LEFT JOIN `shows` AS `Show` ON (`Show`.`id` = `Schedule`.`shows_id` AND `Show`.`deleted` = 0 AND `Show`.`is_online` = 1) LEFT JOIN `episodes_has_schedules` AS `EpisodesHasSchedule` ON (`EpisodesHasSchedule`.`date` = '$episodeDate' AND `EpisodesHasSchedule`.`time` = `Schedule`.`time` AND `EpisodesHasSchedule`.`schedules_id` = `Schedule`.`id`) LEFT JOIN `episodes` AS `Episode` ON (( `Episode`.`date`='$episodeDate' ) AND `Episode`.`time`=`Schedule`.`time` AND (`Schedule`.`id` = `Episode`.`schedules_id` OR `Schedule`.`schedules_id` = `Episode`.`schedules_id` OR `EpisodesHasSchedule`.`episodes_id`=`Episode`.`id`) AND `Episode`.`is_online` = 1) LEFT JOIN `shows_has_hosts` AS `ShowsHasHost` ON (`ShowsHasHost`.`shows_id` = `Show`.`id`) LEFT JOIN `hosts` AS `Host` ON (`Host`.`id` = `ShowsHasHost`.`hosts_id`) LEFT JOIN `channels` AS `Channel` ON (`Schedule`.`channels_id` = `Channel`.`id`)  WHERE `Schedule`.`deleted` = '0' AND `Show`.`deleted` = 0 AND `Show`.`is_online` = '1' AND `Schedule`.`channels_id` = $channelId AND FIND_IN_SET('$day', `Schedule`.`dow`) ".$conditions." GROUP BY `Schedule`.`id` ORDER BY DATE_FORMAT(`Schedule`.`dow`,'%w') ASC, `Schedule`.`time` ASC"));
       
        
        // Handle mulitple shows scheduled for this same day of week, but different weeks, months, years;        
        //
        //echo "<pre>";print_R($data);
        $_shows = array();
       
        // First group by dow and time to check is there one or more shows scheduled for single time slot 
      	//echo "daataaas<pre>";print_R($data);
      	
        foreach ($data as $key=>$d) {
        	
            $dows = array();
            if(!empty($d->schedule_dow)){
            	$dows = explode(",", $d->schedule_dow);
            }
            foreach ($dows as $dow) {
            	
            	/*if($d->schedule_id == 3565){
            		echo "here mine <pre>";print_r($d);
            	}*/
            	$time = substr($d->schedule_time, 0, -3);
            	
	            if (!isset($_shows[$dow][$time])) {
	                $_shows[$dow][$time] = array();
	            }	            
	            $new_arr = (array) $d; 
	            $new_arr['schedule_dow'] = $dow;
	            $_shows[$dow][$time][] = (object) $new_arr;  
            }
        }
        
        $_data = array();
       
        // Get only the right shows depending on current date and time
        // ( There can be two or more shows assigned to this scheduled time slot ) 
        //echo "<pre>";print_R($_shows);
        foreach ($_shows as $dow => $timeScheduledShows) {
            foreach ($timeScheduledShows as $time => $scheduledShows) {
            	//echo $episodeDate;
            	//echo "<pre>";print_R($scheduledShows);
                $_data[] = $this->getScheduledShow($scheduledShows, $episodeDate);
            }
        }

        //echo "HERE DATA <pre>";print_R($_data);
        // Prepare right data 
        $shows = array();
        //foreach ($data as $d) {
        if(!empty($_data)){
	        foreach ($_data as $d) {
	        	if(!empty($d)){		        	
		            // Remove seconds form Mysql time format 
		            $dow = $d->schedule_dow;
		            $time = substr($d->schedule_time, 0, -3);
		            
		            //fb($dow.' '.$time);		            
		            //if (!$fullData) {   // get only base data 
		                $shows[$dow][$time] = $d;
		            //} else {
		                //$shows[$dow][$time] = array_replace_recursive($d, $this->getShowEpisode($channelId, $this->_dow2date($d['Schedule']['dow']), $d['Schedule']['time'], $d));
		            //}

		            if (isset($d->replay_dow)) { // this is replay 
		                                
		                //$replayDate = $this->_dow2date($d['ReplayOf']['dow'], $this->_dow2date($d['Schedule']['dow']));
		                $replayDate = $this->dow2date($d->replay_dow, $episodeDate);
		                $replayTime = substr($d->replay_time, 0, -3);            
		               
		                if ($d->scheduled_shows_count>1){ 
		                	// This is Replay mixed with other shows or replays 
		                    
		                    // ---------------------------------------------------------                
		                    //
		                    // there is more than single show scheduled to date and time which should be replayed
		                    //                
		                    
		                    // get the right one show depending each week of month, month or year is on given date/period 
		                    $scheduledShowsForReplay = DB::select( DB::raw("SELECT  `Schedule`.`id` as `schedule_id`, `Schedule`.`dow` as `schedule_dow`, `Schedule`.`time` as `schedule_time` , `Schedule`.`week`, `Schedule`.`month`, `Schedule`.`year`, `Schedule`.`duration`, `Schedule`.`type`, `Schedule`.`type_label`, `Schedule`.`stream` as `schedule_stream`, `Schedule`.`file` as `schedule_file`, `Schedule`.`channels_id` as `schedule_channels_id`, `Schedule`.`start_date`, `Schedule`.`end_date`, `Show`.`id` as `show_id`, `Show`.`name`, `Show`.`is_online`, `Show`.`is_encore`, `Show`.`stream` as `show_stream`, `Show`.`file` as `show_file`, `Show`.`description` as `show_description` FROM `schedules` AS `Schedule` LEFT JOIN `shows` AS `Show` ON (`Show`.`id` = `Schedule`.`shows_id` AND `Show`.`deleted` = 0) LEFT JOIN `channels` AS `Channel` ON (`Schedule`.`channels_id` = `Channel`.`id`)  WHERE `Schedule`.`channels_id` = ".$d->replay_channels_id." AND `Schedule`.`dow` = '".$d->replay_dow."' AND `Schedule`.`time` = '".$d->replay_time."' AND `Schedule`.`deleted` = '0'"));
		                
		                    $scheduledShowForReplay = $this->getScheduledShow($scheduledShowsForReplay, $replayDate);

		                    if(!empty($scheduledShowForReplay)){
		                    	if (property_exists($scheduledShowForReplay, 'show_id')) {   
			                    	// show to be replayed exists  
			                    
			                        // The show to be replayed exists for given week period                   
			                        $shows[$dow][$time]->name = $scheduledShowForReplay->name;
			                        $shows[$dow][$time]->show_stream = $scheduledShowForReplay->show_stream;
			                        $shows[$dow][$time]->show_file = $scheduledShowForReplay->show_file;
			                       
			                        // remove seconds from Replayed show time 
			                        $shows[$dow][$time]->replay_time = $replayTime;
			                        if(property_exists($shows[$dow][$time], 'replay_date')){
		                            	$shows[$dow][$time]->replay_date = $replayDate;
		                            }else{
		                            	$shows[$dow][$time]->{"replay_date"} = $replayDate;
		                            }
			                        $shows[$dow][$time]->replay_channels_id = $scheduledShowForReplay->schedule_channels_id;
			                        $shows[$dow][$time]->replay_stream = $scheduledShowForReplay->schedule_stream;
			                        $shows[$dow][$time]->replay_file = $scheduledShowForReplay->schedule_file;
			                        
			                    } else { // show to be replayed don't exists in given week period 
			                        unset($shows[$dow][$time]);
			                        // This can heappend when 1st week shows is defined and nothing else at this time slot on next weeks 
			                    }
		                    }
		                    
		                    
		                } else {    // this is only single replay  
		                    $shows[$dow][$time]->replay_time = $replayTime;
		                    $shows[$dow][$time]->{"replay_date"} = $replayDate;
		                }
		                
		                if (/*isset($shows[$dow][$time]) && // show to replay defined for given day of week and exists */
		                        $replayDate > $episodeDate   // original show and replay on this same WEEK period (Sun - Sat), but replay date is BEFORE original show date 
		                        || ($replayDate == $episodeDate && $replayTime >= $time)) { // original show and replay on this same DAY, but replay time is before original show time 
		                    
		                    //                
		                    // This is the repaly of show episode NOT from given week period (Sun - Sat), but from previous week period 
		                    // ( OR this smae day, but replay is BEFORE orginal show Episode ) 
		                    // Move back one week (exactly 7 days) and get replayed show date (replay time stays this same) 
		                    //    
		                    //fb($replayDate.' - '.$replayTime);
		                
		                    $replayDate = date("Y-m-d", strtotime($replayDate.' - 7days'));
		                    
		                    // change replay date 
		                    $shows[$dow][$time]->replay_date = $replayDate;
		                    $shows[$dow][$time]->{"orginalShowDate"} = date("m/d", strtotime($replayDate));
		                   
		                    // Re-read show details!!!! - in case when multiple shows assigned for back date there can be other show than initially scheduled (for current week) 
		                    
		                    if ( $d->scheduled_shows_count > 1 ) {

		                        // there is more than single show scheduled to date and time which should be replayed
		                                        
		                        $scheduledShowForReplay = $this->getScheduledShow($scheduledShowsForReplay, $replayDate);
		                       
		                        if (isset($scheduledShowForReplay->show_id)) { 
		                            
		                            // The show to be replayed exists for given week period

		                            //$shows[$dow][$time]['Schedule'] = $scheduledShowForReplay['Schedule'];
		                            $shows[$dow][$time]->show_id = $scheduledShowForReplay->show_id;
		                            $shows[$dow][$time]->name = $scheduledShowForReplay->name;
		                            $shows[$dow][$time]->is_online = $scheduledShowForReplay->is_online;
		                            $shows[$dow][$time]->is_encore = $scheduledShowForReplay->is_encore;
		                            $shows[$dow][$time]->show_stream = $scheduledShowForReplay->show_stream;
		                            $shows[$dow][$time]->show_file = $scheduledShowForReplay->show_file;
		                            
		                            //Schedule data
		                            $shows[$dow][$time]->schedule_id = $d->schedule_id;
		                            $shows[$dow][$time]->schedule_dow = $d->schedule_dow;
		                            $shows[$dow][$time]->schedule_time = $d->schedule_dow;
		                            $shows[$dow][$time]->week = $d->week;
		                            $shows[$dow][$time]->month = $d->month;
		                            $shows[$dow][$time]->year = $d->year;
		                            $shows[$dow][$time]->duration = $d->duration;
		                            $shows[$dow][$time]->type = $d->type;
		                            $shows[$dow][$time]->type_label = $d->type_label;
		                            $shows[$dow][$time]->schedule_stream = $d->schedule_stream;
		                            $shows[$dow][$time]->schedule_file = $d->schedule_file;
		                            $shows[$dow][$time]->schedule_file = $d->schedule_file;
		                            $shows[$dow][$time]->schedule_channels_id = $d->schedule_channels_id;
		                            $shows[$dow][$time]->start_date = $d->start_date;
		                            $shows[$dow][$time]->end_date = $d->end_date;

		                            // remove seconds from Replayed show time 
		                            $shows[$dow][$time]->replay_time = $replayTime;
		                            if(property_exists($shows[$dow][$time], 'replay_date')){
		                            	$shows[$dow][$time]->replay_date = $replayDate;
		                            }else{
		                            	$shows[$dow][$time]->{"replay_date"} = $replayDate;
		                            }
		                           
		                            $shows[$dow][$time]->replay_dow = date("D", strtotime($replayDate));

		                            if(property_exists($shows[$dow][$time], 'orginalShowDate')){
		                            	$shows[$dow][$time]->orginalShowDate = date("m/d", strtotime($replayDate));
		                            }else{
		                            	$shows[$dow][$time]->{"orginalShowDate"} = date("m/d", strtotime($replayDate));
		                            }

		                            $shows[$dow][$time]->replay_channels_id = $scheduledShowForReplay->schedule_channels_id;
		                            $shows[$dow][$time]->replay_stream = $scheduledShowForReplay->schedule_stream;
		                            $shows[$dow][$time]->replay_file = $scheduledShowForReplay->schedule_file; 
		                           
		                        } else { 
		                        	// show to be replayed don't exists in given week period
		                            unset($shows[$dow][$time]);
		                            // This can heappend when 1st week shows is defined and nothing else at this time slot on next weeks 
		                        }
		                    } 
		                }
		            } // end of Replay     
	        	}
	        }
    	}

        //if ($day == null) {                

        $this->_channelSchedule[$channelId] = $shows;

        //} else {
        //$this->_channelSchedule[$channelId][$this->_date2dow($day)] = $shows; // cache 
        //}
        //}

        return $this->_channelSchedule[$channelId];
        //return $day === null ? $this->_channelSchedule[$channelId] : @$this->_channelSchedule[$channelId][$this->_date2dow($day)];
    }


    /**
     * From set of shows assigned to this same schedule time slot (Day Of Week  and  Start Time) 
     * get the right one (matching target date).
     * !!! This same method is on Channel.Schedule too.
     * 
     * @param Array $shows              array of shows scheduled for episode date and time 
     *                                  (empty, one or more rows) 
     * 
     * @param String $episodeDate       target show episode date (YYYY-MM-DD)       
     * 
     * @return array                    right for given date scheduled show details 
     */
    public function getScheduledShow($shows, $episodeDate) {
        // re-index all scheduled shows by mask: YYYYMMW (YYYY - year, MM - month, W - number of week in month 1 ... 5 ) 
        
        $_shows = array();

        foreach ($shows as $show) 
        {        	
        	$weeks = array();        	
        	if(!empty($show->week)){
        		$weeks = explode(",", $show->week);
        	}

        	if(!empty($weeks)){
        		foreach($weeks as $week){
        			if((strtotime($episodeDate) >= strtotime($show->start_date) && strtotime($episodeDate) <= strtotime($show->end_date)))
		            {
		            	$new_arr = (array) $show; 
		            	$show->week = $week;
			            $new_arr['week'] = $week;			  
		                $_shows[($show->year > 0 ? $show->year : '0000') . str_pad($show->month, 2, "0", STR_PAD_LEFT) . $week] = $show;
		            }
        		}
        	}else{
        		$week = 0;
        		if((strtotime($episodeDate) >= strtotime($show->start_date) && strtotime($episodeDate) <= strtotime($show->end_date)))
	            {
	            	$new_arr = (array) $show; 
			        $new_arr['week'] = $week;
	                $_shows[($show->year > 0 ? $show->year : '0000') . str_pad($show->month, 2, "0", STR_PAD_LEFT) . $week] = (object) $new_arr;
	            }
        	}           
        }
        
        // Mask target show episode date syntax: YYYYMMW (YYYY - year, MM - month, W - number of week in month 1 ... 5 ) 
        $year  = date("Y", strtotime($episodeDate));
        $month = date("m", strtotime($episodeDate));
        $week  = ceil( date( 'j', strtotime( $episodeDate ) ) / 7 );
        
        // Masks definitions (ORDER IS IMPORTANT) 
        $masks = array(
            $year.$month.$week, $year.$month.'0', $year.'00'.$week, 
            '0000'.$month.$week,
            $year.'000', '0000'.$month.'0', '000000'.$week, 
            '0000000'
        );

        // Return first matching show (for first matching mask found)
        foreach ($masks as $m) {
            if (isset($_shows[$m])) {
                return $_shows[$m];
            }
        }
        return array();
    }

    /**
     * Get channel Episodes (only) for given channel and period 
     * 
     * @param Int $channelId
     * @param String $from          date period begin (ex. 2104-07-03) 
     * @param String $to            date period end (ex. 2014-07-20) 
     * @param Bool $fullDetails     if true read also show/episode description and host and guest(s) id/name
     * 
     * @returns Array         Episodes schedule as array:
     *                          [date][time] = Schow/Schedule(/Replay) data
     *                          ex.
     *                          ['2014-07-30']['13:00'] = array('Episode' => array(...))
     */
    public function getEpisodesForReplays($shows=array(), $fullDetails=false) {
        
        // prepare required Epsiodes conditions 
        $conditions = '';
        
        foreach ($shows as $date => $timeScheduledShows) {
            /*if($date == "2019-04-18"){
            	//echo "<pre>";print_R($timeScheduledShows);

            	foreach ($timeScheduledShows as $time => $s) {
                
	                if (property_exists($s, 'replay_date')) {  // replays (to get Episode's Hosts and Guests)     
	                    $conditions .= ($conditions!==''?' OR':'').(' (Episode.channels_id="'.$s->replay_channels_id.'" AND Episode.date="'.$s->replay_date.'" AND Episode.time="'.$s->replay_time.':00")');
	                    
	                }
	            }
            }*/
            foreach ($timeScheduledShows as $time => $s) {
                
                if (property_exists($s, 'replay_date')) {  // replays (to get Episode's Hosts and Guests)     
                    $conditions .= ($conditions!==''?' OR':'').(' (Episode.channels_id="'.$s->replay_channels_id.'" AND Episode.date="'.$s->replay_date.'" AND Episode.time="'.$s->replay_time.':00")');
                    
                }
            }
        }       
       
        $episodes = array();
      	
        if ($conditions !=='' ) {
      
            $data =  DB::select( DB::raw("SELECT `Episode`.`id` as `episode_id`, `Episode`.`date`, `Episode`.`time` as `episode_time`, `Episode`.`schedules_id`, `Episode`.`description` as `episode_description`, (CASE WHEN TRIM(`Episode`.`title`)!='' THEN (CASE WHEN TRIM(`Episode`.`title`)!=TRIM(`Show`.`name`) THEN CASE WHEN `Episode`.`is_cohost_override_show_title`>0 THEN CONCAT( `Episode`.`cohost_show_name`, ': ', `Episode`.`title`) ELSE CONCAT( `Show`.`name`, ': ', `Episode`.`title`) END  ELSE `Show`.`name` END) ELSE `Show`.`name` END) AS  `Episode__title` FROM `episodes` AS `Episode` LEFT JOIN `schedules` AS `Schedule` ON (`Episode`.`schedules_id` = `Schedule`.`id`) LEFT JOIN `shows` AS `Show` ON (`Episode`.`shows_id` = `Show`.`id`) LEFT JOIN `channels` AS `Channel` ON (`Episode`.`channels_id` = `Channel`.`id`)  WHERE ".$conditions." GROUP BY `Episode`.`id`"));

            // convert to format: [date][hour] = show/episode data 
            $_episodes = array();
            foreach ($data as $e) {
                $_episodes[$e->date][substr($e->episode_time, 0, -3)] = $e;
            }
            
            foreach ($shows as $date => $timeScheduledShows) {
                foreach ($timeScheduledShows as $time => $s) {  
                    if ( isset($s->replay_date) && isset($_episodes[$s->replay_date][$s->replay_time]) ) {
                        $episodes[$date][$time] = $_episodes[$s->replay_date][$s->replay_time];
                    }
                }
            }
        }
        //echo "<pre>";print_r($episodes);
        return $episodes;
    }

    /**
     * Build FUllcalender single Event object
     * 
     * @param String $startTime
     * @param Int $duration
     * @param Array $data
     * @param Array $eventData
     * 
     * @return Array
     */
    private function addCalEvent($startTime = null, $duration = null, $data = array(), $eventData = array()) {

        $dstart = strtotime($startTime);
        $dstop = $dstart + (int) $duration * 60; // $duration in minutes 

        return array(
            'id' => $data['id'],
            'title' => $data['title'],
            //'backgroundColor' => $data['backgroundColor'],
            //'borderColor' => $data['borderColor'],
            'className' => $data['className'],
            //'eventTextColor' => '#fff',
            //'eventBackgroundColor' => $bg,
            //'eventBorderColor' => $bg,
            //'eventColor' => $bg,
            'start' => '' . date("Y", $dstart) . '-' . date("m", $dstart) . '-' . date("d", $dstart) . ' ' . date("H", $dstart) . ':' . date("i", $dstart) . '',
            'end' => '' . date("Y", $dstop) . '-' . date("m", $dstop) . '-' . date("d", $dstop) . ' ' . date("H", $dstop) . ':' . date("i", $dstop) . '',
            'allDay' => false,
            // custom row data 
            'data' => $eventData
        );
    }

     /**
     * Delete all outdated schedules;
     * All schedules which has set year and ( month and/or week) 
     */
    public function deleteOutdatedSchedules() {
        
        $postponeDays = 7;
        $currYear = date('Y');
        $currYearMonth = date('Y-m');
        $currWeekNum = date('W') - date('W', strtotime(date('Y-m-01'))) + 1;
      
        $rows = Schedule::where('year', '>',  0)->where('deleted', 0)->get();
        $toDeleteIds = array();
        foreach ($rows as $key => $d) {
            
            $scheduleShowMonth = str_pad($d->month, 2, "0", STR_PAD_LEFT);
            
            if ( ($d->week == 0 && $d->month == 0) ||
                    ($d->week > 0 && $d->month == 0) ) {
                
                // excuted each DayOfWeek for whole year (ex. each Monday of 2016); no week/month defined                
                // OR
                // executed some week of all months for whole year (ex. 1st week of each month of 2016); week defined, month not defined 
                if ($currYear > $d->year) {
                
                    if (time() > strtotime($d->year.'-12-31') + $postponeDays *24*60*60) { // include postpone days
                        
                        $toDeleteIds[] = $d->id;
                    }
                }
            } elseif ($d->week == 0 && $d->month > 0) {
                
                // excuted each DayOfWeek for given month and year (ex. each Monday of January of 2015)                
                if ($currYearMonth > $d->year.'-'.$scheduleShowMonth) {
                
                    $lastDayOfMonth = date('t', strtotime($d->year.'-'.$scheduleShowMonth.'-01'));
                    if (time() > strtotime($d->year.'-'.$scheduleShowMonth.'-'.$lastDayOfMonth) + $postponeDays *24*60*60) { // include postpone days 
                        
                        $toDeleteIds[] = $d->id;
                    }
                }
            } else {
                
                // executed once on given day of week for given week, month (and year) defined 
                // ex. Monday on second week of June 2016 
                if (($currYearMonth > $d->year.'-'.$scheduleShowMonth) || // next month
                    // this month, but next week number 
                    ( $currYearMonth == $d->year.'-'.$scheduleShowMonth &&
                        $currWeekNum > $d->week ) ) {                    
                    
                    if (time() > strtotime($this->weekNumber2String($d).' '.$d->dow.' of '.date('F', strtotime($d->year.'-'.$scheduleShowMonth)).' '.$d->year) + $postponeDays *24*60*60) {
                        
                        $toDeleteIds[] = $d->id;
                    }
                }                
            }
        }

        // Delete outdated schedules
        if(!empty($toDeleteIds)){
        	foreach($toDeleteIds as $schedule_id){
        		Schedule::where(['id'=>$schedule_id, 'deleted'=>0])->update(['deleted'=>1]);
        	}
        	return true;
        }else{
        	return false;
        }
               
       /* if ($this->updateAll(array('Schedule.deleted' => 1), array('Schedule.id' => $toDeleteIds))) {   
            return true;
        } else {            
            return false;
        }*/
    }

    private function weekNumber2String($d) {
        
        switch ($d->week) {
            
            case 1:
                    return 'first';
                break;
            case 2:
                    return 'second';
                break;
            case 3:
                    return 'third';
                break;
            case 4:
                    return 'fourth';
                break;
            case 5:
                    return 'fifth';
                break;
        }
    }	
    /**
     * Get the schedules of the shows under which episode has assigned
     * @param $shows_id is the show id of the episode
     */
    public function getSchedulesOfEpisodeShow($shows_id){
    	
    	$schedules = $this->select('schedules.*')->leftJoin('shows', 'schedules.shows_id', '=', 'shows.id')
				->leftJoin('channels', 'channels.id', '=', 'schedules.channels_id')
                    ->where('schedules.shows_id', $shows_id)
                    ->where('schedules.deleted', 0)
                    ->where('schedules.type', '!=', 'replay')
                    ->orderBy('schedules.time', 'asc')
                    ->get();
                    
    	return $schedules;
    }
   
}
