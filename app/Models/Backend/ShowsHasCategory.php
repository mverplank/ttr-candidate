<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ShowsHasCategory
 * 
 * @property int $shows_id
 * @property int $categories_id
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\Show $show
 *
 * @package App\Models\Backend
 */
class ShowsHasCategory extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;
	protected $fillable = [
			'shows_id',
			'categories_id',
		];
	protected $casts = [
		'shows_id' => 'int',
		'categories_id' => 'int'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Backend\Category::class, 'categories_id');
	}

	public function show()
	{
		return $this->belongsTo(\App\Models\Backend\Show::class, 'shows_id');
	}
	
	public function add($new_arr){
		if(!empty($new_arr)){
			$obj = new ShowsHasCategory($new_arr);
			$obj->save();
		}
	}	
	
}
