<?php

/**
 * @author: Contriverz
 * @since :  Wed, 26 Dec 2018 09:24:16 +0000.
 */

namespace App\Models\Backend;
use App\Models\Backend\User;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Models\Backend\Activity;
use Auth;
use Carbon;
use DB;
/**
 * Class Host
 * 
 * @property int $id
 * @property int $users_id
 * @property int $default_channel_id
 * @property int $cohost_types_id
 * @property bool $is_online
 * @property bool $is_online_global
 * @property string $bio
 * @property string $pr_type
 * @property string $pr_name
 * @property string $pr_company_name
 * @property string $pr_phone
 * @property string $pr_cellphone
 * @property string $pr_skype
 * @property string $pr_skype_phone
 * @property string $pr_email
 * @property bool $deleted
 * @property string $itunes_categories
 * @property string $itunes_title
 * @property string $itunes_subtitle
 * @property string $itunes_desc
 * @property string $tags
 * @property string $type
 * @property bool $cohost_option_override_host_banners
 * @property bool $is_feedback_email_notification
 * @property string $feedback_notification_email
 * @property bool $is_featured
 * @property int $off
 * @property int $favorites
 * @property bool $is_podcasthost_only
 * 
 * @property \App\Models\CohostType $cohost_type
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $host_contents
 * @property \Illuminate\Database\Eloquent\Collection $host_itunes
 * @property \Illuminate\Database\Eloquent\Collection $host_testimonials
 * @property \Illuminate\Database\Eloquent\Collection $hosts_has_host_types
 * @property \Illuminate\Database\Eloquent\Collection $sponsors
 * @property \Illuminate\Database\Eloquent\Collection $shows
 * @property \Illuminate\Database\Eloquent\Collection $urls
 * @property \Illuminate\Database\Eloquent\Collection $videos
 *
 * @package App\Models\Backend
 */
class Host extends Eloquent
{
	
	public $timestamps = true;
	
	protected $casts = [
		'users_id' => 'int',
		'default_channel_id' => 'int',
		'cohost_types_id' => 'int',
		'is_online' => 'int',
		'is_online_global' => 'bool',
		'deleted' => 'bool',
		'cohost_option_override_host_banners' => 'bool',
		'is_feedback_email_notification' => 'bool',
		'is_featured' => 'bool',
		'off' => 'int',
		'favorites' => 'int',
		'is_podcasthost_only' => 'bool',
		'is_guest' => 'bool',
		'itunes_categories' => 'object'
	];
	
	protected $dates = [
		'updated_at',
		'created_at'
	];
	
	protected $fillable = [
		'users_id',
		'default_channel_id',
		'cohost_types_id',
		'is_online',
		'is_online_global',
		'profile_url',
		'bio',
		'pr_type',
		'pr_name',
		'pr_company_name',
		'pr_phone',
		'pr_cellphone',
		'pr_skype',
		'pr_skype_phone',
		'alternate_email',
		'pr_email',
		'zoom_link',
		'deleted',
		'itunes_categories',
		'itunes_title',
		'itunes_subtitle',
		'itunes_desc',
		'tags',
		'type',
		'cohost_option_override_host_banners',
		'is_feedback_email_notification',
		'feedback_notification_email',
		'is_featured',
		'off',
		'favorites',
		'is_podcasthost_only',
		'is_guest',
		'updated_at',
		'created_at',
		'updated_by'
	];

	public function cohost_type()
	{
		return $this->belongsTo(\App\Models\Backend\CohostType::class, 'cohost_types_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_hosts', 'hosts_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->belongsToMany(\App\Models\Backend\Episode::class, 'episodes_has_hosts', 'hosts_id', 'episodes_id')
					->withPivot('id', 'off', 'type');
	}

	public function host_contents()
	{
		return $this->hasMany(\App\Models\Backend\HostContent::class, 'hosts_id');
	}

	public function host_itunes()
	{
		return $this->hasMany(\App\Models\Backend\HostItune::class, 'hosts_id');
	}

	public function host_testimonials()
	{
		return $this->hasMany(\App\Models\Backend\HostTestimonial::class, 'hosts_id');
	}

	public function hosts_has_host_types()
	{
		return $this->hasMany(\App\Models\Backend\HostsHasHostType::class, 'hosts_id');
		//return $this->belongsToMany(\App\Models\Backend\HostType::class, 'hosts_has_host_types','hosts_id', 'host_types_id');
	}

	public function sponsors()
	{
		return $this->belongsToMany(\App\Models\Backend\Sponsor::class, 'hosts_has_sponsors', 'hosts_id', 'sponsors_id');
	}

	public function shows()
	{
		return $this->belongsToMany(\App\Models\Backend\Show::class, 'shows_has_hosts', 'hosts_id', 'shows_id')
					->withPivot('id', 'off', 'type');
	}

	public function urls()
	{
		return $this->hasMany(\App\Models\Backend\Url::class, 'hosts_id');
	}

	public function videos()
	{
		return $this->hasMany(\App\Models\Backend\Video::class, 'hosts_id');
	}

	// Fetch all hosts with id for the filters
	public function getHostsForFilter($type='',$operation=''){
		$host_obj = Host::select(DB::raw('`hosts`.`id`, `profiles`.`name`, TRIM(CONCAT( `profiles`.`lastname`, " ", `profiles`.`firstname` )) as `fullname`'))
						->join('profiles', 'profiles.users_id','=', 'hosts.users_id');
		if(!empty($type)){
			$host_obj = $host_obj->join('hosts_has_host_types','hosts_has_host_types.hosts_id','=','hosts.id')->where('hosts_has_host_types.host_types_id',$type);
		}
		if(!empty($operation) && $operation == "import"){

			$host_obj = $host_obj->whereHas('user', function($query)
							{
							    $query->where('is_guest', 0);   	  
							});
		}
		
		$host_obj = $host_obj->where('hosts.deleted', 0)->orderBy('fullname', 'asc')
						    ->pluck('profiles.fullname', 'hosts.id');
		return $host_obj;
	}

	/**
	* Count all the hosts
	* @return result as a total
	*/
	public function countAllHosts($type="", $channel_id=0, $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$hosts = $this;
		if(!empty($type)){
            $hosts = $hosts->leftJoin('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('host_types_id', '=', $type);
		}

		if(!empty($channel_id)){
			$hosts = $hosts->leftJoin('channels_has_hosts', 'channels_has_hosts.hosts_id', '=', 'hosts.id')->where('channels_id', '=', $channel_id);
		}
		//Fetch who are not deleted
		$hosts = $hosts->leftJoin('users', 'users.id', '=', 'hosts.users_id')->where('users.deleted', 0);
		// Overall Search 
        if(!empty($search_value)){
           
    		// Join for searching the host name from profiles
			$hosts = $hosts->leftJoin('profiles', 'profiles.users_id', '=', 'hosts.users_id');
			$hosts = $hosts->where(function($q)  use ($search_value){
				        $q->orWhere('hosts.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%');
				          //->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
				          //->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%');
				   });   						
        }

        // Sorting by column
        if($order != null){
            $hosts = $hosts->orderBy($order, $dir);
        }
		$hosts = $hosts->count();
		return $hosts;
	}


	/**
	* Fetches all the hosts
	* @return result in the form of object
	*/
	public function getAllHosts($type="", $channel_id=0, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null){

		$hosts = $this->select('hosts.*', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix', 'profiles.name', 'hosts.is_online', 'users.status', 'media.id AS media_id', 'media.filename AS media_file', 'media.type AS media_type')
					->leftJoin('media_linking', function ($join) {
			            $join->on('media_linking.module_id', '=', 'hosts.id')
			            ->where('media_linking.main_module', '=', 'Host')
			            ->where('media_linking.sub_module', '=', 'photo');
			        })->leftJoin('media', 'media_linking.media_id', '=', 'media.id');

		if(!empty($type)){
            $hosts = $hosts->join('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')->where('hosts_has_host_types.host_types_id', '=', $type);
		}
		if(!empty($channel_id)){
			$hosts = $hosts->join('channels_has_hosts', 'channels_has_hosts.hosts_id', '=', 'hosts.id')->where('channels_has_hosts.channels_id', '=', $channel_id);
		}

		//Fetch who are not deleted
		$hosts = $hosts->join('users', 'users.id', '=', 'hosts.users_id')->where('users.deleted', 0);

		//Fetch name of the host
		$hosts = $hosts->join('profiles', 'profiles.users_id', '=', 'hosts.users_id');

		// Overall Search 
        if(!empty($search_value)){
          
            // Join for searching the host name from profiles
			//$hosts = $hosts->join('profiles', 'profiles.users_id', '=', 'hosts.users_id');
			$hosts = $hosts->where(function($q) use ($search_value){
				        $q->orWhere('hosts.bio' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
				          ->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%');
				          //->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
				          //->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%');
				   });
        }

        // Sorting by column
        if($order != null){
            $hosts = $hosts->orderBy($order, $dir);
        }
        $hosts = $hosts->offset($start)->limit($length)->get();
		return $hosts;
	}
	/**
	 * @description Addng the Host
	 * @param $data is the $request object
	 */	
	public function add($data){
		$currentdate = date('Y-m-d H:i:s');
		try{
			
			$data['User']['password'] = bcrypt($data['User']['password']);
			$user = new User($data['User']);
			$user->save();

			// Save data to profiles
			try{
				$data['Profile']['users_id'] = $user->id;
				$data['Profile']['name'] = $data['Profile']['firstname']." ".$data['Profile']['lastname'];
				$profile = new Profile($data['Profile']);
				$profile->save();
			
				// Save data to Host
				try{
					if(isset($data['ItunesCategory'])){
						$data['Host']['itunes_categories'] = $data['ItunesCategory'];
					}
					$host = new Host($data['Host']);
					
					if($user->hosts()->save($host)){
						// Add Transaction Report
						try{
							$fullname = $profile->title . ' ' . $profile->firstname . ' ' . $profile->lastname . ' ' . $profile->sufix;
							$newactivity = array(
								'created'=>$currentdate,
								'users_id'=> Auth::id(),
								'activity' =>'ADDED HOST "'. $fullname .'" (ID:' . $host->id .')'
							);
							$activity = new Activity();								
							$activity->add($newactivity);
						}
						catch(\Exception $e){
							//echo $e->getMessage();die('transaction');
							return false;
						}

						$media = new MediaLinking();
						$media->add($host->id, 'Host');
			        }
					// Save data to host types
					try{
						$host_types_objects_arr = array();
						if(isset($data['HostType'])){
							if(!empty($data['HostType'])){
								if(!empty($data['HostType']['HostType'])){
									foreach($data['HostType']['HostType'] as $item){
										$obj = array('hosts_id'=>$host->id , 'host_types_id'=>$item);
										$host_types_objects_arr[] = new HostsHasHostType($obj);
									}
									if(!empty($host_types_objects_arr)){
										$host->hosts_has_host_types()->saveMany($host_types_objects_arr);
									}
								}
							}
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('type');
						return false;
				    }
				    // Save data to Urls
					try{
						$urls_objects_arr = array();
						if(isset($data['Url'])){
							if(!empty($data['Url'])){
								foreach($data['Url'] as $item){
									$urls_objects_arr[] = new Url($item);
								}
								if(!empty($urls_objects_arr)){
									$host->urls()->saveMany($urls_objects_arr);
								}
							}
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('url');
						return false;
				    }

				    // Save data to Videos
					try{
						$videos_objects_arr = array();
						if(isset($data['Video'])){
							if(!empty($data['Video'])){
								foreach($data['Video'] as $item){
									if($item['title'] ==''){
										$item['is_enabled'] ='0';
									}
									$videos_objects_arr[] = new Video($item);
								}
								if(!empty($videos_objects_arr)){
									$host->videos()->saveMany($videos_objects_arr);
								}
							}
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('video');
						return false;
				    }

				    // Save data to UserSettings
					try{
						if(isset($data['UserSetting'])){
							if(!empty($data['UserSetting'])){
								$user_setting_objects_arr = new UserSetting($data['UserSetting']);
								$user->user_settings()->save($user_setting_objects_arr);
							}
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('user setting');
						return false;
				    }

				    // Save data to channels_has_hosts
					try{
						if(isset($data['Channel'])){
							if(!empty($data['Channel'])){
								if(!empty($data['Channel']['Channel'])){
									foreach($data['Channel']['Channel'] as $item){
										$host->channels()->attach($item);
									}
								}
							}
						}
					}
					catch(\Exception $e){
						//echo $e->getMessage();die('channel');
						return false;
				    }
				}catch(\Exception $e){
					//echo $e->getMessage();die('host');
					return false;
				}
			}
			catch(\Exception $e){ 
				//echo $e->getMessage();die('profiles');
				return false;
		    }
		    return true;		    
		}
		catch(\Exception $e){
			//echo $e->getMessage();die('user');
			return false;
	    }
	}

	/**
	 * @description Edit the Host
	 * @param where $id is host_id and $data is the $request object for updating the object
	 */	
	public function edit($id=0, $data){
		$currentdate = date('Y-m-d H:i:s');
		if($id != 0){
			if(!empty($data)){
				try{
					
					// Update data to Host
					$remove_host_type = array();
					$add_host_type = array();
					$html  = '';
					$html1 = '';
					$host  = Host::find($id);
					//print_r($data['Host']['is_online']) ; die;
					if(isset($data['ItunesCategory'])){
						$data['Host']['itunes_categories'] = $data['ItunesCategory'];
					}
					//updated by
					$data['Host']['updated_by'] = Auth::id();
					$host->update($data['Host']);	

					// Update data to host types
				    try{
				    	//Fetch if exists the host types
				    	$old_host = array();
						$old_host_types = $host->hosts_has_host_types()->get();
						if($old_host_types->count() > 0){
				            foreach($old_host_types as $host_type){
				                $old_hosts[] = $host_type->host_types_id;
				            }
				        }	    	
				    
				    	$host_types_objects_arr = array();
				    	if(isset($data['HostType'])){
							if(!empty($data['HostType'])){
								if(!empty($data['HostType']['HostType'])){									
							        //When there exists some host types 
							        if(!empty($old_hosts)){
							        	foreach($old_hosts as $host_type){
											if(!in_array($host_type, $data['HostType']['HostType'])){
												$del_host_types_object = HostsHasHostType::where('host_types_id' , '=', $host_type)->where('hosts_id', '=', $host->id)->delete();
												array_push($remove_host_type,$host_type);
											}
										}
										foreach($data['HostType']['HostType'] as $item){
											if(!in_array($item, $old_hosts)){
												$obj = array('hosts_id'=>$host->id , 'host_types_id'=>$item);
												$host_types_objects_arr[] = new HostsHasHostType($obj);
												array_push($add_host_type,$item);
											}
										}
							        }
							        else{
							        	//When there does not exist some host types 
							        	foreach($data['HostType']['HostType'] as $item){
											$obj = array('hosts_id'=>$host->id , 'host_types_id'=>$item);
											$host_types_objects_arr[] = new HostsHasHostType($obj);
											array_push($add_host_type,$item);
										}
							        }							       
									// Add host types
									if(!empty($host_types_objects_arr)){
										$host->hosts_has_host_types()->saveMany($host_types_objects_arr);
									}
								}
							}
						}else{
							//If no new new host types the delete all the old ones.
							if(!empty($old_hosts)){
					        	foreach($old_hosts as $host_type){				
									$del_host_types_object = HostsHasHostType::where('host_types_id' , '=', $host_type)->where('hosts_id', '=', $host->id)->delete();	
										array_push($remove_host_type,$host_type);		
								}
					        }
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('host types');*/
						return false;
				    }

					// Update data to users
					try{
						if(isset($data['User']['password'])){
							$data['User']['password'] = bcrypt($data['User']['password']);
						}
						$user = User::find($data['User']['id']);
						$user->update($data['User']);
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('user');*/
						return false;
				    }

					// Update data to profiles
					try{
						$profile = Profile::find($data['Profile']['id']);
						$profile->update($data['Profile']);

						//Add Transaction Report
						try{
							
							if(!empty($remove_host_type)){
								$html ='Removed host type(';
								foreach($remove_host_type as $host_type){
									if($host_type == '1'){
										$html .= 'HOST,';
									}
									if($host_type == '2'){
										$html .= 'CO-HOST,';
									}
									if($host_type == '3'){
										$html .= 'PODCAST-HOST,';
									}
								}

								$html .=')';
							}

							if(!empty($add_host_type)){
								$html1 ='Added host type(';
								foreach($add_host_type as $host_type){
									if($host_type == '1'){
										$html1 .= 'HOST,';
									}
									if($host_type == '2'){
										$html1 .= 'CO-HOST,';
									}
									if($host_type == '3'){
										$html1 .= 'PODCAST-HOST,';
									}
								}
									$html1 .=')';
							}
							$activity =$html1.$html;
							$fullname = $profile->title . ' ' . $profile->firstname . ' ' . $profile->lastname . ' ' . $profile->sufix;

							if(empty($activity)){
								$activity ='UPDATED HOST "'. $fullname .'" (ID:' . $host->id .')';
							}else{
								$activity .=$fullname .'" (ID:' . $host->id .')';
							}

							$newactivity = array(
								'created'=>$currentdate,
								'users_id'=> Auth::id(),
								'activity' =>$activity
							);
							$activity = new Activity();								
							$activity->add($newactivity);
						}
						catch(\Exception $e){
							// echo $e->getMessage();die();
							return false;
						}

					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('profile');*/
						return false;
				    }

					// Update data to Urls
					try{	
						//Fetch if exists the social urls
				    	$old_urls_arr = array();
						$old_urls = $host->urls()->get();
						if($old_urls->count() > 0){
				            foreach($old_urls as $url){
				                $old_urls_arr[] = $url->id;
				            }
				        }
				    
						$urls_objects_arr = array();
						if(isset($data['Url'])){
							if(!empty($data['Url'])){
								
								foreach($data['Url'] as $item){
									if(!empty($old_urls_arr)){
										//If new video link is added
										if(isset($item['id'])){
											if($item['id'] > 0){
												if(!in_array($item['id'], $old_urls_arr)){
													$urls_objects_arr[] = new Url($item);
												}else{
													//Else update the existing social url
													$urlObj = Url::find($item['id']);
													$urlObj->update($item);
													if (($key = array_search($item['id'], $old_urls_arr)) !== false){
													    unset($old_urls_arr[$key]);
													}
												}
											}
										}else{
											$urls_objects_arr[] = new Url($item);
										}								
									}else{
										if(isset($item['id'])){
											if($item['id'] > 0){
												$url = Url::find($item['id']);
												$url->update($item);
											}
										}else{
											$urls_objects_arr[] = new Url($item);
										}
									}
								}
								if(!empty($urls_objects_arr)){
									$host->urls()->saveMany($urls_objects_arr);
								}
								// Delete the removed sociall urls while editing
								if(!empty($old_urls_arr)){
									foreach($old_urls_arr as $old_url){
										$del_url_obj = Url::find($old_url);
										$del_url_obj->delete();
									}
								}
							}
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('urls');*/
						return false;
				    }

					// Update data to Videos
					try{	
						//Fetch if exists the video links
				    	$old_videos_arr = array();
						$old_videos = $host->videos()->get();
						if($old_videos->count() > 0){
				            foreach($old_videos as $video){
				                $old_videos_arr[] = $video->id;
				            }
				        }

						$videos_objects_arr = array();
						if(isset($data['Video'])){
							if(!empty($data['Video'])){
								foreach($data['Video'] as $item){
									if(!isset($item['is_enabled'])){
										$item['is_enabled'] = 0;
									}
									if($item['title'] ==''){
										$item['is_enabled'] ='0';
									}
									if(!empty($old_videos_arr)){
										//If new video link is added
										if(isset($item['id'])){
											if($item['id'] > 0){
												if(!in_array($item['id'], $old_videos_arr)){
													$videos_objects_arr[] = new Video($item);
												}else{
													//Else update the existing video link
													$videoObj = Video::find($item['id']);
													$videoObj->update($item);
													if (($key = array_search($item['id'], $old_videos_arr)) !== false){
													    unset($old_videos_arr[$key]);
													}
												}
											}
										}else{
											$videos_objects_arr[] = new Video($item);
										}									
										
									}else{
										if(isset($item['id'])){
											if($item['id'] > 0){
												$video = Video::find($item['id']);
												$video->update($item);
											}
										}else{
											$videos_objects_arr[] = new Video($item);
										}
									}
								}
								if(!empty($videos_objects_arr)){
									$host->videos()->saveMany($videos_objects_arr);
								}
								// Delete the removed video links while editing
								if(!empty($old_videos_arr)){
									foreach($old_videos_arr as $old_video){
										$del_videos_obj = Video::find($old_video);
										$del_videos_obj->delete();
									}
								}
							}
						}
					}
					catch(\Exception $e){
						// echo $e->getMessage();
						// die('video');
						return false;
				    }

					// Update data to UserSettings
				    try{
				    	
						if(isset($data['UserSetting'])){
							if(!empty($data['UserSetting'])){
								if(!empty($data['UserSetting']['membership_end'])){
									$data['UserSetting']['membership_end'] = Carbon::parse($data['UserSetting']['membership_end'])->format('Y-m-d');
								}

								if(isset($data['UserSetting']['id'])){
									if($data['UserSetting']['id'] > 0){			
										if(!empty($data['UserSetting']['membership_end'])){
											$data['UserSetting']['membership_end'] = date_format(date_create($data['UserSetting']['membership_end']), "Y-m-d");
										}
										// if(!isset($data['UserSetting']['studioapp_enabled'])){
										// 	$data['UserSetting']['studioapp_enabled'] = 0;
										// }
										$user_setting = UserSetting::find($data['UserSetting']['id']);
										$user_setting->update($data['UserSetting']);					
									}else{
										
										$user_setting_objects_arr = new UserSetting($data['UserSetting']);	
										$user->user_settings()->save($user_setting_objects_arr);
									}
								}else{
									$user_setting_objects_arr = new UserSetting($data['UserSetting']);
									$user->user_settings()->save($user_setting_objects_arr);
								}
							}
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('settings');*/
						return false;
				    }

				    // Update data to channels_has_hosts
				    try{
				    	$old_channels = array();
						$old_host_channels = $host->channels()->get();
						if($old_host_channels->count() > 0){
				            foreach($old_host_channels as $channel){
				                $old_channels[] = $channel->id;
				            }
				        }	    	
				    	
				    	if(isset($data['Channel'])){
							if(!empty($data['Channel'])){
								if(!empty($data['Channel']['Channel'])){									
							        
							        if(!empty($old_channels)){
							        	foreach($old_channels as $chn){
											if(!in_array($chn, $data['Channel']['Channel'])){
												$host->channels()->detach($chn);
											}
										}
							        }
							       
									foreach($data['Channel']['Channel'] as $item){
										if(!in_array($item, $old_channels)){
											$host->channels()->attach($item);
										}
									}
								}
							}
						}else{
							if(!empty($old_channels)){
					        	foreach($old_channels as $chn){
									$host->channels()->detach($chn);									
								}
					        }
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('channels');*/
						return false;
				    }

					return true;
				}catch(\Exception $e){
					/*echo $e->getMessage();
					die('end datat');*/
					return false;
			    }
			}
		}else{
			return false;
		}
	}

	/**
	 * @description Edit the Host Profile
	 * @param where $id is user id of the host and $data is the $request object for updating the object
	 */	
	public function edit_profile($id=0, $data){
		
		if($id != 0){
			if(!empty($data)){
				try{
					//echo $id;
					//echo "<pre>";print_R($data);die('success');
					// Update data to User
					$user_obj = User::find($id);
					if(isset($data['User']['password'])){
						$data['User']['password'] = bcrypt($data['User']['password']);
					}
					$user_obj->update($data['User']);

					// Update data to profiles
					try{
						$profile = Profile::find($data['Profile']['id']);
						$profile->update($data['Profile']);
					}
					catch(\Exception $e){
						return false;
					}

					// Update data to Host
					$host_id = $host_id = array('hosts_id' =>$data['Host']['id']);
					$host = Host::find($data['Host']['id']);
					if(isset($data['ItunesCategory'])){
						$data['Host']['itunes_categories'] = $data['ItunesCategory'];
					}
					//updated by
					$data['Host']['updated_by'] = $id;
					$host->update($data['Host']);
					
					// Update data to host types
				    try{
				    	//Fetch if exists the host types
				    	$old_host = array();
						$old_host_types = $host->hosts_has_host_types()->get();
						if($old_host_types->count() > 0){
				            foreach($old_host_types as $host_type){
				                $old_hosts[] = $host_type->host_types_id;
				            }
				        }	    	
				    
				    	$host_types_objects_arr = array();
				    	if(isset($data['HostType'])){
							if(!empty($data['HostType'])){
								if(!empty($data['HostType']['HostType'])){									
							        //When there exists some host types 
							        if(!empty($old_hosts)){
							        	foreach($old_hosts as $host_type){
											if(!in_array($host_type, $data['HostType']['HostType'])){
												$del_host_types_object = HostsHasHostType::where('host_types_id' , '=', $host_type)->where('hosts_id', '=', $host->id)->delete();
											}
										}
										foreach($data['HostType']['HostType'] as $item){
											if(!in_array($item, $old_hosts)){
												$obj = array('hosts_id'=>$host->id , 'host_types_id'=>$item);
												$host_types_objects_arr[] = new HostsHasHostType($obj);
											}
										}
							        }
							        else{
							        	//When there does not exist some host types 
							        	foreach($data['HostType']['HostType'] as $item){
											$obj = array('hosts_id'=>$host->id , 'host_types_id'=>$item);
											$host_types_objects_arr[] = new HostsHasHostType($obj);
										}
							        }							       
									// Add host types
									if(!empty($host_types_objects_arr)){
										$host->hosts_has_host_types()->saveMany($host_types_objects_arr);
									}
								}
							}
						}else{
							//If no new new host types the delete all the old ones.
							if(!empty($old_hosts)){
					        	foreach($old_hosts as $host_type){				
									$del_host_types_object = HostsHasHostType::where('host_types_id' , '=', $host_type)->where('hosts_id', '=', $host->id)->delete();			
								}
					        }
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('host types');*/
						return false;
				    }

					// Update data to Urls
					try{	
						//Fetch if exists the social urls
				    	$old_urls_arr = array();
						$old_urls = $host->urls()->get();
						if($old_urls->count() > 0){
				            foreach($old_urls as $url){
				                $old_urls_arr[] = $url->id;
				            }
				        }
				    
						$urls_objects_arr = array();
						if(isset($data['Url'])){
							if(!empty($data['Url'])){
								
								foreach($data['Url'] as $item){
									if(!empty($old_urls_arr)){
										//If new video link is added
										if(isset($item['id'])){
											if($item['id'] > 0){
												if(!in_array($item['id'], $old_urls_arr)){
													$urls_objects_arr[] = new Url($item);
												}else{
													//Else update the existing social url
													$urlObj = Url::find($item['id']);
													$urlObj->update($item);
													if (($key = array_search($item['id'], $old_urls_arr)) !== false){
													    unset($old_urls_arr[$key]);
													}
												}
											}
										}else{
											$urls_objects_arr[] = new Url($item);
										}								
									}else{
										if(isset($item['id'])){
											if($item['id'] > 0){
												$url = Url::find($item['id']);
												$url->update($item);
											}
										}else{
											$urls_objects_arr[] = new Url($item);
										}
									}
								}
								if(!empty($urls_objects_arr)){
									$host->urls()->saveMany($urls_objects_arr);
								}
								// Delete the removed sociall urls while editing
								if(!empty($old_urls_arr)){
									foreach($old_urls_arr as $old_url){
										$del_url_obj = Url::find($old_url);
										$del_url_obj->delete();
									}
								}
							}
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('urls');*/
						return false;
				    }

					// Update data to Videos
					try{	
						//Fetch if exists the video links
				    	$old_videos_arr = array();
						$old_videos = $host->videos()->get();
						if($old_videos->count() > 0){
				            foreach($old_videos as $video){
				                $old_videos_arr[] = $video->id;
				            }
				        }

						$videos_objects_arr = array();
						if(isset($data['Video'])){
							if(!empty($data['Video'])){
								foreach($data['Video'] as $item){
									if(!isset($item['is_enabled'])){
										$item['is_enabled'] = 0;
									}
									if(!empty($old_videos_arr)){
										//If new video link is added
										if(isset($item['id'])){
											if($item['id'] > 0){
												if(!in_array($item['id'], $old_videos_arr)){
													$videos_objects_arr[] = new Video($item);
												}else{
													//Else update the existing video link
													$videoObj = Video::find($item['id']);
													$videoObj->update($item);
													if (($key = array_search($item['id'], $old_videos_arr)) !== false){
													    unset($old_videos_arr[$key]);
													}
												}
											}
										}else{
											$videos_objects_arr[] = new Video($item);
										}									
										
									}else{
										if(isset($item['id'])){
											if($item['id'] > 0){
												$video = Video::find($item['id']);
												$video->update($item);
											}
										}else{
											$videos_objects_arr[] = new Video($item);
										}
									}
								}
								if(!empty($videos_objects_arr)){
									$host->videos()->saveMany($videos_objects_arr);
								}
								// Delete the removed video links while editing
								if(!empty($old_videos_arr)){
									foreach($old_videos_arr as $old_video){
										$del_videos_obj = Video::find($old_video);
										$del_videos_obj->delete();
									}
								}
							}
						}
					}
					catch(\Exception $e){
					/*	echo $e->getMessage();
						die('video');*/
						return false;
				    }

					// Update data to UserSettings
				    try{
						if(isset($data['UserSetting'])){
							if(!empty($data['UserSetting'])){
								if(isset($data['UserSetting']['id'])){
									if($data['UserSetting']['id'] > 0){										
										if(!empty($data['UserSetting']['membership_end'])){
											$data['UserSetting']['membership_end'] = date_format(date_create($data['UserSetting']['membership_end']), "Y-m-d");
										}
										// if(!isset($data['UserSetting']['studioapp_enabled'])){
										// 	$data['UserSetting']['studioapp_enabled'] = 0;
										// }
										$user_setting = UserSetting::find($data['UserSetting']['id']);
										$user_setting->update($data['UserSetting']);						
									}
								}else{
									$user_setting_objects_arr = new UserSetting($data['UserSetting']);
									$user->user_settings()->save($user_setting_objects_arr);
								}
							}
						}
					}
					catch(\Exception $e){
						// echo $e->getMessage();
						// die('settings');
						return false;
				    }

				    // Update data to channels_has_hosts
				    try{
				    	$old_channels = array();
						$old_host_channels = $host->channels()->get();
						if($old_host_channels->count() > 0){
				            foreach($old_host_channels as $channel){
				                $old_channels[] = $channel->id;
				            }
				        }	    	
				    	
				    	if(isset($data['Channel'])){
							if(!empty($data['Channel'])){
								if(!empty($data['Channel']['Channel'])){									
							        
							        if(!empty($old_channels)){
							        	foreach($old_channels as $chn){
											if(!in_array($chn, $data['Channel']['Channel'])){
												$host->channels()->detach($chn);
											}
										}
							        }
							       
									foreach($data['Channel']['Channel'] as $item){
										if(!in_array($item, $old_channels)){
											$host->channels()->attach($item);
										}
									}
								}
							}
						}else{
							if(!empty($old_channels)){
					        	foreach($old_channels as $chn){
									$host->channels()->detach($chn);									
								}
					        }
						}
					}
					catch(\Exception $e){
						/*echo $e->getMessage();
						die('channels');*/
						return false;
				    }
					try{
						$old_testimonials = array();
						$old_host_testimonials = $host->host_testimonials()->get();

						//Get all old Testimonials
						if($old_host_testimonials->count() > 0){
				            foreach($old_host_testimonials as $testimonials){
				                $old_testimonials[] = $testimonials->id;
				            }
				        }	
				        $new_testimonials = array();
						if(isset($data['HostTestimonial'])){
							if(!empty($data['HostTestimonial'])){
					        	foreach($data['HostTestimonial'] as $test){
					        		//print_r($test);die();
					        		$final_ar = array_merge($test,$host_id);
					        		if(!empty($test['id'])){
					        			$new_testimonials[] = $test['id'];
					        			HostTestimonial::updateOrCreate(['id'=> $test['id']],$final_ar);
									}else{
										//echo"hjfdfdg";die();
										HostTestimonial::create($final_ar);
								  	}
								}
								if(!empty($old_testimonials)){
						        	foreach($old_testimonials as $tes){
										if(!in_array($tes, $new_testimonials)){
											$detete_tes = HostTestimonial::find($tes);
											$detete_tes->delete();
										}
									}
						        }
							}
						}					

					}catch(\Exception $e){	
					// echo $e->getMessage();
					// 	die('testimonial');					
						return false;
					}
					try{
						//Fetch old host content if any
						//die('dcksdcksdjc');
						$old_contents = array();
						$old_host_contents = $host->host_contents()->get();
						if($old_host_contents->count() > 0){
				            foreach($old_host_contents as $contents){
				                $old_contents[] = $contents->id;
				            }
				        }	

				        $new_contents = array();						
						if(isset($data['HostContent'])){
							//echo "<pre>";print_R($data['HostContent']);die;
							if(!empty($data['HostContent'])){
					        	foreach($data['HostContent'] as $key=>$cont){
						        	// Add Host id here
					        		$final_arr = array_merge($cont,$host_id);
					        		if(!empty($cont['id'])){
					        			//die('ifff');
					        			// Update the existing host content
					        			$new_contents[] = $cont['id'];
					        			HostContent::updateOrCreate(['id'=> $cont['id']],$cont);
					        			//echo "<pre>";print_R($final_arr);die;
					        			//$this->procesHostContentImage($final_arr, 'HostContent');
					        		}else{
					        			
					        			// Add the new host content
					        			//HostContent:: create($final_arr);
					        			foreach ($final_arr as $index => $dat) {
										    if ($index == 'id') {
										        unset($final_arr[$index]);
										        break;
										    }
										}
					        			$new_content = new HostContent($final_arr);
					        			$new_content->save();
					        			$final_arr['id'] = $new_content->id;
					        			//echo "<pre>";print_R($final_arr);
					        			//$this->procesHostContentImage($final_arr, 'HostContent');
					        			$media = new MediaLinking();
										$media->add($new_content->id, 'HostContent');

					        		}
								}
							}
							//echo "<pre>";print_R($old_contents);
				        	//echo "<pre>";print_R($new_contents);die;

							if(!empty($old_contents)){
				        		foreach($old_contents as $con){

									if(!in_array($con, $new_contents)){
										$detete_tes = HostContent::find($con);
										$detete_tes->delete();
										// Delete image also
										$new_path = 'HostContent/'.$con;
							            $new_exists = Storage::disk('public')->has($new_path);
										//echo $new_exists;
							            //If folder exists then delete
							            if($new_exists){
							                Storage::disk('public')->deleteDirectory($new_path);
							            }
									}
								}
				        	}
						}	
						//return true;					
					}catch(\Exception $e){
						// echo $e->getMessage();echo "<br>";die('content');
						return false;
					}

					return true;
				}catch(\Exception $e){
					return false;
			    }
			}
		}else{
			return false;
		}
	}
	/**
	 * @description Edit the Host Profile Custom Content
	 * @param where $id is user id of the host and $data is the $request object for updating the object
	 */	

	//this is commented because this function code merge to edit_profile function	
	
	public function edit_custom_content($id=0, $data){
		//echo"<pre>";print_r($data); die();
		if($id != 0){
			if(!empty($data)){
				try{
					$host = Host::find($id);
					$host_id = array('hosts_id' =>$id );
					try{
						$old_testimonials = array();
						$old_host_testimonials = $host->host_testimonials()->get();

						//Get all old Testimonials
						if($old_host_testimonials->count() > 0){
				            foreach($old_host_testimonials as $testimonials){
				                $old_testimonials[] = $testimonials->id;
				            }
				        }	
				        $new_testimonials = array();
						if(isset($data['HostTestimonial'])){
							if(!empty($data['HostTestimonial'])){
					        	foreach($data['HostTestimonial'] as $test){
					        		//print_r($test);die();
					        		$final_ar = array_merge($test,$host_id);
					        		if(!empty($test['id'])){
					        			$new_testimonials[] = $test['id'];
					        			HostTestimonial::updateOrCreate(['id'=> $test['id']],$final_ar);
									}else{
										//echo"hjfdfdg";die();
										HostTestimonial::create($final_ar);
								  	}
								}
								if(!empty($old_testimonials)){
						        	foreach($old_testimonials as $tes){
										if(!in_array($tes, $new_testimonials)){
											$detete_tes = HostTestimonial::find($tes);
											$detete_tes->delete();
										}
									}
						        }
							}
						}					

					}catch(\Exception $e){						
						return false;
					}
					try{
						//Fetch old host content if any
						//die('dcksdcksdjc');
						$old_contents = array();
						$old_host_contents = $host->host_contents()->get();
						if($old_host_contents->count() > 0){
				            foreach($old_host_contents as $contents){
				                $old_contents[] = $contents->id;
				            }
				        }	

				        $new_contents = array();						
						if(isset($data['HostContent'])){
							if(!empty($data['HostContent'])){
					        	foreach($data['HostContent'] as $cont){
						        	// Add Host id here
					        		$final_arr = array_merge($cont,$host_id);
					        		if(!empty($cont['id'])){
					        			//die('ifff');
					        			// Update the existing host content
					        			$new_contents[] = $cont['id'];
					        			HostContent::updateOrCreate(['id'=> $cont['id']],$cont);
					        			//echo "<pre>";print_R($final_arr);die;
					        			//$this->procesHostContentImage($final_arr, 'HostContent');
					        		}else{
					        			
					        			// Add the new host content
					        			//HostContent:: create($final_arr);
					        			foreach ($final_arr as $index => $dat) {
										    if ($index == 'id') {
										        unset($final_arr[$index]);
										        break;
										    }
										}
					        			$new_content = new HostContent($final_arr);
					        			$new_content->save();
					        			$final_arr['id'] = $new_content->id;
					        			//echo "<pre>";print_R($final_arr);
					        			$this->procesHostContentImage($final_arr, 'HostContent');
					        		}
								}
							}
							//echo "<pre>";print_R($old_contents);
				        	//echo "<pre>";print_R($new_contents);die;

							if(!empty($old_contents)){
				        		foreach($old_contents as $con){

									if(!in_array($con, $new_contents)){
										$detete_tes = HostContent::find($con);
										$detete_tes->delete();
										// Delete image also
										$new_path = 'HostContent/'.$con;
							            $new_exists = Storage::disk('public')->has($new_path);
										//echo $new_exists;
							            //If folder exists then delete
							            if($new_exists){
							                Storage::disk('public')->deleteDirectory($new_path);
							            }
									}
								}
				        	}
						}	
						//return true;					
					}catch(\Exception $e){
						//echo $e->getMessage();echo "<br>";die('content');
						return false;
					}
				}
				catch(\Exception $e){
					echo $e->getMessage();die('outer');
					return false;
				}
				//die('sdsdvsdvsdv');
				return true;
			}
		}else{
			return false;
		}
	}
  
	/**
	 * @description Delete the Host and its relational data 
	 * @param where $id is host_id
	 */	
	public function deleteHost($id=0){
		
		if($id != '' || $id !=0){

			$host = Host::find($id);
			$host->update(['deleted'=>1]);		
			if ($host->has('user')) {
				$check_users = $host->user()->get();
				if($check_users->count() > 0){
					foreach($check_users as $user)
					{
						$user = User::find($user->id);
						$user->update(['deleted'=>1]);
					}
				}	
			}
			
			/*if ($host->has('hosts_has_host_types')) {
			  	$host->hosts_has_host_types()->delete();
			}
			if ($host->has('channels')) {
			   $host->channels()->detach();
			}
			if ($host->has('urls')) {
			   $host->urls()->delete();
			}
			if ($host->has('videos')) {
			   $host->videos()->delete();
			}
			if ($host->has('user')) {
				
				$check_users = $host->user()->get();
				if($check_users->count() > 0){
					foreach($check_users as $user)
					{	
						
						$user->profiles()->delete();
					    $user->user_settings()->delete();
					  	//  $user->delete();
					}
				}
				$host->user()->delete();
			}
			
			return $host->delete();*/
			return true;
		}
	}

	/**
	* Export  hosts
	* @return result in the form of object
	*/
	public function eportHosts($type='',$channel_id='',$all='', $online='', $offline='', $featured=''){

			$hosts = User::join('profiles', 'users.id', '=', 'profiles.users_id')
						   ->select('profiles.firstname', 'profiles.lastname','users.email', 'profiles.phone', 'profiles.cellphone', 'profiles.skype', 'profiles.skype_phone');

			if(!empty($type)){
				if($type == "Both"){
					$hosts = $hosts->whereIn('hosts_has_host_types.host_types_id', [1,2]);

					}else{
					$hosts = $hosts->where('hosts_has_host_types.host_types_id', '=', $type);
					}
			    $hosts = $hosts->join('hosts', 'users.id', '=', 'hosts.users_id')
			    			   ->join('hosts_has_host_types', 'hosts_has_host_types.hosts_id', '=', 'hosts.id')
			    	           ->where('users.id', '!=', Auth::id())
			    	           ->where('users.deleted',0)
			    	           ->where('users.groups_id','=',2);
			}
			if(!empty($channel_id)){
				$hosts =  $hosts->join('hosts', 'users.id', '=', 'hosts.users_id')
								->join('channels_has_hosts', 'channels_has_hosts.hosts_id', '=', 'hosts.id')
								->where('channels_has_hosts.channels_id', '=', $channel_id)
								->where('users.id', '!=', Auth::id())
			    				->where('users.deleted',0)
			    				->where('users.groups_id','=',2);
			}
			if(!empty($online)){
				$hosts =  $hosts->join('hosts', 'users.id', '=', 'hosts.users_id')
								->where('users.id', '!=', Auth::id())
                   				->where('users.deleted',0)
                   				->where('users.groups_id','=',2)
                   				->where('hosts.is_online','=',1);
			}
			if(!empty($offline)){
				$hosts =  $hosts->join('hosts', 'users.id', '=', 'hosts.users_id')
								->where('users.id', '!=', Auth::id())
                   				->where('users.deleted',0)
                   				->where('users.groups_id','=',2)
                   				->where('hosts.is_online','=',0);
			}
			if(!empty($featured)){
				$hosts =  $hosts->join('hosts', 'users.id', '=', 'hosts.users_id')
								->where('users.id', '!=', Auth::id())
                   				->where('users.deleted',0)
                   				->where('users.groups_id','=',2)
                   				->where('hosts.is_featured','=',1);
			}									

			if(!empty($all)){
				$hosts =  $hosts->join('hosts', 'users.id', '=', 'hosts.users_id')
								->where('users.id', '!=', Auth::id())
                   				->where('users.deleted',0)
                   				->where('users.groups_id','=',2);
			}
				$hosts = $hosts->orderBy('profiles.lastname', 'asc')->get();
				return $hosts;
	}

	/**
    * Move All Host related images From Tmp Location to permananet folder
    * @param $object current object of the module and $module is the module name
    */
    public function procesImage($object, $module){
    
        $folders = ['player', 'zapbox', 'zapboxsmall', 'widget', 'cover', 'attachments', 'photo', 'itunes'];
        $userid = \Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }    
       
        return 'success';
    }

    /**
    * Move All Host related images From Tmp Location to permananet folder
    * @param $array current array of the module and $module is the module name
    */
    public function procesHostContentImage($array, $module){
    
        $userid = \Auth::user()->id;
        /*echo "<pre>";print_R($array);
        echo "<pre>";print_R($module);*/

        $arr = explode('image_', $array['offset']);
        $get_offset = $arr[1];
        $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$get_offset.'/image';

        //check if at tmp location folder and file exist
        $tmp_exists = Storage::disk('public')->has($tmp_path);
        if($tmp_exists){
        	
            //check if folder on new path exist
            $new_path = $module.'/'.$array['id'].'/image';
            $new_exists = Storage::disk('public')->has($new_path);

            //if new folder not exist then create new folder
            if(!$new_exists){
                Storage::disk('public')->makeDirectory($new_path);
            }
            //echo "YES";
            echo $tmp_path."<br>";
            $files = Storage::disk('public')->files($tmp_path);  
            echo "FILES<pre>";print_R($files);              
            if($files){
            	
                foreach($files as $file){
                    $file_name = explode($get_offset.'/image', $file);
                    Storage::disk('public')->move($file, $new_path.$file_name[1]);
                }
            }
        }

        return 'success';
    }
	// Get Host id of the curent user id
	public function getHostId($user_id){

		$host_id  = 0;
		$host_obj = Host::select('id')->where('users_id', $user_id)->first();
		
		if($host_obj){
			$host_id = $host_obj->id;
		}
		
		return $host_id;
	}

	/**
	 * Fetch all channels with id for the filters
	 * @param $host_id if the id of the host
	 * @return the objects of the channels
	 */
	public function getAllChannels($host_id){

		$host_obj = Host::find($host_id);
		$channel_obj = $host_obj->channels()
							->where('hosts_id', $host_id)
							->where('deleted', 0)
							->orderBy('name', 'asc')
							->pluck('name', 'id');

		return $channel_obj;
	}

	/** 
	 * Fetch the show names of the host
	 *
	 */
	public function getHostShowNames(){
		$show_names = array();
		$get_shows  = $this->shows()->get();
		if($get_shows->count() > 0){
			foreach($get_shows as $show){
				$show_names[] = $show->name;
			}
		}
		return $show_names;
	}

	/** 
	 * Fetch the default channeld id of the host
	 * @param $host id of the host table
	 */
	public function getDefaultChannelId($id=0){
		if($id != 0){
			$default_channel = $this->select('default_channel_id')->where('id', $id)->first();
			if($default_channel){
				return $default_channel->default_channel_id;
			}
			return false;
		}
		return false;
	}

	/** 
	 * Fetch the host detail using host id
	 * @param $id is id of the host table
	 */
	public function getHostDetailById($id=0){

		$host_full_info = array();
		if($id != 0 || $id != ''){
			$host_detail = $this->with('user.profiles')->with('urls')->with('videos')->where('id', $id )->first();
			if($host_detail){
				//Get data from host table
				$host_full_info['bio']             = $host_detail->bio;
				$host_full_info['pr_type']         = $host_detail->pr_type;
				$host_full_info['pr_name']         = $host_detail->pr_name;
				$host_full_info['pr_company_name'] = $host_detail->pr_company_name;
				$host_full_info['pr_phone']        = $host_detail->pr_phone;
				$host_full_info['pr_cellphone']    = $host_detail->pr_cellphone;
				$host_full_info['pr_skype']        = $host_detail->pr_skype;
				$host_full_info['pr_skype_phone']  = $host_detail->pr_skype_phone;
				$host_full_info['pr_email']        = $host_detail->pr_email;

				//Get data from users table
				$host_full_info['email']    = $host_detail->user->email;
				$host_full_info['users_id'] = $host_detail->user->id;
				$host_full_info['host_id']  = $host_detail->id;
				if( $host_detail->user->status == "active" ){
					$host_full_info['is_online'] = 1;
				}else{
					$host_full_info['is_online'] = 0;
				}

				//Get data from profiles table
				if($host_detail->user->profiles->count() > 0){
					foreach($host_detail->user->profiles as $profile){
						$host_full_info['title']       = $profile->title;
						$host_full_info['firstname']   = $profile->firstname;
						$host_full_info['lastname']    = $profile->lastname;
						$host_full_info['sufix']       = $profile->sufix;
						$host_full_info['phone']       = $profile->phone;
						$host_full_info['cellphone']   = $profile->cellphone;
						$host_full_info['skype']       = $profile->skype;
						$host_full_info['skype_phone'] = $profile->skype_phone;
					}
				}
				
				//Get data from urls table
				if($host_detail->urls->count() > 0){
					$count = 0;
					foreach($host_detail->urls as $url){
						$host_full_info['urls'][$count]['type'] = $url->type;
						$host_full_info['urls'][$count]['url']  = $url->url;
						$count++;
					}
				}
				
				//Get data from videos table
				if($host_detail->videos->count() > 0){
					$count = 0;
					foreach($host_detail->videos as $video){
						$host_full_info['videos'][$count]['title']      = $video->title;
						$host_full_info['videos'][$count]['code']       = $video->code;
						$host_full_info['videos'][$count]['is_enabled'] = $video->is_enabled;
						$count++;
					}
				}
				return ['status'=>'success', 'data'=>$host_full_info, 'msg'=>'Successfully fetched the host details'];
			}			 
			return ['status'=>'error', 'msg'=>'Host does not exist'];
		}else{
			return ['status'=>'error', 'msg'=>'Host does not exist'];
		}
	}
}
