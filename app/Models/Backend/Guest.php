<?php

/**
 * @author: Contriverz.
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use App\Models\Backend\Activity;
use Reliese\Database\Eloquent\Model as Eloquent;
use Auth;
use DB;
/**
 * Class Guest
 * 
 * @property int $id
 * @property int $users_id
 * @property int $guests_id
 * @property bool $deleted
 * @property string $title
 * @property string $sufix
 * @property string $firstname
 * @property string $lastname
 * @property string $name
 * @property string $bio
 * @property string $email
 * @property string $phone
 * @property string $cellphone
 * @property string $skype
 * @property string $skype_phone
 * @property string $pr_type
 * @property string $pr_name
 * @property string $pr_company_name
 * @property string $pr_phone
 * @property string $pr_cellphone
 * @property string $pr_skype
 * @property string $pr_skype_phone
 * @property string $pr_email
 * @property bool $is_online
 * @property bool $is_featured
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $off
 * @property int $favorites
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property \Illuminate\Database\Eloquent\Collection $guest_contents
 * @property \Illuminate\Database\Eloquent\Collection $guests_has_categories
 * @property \Illuminate\Database\Eloquent\Collection $urls
 * @property \Illuminate\Database\Eloquent\Collection $videos
 *
 * @package App\Models\Backend
 */
class Guest extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'users_id' => 'int',
		'hosts_id' => 'int',
		'deleted' => 'bool',
		'is_online' => 'bool',
		'is_featured' => 'bool',
		'off' => 'int',
		'favorites' => 'int'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'users_id',
		'created_by',
		'hosts_id',
		'deleted',
		'title',
		'sufix',
		'firstname',
		'lastname',
		'name',
		'bio',
		'email',
		'phone',
		'cellphone',
		'skype',
		'skype_phone',
		'pr_type',
		'pr_name',
		'pr_company_name',
		'pr_phone',
		'pr_cellphone',
		'pr_skype',
		'pr_skype_phone',
		'pr_email',
		'is_online',
		'is_featured',
		'created_at',
		'updated_at',
		'off',
		'favorites',
		'to_be_contacted'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Backend\Channel::class, 'channels_has_guests', 'guests_id', 'channels_id');
	}

	public function episodes()
	{
		return $this->belongsToMany(\App\Models\Backend\Episode::class, 'episodes_has_guests', 'guests_id', 'episodes_id')
					->withPivot('id', 'off');
	}

	public function guest_contents()
	{
		return $this->hasMany(\App\Models\Backend\GuestContent::class, 'guests_id');
	}

	public function guests_has_categories()
	{
		return $this->hasMany(\App\Models\Backend\GuestsHasCategory::class, 'guests_id');
	}

	public function urls()
	{
		return $this->hasMany(\App\Models\Backend\Url::class, 'guests_id');
	}

	public function videos()
	{
		return $this->hasMany(\App\Models\Backend\Video::class, 'guests_id');
	}

	/**
	 * @description Adding the Guest
	 * @param $data is the $request object
	 */	
	public function add($data){
		$currentdate = date('Y-m-d H:i:s');
		// Save data to guest
		try{
			//$data['User']['password'] = bcrypt($data['User']['password']);
			$user = new User($data['User']);
			$user->save();

			// Save data to profiles
			try{
				$data['Profile']['users_id'] = $user->id;
				$data['Profile']['name'] = $data['Profile']['firstname']." ".$data['Profile']['lastname'];
				$profile = new Profile($data['Profile']);
				$profile->save();			

				//Save data into Guest table
				$data['Guest']['users_id']   = $user->id;
				$data['Guest']['created_by'] = Auth::id();
				$data['Guest']['name'] = $data['Profile']['name'];
				// Check the guests who are to be contacted or not
				$data['Guest']['to_be_contacted'] = isset($data['Guest']['to_be_contacted']) ? $data['Guest']['to_be_contacted'] : 0;

				$guest = new Guest($data['Guest']);
				if($guest->save()){
					$media = new MediaLinking();
					$media->add($guest->id, 'Guest');
					// Add Transaction Report
					try{
						$fullname = $guest->title . ' ' . $guest->firstname . ' ' . $guest->lastname . ' ' . $guest->sufix;
						$newactivity = array(
							'created'=>$currentdate,
							'users_id'=>$data['Guest']['users_id'],
							'activity' =>'ADDED GUEST "'. $fullname .'" (ID:' . $guest->id .')'
						);
						$activity = new Activity();								
						$activity->add($newactivity);
					}
					catch(\Exception $e){
						return false;
					}			

					// Save data to Urls
					try{
						$urls_objects_arr = array();
						if(isset($data['Url'])){
							if(!empty($data['Url'])){
								foreach($data['Url'] as $item){
									$urls_objects_arr[] = new Url($item);
								}
								if(!empty($urls_objects_arr)){
									$chexk = $guest->urls()->saveMany($urls_objects_arr);
								}
							}
						}
					}
					catch(\Exception $e){
						return false;
				    }
				    // Save data to Videos
					try{
						$videos_objects_arr = array();
						if(isset($data['Video'])){
							if(!empty($data['Video'])){
								foreach($data['Video'] as $item){
									$videos_objects_arr[] = new Video($item);
								}
								if(!empty($videos_objects_arr)){
									$guest->videos()->saveMany($videos_objects_arr);
								}
							}
						}
					}
					catch(\Exception $e){
						return false;
				    }				
				}
			    
			    // Save data to guests_has_categories
				try{
					if(isset($data['Category']) && !empty($data['Category'])){
						if(!empty($data['Category']['Category'])){		
							foreach($data['Category']['Category'] as $cate){
								if(!empty($cate)){
									$newcate = array('guests_id'=>$guest->id, 'categories_id' => $cate );
									$guestcate = new GuestsHasCategory();								
									$guestcate->add($newcate);
								}
							}													
						}
					}
				}
				catch(\Exception $e){
					return false;
			    }

			    //Add data to User settings
			    $data['UserSetting']['users_id'] = $user->id;
			    $user_setting_objects_arr = new UserSetting($data['UserSetting']);
				$user->user_settings()->save($user_setting_objects_arr);
			}
			catch(\Exception $e){ 
				return false;
		    }
		    return true;

		}catch(\Exception $e){
			// echo $e->getMessage();
			// 			die('main');
			return false;
		}
	    return true;		    
	}

	/**
	 * @description Edit the guest
	 * @param where $id is guest_id and $data is the $request object for updating the object
	 */	
	public function edit($id=0, $data){
		$currentdate = date('Y-m-d H:i:s');
		if($id != 0){
			if(!empty($data)){
				try{
					
					// Update data to guest
					$data['Guest']['created_by'] = Auth::id();
					$data['Guest']['name'] = $data['Profile']['firstname']." ".$data['Profile']['lastname'];

					// Check the guests who are to be contacted or not
					$data['Guest']['to_be_contacted'] = isset($data['Guest']['to_be_contacted']) ? $data['Guest']['to_be_contacted'] : 0;
					
					$guest = Guest::find($id);
					$guest->update($data['Guest']);

					// Update data to users
					try{
						
						$user = User::find($data['User']['id']);
						$user->update($data['User']);
					}
					catch(\Exception $e){
						//echo $e->getMessage();
						//die('user');
						return false;
				    }

					// Update data to profiles
					try{
						$profile = Profile::find($data['Profile']['id']);
						$profile->update($data['Profile']);
					}
					catch(\Exception $e){
						//echo $e->getMessage();
						//die('profile');
						return false;
				    }

					// Add Transaction Report
					try{
						$fullname = $profile->title . ' ' . $profile->firstname . ' ' . $profile->lastname . ' ' . $profile->sufix;
						$newactivity = array(
							'created'=>$currentdate,
							'users_id'=>$data['Guest']['created_by'],
							'activity' =>'UPDATED GUEST "'. $fullname .'" (ID:' . $guest->id .')'
						);
						$activity = new Activity();								
						$activity->add($newactivity);
					}
					catch(\Exception $e){
						//echo "transaction = ".$e->getMessage();die();
						return false;
					}	
					// Update data to Urls
					try{	
						//Fetch if exists the social urls
				    	$old_urls_arr = array();
						$old_urls = $guest->urls()->get();
						if($old_urls->count() > 0){
				            foreach($old_urls as $url){
				                $old_urls_arr[] = $url->id;
				            }
				        }
				    
						$urls_objects_arr = array();
						if(isset($data['Url'])){
							if(!empty($data['Url'])){
								
								foreach($data['Url'] as $item){
									if(!empty($old_urls_arr)){
										//If new video link is added
										if(isset($item['id'])){
											if($item['id'] > 0){
												if(!in_array($item['id'], $old_urls_arr)){
													$urls_objects_arr[] = new Url($item);
												}else{
													//Else update the existing social url
													$urlObj = Url::find($item['id']);
													$urlObj->update($item);
													if (($key = array_search($item['id'], $old_urls_arr)) !== false){
													    unset($old_urls_arr[$key]);
													}
												}
											}
										}else{
											$urls_objects_arr[] = new Url($item);
										}								
									}else{
										if(isset($item['id'])){
											if($item['id'] > 0){
												$url = Url::find($item['id']);
												$url->update($item);
											}
										}else{
											$urls_objects_arr[] = new Url($item);
										}
									}
								}
								if(!empty($urls_objects_arr)){
									$guest->urls()->saveMany($urls_objects_arr);
								}
								// Delete the removed sociall urls while editing
								if(!empty($old_urls_arr)){
									foreach($old_urls_arr as $old_url){
										$del_url_obj = Url::find($old_url);
										$del_url_obj->delete();
									}
								}
							}
						}
					}
					catch(\Exception $e){
						//echo "social urls = ".$e->getMessage();die();
						return false;
				    }

					// Update data to Videos
					try{	
						//Fetch if exists the video links
				    	$old_videos_arr = array();
						$old_videos = $guest->videos()->get();
						if($old_videos->count() > 0){
				            foreach($old_videos as $video){
				                $old_videos_arr[] = $video->id;
				            }
				        }

						$videos_objects_arr = array();
						if(isset($data['Video'])){
							if(!empty($data['Video'])){
								foreach($data['Video'] as $item){
									if(!isset($item['is_enabled'])){
										$item['is_enabled'] = 0;
									}
									if(!empty($old_videos_arr)){
										//If new video link is added
										if(isset($item['id'])){
											if($item['id'] > 0){
												if(!in_array($item['id'], $old_videos_arr)){
													$videos_objects_arr[] = new Video($item);
												}else{
													//Else update the existing video link
													$videoObj = Video::find($item['id']);
													$videoObj->update($item);
													if (($key = array_search($item['id'], $old_videos_arr)) !== false){
													    unset($old_videos_arr[$key]);
													}
												}
											}
										}else{
											$videos_objects_arr[] = new Video($item);
										}									
										
									}else{
										if(isset($item['id'])){
											if($item['id'] > 0){
												$video = Video::find($item['id']);
												$video->update($item);
											}
										}else{
											$videos_objects_arr[] = new Video($item);
										}
									}
								}
								if(!empty($videos_objects_arr)){
									$guest->videos()->saveMany($videos_objects_arr);
								}
								// Delete the removed video links while editing
								if(!empty($old_videos_arr)){
									foreach($old_videos_arr as $old_video){
										$del_videos_obj = Video::find($old_video);
										$del_videos_obj->delete();
									}
								}
							}
						}
					}
					catch(\Exception $e){
						//echo "videos = ".$e->getMessage();die();
						return false;
				    }

				    // Update data to categories_has_guest
				    try{  	
				    	$same = 0;
				    	if(isset($data['Category'])){
							if(!empty($data['Category'])){
								if(!empty($data['Category']['Category'])){
							        if(!empty($data['Category']['select_main_id']))	{

							        	//Change main category the delete all the categories
							        	if(!in_array($data['Category']['select_main_id'], $data['Category']['Category'])){
							        		$del_guest_cat_object = GuestsHasCategory::where('guests_id' , '=', $guest->id)->delete();
										}else{
											if(!empty($data['Category']['select_sub_id']))	{
												$sub_cats = json_decode($data['Category']['select_sub_id']);
												
												foreach($sub_cats as $sub_cat){
													if(!in_array($sub_cat, $data['Category']['Category'])){
														$del_guest_cat_object = GuestsHasCategory::where(['guests_id' => $guest->id, 'categories_id' => $sub_cat])->delete();
													}
												}
											}
										}
							        }
							        
						        	foreach($data['Category']['Category'] as $item){
						        		if(!empty($item))	{
						        			$newcat = array('guests_id'=>$guest->id, 'categories_id' => $item );
											$guest_cat = GuestsHasCategory::updateOrCreate($newcat);
											$guest_cat->save();
						        		}
									}
								}
							}
						}
					}catch(\Exception $e){
						//echo "catgeory = ".$e->getMessage();die();
						return false;
				    }

					return true;
				}catch(\Exception $e){
					//echo "guest = ".$e->getMessage();die();
					return false;
			    }
			}
		}else{
			return false;
		}
	}


	/**
	 * @description Delete the Host and its relational data 
	 * @param where $id is host_id
	 */	
	public function deleteGuest($id=0){
		
		if($id != '' || $id !=0){

			$guest = Guest::find($id);
			$guest->update(['deleted'=>1]);

			//Also update in the users table
			User::where('id', $guest->users_id)->update(['deleted'=>1]);
			
			if ($guest->has('urls')) {
			   $guest->urls()->delete();
			}
			if ($guest->has('videos')) {
			   $guest->videos()->delete();
			}
			if ($guest->has('guests_has_categories')) {
				
				//$check_cats = $guest->guests_has_categories()->get();
				/*if($check_cats->count() > 0){
					foreach($check_cats as $cat)
					{	
						//$cat->guests_has_categories()->delete();
						$cat->guests_has_categories()->delete();
					}
				}*/
				$guest->guests_has_categories()->delete();
			}				
			
			return true;
		}
	}

	/**
	* Count all the guests
	* @return result in the form of object
	* @param 
	*/
	public function countAllGuests($alphabet='', $search_column='general', $order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{

		$guests = User::select('users.*', 'guests.id AS guest_id', 'guests.bio AS guest_bio', 'hosts.id AS host_id', 'hosts.bio AS host_bio', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix', 'profiles.name');

		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
			$guests = $guests->where('profiles.lastname' ,'like', $alphabet.'%');
		}
		// Overall Search 
        if(!empty($search_value)){
        	if($search_column != "general"){
        		if($search_column == "bio"){
        			$guests = $guests->where(function($q) use ($search_column, $search_value){
    								$q->orWhere('hosts.'.$search_column ,'like', '%'.$search_value.'%')
    								  ->orWhere('guests.'.$search_column ,'like', '%'.$search_value.'%');
    						  });
        		}else{
        			$guests = $guests->where('profiles.'.$search_column ,'like', '%'.$search_value.'%');
        		}
    			
			}else{
          
	            // Join for searching the host name from profiles
				$guests = $guests->where(function($q) use ($search_value){
					       /* $q->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
					          ->orWhere('hosts.bio' ,'like', '%'.$search_value.'%')*/
					        $q->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
					          ->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
					          ->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
					          ->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
					          ->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%');
					   });
			}
        }
		
		
		//Fetch who are not deleted
		$guests = $guests->leftJoin('guests', 'users.id', '=', 'guests.users_id')->leftJoin('hosts', 'users.id', '=', 'hosts.users_id');

		//Fetch name of the host
		$guests = $guests->leftJoin('profiles', 'profiles.users_id', '=', 'users.id');

		 $guests = $guests->where(function($query) {
		                $query->where('users.groups_id', 5)
		                	 ->orWhere(function($q){
		                	 	$q->where('users.groups_id', 2)
		                    	->where('users.is_guest', 1);
		                	 });			                	
		            })->where('users.deleted', 0);

        // Sorting by column
        if($order != null){
            $guests = $guests->orderBy($order, $dir);
        }else{
            $guests = $guests->orderBy('profiles.name', 'asc')->orderBy('profiles.firstname', 'asc');
        } 
        $guests = $guests->count();
		return $guests;	
	}

	/**
	 * Fetches all the guests
	 * @return result in the form of object
	 * @param 
	 */
	public function getAllGuests($alphabet='', $search_column='general', $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null)
	{
		
		$guests = User::select('users.*', 'guests.id AS guest_id', 'guests.bio AS guest_bio', 'hosts.id AS host_id', 'hosts.bio AS host_bio', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix', 'profiles.name');

		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
			$guests = $guests->where('profiles.lastname' ,'like', $alphabet.'%');
		}


		// Overall Search 
        if(!empty($search_value)){
        	if($search_column != "general"){
        		if($search_column == "bio"){
        			$guests = $guests->where(function($q) use ($search_column, $search_value){
    								$q->orWhere('hosts.'.$search_column ,'like', '%'.$search_value.'%')
    								  ->orWhere('guests.'.$search_column ,'like', '%'.$search_value.'%');
    						  });
        		}else{
        			$guests = $guests->where('profiles.'.$search_column ,'like', '%'.$search_value.'%');
        		}
    			
			}else{
				$guests = $guests->where(function($q) use ($search_value){
					$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
						->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
						->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
						->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
						->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%');
						//->orWhere('guests.bio' ,'like', '%'.$search_value.'%')
						//->orWhere('hosts.bio' ,'like', '%'.$search_value.'%');
				});
			}
            
        }
		
		//Fetch who are not deleted
		$guests = $guests->leftJoin('guests', 'users.id', '=', 'guests.users_id')->leftJoin('hosts', 'users.id', '=', 'hosts.users_id');

		//Fetch name of the host
		$guests = $guests->leftJoin('profiles', 'profiles.users_id', '=', 'users.id');

        $guests = $guests->where(function($query) {
		                $query->where('users.groups_id', 5)
		                	 ->orWhere(function($q){
		                	 	$q->where('users.groups_id', 2)
		                    	->where('users.is_guest', 1);
		                	 });			                	
		            })->where('users.deleted', 0);
        // Sorting by column
        if($order != null){
            $guests = $guests->orderBy($order, $dir);
        }else{
            $guests = $guests->orderBy('profiles.name', 'asc')->orderBy('profiles.firstname', 'asc');
        } 
        //$guests = $guests->offset($start)->limit($length)->toSql();
        $guests = $guests->offset($start)->limit($length)->get();
        //echo "<pre>";print_R($guests);die;
		return $guests;

		/*******************************************************/

		/*
		$guests = $this->select('id', 'name', 'bio', 'firstname', 'lastname', 'title', 'sufix')->where('deleted', 0);
		
		$guests = $guests->selectRaw("'guest' as source");
		
		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
			$guests = $guests->where('lastname' ,'like', $alphabet.'%');
		}

		// Overall Search 
        if(!empty($search_value)){
            $guests = $guests->where(function($q) use ($search_value){
    							$q->orWhere('name' ,'like', '%'.$search_value.'%')
    								->orWhere('title' ,'like', '%'.$search_value.'%')
    								->orWhere('firstname' ,'like', '%'.$search_value.'%')
    								->orWhere('lastname' ,'like', '%'.$search_value.'%')
    								->orWhere('sufix' ,'like', '%'.$search_value.'%')
    								->orWhere('bio' ,'like', '%'.$search_value.'%');
    						});
        }


		// HOST Guests
		$hosts = Host::select('hosts.id', 'profiles.name', 'hosts.bio', 'profiles.firstname', 'profiles.lastname', 'profiles.title', 'profiles.sufix')
					->leftJoin('profiles', 'profiles.users_id','=', 'hosts.users_id')
					->where('is_guest', 1);
		$hosts = $hosts->selectRaw("'host' as source");
		$hosts = $hosts->union($guests);

		// Fetch records only starts from the alphabet given
		if(!empty($alphabet)){
			$hosts = $hosts->where('lastname' ,'like', $alphabet.'%');
		}

		// Overall Search 
        if(!empty($search_value)){
            $hosts = $hosts->where(function($q) use ($search_value){
    							$q->orWhere('profiles.name' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.title' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.firstname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.lastname' ,'like', '%'.$search_value.'%')
    								->orWhere('profiles.sufix' ,'like', '%'.$search_value.'%')
    								->orWhere('hosts.bio' ,'like', '%'.$search_value.'%');
    						});
        }

        // Sorting by column
        if($order != null){
            $hosts = $hosts->orderBy($order, $dir);
        }else{
            $hosts = $hosts->orderBy('name', 'asc')->orderBy('firstname', 'asc');
        }
        
		
		$hosts = $hosts->offset($start)->limit($length)->get();
	
		// Merge data from 2 tables - Guest and Host
		//$merged = $hosts->merge($guests);
		//$result = $merged->all();		
		return $hosts;*/
		/********************************************************************/
	}

	/**
	 * Export  guests
	 * @return result in the form of object
	 */
	public function eportGuests($channel_id='',$all='', $online='', $offline='', $featured=''){
		
			$guests = Guest::select('guests.lastname', 'guests.firstname', 'guests.email',
                'guests.phone', 'guests.cellphone', 'guests.skype', 'guests.skype_phone',
                'pr_name', 'pr_company_name', 'pr_phone', 'pr_cellphone',
                'pr_skype', 'pr_skype_phone', 'pr_email');
			if(!empty($channel_id)){
				$guests = $guests->Join('channels_has_guests', 'channels_has_guests.guests_id', '=', 'guests.id')
								->where('channels_has_guests.channels_id', '=', $channel_id)
								->where('guests.id', '!=', Auth::id())
			    				->where('guests.deleted',0);
			}
			if(!empty($online)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
	                   			  ->where('guests.deleted',0)
	                   			  ->where('guests.is_online','=',1);
			}
			if(!empty($offline)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
                   				  ->where('guests.deleted',0)
                   				  ->where('guests.is_online','=',0);
			}
			if(!empty($featured)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
                   				  ->where('guests.deleted',0)
                   				  ->where('guests.is_featured','=',1);
			}									

			if(!empty($all)){
				$guests =  $guests->where('guests.id', '!=', Auth::id())
                   				  ->where('guests.deleted',0);
			}

			// Exclude the guests who don't want to be contacted
			$guests = $guests->where('guests.to_be_contacted',1);
			$guests = $guests->orderBy('guests.lastname', 'asc')->get();
			return $guests;
	}

	/**
	 * Fetch the guests for the filters 
	 * @return result is the object of the name and id of the guests
	 */
	public function getGuestsForFilter(){
		$guests_list = $this->select(DB::raw('`guests`.`id`, TRIM(CONCAT( `lastname`, " ", `firstname` )) as `fullname`'))->orderBy('lastname', 'asc')->pluck('fullname', 'id');
		return $guests_list;
	}
}
