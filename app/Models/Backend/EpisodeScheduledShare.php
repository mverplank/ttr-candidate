<?php

/**
 * @author: Contriverz
 * @since : Wed, 26 Dec 2018 09:24:16.
 */

namespace App\Models\Backend;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EpisodeScheduledShare
 * 
 * @property int $id
 * @property int $users_id
 * @property int $episodes_id
 * @property string $type
 * @property \Carbon\Carbon $time
 * @property bool $is_twitter
 * @property bool $is_facebook
 * @property string $desc
 * @property string $url
 * 
 * @property \App\Models\Backend\Episode $episode
 * @property \App\Models\Backend\User $user
 *
 * @package App\Models\Backend
 */
class EpisodeScheduledShare extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int',
		'episodes_id' => 'int',
		'is_twitter' => 'bool',
		'is_facebook' => 'bool'
	];

	protected $dates = [
		'time'
	];

	protected $fillable = [
		'users_id',
		'episodes_id',
		'type',
		'facebook_time',
		'twitter_time',
		'is_twitter',
		'is_facebook',
		'twitter_desc',
		'facebook_desc',
		'url',
		'fb_shared',
		'twitter_shared'
	];

	public function episode()
	{
		return $this->belongsTo(\App\Models\Backend\Episode::class, 'episodes_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\Backend\User::class, 'users_id');
	}
}
