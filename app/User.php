<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'last_login', 'last_login_at', 'last_login_ip', 'created_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Get ID of the current user
    public function getId()
    {
      return $this->id;
    }

    // Fetch current user profile image
    public function photo()
    {
        if (file_exists( storage_path('app/public') . '/User/'.$this->id.'/photo/001.jpg')) {
            return asset('storage/app/public/User/'.$this->id.'/photo/001.jpg');
            //return asset('public/assets/images/users/avatar.jpeg');
        } else {
            return asset('public/assets/images/users/avatar.jpeg');
        }     
    }

}
