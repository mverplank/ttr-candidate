<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('front_home');
Route::get('/about-transformation-radio', 'AboutController@index')->name('front_about');
Route::get('/company-history', 'HistoryController@index')->name('company_history');
Route::get('/radio-shows', 'ShowsController@index')->name('front_shows');
Route::get('archives', 'EpisodesController@index')->name('front_podcasts');
Route::post('archives', 'EpisodesController@index')->name('front_ajax_podcasts');
Route::get('/host-a-show', 'HostsController@become_host')->name('front_become_a_host');
Route::get('/newsletter', 'PositiveMediaController@index')->name('front_positive_media');
Route::get('/contact-us', 'PagesController@contact_us')->name('front_contact');
Route::get('/guests', 'GuestsController@index')->name('front_guests');
Route::post('/guests', 'GuestsController@index')->name('front_guests');
Route::get('/hosts', 'HostsController@index')->name('front_hosts');
Route::post('/hosts', 'HostsController@index')->name('front_hosts');
Route::get('/co-hosts', 'HostsController@co_hosts')->name('front_co_hosts');
Route::post('/co-hosts', 'HostsController@co_hosts')->name('front_co_hosts');
Route::get('/become-a-sponsor', 'SponsorsController@become_sponsor')->name('front_become_a_sponsor');

Route::get('/featured-shows', 'ShowsController@featured_shows')->name('front_featured_shows');
Route::post('/featured-shows', 'ShowsController@featured_shows')->name('front_featured_shows');
Route::get('/schedule', 'ShowsController@schedule')->name('front_schedule');
Route::get('/schedule', 'ShowsController@schedule')->name('front_schedule');

Route::get('sponsors', 'SponsorsController@index')->name('front_sponsors');
Route::post('sponsors', 'SponsorsController@index')->name('front_ajax_sponsors');

Route::group(['prefix' => 'schedule'], function () {
	Route::post('/zapbox', 'ShowsController@zapbox')->name('ajax_zapbox');
});

Route::group(['prefix' => 'advertising'], function () {
	Route::get('/banners/get', 'BannerController@get')->name('ajax_banners');
});

// CTR redirection on advertisement click route
Route::get('/rdr/{page_id}', 'BannerController@redir')->name('banner_redir');

Route::get('/events', 'PositiveMediaController@events')->name('front_events');
Route::get('/resource', 'PositiveMediaController@resources')->name('front_resources');
Route::get('/transformational-blog', 'PositiveMediaController@blog')->name('front_blog');

Route::get('/testimonials', 'PositiveMediaController@testimonials')->name('front_testimonials');

Route::get('/affiliates', 'PagesController@affiliates')->name('front_affiliates');


/* Individual Pages */
Route::get('/show-details/{slug},{id}.html', 'ShowsController@show_details')->name('front_show_details');
Route::get('/guest/{slug},{id}.html', 'GuestsController@guest')->name('front_guest_details');
Route::get('/{slug},{id}.html', 'ShowsController@show_details')->name('front_show_details');

/* Feeds */
Route::get('feed/episodes/itunes.rss', 'Feed\FeedsController@itunes')->name('feed_itunes');
Route::get('feed/episodes/archived.rss', 'Feed\FeedsController@archived')->name('feed_archived');
Route::get('feed/episodes/upcoming.rss', 'Feed\FeedsController@upcoming')->name('feed_upcoming');

Route::get('feed/{type?}', ['as' => 'feed.atom', 'uses' => 'Feed\FeedsController@getFeed']);

#Post archive
Route::get('posts/', ['as' => 'post.archive', 'uses' => 'PostController@archive']);
#Single
Route::get('post/{id}/{slug?}', ['as' => 'post.single', 'uses' => 'PostController@single']);

//Download
//Route::get('download/{any}', 'DownloadController@files');
Route::get("download/app/public/media/{id}/{files}/{filename}", "DownloadController@files");
Route::get("download/app/public/media/{id}/{image}/{filename}", "DownloadController@files");


// Custom pages
//Route::connect('/page/*', array('plugin' => 'cms', 'controller' => 'pages', 'action' => 'display'));
Route::get('/{page}.html', 'PagesController@index');
//Route::get("download/{path}", "DownloadController@files")->where('path', '.+');