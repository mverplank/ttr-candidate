<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/admin', 'AdminController@index')->name('admin');
	Route::get('/partner', 'AdminController@index')->name('partner');
	Route::get('/host', 'AdminController@index')->name('host');
	Route::post('/admin/print', 'AdminController@print')->name('print');
	Route::get('/admin/guest/to/user', 'AdminController@HostToUser');
	//Admin Routes
	Route::group(['prefix' => 'admin'], function () {

		// Help Routes
		Route::group(['prefix' => 'help'], function () {
			Route::group(['prefix' => 'pages'], function () {
				Route::get('/display/index', 'HelpController@index')->name('display_index');
				Route::get('/view/{page_name}', 'HelpController@show')->name('help_index');
			});
		});

		/** Scheduled Sharing on Social Media**/
		Route::get('/share', 'AutomaticSocialSharingController@sendDataToSocialMedia')->name('shareToFacebook');

		Route::get('/podcastStats', 'AdminController@podcastStats')->name('ajax_podcastStats');
		Route::get('/favoritesStats', 'AdminController@favoritesStats')->name('ajax_favoritesStats');
		// episodes schedules check
		Route::get('/episodes/schedules/check', 'SchedulesController@check_schedule')->name('check_schedule');

		//Channels
		Route::group(['prefix' => 'channel'], function () {
			Route::get('/channels', 'ChannelsController@index')->name('channels');
			Route::post('/channels', 'ChannelsController@ajax_index')->name('ajax_channels');
			Route::get('/channels/add', 'ChannelsController@create')->name('add_chhanel');
			//Route::post('/channels/add', 'ChannelsController@store')->name('add_chhanel');
			Route::post('/channels/add', 'ChannelsController@store')->name('store_channel');
			Route::get('/channels/edit/{id}', 'ChannelsController@edit')->name('edit_channel');
			Route::post('/channels/delete', 'ChannelsController@delete')->name('delete_channel');
			Route::post('/channels/update/{id}', 'ChannelsController@update')->name('update_channel');
			Route::get('/schedules/agenda/{channel_id?}', 'SchedulesController@index')->name('add_get_schedule_channel');
			/*Route::post('/schedules/agenda/{channel_id?}', 'SchedulesController@ajax_schedule')->name('add_post_schedule_channel');*/
			Route::post('/schedules/add', 'SchedulesController@add_schedule')->name('add_schedule');
			Route::post('/schedules/edit', 'SchedulesController@edit_schedule')->name('edit_schedule');
			Route::post('/schedules/delete', 'SchedulesController@delete_schedule')->name('delete_schedule');
			Route::post('/schedules/get', 'SchedulesController@get_schedule')->name('get_schedule');
			Route::post('/schedules/get_selected_schedule', 'SchedulesController@getSelectedSchedule')->name('get_selected_schedule');
			Route::post('/schedules/get_channel_shows', 'SchedulesController@getChannelShows')->name('get_channel_shows');
			Route::post('/schedules/get_shows_replays', 'SchedulesController@getShowsReplay')->name('get_shows_replay');
		});

		// Users
		Route::group(['prefix' => 'user'], function () {
			
			Route::get('/users', 'UsersController@index')->name('users');
			Route::post('/users', 'UsersController@ajax_index')->name('ajax_users');
			Route::get('/users/add', 'UsersController@add_user')->name('add_user');
			Route::post('/users/add', 'UsersController@add_user')->name('add_user');
			Route::get('/users/edit/{id}', 'UsersController@edit_user')->name('edit_user');
			Route::put('/users/edit/{id}', 'UsersController@edit_user')->name('edit_user');
			Route::get('/users/export', 'UsersController@export_user')->name('export_user');

			// Members
			Route::get('/members', 'MembersController@index')->name('members');
			Route::post('/members', 'MembersController@ajax_index')->name('ajax_members');
			Route::get('/members/add', 'MembersController@add_member')->name('add_member');
			Route::post('/members/add', 'MembersController@add_member')->name('add_member');
			Route::get('/members/edit/{id}', 'MembersController@edit_member')->name('edit_member');
			Route::post('/members/edit/{id}', 'MembersController@edit_member')->name('edit_member');
			Route::get('/members/export', 'MembersController@export_members')->name('export_members');

			// Hosts
			Route::get('/hosts', 'HostsController@index')->name('hosts');
			Route::post('/hosts', 'HostsController@ajax_index')->name('ajax_hosts');
			Route::get('/hosts/add', 'HostsController@add_host')->name('add_host');
			Route::post('/hosts/add', 'HostsController@add_host')->name('add_host');
			Route::get('/hosts/edit/{id}', 'HostsController@edit_host')->name('edit_host');
			Route::post('/hosts/edit/{id}', 'HostsController@edit_host')->name('edit_host');
			Route::get('/hosts/export', 'HostsController@export_hosts')->name('export_hosts');
			Route::post('/hosts/delete', 'HostsController@delete')->name('delete_host');
			Route::get('/hosts/export', 'HostsController@export_hosts')->name('export_hosts');
			Route::get('/hosts/banners/{id}', 'HostsController@hosts_banners_add')->name('hosts_banners_add');
			Route::get('/hosts/edit/custom_content/{id}', 'HostsController@custom_content')->name('host_custom_content');
			Route::put('/hosts/edit/custom_content/{id}', 'HostsController@custom_content')->name('host_custom_content');
			
			// Partners
			Route::get('/partners', 'PartnersController@index')->name('partners');
			Route::post('/partners', 'PartnersController@ajax_index')->name('ajax_partners');
			Route::get('/partners/add', 'PartnersController@add_partner')->name('add_partner');
			Route::post('/partners/add', 'PartnersController@add_partner')->name('add_partner');
			Route::get('/partners/edit/{id}', 'PartnersController@edit_partner')->name('edit_partner');
			Route::put('/partners/edit/{id}', 'PartnersController@edit_partner')->name('edit_partner');
			Route::get('/partners/export', 'PartnersController@export_partners')->name('export_partners');
			
			// Delete users
			Route::post('/users/delete', 'UsersController@delete')->name('delete_user');

			//Route::get('/members', 'UsersController@members')->name('members');
			Route::get('/activities', 'ActivitiesController@index')->name('activities');
			Route::post('/activities', 'ActivitiesController@ajax_index')->name('ajax_activities');
			Route::get('/activities/export', 'ActivitiesController@export_transaction_report')->name('export_transaction_report');

			//Change password
			Route::get('/users/password', 'AdminController@changepassform')->name('changepassform');	
			Route::post('/users/password', 'AdminController@updatePass')->name('changepassword');
			
			// Edit profile
			Route::get('/users/profile', 'AdminController@edit_profile')->name('editprofileform');
			Route::put('/users/profile', 'AdminController@edit_profile')->name('editprofileform');

			//Add module info
			Route::post('/get_module_info', 'ModuleInfoController@get_module_info')->name('get_module_info');
			Route::post('/add_module_info', 'ModuleInfoController@add_module_info')->name('add_module_info');
			
		});

		//Memberships
		Route::group(['prefix' => 'membership'], function () {
			Route::get('/memberships', 'MembershipsController@index')->name('memberships');
			Route::post('/memberships', 'MembershipsController@ajax_index')->name('ajax_memberships');
			Route::get('/memberships/add', 'MembershipsController@add_membership')->name('add_membership');
			Route::post('/memberships/add', 'MembershipsController@add_membership')->name('add_membership');
			Route::get('/memberships/edit/{id}', 'MembershipsController@edit_membership')->name('edit_membership');
			Route::post('/memberships/edit/{id}', 'MembershipsController@edit_membership')->name('edit_membership');
			Route::post('/memberships/delete', 'MembershipsController@delete')->name('delete_membership');
		});	

		// Advertising
		Route::group(['prefix' => 'advertising'], function () {
			Route::get('/banners', 'BannersController@index')->name('banners');
			Route::post('/banners', 'BannersController@ajax_index')->name('ajax_banners');
			Route::get('/banners/add', 'BannersController@create')->name('add_get_banner');
			Route::post('/banners/add', 'BannersController@add_banner')->name('add_post_banner');
			Route::get('/banners/edit/{id}', 'BannersController@edit_banner')->name('edit_get_banner');
			Route::post('/banners/edit/{id}', 'BannersController@edit_banner')->name('edit_post_banner');
			Route::post('/banners/delete', 'BannersController@delete')->name('delete_banner');

			Route::get('/sponsors', 'SponsorsController@index')->name('sponsors');
			Route::post('/sponsors', 'SponsorsController@ajax_index')->name('ajax_sponsors');
			Route::get('/sponsors/add', 'SponsorsController@add_sponsor')->name('add_get_sponsor');
			Route::post('/sponsors/add', 'SponsorsController@add_sponsor')->name('add_post_sponsor');
			Route::get('/sponsors/edit/{id}', 'SponsorsController@edit_sponsor')->name('edit_get_sponsor');
			Route::post('/sponsors/edit/{id}', 'SponsorsController@edit_sponsor')->name('edit_post_sponsor');
			Route::post('/sponsors/get_hosts', 'SponsorsController@getHosts')->name('get_sponsors_hosts');
			Route::get('/sponsors/export', 'SponsorsController@export_sponsor')->name('export_sponsor');
			Route::post('/sponsors/delete', 'SponsorsController@delete')->name('delete_sponsor');

			Route::get('/sponsors/export', 'SponsorsController@export_sponsor')->name('export_sponsor');
			Route::get('/sponsors/episodes/{id}', 'SponsorsController@episodes')->name('sponsor_get_episodes');
			Route::post('/sponsors/episodes/{id}', 'SponsorsController@episodes')->name('sponsor_post_episodes');
			Route::post('/host/multisearch', 'SponsorsController@host_multisearch')->name('host_multisearch');
			Route::post('add/banner_categories', 'BannersController@add_banner_categories')->name('add_banner_categories');
			Route::post('/banner_categories/delete', 'BannersController@delete_banner_categories')->name('delete_banner_categories');
		});

		//Bonus Items
		Route::group(['prefix' => 'inventory'], function () {
			Route::get('/items/index', 'ItemsController@index')->name('all_index_items');
			Route::post('/items/index', 'ItemsController@ajax_index')->name('ajax_all_index_items');
			Route::get('/items', 'ItemsController@index')->name('all_items');
			Route::post('/items', 'ItemsController@ajax_index')->name('ajax_all_items');
			Route::get('/items/add', 'ItemsController@add_item')->name('add_get_item');
			Route::post('/items/add', 'ItemsController@add_item')->name('add_post_item');
			Route::get('/items/partners', 'ItemsController@partner_index')->name('partner_items');
			Route::post('/items/partners', 'ItemsController@partner_ajax_index')->name('ajax_partner_items');
			// Edit for other admin items
			Route::get('/items/edit/{id}', 'ItemsController@edit_item')->name('edit_get_item');
			Route::post('/items/edit/{id}', 'ItemsController@edit_item')->name('edit_post_item');
			// Edit for partner items
			Route::get('/items/accept/{id}', 'ItemsController@edit_item')->name('edit_get_item');
			Route::post('/items/accept/{id}', 'ItemsController@edit_item')->name('edit_post_item');
			Route::post('/items/delete', 'ItemsController@delete')->name('delete_item');
			Route::get('/categories', 'InventoryCategoriesController@index')->name('item_categories');
			Route::post('/categories', 'InventoryCategoriesController@ajax_index')->name('ajax_item_categories');
			//Route::post('/add/categories', 'CategoriesController@AddItemCategories')->name('add_item_categories');
			Route::post('/add/categories', 'InventoryCategoriesController@add_item_categories')->name('add_item_categories');
			Route::post('/delete/categories', 'InventoryCategoriesController@delete_inventory_categories')->name('delete_inventory_categories');
			Route::get('/months/index/{id}', 'ItemsController@month_index')->name('all_month_items');
			Route::post('/months/index', 'ItemsController@ajax_month_index')->name('ajax_all_month_items');
			//id item id mid membership id
			Route::get('/months/edit/{id}/{mid}', 'ItemsController@month_edit')->name('get_month_edit');
			Route::post('/months/edit/{id}/{mid}', 'ItemsController@month_edit')->name('post_month_edit');

		});

		//Shows
		Route::group(['prefix' => 'show'], function () {

			Route::get('/shows', 'ShowsController@index')->name('shows');
			Route::post('/shows', 'ShowsController@ajax_index')->name('ajax_shows');
			Route::get('/shows/add', 'ShowsController@add_show')->name('add_get_show');
			Route::post('/shows/add', 'ShowsController@add_show')->name('add_post_show');
			Route::get('/shows/edit/{id}', 'ShowsController@edit_show')->name('edit_get_show');
			Route::post('/shows/edit/{id}', 'ShowsController@edit_show')->name('edit_post_show');
			Route::post('/shows/delete', 'ShowsController@delete')->name('delete_show');
			Route::post('/shows/get_hosts', 'ShowsController@getHosts')->name('get_show_hosts');
			Route::get('/categories/{type?}', 'CategoriesController@index')->name('all_categories');
			Route::post('/categories/{type?}', 'CategoriesController@ajax_index')->name('ajax_all_categories');
			;
			Route::post('/getcategories', 'CategoriesController@get_categories')->name('get_categories');
			;
			//Route::post('/categories/show', 'CategoriesController@categories')->name('get_categories');

			Route::post('/search/categories', 'CategoriesController@searchCat')->name('searchCat');
			Route::get('/guests', 'GuestsController@index')->name('guests');
			Route::post('/guests', 'GuestsController@ajax_index')->name('ajax_guests');
			Route::get('/guests/add', 'GuestsController@add_guest')->name('add_get_guest');
			Route::post('/guests/add', 'GuestsController@add_guest')->name('add_post_guest');
			Route::get('/guests/edit/{id}', 'GuestsController@edit_guest')->name('edit_get_guest');
			Route::post('/guests/edit/{id}', 'GuestsController@edit_guest')->name('edit_post_guest');
			Route::post('/guests/delete', 'GuestsController@delete')->name('delete_guest');
			Route::post('/guests/guest_host_unlink', 'GuestsController@guest_host_unlink')->name('guest_host_unlink');
			Route::get('/guests/export_guests', 'GuestsController@export_guests')->name('export_guests');
			Route::post('/guests/import_host', 'GuestsController@getHostDetailToImport')->name('import_host');
			Route::get('/episodes/archived', 'EpisodesController@archived')->name('archived_episodes');
			Route::post('/episodes/archived', 'EpisodesController@ajax_archived')->name('ajax_archived_episodes');
			Route::post('/episodes/encores_details', 'EpisodesController@encores_details')->name('encores_details');
			Route::get('/episodes/edit_archived/{id}', 'EpisodesController@edit_archived')->name('edit_get_archived');
			Route::post('/episodes/edit_archived/{id}', 'EpisodesController@edit_archived')->name('edit_post_archived');
			Route::post('/episodes/archived/delete', 'EpisodesController@delete_archive')->name('delete_archive');
			Route::get('/episodes/view_archived/{id}', 'EpisodesController@view_archived')->name('view_archived_episodes');
			Route::put('/episodes/view_archived/{id}', 'EpisodesController@view_archived')->name('post_view_archived');
			Route::post('/episodes/podcastStats', 'EpisodesController@podcastStats')->name('podcast_stats');
			Route::get('/episodes/share/{id}/{social}', 'EpisodesController@episode_share')->name('episode_share');
			Route::get('/episodes/share/{id}/{social}/{segment}', 'EpisodesController@episode_share_segment')->name('episode_share_segment');
			Route::get('/episodes/edit_view/{id}', 'EpisodesController@edit_episode')->name('edit_get_episode');
			Route::post('/episodes/edit_view/{id}', 'EpisodesController@edit_episode')->name('edit_post_episode');
			Route::get('/episodes/edit_upcoming/{id}', 'EpisodesController@edit_episode_upcoming')->name('edit_get_upcoming');
			Route::post('/episodes/edit_upcoming/{id}', 'EpisodesController@edit_episode_upcoming')->name('edit_post_upcoming');
			Route::post('/episodes/get_hosts', 'EpisodesController@getHosts')->name('get_episode_hosts');
			Route::post('/episodes/get_guests', 'EpisodesController@get_guests')->name('get_episode_guests');
			Route::get('/episodes/upcoming', 'EpisodesController@upcoming')->name('upcoming_episodes');
			Route::post('/episodes/upcoming', 'EpisodesController@ajax_upcoming')->name('ajax_upcoming_episodes');
			Route::get('/episodes/view_agenda/{id}', 'EpisodesController@get_view_agenda')->name('get_view_agenda');
			Route::put('/episodes/view_agenda/{id}', 'EpisodesController@get_view_agenda')->name('post_view_agenda');
			Route::get('/episodes/view_upcoming/{id}', 'EpisodesController@get_view_upcoming')->name('get_view_upcoming');
			Route::put('/episodes/view_upcoming/{id}', 'EpisodesController@get_view_upcoming')->name('post_view_upcoming');
			Route::get('/episodes/edit_view_upcoming/{id}', 'EpisodesController@edit_episode_upcoming')->name('get_edit_episode_upcoming');
			Route::post('/episodes/edit_view_upcoming/{id}', 'EpisodesController@edit_episode_upcoming')->name('post_edit_post_episode');
			//check reschedule
			Route::get('/reschedule', 'TestController@reschedule');
			
			Route::post('/episodes/reschedule_upcoming', 'EpisodesController@reschedule_episode')->name('reschedule_episode');
			
			Route::get('/schedules/agenda', 'SchedulesController@shows_agenda')->name('shows_agenda');
			Route::post('/schedules/agenda', 'SchedulesController@shows_ajax_agenda')->name('shows_ajax_agenda');
			Route::get('/schedules/get', 'SchedulesController@get_shows_schedule')->name('get_shows_schedule');
			Route::group(['prefix' => 'episodes'], function () {
				Route::get('/add_agenda/{id}/{date}/{day}', 'EpisodesController@add')->name('add_get_episode');
				Route::post('/add_agenda/{id}/{date}/{day}', 'EpisodesController@add')->name('add_post_episode');
				Route::post('/episode_schedule/', 'EpisodesController@episode_schedule')->name('add_episode_schedule');
				Route::post('/ajax_episode_schedule', 'EpisodesController@ajax_episode_schedule')->name('ajax_add_episode_schedule');
				Route::get('/select_encore/{id}/{date}', 'EpisodesController@select_encore')->name('select_get_encores');
				Route::post('/select_encore/{id}/{date}', 'EpisodesController@ajax_select_encore')->name('select_post_encores');
				Route::get('/add_encore/{id}/{date}/{episode_id}', 'EpisodesController@add_encore')->name('add_get_encore');//  schedule id ,date episode id				
				Route::put('/add_encore/{id}/{date}/{episode_id}', 'EpisodesController@add_encore')->name('add_post_encore');//  schedule id ,date episode id
				Route::post('/set_interval', 'EpisodesController@set_interval')->name('set_interval');//  schedule id ,date episode id
			});
		});
	
		// CMS
		Route::group(['prefix' => 'cms'], function () {
			//Route::get('/media', 'CmsController@media')->name('media_library');
			Route::post('/media', 'CmsController@ajax_media')->name('ajax_media_library');
			Route::get('/media/add/{type}', 'CmsController@media_add')->name('add_media_get');
			Route::post('/media/add/{type}', 'CmsController@ajax_media_add')->name('add_post_media_get');
			/*Route::post('/media/add/{type}', 'CmsController@media_add')->name('add_media_post');*/
			Route::post('/media/edit', 'CmsController@media_edit')->name('edit_media');
			Route::post('/media/get_media', 'CmsController@get_media')->name('get_media');
			Route::post('/media/get_module_media', 'CmsController@get_module_media')->name('get_module_media');
			Route::post('/media/get_more_media', 'CmsController@get_more_media')->name('get_more_media');
			Route::post('/media/link_media', 'CmsController@link_media')->name('link_media');
			Route::post('/media/delete_media', 'CmsController@delete_media')->name('delete_media');
			Route::post('/media/delete_media_link', 'CmsController@delete_media_link')->name('delete_media_link');
			Route::post('/media/add_media_link', 'CmsController@add_media_link')->name('add_media_link');
			Route::post('/media/edit_media_link', 'CmsController@edit_media_link')->name('edit_media_link');
			Route::post('/media/get_media_info', 'CmsController@get_media_info')->name('get_media_info');

			Route::group(['prefix' => 'configurations'], function () {
				Route::get('/content', 'ConfigurationsController@prefix_sufix')->name('prefix_sufix');
				Route::post('/content', 'ConfigurationsController@prefix_sufix')->name('post_prefix_sufix');
				Route::get('/base', 'ConfigurationsController@base_cofig')->name('get_base_cofig');
				Route::post('/base', 'ConfigurationsController@base_cofig')->name('post_base_cofig');
				Route::get('/payments', 'ConfigurationsController@payments')->name('get_payments');
				Route::post('/payments', 'ConfigurationsController@payments')->name('post_payments');

				//Route::get('/memberships', 'MembershipsController@index')->name('memberships');
			});

			Route::get('/content_categories/tree', 'ContentsController@index')->name('get_tree');
			Route::post('/content_categories/tree', 'ContentsController@ajax_index')->name('ajax_get_tree');
			Route::post('/getcategories', 'ContentsController@getcategories')->name('getcategories');

			Route::group(['prefix' => 'contents'], function () {
				Route::get('/emails', 'ContentsController@content_email')->name('get_content_email');
				Route::post('/emails', 'ContentsController@content_email')->name('ajax_get_content_email');
				Route::post('/getfile', 'ContentsController@getfile')->name('ajax_content_email');
			});

			Route::group(['prefix' => 'pages'], function () {
				Route::get('/', 'PageController@index')->name('get_cms_pages');
				Route::post('index', 'PageController@ajax_cms_pages_index')->name('ajax_cms_pages_index');
				Route::get('/add', 'PageController@add')->name('get_add_cms_pages');
				Route::post('/add', 'PageController@add')->name('post_add_cms_pages');
				Route::get('/edit/{id}', 'PageController@edit')->name('get_edit_cms_pages');
				Route::post('/edit/{id}', 'PageController@edit')->name('post_edit_cms_pages');
				Route::post('/delete', 'PageController@delete')->name('post_delete_cms_pages');
				//Route::post('/emails', 'ContentsController@content_email')->name('ajax_get_content_email');
				//Route::post('/getfile', 'ContentsController@getfile')->name('ajax_content_email');
			});

			Route::group(['prefix' => 'themes'], function () {
				Route::get('/', 'ThemeController@index')->name('get_cms_themes');
				Route::post('index', 'ThemeController@ajax_cms_themes_index')->name('ajax_cms_themes_index');
				Route::get('/add', 'ThemeController@add')->name('get_add_cms_themes');
				Route::post('/add', 'ThemeController@add')->name('post_add_cms_themes');
				Route::get('/edit/{id}', 'ThemeController@edit')->name('get_edit_cms_themes');
				Route::post('/edit/{id}', 'ThemeController@edit')->name('post_edit_cms_themes');
				Route::post('/delete', 'ThemeController@delete')->name('post_delete_cms_themes');
				//Route::post('/emails', 'ContentsController@content_email')->name('ajax_get_content_email');
				//Route::post('/getfile', 'ContentsController@getfile')->name('ajax_content_email');
			});

			Route::get('content_data/index/{id?}', 'ContentsController@content_data_index')->name('content_data_index');
			Route::post('content_data/index', 'ContentsController@ajax_content_data_index')->name('ajax_content_data_index');
			Route::get('content_categories/add', 'ContentsController@add_content_categories')->name('get_add_content_categories');
			Route::post('content_categories/add', 'ContentsController@add_content_categories')->name('post_add_content_categories');
			Route::get('content_categories/edit/{id}', 'ContentsController@edit_content_categories')->name('post_edit_content_categories');
			Route::post('content_categories/edit/{id}', 'ContentsController@edit_content_categories')->name('post_edit_content_categories');
			Route::get('content_categories/delete/{id}', 'ContentsController@delete_content_categories')->name('delete_content_categories');
			Route::get('content_data/add/{id}', 'ContentsController@add_content_data')->name('get_add_content_data');
			Route::post('content_data/add/{id}', 'ContentsController@add_content_data')->name('post_add_content_data');
			Route::post('content_data/reorder', 'ContentsController@reorder_category')->name('post_reorder_category');
			Route::post('content_data/delete', 'ContentsController@delete_content_data')->name('delete_content_data');
			Route::get('content_data/edit/{id}', 'ContentsController@edit_content_data')->name('get_edit_content_data');
			Route::post('content_data/edit/{id}', 'ContentsController@edit_content_data')->name('post_edit_content_data');
			Route::get('content/validation', 'ContentsController@content_validation')->name('ajax_content_validation');
		});
		
		// Studio
		Route::group(['prefix' => 'studio'], function () {
			Route::get('/channel/select', 'StudioController@index')->name('studio_channels');
			Route::post('/channel/select', 'StudioController@ajax_index')->name('ajax_studio_channels');
			Route::get('/channel/app/{id}', 'StudioController@studio')->name('studio_app');
		});

		Route::post('/forms/validate_input', 'FormsController@validate_input');
		Route::post('/uploads/upload', 'UploadsController@upload');
		Route::post('/uploads/sort_files', 'UploadsController@sort_files');
		Route::post('/uploads/get_files', 'UploadsController@get_files');
		Route::post('/uploads/delete_files', 'UploadsController@delete_files');
		// Tool route
		Route::get('mailing/composer', 'ToolController@news_letter')->name('get_news_letter');
		Route::post('mailing/composer', 'ToolController@news_letter')->name('post_news_letter');
	});
	
	// Partner Routes
	Route::group(['prefix' => 'partner'], function () {
		Route::group(['prefix' => 'inventory'], function () {
			Route::get('/items/my', 'ItemsController@partner_index')->name('partner_items');
			Route::post('/items/my', 'ItemsController@partner_ajax_index')->name('partner_ajax_items');
			Route::get('/items/add', 'ItemsController@add_item')->name('add_get_item');
			Route::post('/items/add', 'ItemsController@add_item')->name('add_post_item');
			// Edit for  items
			Route::get('/items/edit/{id}', 'ItemsController@edit_item')->name('edit_get_item');
			Route::post('/items/edit/{id}', 'ItemsController@edit_item')->name('edit_post_item');
		});
	});
	
	// Host Routes
	Route::group(['prefix' => 'host'], function () {
		Route::group(['prefix' => 'show'], function () {
			Route::get('/episodes/upcoming', 'EpisodesController@upcoming')->name('host_upcoming_episodes');
			Route::post('/episodes/upcoming', 'EpisodesController@ajax_upcoming')->name('ajax_host_upcoming_episodes');
			Route::get('/episodes/archived', 'EpisodesController@archived')->name('host_archived_episodes');
			Route::post('/episodes/archived', 'EpisodesController@ajax_archived')->name('host_ajax_archived_episodes');

			Route::get('/guests', 'GuestsController@index')->name('host_guests');
			Route::post('/guests', 'GuestsController@ajax_index')->name('host_ajax_guests');
			Route::get('/guests/add', 'GuestsController@add_guest')->name('host_add_get_guest');
			Route::post('/guests/add', 'GuestsController@add_guest')->name('host_add_post_guest');
			Route::get('/guests/edit/{id}', 'GuestsController@edit_guest')->name('edit_get_guest');
			Route::post('/guests/edit/{id}', 'GuestsController@edit_guest')->name('edit_post_guest');
			Route::get('/episodes/edit_archived/{id}', 'EpisodesController@edit_archived')->name('host_edit_get_archived');
			Route::post('/episodes/edit_archived/{id}', 'EpisodesController@edit_archived')->name('host_edit_post_archived');
			Route::get('/episodes/view_archived/{id}', 'EpisodesController@view_archived')->name('host_view_archived_episodes');
			Route::group(['prefix' => 'episodes'], function () {
				Route::get('/add_agenda/{id}/{date}/{day}', 'EpisodesController@add')->name('host_add_get_episode');
				Route::post('/add_agenda/{id}/{date}/{day}', 'EpisodesController@add')->name('host_add_post_episode');
				Route::get('/select_encore/{id}/{date}', 'EpisodesController@select_encore')->name('host_select_get_encores');
				Route::post('/select_encore/{id}/{date}', 'EpisodesController@ajax_select_encore')->name('host_select_post_encores');
				// Schedule id, date episode id
				Route::get('/add_encore/{id}/{date}/{episode_id}', 'EpisodesController@add_encore')->name('host_add_get_encore');
				// Schedule id, date episode id
				Route::put('/add_encore/{id}/{date}/{episode_id}', 'EpisodesController@add_encore')->name('host_add_post_encore');
			});
		});
		Route::get('/user/hosts/profile', 'AdminController@edit_profile')->name('edithostprofileform');
		Route::put('/user/hosts/profile', 'AdminController@edit_profile')->name('edithostprofileform');
		Route::get('user/hosts/page', 'AdminController@edit_profile_custom_content')->name('edithost_profile_custom_content');
		Route::put('user/hosts/page', 'AdminController@edit_profile_custom_content')->name('edithost_profile_custom_content');
		Route::group(['prefix' => 'show'], function () {
			Route::get('/schedules/agenda', 'SchedulesController@shows_agenda')->name('shows_agenda');
			Route::post('/schedules/agenda', 'SchedulesController@shows_ajax_agenda')->name('shows_ajax_agenda');
		});
	});

	Route::get('tools/clear_cache', 'ToolController@clear_cache')->name('tools_clear_cache');