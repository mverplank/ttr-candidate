<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Page;
use App\Models\Backend\User;
use Session;
use Validator;
use Auth;
//use Mail;
use Redirect;
//use Illuminate\Support\Facades\Artisan;
//use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Add thenews letter.
     *
     * @return Response
     */
    public function index(Request $request){
      return view('backend/cms/pages');
    }

    private function setCMSPages($categoryId, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null) {
        $order_field     = 'title';
        $order_direction = 'ASC';
        $page_obj = new Page();
        $data = $page_obj->fetchPages($categoryId, $order_field, $order_direction, $start, $length, $order, $dir, $column_search, $search_value, $search_regex);
        $cdata = $page_obj->countPages($categoryId, $order_field, $order_direction, $order, $dir, $column_search, $search_value, $search_regex);
        // build order and direction human readable description 
        return ['data'=>$data, 'cdata'=>$cdata];
    }
    public function ajax_cms_pages_index(Request $request){

        /*$categoryId = $request->content_cat_id;
        if (is_null($categoryId)) {

            //
            // All Content Categories
            //            
            $categoryId = '';
            $constants  = array();
        } else {
            $constants  = array('content_categories_id' => $categoryId);
        }*/
     
        $draw          = 1;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 

        $cmsPageData = $this->setCMSPages(0, $start, $length, $order, $dir, null, $search_value); 
        $all_page_data   = $cmsPageData['data'];
        $count_page_data = $cmsPageData['cdata'];
       
        $data = array();
        $i    = 0;   

        if(!empty($all_page_data)){  
            foreach($all_page_data as $page_data){

                $online = '<h4>'.$page_data->title.'<h4>';
                /*if($content_data->is_online == 1){
                    $online .= '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Online</button>';
                }else{
                    $online .= '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Offline</button>';
                }

                if($content_data->is_featured == 1){
                    $online .= '&nbsp;&nbsp;<button class="btn btn-brown disabled waves-effect waves-light btn-xs m-b-5">Featured</button>';
                }*/
                $data[$i][] = $online;
                /*$data[$i][]  = strlen($content_data->content) > 400 ? substr($content_data->content,0,400).'...' : $content_data->content;*/
                /*$data[$i][] = date("m/d/Y", strtotime($content_data->created_at));
                $data[$i][] = date("m/d/Y", strtotime($content_data->updated_at));*/
                $data[$i][] = '/'.$page_data->name.'.html';
                $data[$i][] = url($page_data->name.'.html');
                $data[$i][] = '<a href="'.url('admin/cms/pages/edit/'.$page_data->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteCmsPages('.$page_data->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $data[$i]['DT_RowId']    = $page_data->id;
                $data[$i]['DT_RowClass'] = 'off_'.$page_data->off;
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_page_data,
                    'recordsFiltered' => $count_page_data,
                    'data' => $data,

                );

        echo json_encode($output);
        exit();
    }

    /**
     * Add cms pages .
     *
     * @return get method return view post method add new 
    */
    public function add(Request $request){
       $pages = Page::all()->pluck('name', 'id')->toArray();
        if ($request->isMethod('post')) {           
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $page = new Page();
            $response = $page->add($data);
            if($response){
                $message = "New Page successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the Page. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/pages');   
        }else{

            return view('backend/cms/add_pages')->with(compact('pages'));
        }
    }

    /**
     * edit cms pages .
     *
     * @return get method return view post method update 
    */
    public function edit($id, Request $request){
        $page = Page::where('id', '=', $id)->first();
        $page->full_url = url($page->name.'.html');
        if ($request->isMethod('post')) {           
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $page = new Page();
            $response = $page->edit($id, $data);
            if($response){
                $message = "The Page details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the Page. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/pages/edit/'.$id);   
        }else{
            return view('backend/cms/edit_pages')->with(compact('page'));
        }
    }

    // Delete Shows
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $page_id = $post_data['id'];
                $page = new Page();
                $delete = $page->deletePage($page_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

}
