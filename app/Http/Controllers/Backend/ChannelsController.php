<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\Theme;
use App\Models\Backend\User;
use App\Models\Backend\MediaLinking;
use Session;
use Validator;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

class ChannelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backend/channel/index');
    
    }

    public function ajax_index(Request $request)
    {
        $channels  = new Channel();
        $draw      = 1;
        $start     = $request->input('start');
        $length    = $request->input('length');
        $draw      = $request->input('draw');
        $order     = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
       
        $all_channels = $channels->getChannels($start, $length, $order, $dir, null, $search_value);
        $count_channels = $channels->countChannels($order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   

        if(!empty($all_channels)){
            foreach($all_channels as $channel){
               
                $data[$i][]  = '<h4>'.$channel->name.'<h4>';
                $data[$i][]  = strlen($channel->description) > 400 ? substr($channel->description,0,400).'...' : $channel->description;
                                   
                $data[$i][]  = '<a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="'.url('admin/channel/schedules/agenda/'.$channel->id ).'" title="Edit Schedule"><i class="glyphicon glyphicon-time"></i></a>
                                    <a href="javascript:void(0)" title="Edit Website Content"><i class="glyphicon glyphicon-blackboard"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteChannel('.$channel->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $all_channels,
                    'recordsFiltered' => $count_channels,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        $channel = new Channel();
        // $auth = \Auth::user()->id;
        // $media = MediaLinking::where('module_id',0)->where('main_module','Channel')->where('created_by',$auth)->get();
        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Channel');
        return view('backend/channel/add')->with(compact('channel','media'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request){
        $auth = \Auth::user()->id;
        $channel = new Channel();
        $channel->name                         = $request->data['Channel']['name'];
        $channel->description                  = $request->data['Channel']['description'];
        $channel->domain                       = $request->data['Channel']['domain'];
        $channel->stream                       = $request->data['Channel']['stream'];
        $channel->url_archived_episodes        = $request->data['Channel']['url_archived_episodes'];
        $channel->seo_title                    = $request->data['Channel']['seo_title'];
        $channel->seo_keywords                 = $request->data['Channel']['seo_keywords'];
        $channel->seo_description              = $request->data['Channel']['seo_description'];
        $channel->sam_dir_shows                = $request->data['Channel']['sam_dir_shows'];
        //$channel->sam_file_connecting          = $request->data['Channel']['sam_file_connecting'];
        $channel->sam_min_show_duration        = $request->data['Channel']['sam_min_show_duration'];
        if(isset($request->data['Channel']['is_missing_episodes_reminder'])){
            $channel->is_missing_episodes_reminder = $request->data['Channel']['is_missing_episodes_reminder'];
        }else{
            $channel->is_missing_episodes_reminder = 0;
        }
        
        $channel->deleted                      = 0;
        $channel->is_online                    = 1;
        
        if($channel->save()){
            $media = new MediaLinking();
            $media->add($channel->id, 'Channel');
            // MediaLinking::where('main_module','Channel')
            //               ->where('module_id',0)
            //               ->where('created_by',$auth)->update(['module_id'=>$channel->id]);
                Session::flash('success', 'Channel Added Successfully.....');
                return redirect('admin/channel/channels');
        }else{
            return Redirect::back()->withErrors(['error', 'Some Internal Error Occured Please Try Again.....']);
        }

    }

    /*
    * media linking
    *
    */
    public function mediaLinking($media, $module){
        $media_arr =explode(",", $media);
        unset($media_arr[0]);
        foreach ($media_arr as $mediaid) {
           $media_link = new MediaLinking();
           $media_link->media_id =  $mediaid;
           $media_link->module_id =$module['id'];
           $media_link->main_module =$module['name'];
           $media_link->sub_module =$module['sub_module'];
           $media_link->created_by =\Auth::user()->id;
           $media_link->save();
        }
        return 'success';
    }
    /*
    * Move All channel Image From Tmp Location to permananet folder
    *
    */

    public function procesImage($object, $module){
        $folders = ['player', 'zapbox', 'zapboxsmall', 'widget', 'cover'];
        $userid = Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }    
       
        return 'success';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $catogries
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $channel = Channel::FindorFail($id);
        $themes = Theme::all()->pluck('name', 'name')->toArray();
        //echo"<pre>";print_r($themes);die;
        $media_model =  new MediaLinking();
       $media = $media_model->get_media($id, 'Channel');
        //Bit Rate
        $bitrate = array(
                        '8'   => '8',
                        '32'  => '32',
                        '64'  => '64',
                        '96'  => '96',
                        '112' => '112',
                        '128' => '128',
                        '160' => '160',
                        '192' => '192',
                        '256' => '256',
                        '320' => '320'
                        );

        return view('backend/channel/edit', compact('channel', 'bitrate','media','themes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function update(Request $request, $id){
        //echo"<pre>";print_r($request->all());die;
        $channel = Channel::findOrFail($id);
        $channel->name                         = $request->data['Channel']['name'];
        $channel->description                  = $request->data['Channel']['description'];
        $channel->domain                       = $request->data['Channel']['domain'];
        $channel->stream                       = $request->data['Channel']['stream'];
        $channel->url_archived_episodes        = $request->data['Channel']['url_archived_episodes'];
        $channel->seo_title                    = $request->data['Channel']['seo_title'];
        $channel->seo_keywords                 = $request->data['Channel']['seo_keywords'];
        $channel->seo_description              = $request->data['Channel']['seo_description'];
        $channel->sam_dir_shows                = $request->data['Channel']['sam_dir_shows'];
        $channel->studioapp_enabled            = $request->data['Channel']['studioapp_enabled'];
        $channel->studioapp_enabled            = $request->data['Channel']['studioapp_enabled'];
        $channel->theme_name                   = $request->data['Channel']['theme_name'];
        //$channel->sam_file_connecting          = $request->data['Channel']['sam_file_connecting'];
        $channel->sam_min_show_duration        = $request->data['Channel']['sam_min_show_duration'];
        if(isset($request->data['Channel']['is_missing_episodes_reminder'])){
            $channel->is_missing_episodes_reminder = $request->data['Channel']['is_missing_episodes_reminder'];
        }else{
            $channel->is_missing_episodes_reminder        = 0;
        }
        if($channel->save()){
            Session::flash('success', 'Channel Updated Successfully.....');
            return redirect('admin/channel/channels');
        }else{
            return Redirect::back()->withErrors(['error', 'Some Internal Error Occured Please Try Again.....']);
        }
    }
    

     /**
     * Delete Channel
     *
     * @return Response
     */
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
           
            if(!empty($post_data)){
                $channel_id = $post_data['id'];
                $channel = new Channel();
                $delete = $channel->deleteChannel($channel_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }
}
