<?php

/**
 * @author : Contriverz
 * @since  : 13-02-2019
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\Show;
use App\Models\Backend\ShowsHasHost;
use App\Models\Backend\Category;
use App\Models\Backend\MediaLinking;

use Auth;
use Session;

class ShowsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        $all_channels->put('', 'Any Channel');
        $selected_channel = '';
        
        if ($request->has('channel_id')) {
            $selected_channel = $request->input('channel_id');
        }
        
        return view('backend/show/shows')->with(compact('selected_channel', 'all_channels'));
    }

   
    public function ajax_index(Request $request)
    {
        $shows        = new Show();
        $draw         = 1;
        $channel_id   = 0;
        $start        = $request->input('start');
        $length       = $request->input('length');
        $draw         = $request->input('draw');
        $order        = $request->post("order");
        $channel_id   = $request->post("channel_id");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }  

        $all_shows   = $shows->getAllShows( $channel_id, $start, $length, $order, $dir, null, $search_value);
        $count_shows = $shows->countAllShows( $channel_id, $order, $dir, null, $search_value);
    
        $data = array();
        $i    = 0;   
        foreach($all_shows as $show){
            
            if($show->featured > 0){
                $data[$i][]  = $show->name." <button class='btn btn-teal disabled m-b-5'>Featured (".$show->featured.")</button>";
            }else{
                $data[$i][]  = $show->name;
            }

            $data[$i][]  = $shows->getShowHostsCohosts($show->id, true);
            $data[$i][]  = $shows->getShowChannels($show->id, true);
            $data[$i][]  = '<a href="'.url('admin/show/shows/edit/'.$show->id ).'"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)" onclick="deleteShow('.$show->id.', this)"><i class="glyphicon glyphicon-trash"></i></a>';
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_shows,
                    'recordsFiltered' => $count_shows,
                    'data' => $data
                );

        echo json_encode($output);
        exit();        
    }

    // Add Shows
    public function add_show(Request $request)
    {

        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Show');
        // Fetch all Categories for filters
        $categories = new Category();
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false);        
        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );

        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
       
        if ($request->isMethod('post')) {
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $show = new Show();
            $response = $show->add($data);
            if($response){
                $message = "New Show has been successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the show. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/show/shows');
        }else{
            return view('backend/show/add')->with(compact('social_media', 'all_channels','all_categories','media'));
        }
    }
       
    // Edit Show
    public function edit_show($id, Request $request)
    {

        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Show');
        
        $categories = new Category();               
        //All categories of type show
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false);
        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );

        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();

        $assigned_categories = array();
        $show = Show::with('channels')->where('id',$id)->first();

        if($show->shows_has_categories->count() > 0){
            foreach($show->shows_has_categories as $cat){
                $assigned_categories[] = $cat->categories_id;
            }
        }
        //echo "<pre>";print_R($show);die('success');
        // Fetch channels of the show
        $show_has_channels = array();
        if($show->channels->count() > 0){
            foreach($show->channels as $channel){
                $show_has_channels[] = $channel->id;
            }
        }

        if ($request->isMethod('post')) {

            $update_data = $request->data;
            $show = new Show();
            //echo "<pre>";print_R($update_data);die('success');
            $update = $show->edit($id, $update_data);
            if($update){
                $message = "The Show details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the show. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/show/shows');
        }else{
            return view('backend/show/edit', compact('social_media', 'all_channels', 'show', 'show_has_channels','all_categories','assigned_categories','media'));
        }
    }

    // Delete Shows
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $show_id = $post_data['id'];
                $show = new Show();
                $delete = $show->deleteShow($show_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    /**
     * @description Get hosts for the show
     * @param $request is the request object
     */
    public function getHosts(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
             
                $show_id = $post_data['show_id'];
                $type    = $post_data['type'];
                $show    = new ShowsHasHost();
                $get     = $show->getHostsByType($show_id, $type);
               
                if($get){
                    return $get;
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }
}
