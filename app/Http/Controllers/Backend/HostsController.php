<?php

/**
 * @author : Contriverz   
 * @since  : 09-01-2019   
*/

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\Host;
use App\Models\Backend\User;
use App\Models\Backend\Profile;
use App\Models\Backend\HostType;
use App\Models\Backend\Setting;
use App\Helpers\ControllerHelper; 
use App\Models\Backend\MediaLinking;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;
use Session;


class HostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $selected_channel = "";
        $selected_channel = $request->input("channel_id");
        //Fetch all Channels
        $channels = new Channel();
        $all_export_channels = $channels->getAllChannels()->toArray();
        $all_channels = $channels->getAllChannels();
        $all_channels->put('', 'Any Channel');
       /* if($all_channels->count() > 0){
            $all_channels->put('', 'Any Channel');
        }*/
        $host_type = 0;
        if ($request->has('type')) {
            $host_type = $request->input('type');
        }
        return view('backend/user/hosts')->with(compact('all_channels', 'host_type', 'all_export_channels', 'selected_channel'));
    }

    /**
    * Add hosts banners
    */
    public function hosts_banners_add($id, Request $request){

         $media_model =  new MediaLinking();
         $media = $media_model->get_media($id, 'Host');
         
         $host    = Host::with('shows')->where('id', '=' , $id)->get()->toArray();
         $profile = Profile::where('users_id', '=',  $host[0]['users_id'])->get()->toArray();
      
        return view('backend/user/host_banners',compact('media','host','profile'));
    }

    /**
     * Show the application dashboard when calling thorugh ajax.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ajax_index(Request $request)
    {
        $hosts  = new Host();
        $draw   = 1;
        $type          = $request->input('type');
        $channel_id    = $request->input('channel_id');
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
      
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        $all_hosts   = $hosts->getAllHosts($type, $channel_id, $start, $length, $order, $dir, null, $search_value);
        $count_hosts = $hosts->countAllHosts($type, $channel_id, $order, $dir, null, $search_value);
        $data = array();
        $i    = 0;   
        $user_type = "'host'";
        
        if(!empty($all_hosts)){
          
            foreach($all_hosts as $host){          
                //echo "<pre>";print_R($host);die;     
                $data[$i][]  = html_entity_decode(showProfileImage($host->media_id, $host->media_type, $host->media_file));                         
                //$data[$i][]  = asset('storage/app/public/media/'.$host->media_id.'/'.$host->media_type.'/'.$host->media_file);                         
                $data[$i][]  = "<h4>".$host->title." ".$host->firstname." ".$host->lastname." ".$host->sufix."</h4>".strip_tags(str_limit($host->bio, $limit = 280, $end = '...'));  
                if($host->is_online == 1){
                    $data[$i][] = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
                }else if($host->is_online == 0){
                    $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Totally Inactive</button> ';
                } else if($host->is_online == 2){
                    $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Login Inactive</button> ';
                }   

               /* if($host->status == 1){
                    $data[$i][] = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
                }else if($host->status == 0){
                    $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Totally Inactive</button> ';
                } else if($host->status == 2){
                    $data[$i][] = '<button class="btn btn-warning disabled waves-effect waves-light btn-xs m-b-5">Login Inactive</button> ';
                } */  
                         
                $data[$i][]  = ' <a href="'.url('admin/user/hosts/edit/'.$host->id ).'"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteUser('.$host->id.', this, '.$user_type.')" ><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_hosts,
                    'recordsFiltered' => $count_hosts,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    /**
     * Add the Host.
     * @param $request as a Request Object
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add_host(Request $request)
    {
        // Get all memberships
        /*$memberships = array();
        $membership_obj  = new Membership;
        $all_memberships = $membership_obj->getAllMemberships();
        
        foreach($all_memberships as $membership){
            $memberships[$membership->id] = $membership->name;
        }*/
        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Host');
        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'linkedin'  => 'linkedin',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );


        // Fetch all prefix and suffix
        $setting = new Setting();
        $prefix_suffix = $setting->allPrefixSuffix();
       
        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        if($all_channels->count() > 0){
            $all_channels = $all_channels->toArray();
        }

        $itunes_categories = ControllerHelper::comboCategoriesSubcategories();

        if(!empty($itunes_categories)){
            $itunes_categories = json_encode($itunes_categories);
        }
       
        if ($request->isMethod('post')) {
          
            $data = $request->input('data');
            $data['User']['created_ip'] = $request->ip();
            $var  = "";
            $message  = "";
            $host     = new Host();
            $response = $host->add($data);
            if($response){
                $message = "New Host has been created successfully!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the host. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/hosts');
        }else{

            return view('backend/user/host_add', compact('prefix_suffix', 'all_channels', 'social_media', 'itunes_categories','media'));
            
        }
    }  

    /**
     * Edit Host.
     * @param $id as host id and $request as a request object 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_host($id, Request $request)
    {
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Host');

        $host_types_array  = array();
        $host_has_channels = array();

        // Fetch all prefix and suffix
        $setting = new Setting();
        $prefix_suffix = $setting->allPrefixSuffix();
        $updatedby  = '';
        $updated_at = '';

        //Bit Rate
        $bitrate = array(
                        '8'   => '8',
                        '32'  => '32',
                        '64'  => '64',
                        '96'  => '96',
                        '112' => '112',
                        '128' => '128',
                        '160' => '160',
                        '192' => '192',
                        '256' => '256',
                        '320' => '320'
                        );

        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'linkedin'    => 'linkedin',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );

        $itunes_categories = ControllerHelper::comboCategoriesSubcategories();
         //echo "<pre>";print_R($itunes_categories);die;
        if(!empty($itunes_categories)){
            $itunes_categories = json_encode($itunes_categories);
        }

        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        if($all_channels->count() > 0){
            $all_channels = $all_channels->toArray();
        }

        // Get host and its relational data
        $host = Host::with('user')->with('user.profiles')->with('user.user_settings')->with('hosts_has_host_types')->with('urls')->with('videos')->with('channels')->where('id',$id)->first();

        /**  Make profile url suggestions  **/ 
        $profile_urls = array();   

        /**  Suggestions from the username  **/ 
        if($host){
            if($host->user->count() > 0 ){
                if($host->user->profiles->count() > 0 ){                
                    foreach($host->user->profiles as $profile){
                        if(!empty($profile->title) && !empty($profile->firstname)){
                            $profile_urls[] = '/'.preg_replace("/[^a-zA-Z]/", "", strtolower($profile->title)).preg_replace("/[^a-zA-Z]/", "", strtolower($profile->firstname));
                        }if(!empty($profile->firstname) && !empty($profile->lastname)){
                            $profile_urls[] = '/'.preg_replace("/[^a-zA-Z]/", "", strtolower($profile->firstname)).preg_replace("/[^a-zA-Z]/", "", strtolower($profile->lastname));
                        }if(!empty($profile->title) && !empty($profile->sufix)){
                            $profile_urls[] = '/'.preg_replace("/[^a-zA-Z]/", "", strtolower($profile->title)).preg_replace("/[^a-zA-Z]/", "", strtolower($profile->firstname)).preg_replace("/[^a-zA-Z]/", "", strtolower($profile->lastname)).preg_replace("/[^a-zA-Z]/", "", strtolower($profile->sufix));
                        }                    
                    }                
                }
            } 
            
            /**  Suggestions from the username  **/    
            $get_show_names = $host->getHostShowNames();
            if(!empty($get_show_names)){
                foreach($get_show_names as $show_name){
                    $profile_urls[] = '/'.preg_replace("/[^a-zA-Z0-9_]/", "", str_replace(' ', '_', trim(strtolower($show_name))));
                }
            }
          
            //updated by 
            if($host->updated_by){
                $updatedby =Profile::where('users_id',$host->updated_by)->first();
            }
           
            if($host->hosts_has_host_types->count() > 0){
                foreach($host->hosts_has_host_types as $host_types){
                    $host_types_array[] = $host_types->host_types_id;
                }
            }
            if($host->channels->count() > 0){
                foreach($host->channels as $channel){
                    $host_has_channels[] = $channel->id;
                }
            }
        }
        
     
        if ($request->isMethod('put') || $request->isMethod('post')) {
            //echo "<pre>";print_R($request->all());die('success');
            $update_data = $request->data;
            if (empty($update_data['User']['password'])) {
                unset($update_data['User']['password']);
            }
            $host = new Host();
            $update = $host->edit($id, $update_data);
            if($update){
                $message = "The host details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the host. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/hosts');
        }else{
            return view('backend/user/host_edit', compact('host', 'prefix_suffix', 'social_media', 'itunes_categories', 'all_channels', 'host_types_array', 'host_has_channels', 'bitrate','updatedby','updated_at', 'profile_urls','media'));
        }
    }

    // Delete Hosts
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $host_id = $post_data['id'];
                $host = new Host();
                $delete = $host->deleteHost($host_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    public function export_hosts(Request $request){

        $data = $request->data;
        $hosts  = new Host();
        $users ='';
        $result = array();
        $filename ='';
        if($request->lebel =='undefined'){

            if($data['Host']['export_filter'] =='All'){

                $users = $hosts->eportHosts($type='',$channel_id='',$all=$data['Host']['export_filter'], $online='', $offline='', $featured='')->toArray();
                $filename =$data['Host']['export_filter'];

            }else if($data['Host']['export_filter'] =='Offline'){

                $users = $hosts->eportHosts($type='',$channel_id='',$all='', $online='', $offline=$data['Host']['export_filter'], $featured='')->toArray();
                $filename =$data['Host']['export_filter'];    

            }else if($data['Host']['export_filter'] =='Online'){

                $users = $hosts->eportHosts($type='',$channel_id='',$all='', $online=$data['Host']['export_filter'], $offline='', $featured='')->toArray();
                $filename =$data['Host']['export_filter'];

            }else{

                $users = $hosts->eportHosts($type='',$channel_id='',$all='', $online='', $offline='', $featured=$data['Host']['export_filter'])->toArray();
                $filename =$data['Host']['export_filter'];
            }      

        }else if($request->lebel =='Channels'){

            $channel_id = $data['Host']['export_filter'];
                $users = $hosts->eportHosts($type='',$channel_id,$all='', $online='', $offline='', $featured='')->toArray();
                $filename ='channels-'.$data['Host']['export_filter'];

        }else{

            if($data['Host']['export_filter'] =='Host'){

                $type =1;

                $users = $hosts->eportHosts($type, $channel_id='',$all='', $online='', $offline='', $featured='')
                         ->toArray();
                $filename ='type-'.$data['Host']['export_filter'];              

            }else if($data['Host']['export_filter'] =='Cohost'){

                $type =2;
                $users = $hosts->eportHosts($type, $channel_id='',$all='', $online='', $offline='', $featured='')
                         ->toArray();
                $filename ='type-'.$data['Host']['export_filter'];

            }else{

                $type =$data['Host']['export_filter'];
                $users = $hosts->eportHosts($type, $channel_id='',$all='', $online='', $offline='', $featured='')
                         ->toArray();
                $filename ='type-'.$data['Host']['export_filter'];
            }
        }

        $field = array('firstname' =>'firstname','lastname' =>'lastname','email' =>'email','phone' =>'phone','cellphone' =>'cellphone','skype' =>'skype','skype_phone' =>'skype_phone' );

        foreach ($users as $user) {
            //modification
            $result[] = (array)$user;  
        } 

        if(!empty($result)){          
            return Excel::create('hosts-'.$filename, function($excel) use ($result,$filename) {
                $excel->sheet('hosts-'.$filename, function($sheet) use ($result)
                {
                   $sheet->fromArray($result);
                });
            })->download('csv');

        }else{
            return Excel::create('hosts-'.$filename, function($excel) use ($field,$filename) {
                $excel->sheet('hosts-'.$filename, function($sheet) use ($field)
                {
                   $sheet->fromArray($field);
                });
            })->download('csv');           

        }
    }

    //this is commented because this function code merge to edit_profile function 
    public function custom_content($id,Request $request){
        // Get host and its relational data
        // $host = new Host();
        // $host_id = $host->getHostId($id);
        $user = Host::with('host_contents')->with('host_testimonials')->where('id',$id)->first();
        
        //$media = $media_model->get_media($id, 'Host');
        $host_content_media =MediaLinking::where('main_module','=','HostContent')->get();
        if ($request->isMethod('put') || $request->isMethod('post')) { 
            $update_data = $request->data;  
            $host = new Host();         
            $update = $host->edit_custom_content($id, $update_data); 
                  
            if($update){
                $message = "The details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the user. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/hosts/edit/custom_content/'.$id);

        }else{
            return view('backend.user.host_custom_content_edit', compact('user','host_content_media'));
        }
    } 
}
