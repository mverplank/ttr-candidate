<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Group;
use App\Models\Backend\User;
use Session;
use Validator;
use Auth;
use Mail;
use Redirect;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class ToolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Add thenews letter.
     *
     * @return Response
     */
    public function news_letter(Request $request){

        $groups = Group::select('id', 'desc')->pluck('desc', 'id')->toArray();
      if ($request->isMethod('post')) {
        $data = $request->input('data');
        $mailto = array();
        $subject = $data['Mailing']['subject'];
        $body  = $data['Content']['email_body'];

        foreach ($data['Mailing']['audience'] as $var) {
          if($var ==1){
            $user = User::select('email')->where('groups_id', 1)->get();
            foreach ($user as $mail){
                $mailto [] =$mail->email;
            }
          }
          if($var ==2){
                 $user = User::select('email')->where('groups_id', 2)->get();
                foreach ($user as $mail){
                    $mailto [] =$mail->email;
            }
          }
          if($var ==3){
             $user = User::select('email')->where('groups_id', 3)->get();
                        foreach ($user as $mail){
                            $mailto [] =$mail->email;
            }
          }
          if($var ==4){
             $user = User::select('email')->where('groups_id', 3)->get();
                        foreach ($user as $mail){
                            $mailto [] =$mail->email;
                        }
          }
        }
        foreach ($mailto as $to) {
            Mail::send([],[], function ($message)use ($to,$subject,$body) {
                $message->to($to)
                ->subject($subject)
                ->setBody($body);
            });
        }
            Session::flash('success' ,'successfully send.');
            return redirect('admin/mailing/composer');
      } else{

        return view('backend.tool.news_letter')->with(compact('groups'));

      } 

    }

    /**
     * Add thenews letter.
     *
     * @return Response
     */
    public function clear_cache(Request $request){
      \Artisan::call('cache:clear');
      Session::flash('success' ,'Cache cleared successfully.');
      return Redirect::back();
      //return Redirect::back()->withErrors(['msg', 'The Message']);
    }
}
