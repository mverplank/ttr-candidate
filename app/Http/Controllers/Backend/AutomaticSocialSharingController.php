<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Toolkito\Larasap\SendTo;
use App\Models\Backend\Episode;
use Storage;
/*use App\Models\Backend\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Backend\MediaLinking;
use DB;
use Auth;
use Session;*/

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class AutomaticSocialSharingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createClient($accessToken=null) {

        return new Facebook([
            'app_id' => '1039516709496883',
            'app_secret' => 'f450f29188cbe9ab3bd6a81c38ac4c21'
            /*,'default_access_token' => $accessToken ? $accessToken :
            (isset($_SESSION['facebook_access_token']) ? $_SESSION['facebook_access_token'] :
              Configure::read('Settings.facebook_app_id').'|'.Configure::read('Settings.facebook_app_secret')
            )*/
        ]);
    }

    public function getAccessToken() {

        $fb = $this->createClient();
        $helper = $fb->getRedirectLoginHelper();
        //echo "<pre>";print_R($helper->getAccessToken());die;
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
            //pr('Graph returned an error: ' . $e->getMessage());
            return [error => $e->getMessage()];
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            //pr('Facebook SDK returned an error: ' . $e->getMessage());
            return [error => $e->getMessage()];
        }

        if (isset($accessToken)) {

            // Logged in!
            //$_SESSION['facebook_access_token'] = (string) $accessToken;
            return (string) $accessToken;

        } elseif ($helper->getError()) {
            // The user denied the request
            //pr('user denied');
            return false;
        }

    }

    /**
     * Share the episode over the facebook on the scheduled date
     * Also update the entry which identifies that the data is being successfully shared over * the facebook. 
     */

    public function sendDataToSocialMedia()
    {
        /*$fb = $this->createClient();
        $fbt = $this->getAccessToken();
        echo "<pre>";print_R($fbt);die;*/
       
        /* $fb = new \Facebook\Facebook([
              'app_id' => '1039516709496883',
              'app_secret' => 'f450f29188cbe9ab3bd6a81c38ac4c21',
              'default_graph_version' => 'v3.2',
          ]);

        try {
          // Returns a `Facebook\FacebookResponse` object
          $response = $fb->get('/me?fields=id,name', '{access-token}');
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        $user = $response->getGraphUser();

        echo 'Name: ' . $user['name'];*/
        // OR
        // echo 'Name: ' . $user->getName();
        //die('success');

        //Share on Facebook
        $successfully_shared = 0;

        $base_url = url('/');
        $types    = ["episode", "segment1", "segment2", "segment3", "segment4"];
        foreach($types as $type){
            $update_column = 'fb_shared';
            $episode = new Episode();
            $check_data_to_share = $episode->getDataToShare($type, 'is_facebook', 'facebook_time', $update_column);  

            if($check_data_to_share->count() > 0){
                foreach($check_data_to_share as $epsd){
                   
                    $path = url('/').'/storage/app/public/media/'.$epsd->media_id.'/'.$epsd->type.'/'.$epsd->filename;
                   
                    $description  = '';
                    $epsd_schd_id = '';
                    if($epsd->episode_scheduled_shares->count() > 0){
                        $description  = $epsd->episode_scheduled_shares[0]->facebook_desc;
                        $epsd_schd_id = $epsd->episode_scheduled_shares[0]->id;
                    }

                    if (!file_exists( 'storage/app/public/media/'.$epsd->media_id.'/'.$epsd->type.'/'.$epsd->filename )) {
                        $path = url('/');
                    }
                    //$path = '';
                    /*SendTo::Facebook(
                        'link',
                        [
                            'link' => 'https://github.com/toolkito/laravel-social-auto-posting',
                            // 'link' => url('/'),
                            'message' => $epsd->title,
                            'description' => $description
                        ]
                    );*/
                    //if (file_exists( $path )) {
                       
                    try{
                        SendTo::Facebook(
                            'photo',
                            [
                                'photo'       => $path,
                                'message'     => $epsd->title,
                                'description' => $description
                            ]
                        );
                        $successfully_shared = 1;
                        $episode->updateSuccessfullySharedSchedule($epsd_schd_id, $update_column);
                       
                    }catch(\Exception $e){
                        echo $e->getMessage();die;
                        return false;
                    }
                }
            }else{
                //dd('No episode scheduled to share on facebook !!');
            }
           
            //Tweet on Twitter
            $column = 'twitter_shared';
            $t_path = '';
            $check_data_to_share = $episode->getDataToShare($type, 'is_twitter', 'twitter_time', $column);
           
            if($check_data_to_share->count() > 0){
                foreach($check_data_to_share as $epsd){
                    if(!empty($epsd->media_id)){
                        $t_path = 'storage/app/public/media/'.$epsd->media_id.'/'.$epsd->type.'/'.$epsd->filename;
                    }
                  
                    $description  = '';
                    $epsd_schd_id = '';
                    if($epsd->episode_scheduled_shares->count() > 0){
                        $description  = $epsd->episode_scheduled_shares[0]->twitter_desc;
                        $epsd_schd_id = $epsd->episode_scheduled_shares[0]->id;
                    }

                    if (!file_exists( $t_path )) {
                        $t_path = '';
                    }
                   
                    try{
                        //$options = ['status'=>'www.contriverz.com'];

                        if(!empty($t_path)){
                            
                            SendTo::Twitter(
                                $epsd->title." --- ".$description,
                                [
                                    $t_path
                                ]
                            );
                        }else{
                            
                            SendTo::Twitter($epsd->title." --- ".$description);
                        }
                        
                        $successfully_shared = 1;
                        $episode->updateSuccessfullySharedSchedule($epsd_schd_id, $column);
                        
                    }catch(\Exception $e){
                        return false;
                    }
                }
            }else{
               // dd('No episode scheduled to share on facebook !!');
            } 
        }
        

        //Sent message at last
        if($successfully_shared == 1){
            dd('Successfully Shared !!');
        }else{
            dd('No episode scheduled to share on facebook and twitter !!');
        }
    }
}
