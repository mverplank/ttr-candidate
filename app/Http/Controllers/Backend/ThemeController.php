<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Theme;
use App\Models\Backend\User;
use Session;
use Validator;
use Auth;
//use Mail;
use Redirect;
//use Illuminate\Support\Facades\Artisan;
//use Illuminate\Support\Facades\Storage;

class ThemeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Add thenews letter.
     *
     * @return Response
     */
    public function index(Request $request){
      return view('backend/cms/themes');
    }

    private function setCMSThemes($categoryId, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null) {
        $order_field     = 'theme_name';
        $order_direction = 'ASC';
        $page_obj = new Theme();
        $data = $page_obj->fetchThemes($categoryId, $order_field, $order_direction, $start, $length, $order, $dir, $column_search, $search_value, $search_regex);
        $cdata = $page_obj->countThemes($categoryId, $order_field, $order_direction, $order, $dir, $column_search, $search_value, $search_regex);
        // build order and direction human readable description 
        return ['data'=>$data, 'cdata'=>$cdata];
    }

    public function ajax_cms_themes_index(Request $request){
     
        $draw          = 1;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 

        $cmsThemeData = $this->setCMSThemes(0, $start, $length, $order, $dir, null, $search_value);
        //$categoryId, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null
        $all_theme_data   = $cmsThemeData['data'];
        $count_theme_data = $cmsThemeData['cdata'];
       
        $data = array();
        $i    = 0;   

        if(!empty($all_theme_data)){  
            foreach($all_theme_data as $theme_data){

                $online = '<h4>'.$theme_data->theme_name.'<h4>';
                $data[$i][] = $online;
                $data[$i][] = $theme_data->channel_name;
                $data[$i][] = '<a href="'.url('admin/cms/themes/edit/'.$theme_data->t_id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" onclick="javascript:void(0)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $data[$i]['DT_RowId']    = $theme_data->t_id;
                $data[$i]['DT_RowClass'] = 'off_'.$theme_data->off;
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_theme_data,
                    'recordsFiltered' => $count_theme_data,
                    'data' => $data,

                );

        echo json_encode($output);
        exit();
    }

    /**
     * Add cms pages .
     *
     * @return get method return view post method add new 
    */
    public function add(Request $request){
       $themes = Theme::all()->pluck('name', 'id')->toArray();
        if ($request->isMethod('post')) {           
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $page = new Theme();
            $response = $page->add($data);
            if($response){
                $message = "New Theme successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the Theme. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/themes');   
        }else{
            return view('backend/cms/add_themes')->with(compact('themes'));
        }
    }

    /**
     * edit cms pages .
     *
     * @return get method return view post method update 
    */
    public function edit($id, Request $request){
        $theme = Theme::where('id', '=', $id)->first();
        /*echo '<pre>';
        print_r($theme);
        die;*/
        //echo"<pre>";print_r(json_decode($theme->data_json));die;
        $fontweight = array('' =>'not set','normal'=>'normal','bold'=>'bold','bolder'=>'bolder','lighter'=>'lighter','100'=>'100','100'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900', );
        $fontstyle = array('' =>'not set','none'=>'none','underline'=>'underline','overline'=>'overline','line-through'=>'line-through' );
        $fontalign = array('' =>'not set','left'=>'left','right'=>'right','center'=>'center','justify'=>'justify' );

        $position = array('left top' =>'left top','left center'=>'left center','left bottom'=>'left bottom','right top'=>'right top','right center'=>'right center',    'right bottom'=>'right bottom','center top'=>'center top','center center'=>'center center','center bottom'=>'center bottom',);

        $repeat = array('repeat' =>'repeat','no-repeat'=>'no-repeat');

        $attachment = array('scroll' =>'scroll','fixed'=>'fixed','local'=>'local');

        $size = array('auto' =>'auto','cover'=>'cover','contain'=>'contain');

        if ($request->isMethod('post')) {           
            $data = $request->input('data');
           // echo"<pre>";print_r($data);die;
            $var  = "";
            $message  = "";
            $page = new Theme();
            $response = $page->edit($id, $data);
            if($response){
                $message = "The Theme details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the Theme. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/themes/edit/'.$id);   
        }else{

            return view('backend/cms/edit_themes')->with(compact('theme','fontweight','fontstyle','fontalign','position','repeat','attachment','size'));
        }
    }

    // Delete Shows
    // public function delete(Request $request){

    //     if ($request->isMethod('post')) {
    //         $post_data =  $request->all();
    //         if(!empty($post_data)){
    //             $page_id = $post_data['id'];
    //             $page = new Page();
    //             $delete = $page->deletePage($page_id);
    //             if($delete){
    //                 return array('status' => 'success');
    //             }else{
    //                 return array('status' => 'error');
    //             }
    //         }else{
    //             return array('status' => 'error');
    //         }
    //     }
    // }

}
