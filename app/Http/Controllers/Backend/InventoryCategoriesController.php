<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\InventoryCategory;
use Illuminate\Support\Facades\Validator; //add by parmod
use Session;

class InventoryCategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function add_item_categories(Request $request){
        /* Validator is used to validate all the details which are recived in the $request */
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:inventory_categories',
        ]);

        /* fails() will return true only if any of details which validator checks is no valid */
        if ($validator->fails()) {
           $errors = $validator->getMessageBag()->toArray();
           return response()->json(['validation_error'=>true,'message'=>"The name field is unique and required"]);
        } else {
            $ItemName = $request->name;
            $inventory_categories = new InventoryCategory();
            $inventory_categories->name = $ItemName; 
            $addCate = $inventory_categories->save();       
            if($addCate){
               return response()->json(['seccess'=>true,'message'=>"Add successfully"]);
            }   
        }
    }
    // List out the item categories
    public function index(Request $request)
    {
        // $inventory_categories = new InventoryCategory();
        //$all_inventory_categories = $inventory_categories->allInventoryCategories();
        //echo "<pre>";print_R($all_inventory_categories);die('suscsca');        
        //return view('backend/bonus_items/categories')->with(compact('all_inventory_categories'));
        return view('backend/bonus_items/categories');
    }

    // List out the item categories
    public function ajax_index(Request $request)
    {

        $inventory_categories = new InventoryCategory();
        $draw    = 1;
        $type    = $request->input('type');
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }  

        $all_inventory_categories = $inventory_categories->allInventoryCategories($start, $length, $order, $dir, null, $search_value);
  
        $count_inventory_categories = $inventory_categories->countInventoryCategories($order, $dir, null, $search_value);

        $data = array();
        $i    = 0;   
        if(!empty($all_inventory_categories)){
            foreach($all_inventory_categories as $item_cat){
               
                $data[$i][]  = $item_cat->name;
                $data[$i][]  = ' <a href="javascript:void(0)" onclick="deleteInventoryCategory('.$item_cat->id.', this)" ><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_inventory_categories,
                    'recordsFiltered' => $count_inventory_categories,
                    'data' => $data
                );

        echo json_encode($output);
        exit();

        //$all_inventory_categories = $inventory_categories->allInventoryCategories();
        //return view('backend/bonus_items/categories')->with(compact('all_inventory_categories'));
    }

    // Delete Item categories
    public function delete_inventory_categories(Request $request){
       
        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $cate_id = $post_data['id'];
                $categories = new InventoryCategory();
                $delete = $categories->deleteCategory($cate_id);
                if($delete){
                   return array('status' => 'success');
                }else{
                   return array('status' => 'error');
                }
            }else{
               return array('status' => 'error');
            }
        }
    } 
}
