<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Activity;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;
use Session;

class ActivitiesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
      /*  $activities = new Activity();
        $all_activities   = $activities->getAllActivities();
        return view('backend/user/activities')->with(compact('all_activities'));
*/        return view('backend/user/activities');
    }

    public function ajax_index(Request $request)
    {
        $activities = new Activity();
        $draw   = 1;
        $start  = $request->input('start');
        $length = $request->input('length');
        $draw   = $request->input('draw');
        $order  = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");
       // echo "column = <pre>";print_r($_POST);die;
        /*$column_search_value = array();
        $column_search_name  = array();
        $i=0;
        
        echo $search_value."<br>";
        echo "column = <pre>";print_r($column_search);die;*/
       /* if(!empty($search_value)){

        }*/
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
               // echo "<pre>";print_R($columns[$col]);
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }
    
        $all_activities   = $activities->getAllActivities($start, $length, $order, $dir, null, $search_value);
        $count_activities = $activities->countActivities($order, $dir, null, $search_value);
        //echo "<pre>";print_R($all_activities);die;
        $data = array();
        $i = 0;   
        foreach($all_activities as $activity){
            $currentDateTime = $activity->created;
            $newDateTime = date('m/d/Y h:i A', strtotime($currentDateTime));
            $data[$i][]  = $newDateTime;
            $data[$i][]  = $activity->username;
            $data[$i][]  = $activity->activity;
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_activities,
                    'recordsFiltered' => $count_activities,
                    'data' => $data
                );

        echo json_encode($output);
    }

    // Export transaction report
    public function export_transaction_report(){ 
       $result = array();
       $reports =DB::table('activities')
                   ->leftJoin('users', 'activities.users_id', '=', 'users.id')    
                  ->where(function($q) {
                      $q->where('users.id', '!=', Auth::id())
                      ->where('users.deleted',0);
                  })
                  ->select('users.username', 'activities.activity', 'activities.created')
                  ->orderBy('activities.created', 'asc')
                  ->get()->toArray(); 
                   foreach ($reports as $report) {
                       //modification
                       $result[] = (array)$report;  
                   } 

       return Excel::create('transaction_reports', function($excel) use ($result) {
          $excel->sheet('transaction_report', function($sheet) use ($result)
          {
              $sheet->fromArray($result);
          });
        })->download('csv');
    }
}
