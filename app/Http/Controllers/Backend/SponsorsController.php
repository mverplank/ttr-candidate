<?php

/**
 * @author: Conrverz
 * @since : 21-01-2018 Monday
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Sponsor;
use App\Models\Backend\Channel;
use App\Models\Backend\Host;
use App\Models\Backend\Profile;
use App\Models\Backend\Episode;
use App\Models\Backend\HostsHasSponsor;
use App\Models\Backend\MediaLinking;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;
use Session;
use Carbon\Carbon;

class SponsorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the sponsors list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $channels = new Channel();
        $sponsors = new Sponsor();
        $hosts    = new Host();
        $selected_channel = 0;
        $selected_host    = 0;
        if($request->has("channel_id")){
            $selected_channel = $request->input("channel_id");
        }if($request->has("host_id")){
            $selected_host    = $request->input("host_id");
        }
        
        //Fetch channels for export
        $all_export_channels = $channels->getAllChannels()->toArray(); 

        //Fetch all channels
        //$all_channels = new \stdClass;        
        $all_channels = $channels->getAllChannels();
        $all_channels->put(0, 'Any Channel');

        //Fetch all hosts
        $all_hosts = $hosts->getHostsForFilter(1);
        $all_hosts->put(0, 'All Hosts');

        $is_online = 2;
        if ($request->has('is_online')) {
            $is_online = $request->input('is_online');
        }
        return view('backend/advertising/sponsors')->with(compact('is_online', 'all_export_channels', 'all_channels', 'all_hosts', 'selected_channel', 'selected_host' ));
    }

    public function ajax_index(Request $request)
    {
        $sponsors      = new Sponsor();
        $draw          = 1;
        $status        = 2;
        $channel_id    = 0;
        $host_id       = 0;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        if($request->exists('is_online')){
            $status  = $request->input('is_online');
        }
        
        if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
        }if ($request->has('host_id')) {
            $host_id = $request->input('host_id');
        }
        
        $all_sponsors = $sponsors->getAllSponsors($host_id, $channel_id, $status, $start, $length, $order, $dir, null, $search_value);
        $count_sponsors = $sponsors->countAllSponsors($host_id, $channel_id, $status, $order, $dir, null, $search_value);

        $data = array();
        $i    = 0;

        if(!empty($all_sponsors)){
            foreach($all_sponsors as $sponsor){
                $data[$i][]  = $sponsor->firstname;
                $data[$i][]  = $sponsor->lastname;
                $data[$i][]  = $sponsor->company;
                if($sponsor->is_online == 1){
                    $data[$i][] = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
                }else{
                    $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Inactive</button> ';
                }
                if($sponsor->is_featured == 1) {
                    $data[$i][] = '<i class="ion-checkmark"></i>';
                } else{
                    $data[$i][] = '';
                } 

                if(!empty($sponsor->start_date)){
                    $data[$i][] = Carbon::parse($sponsor->start_date)->format('m/d/Y');
                } else{
                    $data[$i][] = '';
                }

                if(!empty($sponsor->start_date)){
                    $data[$i][] = Carbon::parse($sponsor->end_date)->format('m/d/Y');
                } else{
                    $data[$i][] = '';
                }
               
                $data[$i][] = '<a href="'.url('admin/advertising/sponsors/episodes/'.$sponsor->id ).'"><i class="glyphicon glyphicon-th-list" title="View Sponsored Episodes"></i></a>
                                    <a href="'.url('admin/advertising/sponsors/edit/'.$sponsor->id ).'"><i class="glyphicon glyphicon-pencil" title="Edit"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteSponsor('.$sponsor->id.', this)" ><i class="glyphicon glyphicon-trash" title="Delete"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_sponsors,
                    'recordsFiltered' => $count_sponsors,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    // Add Sponsors
    public function add_sponsor(Request $request)
    {
        //Fetch all channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        
        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'linkedin'  => 'linkedin',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );

        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Sponsor');

        if ($request->isMethod('post')) {
           
            $data = $request->input('data');      
            $var  = "";
            $message  = "";
            $sponsor  = new Sponsor();
            $response = $sponsor->add($data);
            if($response){
                $message = "New Sponsor successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the sponsor. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/advertising/sponsors');
        }else{
            return view('backend/advertising/sponsor_add')->with(compact('all_channels', 'social_media','media'));
        }
    }
       
    // Edit Sponsors
    public function edit_sponsor($id, Request $request)
    {
        //Fetch all channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        //media 
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Sponsor');
        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'linkedin'  => 'linkedin',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );
        $sponsor = '';
        $assigned_channels = array();
        $sponsor = Sponsor::with('channels')->with('hosts')->where('id',$id)->first();
     
        if($sponsor->channels->count() > 0){
            foreach($sponsor->channels as $chn){
                $assigned_channels[] = $chn->id;
            }
        }

        if ($request->isMethod('post')) {

            $update_data = $request->data;
            $sponsor     = new Sponsor();
            $update      = $sponsor->edit($id, $update_data);
            if($update){
                $message = "The sponsor details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the sponsor. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/advertising/sponsors');
        }else{
            return view('backend/advertising/sponsor_edit', compact('sponsor', 'all_channels', 'social_media', 'assigned_channels','media'));
        }
    }

    // Delete Sponsors
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $sponsor_id = $post_data['id'];
                $sponsor = new Sponsor();
                $delete = $sponsor->deleteSponsor($sponsor_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    // Export Sponsor
    public function export_sponsor(Request $request){

        $data = $request->data;
        $sponsors  = new Sponsor();
        $users ='';
        $result = array();
        $filename ='';

        if($request->lebel =='undefined'){

            if($data['Sponsor']['export_filter'] =='All'){

                $users = $sponsors->eportSponsors($channel_id='',$all=$data['Sponsor']['export_filter'], $online='', $offline='', $featured='')->toArray();
                $filename =$data['Sponsor']['export_filter'];

            }else if($data['Sponsor']['export_filter'] =='Online'){

                $users = $sponsors->eportSponsors($channel_id='',$all='', $online=$data['Sponsor']['export_filter'], $offline='', $featured='')->toArray();
                $filename =$data['Sponsor']['export_filter'];                 

            }else if($data['Sponsor']['export_filter'] =='Offline'){

                $users = $sponsors->eportSponsors($channel_id='',$all='', $online='', $offline=$data['Sponsor']['export_filter'], $featured='')->toArray();
                $filename =$data['Sponsor']['export_filter'];            

            }else{

                $users = $sponsors->eportSponsors($channel_id='',$all='', $online='', $offline='', $featured=$data['Sponsor']['export_filter'])->toArray();
                $filename =$data['Sponsor']['export_filter'];              

            }                                                   
 
        }else{

            $channel_id = $data['Sponsor']['export_filter'];
            $users = $sponsors->eportSponsors($channel_id,$all='', $online='', $offline='', $featured='')
                ->toArray();

            $filename ='channel-'.$data['Sponsor']['export_filter'];
        }
        $field = array('firstname' =>'firstname','lastname' =>'lastname','email' =>'email','phone' =>'phone','cellphone' =>'cellphone','skype' =>'skype','skype_phone' =>'skype_phone' );
        foreach ($users as $user) {
            //modification
            $result[] = (array)$user;  
        } 
        if(!empty($result)){          
            return Excel::create('sponsors-'.$filename, function($excel) use ($result,$filename) {
               $excel->sheet('sponsors-'.$filename, function($sheet) use ($result)
               {
                  $sheet->fromArray($result);
               });
            })->download('csv');

        }else{
           return Excel::create('sponsors-'.$filename, function($excel) use ($field,$filename) {
               $excel->sheet('sponsors-'.$filename, function($sheet) use ($field)
               {
                  $sheet->fromArray($field);
               });
           })->download('csv');           
        }
    }
    
    /**
    * @description Search the Host, Cohost and Podcast Host for the suggestions 
    * @param $request id the object of the Request class
    * @return json values for the select options
    */
    public function host_multisearch(Request $request){

        $searchtext = $request->text;
        $type       = $request->type;
        //echo "<pre>";print_r($type);die();
        $sponsor    = new Sponsor();
        if($type == 'Guest'){
            $result = $sponsor->GuestSearch($searchtext, $type);
        }else if($type == 'Sponsor'){
            $result = $sponsor->SponsorSearch($searchtext, $type);
        }else{
            $result = $sponsor->hostSearch($searchtext, $type);
        }
       
        return $result;
    }

    /**
    * @description Get the sponsors host if any
    * @param $request is the request object
    */
    public function getHosts(Request $request){
        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                
                $sponsor_id = $post_data['sponsor_id'];
                $hosts      = new HostsHasSponsor();
                $get        = $hosts->getHostsByType($sponsor_id);
               
                if($get){
                    return $get;
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    /**
    * @description Get the episodes assigned to  the sponsors if any
    * @param $request is the request object
    * @param $id is the sponsor id
    */
    public function episodes($id, Request $request){
       
        if($id != 0){
            $sponsor = Sponsor::find($id);
            if ($request->isMethod('post') || $request->isMethod('put')) {
                $draw          = 1;
                $start         = $request->input('start');
                $length        = $request->input('length');
                $draw          = $request->input('draw');
                $order         = $request->post("order");
                $search_arr    = $request->post("search");
                $search_value  = $search_arr['value'];
                $search_regex  = $search_arr['regex'];
                $columns       = $request->post("columns");
                $count_banners = 0;
                $col = 0;
                $dir = "";
                if(!empty($order)) {
                    foreach($order as $o) {
                        $col   = $o['column'];
                        $dir   = $o['dir'];
                        $order = $columns[$col]['name'];
                    }
                }
             
                if($dir != "asc" && $dir != "desc") {
                    $dir = "asc";
                } 
                
                $episodeObj = new Episode();
                $get_sponsor_episodes = $episodeObj->getAllSponsorEpisodes($id, $start, $length, $order, $dir, null, $search_value);

                $count_sponsor_episodes = $episodeObj->countAllSponsorEpisodes($id, $order, $dir, null, $search_value);
                $data = array();
                $i    = 0;
                if(!empty($get_sponsor_episodes)){
                    foreach($get_sponsor_episodes as $episode){
                        //echo "<pre>";print_R($episode);die('success');
                        $data[$i][] = date_format(date_create($episode->Episode__fulldate),"M dS Y g:i A");
                        $episode_title  = $episode->Episode__title."<br>";
                        
                        if($episode->hosts->count() > 0){
                            $episode_title .= '<div class="l-hosts text-teal">Hosts:</div>';
                            foreach($episode->hosts as $host){
                                if($host->pivot){
                                    if($host->pivot->type == "host"){
                                        $episode_title .= '<div class="tags">'.$host->user->profiles[0]->name.'</div><br>';
                                    }
                                }
                            }
                        }
                        if($episode->hosts->count() > 0){
                            $episode_title .= '<div class="l-cohosts text-purple">Co-Hosts:</div>';
                            foreach($episode->hosts as $host){
                                if($host->pivot){
                                    if($host->pivot->type == "cohost"){
                                        $episode_title .= '<div class="tags">'.$host->user->profiles[0]->name.'</div><br>';
                                    }
                                }
                            }
                            
                        }
                        if($episode->guests->count() > 0){
                            $episode_title .= '<div class="l-guests text-orange">Guests:</div>';
                            foreach($episode->guests as $guest){
                                $episode_title .= '<div class="tags">'.$guest->firstname.' '.$guest->lastname.'</div>';
                            }                            
                        }

                        $episode_title .= "";
                        $episode_title .= "";
                        $data[$i][]  = $episode_title;
                        $data[$i][]  = '<a href="'.url('admin/advertising/sponsors/episodes/'.$episode->s_episode_id ).'"><i class="glyphicon glyphicon-th-list" title="View Sponsored Episodes"></i></a>';
                        $i++;
                    } 
                }
                $output = array(
                            'draw'            => $draw,
                            'recordsTotal'    => $count_sponsor_episodes,
                            'recordsFiltered' => $count_sponsor_episodes,
                            'data'            => $data
                        );

                echo json_encode($output);
                exit();
            }
            return view('backend/advertising/sponsor_episodes')->with(compact('id', 'sponsor'));
        }
    }
}
