<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Setting;
use App\Models\Backend\User;
use App\Models\Backend\Show;
use Session;
use Auth;
/*use Validator;

*/


class ConfigurationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($channel_id='')
    {
        
    }

    public function ajax_index(Request $request)
    {
        
    }

    /**
     * Add the Base configration
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function base_cofig(Request $request)           
    {   
        $data = array();
        $email = Setting::where('name','=','email')->first();
        $default_domain = Setting::where('name','=','default_domain')->first();
        $meta_title = Setting::where('name','=','meta_title')->first();
        $meta_description = Setting::where('name','=','meta_description')->first();
        $time_format = Setting::where('name','=','time_format')->first();
        $date_format = Setting::where('name','=','date_format')->first();
        $date_delimiter = Setting::where('name','=','date_delimiter')->first();
        $recaptcha_sitekey = Setting::where('name','=','recaptcha_sitekey')->first();
        $recaptcha_secretkey = Setting::where('name','=','recaptcha_secretkey')->first();
        $rss_ttl = Setting::where('name','=','rss_ttl')->first();
        $rss_item_desc_limit = Setting::where('name','=','rss_item_desc_limit')->first();
        $rss_items_limit = Setting::where('name','=','rss_items_limit')->first();
        $facebook_app_id = Setting::where('name','=','facebook_app_id')->first();
        $facebook_app_secret = Setting::where('name','=','facebook_app_secret')->first();
        $facebook_auth_app_id = Setting::where('name','=','facebook_auth_app_id')->first();
        $facebook_auth_app_secret = Setting::where('name','=','facebook_auth_app_secret')->first();
        $twitter_app_key = Setting::where('name','=','twitter_app_key')->first();
        $twitter_app_secret = Setting::where('name','=','twitter_app_secret')->first();
        $create_episodes_for_empty_shows = Setting::where('name','=','create_episodes_for_empty_shows')->first();
        $mailing__host_no_episodes_details = Setting::where('name','=','mailing__host_no_episodes_details')->first();
        $custom_podcasts_channel_id = Setting::where('name','=','custom_podcasts_channel_id')->first();
        $studioapp_client_path = Setting::where('name','=','studioapp_client_path')->first();
        $studioapp_server_path = Setting::where('name','=','studioapp_server_path')->first();
        $studioapp_server_url = Setting::where('name','=','studioapp_server_url')->first();
        $studioapp_url = Setting::where('name','=','studioapp_url')->first();

        $data = array('base_url'=>$email->value,'default_domain'=>$default_domain->value,'meta_title'=>$meta_title->value,'meta_description'=>$meta_description->value,'time_format'=>$time_format->value,'date_format'=>$date_format->value,'date_delimiter'=>$date_delimiter->value,'recaptcha_sitekey'=>$recaptcha_sitekey->value,'recaptcha_secretkey'=>$recaptcha_secretkey->value,'rss_ttl'=>$rss_ttl->value,'rss_item_desc_limit'=>$rss_item_desc_limit->value,'rss_items_limit'=>$rss_items_limit->value,'facebook_app_id'=>$facebook_app_id->value,'facebook_app_secret'=>$facebook_app_secret->value,'facebook_auth_app_id'=>$facebook_auth_app_id->value,'facebook_auth_app_secret'=>$facebook_auth_app_secret->value,'twitter_app_key'=>$twitter_app_key->value,'twitter_app_secret'=>$twitter_app_secret->value,'create_episodes_for_empty_shows'=>$create_episodes_for_empty_shows->value,'mailing__host_no_episodes_details'=>$mailing__host_no_episodes_details->value,'custom_podcasts_channel_id'=>$custom_podcasts_channel_id->value,'studioapp_client_path'=>$studioapp_client_path->value,'studioapp_server_path'=>$studioapp_server_path->value,'studioapp_url'=>$studioapp_url->value,'studioapp_server_url'=>$studioapp_server_url->value);

        if ($request->isMethod('post')) {
            $data = $request->input('data'); 
                Setting::where('name','=','email')->update(['value' =>$data['Configuration']['email']]);
                Setting::where('name','=','default_domain')->update(['value' =>$data['Configuration']['default_domain']]);
                Setting::where('name','=','meta_title')->update(['value' =>$data['Configuration']['meta_title']]);
                Setting::where('name','=','meta_description')->update(['value' =>$data['Configuration']['meta_description']]);
                Setting::where('name','=','time_format')->update(['value' =>$data['Configuration']['time_format']]);
                Setting::where('name','=','date_format')->update(['value' =>$data['Configuration']['date_format']]);
                Setting::where('name','=','date_delimiter')->update(['value' =>$data['Configuration']['date_delimiter']]);
                Setting::where('name','=','recaptcha_sitekey')->update(['value' =>$data['Configuration']['recaptcha_sitekey']]);
                Setting::where('name','=','recaptcha_secretkey')->update(['value' =>$data['Configuration']['recaptcha_secretkey']]);
                Setting::where('name','=','rss_ttl')->update(['value' => $data['Configuration']['rss_ttl']]);
                Setting::where('name','=','rss_item_desc_limit')->update(['value' =>$data['Configuration']['rss_item_desc_limit']]);
                Setting::where('name','=','rss_items_limit')->update(['value' =>$data['Configuration']['rss_items_limit']]);
                Setting::where('name','=','facebook_app_id')->update(['value' =>$data['Configuration']['facebook_app_id']]);
                Setting::where('name','=','facebook_app_secret')->update(['value' =>$data['Configuration']['facebook_app_secret']]);
                Setting::where('name','=','facebook_auth_app_id')->update(['value' =>$data['Configuration']['facebook_auth_app_id']]);
                Setting::where('name','=','facebook_auth_app_secret')->update(['value' => $data['Configuration']['facebook_auth_app_secret']]);
                Setting::where('name','=','twitter_app_key')->update(['value' =>$data['Configuration']['twitter_app_key']]);
                Setting::where('name','=','twitter_app_secret')->update(['value' =>$data['Configuration']['twitter_app_secret']]);
                Setting::where('name','=','create_episodes_for_empty_shows')->update(['value' =>$data['Configuration']['create_episodes_for_empty_shows']]);
                Setting::where('name','=','mailing__host_no_episodes_details')->update(['value' =>$data['Configuration']['mailing__host_no_episodes_details']]);
                Setting::where('name','=','custom_podcasts_channel_id')->update(['value' =>$data['Configuration']['custom_podcasts_channel_id']]);
                Setting::where('name','=','studioapp_client_path')->update(['value' =>$data['Configuration']['studioapp_client_path']]);
                Setting::where('name','=','studioapp_server_path')->update(['value' =>$data['Configuration']['studioapp_server_path']]);
                Setting::where('name','=','studioapp_server_url')->update(['value' =>$data['Configuration']['studioapp_server_url']]);
                Setting::where('name','=','studioapp_url')->update(['value' =>$data['Configuration']['studioapp_url']]);

                Session::flash('success' ,'Base configurations has been added successfully.');               
                return redirect('admin/cms/configurations/base');
        }else{

            return view('backend/configuration/base')->with(compact('data'));

        }
    }

    public function shows_ajax_agenda(Request $request)
    {
        $channels  = new Channel();
        $draw      = 1;
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
       
        $all_channels = $channels->getChannels($start, $length, $order, $dir, null, $search_value);
        $count_channels = $channels->countChannels($order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   

        if(!empty($all_channels)){
            foreach($all_channels as $channel){
               
                $data[$i][]  = '<h4>'.$channel->name.'<h4>';
                $data[$i][]  = strlen($channel->description) > 400 ? substr($channel->description,0,400).'...' : $channel->description;
                                             
                                   
                $data[$i][]  = '<a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit Schedule"><i class="glyphicon glyphicon-time"></i></a>
                                    <a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit Website Content"><i class="glyphicon glyphicon-blackboard"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteChannel('.$channel->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $all_channels,
                    'recordsFiltered' => $count_channels,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    
    /**
     * @description Add the prefix ans sufix to the system
     * @param $request is the Request object 
     * @return view or route
     */
    public function prefix_sufix(Request $request)           
    {
       if (Auth::check()) {
            $user = Auth::user();
            if(\Config::get('constants.ADMIN') == $user->groups_id){
                $contentObj  = new Setting();
                $all_content = $contentObj->fetchPrefixSufixToShow();
                $prefix = $suffix = array();
                if(!empty($all_content)){
                    $prefix = $all_content['prefix'];
                    $suffix = $all_content['sufix'];
                }

                // Update the content into the settings
                if ($request->isMethod('post')) {
                    $update_data = $request->data;
                    $update      = $contentObj->updatePrefixSufix($update_data);
                    if($update){
                        $message = "The Configuration has been saved";
                        $var     = "success";
                    }else{
                        $message = "There is some problem while saving the configuration. Please try again.";
                        $var     = "error";
                    }
                    Session::flash($var , $message);
                    return redirect()->route('prefix_sufix');
                }
                return view('backend/configuration/prefix_sufix')->with(compact('prefix', 'suffix'));
            }
            else{
                $message = "You don't have acces for this page";
                $var     = "error";
                Session::flash($var , $message);
                return redirect('/login');
            }
        }
    }

    /**
     * @description Add payments to the system
     * @param $request is the Request object 
     * @return view or route
     */
    public function payments(Request $request)           
    {
        // Add payments
        $data = array();
        $currency = Setting::where('name','=','default_currency')->first();
        $currency_arr = json_decode($currency['options'], true);
        $payments_mode = Setting::where('name','=','payments_mode')->first();
        $payments_mode_arr = json_decode($payments_mode['options'], true);
        $payments_paypal_enabled = Setting::where('name','=','payments_paypal_enabled')->first();
        $payments_paypal_email = Setting::where('name','=','payments_paypal_email')->first();
        $data = array(
                'currency'=>$currency_arr['options'],
                'payments_mode'=>$payments_mode_arr['options']
                );
        //echo"<pre>";print_r($payments_mode->value);die();
        if ($request->isMethod('post')) {
            $data = $request->data;
            //echo"<pre>";print_r($data);die();
                Setting::where('name','=','default_currency')->update(['value' =>$data['Configuration']['default_currency']]);
                Setting::where('name','=','payments_mode')->update(['value' =>$data['Configuration']['payments_mode']]);
                if(isset($data['Configuration']['payments_paypal_enabled'])){
                    Setting::where('name','=','payments_paypal_enabled')->update(['value' =>$data['Configuration']['payments_paypal_enabled']]);
                }else{
                    Setting::where('name','=','payments_paypal_enabled')->update(['value' =>'0']);
                }
                Setting::where('name','=','payments_paypal_email')->update(['value' =>$data['Configuration']['payments_paypal_email']]);

                Session::flash('success' ,'Payments configurations has been added successfully.');               
                return redirect('admin/cms/configurations/payments');
        }else{

            return view('backend/configuration/payments')->with(compact('data','currency','payments_mode','payments_paypal_enabled','payments_paypal_email'));

        }
    }


}
