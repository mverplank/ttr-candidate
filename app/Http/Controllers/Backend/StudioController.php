<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\Studio;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
/*use App\Models\Backend\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Backend\MediaLinking;
use DB;
use Auth;
use Session;*/

class StudioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $channel = new Channel();
        return view('backend/studio/channels');
    }

    public function ajax_index(Request $request)
    {
        $channel       = new Channel();      
        $draw          = 1;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        

        if(!empty($search_arr)){
            $search_value = $search_arr['value'];
        }if(!empty($search_arr)){
            $search_regex = $search_arr['regex'];
        }

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }    

        $studio_channels       = $channel->getStudioChannels($start, $length, $order, $dir, null, $search_value);
        $count_studio_channels = $channel->countStudioChannels($order, $dir, null, $search_value);
        
        $data = array();
        $i = 0;   
       
        foreach($studio_channels as $channel){
            $data[$i][]  = $channel->name;            
            $data[$i][]  = '<a href="'.url('/admin/studio/channel/app/'.$channel->id).'" title="Open Channel StudioApp"><i class="ion-mic-c" title=""></i></a>';
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_studio_channels,
                    'recordsFiltered' => $count_studio_channels,
                    'data' => $data
                );

        echo json_encode($output);
        exit();        
    }

    /**
     * Streaming the url for now.
     * @param $id is the channel id of the studio app
     */
    public function studio(Request $request, $id){
       
        //$url = 'http://edgev1.den.echo.liquidcompass.net/KKNWAMMP3';
        // $url = 'https://www.thedrpatshow.com/shows/drp-150407-conner.mp3';
        //$url = storage_path('app/public').'/media/598/audio/1557218501.mp3';
        

        //$url = storage_path('app/public').'/media/623/video/1557374928.mp4';
        // echo $url;die;
        //$url = 'http://50.62.144.90/shows/upc-150123-mollan-masters.mp3';

        //Live Video
        //$url = storage_path('app/public').'/media/248/video/1554454263.mp4';
        //Live Audio
        $url = storage_path('app/public').'/media/237/audio/1554454108.mp3';


        $response = new BinaryFileResponse($url);
        //BinaryFileResponse::trustXSendfileTypeHeader(); 
        //echo "<pre>";print_R($response);die;
        /*return response($response)
            ->header('Content-Type', 'audio/mpeg')
            ->header('Cache-Control',' max-age=2592000, public')
            ->header("Expires", gmdate('D, d M Y H:i:s', time()+2592000) . ' GMT');*/
        return $response; 

        // $start_app = new Studio($url);
        // $start_app->start();
        //echo "<pre>";print_R($start_app->start());die('success');
    }
}
