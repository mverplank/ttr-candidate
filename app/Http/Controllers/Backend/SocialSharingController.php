<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Toolkito\Larasap\SendTo;
/*use App\Models\Backend\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Backend\MediaLinking;
use DB;
use Auth;
use Session;*/

class SocialSharingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Function For Twitter 
     */

    public function sendTextTweet()
    {
        SendTo::Twitter('Hello, Im testing Laravel social auto posting');
        dd('Youe message send successfully!!');
    }

    public function sendTweetWithMedia()
    {
        SendTo::Twitter(
            'Hello, Im testing Laravel social auto posting',
            [
                public_path('photo-1.jpg'),
                public_path('photo-2.jpg')
            ]
        );
        dd('Youe message send successfully!!');
    }


    /**
     * Function For Facebook
     */
    
    public function sendLinkToFacebook()
    {
        SendTo::Facebook(
            'link',
            [
                'link' => 'https://github.com/toolkito/laravel-social-auto-posting',
                'message' => 'Laravel social auto posting'
            ]
        );
        dd('Youe message send successfully!!');
    }

    public function sendPhotoToFacebook(Request $request)
    {
        SendTo::Facebook(
            'photo',
            [
                'photo' => public_path('img/1.jpg'),
                'message' => 'Laravel social auto posting'
            ]
        );
        dd('Youe message send successfully!!');
    }

    public function sendVideoToFacebook()
    {
        SendTo::Facebook(
            'video',
            [
                'video' => public_path('upload/1.mp4'),
                'title' => 'Let Me Be Your Lover',
                'description' => 'Let Me Be Your Lover - Enrique Iglesias'
            ]
        );
        dd('Youe message send successfully!!');
    }

}
