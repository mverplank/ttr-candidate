<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Password;
use App\Models\Backend\MediaLinking;
use App\Models\Backend\User;
use App\Models\Backend\Host;
use App\Models\Backend\Guest;
use App\Models\Backend\Profile;
use App\Models\Backend\HostItune;
use App\Models\Backend\PodcastStat;
use App\Models\Backend\Episode;
use App\Models\Backend\Show;
use App\Helpers\ControllerHelper; 
use Illuminate\Validation\Rule;

use Redirect;
use View;
use Session;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $time = date('H:i:s');
        $date = date('Y-m-d');
        $prevDay = $this->date2dow(date('Y-m-d', strtotime($date) - 24 * 60 * 60));
        $currDay = $this->date2dow($date);

        // Check code from the Feedbacks componenet in the Schedule plugin having function get()

        return view('backend/admin');
    }

    public function editprofileform(){
      
        return view('backend.admin_profile');
    }

    public function edit_profile(Request $request){

        if (Auth::check()) {
            $user = Auth::user();
            $id   = $user->id;       
            $updatedby ='';
            $social_media = array();
            $itunes_categories =array();
            $media_model =  new MediaLinking();
            $host_content_media ='';
            if(\Config::get('constants.HOST') == $user->groups_id){
                $media = $media_model->get_media($id, 'Host');
                $host_content_media =MediaLinking::where('main_module','=','HostContent')->get();
                //echo "<pre>";print_r($host_content_media);die;
                //Social media types
                $social_media = array(
                                    'website'   => 'website',
                                    'blog'      => 'blog',
                                    'rss'       => 'rss',
                                    'twitter'   => 'twitter',
                                    'facebook'  => 'facebook',
                                    'google'    => 'google',
                                    'linkedin'  => 'linkedin',
                                    'itunes'    => 'itunes',
                                    'youtube'   => 'youtube',
                                    'instagram' => 'instagram',
                                    'pinterest' => 'pinterest',
                                    'other'     => 'other'
                                );

                $itunes_categories = ControllerHelper::comboCategoriesSubcategories();
                if(!empty($itunes_categories)){
                    $itunes_categories = json_encode($itunes_categories);
                }
                // Get host and its relational data
                $user = User::with('hosts')->with('profiles')->with('user_settings')->with('hosts.hosts_has_host_types')->with('hosts.urls')->with('hosts.videos')->with('hosts.host_itunes')->where('id',$id)->first();
                //updateed by 
                if($user['hosts']['0']['updated_by']){
                    $updatedby =User::find($user['hosts']['0']['updated_by']);
                }
                
                //echo"<pre>";print_r($user['hosts']['0']['updated_by']);die();
            }else{
                $user = User::with('profiles')->with('user_settings')->where('id',$id)->first();

                $media = $media_model->get_media($id, 'User');
            }

            $data = $request->all();
            $no_check = 0;
           
            if ($request->isMethod('put') || $request->isMethod('post')) {
                
                //echo "<pre>";print_R($_POST);die('success');
                if($id == $data['data']['User']['id']){
                    $no_check = 1;
                }
               
                $validator = Validator::make($request->all(), [
                    'data.User.email'    => 'required|unique:users,email,'.$id,
                    'data.User.username' => 'required|unique:users,username,'.$id,
                    //'email' => ['required', Rule::unique('users', 'email')->ignore($id)],
                ]);
                  
                if ($validator->fails() && $no_check == 0) {
                    $errors = $validator->getMessageBag()->toArray();
                    $message = "The email or username has already been taken.";
                    $var     = "error";            
                    Session::flash($var , $message);
                    return redirect('admin/user/users/profile');

                } else{  
                                
                    $update_data = $request->data; 
                    if(\Config::get('constants.HOST') == $user->groups_id){
                        $host = new Host();
                        $update = $host->edit_profile($id, $update_data);
                        $redirect ='host/user/hosts/profile';
                    }else{
                        $user = new User();
                        $update = $user->edit($id, $update_data);
                        $redirect ='admin/user/users/profile';
                    }                       
                    
                    if($update){
                        $message = "The user details has been updated.";
                        $var     = "success";
                    }else{
                        $message = "There is some problem while updating the user. Please try again.";
                        $var     = "error";
                    }
                    Session::flash($var , $message);
                    return redirect($redirect);
                }
            }else{
                return view('backend.admin_profile', compact('user', 'social_media', 'itunes_categories','updatedby','media','id','host_content_media'));
            }
        }
        return redirect('login');
    }  

    // Call view of change password
    public function changepassform(){
        return view('auth.passwords.changepassword');
    }   

    public function updatePass(Request $request){
        
        $validator = Validator::make($request->all(), [
                    'current_password' => 'required',
                    'new_password' => 'required|string|min:6|',
                    'confirm_password' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->getMessageBag()->toArray();
            return response()->json(['validation_failed'=>true,'errors'=>$errors]);   
        } else{       
            $current_password = $request->current_password;            
            $get_new_password = $request->new_password;
            $confirm_password = $request->confirm_password;
            if($get_new_password == $confirm_password){
                $userpassword     = Auth::user()->password;
                if(Hash::check($current_password, $userpassword)){
                    $new_password     =  bcrypt($get_new_password);           
                    $user             = Auth::user();
                    $user->update(['password'=>$new_password]);
                   
                    return redirect('admin/user/users/password')->with('status', 'Password has been updated!');

                }else{
                    return redirect('admin/user/users/password')->with('error', 'Current Password is incorrect!');
                }            

            }else{
                return redirect('admin/user/users/password')->with('error', 'Confirm password does not match');
            }
        }
    }

    //this is commented because this function code merge to edit_profile function
  
    public function edit_profile_custom_content(Request $request){
		return view('backend.host_custom_profile');
	/*
        if (Auth::check()) {
            $user = Auth::user();
            $id   = $user->id;       
            if(\Config::get('constants.HOST') == $user->groups_id){
                // Get host and its relational data
                $user = User::with('hosts')->with('hosts.host_contents')->with('hosts.host_testimonials')->where('id',$id)->first();
            }

            $data = $request->all();
            $no_check = 0;
           
            if ($request->isMethod('put') || $request->isMethod('post')) { 
                $update_data = $request->data;
                $host = new Host();
                $host_id = $host->getHostId($data['edit_id']);
                $update = $host->edit_custom_content($host_id, $update_data); 
               // echo"<pre>";print_r($update);die();        
                if($update){
                    $message = "The details has been updated.";
                    $var     = "success";
                }else{
                    $message = "There is some problem while updating the user. Please try again.";
                    $var     = "error";
                }
                Session::flash($var , $message);
                return redirect('host/user/hosts/page');

            }else{
                return view('backend.host_custom_profile', compact('user'));
            }
        }
        return redirect('login');
	*/
    } 


    /**
     * Fetch the Podcast Stats for charts.
     *
     * @return podcast stat data
     */
    public function podcastStats(Request $request){
        $type = $request->type;
        $episode =array();
        $data_arr =array();
        if($type == 'last_week'){
            //$date = \Carbon\Carbon::today()->subDays(7);
            $date = new \Carbon\Carbon; 
            $date->subWeek();
           
            $lastweak = PodcastStat::select('last_week','episodes_id')->where('last_week', '>', 0)->orderBy('last_week', 'DESC')->limit(15)->get();
           
            //$lastweak = PodcastStat::select('last_week','episodes_id')->where('last_week', '>', 0)->where('created', '>', $date->toDateTimeString() )->orderBy('last_week', 'DESC')->limit(15)->get();

            foreach ($lastweak as $value) {
              $color ="#".substr(md5(rand()), 0, 6);
              $Episod =Episode::find( $value->episodes_id)->toArray();
              $show =Show::find($Episod['shows_id'])->toArray();
              $Episod['chart_series'] =$value->last_week;
              $Episod['bar_color'] =$color;
              $date_ =explode(" ", $Episod['date']);
              $date = date('D, M j Y H:i:s A', strtotime($date_[0].' '.$Episod['time']));
              $Episod['show_date'] = $date;
              $episode[]= $Episod;
              $label = $Episod['title'].' ('.$date.' ) ['.$show['name'].' ]';
              $data_arr []= array('meta' =>$label,'value' =>$value->last_week,'color'=>$color);
            }
        } 
        if($type == 'last_month'){

            $date = new \Carbon\Carbon; 
            $date->subDays(30);
            $lastmonth = PodcastStat::select('last_month','episodes_id')->where('last_month', '>', 0)->orderBy('last_month', 'DESC')->limit(15)->get();
            //$lastmonth = PodcastStat::select('last_month','episodes_id')->where('last_month', '>', 0)->where('created', '>=', $date->toDateTimeString())->orderBy('last_month', 'DESC')->limit(15)->get();
            foreach ($lastmonth as $value) {
              $color ="#".substr(md5(rand()), 0, 6);
              $Episod =Episode::find( $value->episodes_id)->toArray();
              $show =Show::find($Episod['shows_id'])->toArray();
              $Episod['chart_series'] =$value->last_month;
              $Episod['bar_color'] =$color;
              $date_ =explode(" ", $Episod['date']);
              $date = date('D, M j Y H:i:s A', strtotime($date_[0].' '.$Episod['time']));
              $Episod['show_date'] = $date;
              $episode[]= $Episod;
              $label = $Episod['title'].' ('.$date.' ) ['.$show['name'].' ]';
              $data_arr []= array('meta' =>$label,'value' =>$value->last_month,'color'=>$color);
            }
        }   
        if($type == 'total'){
            $total = PodcastStat::select('total','episodes_id')->where('total', '>', 0)->orderBy('total', 'DESC')->limit(30)->get();
            foreach ($total as $value) {
              $color ="#".substr(md5(rand()), 0, 6);
              $Episod =Episode::find( $value->episodes_id)->toArray();
              $show =Show::find($Episod['shows_id'])->toArray();
              $Episod['chart_series'] =$value->total;
              $Episod['bar_color'] =$color;
              $date_ =explode(" ", $Episod['date']);
              $date = date('D, M j Y H:i:s A', strtotime($date_[0].' '.$Episod['time']));
              $Episod['show_date'] = $date;
              $episode[]= $Episod;
              $label = $Episod['title'].' ('.$date.' ) ['.$show['name'].' ]';
              $data_arr []= array('meta' =>$label,'value' =>$value->total,'color'=>$color);
            }
        }  
      return $arrayName = array('chart_series' =>$data_arr,'episode'=>$episode );

    }   

    /**
     * Fetch thefavorites Stats for charts.
     *
     * @return favorites Stats data
     */
    public function favoritesStats(Request $request){
        
        $type = $request->type;
        $start_date = $end_date = '';
        if(isset($request->startdate)){
            $start_date = $request->startdate;
        }
        if(isset($request->enddate)){
            $end_date = $request->enddate;
        }
        $episode  = array();
        $data_arr = array();
        
        if($type == 'episode'){
            if(!empty($start_date)){
                $episodes = Episode::select('id','shows_id','date','time','favorites') 
                                ->where(DB::raw(' DATE(created_at)'), '>=', $start_date) 
                                ->where(DB::raw(' DATE(created_at) '), '<=', $end_date)
                                ->orderBy('favorites', 'DESC')
                                ->limit(15)
                                ->get();
            }else{
                $episodes = Episode::select('id','shows_id','date','time','favorites')
                                ->where('favorites', '>', 0)
                                ->orderBy('favorites', 'DESC')
                                ->limit(15)->get();
            }
            
            foreach ($episodes as $value) {
                $color  = "#".substr(md5(rand()), 0, 6);
                $date_  = explode(" ",$value->date);
                $date   = date('D, M j Y H:i:s A', strtotime($date_[0].' '.$value->time));
                $Episod = Show::find( $value->shows_id)->toArray();
                $Episod['episode_favorites'] =$value->favorites;
                $Episod['episode_id'] =$value->id;
                $Episod['episode_date'] =$date;
                $Episod['episode_time'] =$value->time;
                $Episod['bar_color'] =$color;
                $episode[]= $Episod;
                $label = $Episod['name'].' ('.$date.')';
                $data_arr [] = array('meta' =>$label,'value' =>$value->favorites,'color'=>$color);
            }
        } 
        if($type == 'show'){
            if(!empty($start_date)){
                $shows = Show::select('id','name','favorites')
                        ->where(DB::raw('DATE(created_at)'), '>=', $start_date) 
                        ->where(DB::raw(' DATE(created_at) '), '<=', $end_date)
                        ->orderBy('favorites', 'DESC')
                        ->limit(15)
                        ->get();
            }else{
                $shows = Show::select('id','name','favorites')
                        ->where('favorites', '>', 0)
                        ->orderBy('favorites', 'DESC')
                        ->limit(15)->get();
            }

            foreach ($shows as $value) {
                $color ="#".substr(md5(rand()), 0, 6);
                $label = $value->name.' favorited:';
                $data_arr [] =array('meta' =>$label,'value' =>$value->favorites,'color'=>$color);
                $Episod['id'] =$value->id;
                $Episod['name'] =$value->name;
                $Episod['favorites'] =$value->favorites;
                $Episod['bar_color'] =$color;
                $episode[]= $Episod;
            }
        }   
        if($type == 'host'){
            if(!empty($start_date)){
                $hosts = Host::select('id','users_id','favorites')
                            ->where(DB::raw('DATE(created_at)'), '>=', $start_date) 
                            ->where(DB::raw(' DATE(created_at) '), '<=', $end_date)
                            ->orderBy('favorites', 'DESC')
                            ->limit(15)
                            ->get();
            }else{
                $hosts = Host::select('id','users_id','favorites')
                            ->where('favorites', '>', 0)
                            ->orderBy('favorites', 'DESC')
                            ->limit(15)->get();
            }
            foreach ($hosts as $value) {
                $color ="#".substr(md5(rand()), 0, 6);
                $Episod =Profile::where('users_id',$value->users_id)->first()->toArray();
                $Episod['host_id'] =$value->id;
                $Episod['favorites'] =$value->favorites;
                $Episod['bar_color'] =$color;
                $episode[]= $Episod;
                $label = $Episod['name'].' favorited:';
                $data_arr [] =array('meta' =>$label,'value' =>$value->favorites,'color'=>$color);
            }
        } 
        if($type == 'guest'){
            if(!empty($start_date)){
                $guests = Guest::select('id','name','favorites')
                            ->where(DB::raw('DATE(created_at)'), '>=', $start_date) 
                            ->where(DB::raw(' DATE(created_at) '), '<=', $end_date)
                            ->orderBy('favorites', 'DESC')
                            ->limit(15)
                            ->get();
            }else{
                $guests = Guest::select('id','name','favorites')
                                ->where('favorites', '>', 0)
                                ->orderBy('favorites', 'DESC')
                                ->limit(15)->get();
            }
            foreach ($guests as $value) {
                $color ="#".substr(md5(rand()), 0, 6);
                $label = $value->name.' favorited:';
                $data_arr [] = array('meta' =>$label,'value' =>$value->favorites,'color'=>$color);
                $Episod['id']   = $value->id;
                $Episod['name'] = $value->name;
                $Episod['favorites'] = $value->favorites;
                $Episod['bar_color'] = $color;
                $episode[]= $Episod;
            }  
        }
        return $arrayName = array('chart_series' => $data_arr,'episode'=>$episode );
    }

    /**
     * Convert given date to day of week
     * 
     * @param String $date      date as any acceptable String date 
     * 
     * @return String           day of week name (ex. 'Sun')
     * 
     */
    private function date2dow($date) {

        $timestamp = strtotime($date);
        $dow = date("D", $timestamp); // day of week  ex. 'Mon' 
        return $dow;
    }

    public function HostToUser(Request $request){
        $guests = Guest::where('users_id', '=', 0)->get();
        //echo"<pre>";print_r($guests);
        foreach ($guests as $guest) {
           $user = new User();
           $user->groups_id ='5';
           $user->email = $guest->email;
          if($user->save()){
            $guest = Guest::where('id', '=',$guest->id)->first();
            $guest->update(['users_id'=>$user->id]);
            $profile =new Profile();
            $profile->users_id = $user->id;
            $profile->title =  $guest->title;
            $profile->bio = $guest->bio;
            $profile->sufix = $guest->sufix;
            $profile->firstname = $guest->firstname;
            $profile->lastname = $guest->lastname;
            $profile->name = $guest->name;
            $profile->phone = $guest->phone;
            $profile->cellphone = $guest->cellphone;
            $profile->skype = $guest->skype;
            $profile->skype_phone = $guest->skype_phone;
            $profile->notes = '';
            $profile->save();

          }
        }
    } 
}
