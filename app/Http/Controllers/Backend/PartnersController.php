<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Backend\MediaLinking;
use DB;
use Auth;
use Session;

class PartnersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('backend/user/partners');
    }

    public function ajax_index(Request $request)
    {
       
        $users  = new User();
        $draw   = 1;
        $type   = 4;
      
        $start  = $request->input('start');
        $length = $request->input('length');
        $draw   = $request->input('draw');
        $order  = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
         
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }    
      
        $all_partners   = $users->getAllUsers($type, $start, $length, $order, $dir, null, $search_value);
        $count_partners = $users->countUsers($type, $order, $dir, null, $search_value);
       
        $data = array();
        
        $i = 0;   
        $user_type = "'partner'";
        
        foreach($all_partners as $partner){
            $data[$i][]  =  html_entity_decode(showImage($partner->id, 'Partner', 'photo', '001.jpg'));
            $data[$i][]  = '<h2 class="partner_name">'.$partner->title.' '.$partner->firstname.' '.$partner->lastname.'</h2>'.strip_tags(str_limit($partner->bio, $limit = 280, $end = '...')).'';
            
            if($partner->status == '1'){
                $data[$i][] = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
            }else if($partner->status == '0'){
                $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Totally Inactive</button>';
            }else if($partner->status == '2'){
                $data[$i][] = '<button class="btn btn-warning disabled waves-effect waves-light btn-xs m-b-5">Login Inactive</button>';
            }

            $data[$i][]  = '<a title="Edit" href="'.url('admin/user/partners/edit/'.$partner->id ).'"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a><a href="javascript:void(0)" title="Delete" onclick="deleteUser('.$partner->id.', this, '.$user_type.')" ><i class="glyphicon glyphicon-trash"></i></a>';
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_partners,
                    'recordsFiltered' => $count_partners,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }


    public function hosts()
    {
        return view('backend/user/hosts');
    }

    public function partners()
    {
        return view('backend/user/partners');
    }

    public function members()
    {
        return view('backend/user/members');
    }

    public function activities()
    {
        return view('backend/user/activities');
    }

    public function add_partner(Request $request)
    {

        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Partner');

        if ($request->isMethod('post')) {
         
            if ($data = $request->input('files')) {
                $session_id = session()->getId();
            }
           
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $partner  = new User();
            $insert_id = $partner->add($data, 'Partner');
            if($insert_id){
                
                // Save profile image
                $message = "New Partner has been created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the partner. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/partners');
        }else{
            return view('backend/user/partner_add')->with(compact('media'));
        }
    }

    public function edit_partner($id, Request $request)
    {  
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Partner');

        $partner = User::with('profiles')->with('user_settings')->where('id',$id)->first();
        if ($request->isMethod('put') || $request->isMethod('post')) {
           // echo "<pre>";print_R($request->post());die('success');
            $update_data = $request->data;
            if (empty($update_data['Partner']['password'])) {
                unset($update_data['Partner']['password']);
            }
            $partner = new User();
            $update = $partner->edit($id, $update_data);
            if($update){
                $message = "The partner details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the partner. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/partners');
        }else{
            return view('backend/user/partner_edit', compact('partner','media'));
        }
    }

    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $partner_id = $post_data['id'];
                $partner = new User();
                $delete = $partner->deletePartner($partner_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
            //echo "Post data = <pre>";print_R($post_data);die('success');
        }
    }

    // Export Partners
    public function export_partners(){ 
        $result = array();
        $partners =DB::table('users')
                   ->where(function($q) {
                       $q->where('users.id', '!=', Auth::id())
                       ->where('users.deleted',0)
                       ->where('users.groups_id','=',4);
                   })
                   ->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
                   ->select('profiles.firstname', 'profiles.lastname','users.email', 'profiles.phone', 'profiles.cellphone', 'profiles.skype', 'profiles.skype_phone')
                   ->orderBy('profiles.firstname', 'asc')
                   ->get()->toArray(); 

            $field = array('firstname' =>'firstname','lastname' =>'lastname','email' =>'email','phone' =>'phone','cellphone' =>'cellphone','skype' =>'skype','skype_phone' =>'skype_phone' ); 
                              
                    foreach ($partners as $partner) {
                        //modification
                        $result[] = (array)$partner;  
                    } 
        if(!empty($result)){          
           return Excel::create('partners-', function($excel) use ($result) {
               $excel->sheet('partners-', function($sheet) use ($result)
               {
                  $sheet->fromArray($result);
               });
           })->download('csv');

       }else{
           return Excel::create('partners-', function($excel) use ($field) {
               $excel->sheet('partners-', function($sheet) use ($field)
               {
                  $sheet->fromArray($field);
               });
           })->download('csv');           

       }

    }
}
