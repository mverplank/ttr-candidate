<?php

/**
 * @author: Conrverz
 * @since : 21-01-2018 Monday
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Guest;
use App\Models\Backend\Setting;
use App\Models\Backend\Channel;
use App\Models\Backend\Category;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Backend\MediaLinking;
use App\Models\Backend\Host;
use App\Models\Backend\User;
use DB;
use Auth;
use Session;

class GuestsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the sponsors list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){

        //Fetch all Channels
        $channels = new Channel();
        $all_export_channels = $channels->getAllChannels()->toArray();
        return view('backend/show/guests')->with(compact('all_export_channels'));
    }

    public function ajax_index(Request $request)
    { 
        //echo "<pre>";print_R($request->all());die('success');
        $guests   = new Guest();
        $draw     = 1;
        $alphabet = '';
        $start    = $request->input('start');
        $length   = $request->input('length');
        $draw     = $request->input('draw');
        $order    = $request->post("order");
        $alphabet      = $request->post("alphabet");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $search_column = 'general';
        if($request->has('search_column')){
            $search_column_temp = $request->input('search_column');
            !empty(trim($search_column_temp))?($search_column_temp = $request->input('search_column')):'';
        }
        //echo $search_column;die;
        //$count_banners = 0;
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        if($request->exists('is_online')){
            $status  = $request->input('is_online');
        }
            
        $all_guests = $guests->getAllGuests( $alphabet, $search_column, $start, $length, $order, $dir, null, $search_value);
        $count_guests = $guests->countAllGuests( $alphabet, $search_column, $order, $dir, null, $search_value);

        $data = array();
        $i    = 0;   
      
        if(!empty($all_guests)){
           
            foreach($all_guests as $guest){  
                $guest_img = '<img src="'.url('/').'/public/assets/images/users/avatar.jpeg" width="120">';   
                if(!empty($guest->guest_id)){
                    $media_model =  new MediaLinking();
                    $media = $media_model->get_media($guest->guest_id, 'Guest');
                    if($media->count() > 0){
                        //echo "<pre>";print_R($media[0]);
                        $med = $media[0];
                        if($med->sub_module == 'photo'){
                            $guest_img = '<img src="'.url('/').'/storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename.'" width="120">';
                        }
                    }
                }else{
                    $media_model =  new MediaLinking();
                    $media = $media_model->get_media($guest->host_id, 'Guest');
                    if($media->count() > 0){
                        //echo "<pre>";print_R($media[0]);
                        $med = $media[0];
                        if($med->sub_module == 'photo'){
                            $guest_img = '<img src="'.url('/').'/storage/app/public/media/'.$med->media->id.'/'.$med->media->type.'/'.$med->media->filename.'" width="120">';
                        }
                    }
                }

                $data[$i][] = $guest_img; 
                if(!empty($guest->guest_id)){
                    $data[$i][] = "<h4>".$guest->title." ".$guest->firstname." ".$guest->lastname." ".$guest->sufix."</h4>".strip_tags(str_limit($guest->guest_bio, $limit = 280, $end = '...'));
                }else{
                      $data[$i][] = "<h4>".$guest->title." ".$guest->firstname." ".$guest->lastname." ".$guest->sufix."</h4>".strip_tags(str_limit($guest->host_bio, $limit = 280, $end = '...'));
                }
              
                /*if($sponsor->is_online == 1){
                    $data[$i][] = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
                }else{
                    $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Inactive</button> ';
                }*/                                            
                if(\Config::get('constants.HOST') == getCurrentUserType()){
                    $data[$i][]  = '<a href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a>';
                }else{
                  
                    if(empty($guest->guest_id)){
                        $data[$i][]  = '<a title="Edit" href="'.url('admin/user/hosts/edit/'.$guest->host_id ).'"><i class="glyphicon glyphicon-pencil" title="Edit"></i></a>
                        <a target="_blank" href="'.url('/guest/'.str_slug($guest->title." ".$guest->firstname." ".$guest->lastname." ".$guest->sufix, '-').','.$guest->guest_id.'.html').'"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a>
                        <a title="Delete" href="javascript:void(0)" onclick="deleteUser('.$guest->host_id.', this, &quot;host&quot;, &quot;guest&quot;)" ><i class="glyphicon glyphicon-trash" title="Delete"></i></a>';
                    }else{
                        $data[$i][]  = '<a title="Edit" href="'.url('admin/show/guests/edit/'.$guest->guest_id ).'"><i class="glyphicon glyphicon-pencil" title="Edit"></i></a>
                        <a target="_blank" href="'.url('/guest/'.str_slug($guest->title." ".$guest->firstname." ".$guest->lastname." ".$guest->sufix, '-').','.$guest->guest_id.'.html').'"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a>
                        <a title="Delete" href="javascript:void(0)" onclick="deleteGuest('.$guest->guest_id.', this)" ><i class="glyphicon glyphicon-trash" title="Delete"></i></a>';
                    }
                   
                }
                $i++;
            } 
           // die;
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_guests,
                    'recordsFiltered' => $count_guests,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    // Add Guests
    public function add_guest(Request $request)
    {
        
        $media_model =  new MediaLinking();
       $media = $media_model->get_media(0, 'Guest');

        // Fetch all Categories for filters
        $categories = new Category();
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false); 

        // Fetch all prefix and suffix
        $setting = new Setting();
        $prefix_suffix = $setting->allPrefixSuffix();

        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'linkedin'  => 'linkedin',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );

        //Fetch all Host for importing the host
        $hosts = new Host();
        $all_hosts = $hosts->getHostsForFilter('','import');

        $response = 0;
        if ($request->isMethod('post')) {           
            $data = $request->input('data'); 
            //echo "<pre>";print_R($data);die;    
           
            if($data['Host']['id'] == 0){
                $var         = "";
                $message     = "";
                $guest       = new Guest();
                $response    = $guest->add($data);
               
            }else{
               
                //Add the host as a guest in the host table
                $host = Host::find($data['Host']['id']);
                $user     = User::find($host->users_id);
                $user->is_guest = 1;
                $response = $user->update();
            }    

            if($response){
                $message = "New guest successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the guest. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/show/guests');        

        }else{
            return view('backend/show/guest_add')->with(compact('prefix_suffix','social_media','all_categories','media', 'all_hosts'));
        }
    }
       
    // Edit guest
    public function edit_guest($id, Request $request)
    {                           
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Guest');

        // Fetch all Categories for filters
        $categories = new Category();        
        
        //All categories of type show
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false); 
       
        // Fetch all prefix and suffix
        $setting = new Setting();
        $prefix_suffix = $setting->allPrefixSuffix();       
        
        //Social media types
        $social_media = array(
                            'website'   => 'website',
                            'blog'      => 'blog',
                            'rss'       => 'rss',
                            'twitter'   => 'twitter',
                            'facebook'  => 'facebook',
                            'google'    => 'google',
                            'linkedin'  => 'linkedin',
                            'itunes'    => 'itunes',
                            'youtube'   => 'youtube',
                            'instagram' => 'instagram',
                            'pinterest' => 'pinterest',
                            'other'     => 'other'
                        );
        $guest = '';
        $assigned_categories = array();

        $guest = Guest::with('user')->with('user.profiles')->with('urls')->with('videos')->where('id',$id)->first();

        if($guest->guests_has_categories->count() > 0){
            foreach($guest->guests_has_categories as $cat){
                $assigned_categories[] = $cat->categories_id;
            }
        }
        
        if ($request->isMethod('post')) {
            $update_data = $request->data;
            $guest  = new Guest();            
            $update = $guest->edit($id, $update_data);
            if($update){
                $message = "The guest details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the guest. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                return redirect('/host/show/guests');
            }else{
                return redirect('/admin/show/guests');
            }            
        }else{
            return view('backend/show/guest_edit', compact('guest','all_categories', 'social_media', 'assigned_categories','prefix_suffix','media'));
        }
    }

    // Delete guest
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $guest_id = $post_data['id'];
                $guest = new Guest();
                $delete = $guest->deleteGuest($guest_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
            //echo "Post data = <pre>";print_R($post_data);die('success');
        }
    }

    // Unlink the host from the guest when it's importing from the host.
    public function guest_host_unlink(Request $request){
        
        $host_id = $request->id;
        $host    = Host::find($host_id);
        if($host){
            
            //Set the is_guest to 0 so that it will configure as a guest.
            $update = User::where('id', $host->users_id)->update(['is_guest'=>0]);
            if($update){
                return array('status' => 'success');
            }else{
                return array('status' => 'error');
            }
        }
    }

    public function export_guests(Request $request){

        $data     = $request->data;
        $guests   = new Guest();
        $users    = '';
        $result   = array();
        $filename = '';
        if($request->lebel =='undefined'){

            if($data['Guest']['export_filter'] =='All'){

                $users = $guests->eportGuests($channel_id='',$all=$data['Guest']['export_filter'], $online='', $offline='', $featured='')->toArray();
                $filename =$data['Guest']['export_filter'];

            }else if($data['Guest']['export_filter'] =='Offline'){

                $users = $guests->eportGuests($channel_id='',$all='', $online='', $offline=$data['Guest']['export_filter'], $featured='')->toArray();
                $filename =$data['Guest']['export_filter'];    

            }else if($data['Guest']['export_filter'] =='Online'){

                $users = $guests->eportGuests($channel_id='',$all='', $online=$data['Guest']['export_filter'], $offline='', $featured='')->toArray();
                $filename =$data['Guest']['export_filter'];

            }else{

                $users = $guests->eportGuests($channel_id='',$all='', $online='', $offline='', $featured=$data['Guest']['export_filter'])->toArray();
                $filename =$data['Guest']['export_filter'];
            }      

        }else {

            if($request->lebel =='Channels'){

                $channel_id = $data['Guest']['export_filter'];
                    $users = $guests->eportGuests($channel_id,$all='', $online='', $offline='', $featured='')->toArray();
                    $filename ='channels-'.$data['Guest']['export_filter'];

            }
        }

        $field = array('Lastname' =>'Lastname','Firstname' =>'Firstname','Email' =>'Email','Phone' =>'Phone','Cellphone' =>'Cellphone','Skype' =>'Skype','Skype Phone' =>'Skype Phone','PR name'=>'PR name','PR company'=>'PR company','PR phone'=>'PR phone','PR cellphone'=>'PR cellphone','PR Skype'=>'PR Skype','PR Skype phone'=>'PR Skype phone','PR email'=>'PR email' );

        foreach ($users as $user) {
            //modification
            $result[] = (array)$user;  
        } 

        if(!empty($result)){          
            return Excel::create('guests-'.$filename, function($excel) use ($result,$filename) {
                $excel->sheet('guests-'.$filename, function($sheet) use ($result)
                {
                   $sheet->fromArray($result);
                });
            })->download('csv');

        }else{
            return Excel::create('guests-'.$filename, function($excel) use ($field,$filename) {
                $excel->sheet('guests-'.$filename, function($sheet) use ($field)
                {
                   $sheet->fromArray($field);
                });
            })->download('csv');           

        }
    }

    /** 
     * Fetch the host detail using host id to import the data into the guest 
     * @param $id is id of the host table
     */
    public function getHostDetailToImport(Request $request){
        
        $host_id = $request->id;
        $host = new Host();
        $return_info = $host->getHostDetailById($host_id);
        return $return_info;
    }
}
