<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\InventoryCategory;
use App\Models\Backend\Category;
use Illuminate\Support\Facades\Validator;

use Session;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $type=""){
       
        $all_categories = '';
        $generate_tree  = '';
        $compact        = '';
        $view_url       = '';
        if($type =='banner'){
           $view_url = "backend/advertising/banners_categories";
        }else if($type == 'show'){
            $catObj = new Category();
            $get_all_categories = $catObj->getFlatArrayOfCategoriesByType('show');
            //echo "<pre>";print_R($get_all_categories);die;
            
            $get_tree = array();
            if(!empty($get_all_categories)){
                $get_tree = $this->convertFlatArrayToTreeArray($get_all_categories);   
            }
            if(!empty($get_tree)){
                $generate_tree = $this->generateTree($get_tree);  
                $compact = 'generate_tree'; 
            }
           
            $view_url ="backend/configuration/categories";            
        }
       
        if(!empty($compact)){
            return view($view_url)->with(compact($compact));
        }else{
            return view($view_url);
        }
    }

    public function ajax_index(Request $request)
    {
        $categories = new Category();
        $type       = $request->input('type');
        $operation  = $request->input('operation');
        $output     = array();
      
        $draw    = 1;
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 

        if(!empty($type)){
           $all_categories   = $categories->getCategories($type, $start, $length, $order, $dir, null, $search_value);
           $count_categories = $categories->countCategories($type, $order, $dir, null, $search_value);
        }else{
           $all_categories   = $categories->getCategories('', $order, $dir, null, $search_value);
           $count_categories = $categories->countCategories('',$order, $dir, null, $search_value);
        }

        $data = array();
        $i    = 0;   
        if(!empty($all_categories)){
            foreach($all_categories as $category){
               
                $data[$i][]  = $category->name;
                $data[$i][]  = '<a href="javascript:void(0)" onclick="deleteBannerCate('.$category->id.', this)" ><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_categories,
                    'recordsFiltered' => $count_categories,
                    'data' => $data
                );
        
        echo json_encode($output);
        exit();
    }

    public function get_categories(Request $request)
    {
        $result    = null;
        $category  = new Category();
        $operation = $request->input('operation');
        if($operation == "rename_node"){
            
            $update = $category->where('id', $request->input('id'))->update(['name'=>$request->input('text')]);
            if($update){
                $result  = array('status' => 'Successfully renamed.');
            }else{
                $result  = array('status' => 'Error');
            }
        }elseif($operation == "create_node"){
            $data    = $request->all();
            $catObj  = new Category($data);
            $catObj->save();
            $result  = array('id' => $catObj->id);

        }elseif($operation == "move_node"){
            $data    = $request->all();
            $catObj  = new Category($data);
            $update  = $category->where('id', $request->input('id'))->update(['parent_id'=>$request->input('parent_id')]);
            if($update){
                $result  = array('status' => 'Successfully moved.');
            }else{
                $result  = array('status' => 'Error');
            }

        }elseif($operation == "delete_node"){

            if($request->input('id')){
                $catObj  = new Category(); 

                //Delete Child categories
                try{
                    $this->deleteSubCategories($request->input('id'));
                    $result  = array('status' => 'Successfully deleted.');
                }catch(\Exception $e){
                    $result  = array('status' => 'Error', 'msg'=>$e->getmessage()); 
                }
            }           
        }
        echo json_encode($result);
        exit();
    }

    /**  
     * Delete all subcategories into the category
     * @param $id is the main category id
     */
    public function deleteSubCategories($id){
        if($id != 0){
            $catObj  = new Category(); 
            $get_categories = $catObj->select('id')->where( 'parent_id', $id)->get();
            if($get_categories->count() > 0){
                foreach($get_categories as $category){
                    $this->deleteSubCategories($category->id);
                }
            }
           /* try{
                die;*/
                $delete = $catObj->where( 'id', $id)->delete();
              /*  if($delete){
                    die('yes');
                }else{
                      die('no');
                }
                return true;
            }catch(\Exception $e){
                echo $e->getMessage(); 
                die;
                return $e->getMessage(); 
            }*/
        }
    }

    /**  
     * Convert Flat category array into the Tree array
     */
    public function convertFlatArrayToTreeArray($data=array(), $parentId=null) {
       
        $this->treeChildren = array();

        // convert flat array to tree array (children nodes stays as root nodes) 
        $treeArr = $this->convFlat2Tree($data, $parentId);

        // remove from root level nodes nodes which were assigned as child nodes 
        foreach ($treeArr as $key => $n) { // loop throu root level 
            
            if (in_array($n['id'], $this->treeChildren)) {
                            
                unset($treeArr[$key]);
            }        
        }

        return array_values($treeArr);  // normalize array keys 
    }
    
    public function convFlat2Tree($data=array(), $parentId=null) {
        
        $branch = array();        
        foreach ($data as $d) {           
            
            if ($d['parent_id'] === $parentId) {
                $children = $this->convFlat2Tree($data, $d['id']);
                if ($children) {
                    $d['children'] = $children;
                }
                $branch[] = $d;
            }
        }       
        return $branch;
    }

    private function generateTree($data=array(), $opened=false) {

        $c = '<ul>';
        foreach ($data as $d) {

            $c .= '<li data-id="'.$d['id'].'"'.($opened ? ' data-jstree=\'{"opened":true}\'': '').'>' . $d['name'];
            if (isset($d['children'])) {

                $c .= $this->generateTree($d['children'], $opened=false);
            }
            $c .= '</li>';
        }

        return $c . '</ul>';
    }
        

    // Add Item Category
    public function AddItemCategories(Request $request){
       /* Validator is used to validate all the details which are recived in the $request */
       $validator = Validator::make($request->all(), [
                   'name' => 'required|unique:inventory_categories',
       ]);

       /* fails() will return tru only if any of details which validator checks is no valid */
       if ($validator->fails()) {

           $errors = $validator->getMessageBag()->toArray();

           return response()->json(['validation_error'=>true,'message'=>"The name field is unique and required"]);

       } else {

            $ItemName = $request->name;
            $inventory_categories = new InventoryCategory();
            $inventory_categories->name = $ItemName; 
            $addCate = $inventory_categories->save();       
            if($addCate){
               return response()->json(['seccess'=>true,'message'=>"Add successfully"]);
            }  
        }
    }

    public function searchCat(Request $request){
        $parent_id = $request->parent_id;
        $type = $request->type;
        $categories = new Category();
        $result = $categories->searchCat($parent_id, $type)->toArray();
        return $result; 
    }

    /** Fetch all the categories in the system
     */
    public function categories(){
        return view("backend/configuration/categories");
    }
}
