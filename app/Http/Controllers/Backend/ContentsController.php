<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\User;
use App\Models\Backend\Category;
use App\Models\Backend\ContentData;
use App\Models\Backend\ContentCategory;
use App\Models\Backend\Page;
use App\Models\Backend\MediaLinking;
use App\Helpers\ControllerHelper; 
use Session;
use Validator;
use Auth;
use Mail;
use Illuminate\Support\Facades\Storage;

class ContentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**  
     * check already exist  category
     * @param $name is category name
     */
    public function content_validation(Request $request){
        
        // Get the value from the form
        $input['name'] = $request->input('name');

        // Must not already exist in the `email` column of `users` table
        $rules = array('name' => 'unique:content_categories,name');

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
           return ['status'=>'error','message'=>'this is already exist'];
        }else{
            return ['status'=>'success', 'message'=>' '];
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $type=""){
       
        $all_categories = '';
        $generate_tree  = '';
        $compact        = '';
        $view_url       = '';

        $catObj = new ContentCategory();
        $get_all_categories = $catObj->getFlatArrayOfCategoriesByType();
        /*echo '<pre>';
        print_r($get_all_categories);
        die;*/
        $get_tree = array();
        if(!empty($get_all_categories)){
            $get_tree = $this->convertFlatArrayToTreeArray($get_all_categories);   
        }
        if(!empty($get_tree)){
            $generate_tree = $this->generateTree($get_tree);  
            $compact = 'generate_tree'; 
        }

        if(!empty($compact)){
            return view("backend/cms/categories_tree")->with(compact($compact));
        }else{
            return view("backend/cms/categories_tree")->with(compact('generate_tree'));
        }
    }

    public function ajax_index(Request $request)
    {
        /*$categories   = new Category();
        $type         = $request->input('type');
        $operation    = $request->input('operation');
        $output       = array();
        $draw         = 1;
        $start        = $request->input('start');
        $length       = $request->input('length');
        $draw         = $request->input('draw');
        $order        = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 

        if(!empty($type)){
           $all_categories   = $categories->getCategories($type, $start, $length, $order, $dir, null, $search_value);
           $count_categories = $categories->countCategories($type, $order, $dir, null, $search_value);
        }else{
           $all_categories   = $categories->getCategories('', $order, $dir, null, $search_value);
           $count_categories = $categories->countCategories('',$order, $dir, null, $search_value);
        }

        $data = array();
        $i    = 0;   
        if(!empty($all_categories)){
            foreach($all_categories as $category){
               
                $data[$i][]  = $category->name;
                $data[$i][]  = '<a href="javascript:void(0)" onclick="deleteBannerCate('.$category->id.', this)" ><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_categories,
                    'recordsFiltered' => $count_categories,
                    'data' => $data
                );

        echo json_encode($output);
        exit();*/
    }

    public function getcategories(Request $request){

        $result = null;
        
        $operation = $request->input('operation');
        if($operation == "rename_node"){
            $category = new ContentCategory();
            $update = $category->where('id', $request->input('id'))->update(['name'=>$request->input('text')]);
            if($update){
                $result  = array('status' => 'Successfully renamed.');
            }else{
                $result  = array('status' => 'Error');
            }
        }elseif($operation == "create_node"){
            $data    = $request->all();
            $catObj  = new ContentCategory($data);
            $catObj->save();
            $result  = array('id' => $catObj->id);

        }elseif($operation == "move_node"){
            $data    = $request->all();
            $catObj  = new ContentCategory($data);
            $update  = $category->where('id', $request->input('id'))->update(['parent_id'=>$request->input('parent_id')]);
            if($update){
                $result  = array('status' => 'Successfully moved.');
            }else{
                $result  = array('status' => 'Error');
            }

        }elseif($operation == "delete_node"){
            if($request->input('id')){

                //Delete Child categories
                $delete = $this->deleteSubCategories($request->input('id'));
                if($delete['status'] == 'success'){
                    $result  = array('status' => 'Successfully deleted.');
                }else{
                    $result  = array('status' => 'Error', 'msg'=>$delete['msg']); 
                }
            }           
        }
        echo json_encode($result);
        exit();
    }
   
    /**  
     * Delete all subcategories into the category
     * @param $id is the main category id
     */
    public function deleteSubCategories($id){
        $result = array('status' => 'error');
        if($id != 0){
            $catObj  = new ContentCategory(); 
            $get_categories = $catObj->select('id')->where( 'parent_id', $id)->get();
            if($get_categories->count() > 0){
                foreach($get_categories as $category){
                    $this->deleteSubCategories($category->id);
                }
            }
            try{
                $delete = $catObj->where( 'id', $id)->delete();
                $result['status'] = 'success';
            }catch(\Exception $e){
                $result['msg']=$e->getMessage();
            }
        }
        return $result;
    }

    /**
     * Update the content email.
     *
     * @return Response
     */
    public function content_email(Request $request){
        $file_arr = array(
                          ''=>'[ Please select ... ]',
                          'host_feedback_notification' =>'host_feedback_notification',
                          'host_no_episodes_details'=>'host_no_episodes_details',
                          'member_new_account'=>'member_new_account',
                          'member_password_changed'=>'member_password_changed',
                          'membership_podcast-host_welcome_letter'=>'membership_podcast-host_welcome_letter',
                          'membership_website-user_welcome_letter'=>'membership_website-user_welcome_letter',
                          'new_podcasts_notification'=>'new_podcasts_notification'
                    );
        if ($request->isMethod('post')) {
            $data = $request->input('data');
            if($data['Content']['action'] == 'edit'){
                $templete_name = $data['Content']['email_template'];
                $content = $data['Content']['email_body'];
                $path =  base_path()."/resources/views/emailtemplete/$templete_name.blade.php";
                $response = file_put_contents($path,$content);
                 if($response){
                    $message = "successfully! Saved";
                    $var     = "success";
                }else{
                    $message = "Please try again.";
                    $var     = "error";
                }
                Session::flash($var , $message);
                return redirect('admin/cms/contents/emails');

            }
            if($data['Content']['action'] == 'send'){
                $templete_name = $data['Content']['email_template'];
                $user =array(
                              'name' => "Parmod",
                              'subject'=>$data['Content']['email_subject']
                        );
                $response = Mail::send(['html'=>"emailtemplete.$templete_name"],$user, function($message) use ($user) {
                  $swiftMessage = $message->getSwiftMessage();
                  $headers = $swiftMessage->getHeaders();
                  $headers->addTextHeader('MIME-Version: 1.0" . "\n Content-type:text/html;charset=UTF-8" . "\n');
                    $message->from('hello@example.com','TTR');
                    $message->to('pramod.kumar@contriverz.com');
                    $message->subject($user['subject']);
                });
                Session::flash('success' , 'Mail Send Successfully');
                return redirect('admin/cms/contents/emails');
            }
            if($data['Content']['action'] == 'preview'){
               $templete_name = $data['Content']['email_template'];
              return view('emailtemplete/'.$templete_name);

            }

        }else{
          return view('backend/configuration/content_email')->with(compact('file_arr'));
        }
    }

    public function getfile(Request $request){
        $path = $request->path;
        $path =  url('/').'/resources/views/emailtemplete/'.$path.'.blade.php';
        $contents = file_get_contents($path);
        return $contents;
    }
  
    public function content_data_index(Request $request, $content_type_id=''){
      
        $content_cate  = array(''=>'Select Category');
        $subcate  = array();
       
        // Fetch all Categories for filters
        $content_categories = new ContentCategory();
        //Here 53 is the content category id
        $content_cate = $content_categories->getCategoriesForFilter(null, true, 0, true);
        
        //Get content type related data
        /* if (is_null($categoryId)) {

            //
            // All Content Categories
            //            
            $categoryId = '';
            $constants  = array();
        } else {            
            $constants  = array('content_categories_id' => $categoryId);
        }

        $contentCategoryData = $this->_setContentCategory($categoryId);            

        // allow sort if content cateogry is sortable 
        $contentCategoryData['ContentCategory']['is_sortable'] = $contentCategoryData['ContentCategory']['order_field'] === 'off' ? true : false;

        $this->set('ContentCategoryData', $contentCategoryData);*/

        //Apply ordering/sorting for these category types only
        $drag = 0;
        $categories_for_ordering = ['1', '12', '14', '15', '38', '42', '45', '51', '61', '63', '70' ];
        if(!empty($content_type_id)){
            if(in_array($content_type_id, $categories_for_ordering)){
                $drag = 1;
            }
        }
        // echo $drag;die;
        /** End content data fetching **/
        return view('backend.cms.content_data',compact('content_cate', 'content_type_id', 'drag'));
    }

    public function ajax_content_data_index(Request $request){

        $categoryId = $request->content_cat_id;
        if (is_null($categoryId)) {

            //
            // All Content Categories
            //            
            $categoryId = '';
            $constants  = array();
        } else {
            $constants  = array('content_categories_id' => $categoryId);
        }
     
        $draw          = 1;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 

        $contentCategoryData = $this->setContentCategory($categoryId, $start, $length, $order, $dir, null, $search_value); 
        $all_content_data   = $contentCategoryData['data'];
        $count_content_data = $contentCategoryData['cdata'];
       
        $data = array();
        $i    = 0;   

        if(!empty($all_content_data)){  
            foreach($all_content_data as $content_data){

                $online = '<h4>'.$content_data->title.'<h4>';
                if($content_data->is_online == 1){
                    $online .= '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Online</button>';
                }else{
                    $online .= '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Offline</button>';
                }

                if($content_data->is_featured == 1){
                    $online .= '&nbsp;&nbsp;<button class="btn btn-brown disabled waves-effect waves-light btn-xs m-b-5">Featured</button>';
                }
                $data[$i][] = $online;
                /*$data[$i][]  = strlen($content_data->content) > 400 ? substr($content_data->content,0,400).'...' : $content_data->content;*/
                $data[$i][] = date("m/d/Y", strtotime($content_data->created_at));
                $data[$i][] = date("m/d/Y", strtotime($content_data->updated_at));     
                $data[$i][] = '<a href="'.url('admin/cms/content_data/edit/'.$content_data->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteContentData('.$content_data->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $data[$i]['DT_RowId']    = $content_data->id;
                $data[$i]['DT_RowClass'] = 'off_'.$content_data->off;
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_content_data,
                    'recordsFiltered' => $count_content_data,
                    'data' => $data,

                );

        echo json_encode($output);
        exit();
    }

    
    /**
     * Reorder the content data acc. the category
     */
    public function reorder_category(Request $request){

        if($request->has('ajax')){
            $category_id            = $request->categoryId;
            $movedElementId         = $request->movedElementId;
            $movedElementNextId     = $request->movedElementNextId;

            $content_data_obj = new ContentData();
            //Fetch offsets of the moved element
            $movedElementOff     = $content_data_obj->fetchOffsetById($movedElementId);
            $movedElementNextOff = $content_data_obj->fetchOffsetById($movedElementNextId);

            $isSorted = $content_data_obj->checkDataExistOfCategory($category_id);
            
            //If there is not any offset set then re-print their row id values to off values
            if (!$isSorted) {
                $update_offsets = $content_data_obj->updateOffsetsIfNotSet($category_id);
            }

            // Are we moving element UP or DOWN ?
            $r1 = false; 
            $r2 = false;
            if ($movedElementOff < $movedElementNextOff) { // moved UP 
                //die('1');
                // change offset of all elements between old and new offset 
                $r1 = $content_data_obj->sort('up', $movedElementNextOff, $movedElementOff, $category_id);
                // set moved element new offset
                $r2 = $content_data_obj->setOffsetOfMovedElement('up', $movedElementId, $movedElementNextOff);
                         
            } else { // moved DOWN
                //die('2');
                // change offset of all elements between old and new offset 
                $r1 = $content_data_obj->sort('down', $movedElementNextOff, $movedElementOff, $category_id);
                $r2 = $content_data_obj->setOffsetOfMovedElement('down', $movedElementId, $movedElementNextOff);         
            }

            return array('status' => ($r1 && $r2));
        }
    }

    /**
     * @decription Add content Category
     * @param $request data object
     */
    public function add_content_categories(Request $request){
        
        $drderbyfield  = array('created_at' =>'Created','updated_at'=>'Modified','title'=>'Title','date'=>'Date','time'=>'Time','off'=>'Custom');
        $drderby = array('DESC' =>'descending','ASC'=>'ascending');
        $content_cate  = array(''=>'Select Category');
        
        // Fetch all Categories for filters
        $content_categories = new ContentCategory();
        //Here 53 is the content category id
        $content_cate = $content_categories->getCategoriesForFilter(null, true, 0, true);

      
       
        if($request->isMethod('post')){

            $data = $request->input('data');
            $categories  = new ContentCategory;
            $response    = $categories->add_content_categories($data);
            if($response){
                $message = "New content type has been added successfully!";
                $var     = "success";
            }else{
                $message = "There is some problem while adding content type. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/content_data/index');
        }else{
           return view('backend.cms.add_content_categories',compact('content_cate','drderbyfield','drderby'));
        }
    }

    /**
    * @decription Edit content Category
    * @param $request data object
    */
    public function edit_content_categories($id,Request $request){
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'ContentCategory');
        $drderbyfield = array('created_at' =>'Created','updated_at'=>'Modified','title'=>'Title','date'=>'Date','time'=>'Time','off'=>'Custom');
        $imageorientation  = array('h' =>'horizontal','v'=>'vertical','s'=>'square');
        $mediumsizecolumns  = array('6' =>'2','4'=>'3','3'=>'2',''=>'6');
        $smallsizecolumns  = array('6' =>'2','4'=>'3','3'=>'2',''=>'6');
        $showtitle  = array('0' =>'off','1'=>'on');
        $drderby = array('DESC' =>'descending','ASC'=>'ascending');
        $content_cate = array(''=>'Select Category');
        $thiscate = ContentCategory::where('id', '=', $id)->first();
        // Fetch all Categories for filters
        $content_categories = new ContentCategory();
        //Here 53 is the content category id
        $content_cate = $content_categories->getCategoriesForFilter(53, true, $thiscate->parent_id, true);
        /*echo '<pre>';
        print_r($thiscate);
        die;*/
        if($request->isMethod('post')){
            $data = $request->input('data');  
            //echo "<pre>";print_r($data);die; 
            if(!isset($data['ContentCategory']['parent_id']))
                $data['ContentCategory']['parent_id'] = 53;
            $categories  = new ContentCategory;
            $response    = $categories->edit_content_categories($id,$data);
            if($response){
                $message = "Content type has been update successfully!";
                $var     = "success";
            }else{
                $message = "There is some problem while adding content type. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/cms/content_categories/edit/'.$id);

        }else{
           return view('backend.cms.edit_content_categories',compact('content_cate','drderbyfield','drderby','thiscate','media','imageorientation','mediumsizecolumns','smallsizecolumns','showtitle'));
        }   
    }


    /**
    * @decription Delete content Category
    * @param $request data object
    * @param $id content Category id
    */
    public function delete_content_categories($id, Request $request){
     
        if($id != '' || $id !=0){
             $category = ContentCategory::where('id', '=' ,$id)->first();
              $delete = $category->update(['deleted'=>1]);
              if($delete){
                $message = "content type has been delete successfully!";
                $var     = "success";
              }else{
                $message = "There is some problem while delete content type. Please try again.";
                $var     = "error";
              }
        }else{
             $message = "There is some problem while delete content type. Please try again.";
                $var     = "error";
        }
         
        Session::flash($var , $message);  
        return redirect('admin/cms/content_data/index');
    }

    private function setContentCategory($categoryId, $start=0, $length=10,$order=null, $dir=null,  $column_search=null, $search_value=null, $search_regex=null) {
        
        /* $order_field     = 'created';
        $order_direction = 'DESC'; */

        $order_field     = 'off';
        $order_direction = 'ASC';
        if ($categoryId > 0) {
            
            // Get Content category details
            $cc = ContentCategory::find($categoryId);
            if($cc){
                if(!empty($cc->order_field)){
                    $order_field     = $cc->order_field;
                }if(!empty($cc->order_direction)){
                    $order_direction = $cc->order_direction;
                }
            }
        } 
       
        $content_data_obj = new ContentData();
        $data = $content_data_obj->fetchDataOfCategory($categoryId, $order_field, $order_direction, $start, $length, $order, $dir, $column_search, $search_value, $search_regex);
        $cdata = $content_data_obj->countDataOfCategory($categoryId, $order_field, $order_direction, $order, $dir, $column_search, $search_value, $search_regex);
      
        // build order and direction human readable description 
        return ['data'=>$data, 'cdata'=>$cdata];
    }

    /**
    * @decription Add content Category
    * @param $request data object
    */  
    public function add_content_data($id, Request $request){
        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'ContentData');

        // Fetch all Categories for filters
        $categories = new Category();
        $all_categories = $categories->getContentCategories('show', true, $id, true);

        // $itunes_categories = ControllerHelper::comboCategoriesSubcategories();

        // if(!empty($itunes_categories)){
        //     $itunes_categories = json_encode($itunes_categories);
        // }

        $contentdata  =ContentCategory:: where('id', '=', $id)->first();
        if($request->isMethod('post')){

            $data = $request->input('data');
          
            $contentdata  = new ContentData();
            $response    = $contentdata->add_content_data($data);
            if($response){
                $message = "New content type has been added successfully!";
                $var     = "success";
            }else{
                $message = "There is some problem while adding content type. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/content_data/index');
        }else{
           return view('backend.cms.add_content_data',compact('contentdata','media', 'all_categories'));
        }       
        
    }


    public function delete_content_data(Request $request){
       
        $id = $request->id;
        if($id != '' || $id !=0){

            $content_data = new ContentData();
            deleteMediaLink('ContentData', $id);
            $delete = $content_data->delete($id);
            if($delete){
                return array('status' => 'success');
            }else{
                return array('status' => 'error');
            } 
            
        }else{
           return array('status' => 'error');
        }
    }


    /**
    * @decription edit content Category
    * @param $request data object
    * @param $id ontent Category id
    */  
    public function edit_content_data($id, Request $request){
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'ContentData');
        $content  =ContentData:: where('id', '=', $id)->first();
        $contentdata  =ContentCategory:: where('id', '=',$content->content_categories_id)->first();
        $categories = new Category();
        $all_categories = $categories->getContentCategories('show', true,$content->has_category, true);
        //Fetch tags
        $original_tags = $tag_ids = $tags_arr = array();
        $tags = '';
        if($content->tags->count() > 0){
            foreach($content->tags as $tag){
                $tags_arr[] = $tag->name;
                $original_tags[] = $tag_ids[$tag->name] = $tag->id;
            }
            if(!empty($tags_arr)){
                $tags = implode(',',$tags_arr);
            }
        }
        $tag_ids = json_encode($tag_ids);

        if($request->isMethod('post')){

            $data = $request->input('data');
            $contentdata  = new ContentData();
            $response    = $contentdata->edit_content_data($id,$data);
            if($response){
                $message = "content data has been update successfully!";
                $var     = "success";
            }else{
                $message = "There is some problem while adding content type. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('/admin/cms/content_data/index');
        }else{
           return view('backend.cms.edit_content_data',compact('contentdata','content','tags', 'tag_ids', 'original_tags','media','all_categories'));
        }                    
    }

    /**  
     * Convert Flat category array into the Tree array
     */
    public function convertFlatArrayToTreeArray($data=array(), $parentId=null) {
       
        $this->treeChildren = array();
       
        // convert flat array to tree array (children nodes stays as root nodes) 
        $treeArr = $this->convFlat2Tree($data, $parentId);
       
        // remove from root level nodes nodes which were assigned as child nodes 
        foreach ($treeArr as $key => $n) { // loop throu root level 
            
            if (in_array($n['id'], $this->treeChildren)) {
                            
                unset($treeArr[$key]);
            }        
        }
        
        return array_values($treeArr);  // normalize array keys
    }

    public function convFlat2Tree($data=array(), $parentId=null) {
       
        $branch = array();        
        foreach ($data as $d) {           
            
            if ($d['parent_id'] === $parentId) {
               
                $children = $this->convFlat2Tree($data, $d['id']);
                if ($children) {
                    $d['children'] = $children;
                }
                $branch[] = $d;
            }
        }       
       
        return $branch;
    }

    private function generateTree($data=array(), $opened=false) {

        $c = '<ul>';
        foreach ($data as $d) {

            $c .= '<li data-id="'.$d['id'].'"'.($opened ? ' data-jstree=\'{"opened":true}\'': '').'>' . $d['name'];
            if (isset($d['children'])) {

                $c .= $this->generateTree($d['children'], $opened=false);
            }
            $c .= '</li>';
        }

        return $c . '</ul>';
    }
    
}
