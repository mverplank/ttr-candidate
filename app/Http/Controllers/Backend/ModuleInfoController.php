<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\ModuleInfo;
/*use Carbon\Carbon;
use Session;
use Share;
use Auth;*/

class ModuleInfoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Fetch a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_module_info(Request $request)
    {
        $module_info_obj = new ModuleInfo();
        //echo "<pre>";print_R($request->all());die('success');
        $data     = $request->all();
        $get_data = $module_info_obj->getInfo($data);
        if($get_data){
            return array('status'=>'success', 'data'=>$get_data);
        }else{
            return array('status'=>'error');
        }        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_module_info(Request $request)
    {
        $module_info_obj = new ModuleInfo();
        //echo "<pre>";print_R($request->all());die('success');
        $data = $request->all();
        $save = $module_info_obj->saveInfo($data);
        if($save){
            return array('status'=>'success');
        }else{
            return array('status'=>'error');
        }        
    }
}
