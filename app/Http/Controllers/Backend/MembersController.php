<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\User;
use App\Models\Backend\Membership;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;
use Session;

class MembersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = new User();
        //$all_members = $users->getAllMembers();
        //echo "<pre>";print_R($all_members->count());die('suscsca');        
        //return view('backend/user/members')->with(compact('all_members'));
        return view('backend/user/members');
    }

    public function ajax_index(Request $request)
    {
        //$all_members = $users->getAllMembers();

        $members = new User();
        $draw    = 1;
        $type    = $request->input('type');
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }    
        
        $all_members   = $members->getAllMembers($start, $length, $order, $dir, null, $search_value);
        $count_members = $members->countMembers($order, $dir, null, $search_value);
       
        $data = array();
        
        $i = 0;   
        $user_type = "'member'";
        foreach($all_members as $member){
            $data[$i][]  = $member->title." ".$member->firstname;
            $data[$i][]  = $member->lastname." ".$member->sufix;
            $data[$i][]  = (is_null($member->membership)) ? '--' : $member->membership;
            $data[$i][]  = showMembershipEnd($member->membership_end);
            $data[$i][]  = '<a title="Edit" href="'.url('admin/user/members/edit/'.$member->id ).'"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a><a title="Delete" href="javascript:void(0)" onclick="deleteUser('.$member->id.', this, '.$user_type.')" ><i class="glyphicon glyphicon-trash"></i></a>';
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_members,
                    'recordsFiltered' => $count_members,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    // Add members
    public function add_member(Request $request)
    {
        // Get all memberships
        $memberships = array();
        $membership_obj  = new Membership;
        $all_memberships = $membership_obj->getAllMemberships();
        
        foreach($all_memberships as $membership){
            $memberships[$membership->id] = $membership->name;
        }
        if ($request->isMethod('post')) {
           
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $members  = new User();
            $response = $members->add($data, 'Member');
            if($response){
                $message = "New Member has been created successfully!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the member. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/members');
        }else{
            return view('backend/user/member_add', compact('memberships'));
        }
    }   
    // Edit Users
    public function edit_member($id, Request $request)
    {
        // Get all memberships
        $memberships = array();
        $membership_obj  = new Membership;
        $all_memberships = $membership_obj->getAllMemberships();
        
        foreach($all_memberships as $membership){
            $memberships[$membership->id] = $membership->name;
        }
        $member = User::with('profiles')->with('user_settings')->where('id',$id)->first();
        $user_membership = $member->user_settings()->with('membership')->first();
       
        if ($request->isMethod('put') || $request->isMethod('post')) {
           
            $update_data = $request->data;
            if (empty($update_data['Member']['password'])) {
                unset($update_data['Member']['password']);
            }
            $member = new User();
            $update = $member->edit($id, $update_data);
            if($update){
                $message = "The member details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the member. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/members');
        }else{
            return view('backend/user/member_edit', compact('member', 'memberships', 'user_membership'));
        }
    }

    // Delete Users
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $user_id = $post_data['id'];
                $user = new User();
                $delete = $user->deleteUser($user_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    // Export Members
    public function export_members(){ 
  
        $result = array();
        $members =DB::table('users')
                    ->where(function($q) {
                       $q->where('users.id', '!=', Auth::id())
                       ->where('users.deleted',0)
                       ->where('users.groups_id','=',3);
                    })
                    ->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
                    ->select('profiles.firstname', 'profiles.lastname','users.email', 'profiles.phone', 'profiles.cellphone', 'profiles.skype', 'profiles.skype_phone')
                    ->orderBy('profiles.firstname', 'asc')
                    ->get()->toArray(); 
                    foreach ($members as $member) {
                        //modification
                        $result[] = (array)$member;  
                    } 

        return Excel::create('members', function($excel) use ($result) {
            $excel->sheet('member', function($sheet) use ($result)
            {
               $sheet->fromArray($result);
            });
        })->download('csv');
    }

}
