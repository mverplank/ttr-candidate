<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\Host;
use App\Models\Backend\Schedule;
use App\Models\Backend\EpisodeScheduledShare;
use Illuminate\Routing\ResponseFactory;
use Auth;
use Share;
//use App\Models\Backend\Show;
/*use Session;
use Validator;

use Illuminate\Support\Facades\Storage;*/

class SchedulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($channel_id='', Request $request)
    {
        $get_channel_id = '';
        if(isset($request->channel_id)){
            $get_channel_id = $request->channel_id;
        }

        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
       
        $weeks = array(
                    '0' => 'each week',
                    '1' => '1st week',
                    '2' => '2nd week',
                    '3' => '3rd week',
                    '4' => '4th week',
                    '5' => '5th week'
                );

        $months = array(
                    '0'  => 'each month',
                    '1'  => 'January',
                    '2'  => 'February',
                    '3'  => 'March',
                    '4'  => 'April',
                    '5'  => 'May',
                    '6'  => 'June',
                    '7'  => 'July',
                    '8'  => 'August',
                    '9'  => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December'
                );

        $years = array(
                    '0'        => 'each year',
                    date('Y')  => date('Y'),
                    date('Y', strtotime('+1 year'))  => date('Y', strtotime('+1 year')),
                    date('Y', strtotime('+2 year'))  => date('Y', strtotime('+2 year'))
                );

        $minutes = array();
        for($i=45;$i<1440;$i++){
            $minutes[$i] = $i; 
        }

        // Remove from schdules all outdated shcedule rows
        $scheduleObj = new Schedule();
        $scheduleObj->deleteOutdatedSchedules(); 
        //echo $get_channel_id;die;
        return view('backend/channel/channels_schedule')->with(compact('all_channels', 'weeks', 'months', 'years', 'minutes', 'channel_id', 'get_channel_id'));
    }

    public function ajax_index(Request $request)
    {
        // Remove from schdules all outdated shcedule rows
        $scheduleObj = new Schedule();
        $scheduleObj->deleteOutdatedSchedules(); 

        $channels     = new Channel();
        $draw         = 1;
        $start        = $request->input('start');
        $length       = $request->input('length');
        $draw         = $request->input('draw');
        $order        = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
       
        $all_channels = $channels->getChannels($start, $length, $order, $dir, null, $search_value);
        $count_channels = $channels->countChannels($order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   

        if(!empty($all_channels)){
            foreach($all_channels as $channel){
               
                $data[$i][]  = '<h4>'.$channel->name.'<h4>';
                $data[$i][]  = strlen($channel->description) > 400 ? substr($channel->description,0,400).'...' : $channel->description;
                                             
                                   
                $data[$i][]  = '<a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit Schedule"><i class="glyphicon glyphicon-time"></i></a>
                                    <a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit Website Content"><i class="glyphicon glyphicon-blackboard"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteChannel('.$channel->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $all_channels,
                    'recordsFiltered' => $count_channels,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    /**
     * Show the schedules for the episodes.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shows_agenda(Request $request)
    {
        $type         = 'admin';
        $all_channels = '';
        
        if (Auth::check())
        {
            $userId = Auth::id();
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                //Fetch all Channels
                $type = 'host';
                $host = new Host();
                $get_host_id  = $host->getHostId($userId);
                if($get_host_id != 0){
                    $all_channels = $host->getAllChannels($get_host_id);
                }
            }else{
                //Fetch all Channels
                $channels = new Channel();
                $all_channels = $channels->getAllChannels();
            }
       
            $weeks = array(
                        '0' => 'each week',
                        '1' => '1st week',
                        '2' => '2nd week',
                        '3' => '3rd week',
                        '4' => '4th week',
                        '5' => '5th week'
                    );

            $months = array(
                        '0'  => 'each month',
                        '1'  => 'January',
                        '2'  => 'February',
                        '3'  => 'March',
                        '4'  => 'April',
                        '5'  => 'May',
                        '6'  => 'June',
                        '7'  => 'July',
                        '8'  => 'August',
                        '9'  => 'September',
                        '10' => 'October',
                        '11' => 'November',
                        '12' => 'December'
                    );

            $years = array(
                        '0'        => 'each year',
                        date('Y')  => date('Y'),
                        date('Y', strtotime('+1 year'))  => date('Y', strtotime('+1 year')),
                        date('Y', strtotime('+2 year'))  => date('Y', strtotime('+2 year'))
                    );

            $minutes = array();
            for($i=45;$i<1440;$i++){
                $minutes[$i] = $i; 
            }

            $get_channel_id = '';
            if(isset($request->channel_id)){
                $get_channel_id = $request->channel_id;
            }
           
            return view('backend/show/shows_schedule')->with(compact('all_channels', 'weeks', 'months', 'years', 'minutes', 'get_channel_id'));
        }else{
            return redirect('/');
        }
    }

    public function shows_ajax_agenda(Request $request)
    {
        $channels  = new Channel();
        $draw      = 1;
        $start     = $request->input('start');
        $length    = $request->input('length');
        $draw      = $request->input('draw');
        $order     = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
    
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
       
        $all_channels = $channels->getChannels($start, $length, $order, $dir, null, $search_value);
        $count_channels = $channels->countChannels($order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   

        if(!empty($all_channels)){
            foreach($all_channels as $channel){
               
                $data[$i][]  = '<h4>'.$channel->name.'<h4>';
                $data[$i][]  = strlen($channel->description) > 400 ? substr($channel->description,0,400).'...' : $channel->description;
                                             
                                   
                $data[$i][]  = '<a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit Schedule"><i class="glyphicon glyphicon-time"></i></a>
                                    <a href="'.url('admin/channel/channels/edit/'.$channel->id ).'" title="Edit Website Content"><i class="glyphicon glyphicon-blackboard"></i></a>
                                    <a href="javascript:void(0)" onclick="deleteChannel('.$channel->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $all_channels,
                    'recordsFiltered' => $count_channels,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    
    /**
     * @description Add the schedule of the channel
     * @param $request is the Request object 
     * @return json response 
     */
    public function add_schedule(Request $request)
    {
        parse_str($request->data, $post_data);
        $data = array();
        if ($request->isMethod('post')) {
        
            $schedule    = new Schedule();
            $response    = $schedule->add($post_data['data']);
            if($response){
                $data['message'] = "New Schedule has been successfully created!";
                $data['status']  = "success";
            }else{
                $data['message'] = "There is some problem while creating the schedule. Please try again.";
                $data['status']  = "error";
            }
        }else{
            $data['status']  = "error";
        }
        return json_encode($data);
    }

    /**
     * @description Edit the schedule of the channel
     * @param $request is the Request object 
     * @return json response 
     */
    public function edit_schedule(Request $request)
    {
        parse_str($request->data, $post_data);
       
        $data = array();
        if ($request->isMethod('post')) {
        
            $schedule    = new Schedule();
            $response    = $schedule->edit($post_data['data']);
            if($response){
                
                $data['message'] = "Schedule has been successfully updated!";
                $data['status']    = "success";
            }else{
              
                $data['message'] = "There is some problem while updating the schedule. Please try again.";
                $data['status']     = "error";
            }
            
        }else{
            $data['status']  = "error";
        }
        return json_encode($data);
    }


    /**
     * @description Delete the schedule
     * @param $request is the Request object containing the id of the schedule
     * @return json response 
     */
    public function delete_schedule(Request $request)
    {
        $data = array();
        if ($request->isMethod('post')) {
            $schedule_id = $request->id;
            $schedule    = Schedule::find($schedule_id);
            $response    = $schedule->update(['deleted'=>1]);
            if($response){      

                $data['message'] = "Schedule has been successfully deleted!";
                $data['status']  = "success";

            }else{              
                $data['message'] = "There is some problem while deleting the schedule. Please try again.";
                $data['status']  = "error";
            }
        }else{
            $data['status']  = "error";
        }
        return $data;
    }

    /**
     * @description Fetch schedules according to the month and year
     * @param 
     * @return view 
     */
    public function get_shows_schedule(Request $request)
    {
        $start_date = $request->start;
        $end_date   = $request->end;
        $channel_id = $request->channel_id;
        $schedule   = new Schedule();
        $host_id    = 0;
        if (Auth::check())
        {
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                $host    = new Host();
                $host_id = $host->getHostId(Auth::id());
            }
        }
        $fetch_schedule  = $schedule->getCalendarEvents($start_date, $end_date, $channel_id, $host_id);
        
        //return array('calendarEvents' => $fetch_schedule['calendarEvents']);
        //return Response::json($fetch_schedule['calendarEvents'],200);
        return response()->json($fetch_schedule['calendarEvents']);
    } 


    /**
     * @description Fetch schedules according to the start and end date for the episodes
     * @param 
     * @return view 
     */
    public function get_schedule(Request $request)
    {
        $month  = $request->month;
        $year   = $request->year;
        $colors = [];
        $channel_id     = $request->channel_id;    
        $schedule       = new Schedule();
        $fetch_schedule = $schedule->fetchSchedule($month, $year, $channel_id);
        $schedules      = array();
        $calendarEvents = array();
        $calendarEventsData = array();  
        if(!empty($fetch_schedule)){
            // Prepare data for FullCalendar                    
            $slots = array();
            foreach ($fetch_schedule as $schedule) {
                $new_schedule = $schedule;               
                //
                // Fullcalendar format events date/time definition 
                //                               
                $dow_arr  = array();
                $week_arr = array();
                if(!empty($schedule->schedule_dow)){
                    $dow_arr = explode(",", $schedule->schedule_dow);
                }
                // Make multiple slots for multiple weeks
                $week_arr = explode(",", $schedule->week);
                
                if(!empty($dow_arr)){
                    foreach($dow_arr as $dow){
                        $new_schedule->schedule_dow = $dow;
                        $start = $this->day2date($dow) . ' ' . $schedule->schedule_time;
                        $dstart = strtotime($start);
                        $dstop = $dstart + (int)$schedule->duration*60; 
                        // duration uses minutes unit [min] 
                    
                        if ($schedule->type === 'replay') { // replay show                    
                            if (isset($schedule->replay_dow)) {                       
                                $title = '[R: ' . (isset($schedule->channels_id) && $channel_id != $schedule->channels_id ? 'Ch#' . $schedule->channels_id . ' ' : '') . $schedule->replay_dow . ' ' . (($schedule->replay_time) ? date("g:i a", strtotime($schedule->replay_time)) : '') . '] ' . ($schedule->replays_count == 1 ? $schedule->name : '');
                                $bg = '#444444';
                            } else { 
                                // show to be replayed DON'T exists (was deleted)
                                $title = "[R: SCHEDULED SHOW DELETED!]\nRe-assign to new/other show";
                                $bg = '#FFA112';
                            }                            
                        } elseif ($schedule->type === 'live') { // live show
                            
                            $title = $schedule->name;
                            $bg = '#C60000';                            
                        } else { // file or stream show 
                            
                            $title = $schedule->name;
                            if (!($schedule->is_encore)) {
                                $bg = '#608800';
                            } else {
                                $bg = '#8F40A8';
                            }               
                        }
                        
                        $slotDate = date("Y-m-d H:i", $dstart);
                        if (!isset($slots[$slotDate])) {
                            $slots[$slotDate] = array();
                        }

                        
                        if(!empty($week_arr)){
                            foreach($week_arr as $week){
                                $new_schedule->week = $week;
                                $slots[$slotDate][] = array(
                                    'id' => $new_schedule->id,
                                    'title' => $title,
                                    'backgroundColor' => $bg,
                                    'borderColor' => $bg,
                                    //'eventTextColor' => '#fff',
                                    //'eventBackgroundColor' => $bg,
                                    //'eventBorderColor' => $bg,
                                    //'eventColor' => $bg,
                                    'start' => ''.date("Y", $dstart).'-'.date("m", $dstart).'-'.date("d", $dstart).' '.date("H", $dstart).':'.date("i", $dstart).'',
                                    'end' => '' . date("Y", $dstop) . '-' . date("m", $dstop) . '-' . date("d", $dstop) . ' ' . date("H", $dstop) . ':' . date("i", $dstop) . '',
                                    'allDay' => false,
                                    // custom row data 
                                    'data' => array(
                                        'Schedule' => array(
                                            'id'   => $new_schedule->id
                                        ),
                                        'Show' => array(
                                            'name' => $new_schedule->name
                                        ),
                                        'Slot' => array(
                                            'start' => $dstart,
                                            'stop'  => $dstop,
                                            'week'  => $week,
                                            'month' => $new_schedule->month,
                                            'year'  => $new_schedule->year,
                                            'repeatStr' => $this->repeatStringify($new_schedule),
                                            'sd' => date('m/d/Y', strtotime($new_schedule->start_date)),
                                            'ed' => date('m/d/Y', strtotime($new_schedule->end_date))
                                        )
                                    )
                                );
                            }                           
                        }                        
                    }
                }               
            }
                    
            // sort slots by repeats count
            /* function cmp($a, $b) {
               
                $aTime = ($a['data']["Slot"]['year']>0?$a['data']["Slot"]['year']:'0000').str_pad($a['data']["Slot"]['month'], 2, "0", STR_PAD_LEFT).$a['data']["Slot"]['week'];
                $bTime = ($b['data']["Slot"]['year']>0?$b['data']["Slot"]['year']:'0000').str_pad($b['data']["Slot"]['month'], 2, "0", STR_PAD_LEFT).$b['data']["Slot"]['week'];                    
                return $aTime > $bTime;
            }*/
            
            foreach ($slots as $dateTime => $slot) {
                if(!empty($slot)){
                    //echo "<pre>";print_R($slot);
                    if (!isset($slot[1])) { // only single show assigned to this time slot 
                        //
                        // If only single show assinged to schedule time slot AND not default repeat (not each week of each month of each year) 
                        // show this repeat period directly on time shol on schedule 

                        //echo "slot slot<pre>";print_R($slot);

                        if ($slot[0]['data']['Slot']['week'] > 0 || $slot[0]['data']['Slot']['month'] > 0 || $slot[0]['data']['Slot']['year'] > 0) {

                            $slot[0]['data']['Slot']['dow'] = date("D", strtotime($slot[0]['data']['Slot']['start']));
                            $slot[0]['title'] = $slot[0]['title'] . "\n(" . strip_tags($this->repeatStringify($slot[0]['data']['Slot'])) . ')';
                        }                
                           //die('frefref');
                        $calendarEvents[] = $slot[0];
                        
                    } else {    // multiple shows assigned 

                        usort($slot,array($this,"cmp"));
                        //echo "Second <pre>";print_r($slot);die;
                        $dstart = $slot[0]['data']['Slot']['start'];
                        $dstop = $slot[0]['data']['Slot']['stop'];
                        
                        $calendarEvents[] = array(                    
                            'id' => $slot[0]['id'],
                            'title' => false,
                            'backgroundColor' => '#6B6B6B',
                            'borderColor' => '#6B6B6B',
                            //'eventTextColor' => '#fff',
                            //'eventBackgroundColor' => $bg,
                            //'eventBorderColor' => $bg,
                            //'eventColor' => $bg,
                            'start' => ''.date("Y", $dstart).'-'.date("m", $dstart).'-'.date("d", $dstart).' '.date("H", $dstart).':'.date("i", $dstart).'',
                            'end' => '' . date("Y", $dstop) . '-' . date("m", $dstop) . '-' . date("d", $dstop) . ' ' . date("H", $dstop) . ':' . date("i", $dstop) . '',
                            'allDay' => false,
                            // custom row data 
                            'data' => array(
                                'Title' => date("l, g:i a", $dstart).' - '.date("g:i a", $dstop),
                                'Slots' => $slot
                            )
                        );
                    }
                }                
            }
        }
        return array('calendarEvents' => $calendarEvents, 'calendarEventsData' => $calendarEventsData);
    }   

    // sort slots by repeats count
    private static function cmp($a, $b) {
          
        $aTime = ($a['data']["Slot"]['year']>0?$a['data']["Slot"]['year']:'0000').str_pad($a['data']["Slot"]['month'], 2, "0", STR_PAD_LEFT).$a['data']["Slot"]['week'];
        $bTime = ($b['data']["Slot"]['year']>0?$b['data']["Slot"]['year']:'0000').str_pad($b['data']["Slot"]['month'], 2, "0", STR_PAD_LEFT).$b['data']["Slot"]['week'];
        return $aTime > $bTime;
    }
    /**
     * Convert week day name to MySQL date relative to Today 
     * 
     * @param String $day
     */
    private function day2date($day = 'Mon', $format="Y-m-d", $firstDayOfWeek =  'Sunday') {

        $days = array();
        if (date("l") !== $firstDayOfWeek) {
            $d = strtotime('last ' . $firstDayOfWeek);
        } else {
            $d = time();
        }
        
        $days[date("D", $d)] = date($format, $d);
      
        for ($i = 0; $i < 6; $i++) {
            $d = strtotime('+1 day', $d);

            $days[date("D", $d)] = date($format, $d);
        }
        return $days[ucwords(strtolower($day))];
    }

    /**
     * Show repeat cycle as string description 
     * 
     * @param Array $d
     * @return String
     */
    private function repeatStringify($d) {
        //echo "dddddddddddd = <pre>";print_R($d);
       /* if($d->id == 3565){
            echo "dddddddddddd = <pre>";print_R($d);
        }*/
        $year = '';
        $month = '';
        $week = '';
        if(is_array($d)){
            if ($d['year']>0) {            
                $year = ' '.$d['year'];
            }
            if ($d['month']>0) {            
                $month = ' of '.date("F", strtotime(date('Y').'-'.$d['month']));
            }
            if ($d['week']>0) {
                $sdow = '';
                if(isset($d['schedule_dow'])){
                    $sdow = $d['schedule_dow'];
                }else if(isset($d['dow'])){
                    $sdow = $d['dow'];
                }
                $week = ' ' . $d['week'].$this->abbr($d['week']).' '.date("l", strtotime($sdow)) .($d['month']==0 ? ' of each month ' : '');
            } else {
                $week = ' each week';
            }
        }else{
            if ($d->year>0) {            
                $year = ' '.$d->year;
            }
            if ($d->month>0) {            
                $month = ' of '.date("F", strtotime(date('Y').'-'.$d->month));
            }
            if ($d->week>0) {
                $week = ' ' . $d->week.$this->abbr($d->week).' '.date("l", strtotime($d->schedule_dow)) .($d->month==0 ? ' of each month ' : '');
            } else {
                $week = ' each week';
            }
        }
        
        return '<span>Repeat '.$week.$month.$year.'</span>';
    }

    private function abbr($number) {

        $ends = array('th','st','nd','rd','th','th','th','th','th','th');

        if (($number %100) >= 11 && ($number%100) <= 13)
           return $number. 'th';
        else
           return $ends[$number % 10];
    }

    /**
     * Fetch the shows of the channel
     * 
     * @param Array $request containg the channel id
     * @return all shows of that channel
     */
    public function getChannelShows( Request $request ){
        $shows = new Channel();
        $channelShows = $shows->fetchChannelShows($request->channel_id);
        //return "hello";
        //echo "<pre>";print_R($channelShows);die;
        return response()->json($channelShows);
        //return ['data' => $channelShows];
        //return $channelShows;
    }

    /**
     * Fetch the replays of the shows
     * 
     * @param Array $request containg the channel id
     * @return all replays of the shows for that channel
     */
    public function getShowsReplay( Request $request ){
       
        $schedule = new Schedule();
        $data     = array();
        $data['replays'] = $schedule->fetchReplays($request->channel_id);
        
        // Channels         
        $channels     = new Channel();
        $all_channels = $channels->getAllChannels();
        
        if($all_channels->count() > 0){
            foreach($all_channels as $key=>$channel){
                $showsReplays = $schedule->fetchReplays($key);
                if (empty($showsReplays)) { 
                    // show channel only if contains any shows already 
                    $all_channels->forget($key);
                }
            }
            $data['channels'] = $all_channels;
        }        
        return $data;
    }

    /**
     * Fetch the replays of the shows
     * 
     * @param Array $request containg the channel id
     * @return all replays of the shows for that channel
     */
    public function getSelectedSchedule( Request $request ){
       
        $scheduleObj = new Schedule();
        $data        = array();
        $schedule    = $scheduleObj->fetchScheduleByID($request->schedule_id);

        if($schedule){
            $data['id']           = $schedule->id;
            $data['channels_id']  = $schedule->channels_id;
            $data['shows_id']     = $schedule->shows_id;
            $data['schedules_id'] = $schedule->schedules_id;
            $data['dow']          = $schedule->dow;
            $data['week']         = $schedule->week;
            $data['month']        = $schedule->month;
            $data['year']         = $schedule->year;
            $data['time']         = date("g:i A", strtotime($schedule->time));
            $data['duration']     = $schedule->duration;
            $data['type']         = $schedule->type;
            $data['file']         = $schedule->file;
            $data['stream']       = $schedule->stream;
            $data['type_label']   = $schedule->type_label;
            $data['start_date']   = date('m/d/Y', strtotime($schedule->start_date));
            $data['end_date']     = date('m/d/Y', strtotime($schedule->end_date));
        }
       
        return json_encode($data);
    }

    /**
     * Fetch the replays of the episode schedules by current date
     * @param $request 
     */
    public function check_schedule(Request $request){
            $schedules = EpisodeScheduledShare::whereDate('time', '>=', date("Y-m-d h:i:s", time()))->get();

            foreach ($schedules as $schedule) {
                //echo "<pre>";print_r($schedule);die();
                if($schedule->is_facebook == 1){
                    $url = Share::load('http://178.128.179.99/e/'.$schedule->episodes_id, $schedule->desc)->facebook();
                }
                if($schedule->is_twitter == 1){
                    $url = Share::load('http://178.128.179.99/e/'.$schedule->episodes_id, $schedule->desc)->twitter();
                }
            }
           
    }
}
