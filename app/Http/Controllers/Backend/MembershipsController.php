<?php

/*
 * @author : Contriverz
 * @since  : 04-01-2019
 */
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Membership;
use App\Models\Backend\InventoryItem;

use Auth;
use Session;

class MembershipsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all items
     */
    public function index(Request $request) {

        $memberships = new Membership();
        $membership_type = '';
        if ($request->has('type')) {
            $membership_type = $request->input('type');
        }
       
        return view('backend/membership/memberships')->with(compact('membership_type'));
    }

    public function ajax_index(Request $request)
    {
        $memberships   = new Membership();
        $draw          = 1;
        $type          = $request->input('type');
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        $membership_type = '';
        if ($request->has('type')) {
            $membership_type = $request->input('type');
        }
     
        $all_memberships   = $memberships->getAllMemberships($membership_type,$start, $length, $order, $dir, null, $search_value);
        $count_memberships = $memberships->countAllMemberships($membership_type, $order, $dir, null, $search_value);
          
        $data = array();
        $i    = 0;   

        if(!empty($all_memberships)){
            foreach($all_memberships as $membership){
                $data[$i][] = $membership->name;
                $add_monthly_items = "";
                if($membership->type == "website-user"){
                    $add_monthly_items .= '<a href="'.url('admin/inventory/months/index').'/'.$membership->id.'" title="Edit monthly/weekly items"><i class="ti-calendar"></i></a> ';
                }

                $data[$i][] = ' <a href="'.url('admin/membership/memberships/edit/'.$membership->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)" onclick="deleteMembership('.$membership->id.', this)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>'.$add_monthly_items;
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_memberships,
                    'recordsFiltered' => $count_memberships,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    /**
     * ADD 
     */
    public function add_membership(Request $request) {
    
        $current_user_id = 0;
        $inventory_item  = new InventoryItem;
        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();
        }
        $all_items = $inventory_item->findItemsOfCurrentUser($current_user_id);
       
        if ($request->isMethod('post')) {
            $data = $request->input('data');
            $var  = "";
            $message    = "";
            $membership = new Membership();
            $insert_id  = $membership->add($data);
            if($insert_id){
                // Save profile image
                $message = "New membership has been created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the membership. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/membership/memberships');
        }else{
            return view('backend/membership/add', compact('all_items'));
        }
    }

    /**
     * EDIT 
     */
    public function edit_membership($id = null, Request $request) {
       
        $membership = Membership::with('membership_periods')->with('inventory_items')->where('id',$id)->first();
        $current_user_id = 0;
        $inventory_item  = new InventoryItem;
        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();
        }
        $all_items = $inventory_item->findItemsOfCurrentUser($current_user_id);
        $items = array();
        if(isset($all_items) && !empty($all_items)){
            foreach($all_items as $item){
                $items[$item->id] = '('.$item->category_name.') '.$item->name;
            }
        }

        if ($request->isMethod('put') || $request->isMethod('post')) {
            $update_data = $request->data;
            $membership = new Membership();
            $update = $membership->edit($id, $update_data);
            if($update){
                $message = "The membership details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the membership. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/membership/memberships');
        }else{
            return view('backend/membership/edit', compact('membership', 'items'));
        }
    }   

    /**
     * DELETE Memberships
     */
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){

                $membership_id = $post_data['id'];              
                $membership = new Membership();
                $delete = $membership->deleteMembership($membership_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }
}
