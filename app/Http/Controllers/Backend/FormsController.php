<?php

namespace App\Http\Controllers\Backend;

use Validator;
use Illuminate\Http\Request;
use App\Models\Backend\Channel;
use App\Models\Backend\User;
use App\Models\Backend\Show;
use Session;
use Auth;

class FormsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function validate_input(Request $request)
    {
       // echo "<pre>";print_R($request->all());die('success');
        $form_user_id = $request->user_id;
        $data  = [$request->details['fieldname'] => $request->value];
        $table = '';
        $userId   = 0;
        $no_check = 0;
        $moduleId = 0;
       // echo "<pre>";print_R($request->all());die;
        if(isset($request->details['formtype'])){
            if($request->details['formtype'] == 'profile'){
               $moduleId  = Auth::user()->id;
            }elseif ($request->details['formtype'] == 'userprofile') {
               $moduleId = $request->details['userId'];
            }elseif ($request->details['formtype'] == 'show') {
               $moduleId = $request->details['moduleId'];
            }
        }
       

        if($request->details['model'] == 'Channel'){
            $table = with(new Channel)->getTable();
        }else if($request->details['model'] == 'User' || $request->details['model'] == 'Member' || $request->details['model'] == 'Partner'){
            $table = with(new User)->getTable();
        }else if($request->details['model'] == 'Show'){
            $table = with(new Show)->getTable();
        }
        
        if($moduleId != 0){
            if($request->details['model'] != "Partner"){
                if($moduleId == $form_user_id){
                    $no_check = 1;
                }
            }
        }
      
        $validation = Validator::make($data, [
            $request->details['fieldname'] =>'required|'.$request->type.':'.$table.','.$request->details['fieldname'].','.$moduleId,
        ]);
   
        if ($validation->fails() && $no_check == 0) {
            //$messages = $validation->getMessageBag()->$request->details['fieldname']();
            $messages = $validation->messages()->first();
            return response()->json(['status' => 'error', 'alert' => $messages]);
        }else{
            // /die('yes');
            return response()->json(['status' => 'success', 'alert' => '']);
        }
        /*}else
        {*/
            //return response()->json(['status' => 'success', 'alert' => '']);
        //}
       

        
        
        /*$validator = $request->validate([
            $request->details['fieldname'] => 'required|'.$request->type.':'.$table,
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'alert' => $validator]); 
            //return Redirect::to('workcharts/'.$request->workchart_id.'/create')->withErrors($validator);
        }else{
            return response()->json(['status' => 'success', 'alert' => '']); 
        } */
        
    }

    public function unique_validation($model, $table, $value){

    }

}
