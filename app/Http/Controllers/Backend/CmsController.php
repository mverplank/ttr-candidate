<?php
/**
 * @author : Contriverz
 * @since  : 28-03-2019
 */
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Media;
use App\Models\Backend\MediaLinking;
use ButterCMS\ButterCMS;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Auth;
use View;

class CmsController extends Controller
{

    //private $apiToken;
    private static $apiToken = '7b76b1534d29de0b8650610a7b2b5298e06e642b';
    private $client;


    /**
     * Create a new controller instance.
     *
     * @return void
     */   
    public function __construct()
    {
        $this->client = new ButterCMS(CmsController::$apiToken);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       //die('hjeghdg');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private function media($type, $start=0, $length=15)
    {
        if(!empty($type)){
            $outer_path = 'media';
            $images     = [];
            $has_folder = Storage::disk('public')->has($outer_path);
            if($has_folder){
                $all_media = new Media();                
                $all_media = $all_media->where('type', $type);
                
                /*if(!empty($request->date)){
                    $check_date = Carbon::parse($request->date)->format('Y-m-d');
                    $all_media  = $all_media->whereRaw('DATE(created_at) = "'.$check_date.'"');
                }*/
                $all_media = $all_media->orderBy('id', 'desc')->offset($start)->limit($length)->get();
                $i = 0;
                foreach($all_media as $file){
                    $path = 'media/'.$file->id.'/'.$type;
                    $has_inside_folder = Storage::disk('public')->has($path);
                    if($has_inside_folder){
                        $created = strtotime($file->created_at);
                        $updated = strtotime($file->updated_at);
                        $images[$i]['id']  = $file->id;
                        $images[$i]['url'] = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        if($type == 'image'){
                            $images[$i]['src'] = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                            $images[$i]['class'] = "";
                        }else if($type == 'audio'){
                            $images[$i]['src'] = url('/').'/storage/app/public/media/default/audio.png';
                            $images[$i]['class'] = "set_image";
                        } else if($type == 'video'){
                            $images[$i]['src'] = url('/').'/storage/app/public/media/default/video.png';
                            $images[$i]['class'] = "set_image";
                        }
                        $images[$i]['file']          = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        $images[$i]['name']          = $file->filename;
                        $images[$i]['media_id']      = $file->id;
                        $images[$i]['type']          = $file->type;
                        $images[$i]['title']         = $file->title;
                        $images[$i]['created_at']    = date('Y-m-d',$created);
                        $images[$i]['updated_at']    = date('Y-m-d',$updated);
                        $images[$i]['alt']           = $file->alt;
                        $images[$i]['caption']       = $file->caption;
                        $images[$i]['uploaded_by']   = $file->uploaded_by;
                        $images[$i]['uploaded_area'] = $file->uploaded_area;
                        $images[$i]['description']   = $file->description;
                        $images[$i]['meta_json']     = $file->meta_json;
                        if(!empty($file->meta_json)){                           
                            if(isset($file->meta_json->filesize)){
                                $images[$i]['size'] = number_format($file->meta_json->filesize / 1024, 2);
                            }
                        }                                               
                        $i++;
                    }
                }
            }
            return $images;
        }
    }

    /**
     * Fetch the total pages of the media
     * @param $type id the type(image/audio/video) of the media
     * @param $limit is the number of medias display on the screen
     * @return number of pages
     */
    private function totalPages($type, $limit, $dimension = ''){
        $media_count = $total_pages = 0;
        $all_media   = new Media();                
        $all_media   = $all_media->where('type', $type);
        if(!empty($dimension))
        {
            list($width, $height) = explode('x', $dimension);
            //$all_media = $all_media->where('height', '<=', $height)->where('width', '<=' , $width);  
            $all_media = $all_media->where('height', '=', $height)->where('width', '=' , $width);
        }
        
        /*if(!empty($request->date)){
            $check_date = Carbon::parse($request->date)->format('Y-m-d');
            $all_media  = $all_media->whereRaw('DATE(created_at) = "'.$check_date.'"');
        }*/

        $media_count = $all_media->count();
        if($media_count != 0){
            $total_pages = ceil($media_count / $limit);
        }
        return $total_pages;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ajax_media(Request $request)
    {
        $all_files = [];
       
        $type = $request->type;       
        $path = 'media/'.$type;
        $has_folder = Storage::disk('public')->has($path);
        if($has_folder){
            $files = Storage::disk('public')->files($path);
            
            if($files){
                $tmpdata = [];
                $i = 0;
                foreach($files as $file){
                    $all_files[$i]['path'] = $file;                    
                    $all_files[$i]['file'] = storage_path() .'/'.$file;
                    $all_files[$i]['size'] = Storage::disk('public')->size($file);
                    $all_files[$i]['name'] = explode('/', $file)[2];                   
                    $all_files[$i]['type'] = Storage::disk('public')->mimeType($file);
                    $all_files[$i]['url']  = url('/').'/storage/app/public/'.$file;
                    if($type == 'image'){
                        $all_files[$i]['src']  = url('/').'/storage/app/public/'.$file;
                    } else if($type == 'audio'){
                        $all_files[$i]['src']  = url('/').'/storage/app/public/media/default/audio.png';
                    } else if($type == 'video'){
                        $all_files[$i]['src']  = url('/').'/storage/app/public/media/default/video.png';
                    }
                    $i++;
                }
            }
        }
        
        if(count($all_files) > 0){
            return response()->json(['status' => 'success', 'data' => $all_files]);
        }else{
            return response()->json(['status' => 'error']);
        }
        exit();
    }

    /**
     * @desc Add the media 
     * @param $type is the media type like the images, audios and videos
     */
    public function media_add($type, Request $request){
        $start  = 0;
        $length = 15;
        $ajax   = 0;
        if($request->has('ajax')){
            $ajax = $request->ajax;
        }
        //
        // Get All Media
        $all_media = $this->media($type, $start, $length);
        // Get total pages
        $total_pages = $this->totalPages($type, $length);
        if ($request->hasFile('files')) {
           // echo "<pre>";print_R($request->all());die('success');
            $file = $request->file('files')[0];
            $filename   = $file->getClientOriginalName();
            $name =preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
            $media_data = array();
            if ($file->isValid()) {                
                // Add IMAGE              
                $filename   = $file->getClientOriginalName();
                $name =preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                $mimeType   = $file->getMimeType();
                $dimension  = getimagesize($file);                
                $width      = $dimension[0];
                $height     = $dimension[1];                    
                $extension  = \Illuminate\Support\Facades\File::extension($filename);
                $image_path = 'media';                        
                $exists     = [];
                $save_file_name =  time().'.'.$extension;
                if($save_file_name != ''){

                    //Save to database 
                    $media_data['title']  = $name;                      
                    $media_data['filename']  = $save_file_name;
                    $media_data['type']      = $type;
                    $media_data['uploaded_by']   = Auth::user()->id;
                    $media_data['uploaded_area'] = 'media';                       
                    $mediaObj = new Media($media_data);
                    $mediaObj->save();
                    $image['id'] = $mediaObj->id;                    
                    $new_path = $image_path.'/'.$image['id'].'/'.$type;
                    $new_exists = Storage::disk('public')->has($new_path);

                    //if new folder not exist then create new folder
                    if(!$new_exists){
                        Storage::disk('public')->makeDirectory($new_path);
                    }
                    $path = Storage::disk('public')->putFileAs($new_path, $file, $save_file_name);
                    $image['path']      = $path;
                    $image['title']     = $name;
                    $image['file_name'] = $save_file_name;
                    $image['size']      = Storage::disk('public')->size($path);
                    $image['accepted']  = true;
                    $image['url']       = url('/').'/storage/app/public/'.$path;

                    // Update the meta data of the file after uploading the image
                    $meta_json = array(
                                    'filename' => $save_file_name,
                                    'filetype' => $extension,
                                    'filesize' => $image['size'],
                                    'image' => array(
                                                'width'  => $width,
                                                'height' => $height,
                                                'mime'   => $mimeType
                                                ),
                                    );
                    $mediaObj->update(['meta_json'=>$meta_json, 'height'=>$height, 'width'=>$width]);
                    return response()->json(['upload' => 'success', 'data' => $image]);

                }else{
                    return response()->json(['new_name' => $save_file_name, 'location' => $image_path, 'path' => '', 'upload' => 'error']);
                }                
            }
        }

        if($ajax == 1){

           /* $data['type']        = $type;
            $data['all_media']   = $all_media;
            $data['total_pages'] = $total_pages;
            return array('data'=>$data);*/
            $output['type']         = $data['type']        = $type;
            $data['all_media'   ]   = $all_media;
            $output['total_pages']  = $data['total_pages'] = $total_pages;
            $view                   = View::make('backend.cms.add_ajax_media', $data);
            $output["html"]         = $view->render();
            echo json_encode($output);
            //return view('backend/cms/add_ajax_media')->with(compact('type', 'all_media', 'total_pages'));
        }else{
            //echo "<pre>";print_R($all_media);die;
            return view('backend/cms/add_media')->with(compact('type', 'all_media', 'total_pages'));
        }
        
    }

    /**
     * @desc Add the media 
     * @param $type is the media type like the images, audios and videos
     */
    public function ajax_media_add($type, Request $request){
        //echo "<pre>";print_r($_FILES);die;
        //echo "<pre>";print_R($request->all());die;
        if ($request->hasFile('files')) {
            //die('1');
            $area = 'media';
            if(isset($request->module)){
                $area = strtolower($request->module);
            }
            $responsedata = array();
            $response ='';
            $mediaids = array();
            if($request->crop == '0'){
                foreach ($request->file('files') as $key => $value) {
                    if($key < $request->remaining_file){
                        $file =$value;
                        // echo $file;die;
                        $filename   = $file->getClientOriginalName();
                        $name =preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                        $media_data = array();
                        if ($file->isValid()) {                
                            // Add IMAGE        
                            $filename   = $file->getClientOriginalName();
                            $name =preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                            $mimeType   = $file->getMimeType();
                            $dimension  = getimagesize($file);
                            $width      = $dimension[0];
                            $height     = $dimension[1];                    
                            $extension  = \Illuminate\Support\Facades\File::extension($filename);
                           
                            $image_path = 'media';                        
                            $exists     = [];
                            $save_file_name =  time().'.'.$extension;
                            if($save_file_name != ''){

                                //Save to database  
                                $media_data['title']  = $name;                     
                                $media_data['filename']  = $save_file_name;
                                $media_data['type']      = $type;
                                $media_data['uploaded_by']   = Auth::user()->id;
                                $media_data['uploaded_area'] = $area;                    
                                $mediaObj = new Media($media_data); 
                                $mediaObj->save();
                                $image['id'] = $mediaObj->id;                    
                                $new_path = $image_path.'/'.$image['id'].'/'.$type;
                                $new_exists = Storage::disk('public')->has($new_path);

                                //if new folder not exist then create new folder
                                if(!$new_exists){
                                    Storage::disk('public')->makeDirectory($new_path);
                                }
                                $path = Storage::disk('public')->putFileAs($new_path, $file, $save_file_name);
                                $image['title']      = $name;
                                $image['path']      = $path;
                                $image['file_name'] = $save_file_name;
                                $image['size']      = Storage::disk('public')->size($path);
                                $image['accepted']  = true;
                                $image['url']       = url('/').'/storage/app/public/'.$path;
                                if($type == 'image'){
                                    $image['src']  = url('/').'/storage/app/public/'.$path;
                                }else if($type == 'audio'){
                                    $image['src']  = url('/').'/storage/app/public/media/default/audio.png';
                                } else if($type == 'video'){
                                    $image['src']  = url('/').'/storage/app/public/media/default/video.png';
                                }else{
                                  $image['src']  = url('/').'/storage/app/public/media/default/file.png';  
                                }
                                // Update the meta data of the file after uploading the image
                                $meta_json = array(
                                                            'filename' => $save_file_name,
                                                            'filetype' => $extension,
                                                            'filesize' => $image['size'],
                                                            'image' => array(
                                                                        'width'  => $width,
                                                                        'height' => $height,
                                                                        'mime'   => $mimeType
                                                                        ),
                                                            );
                                $mediaObj->update(['meta_json'=>$meta_json, 'height'=>$height, 'width'=>$width]);
                                array_push($responsedata, $image);
                                array_push($mediaids, $image['id']);
                                $response ='success';
                                
                                //return response()->json(['upload' => 'success', 'data' => $image]);

                            }else{
                                array_push($responsedata, ['new_name' => $save_file_name, 'location' => $image_path, 'path' => '']);
                                $mediaids='';
                                 $response ='error';
                               // return response()->json(['new_name' => $save_file_name, 'location' => $image_path, 'path' => '', 'upload' => 'error']);
                            }                
                        }
                    }
                }
               
                return response()->json(['upload' => $response, 'data' => $responsedata,'mediaids'=> json_encode($mediaids)]);
            }else{
                $file = $request->file('files')[0];
                // echo $file;die;
                $filename   = $file->getClientOriginalName();
                $name =preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                $media_data = array();
                if ($file->isValid()) {                
                    // Add IMAGE        
                    $filename   = $file->getClientOriginalName();
                    $name =preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                    $mimeType   = $file->getMimeType();
                    $dimension  = getimagesize($file);
                    $width      = $dimension[0];
                    $height     = $dimension[1];                    
                    $extension  = \Illuminate\Support\Facades\File::extension($filename);
                   
                    $image_path = 'media';                        
                    $exists     = [];
                    $save_file_name =  time().'.'.$extension;
                    if($save_file_name != ''){

                        //Save to database  
                        $media_data['title']  = $name;                     
                        $media_data['filename']  = $save_file_name;
                        $media_data['type']      = $type;
                        $media_data['uploaded_by']   = Auth::user()->id;
                        $media_data['uploaded_area'] = $area;                    
                        $mediaObj = new Media($media_data); 
                        $mediaObj->save();
                        $image['id'] = $mediaObj->id;                    
                        $new_path = $image_path.'/'.$image['id'].'/'.$type;
                        $new_exists = Storage::disk('public')->has($new_path);

                        //if new folder not exist then create new folder
                        if(!$new_exists){
                            Storage::disk('public')->makeDirectory($new_path);
                        }
                        $path = Storage::disk('public')->putFileAs($new_path, $file, $save_file_name);
                        $image['title']      = $name;
                        $image['path']      = $path;
                        $image['file_name'] = $save_file_name;
                        $image['size']      = Storage::disk('public')->size($path);
                        $image['accepted']  = true;
                        $image['url']       = url('/').'/storage/app/public/'.$path;
                        if($type == 'image'){
                            $image['src']  = url('/').'/storage/app/public/'.$path;
                        }else if($type == 'audio'){
                            $image['src']  = url('/').'/storage/app/public/media/default/audio.png';
                        } else if($type == 'video'){
                            $image['src']  = url('/').'/storage/app/public/media/default/video.png';
                        }else{
                            $image['src']  = url('/').'/storage/app/public/media/default/file.png';
                        }
                        // Update the meta data of the file after uploading the image
                        $meta_json = array(
                                                    'filename' => $save_file_name,
                                                    'filetype' => $extension,
                                                    'filesize' => $image['size'],
                                                    'image' => array(
                                                                'width'  => $width,
                                                                'height' => $height,
                                                                'mime'   => $mimeType
                                                                ),
                                                    );
                        $mediaObj->update(['meta_json'=>$meta_json, 'height'=>$height, 'width'=>$width]);
                        return response()->json(['upload' => 'success', 'data' => $image]);

                    }else{
                        return response()->json(['new_name' => $save_file_name, 'location' => $image_path, 'path' => '', 'upload' => 'error']);
                    }                
                }
                if ($request->has('croppedImage')) {
                    //die('2');
                    $file     = $request->croppedImage;
                    $filename = $name = $request->file_name;
                    $media_data = array();
                    if ($file->isValid()) {  

                        // Add IMAGE        
                        $mimeType        = $file->getMimeType();
                        $dimension       = getimagesize($file);
                        $width           = $dimension[0];
                        $height          = $dimension[1];                    
                        $extension       = \Illuminate\Support\Facades\File::extension($filename);
                        $image_path      = 'media';                        
                        $exists          = [];
                        $save_file_name  = time().'.'.$extension;
                        if($save_file_name != ''){
                            $actual_path = $image_path.'/'.$request->id.'/image';
                            $new_path    = $image_path.'/'.$request->id.'/image/'.$filename;
                            $new_exists  = Storage::disk('public')->has($new_path);
                            
                            // Delete old file and create new there
                            if($new_exists){                        
                                $old_file_path = storage_path('app/public').'/'.$new_path;
                                \File::delete($old_file_path); // Delete old flyer
                                $path = Storage::disk('public')->putFileAs($actual_path, $file, $save_file_name);

                                //Update into database
                                $media_data['filename'] = $save_file_name;
                                $media_data['height']   = $height;
                                $media_data['width']    = $width;
                                       
                                $mediaObj = Media::find($request->id); 
                                if(isset($mediaObj->meta_json->image->mime)){
                                    $mimeType = $mediaObj->meta_json->image->mime;       
                                }
                                // Update the meta data of the file after uploading the image
                                $media_data['meta_json'] = array(
                                                    'filename' => $save_file_name,
                                                    'filetype' => $extension,
                                                    'filesize' => Storage::disk('public')->size($path),
                                                    'image' => array(
                                                                'width'  => $width,
                                                                'height' => $height,
                                                                'mime'   => $mimeType
                                                                ),
                                                    ); 
                                
                                $update = $mediaObj->update($media_data);
                                $mediaObj->new_src  = url('/').'/storage/app/public/'.$path;
                                return response()->json(['upload' => 'success', 'data' => $mediaObj]);
                            }   
                        }
                    }
                }

            }
        }
    }

    /**
     * @desc Get the media first time on popup
     * @param $type is the media type like the images, audios and videos
     */
    public function get_module_media($type, Request $request){
        $start  = 0;
        $length = 15;
        // Get All Media
        $all_media = $this->media($type, $start, $length);
        // Get total pages
        $total_pages = $this->totalPages($type, $length);
        if ($request->hasFile('files')) {
            $file = $request->file('files')[0];
            $filename   = $file->getClientOriginalName();
            $media_data = array();
            if ($file->isValid()) {                
                // Add IMAGE              
                $filename   = $file->getClientOriginalName();
                $mimeType   = $file->getMimeType();
                $dimension  = getimagesize($file);                
                $width      = $dimension[0];
                $height     = $dimension[1];                    
                $extension  = \Illuminate\Support\Facades\File::extension($filename);
                $image_path = 'media';                        
                $exists     = [];
                $save_file_name =  time().'.'.$extension;
                if($save_file_name != ''){

                    //Save to database                       
                    $media_data['filename']  = $save_file_name;
                    $media_data['type']      = $type;
                    $media_data['uploaded_by']   = Auth::user()->id;
                    $media_data['uploaded_area'] = 'media';                       
                    $mediaObj = new Media($media_data);
                    $mediaObj->save();
                    $image['id'] = $mediaObj->id;                    
                    $new_path = $image_path.'/'.$image['id'].'/'.$type;
                    $new_exists = Storage::disk('public')->has($new_path);

                    //if new folder not exist then create new folder
                    if(!$new_exists){
                        Storage::disk('public')->makeDirectory($new_path);
                    }
                    $path = Storage::disk('public')->putFileAs($new_path, $file, $save_file_name);
                    $image['path']      = $path;
                    $image['file_name'] = $save_file_name;
                    $image['size']      = Storage::disk('public')->size($path);
                    $image['accepted']  = true;
                    $image['url']       = url('/').'/storage/app/public/'.$path;

                    // Update the meta data of the file after uploading the image
                    $meta_json = array(
                                                'filename' => $save_file_name,
                                                'filetype' => $extension,
                                                'filesize' => $image['size'],
                                                'image' => array(
                                                            'width'  => $width,
                                                            'height' => $height,
                                                            'mime'   => $mimeType
                                                            ),
                                                );
                    $mediaObj->update(['meta_json'=>$meta_json, 'height'=>$height, 'width'=>$width]);
                    return response()->json(['upload' => 'success', 'data' => $image]);

                }else{
                    return response()->json(['new_name' => $save_file_name, 'location' => $image_path, 'path' => '', 'upload' => 'error']);
                }                
            }
        }
        return view('backend/cms/add_media')->with(compact('type', 'all_media', 'total_pages'));
    }

    /**
     * Edit the media 
     * @param $type is the media type like the images, audios and videos
     */
    public function media_edit( Request $request ){
        
        parse_str($request->data, $post_data);
        $data = array();
        if ($request->isMethod('post')) {
        
            $media               = Media::find($post_data['data']['Media']['id']);
            $media->title        = $post_data['data']['Media']['title'];
            $media->alt          = $post_data['data']['Media']['alt'];
            $media->caption      = $post_data['data']['Media']['caption'];
            $media->description  = $post_data['data']['Media']['description'];
            $response = $media->update();

            if($response){
                $data['message'] = "Media has been updated successfully";
                $data['media']   = $media;
                $data['status']  = "success";
            }else{
                $data['message'] = "There is some problem while editing the media. Please try again.";
                $data['media']   = $media;
                $data['status']  = "error";
            }
        }else{
            $data['message'] = "There is some problem while editing the media. Please try again.";
            $data['status']  = "error";
        }
        return $data;
       
    }

    public function get_media_info( Request $request ){
        $get_media = Media::where('id', $request->id)->first();
        $get_media['path'] = url('/').'/storage/app/public/media/'.$get_media['id'].'/'.$get_media['type'].'/'.$get_media['filename'];
        if($get_media['type'] == 'image'){
            $get_media['src'] = url('/').'/storage/app/public/media/'.$get_media['id'].'/'.$get_media['type'].'/'.$get_media['filename'];
        }else if($get_media['type'] == 'audio'){
            $get_media['src'] = url('/').'/storage/app/public/media/default/audio.png';
        }else if($get_media['type'] == 'video'){
            $get_media['src'] = url('/').'/storage/app/public/media/default/video.png';
        }
        return response()->json(['status' => 'success', 'data' => $get_media]);
    }
    /**
     * Required for the WebDevEtc\BlogEtc package.
     * Enter your own logic (e.g. if ($this->id === 1) to
     *   enable this user to be able to add/edit blog posts
     *
     * @return bool - true = they can edit / manage blog posts,
     *        false = they have no access to the blog admin panel
     */
    public function canManageBlogEtcPosts()
    {
        // Enter the logic needed for your app.
        // Maybe you can just hardcode in a user id that you
        //   know is always an admin ID?

        if ( $this->id === 1 && $this->email === "your_admin_user@your_site.com" ){

           // return true so this user CAN edit/post/delete
           // blog posts (and post any HTML/JS)

           return true;
        }

        // otherwise return false, so they have no access
        // to the admin panel (but can still view posts)

        return false;
    }   


    /*   public function fields(Request $request)
    {
        return [
            Images::make('Main image', 'main') // second parameter is the media collection name
                ->thumbnail('thumb') // conversion used to display the image
                ->rules('required'), // validation rules
        ];
    }*/

    public function get_media(Request $request){
        $type = $request->input('type');
        $dimension = $request->input('dimension');
        if(!empty($type)){
            $outer_path = 'media';
            $images = [];
            $has_folder = Storage::disk('public')->has($outer_path);
            if($has_folder){
                $all_media = new Media();                
                $all_media = $all_media->where('type', $type);
                if(!empty($dimension)){
                    list($height, $width) = explode('x', $dimension);
                    $all_media = $all_media->where('height' , $height)->where('width' , $width);  
                }
                if(!empty($request->date)){
                    $check_date = Carbon::parse($request->date)->format('Y-m-d');
                    $all_media  = $all_media->whereRaw('DATE(created_at) = "'.$check_date.'"');
                }
                    $all_media = $all_media->get();                             
                $i = 0;
                foreach($all_media as $file){
                    $path = 'media/'.$file->id.'/'.$type;
                    $has_inside_folder = Storage::disk('public')->has($path);
                    if($has_inside_folder){
                        $created = strtotime($file->created_at);
                        $updated = strtotime($file->updated_at);
                        $images[$i]['id']           = $file->id;
                        if($type == 'image'){
                            $images[$i]['src']  = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        }else if($type == 'audio'){
                            $images[$i]['src']  = url('/').'/storage/app/public/media/'.$type.'/audio.png';
                        } else if($type == 'video'){
                            $images[$i]['src']  = url('/').'/storage/app/public/media/'.$type.'/video.png';
                        }
                        $images[$i]['url']  = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        $images[$i]['file'] = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        $images[$i]['name']          = $file->filename;
                        $images[$i]['media_id']            = $file->id;
                        $images[$i]['type']          = $file->type;
                        $images[$i]['title']         = $file->title;
                        $images[$i]['created_at']    = date('Y-m-d',$created);
                        $images[$i]['updated_at']    = date('Y-m-d',$updated);
                        $images[$i]['alt']           = $file->alt;
                        $images[$i]['caption']       = $file->caption;
                        $images[$i]['uploaded_by']   = $file->uploaded_by;
                        $images[$i]['uploaded_area'] = $file->uploaded_area;
                        $images[$i]['description']   = $file->description;
                        $images[$i]['meta_json']     = $file->meta_json;
                        if(!empty($file->meta_json)){                           
                            if(isset($file->meta_json->filesize)){
                                 $images[$i]['size'] = $file->meta_json->filesize;
                            }
                        }                                               
                        $i++;
                    }
                }
            }

            if(count($images) > 0){
                return response()->json(['status' => 'success', 'data' => $images]);
            }else{
                return response()->json(['status' => 'error']);
            }
        }
    }
        
    /**
     * Get more media
     */
    public function get_more_media(Request $request, $offset=0, $limit=15){
        //print_r(Input::post());
        //echo "<pre>";print_R($request->all());die('success');
        $currentPage = $request->page;
        $type = $request->input('type');

        // Get total pages
        $total_pages = $this->totalPages($type, $limit, $request->dimension);
        if(!empty($type)){
            $outer_path = 'media';
            $media = [];

            $has_folder = Storage::disk('public')->has($outer_path);
            if($has_folder){
               
                $all_media = new Media();                
                $all_media = $all_media->where('type', $type);
               
                if(isset($request->dimension)){
                    if(!empty($request->dimension)){
                        list($width, $height) = explode('x', $request->dimension);
                        //$all_media = $all_media->where('height', '<=', $height)->where('width', '<=' , $width);  
                        $all_media = $all_media->where('height', '=', $height)->where('width', '=' , $width);
                    }
                }
                
                if(isset($request->date)){
                    if(!empty($request->date)){
                        $check_date = Carbon::parse($request->date)->format('Y-m-d');
                        $all_media  = $all_media->whereRaw('DATE(created_at) = "'.$check_date.'"');
                    }
                }
                //Search by title of media
                if(isset($request->title)){
                    if(!empty($request->title)){
                        $all_media  = $all_media->where('title', 'like', '%'.$request->title.'%');
                    }
                }

                if ($currentPage == 1) {
                    $offset = 0;
                }
                else {
                    $offset = ($currentPage - 1) * $limit;
                }
                $all_media = $all_media->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();                             
                $i = 0;
                //echo "<pre>";print_R($all_media);die;
                foreach($all_media as $file){
                    $path = 'media/'.$file->id.'/'.$type;
                    $has_inside_folder = Storage::disk('public')->has($path);
                    if($has_inside_folder){
                        $created = strtotime($file->created_at);
                        $updated = strtotime($file->updated_at);
                        $media[$i]['id']            = $file->id;
                        if($type == 'image'){
                            $media[$i]['src']       = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                            $media[$i]['class']     = "";
                        }else if($type == 'audio'){
                            $media[$i]['src']       = url('/').'/storage/app/public/media/default/audio.png';
                            $media[$i]['class']     = "set_image";
                        } else if($type == 'video'){
                            $media[$i]['src']       = url('/').'/storage/app/public/media/default/video.png';
                            $media[$i]['class']     = "set_image";
                        }
                        $media[$i]['url']           = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        $media[$i]['file']          = url('/').'/storage/app/public/'.$path.'/'.$file->filename;
                        $media[$i]['filename']      = $file->filename;
                        $media[$i]['name']          = $file->title;
                        $media[$i]['media_id']      = $file->id;
                        $media[$i]['type']          = $file->type;
                        $media[$i]['title']         = $file->title;
                        $media[$i]['created_at']    = date('Y-m-d',$created);
                        $media[$i]['updated_at']    = date('Y-m-d',$updated);
                        $media[$i]['alt']           = $file->alt;
                        $media[$i]['caption']       = $file->caption;
                        $media[$i]['uploaded_by']   = $file->uploaded_by;
                        $media[$i]['uploaded_area'] = $file->uploaded_area;
                        $media[$i]['description']   = $file->description;
                        $media[$i]['meta_json']     = $file->meta_json;
                        if(!empty($file->meta_json)){                           
                            if(isset($file->meta_json->filesize)){
                                $media[$i]['size'] = $file->meta_json->filesize;
                            }
                        }                                               
                        $i++;
                    }
                }
            }
            $inputParam = implode(' = ',Input::post());   
            if($request->page_content == 'popup'){               
                return view('backend/cms/add_more_media_popup')->with(compact('type', 'media', 'total_pages', 'inputParam'));
            }else{
                return view('backend/cms/add_more_media')->with(compact('type', 'media'));
            }
        }        
    }

    /**
     *  Delete the media files
     * 
     */
    public function delete_media(Request $request){
       
        if($request->id != ''){
            try{
                //Delete from database 
                $media = Media::where('id', $request->id)->first();
                $media->medialinking()->delete();
                $media->delete();
                //Delete Directory
                $base_name = 'media/'.$request->id;
                $delete    = Storage::disk('public')->deleteDirectory($base_name);
                if($delete){
                    return ['status' => 'success'];
                }else{
                    return ['status' => 'error'];
                }
                
            }catch(\Exception $e){
                return ['status' => 'error'];
            }           
        }
    }

     /**
     * @desc Delete the media link
     * 
     */
    public function delete_media_link(Request $request){
        if($request->id != ''){
            //Delete from database 
            $delete = MediaLinking::where('id', $request->id)->delete();
            if($delete){
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'error']);
            }
        }
    }

    /**
     * @desc Add the media link
     * 
     */
    public function add_media_link(Request $request){
        $link = new MediaLinking();    
        $link->media_id =$request->media_id;
        $link->main_module =$request->main_module;
        $link->sub_module =$request->sub_module;
        $link->module_id =$request->module_id;
        $link->created_by =Auth::user()->id;
        if($link->save()){
            return ['status' => 'success','id'=>$link->id];
        }else{
            return ['status' => 'error'];
        }
    }

     /**
     * @desc Edit the media link
     * 
     */
    public function edit_media_link(Request $request){
        
        $post_data = $request->all();
        $link      = MediaLinking::find($post_data['id']);
        $link->media_id   = $post_data['media_id'];
        $link->created_by = Auth::user()->id;
        $update = $link->update();
        if($update){
            return ['status' => 'success','id'=>$link->id];
        }else{
            return ['status' => 'error'];
        }
    }
}
