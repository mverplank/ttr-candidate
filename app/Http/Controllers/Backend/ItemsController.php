<?php

/**
 * @author : Contriverz
 * @since  : 14-01-2019
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Membership;
use App\Models\Backend\InventoryItem;
use App\Models\Backend\InventoryMonth;
use App\Models\Backend\InventoryCategory;
use App\Models\Backend\MediaLinking;
use App\Models\Backend\InventoryMonthItem;
use Auth;
use Carbon;
use Session;

class ItemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $inventory_items = new InventoryItem();
        $is_online = 2;
        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();          
            if ($request->has('is_online')) {
                $is_online = $request->input('is_online');
            }
        }
        return view('backend/bonus_items/items')->with(compact('is_online'));
    }

    public function ajax_index(Request $request)
    {
        $inventory_items = new InventoryItem();
        $filter_inventory_items = array();
        $draw      = 1;
        $is_online = 2;
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }  

        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();
            $column = "";
            $value  = 2;
            if($request->exists('column')){
                $column = $request->input('column');
            } if($request->exists('column')){
                $value  = $request->input('value');
            }  
        
            if($column == "is_online"){
                $is_online = $value;                
                $filter_inventory_items = $inventory_items->findItemsOfCurrentUser($current_user_id, $is_online, 'name', $start, $length, $order, $dir, null, $search_value);
                $count_inventory_items = $inventory_items->countItemsOfCurrentUser($current_user_id, $is_online, 'name', $order, $dir, null, $search_value);
            }else if($column == "is_accepted"){
                $filter_inventory_items = $inventory_items->findItemsOfPartners($value, 2, 'name');
            }else{
               
                $filter_inventory_items = $inventory_items->findItemsOfCurrentUser($current_user_id, $value, 'name', $start, $length, $order, $dir, null, $search_value);
                $count_inventory_items = $inventory_items->countItemsOfCurrentUser($current_user_id, $value, 'name', $order, $dir, null, $search_value);
            }

            $data = array();
            $i    = 0;   
            foreach($filter_inventory_items as $item){
                $data[$i][]  = html_entity_decode(showHostProfileImage($item->id, 'InventoryItem', 'image', '001.jpg'));
                $data[$i][]  = $item->name;
                $data[$i][]  = $item->submitted_by;
                $data[$i][]  = Carbon::parse($item->created_at)->format('dS M Y');
                $data[$i][]  = '<a href="'.url('admin/inventory/items/edit/'.$item->id ).'"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)" onclick="deleteInventoryItem('.$item->id.', this)"><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 

            $output = array(
                        'draw' => $draw,
                        'recordsTotal' => $count_inventory_items,
                        'recordsFiltered' => $count_inventory_items,
                        'data' => $data
                    );

            echo json_encode($output);
            exit();
        } 
    }

    /**
     * Show the view for the monthly bonus items of memberships.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function month_index(Request $request, $id){

        $currMonthFirstDay = date("Y-m-01");
        $membership_name   = "";
        $membership_obj    = Membership::find($id);
        //echo "<pre>";print_R($membership_obj);die('success');
        
        //Get name of the membership
        if($membership_obj){
            $membership_name = $membership_obj->name;
        }

        for ($month = 0; $month < 12; $month++) {
                        
            $dayDate = date("Y-m-01", strtotime('+'.$month.' months', time()));           
            $isMonth = InventoryMonth::where('date', $dayDate)->count();

            if (!$isMonth) {
                $inventory_obj = new InventoryMonth(array('date' => $dayDate));
                $inventory_obj->save();
            }
        }
       

        //echo $currMonthFirstDay;die('success');
        return view('backend/bonus_items/monthly_items')->with(compact('membership_name', 'id'));
    }


    public function ajax_month_index(Request $request)
    {
        $currMonthFirstDay = date("Y-m-01");
        $id = $request->id;
        $get_monthly_items = new InventoryMonth();
        
        $draw         = 1;
        $start        = $request->input('start');
        $length       = $request->input('length');
        $draw         = $request->input('draw');
        $order        = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }  
       
        $get_items = $get_monthly_items->getMonthlyItems($currMonthFirstDay, $id, $start, $length, $order, $dir, null, $search_value);       
        $count_items = $get_monthly_items->countMonthlyItems($currMonthFirstDay, $id, $order, $dir, null, $search_value);   

        $data = array();
        $i    = 0;   
        foreach($get_items as $item){
            $data[$i][]  = Carbon::parse($item->date)->format('m/Y');
            $data[$i][]  = $item->InventoryMonth__items;
            $data[$i][]  = '<a href="'.url('admin/inventory/months/edit/'.$item->id ).'/'.$id.'"><i class="glyphicon glyphicon-pencil"></i></a>';
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_items,
                    'recordsFiltered' => $count_items,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
         
    }

    /**
     * Show the application dashboard for displaying the bonus items of partners.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function partner_index(Request $request)
    {
        $inventory_items = new InventoryItem();
        $is_accepted = 0;
        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();
            $is_online = 2;
            /*  if ($request->has('is_online')) {
                $is_online = $request->input('is_online');
            }*/
            if ($request->has('is_accepted')) {
                $is_accepted = $request->input('is_accepted');
            }
            //$all_items = $inventory_items->findItemsOfPartners($is_accepted, $is_online, 'name');
            return view('backend/bonus_items/partners_items')->with(compact('is_accepted'));
        }        
    }

    /**
     * Show the application dashboard for displaying the bonus items of partners.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function partner_ajax_index(Request $request)
    {
        $inventory_items = new InventoryItem();
        $is_accepted     = 0;
        $current_user_id = 1;
        $is_online       = 2;
        $draw    = 1;
        $type    = $request->input('type');
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr   = $request->post("search");
        $search_value = $search_arr['value'];
        $search_regex = $search_arr['regex'];
        $columns      = $request->post("columns");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 

        if ($request->has('is_accepted')) {
            $is_accepted = $request->input('is_accepted');
        }
        if (auth()->check())
        {
            if(\Config::get('constants.ADMIN') == getCurrentUserType()){
                $current_user_id = 0;
            }else{
                $current_user_id = auth()->user()->getId();
            }            
        }
        //echo $is_online;die;
        $parnter_items = $inventory_items->findItemsOfPartners($current_user_id, $is_accepted, $is_online, 'name', $start, $length, $order, $dir, null, $search_value);
        
        $count_partner_items = $inventory_items->countItemsOfPartners($current_user_id, $is_accepted, $is_online, 'name', $order, $dir, null, $search_value);
     
        $data = array();
        $i    = 0;   
        if(!empty($parnter_items)){
            foreach($parnter_items as $item){
                $data[$i][]  = html_entity_decode(showHostProfileImage($item->id, 'InventoryItem', 'image', '001.jpg'));
                $data[$i][]  = $item->name;
                $data[$i][]  = $item->submitted_by;
                $data[$i][]  = Carbon::parse($item->created_at)->format('dS M Y');
                if(\Config::get('constants.ADMIN') == getCurrentUserType()){
                    $data[$i][]  = '<a title="Edit" href="'.url('admin/inventory/items/accept/'.$item->id ).'"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)" title="Delete" onclick="deleteInventoryItem('.$item->id.', this, &quot;partner&quot;)"><i class="glyphicon glyphicon-trash"></i></a>';
                }else if(\Config::get('constants.PARTNER') == getCurrentUserType()){
                    if($item->is_accepted == 0){
                        $data[$i][]  = '<a title="Edit" href="'.url('partner/inventory/items/edit/'.$item->id ).'"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)" title="Delete" onclick="deleteInventoryItem('.$item->id.', this, &quot;partner&quot;)"><i class="glyphicon glyphicon-trash"></i></a>';
                    }else{
                        $data[$i][] = '';
                    }
                }        
                $i++;
            } 
        }
        

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_partner_items,
                    'recordsFiltered' => $count_partner_items,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }


    // Add Inventory Items
    public function add_item(Request $request)
    {
        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'InventoryItem');
        //$token = hash_hmac('sha256', Str::random(40), $user);
        $current_user_id = 0; 
        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();
        }
        //Fetch all bonus items categories
        $get_categories = new InventoryCategory();
        $inventory_categories = $get_categories->allInventoryCategoriesForSelect(); 
        
        if ($request->isMethod('post')) {
           
            $data = $request->input('data');
            $var  = "";
            $message  = "";
            $inventory_item = new InventoryItem();
            $response = $inventory_item->add($data);
            if($response){
                $message = "New Bonus Item successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the bonus item. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            if(\Config::get('constants.ADMIN') == getCurrentUserType()){
                return redirect('admin/inventory/items');
            }else if(\Config::get('constants.PARTNER') == getCurrentUserType()) {
                return redirect('partner/inventory/items/my');
            }
        }else{
            return view('backend/bonus_items/item_add')->with(compact('current_user_id', 'inventory_categories','media'));
        }
    }
       
    // Edit Inventory Items
    public function edit_item($id, Request $request)
    {
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'InventoryItem');
        
        $current_user_id = 0; 
        if (auth()->check())
        {
            $current_user_id = auth()->user()->getId();
        }
        //Fetch all bonus items categories
        $get_categories = new InventoryCategory();
        $inventory_categories = $get_categories->allInventoryCategoriesForSelect(); 

        $inventory_item = InventoryItem::with('inventory_item_urls')->where('id',$id)->first();
        // echo "<pre>";print_R($inventory_item);die;
        /* if($nventory_item->channels->count() > 0){
            foreach($sponsor->channels as $chn){
                $assigned_channels[] = $chn->id;
            }
        }*/

        if ($request->isMethod('post')) {

            $update_data = $request->data;
            $inventory_item = new InventoryItem();
            $update = $inventory_item->edit($id, $update_data);
            if($update){
                $message = "The Bonus Item details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the bonus item. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            if(\Config::get('constants.ADMIN') == getCurrentUserType()){
                return redirect('admin/inventory/items/edit/'.$id);
            }else if(\Config::get('constants.PARTNER') == getCurrentUserType()) {
                return redirect('partner/inventory/items/my');
            }
        }else{
            return view('backend/bonus_items/item_edit', compact('inventory_item', 'inventory_categories', 'current_user_id','media'));
        }
    }

    // Delete Inventory Item
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $item_id = $post_data['id'];
                $item = new InventoryItem();
                $delete = $item->deleteItem($item_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }


    /**
     * Show the view for the monthly bonus items of Edit.
     * @param $mid is the membership id
     * @param $id is the inventory month id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function month_edit(Request $request, $id, $mid){

        $month = InventoryMonth::where('id', '=', $id)->first();

        $check_ids = InventoryMonthItem::select('id', 'inventory_items_id', 'week')->where('inventory_months_id', '=', $id)->where('memberships_id', $mid)->get();
        $get_inventory_relation_ids = $get_inventory_week = array();

        if($check_ids->count() > 0){
            foreach($check_ids as $check_id){
                $get_inventory_relation_ids[$check_id->inventory_items_id] = $check_id->id;
                $get_inventory_week[$check_id->inventory_items_id] = $check_id->week;
            }
        }
    
        $inventory_item  = new InventoryItem;
        $membership = Membership::where('id', '=',$mid)->with('membership_periods')->first();

        $weekly  = array('0' =>'all weeks','1' =>'1st week','2' =>'2nd week','3' =>'3rd week','4'=>'4th week' );
        if (auth()->check()){
            $current_user_id = auth()->user()->getId();
        }
       
        $all_items = $inventory_item->findItemsOfCurrentUser($current_user_id, 2, "category_name", 0, 10, null, null, null, null, null, false);
        if ($request->isMethod('post')) {
            $data = $request->data;  
            foreach ($data['InventoryMonthItem'] as $inventory) {
                if($inventory['id'] > 0){

                    $item_relation_id = $inventory['id'];

                    // Update the week if there exists an inventory item
                    if(isset($inventory['inventory_items_id'])){
                        $check = InventoryMonthItem::find($item_relation_id);
                        if($check){
                            $check->update(['week'=>$inventory['week']]);
                        }
                    }else{

                        //Delete the entry if there is not any inventory item assigned
                        $delete = InventoryMonthItem::where('id', $item_relation_id)->delete();
                    }

                }else{

                    if(isset($inventory['inventory_items_id'])){
                        InventoryMonthItem::insert(['inventory_months_id'=>$id,'memberships_id'=>$mid,'inventory_items_id'=>$inventory['inventory_items_id'],'week'=>$inventory['week']]);
                    }
                }
            }
            Session::flash('success' , 'The Monthly Bonus Items has been saved.');
            return redirect('/admin/inventory/months/index/'.$mid);
        }else{
            return view('backend/bonus_items/edit_monthly_items', compact('all_items','membership','weekly','month', 'get_inventory_relation_ids', 'get_inventory_week'));
            }
       
    }

}
