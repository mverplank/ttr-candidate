<?php

namespace App\Http\Controllers\Backend;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Backend\Channel;
use App\Models\Backend\User;
use Session;
use Auth;

class UploadsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function upload(Request $request)
    {
        // echo "<pre>";print_R($request->all());die('hhghghgh');
        $userid = Auth::user()->id;      
        if($request->input('crop') == 1){
         
            $pathinfo   = pathinfo($request->input('old_image'));
            $pathinfo   = pathinfo('001.jpg');
            $image_name =  $pathinfo['filename'].'.'.$pathinfo['extension'];
            
            //get the base-64 from data
            $base64_str = substr($request->input('image_data'), strpos($request->input('image_data'), ",")+1);

            //decode base64 string
            $image = base64_decode($base64_str);
            if($request->form_type == 'edit'){                
                $image_path = $request->module.'/'.$request->item_id.'/'.$request->image_for;
            }else{
                if($request->module == "HostContent"){
                    //echo "<pre>";print_R($request->all());die('hhghghgh');
                    $arr = explode('image_', $request->image_for);
                    $get_offset = $arr[1];
                    $image_path = 'tmp/uploads/'.$userid.'/'.$request->module.'/'.$get_offset.'/image';
                                // /echo "<pre>";print_R($request->all());die('hhghghgh');
                }else{
                    $image_path = 'tmp/uploads/'.$userid.'/'.$request->module.'/'.$request->image_for;
                }                
            }
            //Delete old image           
            Storage::disk('public')->delete($image_path.'/'.$image_name);
            // Add new cropped image
            $path = Storage::disk('public')->put( $image_path.'/'.$image_name, $image); 
            //$storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix(); 
            //$response = array( 'status' => 'success' );
            //return response()->json($response);
            if($path === true){
                $image_data = array();
                $image_data['path']     = $path;
                $image_data['size']     = Storage::disk('public')->size($image_path."/".$image_name);
                $image_data['name']     = $image_name;
                $image_data['accepted'] = true;
                $image_data['case']     = 'edit';
                //  $image_data['url']      = url('/').'/storage/app/public/'.$path;
                $image_data['url']      = url('/').'/storage/app/public/'.$image_path.'/'.$image_name;
                // echo "<pre>";print_R($image_data);die('success');
                if($request->form_type == 'edit'){
                    return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'new_name' => $image_name, 'location' => $image_path, 'path' => $path, 'upload' => 'success', 'case' => 'edit', 'data'=>$image_data]);
                }else{
                    return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'new_name' => $image_name, 'location' => $image_path, 'path' => $path, 'upload' => 'success', 'case' => 'add', 'data'=>$image_data]);
                }
            }
           
        }else{
           
            if ($request->hasFile('image')) {
                $file = $request->file('image')[0];
                if ($file->isValid()) {
                    //echo "<pre>";print_R($request->all());die('success');
                    if (false !== mb_strpos($file->getMimeType(), "image")) {
                        $filename   = $file->getClientOriginalName();
                        //$extension  = \Illuminate\Support\Facades\File::extension($filename);
                        $extension  = "jpg";
                        if($request->form_type == 'edit'){
                            /*if($request->module == "HostContent"){
                                $image_path = $request->module.'/'.$request->item_id.'/'.$request->image_for;
                            }else{*/
                                $image_path = $request->module.'/'.$request->item_id.'/'.$request->image_for;
                            //}
                            
                        }else{
                            $image_path = 'tmp/uploads/'.$userid.'/'.$request->module.'/'.$request->image_for;
                        }
                       
                        $upfiles    = Storage::disk('public')->files($image_path);
                        
                        $exists     = [];
                        $save_file_name = '';

                        if(count($upfiles) > 0){
                            if($request->module == "Partner"){
                                return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'new_name' => "",  'location' => $image_path, 'path' => '', 'upload' => 'error', 'case' => 'add']);

                            }else{
                                foreach($upfiles as $upfile){
                                    $imageName = $this->get_basename($upfile);
                                    $exists[]  = explode('.', $imageName)[0];
                                }
                            }
                            
                        }
                        $a1=["001","002","003"];
                        $diff=array_diff($a1,$exists);

                        if(count($exists) > 0){
                            if(count($diff) > 0){
                                $save_file_name = array_shift($diff).'.'.$extension;
                            }
                        }else{
                            $save_file_name = '001.'.$extension;
                        }

                        if($save_file_name != ''){
                            //echo "<pre>";print_R($file);die('success');
                            $path = Storage::disk('public')->putFileAs($image_path, $file, $save_file_name);

                            $image['path']     = $path;
                            $image['size']     = Storage::disk('public')->size($path);
                            if($request->form_type == 'edit'){
                                $image['name']     = explode('/', $path)[3];
                            }else{
                                $image['name']     = explode('/', $path)[5];
                            }
                            //$images[$i]['public']   = public_path();
                            $image['accepted'] = true;
                            $image['case']     = 'edit';
                            //$images[$i]['type'] = \File::mimeType(public_path($file));
                            //die('sucess');
                            $image['url']      = url('/').'/storage/app/public/'.$path;//Storage::url($file);
                            return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'new_name' => $save_file_name, 'location' => $image_path, 'path' => $path, 'upload' => 'success', 'case' => 'add', 'data' => $image]);
                        }else{
                            return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'new_name' => $save_file_name, 'location' => $image_path, 'path' => '', 'upload' => 'error', 'case' => 'add']);
                        }
                    } 
                }
            }
            else if($request->has('user_files')){

                $params = $request->all();
                //echo "<pre>";print_R($params);die;
                // echo "<pre>";print_R($request->all());die('success');
                $_fileUploadTimeLimit = 10; // default 10 [ min ]
                // Maximum file size allowed to upload.
                // Limited to 32 due client side mtsoft.ui.upload.js > FileReader limits for Chomre/IE
                // (max 32MB?). If larger file - out of memory error
                $_fileSizeMax = 32; // [MB]
                // Maximum number of files available to upload by single post request
                $_filesCountMax = 5;
                // set upload time limit ( given in [m] )
                set_time_limit($_fileUploadTimeLimit * 60);

                if (ini_get('max_execution_time') < $_fileUploadTimeLimit) {  // to be sure try to set maximum script execution time also here
                    ini_set('max_execution_time', $_fileUploadTimeLimit);
                }
                if (ini_get('upload_max_filesize') < $_fileSizeMax) { // maximum single file size availabel to upload
                    ini_set('upload_max_filesize', $_fileSizeMax . 'M');
                }
                if (ini_get('post_max_size') < $_fileSizeMax) {   // maximum POST data request size (need while sending data via ajax POST request)
                    ini_set('post_max_size', $_fileSizeMax . 'M');
                }
                if (ini_get('max_file_uploads') < $_filesCountMax) { // maximum number of files available to upload by single request
                    ini_set('max_file_uploads', $_filesCountMax);
                }
                if (ini_get('memory_limit') < $_fileSizeMax) { // to process uploaded file we need enough memory
                    ini_set('memory_limit', $_fileSizeMax . 'M');
                }
             
                $minFileSize = isset($params['fileSizeMin']) ? (int) ($params['fileSizeMin'] * 1048576) : 0;
                $maxFileSize = isset($params['file_maxsize']) && !empty($params['file_maxsize']) ? (int) ($params['file_maxsize'] * 1048576) : 1024;
                $file = $request->file('files')[0];
                if ($file->isValid()) {
                   
                        $filename   = $file->getClientOriginalName();
                        // $filesize   = $file->getClientSize();
                        $extension  = \Illuminate\Support\Facades\File::extension($filename);
                       
                        if($request->form_type == 'add'){
                            $file_path = 'tmp/uploads/'.$userid.'/'.$request->module.'/'.$request->image_for;
                        }
                        if($request->form_type == 'edit'){
                            $file_path  = $request->input('dir');
                        }
                        $upfiles    = Storage::disk('public')->files($file_path);
                        // Get total size of the images in the folder
                        /* $file_size = 0;
                        if(Storage::exists($file_path)){
                            $files = \Illuminate\Support\Facades\File::allFiles(storage_path('app/public'.$file_path));
                            
                            foreach( $files as $file)
                            {
                                $file_size += $file->getSize();
                            }
                            $file_size = number_format($file_size / 1048576,2);
                        }*/
                       
                                             
                        $exists     = [];
                        $save_file_name = '';
                        // $files_with_size[$key]['size'] = Storage::disk('public')->size($file);
                        //echo 
                        if(count($upfiles) < 5){
                            $path = Storage::disk('public')->putFileAs($file_path, $file, $filename);
                            $image['path']     = $path;
                            $image['size']     = Storage::disk('public')->size($path);
                           
                            /* if($request->form_type == 'edit'){
                                $image['name'] = explode('/', $path)[3];
                            }else{
                                $image['name'] = explode('/', $path)[5];
                            }*/
                            //$images[$i]['public']   = public_path();
                            $image['accepted'] = true;
                            $image['case']     = 'edit';
                            //$images[$i]['type'] = \File::mimeType(public_path($file));
                            $image['url']      = url('/').'/storage/app/public/'.$path;
                            return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'new_name' => $save_file_name, 'location' => $file_path, 'path' => $path, 'upload' => 'success']);
                        }else{
                            return response()->json(['module' => $request->module, 'image_type' => $request->image_for, 'location' => $file_path, 'upload' => 'error', 'message'=>'Only 5 files can be uploaded']);
                        }               
                    //}
                }
            }
        }       
    }

    public function sort_files(Request $request){
        //echo"<pre>";
        //print_r($request->all());
        $userid = Auth::user()->id;
        if($request->form_type == 'edit'){
            $path = $request->module.'/'.$request->id.'/'.$request->folder;
        }else{
            $path = 'tmp/uploads/'.$userid.'/'.$request->module.'/'.$request->folder;
        }
        
        //check if at tmp location folder and file exist
        $path_exists = Storage::disk('public')->has($path);
        if($path_exists){
            $files = Storage::disk('public')->files($path);
            if($files){
                foreach($files as $file){
                    $file_name = explode('/',$file);
                    $ext_array = '';
                    $image_name = '';
                    if($request->form_type == 'edit'){
                        $ext_array = explode('.', $file_name[3]);
                        $image_name = $file_name[3];
                    }else{
                        $ext_array = explode('.', $file_name[5]);
                        $image_name = $file_name[5];
                    }
                    $key = array_search ($image_name, $request->new_order);
                    $n_name = $ext_array[0].'-'.$key.'-'.'.'.$ext_array[1];
                    Storage::disk('public')->move($file, $path.'/'.$n_name);
                    
                }
            }

            $files = Storage::disk('public')->files($path);
            if($files){
                foreach($files as $file){
                    $file_name = explode('/',$file);
                    $ext_array = '';
                    $image_name = '';
                    if($request->form_type == 'edit'){
                        $ext_array  = explode('.', $file_name[3]);
                        $pos        = explode('-', $file_name[3]);
                        $image_name = $file_name[3];
                    }else{
                        $ext_array  = explode('.', $file_name[5]);
                        $pos        = explode('-', $file_name[5]);
                        $image_name = $file_name[5];
                    }
                    if($pos[1] == 0){
                        $new_file = '001.'.$ext_array[1];
                    }else if($pos[1] == 1){
                        $new_file = '002.'.$ext_array[1];
                    }else{
                        $new_file = '003.'.$ext_array[1];
                    }
                    Storage::disk('public')->move($file, $path.'/'.$new_file);
                }
            }
            return response()->json(['status' => 'success']);
        }
    }


    /*
    * Move Image From Tmp Location to permananet folder
    *
    */

    public function procesImage($object, $module){
        $folders = ['player', 'zapbox', 'zapboxsmall', 'widget', 'cover', 'photo'];
        $userid = Auth::user()->id;
        foreach($folders as $folder){
            $tmp_path = 'tmp/uploads/'.$userid.'/'.$module.'/'.$folder;
            //check if at tmp location folder and file exist
            $tmp_exists = Storage::disk('public')->has($tmp_path);
            if($tmp_exists){
                //check if folder on new path exist
                $new_path = $module.'/'.$object->id.'/'.$folder;
                $new_exists = Storage::disk('public')->has($new_path);
                //if new folder not exist then create new folder
                if(!$new_exists){
                    Storage::disk('public')->makeDirectory($new_path);
                }
                $files = Storage::disk('public')->files($tmp_path);
                if($files){
                    foreach($files as $file){
                        $file_name = explode($folder, $file);
                        Storage::disk('public')->move($file, $new_path.$file_name[1]);
                    }
                }
            }
        }    
       
        return 'success';
    }

    public function get_basename($filename){
        return preg_replace('/^.+[\\\\\\/]/', '', $filename);
    }

    public function delete_files(Request $request){
      
        if($request->file_path != ''){
            $base_name = basename($request->file_path);
            $delete    = Storage::disk('public')->delete($request->file_path);
            if($delete){
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'error']);
            }
        }
    }

    public function get_files(Request $request){
        $userid = Auth::user()->id;
        if($request->form_type == 'edit'){
            $path = $request->module.'/'.$request->id.'/'.$request->folder;
        }else{
            $path = 'tmp/uploads/'.$userid.'/'.$request->module.'/'.$request->folder;
        }
        $images = [];
      
        $has_folder = Storage::disk('public')->has($path);
        if($has_folder){
            $files = Storage::disk('public')->files($path);
           
            if($files){
                $tmpdata = [];
                $i = 0;
                foreach($files as $file){
                    $images[$i]['path']     = $file;
                    $images[$i]['file']     = storage_path() .'/'.$file;
                    $images[$i]['size']     = Storage::disk('public')->size($file);
                    if($request->form_type == 'edit'){
                        $images[$i]['name'] = explode('/', $file)[3];
                    }else{
                        $images[$i]['name'] = explode('/', $file)[5];
                    }
                    $images[$i]['accepted'] = true;
                    $images[$i]['case']     = 'edit';
                    $images[$i]['type']     = Storage::disk('public')->mimeType($file);
                    $images[$i]['url']      = url('/').'/storage/app/public/'.$file;
                    $images[$i]['cropped']  = 1;
                    $i++;
                }
            }
        }
        if(count($images) > 0){
            return response()->json(['status' => 'success', 'data' => $images]);
        }else{
            return response()->json(['status' => 'error']);
        }
    }
}
