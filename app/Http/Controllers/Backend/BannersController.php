<?php
/**
 * @author : Contriverz
 * @since  : 14-01-2019
 */
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\Banner;
use App\Models\Backend\Channel;
use App\Models\Backend\BannersHasChannel;
use App\Models\Backend\Category;
use App\Models\Backend\Sponsor;
use App\Models\Backend\MediaLinking;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Session;
use Auth;
class BannersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $banners = new Banner();
        $is_online = 2;
        if ($request->has('is_online')) {
            $is_online = $request->input('is_online');
            //$all_banners = $banners->getAllBanners($is_online);
        }else if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
            if(!empty($channel_id)){
                $banner_has_channel = new BannersHasChannel();
                //$all_banners = $banner_has_channel->fetchBannersAccToChannel($channel_id);
                $mode = 'relation';
            }else{
                //$all_banners = $banners->getAllBanners($is_online);
                $mode = 'simple';
            }
        }else{
            //$all_banners = $banners->getAllBanners($is_online);
            $mode = 'simple';
        }

        $selected_channel  = '';
        $selected_category = '';
        if($request->has("channel_id")){
            $selected_channel  = $request->input("channel_id");
        }if($request->has("cat_id")){
            $selected_category = $request->input("cat_id");
        }

        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        $all_channels->put('', 'Any Channel');

        //Fetch all Category
        $category = new Category();
        $all_category = $category->getAllCategory('banner'); 
        $all_category->put('', 'All Category');
        
        return view('backend/advertising/banners')->with(compact('is_online', 'all_channels', 'all_category', 'selected_channel', 'selected_category'));
    }

    public function ajax_index(Request $request)
    {
        $banners       = new Banner();
        $draw          = 1;
        $is_online     = 2;
        $channel_id    = 0;
        $category_id   = 0;
        $channel_id    = $request->input('channel_id');
        $category_id   = $request->input('category_id');
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        if($request->exists('is_online')){
            $is_online  = $request->input('is_online');
        }
       
        $mode = "simple";
        $filter_banners = "";
        if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
        }
             
        $all_banners = $banners->getAllBanners($category_id, $is_online, $channel_id, $start, $length, $order, $dir, null, $search_value);
        $count_banners = $banners->countAllBanners($category_id, $is_online, $channel_id, $order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   

        if(!empty($all_banners)){
            foreach($all_banners as $banner){

                $data[$i][]  = $banner->title;
                $data[$i][]  = '<div style="width:200px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><a href="'.$banner->url.'" target="_blank">'.$banner->url.'</a></div>';
                //$data[$i][]  = $banner->name;
                $data[$i][]  = $banner->clicks;
                 if($banner->is_online == 1) {
                   $data[$i][]  = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
                }                                        
                else{
                    $data[$i][]  = ' <button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Inactive</button> ';
                }

                if(!empty($banner->view_period_from)){
                    $data[$i][] = Carbon::parse($banner->view_period_from)->format('m/d/Y');
                } else{
                    $data[$i][] = '';
                }

                if(!empty($banner->view_period_to)){
                    $data[$i][] = Carbon::parse($banner->view_period_to)->format('m/d/Y');
                } else{
                    $data[$i][] = '';
                }
               
                $data[$i][]  = '<a href="'.url('admin/advertising/banners/edit/'.$banner->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" title="Delete" onclick="deleteBanner('.$banner->id.', this)" ><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_banners,
                    'recordsFiltered' => $count_banners,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    //  Add Banner view
    public function create(){

        // $sponsor = new Sponsor();
        // $all_sponsor = $sponsor->getAllSponsor();

        $category = new Category();
        $all_category = $category->getAllCategory('banner');      
        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        if($all_channels->count() > 0){
            $all_channels = $all_channels->toArray();
        }
        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Banner');
        return view('backend/advertising/add_banner')->with(compact('all_category','all_channels','media'));
    }

    // Add Banner
    public function add_banner(Request $request) {
        // Fetch all Sponsor
        // $sponsor = new Sponsor();
        // $all_sponsor = $sponsor->getAllSponsor();
        //Fetch all Category
        $category = new Category();
        $all_category = $category->getAllCategory('banner');      
        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        if($all_channels->count() > 0){
           $all_channels = $all_channels->toArray();
        }
     
        if ($request->isMethod('post')) {
         
            $data = $request->input('data');
            $data['User']['created_ip'] = $request->ip();
            $var  = "";
            $message    = "";
            $banner     = new Banner();
            $response   = $banner->add($data);
            if($response){
               $message = "New banner has been created successfully!";
               $var     = "success";
            }else{
               $message = "There is some problem while creating the banner. Please try again.";
               $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/advertising/banners');
        }else{
           return view('admin/advertising/banners/add', compact('all_channels', 'all_category'));
        }
    }
    // Edit Banners
    public function edit_banner($id, Request $request){

        $banners_has_category  = array();
        $banner_has_channels = array();
        //get media
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Banner');
        //Fetch all Category
        $category = new Category();
        $all_category = $category->getAllCategory('banner');

        //Fetch all Channels
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        if($all_channels->count() > 0){
           $all_channels = $all_channels->toArray();
        }

        // Get banner and its relational data
        $banner = Banner::with('banners_has_categories')->with('channels')->where('id',$id)->first();
        if($banner->banners_has_categories->count() > 0){
            foreach($banner->banners_has_categories as $banner_cat){
               $banners_has_category[] = $banner_cat->categories_id;
            }
        }
        if($banner->channels->count() > 0){
            foreach($banner->channels as $channel){
               $banner_has_channels[] = $channel->id;
            }
        }  
        //die('success');   
        if ($request->isMethod('put') || $request->isMethod('post')) {
           // echo "<pre>";print_R($request->data);die;
            $update_data = $request->data;
            $bannerObj   = new Banner();
            $update      = $bannerObj->edit($id, $update_data);
            if($update){
               $message = "The Banner details has been updated.";
               $var     = "success";
            }else{
               $message = "There is some problem while updating the banner. Please try again.";
               $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/advertising/banners');

            //return view('backend/advertising/edit_banner', compact('banner', 'all_category','all_channels', 'banners_has_category', 'banner_has_channels'));
        }else{
           return view('backend/advertising/edit_banner', compact('banner', 'all_category','all_channels', 'banners_has_category', 'banner_has_channels','media'));
        }
    }

    // Delete Banners
    public function delete(Request $request){
        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $cate_id = $post_data['id'];
                
                $banner = new Banner();
                $delete = $banner->deleteBanner($cate_id);
                if($delete){
                   return array('status' => 'success');
                }else{
                   return array('status' => 'error');
                }
            }else{
               return array('status' => 'error');
            }
        } 
    }
    /**
     * @decription Add Banner Categories
     * @param $request data object
     */
    public function add_banner_categories(Request $request){
        /* Validator is used to validate all the details which are recived in the $request */
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:inventory_categories',
        ]);

        /* fails() will return tru only if any of details which validator checks is no valid */
        if ($validator->fails()) {
           $errors = $validator->getMessageBag()->toArray();
           return response()->json(['validation_error'=>true,'message'=>"The name field is unique and required"]);
        } else {
            $banner_categories = new Category();
            $banner_categories->parent_id =null;
            $banner_categories->lft  = null;
            $banner_categories->rght = null;
            $banner_categories->name = $request->name;
            $banner_categories->type = 'banner';
            $addCate = $banner_categories->save();       
            if($addCate){
               return response()->json(['seccess'=>true,'message'=>"Add successfully"]);
            }           
        }        
    }
     /**
     * @decription Delete Banner Category
     * @param $request data object
     */
    public function delete_banner_categories(Request $request){
       
        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $cate_id = $post_data['id'];
                $categories = new Category();
                $delete = $categories->deleteCategory($cate_id);
                if($delete){
                   return array('status' => 'success');
                }else{
                   return array('status' => 'error');
                }
            }else{
               return array('status' => 'error');
            }
        }       
    }
}
