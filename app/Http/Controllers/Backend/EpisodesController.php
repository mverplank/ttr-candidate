<?php
/**
 * @author : Contriverz
 * @since  : 08-02-2019
 */
namespace App\Http\Controllers\Backend;
use App\Models\Backend\Guest;
use App\Models\Backend\Setting;
use Illuminate\Http\Request;
use App\Models\Backend\Archive;
use App\Models\Backend\Channel;
use App\Models\Backend\Category;
use App\Models\Backend\Host;
use App\Models\Backend\Show;
use App\Models\Backend\Episode;
use App\Models\Backend\Sponsor;
use App\Models\Backend\Schedule;
use App\Models\Backend\EpisodesInterval;
use App\Models\Backend\EpisodesHasHost;
use App\Models\Backend\EpisodesHasGuest;
use App\Models\Backend\EpisodeScheduledShare;
use App\Models\Backend\MediaLinking;
use Carbon\Carbon;
use Session;
use Share;
use Auth;
/*use Illuminate\Support\Facades\Validator;*/

//use Session;

class EpisodesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
      
    }


    public function ajax_index(Request $request)
    {
        $banners       = new Banner();
        $draw          = 1;
        $is_online     = 2;
        $channel_id    = 0;
        $channel_id    = $request->input('channel_id');
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        if($request->exists('is_online')){
            $is_online  = $request->input('is_online');
        }
       
        $mode = "simple";
        $filter_banners = "";
        if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
        }
        /*if(!empty($channel_id)){            
            $banner_has_channel = new BannersHasChannel();
            $filter_banners = $banner_has_channel->fetchBannersAccToChannel($channel_id, $is_online);
            $mode = 'relation';
        }else{  */          
            $all_banners = $banners->getAllBanners($is_online, $channel_id, $start, $length, $order, $dir, null, $search_value);
            $count_banners = $banners->countAllBanners($is_online, $channel_id, $order, $dir, null, $search_value);
            //$mode = 'simple';
        //}
        $data = array();
        $i    = 0;   

        if(!empty($all_banners)){
            foreach($all_banners as $banner){
               
                $data[$i][]  = '<a href="{{$banner->url}}" target="_blank">'.$banner->url.'</a>
                                <br/>'.$banner->title;
                $data[$i][]  = $banner->name;
                $data[$i][]  = $banner->clicks;
                if($banner->is_online == 1) {
                   $data[$i][]  = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
                }                                        
                else{
                    $data[$i][]  = ' <button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Inactive</button> ';
                }
                $data[$i][]  = '<a href="'.url('admin/advertising/banners/edit/'.$banner->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="javascript:void(0)" title="Delete" onclick="deleteBanner('.$banner->id.', this)" ><i class="glyphicon glyphicon-trash"></i></a>';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_banners,
                    'recordsFiltered' => $count_banners,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
        /*if($is_online){
            die('baneers');
            $banners = new Banner();
            $filter_banners = $banners->getAllBanners($is_online);
            $mode = 'simple';
        }*/
    }

    public function archived(Request $request)
    {
        $selected_channel = "";
        $selected_show    = "";
        $selected_host    = "";
        $selected_year    = "";
        $selected_month   = "";
        $selected_cat     = "";        
        $selected_channel = $request->input("channel_id");
        $selected_show    = $request->input("show_id");
        $selected_host    = $request->input("host_id");
        $selected_year    = $request->input("year");
        $selected_month   = $request->input("month");
        $selected_cat     = $request->input("category_id");

        $all_shows = '';
        $all_hosts = '';
        $years     = '';
        $months    = '';
        $type      = 'admin';
        $all_categories = '';
        if (Auth::check())
        {
            $userId = Auth::id();
          
            //Fetch all Channels
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                $type = 'host';
                $host = new Host();
                $get_host_id  = $host->getHostId($userId);
                if($get_host_id != 0){
                    $all_channels = $host->getAllChannels($get_host_id);
                    $all_channels->put('', 'Any Channel');
                }
            }else{
                $type = 'admin';
                $channels = new Channel();
                $all_channels = $channels->getAllChannels();
                $all_channels->put('', 'Any Channel');

                //Fetch all Shows
                $shows = new Show();
                $all_shows = $shows->getShowsForFilter();
                $all_shows->put('', 'All Shows');

                //Fetch all Host
                $hosts = new Host();
                $all_hosts = $hosts->getHostsForFilter();
                $all_hosts->put('', 'All Hosts');
                
                // Years
                $year = date('Y');
                $startYear = 2003;
                $years = ['' => 'All Years'];
                while ($year >= $startYear) {

                    $years[$year] = $year;
                    $year--;
                }

                // Months
                $months = array('' => 'All Months', 1 => 'January', 2 => 'February', 3 => 'March',
                    4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
                    9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');

                // Fetch all Categories for filters
                $categories = new Category();
                $all_categories = $categories->getCategoriesForFilter('show', true, $selected_cat, true);
            }         
            
            return view('backend/show/archived')->with(compact('all_channels', 'all_shows', 'all_hosts', 'years', 'months', 'all_categories', 'selected_channel', 'selected_show', 'selected_year', 'selected_month', 'selected_host', 'selected_cat', 'type'));
        }else{
            return redirect('/');
        }
    }

    public function ajax_archived(Request $request)
    {
        $archives   = new Archive();
        $draw       = 1;
        $year       = 0;
        $month      = 0;
        $show_id    = 0;
        $channel_id = 0;
        $host_id    = 0;
        $cat_id     = 0;
        $type       = 1;
        $current_user_id = '';
        //$channel_id = $request->input('channel_id');
        $start   = $request->input('start');
        $length  = $request->input('length');
        $draw    = $request->input('draw');
        $order   = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        /* if($request->exists('is_online')){
            $is_online  = $request->input('is_online');
        }
       
        $mode = "simple";
        $filter_banners = "";*/
        if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
        }
        if ($request->has('show_id')) {
            $show_id = $request->input('show_id');
        }
        if ($request->has('year')) {
            $year = $request->input('year');
        }
        if ($request->has('month')) {
            $month = $request->input('month');
        }
        if ($request->has('host_id')) {
            $host_id = $request->input('host_id');
        }
        if ($request->has('category_id')) {
            $cat_id = $request->input('category_id');
        }
        
        if (Auth::check())
        {
            // $current_user_id = Auth::id();
            $host = new Host();
            $current_user_id = $host->getHostId(Auth::id());
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                $type = 2;
            }
        }
        
        $all_archive_episodes = $archives->getAllArchivedEpisodes($current_user_id, $type, $cat_id, $host_id, $month, $year, $show_id, $channel_id, $start, $length, $order, $dir, null, $search_value);
        $count_archive_episodes = $archives->countAllArchivedEpisodes($current_user_id, $type, $cat_id, $host_id, $month, $year, $show_id, $channel_id, $order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   
        $icon='';
//echo"<pre>";print_r($all_archive_episodes);die;
        if(!empty($all_archive_episodes)){
            foreach($all_archive_episodes as $archive_episode){
               $checkhasmedia =MediaLinking::where('module_id',$archive_episode->episode_id)
                                ->where('main_module','Episode')
                                ->where('sub_module','media')
                                ->first();
                if ($checkhasmedia) {
                   $icon='<i class="fa fa-play-circle-o" aria-hidden="true"style="font-size:45px;"></i>';
                }else{
                    $icon='';
                }
                $data[$i][]  = date_format(date_create($archive_episode->datetime),"M dS Y g:i a");
                $archive_title = $archive_episode->title.'<br>';
                if(!empty($archive_episode->hosts)){
                    $archive_title .= '<div class="l-hosts">Hosts:</div><div class="tags">'.$archive_episode->hosts.'</div>';
                } if(!empty($archive_episode->cohosts)){
                    $archive_title .= '<div class="l-cohosts">Co-hosts:</div><div class="tags">'.$archive_episode->cohosts.'</div>';
                }if(!empty($archive_episode->podcasthosts)){
                    $archive_title .= '<div class="l-podcasthosts">Podcast-hosts:</div><div class="tags">'.$archive_episode->podcasthosts.'</div>';
                }
                if(\Config::get('constants.ADMIN') == $type){
                    if(!empty($archive_episode->guests)){
                        $archive_title .= '<div class="l-guests">Guests:</div><div class="tags">'.$archive_episode->guests.'</div>';
                    }
                }
                
                $data[$i][]  = $archive_title;
                if(\Config::get('constants.HOST') == $type){
                    $data[$i][]  = '<div class="tags">'.$archive_episode->guests.'</div>';
                    $data[$i][]  = $icon;
                    $data[$i][]  = '<a href="'.url('host/show/episodes/edit_archived/'.$archive_episode->episode_id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="'.url('host/show/episodes/view_archived/'.$archive_episode->episode_id ).'" title="View details" ><i class="mdi mdi-eye"></i></a>
                                <a href="javascript:void(0)" onclick="deleteArchived('.$archive_episode->episode_id.', this)" ><i class="glyphicon glyphicon-trash" title="Delete"></i></a>';
                }else {
                    $data[$i][]  = ($archive_episode->encores_number != 0) ? '<a href="javascript:void(0)" onclick="getEncoreDetail('.$archive_episode->episode_id.')">'.$archive_episode->encores_number.'</a>' : '-';

                    /*'<a href="javascript:void(0)" onclick="getEncoreDetail('.$archive_episode->episode_id.')">'.$archive_episode->encores_number.'</a>' : '-' ;*/
                    $data[$i][]  = $icon;
                    $data[$i][]  = '<a href="'.url('admin/show/episodes/edit_archived/'.$archive_episode->episode_id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="'.url('admin/show/episodes/view_archived/'.$archive_episode->episode_id ).'" title="View details" ><i class="mdi mdi-eye"></i></a>
                                <a href="javascript:void(0)" data-type="archived" onclick="deleteArchived('.$archive_episode->episode_id.', this)" ><i class="glyphicon glyphicon-trash" title="Delete"></i></a>';

                }
                $i++;
            } 
        }
        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_archive_episodes,
                    'recordsFiltered' => $count_archive_episodes,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    /**
     * Display the view for the upcoming episodes.
     *
     * @return view
     */  
    public function upcoming(Request $request)
    {
        $selected_channel = "";
        $selected_sponsor = "";
        $selected_host    = "";
        $selected_channel = $request->input("channel_id");
        $selected_sponsor = $request->input("sponsor_id");
        $selected_host    = $request->input("host_id");
        $all_shows = '';
        $all_hosts = '';
        $type      = 'admin';
        $all_categories = '';
        $all_sponsors   = '';
        $all_channels   = '';
        if (Auth::check())
        {
            $userId = Auth::id();            
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                //Fetch all Channels
                $type = 'host';
                $host = new Host();
                $get_host_id  = $host->getHostId($userId);
                if($get_host_id != 0){
                    $all_channels = $host->getAllChannels($get_host_id);
                    $all_channels->put('', 'Any Channel');
                }
            }else{
                //Fetch all Channels
                $type = 'admin';
                $channels = new Channel();
                $all_channels = $channels->getAllChannels();
                $all_channels->put('', 'Any Channel');

                //Fetch all Shows
                $sponsors = new Sponsor();
                $all_sponsors = $sponsors->getSponsorsForFilter();
                $all_sponsors->put('', 'All Sponsors');

                //Fetch all Host
                $hosts = new Host();
                $all_hosts = $hosts->getHostsForFilter();
                $all_hosts->put('', 'All Hosts');
            }  
            return view('backend/show/upcoming')->with(compact('all_channels', 'all_shows', 'all_hosts', 'all_sponsors', 'selected_channel', 'selected_host', 'selected_sponsor', 'type'));
        }else{
            return redirect('/');
        }
    }

    /**
     * Fecth the information for the upcoming episodes and display the view.
     *
     * @return view
     */  
    public function ajax_upcoming(Request $request)
    {
        $episodes      = new Episode();
        $draw          = 1;
        $year          = 0;
        $month         = 0;
        $sponsor_id    = 0;
        $channel_id    = 0;
        $host_id       = 0;
        $type          = 1;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        $current_user_id = '';
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        /* if($request->exists('is_online')){
            $is_online  = $request->input('is_online');
        }
       
        $mode = "simple";
        $filter_banners = "";*/
        if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
        }
        if ($request->has('sponsor_id')) {
            $sponsor_id = $request->input('sponsor_id');
        }       
        if ($request->has('host_id')) {
            $host_id = $request->input('host_id');
        }       
        
        if (Auth::check())
        {
            // $current_user_id = Auth::id();
            $host = new Host();
            $current_user_id = $host->getHostId(Auth::id());
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                $type = 2;
            }
        }

        //$all_upcoming_episodes = $episodes->getAllUpcomingEpisodes($current_user_id, $type, $host_id, $sponsor_id, $channel_id, $start, $length, $order, $dir, null, $search_value);

        $all_upcoming_episodes = $episodes->getAllUpcomingEpisodesAjax($current_user_id, $type, $host_id, $sponsor_id, $channel_id, $start, $length, $order, $dir, null, $search_value);
        
       
        $count_upcoming_episodes = $episodes->countAllUpcomingEpisodes($current_user_id, $type, $host_id, $sponsor_id, $channel_id, $order, $dir, null, $search_value);
        
        $data = array();
        $i    = 0;   
        $guests = '';
        if(!empty($all_upcoming_episodes)){
            foreach($all_upcoming_episodes as $upcoming_episode){
                //echo '<pre>';
                //print_r($upcoming_episode->pluck('o_episode_id'));
                //print_r($upcoming_episode);
                //echo '</pre>';

                //$data[$i][]  = date_format(date_create($upcoming_episode->Episode__fulldate),"M dS Y g:i A");
                $data[$i][]     = (($upcoming_episode->Episode__fulldate));
                $data[$i][]     = $upcoming_episode->Episode__title;
                $guests         = episode_guests($upcoming_episode->Episode_id);
                //if(\Config::get('constants.ADMIN') == $type){
                    /*if($upcoming_episode->guests->count() > 0){
                        foreach($upcoming_episode->guests as $guest){
                            $guests .= '<div class="tags">'.$guest->name.'</div>';
                        }
                    }*/
                //}
                $data[$i][] = $guests;
                $data[$i][] = $upcoming_episode->sponsor_name;
                
                //$data[$i][]  = $upcoming_episode->Episode__title_only;
                /*if(\Config::get('constants.HOST') == $type){
                    $data[$i][]  = '<div class="tags">'.$upcoming_episode->guests.'</div>';
                    $data[$i][]  = "";
                }else {*/
                $upcoming_episode->o_episode_id = $upcoming_episode->Episode_id;
                $data[$i][]  = '<a href="'.url('admin/show/episodes/edit_upcoming/'.$upcoming_episode->o_episode_id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="'.url('admin/show/episodes/view_upcoming/'.$upcoming_episode->o_episode_id ).'" title="View details" ><i class="mdi mdi-eye"></i></a><a href="javascript:void(0)" onclick="deleteUpcoming('.$upcoming_episode->o_episode_id.', this)" data-type="upcoming"><i class="glyphicon glyphicon-trash" title="Delete"></i></a>';
                //}
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_upcoming_episodes,
                    'recordsFiltered' => $count_upcoming_episodes,
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    /**
     * Display the view for the episodes to add the encores to them
     * @param $id is the schedule id
     * @param $date is the current selected date
     * @return view
     */  
    public function select_encore($id, $date, Request $request)
    {
        $selected_channel = "";
        $selected_show    = "";
        $selected_guest   = "";
        $selected_host    = "";
        $selected_show    = $request->input("show_id");
        $selected_guest   = $request->input("guest_id");
        $selected_host    = $request->input("host_id");
        $all_shows        = '';
        $all_hosts        = '';
        $type             = 'admin';
        $all_categories   = '';
        $all_channels     = '';
        if (Auth::check())
        {
            $userId = Auth::id();

            // Current schedule id
            $schedule_id = 0;
            if (!empty($id) && $id !=0 ) {
                $schedule_id = $id;
            }

            // Current selected date
            $selected_date = "";
            if (!empty($date)) {
                $selected_date = $date;
            }

            //Fetch all Channels
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                $type = 'host';
                $host = new Host();
                $get_host_id  = $host->getHostId($userId);
                if($get_host_id != 0){
                    $all_channels = $host->getAllChannels($get_host_id);
                    $all_channels->put('', 'Any Channel');
                }
            }else{
                //Fetch all Shows
                $type = 'admin';
                $shows = new Show();
                $all_shows = $shows->getShowsForFilter();
                $all_shows->put('', 'All Shows');

                //Fetch all Guests
                $guests = new Guest();
                $all_guests = $guests->getGuestsForFilter();
                $all_guests->put('', 'All Guests');

                //Fetch all Host
                $hosts = new Host();
                $all_hosts = $hosts->getHostsForFilter();
                $all_hosts->put('', 'All Hosts');

            }  
            return view('backend/show/select_encore')->with(compact('all_channels', 'all_shows', 'all_hosts', 'all_guests', 'selected_channel', 'selected_host', 'selected_show', 'selected_guest', 'type', 'selected_date', 'schedule_id'));
            //return view('backend/show/upcoming');
        }else{
            return redirect('/');
        }
    }

    /**
     * Fetch the episodes to add the encores to that
     * @param $id is the schedule id
     * @param $date is the current selected date
     * @return view
     */  
    public function ajax_select_encore(Request $request)
    {
        $episodes      = new Episode();
        $draw          = 1;
        $year          = 0;
        $month         = 0;
        $sponsor_id    = 0;
        $channel_id    = 0;
        $host_id       = 0;
        $episode_date  = '';
        $guest_id      = '';
        $show_id       = '';
        $type          = 1;
        $start         = $request->input('start');
        $length        = $request->input('length');
        $draw          = $request->input('draw');
        $order         = $request->post("order");
        $search_arr    = $request->post("search");
        $search_value  = $search_arr['value'];
        $search_regex  = $search_arr['regex'];
        $columns       = $request->post("columns");
        $schedule_id   = $request->post("schedule_id");
        $selected_date = $request->post("selected_date");
        $count_banners = 0;
        $col = 0;
        $dir = "";
        $current_user_id = '';
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        } 
        /* if($request->exists('is_online')){
            $is_online  = $request->input('is_online');
        }
        $mode = "simple";
        $filter_banners = "";*/
        if ($request->has('channel_id')) {
            $channel_id = $request->input('channel_id');
        }
        if ($request->has('show_id')) {
            $show_id = $request->input('show_id');
        }
        if ($request->has('guest_id')) {
            $guest_id = $request->input('guest_id');
        }       
        if ($request->has('host_id')) {
            $host_id = $request->input('host_id');
        } 
        if ($request->has('episode_date')) {
            $episode_date = $request->input('episode_date');
        }       
        
        if (Auth::check())
        {
            // $current_user_id = Auth::id();
            $host = new Host();
            $current_user_id = $host->getHostId(Auth::id());
            if(\Config::get('constants.HOST') == getCurrentUserType()){
                $type = 2;
            }
        }

        $select_episodes_for_encore = $episodes->getEpisodesForEncores($episode_date, $schedule_id, $selected_date, $current_user_id, $type, $host_id, $guest_id, $show_id, $start, $length, $order, $dir, null, $search_value);       

        $count_episodes_for_encore  = $episodes->countEpisodesForEncores($episode_date, $schedule_id, $selected_date, $current_user_id, $type, $host_id, $guest_id, $show_id, $order, $dir, null, $search_value);
       
        $data = array();
        $i    = 0;   
        
        if(!empty($select_episodes_for_encore)){
            foreach($select_episodes_for_encore as $episode){
               
                $data[$i][]  = date_format(date_create($episode->Episode__fulldate),"M dS Y g:i A");               
                $data[$i][]  = '<a target="_blank" href="'.url('admin/show/episodes/add_encore').'/'.$schedule_id.'/'.$selected_date.'/'.$episode->s_episode_id.'">'.$episode->Episode__title.'</a>';
                $data[$i][]  = ($episode->encores_number != 0) ? '<a href="javascript:void(0)" onclick="getEncoreDetail('.$episode->s_episode_id.')">'.$episode->encores_number.'</a>' : '-';
                $i++;
            } 
        }

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => count($count_episodes_for_encore),
                    'recordsFiltered' => count($count_episodes_for_encore),
                    'data' => $data
                );

        echo json_encode($output);
        exit();
    }

    /**
     * Fecth the information for the archived episodes and display the view.
     *
     * @return view
     */  
    public function view_archived($id, Request $request){
        // $episode = array();
        // if($id != 0){
        //     $episodeObj = new Episode();
        //     $episode    = $episodeObj->getEpisodeById($id);
        // }

        //   $years=array();    
        //     $currYear = date('Y');
        //     $year = '2016'; // stast started on 2016 
        //     while ($year <= $currYear) {
        //         $years[$year] = $year;
        //         $year++;
        //     }
        //     $day= '1'; // days of month
        //     while ($day <= 31) {
        //         $days[$day] = $day;
        //         $day++;
        //     }
        //     $hr= '1'; // days of month
        //     while ($hr <= 12) {
        //         $hours[$hr] = $hr;
        //         $hr++;
        //     }
        //     $nextyears = array($currYear=>$currYear,$currYear+1=>$currYear+1);
        //     $mints = array('00'=>'00','15'=>'15','30'=>'30','45'=>'45');
        //     $months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        //     $meridian  = array('am' =>'am', 'pm'=>'pm');

        //     if($request->isMethod('post') || $request->isMethod('put')){
             
        //     $data = $request->input('data'); 
        //     $episode = new Episode();
        //     $response = $episode->editScheduledShare($id,$data);

        //     if($response){
        //         $message = "successfull";
        //         $var     = "success";
        //     }else{
        //         $message = "There is some problem while creating the episode. Please try again.";
        //         $var     = "error";
        //     }
        //     Session::flash($var , $message);
        //     return redirect('admin/show/episodes/view_archived/'.$id);
        // }else{
        //     return view('backend/show/view_archived')->with(compact('episode','years', 'months','days','hours','nextyears','mints','months','meridian'));
        // }


        $schedule = EpisodeScheduledShare::where('episodes_id', '=' ,$id)->get()->toArray();
        $segment1 = array();
        $segment2 = array();
        $segment3 = array();
        $segment4 = array();
        $episode_arr = array();
        foreach ( $schedule as $value) {
            if($value['type'] == 'segment1'){
                $segment1 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment1['time_twitter'] =date("H:i:s", $dat_t);
                $segment1['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment1['time_facebook'] =date("H:i:s", $dat_f);
                $segment1['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment2'){
                $segment2 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment2['time_twitter'] =date("H:i:s", $dat_t);
                $segment2['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment2['time_facebook'] =date("H:i:s", $dat_f);
                $segment2['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment3'){
                $segment3 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment3['time_twitter'] =date("H:i:s", $dat_t);
                $segment3['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment3['time_facebook'] =date("H:i:s", $dat_f);
                $segment3['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment4'){
                $segment4 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment4['time_twitter'] =date("H:i:s", $dat_t);
                $segment4['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment4['time_facebook'] =date("H:i:s", $dat_f);
                $segment4['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'episode'){
                $episode_arr =$value;
                
                $dat_t = strtotime($value['twitter_time']);
                $episode_arr['time_twitter'] =date("H:i:s", $dat_t);
                $episode_arr['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $episode_arr['time_facebook'] =date("H:i:s", $dat_f);
                $episode_arr['date_facebook'] =date("m-d-Y", $dat_f);
            }         
        }
      
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Episode');

        $episode = array();
        if($id != 0){
            $episodeObj = new Episode();
            $episode    = $episodeObj->getEpisodeById($id);
        }
        // $years=array();    
         $currYear = date('Y');
         $year = '2016'; // start started on 2016 
        while ($year <= $currYear) {
            $years[$year] = $year;
            $year++;
        }
         $months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        return view('backend/show/view_archived')->with(compact('episode','media','segment1','segment2','segment3','segment4','episode_arr','months','years'));
                
    }

    public function edit_archived($id, Request $request)
    {      
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Episode');                   
        // Fetch all Categories for filters
        $categories = new Category();        
        //all categories of type show
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false); 
        $assigned_categories = array();
        $episode =  new Episode();
        $episode =$episode->getEpisodeById($id);

         //Fetch tags
        $original_tags = $tag_ids = $tags_arr = array();
        $tags = '';
        // echo "<pre>";print_R($episode);die;
        if($episode){
            if($episode->tags->count() > 0){
                foreach($episode->tags as $tag){
                    $tags_arr[] = $tag->name;
                    $original_tags[] = $tag_ids[$tag->name] = $tag->id;
                }
                if(!empty($tags_arr)){
                    $tags = implode(',',$tags_arr);
                }
            }
            $tag_ids = json_encode($tag_ids);
        }

        // Fetch channels of the episode
        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        // Fetch channels of the episode
        $episode_has_channels = array();
        if($episode->channels->count() > 0){
            foreach($episode->channels as $channel){
                $episode_has_channels[] = $channel->id;
            }
        }
         $archive = Archive::where('episode_id',$id)->first();
        if($episode->episodes_has_categories->count() > 0){
            foreach($episode->episodes_has_categories as $cat){
                $assigned_categories[] = $cat->categories_id;
            }
        }

        if ($request->isMethod('post')) {
            $update_data = $request->data;
            $episode = new Episode();
            $update = $episode->edit($id, $update_data);
            $archive = new Archive();
            $update_archive = $archive->edit($id, $update_data);
            if($update && $update_archive){
                $message = "The episode details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the episode. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/show/episodes/archived');
        }else{
            return view('backend/show/archived_edit', compact('archive','all_categories', 'assigned_categories', 'episode', 'all_channels', 'episode_has_channels', 'media', 'tags', 'tag_ids', 'original_tags'));
        }
    }

    public function delete_archive(Request $request){
        if ($request->isMethod('post')) {
            $post_data =  $request->all();
            if(!empty($post_data)){
                $episode_id = $post_data['id'];
                $episode = new Episode();
                $delete = $episode->deleteEpisode($episode_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    /**
    * @description Set the interval
    * @param $request is the request object
    */
    public function set_interval(Request $request){
        // Check the page entry in the interval table by the current user
        $current_user_id = 0;
        $id = $request->input('id');
        $intervalObj = new EpisodesInterval();
        if (Auth::check())
        {
            $current_user_id = Auth::user()->id;
        }
        // Check if page entry exists or not
        $episode_exists = $intervalObj->where(['episodes_id'=>$id])->first();
        if($episode_exists){
            $interval_exists = $intervalObj->where(['episodes_id'=>$id, 'users_id'=>$current_user_id])->first();

            if($interval_exists){
               
                // Save the entry to the episodes_interval table        
                $interval_data = array(
                            'episodes_id' => $id,
                            'users_id'    => $current_user_id, 
                            'entry'       => Carbon::now(), 
                        );

                $intervalObj->addToInterval($interval_data);
                return array(['status'=>'success', 'message'=>'']);
            } else{
                
                return array(['status'=>'error', 'message'=>'This episode is already editing by the other user']);
            } 
        }else{
            // Save the entry to the episodes_interval table        
            /*$interval_data = array(
                        'episodes_id' => $id,
                        'users_id'    => $current_user_id, 
                        'entry'       => Carbon::now(), 
                    );

            $intervalObj->addToInterval($interval_data);*/
            return array(['status'=>'success', 'message'=>'']);
        }            
    }

    /**
    * @description edit episodes
    * @param $request is the request object
    */
    public function edit_episode($id, Request $request){ 

        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Episode');
        // Check the page entry in the interval table by the current user
        $current_user_id = 0;
        $intervalObj = new EpisodesInterval();
        if (Auth::check())
        {
            $current_user_id = Auth::user()->id;
        }
        // Check if page entry exists or not
        $episode_exists = $intervalObj->where(['episodes_id'=>$id])->first();
        if($episode_exists){
            $interval_exists = $intervalObj->where(['episodes_id'=>$id, 'users_id'=>$current_user_id])->first();

            if($interval_exists){
                // Save the entry to the episodes_interval table        
                $interval_data = array(
                            'episodes_id' => $id,
                            'users_id'    => $current_user_id, 
                            'entry'       => Carbon::now(), 
                        );

                $intervalObj->addToInterval($interval_data);
            }  
        } else{
            $interval_data = array(
                        'episodes_id' => $id,
                        'users_id'    => $current_user_id, 
                        'entry'       => Carbon::now(), 
                    );

            $intervalObj->addToInterval($interval_data);
        }   

        // Fetch all Categories for filters
        $categories = new Category();  

        // All categories of type show
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false); 
        $assigned_categories = array();
        $episode =  new Episode();
        $episode =$episode->getEpisodeById($id);
        
        //Fetch tags
        $original_tags = $tag_ids = $tags_arr = array();
        $tags = '';
        // echo "<pre>";print_R($episode);die;
        if($episode){
            if($episode->tags->count() > 0){
                foreach($episode->tags as $tag){
                    $tags_arr[] = $tag->name;
                    $original_tags[] = $tag_ids[$tag->name] = $tag->id;
                }
                if(!empty($tags_arr)){
                    $tags = implode(',',$tags_arr);
                }
            }
            $tag_ids = json_encode($tag_ids);

            // Fetch channels of the episode
            $channels = new Channel();
            $all_channels = $channels->getAllChannels();

            // Fetch channels of the episode
            $episode_has_channels = array();
            if($episode->channels->count() > 0){
                foreach($episode->channels as $channel){
                    $episode_has_channels[] = $channel->id;
                }
            }
            if($episode->episodes_has_categories->count() > 0){
                foreach($episode->episodes_has_categories as $cat){
                    $assigned_categories[] = $cat->categories_id;
                }
            }
            if ($request->isMethod('post')) {
                $update_data = $request->data;
                $episode = new Episode();
                
                $update = $episode->edit($id, $update_data);
                if($update){

                    // Remove the interval of the episode
                    $intervalObj->deleteInterval($id);

                    $message = "The episode details has been updated.";
                    $var     = "success";
                }else{
                    $message = "There is some problem while updating the episode. Please try again.";
                    $var     = "error";
                }
                Session::flash($var , $message);
                return redirect('admin/show/episodes/view_agenda/'.$id);
            }else{
                return view('backend/show/episode_edit', compact('all_categories', 'assigned_categories','episode','all_channels','episode_has_channels', 'tags', 'tag_ids', 'original_tags','media'));
            }
        }
        else{
            Session::flash('error' , 'Episode id does not exist.');
            return redirect('admin/show/episodes/view_agenda/'.$id);
        }        
    }

   /**
    * @description edit episodes upcoming
    * @param $request is the request object
    */
    public function edit_episode_upcoming($id, Request $request){
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Episode');
        // Check the page entry in the interval table by the current user
        $current_user_id = 0;
        $intervalObj = new EpisodesInterval();
        if (Auth::check())
        {
            $current_user_id = Auth::user()->id;
        }
        // Check if page entry exists or not
        $episode_exists = $intervalObj->where(['episodes_id'=>$id])->first();
        if($episode_exists){
            $interval_exists = $intervalObj->where(['episodes_id'=>$id, 'users_id'=>$current_user_id])->first();

            if($interval_exists){
                // Save the entry to the episodes_interval table        
                $interval_data = array(
                            'episodes_id' => $id,
                            'users_id'    => $current_user_id, 
                            'entry'       => Carbon::now(), 
                        );

                $intervalObj->addToInterval($interval_data);
            }  
        } else{
            $interval_data = array(
                        'episodes_id' => $id,
                        'users_id'    => $current_user_id, 
                        'entry'       => Carbon::now(), 
                    );

            $intervalObj->addToInterval($interval_data);
        }   

        // Fetch all Categories for filters
        $categories = new Category();  

        // All categories of type show
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false); 
        $assigned_categories = array();
        $episode =  new Episode();
        $episode =$episode->getEpisodeById($id);
        
        //Prepare the Reschedule data
        $get_ahead_schedules = $this->setDataForUpcomingShowsSelect($episode->channels_id, $episode);
        
        //Fetch tags
        $original_tags = $tag_ids = $tags_arr = array();
        $tags = '';

        if($episode){
            if($episode->tags->count() > 0){
                foreach($episode->tags as $tag){
                    $tags_arr[] = $tag->name;
                    $original_tags[] = $tag_ids[$tag->name] = $tag->id;
                }
                if(!empty($tags_arr)){
                    $tags = implode(',',$tags_arr);
                }
            }
            $tag_ids = json_encode($tag_ids);

            // Fetch channels of the episode
            $channels = new Channel();
            $all_channels = $channels->getAllChannels();

            // Fetch channels of the episode
            $episode_has_channels = array();
            if($episode->channels->count() > 0){
                foreach($episode->channels as $channel){
                    $episode_has_channels[] = $channel->id;
                }
            }
            if($episode->episodes_has_categories->count() > 0){
                foreach($episode->episodes_has_categories as $cat){
                    $assigned_categories[] = $cat->categories_id;
                }
            }

            // Make reschedule data

            if ($request->isMethod('post')) {
                $update_data = $request->data;
                $episode = new Episode();
                
                $update = $episode->edit($id, $update_data);
                if($update){

                    // Remove the interval of the episode
                    $intervalObj->deleteInterval($id);

                    $message = "The episode details has been updated.";
                    $var     = "success";
                }else{
                    $message = "There is some problem while updating the episode. Please try again.";
                    $var     = "error";
                }
                Session::flash($var , $message);
                return redirect('admin/show/episodes/upcoming');
            }else{
                return view('backend/show/edit_view_upcoming', compact('all_categories', 'assigned_categories','episode','all_channels','episode_has_channels', 'tags', 'tag_ids', 'original_tags','media', 'get_ahead_schedules'));
            }
        }
        else{
            Session::flash('error' , 'Episode id does not exist.');
            return redirect('admin/show/episodes/upcoming');
        }     
    }

        /**
    * @description Get hosts for the episodes
    * @param $request is the request object
    */
    public function getHosts(Request $request){

        if ($request->isMethod('post')) {

            $post_data =  $request->all();
            if(!empty($post_data)){
                $episode_id = $post_data['episode_id'];
                $type       = $post_data['type'];
                $episodes   = new EpisodesHasHost();
                // echo"<pre>";print_r($episodes);die();
                $get  = $episodes->getHostsByType($episode_id, $type);
               
                if($get){
                    return $get;
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

      /**
    * @description Get guests for the episodes
    * @param $request is the request object
    */
    public function get_guests(Request $request){

        if ($request->isMethod('post')) {

            $post_data =  $request->all();
           // echo"<pre>";print_r($post_data);die();
            if(!empty($post_data)){
                $episode_id = $post_data['episode_id'];
                $episodes    = new EpisodesHasGuest();
               // echo"<pre>";print_r($episodes);die();
                $get  = $episodes->getGuestsByType($episode_id);
               
                if($get){
                    return $get;
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }

    /**
     * @description Get episode view 
     * @param $request is the request object
     */
    public function get_view_agenda($id, Request $request){
        $schedule = EpisodeScheduledShare::where('episodes_id', '=' ,$id)->get()->toArray();
        $segment1 = array();
        $segment2 = array();
        $segment3 = array();
        $segment4 = array();
        $episode_arr = array();
        foreach ( $schedule as $value) {
            if($value['type'] == 'segment1'){
                $segment1 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment1['time_twitter'] =date("H:i:s", $dat_t);
                $segment1['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment1['time_facebook'] =date("H:i:s", $dat_f);
                $segment1['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment2'){
                $segment2 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment2['time_twitter'] =date("H:i:s", $dat_t);
                $segment2['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment2['time_facebook'] =date("H:i:s", $dat_f);
                $segment2['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment3'){
                $segment3 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment3['time_twitter'] =date("H:i:s", $dat_t);
                $segment3['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment3['time_facebook'] =date("H:i:s", $dat_f);
                $segment3['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment4'){
                $segment4 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment4['time_twitter'] =date("H:i:s", $dat_t);
                $segment4['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment4['time_facebook'] =date("H:i:s", $dat_f);
                $segment4['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'episode'){
                $episode_arr =$value;
                
                $dat_t = strtotime($value['twitter_time']);
                $episode_arr['time_twitter'] =date("H:i:s", $dat_t);
                $episode_arr['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $episode_arr['time_facebook'] =date("H:i:s", $dat_f);
                $episode_arr['date_facebook'] =date("m-d-Y", $dat_f);
            }
          
        }
        //echo"<pre>";print_r($segment4);die;
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Episode');
        $episode = array();
        if($id != 0){
            $episodeObj = new Episode();
            $episode    = $episodeObj->getEpisodeById($id);
        }
        // $years=array();    
         $currYear = date('Y');
         $year = '2016'; // start started on 2016 
        while ($year <= $currYear) {
            $years[$year] = $year;
            $year++;
        }
        // $day= '1'; // days of month
        // while ($day <= 31) {
        //     $days[$day] = $day;
        //     $day++;
        // }
        // $hr= '1'; // days of month
        // while ($hr <= 12) {
        //     $hours[$hr] = $hr;
        //     $hr++;
        // }
        // $nextyears = array($currYear=>$currYear,$currYear+1=>$currYear+1);
        // $mints = array('00'=>'00','15'=>'15','30'=>'30','45'=>'45');
         $months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        // $meridian  = array('am' =>'am', 'pm'=>'pm');

        // if($request->isMethod('post') || $request->isMethod('put')){
             
        //     $data = $request->input('data'); 
        //     $episode  = new Episode();
        //     $response = $episode->editScheduledShare($id,$data);

        //     if($response){
        //         $message = "successfull";
        //         $var     = "success";
        //     }else{
        //         $message = "There is some problem while creating the episode. Please try again.";
        //         $var     = "error";
        //     }
        //     Session::flash($var , $message);
        //     return redirect('admin/show/episodes/view_agenda/'.$id);
        // }else{
        //     return view('backend/show/view_episode')->with(compact('episode','media','segment1','segment2','segment3','segment4','episode_arr','months','years'));
        // }

        return view('backend/show/view_episode')->with(compact('episode','media','segment1','segment2','segment3','segment4','episode_arr','months','years'));
    }

    /**
    * @description Get episode upcoming view 
    * @param $request is the request object
    */

      public function get_view_upcoming($id, Request $request){
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'Episode');
       $episode = array();
        if($id != 0){
            $episodeObj = new Episode();
            $episode    = $episodeObj->getEpisodeById($id);
        }
        $years=array();    
        $currYear = date('Y');
        $year = '2016'; // stast started on 2016 
        while ($year <= $currYear) {
            $years[$year] = $year;
            $year++;
        }
         $months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');

        $schedule = EpisodeScheduledShare::where('episodes_id', '=' ,$id)->get()->toArray();
        $segment1 = array();
        $segment2 = array();
        $segment3 = array();
        $segment4 = array();
        $episode_arr = array();
        foreach ( $schedule as $value) {
            if($value['type'] == 'segment1'){
                $segment1 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment1['time_twitter'] =date("H:i:s", $dat_t);
                $segment1['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment1['time_facebook'] =date("H:i:s", $dat_f);
                $segment1['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment2'){
                $segment2 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment2['time_twitter'] =date("H:i:s", $dat_t);
                $segment2['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment2['time_facebook'] =date("H:i:s", $dat_f);
                $segment2['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment3'){
                $segment3 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment3['time_twitter'] =date("H:i:s", $dat_t);
                $segment3['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment3['time_facebook'] =date("H:i:s", $dat_f);
                $segment3['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'segment4'){
                $segment4 =$value;

                $dat_t = strtotime($value['twitter_time']);
                $segment4['time_twitter'] =date("H:i:s", $dat_t);
                $segment4['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $segment4['time_facebook'] =date("H:i:s", $dat_f);
                $segment4['date_facebook'] =date("m-d-Y", $dat_f);
            }
            if($value['type'] == 'episode'){
                $episode_arr =$value;
                
                $dat_t = strtotime($value['twitter_time']);
                $episode_arr['time_twitter'] =date("H:i:s", $dat_t);
                $episode_arr['date_twitter'] =date("m-d-Y", $dat_t);

                $dat_f = strtotime($value['facebook_time']);
                $episode_arr['time_facebook'] =date("H:i:s", $dat_f);
                $episode_arr['date_facebook'] =date("m-d-Y", $dat_f);
            }
        }
        return view('backend/show/view_upcoming')->with(compact('episode','media','segment1','segment2','segment3','segment4','episode_arr','months','years'));
    }

    /**
     * Fetch the Podcast Stats for the episode.
     *
     * @return podcast stat data in json form
     */
    public function podcastStats(Request $request){
        
        if(empty($request->year)){
            $year = date('Y');
        }else{
            $year = $request->year;
        }

        if(empty($request->month)){
            $month = date('n');
        }else{
            $month = $request->month;
        }
        $episode_id = $request->episode_id;
        $episodeObj = new Episode();
      
        $episode    = $episodeObj->fetchPodcastHits($episode_id, $year, $month);
        
        if(!empty($episode)){
            return json_encode(array('status'=>'success', 'data'=>$episode));
        }else{
            return json_encode(array('status'=>'error'));
        }
    }


    public function episode_share(Request $request,$id,$social){

        $episode = Episode:: find($id);
        $description = $episode->description;
        //strip_tags($episode->description);
        if($social == 'Facebook'){
            $url = Share::load('http://178.128.179.99/e/'.$episode->id,"$description")->facebook();
        }
        if($social == 'Twitter'){
            //$url = Share::load(url('/').'/e/'.$episode->id, "$description")->twitter();
            $url = Share::load('http://178.128.179.99/e/'.$episode->id, "$description")->twitter();
        }
        if($social == 'Google'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->gplus();
            $url = Share::load('http://178.128.179.99/e/'.$episode->id, "$description")->gplus();
        }
        if($social == 'Linkedin'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->linkedin();
            $url = Share::load('http://178.128.179.99/e/'.$episode->id, "$description")->linkedin();
        }
        if($social == 'Pinterest'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->pinterest();
            $url = Share::load('http://178.128.179.99/e/'.$episode->id, "$description")->pinterest();
        }
        if($social == 'Email'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->email();
            $url = Share::load('http://178.128.179.99/e/'.$episode->id, "$description")->email();
        }
        if($social == 'Tumblr'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->tumblr();
            $url = Share::load('http://178.128.179.99/e/'.$episode->id, "$description")->tumblr();
        }                                        
        return redirect($url);

    }

    public function episode_share_segment(Request $request,$id,$social,$segment){
        $episode = Episode:: find($id);
        if($segment ==1){
            $description = $episode->segment1_title;
            //strip_tags($episode->description);
        } else if($segment ==2){
            $description = $episode->segment2_title;
            //strip_tags($episode->description);
        } else if($segment ==3){
            $description = $episode->segment3_title;
            //strip_tags($episode->description);
        }else{
             $description = $episode->segment4_title;
            //strip_tags($episode->description);           
        }       
         //return Share::load("http://178.128.179.99/e/$episode->id/segment/$segment",$description)->services();
        // die();
        if($social == 'Facebook'){
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment","$description")->facebook();
        }
        if($social == 'Twitter'){
            //$url = Share::load(url('/').'/e/'.$episode->id, "$description")->twitter();
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment","$description")->twitter();
        }
        if($social == 'Google'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->gplus();
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment","$description")->gplus();
        }
        if($social == 'Linkedin'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->linkedin();
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment", "$description")->linkedin();
        }
        if($social == 'Pinterest'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->pinterest();
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment","$description")->pinterest();
        }
        if($social == 'Email'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->email();
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment","$description")->email();
        }
        if($social == 'Tumblr'){
             //$url = Share::load('178.128.78.109/e/'.$episode->id, "$description")->tumblr();
            $url = Share::load("178.128.179.99/e/$episode->id?segment=$segment","$description")->tumblr();
        }                                        
        return redirect($url);

    }

    /**
     *@decription - This is the function to add the episodes inside a show
     *@param - Request object, $showSheduleId schedule id of the show and the date for the episode to be saved
     */
    public function add($showSheduleId=null, $date=null, $day=null, Request $request){

        //Fetch all channels
        $media_model =  new MediaLinking();
        $media = $media_model->get_media(0, 'Episode');
        $channels     = new Channel();
        $all_channels = $channels->getAllChannels();
		$assigned_categories = array();

        // Fetch schedule data
        $schedule = Schedule::with('show')->where('id', $showSheduleId)->first(); 
		$show = Show::with('shows_has_categories')->where('id',$schedule->show->id)->first();
		if($show->shows_has_categories->count() > 0){
            foreach($show->shows_has_categories as $cat){
                $assigned_categories[] = $cat->categories_id;
            }
        }
        // Fetch all Categories for filters
        $categories = new Category();
        $all_categories = $categories->getCategoriesForFilter('show', false, 0, false); 

        // $weeks = array(
        //             '0' => 'each week',
        //             '1' => '1st week',
        //             '2' => '2nd week',
        //             '3' => '3rd week',
        //             '4' => '4th week',
        //             '5' => '5th week'
        //         );

        // $months = array(
        //             '0'  => 'each month',
        //             '1'  => 'January',
        //             '2'  => 'February',
        //             '3'  => 'March',
        //             '4'  => 'April',
        //             '5'  => 'May',
        //             '6'  => 'June',
        //             '7'  => 'July',
        //             '8'  => 'August',
        //             '9'  => 'September',
        //             '10' => 'October',
        //             '11' => 'November',
        //             '12' => 'December'
        //         );

        // $years = array(
        //             '0'        => 'each year',
        //             date('Y')  => date('Y'),
        //             date('Y', strtotime('+1 year'))  => date('Y', strtotime('+1 year')),
        //             date('Y', strtotime('+2 year'))  => date('Y', strtotime('+2 year'))
        //         );

        // $minutes = array();
        // for($i=45;$i<1440;$i++){
        //     $minutes[$i] = $i; 
        // }

        $years=array();    
        $currYear = date('Y');
        $year = '2016'; // stast started on 2016 
        while ($year <= $currYear) {
            $years[$year] = $year;
            $year++;
        }
        $d= '1'; // days of month
        while ($d <= 31) {
            $days[$d] = $d;
            $d++;
        }
        $hr= '1'; // days of month
        while ($hr <= 12) {
            $hours[$hr] = $hr;
            $hr++;
        }
        $nextyears = array($currYear=>$currYear,$currYear+1=>$currYear+1);
        $mints = array('00'=>'00','15'=>'15','30'=>'30','45'=>'45');
        $months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        $meridian  = array('am' =>'am', 'pm'=>'pm');

        //Fetch shows to override
        $shows = new Show();
        $all_shows = $shows->getShowsForFilter();
        //$all_shows->put('', 'All Shows');
       	
        if($request->isMethod('post') || $request->isMethod('put')){
            
            $data        = $request->input('data');  
            $episode     = new Episode();
            $response    = $episode->add($data);
            if($response){
                $message = "New episode successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the episode. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);

            return view('backend/show/add_episode')->with(compact('all_channels','years', 'months','days','hours','nextyears','mints','months','meridian', 'schedule', 'date', 'day', 'all_categories', 'all_shows','response','assigned_categories','media'));
            // return redirect('/admin/show/schedules/agenda');
        }
        
        return view('backend/show/add_episode')->with(compact('all_channels', 'years', 'months','days','hours','nextyears','mints','months','meridian', 'schedule', 'date', 'day', 'all_categories', 'all_shows','assigned_categories','media'));
    }

    /**
     *@decription - This is the function to view the episode detail
     *@param - Request object, $id schedule id of the episode
     */
    public function view_agenda($id = null){
        return view('backend/show/view_episode');
    }

    /**
     *@decription - This is the function to add the episodes inside a show
     *@param - Request object, $showSheduleId schedule id of the show and the date for the episode to be saved
     */
    public function add_encore($showSheduleId = null, $date = null, $episodeId =null, Request $request){
        $episode = array();
        $shedule = " ";
        if($episodeId != 0){
            $episodeObj = new Episode();
            $episode    = $episodeObj->getEpisodeById($episodeId);
        }
        if($showSheduleId != 0){
            $schedule = Schedule::find($showSheduleId);
        }
        
        if($request->isMethod('post') || $request->isMethod('put')){
             
            $data = $request->input('data'); 
            //echo "<pre>";print_R($data);die;
            $episode = new Episode();
            
            //$response = $episode->editScheduledShare($episodeId,$data);
            $response = $episode->addEncore($episodeId,$data);

            if($response){
                $message = "Encore has been added successfully.";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the encore. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/show/schedules/agenda');
        }else{
            return view('backend/show/add_encore')->with(compact('episode','schedule','date','showSheduleId'));
        }
        
    }

    /**
     *@decription - This is the function to add the episodes schedule
     *@param - Request object, 
     */
    public function episode_schedule(Request $request){     
        $data = $request->input('data'); 
        $episodeObj = new Episode();
        $episode    = $episodeObj->episode_schedule($data);  
        if($episode){
            Session::flash('success' , 'Schedules has been added successfully.');
            return redirect('admin/show/schedules/agenda?channel_id='.$data['EpisodeScheduledShare']['channel_id']);
        }else{
            Session::flash('error' , 'Somthing Wrong.');
            return redirect('admin/show/schedules/agenda?channel_id='.$data['EpisodeScheduledShare']['channel_id']);
        }
        
    }

    /**
     *@decription - This is the function to add the episodes schedule
     *@param - Request object, 
     */
    public function ajax_episode_schedule(Request $request){   
            $data = $request->input('data'); 
            //echo"<pre>";print_r($data);die;
            $episodeObj = new Episode();
            $episode    = $episodeObj->episode_schedule_twitter($data);
            if($episode){
               return 'success';
            }else{
                return 'error';
            }
 
    }
        
    /**
     * Get all scheduled shows of current episode show  (! Channel.Schedule ) used
     * @param type $data
     */
    private function setDataForUpcomingShowsSelect($channelId=null, $data=array()) {
    
        if ($data->date > date('Y-m-d') || ($data->date == date('Y-m-d') && $data->time > date('H:i:s'))) {  

            // only if Episode date/time in the future        	  
            $schedule    = new Schedule();
        	$shows       = $schedule->getSchedulesOfEpisodeShow($data->shows_id);
    		$currDate    = date('Y-m-d');
            $currTime    = date('H:i:s');
            $episodeDate = $data->date;
            $episodeTime = $data->time;

            // three months ahead
            $weeks = array(date('Y-m-d'), date('Y-m-d', strtotime('+1 week')),
                    date('Y-m-d', strtotime('+2 weeks')),
                    date('Y-m-d', strtotime('+3 weeks')),
                    date('Y-m-d', strtotime('+4 weeks')),
                    date('Y-m-d', strtotime('+5 weeks')),
                    date('Y-m-d', strtotime('+6 weeks')),
                    date('Y-m-d', strtotime('+7 weeks')),
                    date('Y-m-d', strtotime('+8 weeks')),
                    date('Y-m-d', strtotime('+9 weeks')),
                    date('Y-m-d', strtotime('+10 weeks')),
                    date('Y-m-d', strtotime('+11 weeks')),
                    date('Y-m-d', strtotime('+12 weeks')),
                    date('Y-m-d', strtotime('+13 weeks')),
                    date('Y-m-d', strtotime('+14 weeks')),
                    date('Y-m-d', strtotime('+15 weeks')),
                    date('Y-m-d', strtotime('+16 weeks')),
                    date('Y-m-d', strtotime('+17 weeks')),
                    date('Y-m-d', strtotime('+18 weeks')),
                    date('Y-m-d', strtotime('+19 weeks')),
                    date('Y-m-d', strtotime('+20 weeks')),
                    date('Y-m-d', strtotime('+21 weeks')),
                    date('Y-m-d', strtotime('+22 weeks')),
                    date('Y-m-d', strtotime('+23 weeks')),
                    date('Y-m-d', strtotime('+24 weeks'))
            );

            $weeklyScheduledShows = array();
            foreach ($weeks as $weekDate) {

                // Re-structure & apply real dates (not days of week)
                $scheduledShows = array();
                if(count($shows) > 0){
	                foreach($shows as $s) {	 
	                	if ($s instanceof \App\Models\Backend\Schedule) {	                		 
	                		$dows = explode(",", $s->dow);
	                		foreach($dows as $d){
	                			$date = $this->day2date($d, $weekDate);
			                    if ($date > $currDate || ($date == $currDate && $s->time > $currTime )) {
			                        $scheduledShows[$s->channels_id][$date][$s->time][] = $s;
		                    	}
	                		}
	                	}
	                }
	            }

                // handle NOT weekly repeated scheduled shows ( Xnd week of the month, etc...)
                if(!empty($scheduledShows)){
                	foreach ($scheduledShows as $_channelId => $channelScheduledShows) {
	                    foreach ($channelScheduledShows as $date => $timeData) {
	                        foreach ($timeData as $time => $data) {
	                            $showd = (array)$schedule->getScheduledShow($data, $date);

                                $prefix = chr(0).'*'.chr(0);
                                $showd = isset($showd[$prefix.'attributes'])?(object)$showd[$prefix.'attributes']:(object)array();
                                /*echo '<pre>';
                                print_r($showd);
                                die;*/
	                            if ( isset($showd->shows_id) ) {
	                            	$show_id = $showd->shows_id;	                            	
	                            	//Get the episode count for that particular date,time and particular show id   
	                            	$episode  = new Episode();
	                            	$any = $episode->getEpisodeCountByDateTimeShow($date, $time, $show_id);
	                                // check is there any episode assigned for this schedule
	                                if (!$any) {
	                                    $weeklyScheduledShows[$_channelId][$date][$time] = $showd;
	                                } else {
	                                	$showd->scheduled  = true;
	                                    $weeklyScheduledShows[$_channelId][$date][$time] = $showd;
	                                }
	                            }
	                        }
	                    }
	                }
                }            
            }
            
            // preapre data for select-box

            $scheduledShowsAhead = array('0' => '[ Select new date and time ]');
            foreach ($weeklyScheduledShows as $_channelId => $scheduledShows) {
                foreach ($scheduledShows as $date => $timeData) {  
                    foreach ($timeData as $time => $data) {                    	
                        if ($data->channels_id == $channelId) {
                            $scheduledShowsAhead[$date.'.'.$time.'.'.$data->channels_id.'.'.$data->id] = date("D", strtotime($date)).' - '.date("m/d/Y", strtotime($date)).' '.date("g:i a", strtotime($time)) . (isset($data->scheduled)?' (Episode scheduled)':'');     
                        } else {

                        }
                    }
                }
            }

            ksort($scheduledShowsAhead);

            // singularize live episodes at this smae date/time, but on different channels
            $scheduledShowsAhead = array_unique($scheduledShowsAhead);
            return $scheduledShowsAhead;
        }
    }

     /**
     * Convert week day name to MySQL date relative to given date / Today 
     * 
     * @param String $day
     * @param String $referenceDate     reference date value (Now by default) 
     */
    private function day2date($day = 'Mon', $refrerenceDate = null, $format = "Y-m-d", $firstDayOfWeek = 'Sunday') {

        if (!in_array($day, array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'))) {  
            // date given as "Y-m-d" 
            return $day;        
        }        
    
        if ($refrerenceDate === null) {
            $refrerenceDate = time();
        } else {
            $refrerenceDate = strtotime($refrerenceDate);
        }
        $days = array();
        if (date("l", $refrerenceDate) !== $firstDayOfWeek) {
            $d = strtotime('last ' . $firstDayOfWeek, $refrerenceDate);
        } else {
            $d = $refrerenceDate;
        }
        $days[date("D", $d)] = date($format, $d);
        for ($i = 0; $i < 6; $i++) {
            $d = strtotime('+1 day', $d);
            $days[date("D", $d)] = date($format, $d);
        }
        return $days[ucwords(strtolower($day))];
    }

     /**
     * Reschedule the upcoming episode
     * 
     * @param String $day
     * @param String $referenceDate reference date value (Now by default) 
     */
    public function reschedule_episode(Request $request){
    	
    	$schedule_data = $request->data;
    	$data_arr      = explode('.', $schedule_data);
    	$new_date      = $data_arr[0];
    	$new_schedule  = $data_arr[3];
    	$episode       = Episode::find($request->id);
    	$episode->date = $new_date;
    	$episode->schedules_id = $new_schedule;
    	$update = $episode->update();
    	if($update){
    		return array('status'=>'success', 'date'=>date('m/d/Y', strtotime($new_date)));
    	}else{
    		return array('status'=>'error');
    	}
    }

    /**
     * Fetch the encore details of the episode
     * @param $request is the object of the Request object
     */
    public function encores_details(Request $request) {

        $episode_id = '';
        $episode_id = $request->episode_id;
        $data = array();
        if(!empty($episode_id)){
           
            $episode = Episode::with('show')->where('id', $episode_id)->first();
            if($episode){
                $data['episode'] = $episode->title;
                if($episode->show){
                    $data['show'] = $episode->show->name;
                }else{
                    $data['show'] = '';
                }
                 
                // orginal Episode premiere date
                $data['premiereDateTime'] = date('m/d/Y', strtotime($episode->date)).' '.date('g:i:s A', strtotime($episode->time));

                // Encores dates and times
                $episode_obj = new Episode();
                $encores     = $episode_obj->getEncores($episode->id);
                $_encores    = array();
                if($encores->count() > 0){
                    foreach($encores as $encore) {
                        $_encores[] = date('m/d/Y', strtotime($encore->date)).' '.date('g:i:s A', strtotime($encore->time));
                    }
                    $data['encoresTxt'] = implode('<br>', $_encores);
                    return ['status'=>'success', 'data'=>$data];
                }  
            }
            return ['status'=>'error', 'data'=>''];
        }
        return ['status'=>'error', 'data'=>''];
    }
}
