<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Backend\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Backend\MediaLinking;
use DB;
use Auth;
use Session;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users      = new User();
        $group_id   = 0;
        $group_type = 0;
        if ($request->has('group')) {
            $group_id   = $request->input('group');
            $group_type = $group_id;
        }
        return view('backend/user/users')->with(compact('group_type'));
    }

    public function ajax_index(Request $request)
    {
        $users   = new User();      
        $draw    = 1;
        $start   = 0;
        $type    = 0;
        $length  = 10;
        $order   = '';
        $search  = '';
        $columns = '';
        $search_value = '';
        $search_regex = '';
        if($request->has('type')){
            $type       = $request->input('type');
        }if($request->has('start')){
            $start      = $request->input('start');
        }if($request->has('length')){
            $length     = $request->input('length');
        }if($request->has('draw')){
            $draw       = $request->input('draw');
        }if($request->has('order')){
            $order      = $request->input('order');
        }if($request->has('length')){
            $length     = $request->input('length');
        }if($request->has('search')){
            $search_arr = $request->input('search');
        }if($request->has('columns')){
            $columns    = $request->post("columns");
        }

        if(!empty($search_arr)){
            $search_value = $search_arr['value'];
        }if(!empty($search_arr)){
            $search_regex = $search_arr['regex'];
        }

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col   = $o['column'];
                $dir   = $o['dir'];
                $order = $columns[$col]['name'];
            }
        }
     
        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }    

        $all_users   = $users->getAllUsers($type, $start, $length, $order, $dir, null, $search_value);
        $count_users = $users->countUsers($type, $order, $dir, null, $search_value);
       
        $data = array();
        $i = 0;   
        $user_type = "'user'";
        foreach($all_users as $user){
            $data[$i][]  = $user->title." ".$user->firstname;
            $data[$i][]  = $user->lastname." ".$user->sufix;
            $data[$i][]  = $user->email;
            $data[$i][]  = date('d M, Y  g:i a', strtotime($user->last_login));
            if($user->status == '1'){
                $data[$i][] = '<button class="btn btn-success disabled waves-effect waves-light btn-xs m-b-5">Active</button>';
            }else if($user->status == '0'){
                $data[$i][] = '<button class="btn btn-danger disabled waves-effect waves-light btn-xs m-b-5">Totally Inactive</button>';
            }else if($user->status == '2'){
                $data[$i][] = '<button class="btn btn-warning disabled waves-effect waves-light btn-xs m-b-5">Login Inactive</button>';
            }

            if($user->groups_id == 2){
                if(count($user->hosts)){
                    $data[$i][]  = '<a href="'.url('admin/user/hosts/edit/'.$user->hosts[0]->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a><a href="javascript:void(0)" title="Delete" onclick="deleteUser('.$user->id.', this, '.$user_type.')" ><i class="glyphicon glyphicon-trash"></i></a>';
                    
                }else{
                    //If a host type but its info. is not settle down in the host table then consider it as member, so that code will not break. 
                    $data[$i][]  = '<a href="'.url('admin/user/users/edit/'.$user->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a><a href="javascript:void(0)" title="Delete" onclick="deleteUser('.$user->id.', this, '.$user_type.')" ><i class="glyphicon glyphicon-trash"></i></a>';
                }               
            }else{
              
                $data[$i][]  = '<a href="'.url('admin/user/users/edit/'.$user->id ).'" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a><a href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open" title="Preview"></i></a><a href="javascript:void(0)" title="Delete" onclick="deleteUser('.$user->id.', this, '.$user_type.')" ><i class="glyphicon glyphicon-trash"></i></a>';
            }
            $i++;
        } 

        $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $count_users,
                    'recordsFiltered' => $count_users,
                    'data' => $data
                );

        echo json_encode($output);
        exit();        
    }

    /*public function hosts()
    {
        return view('backend/user/hosts');
    }

    public function partners()
    {
        return view('backend/user/partners');
    }

    public function members()
    {
        return view('backend/user/members');
    }

    public function activities()
    {
        return view('backend/user/activities');
    }*/
    
    // Add users
    public function add_user(Request $request)
    {   
        $media_model  =  new MediaLinking();
        $media        = $media_model->get_media(0, 'User');

        if ($request->isMethod('post')) {
            $data     = $request->input('data');
            $var      = "";
            $message  = "";
            $user     = new User();
            $response = $user->add($data);
            if($response){
                $message = "New User successfully created!";
                $var     = "success";
            }else{
                $message = "There is some problem while creating the user. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/users');
        }else{
            return view('backend/user/user_add',compact('media'));
        }
    }

    // Edit Users
    public function edit_user($id, Request $request)
    {
        $media_model =  new MediaLinking();
        $media = $media_model->get_media($id, 'User');

        $user = User::with('profiles')->with('user_settings')->where('id',$id)->first();
        if ($request->isMethod('put') || $request->isMethod('post')) {
            $update_data = $request->data;
            if (empty($update_data['User']['password'])) {
                unset($update_data['User']['password']);
            }
            $user = new User();
            $update = $user->edit($id, $update_data);
            if($update){
                $message = "The user details has been updated.";
                $var     = "success";
            }else{
                $message = "There is some problem while updating the user. Please try again.";
                $var     = "error";
            }
            Session::flash($var , $message);
            return redirect('admin/user/users');
        }else{
            return view('backend/user/user_edit', compact('user','media'));
        }
    }

    // Delete Users
    public function delete(Request $request){

        if ($request->isMethod('post')) {
            $post_data =  $request->all();
           
            if(!empty($post_data)){
                $user_id = $post_data['id'];
                $user = new User();
                $delete = $user->deleteUser($user_id);
                if($delete){
                    return array('status' => 'success');
                }else{
                    return array('status' => 'error');
                }
            }else{
                return array('status' => 'error');
            }
        }
    }
    // Export Users
    public function export_user(){ 
  
        $id = Auth::user()->id;
        $result = array();
        $users =DB::table('users')
                   ->where(function($q) {
                       $q->where('users.id', '!=', Auth::id())
                       ->where('users.deleted',0)
                       ->where('users.groups_id','!=',2);
                   })
                   ->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
                   ->select('profiles.firstname', 'profiles.lastname','users.email', 'profiles.phone', 'profiles.cellphone', 'profiles.skype', 'profiles.skype_phone')
                   ->orderBy('profiles.firstname', 'asc')
                   ->get()->toArray(); 

                    foreach ($users as $user) {
                        //modification
                        $result[] = (array)$user;  
                    } 

        return Excel::create('users', function($excel) use ($result) {
            $excel->sheet('user', function($sheet) use ($result)
            {
               $sheet->fromArray($result);
            });
        })->download('csv');
    }
}
