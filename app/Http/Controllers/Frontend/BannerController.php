<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Frontend\Banner;
use View;
use DB;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get()
    {
        $input                              = Input::get();
        $json                               = (int)filter_var($input['json'], FILTER_VALIDATE_BOOLEAN);
        $result                             = [];
        if(isset($input['sizes']) && isset($input['channel_id']))
        {
            foreach($input['sizes'] as $resolution => $limit)
            {
                $top_banner                 = get_banner($resolution, $limit, $input['channel_id']);
                if(!$json){
                    $target                 = '_blank';
                    $result[$resolution]    = banner_template($top_banner, $resolution, $target);  
                }
                else
                    $result[$resolution]    = $top_banner;
            }
        }
        echo json_encode($result);
    }
    public function redir($banner_id)
    {
        $banner     = Banner::find($banner_id);
        $res        = $banner->toArray();
        if($res)
        {
            // Update this banner clicks and redirect to the url
            $banner->clicks = $res['clicks'] + 1;
            $banner->save();

            if(!empty($res['url']))
                return redirect()->away($res['url']);
        }
    }
}
