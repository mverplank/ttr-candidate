<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Frontend\MediaLinking;
use App\Models\Frontend\ContentData;
use App\Models\Frontend\ContentCategory;
class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;
        
        /* Top Slider code starts here */
        $data['slider2']             = get_slider(64, 6, $channel_id, 'DESC');
        /* Top Slider code starts here */

        /* Top Slider code starts here */
        $data['slider_h2'] ='<h2>Enlightened Resources</h2>';
        $data['slider3']                = get_slider(21, 20, $channel_id, 'DESC');
        /* Top Slider code starts here */

        /* Banners code ends here */
        $resolution                     = '625x258';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['625x258']             = banner_template($top_banner, $resolution, $target);

        // $resolution                     = '120x300';
        // $limit                          = 12;
        // $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        // $target                         = '_blank';
        // $class                          = 'tiny-6 small-4 medium-2 large-1 col-md-1';
        // $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        $data['resolutions']            = 'sizes[728x90]=1&sizes[120x300]=12';

        $data['companyhistory'] ='<h2>Company History</h2><div><p>The Transformation Radio Network was created by Dr. Pat Baccili, notable author and researcher on cultivating respect and trust in the workplace, certified career and media coach, CEO of a successful motivation company, inspirational speaker, and the host of the internationally acclaimed, The Dr. Pat Show &ndash; Talk Radio to Thrive By.</p><p>&nbsp;</p><p><strong>LIVE LIFE FULL OUT!</strong></p><p>&nbsp;</p><p>Prior to the start of The Transformation Radio Network, Dr. Pat Baccili launched Crust Busting and The Dr. Pat Show as international brands in 2003. She continued her work to spread positivity and to change lives by creating the International Positive Radio Tour from 2007-2009. Dr. Pat defined positive as the &ldquo;New Mainstream in Media&rdquo;.</p><p>&nbsp;</p><p>Her drive to impact the lives of a broader audience as well as offering individuals the chance to contribute to positive change led her to begin: The Transformation Radio Network.</p><p>&nbsp;</p><p>With its initial launch in 2010, the network featured 30 hosts and has since been providing a platform for a growing number of hosts and co-hosts to broadcast their message. By 2015, the network increased syndication and added additional channels to diversify content and meet the needs of every listener; from sexuality to spirituality and everything in between.</p><p>&nbsp;</p><p>Transformation Talk Radio Network is distributed through 90 digital outlets and mobile apps reaching audiences from all over the world and continues to grow and expand. To make the content even more accessible to listeners who want to transform their lives, a Transformation Talk Radio app has been released on Apple and Android. It allows people to listen in live or search through the archives for a specific show they want to listen to anywhere they go.</p><p>&nbsp;</p><p>The Transformation Radio Network goes beyond radio because it allows hosts to increase their visibility and media presence, supported by an award-winning production team. Sending out messages of positivity and transformation, as well as connecting people through radio broadcasting is just one of the few things that The Transformation Radio Network specializes in.</p></div>';

        return view('frontend.history',with($data));
    }
}
