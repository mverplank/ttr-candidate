<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

class PositiveMediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;


        $data['dont_want_to_miss'][]    = get_slider(63, 6, $channel_id, 'ASC');
        $data['hot_off_the_press'][]    = get_slider(3, 10, $channel_id, 'DESC');

        /* Featured Section code starts here */
        $data['spesial_offers'][]       = get_slider(14, 10, $channel_id, 'ASC');
        $data['featured_articles'][]    = get_slider(42, 6, $channel_id, 'ASC');

        /* */
        /* Testimonials code starts here */
        $cat_id                         = [67, 68];
        $limit                          = [6, 6];
        $order                          = 'DESC';
        foreach($cat_id as $k => $v){
            $data['testimonials'][]           = get_slider($v, $limit[$k], $channel_id, $order);
        }
        /* Testimonials code ends here */

        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target);
        /* Banners code starts here */
        return view('frontend.positive_media.positive_media')->with($data);
    }

    public function events()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Blogs code starts here */
        $data['events']                  = get_slider(1, 20, $channel_id, 'ASC', false);
        /* Blogs code starts here */

        return view('frontend.positive_media.events')->with($data);
    }

    public function resources()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Top Slider code starts here */
        $data['top_slider']           = get_slider(62, 6, $channel_id, 'DESC', false);
        /* Top Slider code starts here */

        /* Resources code starts here */
        $data['resources']              = get_slider(5, 20, $channel_id, 'ASC', true, 'title');


        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 12;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-1 col-md-1';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        $data['resolutions']            = 'sizes[728x90]=1&sizes[120x300]=12';
        /* Banners code starts here */
        return view('frontend.positive_media.resources')->with($data);
    }

    public function blog()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Blogs code starts here */
        $data['blogs']                  = get_slider(44, 100, $channel_id, 'DESC', false);
        /* Blogs code starts here */

        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-2 col-md-2';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        /* Banners code starts here */
        return view('frontend.positive_media.blog')->with($data);
    }

    public function testimonials()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Top Slider code starts here */
        $data['top_slider']           = get_slider(60, 6, $channel_id, 'DESC', false);
        /* Top Slider code starts here */

        /* Testimonials code starts here */
        $data['what_clients_are_saying']           = get_slider(68, 20, $channel_id, 'DESC');
        $data['what_listeners_have_to_say']        = get_slider(67, 15, $channel_id, 'DESC');
        /* Testimonials code ends here */

        /* Testimonials From Network Guests starts here */
        $data['testimonials_from_network_guests']  = get_slider(4, 100, $channel_id, 'ASC');
        /* Testimonials From Network Guests ends here */

        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-2 col-md-2';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        /* Banners code starts here */
        return view('frontend.positive_media.testimonials')->with($data);
    }
}
