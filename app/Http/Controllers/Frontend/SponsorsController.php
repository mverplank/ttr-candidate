<?php

namespace App\Http\Controllers\Frontend;
use App\Models\Frontend\Sponsor;
use Illuminate\Http\Request;
use View;

class SponsorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Fetch the channel id (depends upon on the url or set it into session)
        $channelId = 2; //For now set it 2 by default.

        $page   = 0;
        $limit  = 20;
        $offset = 0;
        if(request()->has('page') && request()->filled('page'))
        {
            $page     = (int)request()->page + 1;
            $offset   = $page * $limit;
        }
        $data['ajax'] = 0;
        if(request()->has('ajax'))
        {
            $data['ajax'] = request()->input('ajax');
        }

        //Fetch sponsors of that channel
        $sponsorObj     = new Sponsor();
        $sponsors       = $sponsorObj->getSponsors($channelId, $offset, $limit);
        $count_sponsors = $sponsorObj->countSponsors($channelId);
      
        $data['count_sponsors'] = $count_sponsors;
        $data['sponsors']       = $sponsors;
        
        $route                      = route('front_sponsors');
        $data['route']              = route('front_sponsors');
        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }
        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page'] = 1;
            }
            $queryString['m']       = 'sponsor';
            $sponsors->appends($queryString);
            $view                   = View::make('frontend.sponsors.sponsors_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = View::make('frontend.ajax_pagination', ['paginator' => $sponsors, 'page_type'=>'sponsor', 'route' => $route]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            
            return view('frontend.sponsors.sponsors')->with($data);
        }
    }

    /**
     * Show the sponsors info page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function become_sponsor(Request $request)
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Top Slider code starts here */
        $data['top_slider']             = get_slider(60, 6, $channel_id, 'DESC', false);
        /* Top Slider code starts here */

        /* Resources code starts here */
        $data['videos']                 = get_slider(8, 1, $channel_id, 'ASC', true, 'title');
        $data['highlights']             = get_slider(15, 1, $channel_id, 'ASC');


        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-2 col-md-2';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        /* Banners code starts here */
        return view('frontend.sponsors.become_sponsor')->with($data);
    }
}
