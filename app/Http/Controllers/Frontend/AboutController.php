<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Frontend\MediaLinking;
use App\Models\Frontend\ContentData;
use App\Models\Frontend\ContentCategory;
class AboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;
        
        /* Top Slider code starts here */
        $data['top_slider']             = get_slider(81, 6, $channel_id, 'DESC', false);
        /* Top Slider code starts here */

        /* Top Slider code starts here */
        $data['outlets']                = get_slider(12, 100, $channel_id, 'DESC');
        /* Top Slider code starts here */

        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 12;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-1 col-md-1';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        $data['resolutions']            = 'sizes[728x90]=1&sizes[120x300]=12';
        /* Banners code starts here */

        //$aboutcontent = array();
        $data['ourvision'] ='<h2><em>Our Vision is to set the standard for a new and fresh kind of Talk Radio,creating conversations that are transforming the world, one listener at a time!!</em></h2>';

        $data['ourmission'] ='<p>Our Mission is to broadcast a distinctive blend of live talk radio interviews with a mix of uplifting and intelligent news, educational and practical information. Topics range from personal development to critical issues relevant to a rapidly changing world. As Dr. Pat says, "we talk about everything from sex to spirituality with a vibration that honors the dignity of the human spirit."</p><p>At The Transformation Network we believe we exist because of &lsquo;divine intervention&rsquo; and this belief is the foundation of everything we do here at the network. In 2003 <a href="http://www.thedrpatshow.com/my-story.html" target="_blank" rel="noopener noreferrer"><strong>Dr. Pat</strong> </a>dialed a wrong phone number and rather than hang up, she listened to her intuition and said &lsquo;YES&rsquo; to the unexpected offer that the Universe presented her that day. It was in that moment she purchased her first hour of air time and the Dr. Pat show was created and has continued to catapult into what is now a syndicated #1 hit radio show heard around the world and the conception of our new network; The Transformation Network which is broadcast globally on <a href="#"><strong>hundreds of AM/FM/Digital Radio stations</strong></a></p><p>&nbsp;</p><p>As the world shifts through these remarkable and fascinating times we are absolutely feeling it here at The Transformation Network as we are experiencing epic expansiveness at an unprecedented pace. This speaks volumes about the shift in our human consciousness as our desire for positive messages from all around the world increases.</p><p>&nbsp;</p><p>At The Transformation Network you will find some of the world&rsquo;s most preeminent visionaries, change makers, best-selling authors, motivational speakers, leading-edge scientists, futurists, world-renowned spiritual leaders, environmentalists, educators, inventors, filmmakers, artists, mystics, and healers that are stimulating and supporting individual and collective growth, positive cultural shifts, and making a meaningful difference in the world. There is a place for you here.</p><h2>&nbsp;</h2></em>';

        $data['yourvision'] ='<p style="font-size: 130%; text-align: center;"><strong><em>If your vision is aligned with ours and you want your message to be part of the collective consciousness that is changing the world <a href="#"><u>we invite you to join us.</u></a> </em></strong></p><p>&nbsp;</p><p>We have created a state of the art radio platform along with a ground breaking media coaching and radio mentoring program to provide you with everything you need to share your message and work with the world while promoting yourself and as leading expert in your industry.</p><p>&nbsp;</p><p>Statistics tell us that over 143 Million people tune into online radio every month and this number is increasing faster than ever before. Please <a href="#"><strong>come join us and be at the forefront of this phenomenon.</strong></a></p><p>&nbsp;</p><p>We believe there are no coincidences. If you have found your way to this message, say &lsquo;YES&rsquo; to your intuition and join us as part of our listening family or our host family because, together, we can co-create a better future for the planet.</p><p>&nbsp;</p><h2 style="font-size: 200%; text-align: center;"><a href="'.Route('company_history').'" target="_blank" rel="noopener">Read our company history here.</a></h2>';

        $data['ourlisteners'] ='<h1 class="box-title">Our Listeners</h1><p><strong>Transformation Talk Radio</strong> is designed to attract the Cultural Creatives, a mass multi-generational cultural and lifestyle market unparalleled in U.S. history. Approximately one of every four adult Americans, some 50 million people in the United States alone have the worldview, values and lifestyle of the Cultural Creatives, with about 80-90 million in the European Union. These individuals are now instituting a wave of progressive social, environmental, spiritual, and economic change. But their power as a consumer market remains virtually untapped. Lifestyles of Health and Sustainability (LOHAS) describes a conservatively estimated $228.9 billion and growing U.S. marketplace for goods and services focused on health, the environment, social justice, personal development and sustainable living.</p><h2><strong>Find out more about our reach, stats, and listeners by <em><span style="text-decoration: underline;"><a href="#" target="_blank" rel="noopener">downloading our media kit here!</a></span></em></strong></h2>';

        //$catid =ContentCategory::where('id', '=', 81)->first();
        /*$contentdata =ContentData::where('content_categories_id', '=', 81)->first();
        $media = MediaLinking::where('module_id', '=', $contentdata->id)
                            ->where('main_module', '=', 'ContentData')
                            ->get();           
        $silder1 =ContentData::where('content_categories_id', '=', 12)
                    ->leftJoin('media_linking', function ($join) {
                             $join->on('media_linking.module_id', '=', 'content_data.id')
                            ->where('media_linking.main_module', '=', 'ContentData')
                            ->where('media_linking.sub_module', '=', 'images');
                        })->leftJoin('media', 'media_linking.media_id', '=', 'media.id')->limit(100);
        $silder1= $silder1->get();*/

        return view('frontend.about',with($data));
        //return view('frontend.about',compact('media','silder1','aboutcontent'));
    }
}
