<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Frontend\Media;
use Illuminate\Support\Facades\Input;
use Storage;
class DownloadController extends Controller
{
    //
    public function files($id, $type, $filename) {
        $file = storage_path().'/app/public/media/'.$id.'/'.$type.'/'.$filename;
        $name = basename($file);
        ob_end_clean();
		$headers = array(
		    'Content-Type: inline',
		);
		return response()->download($file, $name, $headers);
    }
}
