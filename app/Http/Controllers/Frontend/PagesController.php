<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    }

    public function affiliates()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Blogs code starts here */
        $data['affiliates']                  = get_slider(12, 100, $channel_id, 'ASC', false);
        /* Blogs code starts here */
        
        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-2 col-md-2';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        return view('frontend.pages.affiliates')->with($data);
    }

    public function contact_us()
    {
        return view('frontend.pages.contact');
    }
}