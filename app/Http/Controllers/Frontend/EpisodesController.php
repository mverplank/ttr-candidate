<?php

/**
 * @author:Contriver
 * @since : 01-05-2019 12:00:20 
 */

namespace App\Http\Controllers\Frontend;
use App\Models\Frontend\Archive;
use App\Models\Frontend\Channel;
use App\Models\Backend\Show;
use App\Models\Backend\Host;
use App\Models\Backend\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use View;

class EpisodesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $show_id     = '';
        $host_id     = '';
        $year        = '';
        $month       = '';
        $category_id = '';
        $search      = '';

        if($request->has('channel_id')){
            $channelId = $request->channel_id;
        }else{
            //Fetch the channel id (depends upon on the url or set it into session)
            $channelId = 2; //For now set it 2 by default.
        }
        
        //$channelId = $this->Session->read('Settings.domainTheme.current.modelData.Channel.id');
        
        if ( $channelId == 1 ) {
            $channelId = '';
        }
        //Check show id
        if($request->has('show_id')){
            $show_id = $request->show_id;
        }
        //Check host id
        if($request->has('host_id')){
            $host_id = $request->host_id;
        }
        //Check year
        if($request->has('year')){
            $year = $request->year;
        }
        //Check month
        if($request->has('month')){
            $month = $request->month;
        }
        //Check category id
        if($request->has('cat_id')){
            $category_id = $request->category_id;
        }
        //Check search
        if($request->has('search')){
            $search = $request->search;
        }

        $page   = 0;
        $limit  = 20;
        $offset = 0;
        if(request()->has('page') && request()->filled('page'))
        {
            $page     = (int)request()->page + 1;
            $offset   = $page * $limit;
        }
        $data['ajax'] = 0;
        if(request()->has('ajax'))
        {
            $data['ajax'] = request()->input('ajax');
        }


        //Check all the conditions later
        /*$defaultFilters = !$this->request->isAjax() &&
            !isset($this->request->query['q']) && !isset($this->request->data['Episode']['q']) &&
            (!isset($this->request->params['tag']) && !isset($this->request->params['hst'])) ? array('f' => array(
                'Show.channels_ids' => $channelId                
            )) : array();*/

        $defaultFilters = array('f' => array(
                'Show.channels_ids' => $channelId                
            ));

        //Check these conditions later
        /*if (isset($this->request->data['Episode']['q']) && !empty($this->request->data['Episode']['q'])) {

            $this->request->query['q'] = $this->request->data['Episode']['q'];

        }*/

        // For search results show also upcoming shows
        $onlyArchviesCondition = 'UNIX_TIMESTAMP(CONCAT(Archive.date, " ", Archive.time)) + Archive.duration*60 < UNIX_TIMESTAMP()';

        //Check these conditions later
        /*if (isset($this->request->query['q']) && !empty($this->request->query['q'])) {
            $onlyArchviesCondition = false;
        }

        // View tag related episodes on routes must be defiend
        if (isset($this->request->params['tag'])) {

            $this->request->query['f']['Archive.tags'] = $this->request->params['tag'];
        }
        if (isset($this->request->params['hst'])) {

            $this->request->query['f']['Archive.host_ids'] = $this->request->params['hst'];
        }*/


        // when host selected from drop-down include in search results also episodes where selected host is co-host
        /*$conditions = array();        
        if (isset($this->request->query['f']['Archive.host_ids'])) {
    
           $_hostId = $this->request->query['f']['Archive.host_ids'];
           unset($this->request->query['f']['Archive.host_ids']);  
           // REQUIRED - otherwise filter is added with default settings
           $conditions[] = array('( FIND_IN_SET("'.$_hostId.'",Archive.host_ids) > 0 OR FIND_IN_SET("'.$_hostId.'",Archive.cohost_ids) > 0 )');

        }*/

        //
        // if custom podcasts channel podcasts-host uploaded episodes only 
        //
        /*$isCustomUploadPodcastsChannel = (int)$channelId===(int)Configure::read('Settings.custom_podcasts_channel_id') ? true: false;
        if ($isCustomUploadPodcastsChannel) {
            $conditions[] = ['Archive.is_uploaded_by_host' => 1];
            // do not narrow to channel's shows
            unset($defaultFilters['f']['Show.channels_ids']);
        }*/

        //Fetch archive episodes of that channel
        $data['selected_channel'] = 2;
        $data['selected_show']    = '';
        $data['selected_host']    = '';
        $data['selected_cat']     = $selected_cat = '';
        $data['selected_year']    = '';
        $data['selected_month']   = '';

        $archive = new Archive();
        $archived_episodes       = $archive->getArchivedEpisodes($channelId, $offset, $limit, $show_id, $host_id, $year, $month, $category_id, $search);
        $count_archived_episodes = $archive->countArchivedEpisodes($channelId);
        
        $data['count_archived_episodes'] = $count_archived_episodes;
        $data['archived_episodes']       = $archived_episodes;

        $channels = new Channel();
        $all_channels = $channels->getAllChannels();
        $all_channels->prepend('Any Channel', '');
        $data['all_channels'] = $all_channels;

        //Fetch all Shows
        $shows = new Show();
        $all_shows = $shows->getShowsForFilter();
        //$all_shows->put('', 'All Shows');
        $all_shows->prepend('All Shows', '');
        $data['all_shows'] = $all_shows;

        //Fetch all Host
        $hosts = new Host();
        $all_hosts = $hosts->getHostsForFilter();
        //$all_hosts->put('', 'All Hosts');
        $all_hosts->prepend('All Hosts', '');
        $data['all_hosts'] = $all_hosts;
        
        // Years
        $year = date('Y');
        $startYear = 2003;
        $years = ['' => 'All Years'];
        while ($year >= $startYear) {

            $years[$year] = $year;
            $year--;
        }
        $data['years'] = $years;

        // Months
        $months = array('' => 'All Months', 1 => 'January', 2 => 'February', 3 => 'March',
            4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
            9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
        $data['months']    = $months;
        $data['channel_id'] = $data['selected_channel'];

        // Fetch all Categories for filters
        $categories     = new Category();
        $all_categories = $categories->getCategoriesForFilter('show', true, $selected_cat, true);
        $data['all_categories'] = $all_categories;
        $route                  = route('front_podcasts');
        $data['route']          = route('front_podcasts');
        $data['page_type']      = 'archives';
        $queryString            = Input::query();

        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }
        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page']       = 1;
            }
            $queryString['m']       = 'archives';
            $archived_episodes->appends($queryString);
            $view                   = View::make('frontend.episodes.archives_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = View::make('frontend.ajax_pagination', ['paginator' => $archived_episodes, 'page_type'=>'archive', 'route' => $route]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            $data['archived_episodes'] = $archived_episodes->appends($queryString);
            return view('frontend.episodes.archives')->with($data);
        }
    }
}
