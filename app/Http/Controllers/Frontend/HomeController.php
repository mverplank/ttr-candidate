<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Frontend\Show;
use App\Models\Frontend\Schedule;
use App\Models\Frontend\Media;
use App\Models\Frontend\MediaLinking;
use View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application frontend.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /* Zapbox data starts here */
        $channel_id                 = 2;
        $data['channel_id']         = $channel_id;

        $data['today']                  = date("Y-m-d",strtotime('today'));
        $data['this_week']              = get_days('current');
        $data['upcoming']               = get_days('upcoming');

        $schedule                       = new Schedule();
        $host_id                        = 0;
        // Only get current day scheduled episodes/shows
        // So $start = $end = $data['today']
        $fetch_schedule                 = $schedule->getCalendarEvents($data['today'], $data['today'], $channel_id, $host_id);
        $data['fetch_schedule']         = $fetch_schedule['calendarEvents'];

        $ids                            = $fetch_schedule['calendarEvents']['ids'];
        $Media                          = new Media();
        $data['all_media']              = $Media->get_media($ids, 'Show', 'player');


        $cat_id                         = [14, 15];
        $limit                          = [15, 10];
        $column                         = 'ASC';
        foreach($cat_id as $k => $v){
            $data['slider'][]           = get_slider($v, $limit[$k], $channel_id, $column);
        }

        $cat_id                         = "featured_guests";
        $limit                          = 15;
        $data['leaders_of_the_month']   = get_slider($cat_id, $limit, $channel_id);

        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target);

        /*echo '<pre>';
        print_r($data['network_sponsors']);
        die;*/
        //$data['network_sponsors'] = get_slider($v, $limit, $channel_id);
        
        //

        /*echo '<pre>';
        print_r($data['slider']);
        die;*/
        /*print_r(array_values($mids));*/
        //$merged = call_user_func_array('array_merge', $mids);
        //$sids = array_column($mids, 'id', 'id');
        /*sort($merged);
        print_r($merged);
        echo '</pre>';
        die;*/
        /* Zapbox data ends here */
        return view('frontend.home')->with($data);

    }
}