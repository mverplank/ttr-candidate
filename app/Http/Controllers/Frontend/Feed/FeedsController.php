<?php

namespace App\Http\Controllers\Frontend\Feed;

use App\Http\Controllers\Controller;
use App\Services\Feed\FeedBuilder;
use Illuminate\Pagination\Paginator;

use Illuminate\Http\Request;
use DB;
use Artisan;

class FeedsController extends Controller
{
    private $builder;

    public function __construct(FeedBuilder $builder)
    {
        $this->builder = $builder;
    }

    //We're making atom default type
    public function getFeed($type = "atom")
    {
        if ($type === "rss" || $type === "atom") {
            return $this->builder->render($type);
        }
        
        //If invalid feed requested, redirect home
        return redirect()->home();
    }

    /**
     * Generate feed
     *
     * @param type $channelId
     * @param type $hostId
     * @param type $guestId
     * @param type $limit
     */
    public function itunes($channelId = null, $hostId = null, $guestId = null, $limit = null, $type = "rss") {
        if (function_exists('sys_getloadavg')) {
            $sysLoad = sys_getloadavg();
        } else {
            $sysLoad = 0;
        }

        // generate only if low server load (less than 50% in last five minutes)
        if ((float) $sysLoad[0] < 8.0 && (float) $sysLoad[1] < 4.0) {
            $timeLimit = 5;
            set_time_limit($timeLimit * 60); // max generate by 5 minutes
            if (ini_get('max_execution_time') < $timeLimit) {  // to be sure try to set maximum script execution time also here
                ini_set('max_execution_time', $timeLimit . 'M');
            }
            // force $limit
            //$limit = 50; // force limit to RECENT 50 podcasts
            return $r = $this->_generateData($channelId, $hostId, $guestId, $limit, 'archived', 'itunes'); // for itunes 
        } else { // too high load            
            throw new HttpException(503);
            return false;
        }
    }

    /**
     * Generate feed
     *
     * @param type $channelId
     * @param type $hostId
     * @param type $guestId
     *
     * @param Int $days           naumebr of days to look ahead
     */
    public function upcoming($channelId = null, $hostId = null, $guestId = null, $limit = null, $days = 365) {

      // disable
      //throw new ServiceUnavailableException('Service unavailable', 503);
      //return false;

        $timeLimit = 5;
        set_time_limit($timeLimit * 60); // max generate by 5 minutes
        if (ini_get('max_execution_time') < $timeLimit) {  // to be sure try to set maximum script execution time also here
            ini_set('max_execution_time', $timeLimit . 'M');
        }
        //$channelId = 2;
        $this->_generateData($channelId, $hostId, $guestId, $limit, 'upcoming', 'upcoming', $days); // for itunes always use archived feed
        //$this->set('itunes', true);

        $this->render('episodes');
    }

    /**
     * Generate archived episodes RSS feed
     */
    public function archived($channelId = null, $hostId = null, $guestId = null, $limit = null) {

        // disable
        //throw new ServiceUnavailableException('Service unavailable', 503);
        //return false;

        $timeLimit = 5;
        set_time_limit($timeLimit * 60); // max generate by 5 minutes
        if (ini_get('max_execution_time') < $timeLimit) {  // to be sure try to set maximum script execution time also here
            ini_set('max_execution_time', $timeLimit . 'M');
        }
        $this->_generateData($channelId, $hostId, $guestId, $limit);

        $this->render('episodes');
    }

    private function _generateData($channelId = null, $hostId = null, $guestId = null, $limit = null, $type = 'archived', $cacheId = null, $daysAhead = 7, $rsstype = "rss") {
        /*echo $daysAhead;
        die;*/
        $join = array();
        // Channel ID
        $channel = [];
        if ($channelId === null) {
            if(request()->has('channel'))
            {
                $channelId = request()->input('channel');
                $channel = DB::select('SELECT `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name` FROM `channels` AS `Channel`   WHERE `Channel`.`id` = '.$channelId.' LIMIT 1');
            }
        } else {
            $channel = DB::select('SELECT `Channel`.`id`, `Channel`.`name`, `Channel`.`description`, `Channel`.`deleted`, `Channel`.`is_online`, `Channel`.`domain`, `Channel`.`theme`, `Channel`.`stream`, `Channel`.`url_archived_episodes`, `Channel`.`seo_title`, `Channel`.`seo_keywords`, `Channel`.`seo_description`, `Channel`.`sam_dir_shows`, `Channel`.`sam_file_connecting`, `Channel`.`sam_min_show_duration`, `Channel`.`studioapp_enabled`, `Channel`.`studioapp_settings_json`, `Channel`.`is_missing_episodes_reminder`, `Channel`.`theme_name` FROM `channels` AS `Channel`   WHERE `Channel`.`id` = '.$channelId.' LIMIT 1');
        }

        // Host ID
        if ($hostId === null) {
            if(request()->has('host'))
            {
                $hostId = request()->input('host');
            }
        }

        // Guest ID
        if ($guestId === null) {
            if(request()->has('guest'))
            {
                $guestId = request()->input('guest');
            }
        }

        // Sponsor ID
        $sponsorId = null;
        if(request()->has('sponsor'))
        {
            $sponsorId = request()->input('sponsor');
        }

        // Show ID
        $showId = null;
        if(request()->has('show'))
        {
            $showId = request()->input('show');
        }

        // Category ID
        $categoryId = null;
        if(request()->has('category'))
        {
            $categoryId = request()->input('category');
        }

        // Tag ID
        $tagId = null;
        if(request()->has('tag'))
        {
            $tagId = request()->input('tag');
        }

        //
        // Prepare feed conditions
        //
        $host       = [];
        if ($hostId !== null) {
            $host = DB::select('SELECT *, CONCAT(`Profile`.`title`, " ", `Profile`.`firstname`, " ", `Profile`.`lastname`, " ", `Profile`.`sufix`) AS fullname FROM hosts Host LEFT JOIN users AS User ON User.id=Host.users_id LEFT JOIN profiles AS Profile ON Profile.users_id=Host.users_id LEFT JOIN host_itunes AS HostItune ON HostItune.hosts_id=Host.id WHERE Host.id=? LIMIT 1', [$hostId]);
        }
        $guest = [];
        if ($guestId !== null) {
            // to generate channel detials
            $guest = DB::select('SELECT *, CONCAT(`Guest`.`title`, " ", `Guest`.`firstname`, " ", `Guest`.`lastname`, " ", `Guest`.`sufix`) AS fullname FROM guests Guest WHERE Guest.id=? LIMIT 1', [$guestId]);
        }
        if ($sponsorId !== null) {
            $sponsor = DB::select('SELECT `Sponsor`.`id`, `Sponsor`.`created_at`, `Sponsor`.`updated_at`, `Sponsor`.`is_online`, `Sponsor`.`is_featured`, `Sponsor`.`type`, `Sponsor`.`title`, `Sponsor`.`name`, `Sponsor`.`description`, `Sponsor`.`url`, `Sponsor`.`social_urls_json`, `Sponsor`.`company`, `Sponsor`.`address`, `Sponsor`.`city`, `Sponsor`.`state`, `Sponsor`.`zip`, `Sponsor`.`email`, `Sponsor`.`skype`, `Sponsor`.`phone`, `Sponsor`.`cellphone`, `Sponsor`.`contact_name`, `Sponsor`.`contact_email`, `Sponsor`.`contact_phone`, `Sponsor`.`contact_skype`, `Sponsor`.`contact_cellphone`, `Sponsor`.`billing_name`, `Sponsor`.`billing_email`, `Sponsor`.`billing_phone`, `Sponsor`.`billing_skype`, `Sponsor`.`billing_cellphone`, `Sponsor`.`notes`, (TRIM(CONCAT( `Sponsor`.`name`, " (", TRIM(`Sponsor`.`company`), ")" ))) AS  `Sponsor__fullname` FROM `sponsors` AS `Sponsor`   WHERE `Sponsor`.`id` = '.$sponsorId.' LIMIT 1');
        }
        $show = [];
        if ($showId !== null) {
            $show = DB::select('SELECT `Show`.`id`, `Show`.`channels_ids`, `Show`.`deleted`, `Show`.`name`, `Show`.`description`, `Show`.`duration`, `Show`.`is_encore`, `Show`.`is_online`, `Show`.`is_featured`, `Show`.`file`, `Show`.`stream`, `Show`.`favorites`, `Show`.`is_created_by_host` FROM `shows` AS `Show`   WHERE `Show`.`id` = '.$showId.' LIMIT 1');
        }

        if ($categoryId !== null) {
            if ((int)$categoryId > 0) {
            } else {

                // get tag id for given name
                /*$cat= $this->Category->find('first', array('conditions' => array('LOWER(Category.name)' => strtolower($categoryId))));
                if (count($cat)>0) {
                    $conditions['Category.id'] = $cat['Category']['id'];
                } else {
                    // if tag not found - show nothing
                    $conditions['Category.id'] = 0;
                }*/
            }
            //$join = $this->Utils->generateHabtmJoin('Category', 'Show.Episode', 'LEFT');
        }

        if ($tagId !== null) {
            if ((int)$tagId > 0) {
                //$conditions['Tag.id'] = $tagId;
            } else {
                // get tag id for given name
                //$tag = $this->Tag->find('first', array('conditions' => array('LOWER(Tag.name)' => strtolower($tagId))));
                $tag = DB::select('SELECT `Tag`.`id`, `Tag`.`name`, `Tag`.`type` FROM `ttr`.`tags` AS `Tag`   WHERE LOWER(`Tag`.`name`) = "0" LIMIT 1');
                /*if (count($tag)>0) {
                    $conditions['Tag.id'] = $tag['Tag']['id'];
                } else {
                    // if tag not found - show nothing
                    $conditions['Tag.id'] = 0;
                }*/
            }
            //$join = $this->Utils->generateHabtmJoin('Tag', 'Show.Episode', 'LEFT');
        }
        //echo $limit;die;
        //var_dump($limit);
        if ($limit === null) {
            if(request()->has('limit'))
            {
                $limit = request()->input('limit');
            }
            else {
                $limit = 50;
            }

            if ($limit == 0) { // zero means unlimited
                $limit = null;
            }
        }
        else $limit = 0;
        //echo $limit;die;

        //
        // handle 2nd level cache
        //
        $feedId = 'episodes-' . ($cacheId === null ? $type : $cacheId) . ($channelId ? '-channel-' . $channelId : '') .
                ($hostId ? '-host-' . $hostId : '') . ($guestId ? '-guest-' . $guestId : '') .
                ($sponsorId ? '-sponsor-' . $sponsorId : '') . ($limit ? '-limit-' . $limit : '') .
                ($showId ? '-show-' . $showId : '') . ($categoryId ? '-cat-' . $categoryId : '') . ($tagId ? '-tag-' . $tagId : '') .
                '.xml';
        //echo $feedId;die('success');
        DEFINE('DS', DIRECTORY_SEPARATOR); 
        //$cacheDir = APP . 'webroot' . DS . 'files' . DS . 'cache' . DS . 'feed';
        //$cacheDir = storage_path().DS.'app'.DS.'public'.DS.'cache'.DS.'feed';
        $cacheDir = 'app'.DS.'public'.DS.'cache'.DS.'feed';
        $cacheDir = DS.'public'.DS.'cache'.DS.'feed';
        /* echo $cacheDir."<br>";
        echo DS."<br>";*/
        $cacheFile = $cacheDir . DS . $feedId;
        //echo $limit;die;
        //array('channelId' => $channelId, 'limit' => $limit, 'hostId' => $hostId, 'guestId' => $guestId, 'sponsorId' => $sponsorId);
        /*echo '<pre>';
        print_r($host);
        die;*/
        return $this->builder->render($host, $channel, $guest, $show, $hostId, $rsstype, $limit, $cacheFile, $type, array(), $channelId, $guestId, $daysAhead);

        //echo $cacheFile;die('  success');
        //Artisan::call('generate:feed');
        //Artisan::call('generate:feed', []);
        //die;
        // CACHE FILE
        /*
        if ($this->_cache && file_exists($cacheFile) && !isset($this->request->query['nocache'])) {                
            if (filemtime($cacheFile) + (int) Configure::read('Settings.rss_ttl') * 60 > time()) { // cache file is valid
                // get feed from cached file
                $this->Result->rss(file_get_contents($cacheFile));
                // execution stops above!
                   
            } else { // cache file outdated
             
                // if server load too high - don't generate new one - just serve outdated rss feed
                if (function_exists(sys_getloadavg)) {
                    $sysLoad = sys_getloadavg();
                } else {
                    $sysLoad = 0;
                }

                //$sysLoad = 0; // disable sysload check
                // check if server is not loaded too much
                if ((float) $sysLoad[0] > 8.0 || (float) $sysLoad[1] > 4.0) {

                    // get feed from cached file
                    $this->Result->rss(file_get_contents($cacheFile));
                    // execution stops above!
                } else {

                    // new feed will be generated - pass execution down
                }
            }
        }*/
        // set feed id
        //$this->set('feedId', $feedId);

        

        //return $this->builder->render($rsstype, $cacheFile, $type, $episodes);

        /*if (isset($this->request->query['nocache'])) {

          // disable CakePHP caches
          $this->response->disableCache();
        }

        return array('channelId' => $channelId, 'limit' => $limit, 'hostId' => $hostId, 'guestId' => $guestId, 'sponsorId' => $sponsorId);*/
    }
}