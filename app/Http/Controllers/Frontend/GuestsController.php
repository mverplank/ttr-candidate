<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Frontend\Guest;
use Illuminate\Support\Facades\Input;
use View;

class GuestsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       /* $page     = 0;
        $limit    = 20;
        $offset   = 0;*/
        $page         = Input::get('page', 1);
        $lastname     = Input::get('lastname', null);
        $perPage      = 20;
        $offset       = 0;
        $offset       = ($page * $perPage) - $perPage;
        $data['ajax'] = 0;

        $lastname = Input::get('lastname', null);
        // if(request()->has('page') && request()->filled('page'))
        // {
        //     $page     = (int)request()->page + 1;
        //     $offset   = $page * $limit;
        // }
        $data['ajax'] = 0;
        if(request()->has('ajax'))
        {
            $data['ajax'] = request()->input('ajax');
        }

        $guest  = new Guest();
        $guests = $guest->getAllGuests($lastname, $offset, $perPage, 'profiles.lastname');
        //echo "<pre>";print_R($guests);die;
        $count_guests                = $guest->countAllGuests($lastname, 'profiles.lastname');
        $total_guests                = 0;
        if($count_guests)
            $total_guests            = $count_guests;


        //$data['guests']    = $guests;
        //echo "<pre>";print_R($data);die;
        $route             = route('front_guests');
        $data['route']     = route('front_guests');
        $data['page_type'] = 'guests';
        $queryString       = Input::query();
        
        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }

        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page'] = 1;
            }
            $queryString['m']       = 'guests';
            //$guests->appends($queryString);
            $data['guests']         = $this->arrayPaginator($guests, $request, $total_guests, $perPage, $page, $queryString);
            $view                   = View::make('frontend.guests.guests_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = View::make('frontend.ajax_pagination', ['paginator' => $data['guests'], 'page_type'=>'guests', 'route' => $route, 'queryString' => $queryString]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            //$data['guests'] = $guests->appends($queryString);
            $data['guests']      = $this->arrayPaginator($guests, $request, $total_guests, $perPage, $page, $queryString);
            //echo "<pre>";print_R($data);die;
            return view('frontend.guests.guests')->with($data);
        }
    }

    public function arrayPaginator($array, $request, $total, $perPage, $page, $queryString)
    {
        //$page = Input::get('page', 1);
        //$perPage = 48;
        //$offset = ($page * $perPage) - $perPage;
        return new \Illuminate\Pagination\LengthAwarePaginator($array, $total, $perPage, $page,
            ['path' => $request->url(), 'query' => $queryString]);
        /*return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);*/

    }

    public function guest($slug = '', $id = null)
    {
        $guest_id = (int)$id;
        if(is_int($guest_id)){
            // Get show details if exists
            if (Guest::where('id', '=', $guest_id)->exists())
            {
                $data['channel_id']     = 2;
                $data['guest_id']       = $guest_id;
                $Show                   = new Guest();
                $data['guest_details']  = Guest::find($guest_id)->toArray();
                $data['guest_urls']     = get_guest_social_media_urls($guest_id);
                //$data['media_id']       = Show::find($show_id)->shows_has_medialinking;
                /*$param                  =   [
                                                $show_id,   // Show ID
                                                'show',     // Main Module
                                                0,         // Sub Module
                                                '120',      // width
                                                '300'       // height
                                            ];*/
                //echo implode(',', $param);die;
                
                //$param                  = implode(',', $param);
                // Show ID
                // Main Module
                // Sub Module
                // width
                // height
                $data['media_info']     = get_media($guest_id, 'Guest', 'photo');
                //echo '<pre>';
                //print_r($data['guest_details']);
                //print_r($data['guest_urls']);
                //print_r($data['hosts']);
                /*print_r($data['media_info']);
                die;*/
                //die;
                return view('frontend.guests.guest_details')->with($data);
            }
            else
                abort(404, 'Sorry, the show you are looking for could not be found.');
        }
        else
            abort(404, 'Sorry, the show you are looking for could not be found.');
        //echo $show_name;
        /*if(!empty($show_name))
        {
            $temp = explode('.', $show_name);
            if(!empty($temp))
            {
                $temp1      = explode(',', $temp[0]);
                if(!empty($temp)){
                    $show_id    = $temp1[1];
                    if(is_int($show_id)){
                        // Get show details
                    }
                }
            }
        }*/
    }
}
