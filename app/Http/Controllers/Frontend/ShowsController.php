<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Frontend\Show;
use App\Models\Frontend\Schedule;
use App\Models\Frontend\Media;
use App\Models\Frontend\MediaLinking;
use Illuminate\Support\Facades\Input;
use View;

class ShowsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;
        /* Blogs code starts here */
        $data['top_slider']             = get_slider(62, 6, $channel_id, 'DESC', false);
        /* Blogs code starts here */

        $data['featured_episodes']      = get_slider('featured_episodes', 10, $channel_id, 'DESC', false);
        $data['featured_hosts']         = get_slider('featured_hosts', 10, $channel_id, 'DESC', false);

        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);

        $resolution                     = '120x300';
        $limit                          = 6;
        $network_sponsors               = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $class                          = 'tiny-6 small-4 medium-2 large-2 col-md-2';
        $data['network_sponsors']       = banner_template($network_sponsors, $resolution, $target, $class);
        /* Banners code starts here */
        return view('frontend.shows.shows')->with($data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function featured_shows(Request $request)
    {
        $page                       = 0;
        $limit                      = 20;
        $offset                     = 0;
        if(request()->has('page') && request()->filled('page'))
        {
            $page                   = (int)request()->page + 1;
            $offset                 = $page * $limit;
        }
        $data['ajax']               = 0;
        if(request()->has('ajax'))
        {
            $data['ajax']               = request()->input('ajax');
        }

        $channel_id                 = 2;
        $shows                      = new Show();
        $all_featured_shows         = $shows->countAllFeaturedShows($channel_id);
        $featured_shows             = $shows->getAllFeaturedShows($channel_id, $offset, $limit);
        $upcoming_shows             = '';
        $this_week_shows            = '';

        $data['all_featured_shows'] = $all_featured_shows;
        $data['featured_shows']     = $featured_shows;
        $data['upcoming_shows']     = $upcoming_shows;
        $data['this_week_shows']    = $this_week_shows;
        $route                      = route('front_featured_shows');
        $data['route']              = route('front_featured_shows');

        

        /* */

        /* */

        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }
        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page'] = 1;
            }
            $queryString['m']       = 'show';
            $featured_shows->appends($queryString);
            $view                   = view::make('frontend.shows.featured_shows_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = view::make('frontend.ajax_pagination', ['paginator' => $featured_shows, 'page_type'=>'show', 'route' => $route]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            
            return view('frontend.shows.featured_shows')->with($data);
        }
    }

    public function schedule(Request $request)
    {
        $Channel_id                 = 2;
        $data['channel_id']         = $Channel_id;
        //$data['current_week']       = current_week();
        /*print_r($data['current_week']);
        $current_dayname = date("l");*/
        $data['start']              = date("Y-m-d",strtotime('sunday last week'));
        $data['today']              = date("Y-m-d",strtotime('today'));//die;
        $data['end']                = date("Y-m-d",strtotime('saturday this week'));

        $days = [];
        $days['Sun'] = $data['start'];
        $dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        for($i = 1; $i<6;$i++)
        {
            $days[$dayNames[$i]] = date("Y-m-d",strtotime($data['start']. " + $i days"));
        }
        $days['Sat'] = $data['end'];

        /*print_r($days);
        die;*/
        $data['days']   = $days;
        $start_date     = $data['start'];
        $end_date       = $data['end'];
        $channel_id     = $Channel_id;
        $schedule       = new Schedule();
        $host_id        = 0;
        $fetch_schedule = $schedule->getCalendarEvents($start_date, $end_date, $channel_id, $host_id);
        $data['fetch_schedule'] = $fetch_schedule['calendarEvents'];

        
        
        $ids = $fetch_schedule['calendarEvents']['ids'];
        /*$MediaLinking   = new MediaLinking();
        $all_media      = $MediaLinking->get_media($ids, 'Show', 'player');*/
        $MediaLinking       = new Media();
        $data['all_media']  = $MediaLinking->get_media($ids, 'Show', 'player');

        /*echo '<pre>';
        print_r($all_media);
        die;*/
        /*echo '<pre>';
        print_r($fetch_schedule['calendarEvents']);*/

        
        return view('frontend.shows.schedule')->with($data);
    }

    public function zapbox(Request $request)
    {
        /*echo '<pre>';
        print_r($_POST);
        die;*/
        $start;$end;
        $channel_id                 = Input::post('channel_id', 2);
        $type                       = Input::post('type', '');
        $param                      = Input::post('param', '');
        $data['type']               = $type;
        if($type == 'upcoming')
        {
            $start                  = date("Y-m-d",strtotime('sunday this week'));
            $end                    = date("Y-m-d",strtotime('saturday next week'));
            $data['upcoming']       = get_days('upcoming');
        }
        else
        {
            $start                  = date("Y-m-d",strtotime('sunday last week'));
            switch($param):
                /*case 'sun': 
                            $start              = date("Y-m-d",strtotime($start. " + 1 days"));
                            break;*/
                case 'mon': 
                            $start              = date("Y-m-d",strtotime($start. " + 1 days"));
                            break;
                case 'tue': 
                            $start              = date("Y-m-d",strtotime($start. " + 2 days"));
                            break;
                case 'wed': 
                            $start              = date("Y-m-d",strtotime($start. " + 3 days"));
                            break;
                case 'thu': 
                            $start              = date("Y-m-d",strtotime($start. " + 4 days"));
                            break;
                case 'fri': 
                            $start              = date("Y-m-d",strtotime($start. " + 5 days"));
                            break;
                case 'sat': 
                            $start              = date("Y-m-d",strtotime($start. " + 6 days"));
                            break;
            endswitch;
            $end = $start;
        }
        //die;
        //$data['current_week']       = current_week();
        /*print_r($data['current_week']);
        $current_dayname = date("l");*/
        /*$data['start']              = date("Y-m-d",strtotime('sunday last week'));
        $data['today']              = date("Y-m-d",strtotime('today'));//die;
        $data['end']                = date("Y-m-d",strtotime('saturday this week'));*/

        /*$days = [];
        $days['Sun'] = $data['start'];
        $dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        for($i = 1; $i<6;$i++)
        {
            $days[$dayNames[$i]] = date("Y-m-d",strtotime($data['start']. " + $i days"));
        }
        $days['Sat'] = $data['end'];*/

        /*print_r($data);
        die;*/
        /*$data['days']   = $days;
        $start_date     = $data['start'];
        $end_date       = $data['end'];
        $channel_id     = $Channel_id;*/
        $schedule       = new Schedule();
        $host_id        = 0;
        $data['start']  = $start;
        $fetch_schedule = $schedule->getCalendarEvents($start, $end, $channel_id, $host_id);
        $data['fetch_schedule'] = $fetch_schedule['calendarEvents'];
        $ids = $fetch_schedule['calendarEvents']['ids'];
        /*$MediaLinking   = new MediaLinking();
        $all_media      = $MediaLinking->get_media($ids, 'Show', 'player');*/
        $MediaLinking       = new Media();
        $data['all_media']  = $MediaLinking->get_media($ids, 'Show', 'player');

        //echo '<pre>';
        //print_r($data['all_media']);
        //die;
        //echo '<pre>';
        $count = 0;
        if($type == 'upcoming'){
            foreach($data['upcoming'] as $k => $v){
                if(isset($fetch_schedule['calendarEvents'][$v]))
                    $count += count($fetch_schedule['calendarEvents'][$v]);
            }
        }
        
        $data['count'] = $count;
        /*print_r($count);
        print_r($fetch_schedule['calendarEvents']);
        die;*/

        
        return view('frontend.shows.zapbox_'.$type)->with($data);
    }

    public function show_details($slug = '', $id = null)
    {
        $show_id = (int)$id;
        if(is_int($show_id)){
            // Get show details if exists
            if (Show::where('id', '=', $show_id)->exists())
            {
                $data['channel_id']     = 2;
                $data['show_id']        = $show_id;
                $Show                   = new Show();
                //$data['show_details']   = $Show->show_details($show_id);
                $data['show_details']   = Show::find($show_id)->toArray();
                //$data['media_id']       = Show::find($show_id)->shows_has_medialinking;
                /*$param                  =   [
                                                $show_id,   // Show ID
                                                'show',     // Main Module
                                                0,         // Sub Module
                                                '120',      // width
                                                '300'       // height
                                            ];*/
                //echo implode(',', $param);die;
                
                //$param                  = implode(',', $param);
                // Show ID
                // Main Module
                // Sub Module
                // width
                // height
                $data['media_info']     = get_media($show_id, 'show', '', 120, 300); 
                $data['hosts']          = $Show->show_hosts($show_id);
                $data['cohost']         = $Show->show_hosts($show_id, 'cohost');
                $data['podcast_host']   = $Show->show_hosts($show_id, 'podcasthost');
                //echo '<pre>';
                //print_r($data['show_details']);
                //print_r($data['hosts']);
                //print_r($data['media_info']);
                //die;
                return view('frontend.shows.show_details')->with($data);
            }
            else
                abort(404, 'Sorry, the show you are looking for could not be found.');
        }
        else
            abort(404, 'Sorry, the show you are looking for could not be found.');
        //echo $show_name;
        /*if(!empty($show_name))
        {
            $temp = explode('.', $show_name);
            if(!empty($temp))
            {
                $temp1      = explode(',', $temp[0]);
                if(!empty($temp)){
                    $show_id    = $temp1[1];
                    if(is_int($show_id)){
                        // Get show details
                    }
                }
            }
        }*/
    }
}
