<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Frontend\Host;
use App\Models\Frontend\MediaLinking;
use Illuminate\Pagination\Paginator;
//use Illuminate\Support\Facades\Paginator;
use Illuminate\Support\Facades\Input;
use View;
use DB;
//use Paginator;

class HostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $page                       = Input::get('page', 1);
        $lastname                   = Input::get('lastname', null);
        $perPage                    = 48;
        $offset                     = 0;
        //if(request()->has('page') && request()->filled('page'))
        //{
            /*$page                   = (int)request()->page + 1;
            $offset                 = $page * $limit;*/
        $offset                     = ($page * $perPage) - $perPage;
        //}
        $data['ajax']               = 0;
        if(request()->has('ajax'))
        {
            $data['ajax']               = request()->input('ajax');
        }

        $channel_id                 = 2;
        $host                       = new Host();
        $count_hosts                = $host->countAllHosts('', $channel_id, 0, 0, null, null, $lastname);
        $total_hosts                = 0;
        if($count_hosts)
            $total_hosts            = count($count_hosts);
        $route                      = route('front_hosts');
        $data['route']              = route('front_hosts');
        $data['page_type']          = 'hosts';
        
        $all_hosts                  = $host->getAllHosts('', $channel_id, $offset, $perPage, null, null, $lastname);
        $queryString                = Input::query();

        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }
        
        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page']       = 1;
            }
            $queryString['m']       = 'hosts';
            $data['all_hosts']      = $this->arrayPaginator($all_hosts, $request, $total_hosts, $perPage, $page, $queryString);
            $view                   = view::make('frontend.hosts.hosts_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = view::make('frontend.ajax_pagination', ['paginator' => $data['all_hosts'], 'page_type'=>'hosts', 'route' => $route, 'queryString' => $queryString]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            $data['all_hosts']      = $this->arrayPaginator($all_hosts, $request, $total_hosts, $perPage, $page, $queryString);
            return view('frontend.hosts.hosts')->with($data);
        }
    }
    public function arrayPaginator($array, $request, $total_hosts, $perPage, $page, $queryString)
    {
        //$page = Input::get('page', 1);
        //$perPage = 48;
        //$offset = ($page * $perPage) - $perPage;
        return new \Illuminate\Pagination\LengthAwarePaginator($array, $total_hosts, $perPage, $page,
            ['path' => $request->url(), 'query' => $queryString]);
        /*return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);*/

    }

    public function co_hosts(Request $request)
    {
        $page                       = Input::get('page', 1);
        $lastname                   = Input::get('lastname', null);
        $perPage                    = 48;
        $offset                     = 0;
        $offset                     = ($page * $perPage) - $perPage;
        $data['ajax']               = 0;
        if(request()->has('ajax'))
        {
            $data['ajax']               = request()->input('ajax');
        }

        $channel_id                 = 2;
        $host                       = new Host();
        $count_hosts                = $host->countAllHosts(2, $channel_id, 0, 0, null, null, $lastname);
        $total_hosts                = 0;
        if($count_hosts)
            $total_hosts            = count($count_hosts);
        $route                      = route('front_co_hosts');
        $data['route']              = route('front_co_hosts');
        $data['page_type']          = 'co-hosts';
        
        $all_hosts                  = $host->getAllHosts(2, $channel_id, $offset, $perPage, null, null, $lastname);
        $queryString                = Input::query();

        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }
        
        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page']       = 1;
            }
            $queryString['m']       = 'co-hosts';
            $data['all_hosts']      = $this->arrayPaginator($all_hosts, $request, $total_hosts, $perPage, $page, $queryString);
            $view                   = view::make('frontend.hosts.co_hosts_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = view::make('frontend.ajax_pagination', ['paginator' => $data['all_hosts'], 'page_type'=>'hosts', 'route' => $route, 'queryString' => $queryString]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            $data['all_hosts']      = $this->arrayPaginator($all_hosts, $request, $total_hosts, $perPage, $page, $queryString);
            return view('frontend.hosts.co_hosts')->with($data);
        }
    }
    public function guests(Request $request)
    {
        $page                       = Input::get('page', 1);
        $lastname                   = Input::get('lastname', null);
        $perPage                    = 48;
        $offset                     = 0;
        $offset                     = ($page * $perPage) - $perPage;
        $data['ajax']               = 0;
        if(request()->has('ajax'))
        {
            $data['ajax']           = request()->input('ajax');
        }

        $channel_id                 = 2;
        $host                       = new Host();
        $count_hosts                = $host->countAllHosts(3, $channel_id, 0, 0, null, null, $lastname);
        $total_hosts                = 0;
        if($count_hosts)
            $total_hosts            = count($count_hosts);
        $route                      = route('front_guests');
        $data['route']              = route('front_guests');
        $data['page_type']          = 'guests';
        
        // Type value 
        // 2 = Co-Hosts
        // 3 = Guests
        // '' = Hosts
        $all_hosts                  = $host->getAllHosts(3, $channel_id, $offset, $perPage, null, null, $lastname);
        $queryString                = Input::query();

        if(isset($queryString['m']))
        {
            unset($queryString['m']);
        }
        
        if($data['ajax']){
            if(!isset($queryString['page']))
            {
                $queryString['page']       = 1;
            }
            $queryString['m']       = 'co-hosts';
            $data['all_hosts']      = $this->arrayPaginator($all_hosts, $request, $total_hosts, $perPage, $page, $queryString);
            $view                   = view::make('frontend.hosts.guests_ajax', $data);
            $output["html"]         = $view->render();
            $pagination             = view::make('frontend.ajax_pagination', ['paginator' => $data['all_hosts'], 'page_type'=>'hosts', 'route' => $route, 'queryString' => $queryString]);
            $output["pagination"]   = $pagination->render();
            echo json_encode($output);
        }
        else{
            $data['all_hosts']      = $this->arrayPaginator($all_hosts, $request, $total_hosts, $perPage, $page, $queryString);
            return view('frontend.hosts.guests')->with($data);
        }
    }
    public function become_host()
    {
        $channel_id                     = 2;
        $data['channel_id']             = $channel_id;

        /* Top Slider code starts here */
        $data['top_slider']             = get_slider(52, 6, $channel_id, 'DESC', false);
        /* Top Slider code starts here */

        /* Resources code starts here */
        $data['solutions']              = get_slider(61, 11, $channel_id, 'ASC');

        /* Testimonials code starts here */
        $data['people_saying']          = get_slider(4, 6, $channel_id, 'ASC');
        $data['client_reviews']         = get_slider(68, 6, $channel_id, 'DESC');
        /* Testimonials code ends here */


        /* Banners code ends here */
        $resolution                     = '728x90';
        $limit                          = 1;
        $top_banner                     = get_banner($resolution, $limit, $channel_id);
        $target                         = '_blank';
        $data['top_banner']             = banner_template($top_banner, $resolution, $target);
        /* Banners code starts here */
        return view('frontend.hosts.become_a_host')->with($data);
    }
}
