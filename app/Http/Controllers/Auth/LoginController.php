<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Storage;
use App\Models\Backend\MediaLinking;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(){
        return 'username';
    }

    public function logout(Request $request) {
        //Delete all medias which are linked to any module while adding
        MediaLinking::where('module_id',0)->where('created_by',Auth::user()->id)->delete();
        $this->deleteDirectories();
        Auth::logout();
        return redirect('/login');
    }

    protected function authenticated(Request $request, $user)
    {
        $user->update([
            //'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login'    => Carbon::now()->toDateTimeString(),
            //'last_login_ip' => $request->getClientIp(),
            'created_ip'    => $request->getClientIp()
        ]);
        if(\Config::get('constants.PARTNER') == getCurrentUserType()){
            return redirect('/partner');
        }else if(\Config::get('constants.HOST') == getCurrentUserType()){
            return redirect('/host');
        }else{
            return redirect('/admin');  
        }
    }

    function deleteDirectories(){
        $current_id = Auth::user()->id;
        $directory = 'tmp/uploads/'.$current_id;
        $dirs = Storage::disk('public')->directories($directory);
        if(!empty($dirs)){
            foreach ($dirs as $dir) {   
                Storage::disk('public')->deleteDirectory($dir);
            }
        }
    }

    // Apply restriction for the inactive users for entering
    protected function sendLoginResponse(Request $request)
    {      
        /*
         * Status 0 for totally Inactive users, also their content will not be accessible and 2 * for the Inactive Login users but their content will be accessible
         */ 
        if($this->guard()->user()->status == 0 || $this->guard()->user()->status == 2){
            $this->guard()->logout();
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors(['active' => 'User in not activated.']);
        }

        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }
}
