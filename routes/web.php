<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/forgotpassword', function () {
//    return view('auth.passwords.forgotpassword');
// })->name('forgotform');
//


//For the Backend routes
Route::group(['namespace' => 'Backend'], function()
{
	include_once(app_path() . '/Http/Routes/backend.php');
	
});

//For the Frontend routes
Route::group(['namespace' => 'Frontend'], function()
{
	include_once(app_path() . '/Http/Routes/frontend.php');
});


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');